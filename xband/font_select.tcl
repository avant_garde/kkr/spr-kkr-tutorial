#######################################################################################
proc font_select {mode} {#  
#
# NOTE    this is the only procedure that modifies a *.vst-file in ..../locals
#
# mode = check   only not available fonts will be modified
# mode = set     all fonts will be modified
#
#                I_FONT is the number of the first font that is not available / to be set
# 
global Wfsel FONTSPEC FONT FONT_AVAILABLE         
global list_FONT_KEYS FONT_KEY
global font numAliases fonts_available
global I_FONT

#-------------------------------------------------------------------------------------
eval set res [catch "exec xlsfonts" list_available_fonts] 
foreach i $list_FONT_KEYS { set FONT_AVAILABLE($i) 0 }

set I_FONT -1
set n -1
set OK 1
foreach i $list_FONT_KEYS {
   incr n
   set f [string trim $FONT($i)]

   foreach fl $list_available_fonts { if {$f==$fl} {set FONT_AVAILABLE($i) 1} } 

   if {$FONT_AVAILABLE($i)==0} {
       if {$I_FONT==-1} {set I_FONT $n}
       set OK 0
       writescr  .d.tt "\n the font $f \n is not available on your machine \n"
   }
}

if {$OK==1} {
    writescr  .d.tt "\n all fonts are available on your machine \n"
    if {$mode=="check"} {return}
}
#
#-------------------------------------------------------------------------------------
#                                 all fonts will be considered to be set
#
if {$mode=="set"} {
   foreach i $list_FONT_KEYS {set FONT_AVAILABLE($i) 0} 
   set I_FONT 0
}
#-------------------------------------------------------------------------------------

set FONT_KEY [lindex $list_FONT_KEYS $I_FONT]

set Wfsel .w_font_select 
    
if [winfo exists $Wfsel] {destroy $Wfsel}

toplevel $Wfsel

# The menus are big, so position the window
# near the upper-left corner of the display
wm geometry $Wfsel +30+30

# Create a frame and buttons along the top
frame $Wfsel.buttons
pack $Wfsel.buttons -side top -fill x
## button $Wfsel.buttons.quit -text close -command "destroy $Wfsel"
button $Wfsel.buttons.reset -text Reset -command font_select_Reset
button $Wfsel.buttons.accept -text Accept -command "font_select_Accept"
pack $Wfsel.buttons.reset $Wfsel.buttons.accept -side right


label $Wfsel.head  -text "font to be set:     $FONT_KEY  "
pack  $Wfsel.head  -pady 5

#
#---------------------------------------------------------- select font
#

CreateRADIOBUTTON $Wfsel grey 12 "family" FONTSPEC FAMILY a fixed s 1 \
                                 ANY times helvetica lucida courier symbol 
set aux 0
foreach item {ANY times helvetica lucida courier symbol} {
   incr aux
   $Wfsel.wFONTSPECFAMILYa.$aux configure -command "font_select_Reset"
}

CreateRADIOBUTTON $Wfsel grey 12 "weight" FONTSPEC WEIGHT c fixed s 1 \
                                 ANY bold medium demibold demi normal book light
set aux 0
foreach item {ANY bold medium demibold demi normal book light} {
   incr aux
   $Wfsel.wFONTSPECWEIGHTc.$aux configure -command "font_select_Reset"
}

CreateRADIOBUTTON $Wfsel grey 12 "slant" FONTSPEC SLANT d fixed s 1 \
                                 ANY i r o
set aux 0
foreach item {ANY i r o} {
   incr aux
   $Wfsel.wFONTSPECSLANTd.$aux configure -command "font_select_Reset"
}

CreateRADIOBUTTON $Wfsel grey 12 "pixels" FONTSPEC PIXELS b fixed s 1 \
                                 ANY 8 10 11 12 14 15 17 18 20 24 25 34
set aux 0
foreach item {ANY 8 10 11 12 14 15 17 18 20 24 25 34} {
   incr aux
   $Wfsel.wFONTSPECPIXELSb.$aux configure -command "font_select_Reset"
}

CreateRADIOBUTTON $Wfsel grey 12 "space" FONTSPEC SPACE e fixed s 1 \
                                 ANY p m c
set aux 0
foreach item {ANY p m c} {
   incr aux
   $Wfsel.wFONTSPECSPACEe.$aux configure -command "font_select_Reset"
}

set FONTSPEC(FAMILY) courier
set FONTSPEC(PIXELS)   14
set FONTSPEC(WEIGHT) medium 
set FONTSPEC(SLANT)  ANY       
set FONTSPEC(SPACE)  ANY      


# An entry widget is used for status messages
entry $Wfsel.buttons.e -textvar status -relief flat
pack $Wfsel.buttons.e -side top -fill x
proc Status { string } {
	global status
	set status $string
	update idletasks
}
# So we can see status messages
tkwait visibility $Wfsel.buttons.e

# Set up a set of menus.  There is one for each
# component of a font name, except that the two resolutions
# are combined and the avgWidth is supressed.
frame $Wfsel.menubar
set font(comps) {foundry family weight slant swidth \
	adstyle pixels points res res2 \
	space avgWidth registry encoding}
foreach x $font(comps) {
	# font lists all possible component values
	# current keeps the current component values
	set font(cur,$x) *
	set font($x) {}
	# Trim out the second resolution and the average width
	if {$x == "res2" || $x == "avgWidth"} {
	    continue
	}
	# The border and highlight thickness are set to 0 so the 
	# button texts run together into one long string.
	menubutton $Wfsel.menubar.$x -menu $Wfsel.menubar.$x.m -text -$x \
		-padx 0 -bd 0 -font fixed \
		-highlightthickness 0
	menu $Wfsel.menubar.$x.m
	pack $Wfsel.menubar.$x -side left
	# Create the initial wild card entry for the component
	$Wfsel.menubar.$x.m add radio -label * \
		-variable font(cur,$x) \
		-value * \
		-command [list DoFont]
}
# Use traces to patch up the supressed font(comps)
trace variable font(cur,res2) r TraceRes2
proc TraceRes2 { args } {
	global font
	set font(cur,res2) $font(cur,res)
}
trace variable font(cur,avgWidth) r TraceWidth
proc TraceWidth { args } {
	global font
	set font(cur,avgWidth) *
}
# Mostly, but not always, the points are 10x the pixels
trace variable font(cur,pixels) w TracePixels
proc TracePixels { args } {
	global font
	catch {
	    # Might not be a number
	    set font(cur,points) [expr 10*$font(cur,pixels)]
	}
}

# Create a listbox to hold all the font names
frame $Wfsel.body
set font(list) [listbox $Wfsel.body.list \
	-setgrid true -selectmode browse \
	-yscrollcommand "$Wfsel.body.scroll set"]
scrollbar $Wfsel.body.scroll -command "$Wfsel.body.list yview"
pack $Wfsel.body.scroll -side right -fill y
pack $Wfsel.body.list -side left -fill both -expand true

# Clicking on an item displays the font
bind $font(list) <ButtonRelease-1> [list SelectFont $font(list) %y]


set font(sampler) "
ABCDEFGHIJKLMNOPQRSTUVWXYZ
abcdefghijklmnopqrstuvwxyz
0123456789
!@#$%^&*()_+-=[]{};:'\"`~,.<>/?\\|
"
set font(errormsg) "

(No matching font)


"



#=========================================================================
proc font_select_setup {  } {

global font Wfsel numAliases list_available_fonts FONTSPEC

#"-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1"

# Use the xlsfonts program to generate a
# list of all fonts known to the server.
Status "Listing fonts..."
if [catch {open "|xlsfonts *"} list_available_fonts] {
	puts stderr "xlsfonts failed $list_available_fonts"
	exit 1
}

$font(list) delete 0 end
set font(num) 0
set numAliases 0
set font(N) 0
while {[gets $list_available_fonts line] >= 0} {
   set FAM [getvalue $line  2 "-"]
   if { $FONTSPEC(FAMILY)=="ANY" || $FONTSPEC(FAMILY)==$FAM} {
   set WEI [getvalue $line  3 "-"]
   if { $FONTSPEC(WEIGHT)=="ANY" || $FONTSPEC(WEIGHT)==$WEI} {
   set SLA [getvalue $line  4 "-"]
   if { $FONTSPEC(SLANT) =="ANY" || $FONTSPEC(SLANT) ==$SLA} {
   set PIX [getvalue $line  7 "-"]
   if { $FONTSPEC(PIXELS)=="ANY" || $FONTSPEC(PIXELS)==$PIX} {
   set SPA [getvalue $line 11 "-"] 
   if { $FONTSPEC(SPACE) =="ANY" || $FONTSPEC(SPACE) ==$SPA} {

	$font(list) insert end $line
	# fonts(all,$i) is the master list of existing fonts
	# This is used to avoid potenially expensive
	# searches for fonts on the server, and to
	# highlight the matching font in the listbox
	# when a pattern is specified.
	set font(all,$font(N)) $line
	incr font(N)
    
	set parts [split $line -]
	if {[llength $parts] < 14} {
		# Aliases do not have the full information
		lappend aliases $line
		incr numAliases
	} else {
		incr font(num)
		# Chop up the font name and record the
		# unique font(comps) in the font array.
		# The leading - in font names means that
		# parts has a leading null element and we
		# start at element 1 (not zero).
		set i 1
		foreach x $font(comps) {
			set value [lindex $parts $i]
			incr i
			if {[lsearch $font($x) $value] < 0} {
				# Missing this entry, so add it
				lappend font($x) $value
			}
		}
	}

   }}}}}
}
# Fill out the menus
foreach x $font(comps) {
	if {$x == "res2" || $x == "avgWidth"} {
	    continue
	}
	foreach value [lsort $font($x)] {
		if {[string length $value] == 0} {
			set label (nil)
		} else {
			set label $value
		}
		$Wfsel.menubar.$x.m  add radio -label $label \
			-variable font(cur,$x) \
			-value $value \
			-command DoFont
	}
}
Status "Found $font(num) fonts and $numAliases aliases"

# A message displays a string in the font.
if {[winfo exists $Wfsel.font(msg)]==0} {
set font(msg) [message $Wfsel.font(msg) -aspect 1000 -borderwidth 10]
}

} 
#=========================================================================

font_select_setup

# This label displays the current font
label $Wfsel.font -textvar font(current) -bd 5 -font fixed

# Now pack the main display
pack $Wfsel.menubar -side top -fill x
pack $Wfsel.body -side top -fill both -expand true
pack $Wfsel.font $font(msg) -side top


proc DoFont {  } {
	global font
	set font(current) {}
	foreach x $font(comps) {
	    append font(current) -$font(cur,$x)
	}
	SetFont
}

proc SelectFont { list y } {
	# Extract a font name from the listbox
	global font
	set ix [$font(list) nearest $y]
	set font(current) [$font(list) get $ix]
	set parts [split $font(current) -]
	if {[llength $parts] < 14} {
		foreach x $font(comps) {
			set font(cur,$x) {}
		}
	} else {
		set i 1
		foreach x $font(comps) {
			set value [lindex $parts $i]
			incr i
			set font(cur,$x) $value
		}
	}
	SetFont
}

proc SetFont {} {
	global font
	# Generate a regular expresson from the font pattern
	regsub -all -- {\(nil\)} $font(current) {} font(current)
	regsub -all -- {\*} $font(current) {[^-]*} pattern
	for {set n 0} {$n < $font(N)} {incr n} {
		if [regexp -- $pattern $font(all,$n)] {
			$font(msg) config -font $font(current) \
				-text $font(sampler)
			catch {$font(list) select clear \
				[$font(list) curselection]}
			$font(list) select set $n
			$font(list) see $n
			return
		}
	}
	$font(msg) config -text $font(errormsg)
}

proc font_select_Reset {} {
	global font
     
        font_select_setup
    
	foreach x $font(comps) {
		set font(cur,$x) *
	}
	DoFont
	Status "$font(num) fonts"

        set font(current) [$font(list) get 0]
        SetFont
}

proc font_select_Accept { } {
	global font FONT FONT_AVAILABLE
        global list_FONT_KEYS FONT_KEY
        global Wfsel
        global I_FONT
        global xband_path
     
        set FONT($FONT_KEY) $font(current)
        set FONT_AVAILABLE($FONT_KEY) 1

        set I_FONT -1
        set n -1        
        set OK 1
        foreach i $list_FONT_KEYS { 
          incr n
          if {$FONT_AVAILABLE($i)==0} {
             if {$I_FONT==-1} {
                set I_FONT $n
                set FONT_KEY [lindex $list_FONT_KEYS $I_FONT]
                $Wfsel.head configure -text "font to be set:     $FONT_KEY  "
             }
             set OK 0
	  }
        }
      
        if {$OK==1} {

           set ff $xband_path/locals/fonts.vst
           if {[file exists $ff]==1} {
               eval set res [catch "exec mv $ff $ff.sav " message]
               writescr  .d.tt "\n $message \n"
               writescr  .d.tt "\n font file  $ff  moved to  $ff.sav \n"
           }                                
           set fil [open $ff w]

           puts $fil "# this table sets the various fonts used by the Xband package"
           puts $fil "# column 0:    colour address used   e.g.   \$FONT(GEN)"
           puts $fil "# column 1:    full font name"
           puts $fil "#"
           puts $fil "# if the specified font is not available a similar one can be selected"
           puts $fil "# use the unix command  xlsfonts  to get a complete list of availabe fonts"
                                                    
           foreach i $list_FONT_KEYS { 
              puts $fil "#"
              puts $fil "#---------------------------"
              puts $fil "#"
              puts $fil "$i  $FONT($i)"
           }
           close $fil

           writescr  .d.tt "\n all fonts are now available on your machine \n"
           destroy $Wfsel
           return
        }
}

font_select_Reset

} ;#END proc