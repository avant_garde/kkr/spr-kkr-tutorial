########################################################################################
#                                                         modify_system
proc modify_system_exec {} {

global RQU RQV RQW
global RQX RQY RQZ  Wpprc
global NQ BRAVAIS
global LATANG LATPAR
global NT NAT NOQ ITOQ NCL NQCL IQ1Q IQ2Q IQECL
global RCLU RCLV RCLW WYCKOFFCL WYCKOFFQ  ICLQ
global SPACEGROUP SPACEGROUP_AP STRUCTURE_TYPE  per_pedes_occupation
global COLOR WIDTH HEIGHT FONT
global RBASX RBASY RBASZ BOA COA ALAT  scale_RBAS
global TABLABX TABLABY TABLABZ
global per_pedes_input_mode sysfile

global NLQ ZT RWS TXTT0 TXTT CONC
global CHECK_NCL CHECK_NQ CHECK_LATPAR CHECK_LATANG CHECK_NQCL
global CHECK_RQX CHECK_RQY CHECK_RQZ
global ALAT_0 RBASX_0 RBASY_0 RBASZ_0 RQX_0 RQY_0 RQZ_0
global RQU_0 RQV_0 RQW_0 per_pedes_occupation_0 
global BOA_0 COA_0 DIMENSION STRUCTURE_SETUP_MODE
global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
global BRAVAIS_ORG

#
#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE  ZRANGE_TYPE  STRUCTURE_SEQUENCE
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R 

set BRAVAIS_ORG $BRAVAIS
set tcl_precision 17
set PRC modify_system_exec

if { $BRAVAIS<0 || $BRAVAIS>14 } {give_warning "." \
     "WARNING \n\n specify Bravais lattice first \n\n " ; return}

if {$DIMENSION=="2D"} {
   set STRUCTURE_SETUP_MODE "2D LAYERS"
} elseif {$DIMENSION=="3D"} {
   set STRUCTURE_SETUP_MODE "PER PEDES"
} else {
   "WARNING \n\n procedure $PRC \n\n cannot deal with \n\n$DIMENSION-systems  \n\n " 
    return
}

#
#------------------------------------------------------------------------- supply tables
#
table_bravais

table_RWS

#---------------------------------------------------------------------------------------

set Wpprc .w_pprc 

if {[winfo exists $Wpprc]} {destroy $Wpprc } 

set win $Wpprc
toplevel_init $Wpprc "MODIFY $DIMENSION-SYSTEM FILE  $sysfile  -- STEP 1" 0 0

wm sizefrom $Wpprc ""
wm minsize  $Wpprc 100 100
frame $Wpprc.head

pack $Wpprc.head   -fill both

button $Wpprc.head.goon -text "all done --- GO ON" \
       -width 25 -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
       -command "structure_per_pedes_all-done-RETURN" \
       -state disabled   
button $Wpprc.head.reset -text "RESET" \
       -width 25 -height $HEIGHT(BUT1) -bg tomato  \
       -command "read_sysfile ; modify_system_exec " 
button $Wpprc.head.cancel -text "CLOSE" \
       -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
       -command "read_sysfile ; destroy $Wpprc" 
pack $Wpprc.head.reset $Wpprc.head.cancel $Wpprc.head.goon  -side left -expand yes -pady 5

#
#=======================================================================================
#                                    store initial values
#
#------------------------ get RQU .. (cryst. units)  and skip trailing 0's
#
convert_basis_vectors_RQ cart_to_cryst

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set RQU($IQ)   [expr 1.0 * $RQU($IQ)] 
    set RQV($IQ)   [expr 1.0 * $RQV($IQ)] 
    set RQW($IQ)   [expr 1.0 * $RQW($IQ)] 
    set RQU_0($IQ)             $RQU($IQ) 
    set RQV_0($IQ)             $RQV($IQ) 
    set RQW_0($IQ)             $RQW($IQ) 

    set RQX($IQ)   [expr 1.0 * $RQX($IQ)] 
    set RQY($IQ)   [expr 1.0 * $RQY($IQ)] 
    set RQZ($IQ)   [expr 1.0 * $RQZ($IQ)] 
    set RQX_0($IQ)             $RQX($IQ) 
    set RQY_0($IQ)             $RQY($IQ) 
    set RQZ_0($IQ)             $RQZ($IQ) 
}
#=======================================================================================

set BGOLD linen                
set BGSCL orange     
set BG_A khaki1
set BG_a gold1
set BG_x yellow1
set BG_b goldenrod1
set we_old 6
set we_new 8
set we_atoms 30

frame $Wpprc.a

frame $Wpprc.a.col1 -bg $BGOLD  ; frame $Wpprc.a.col2  
                                  frame $Wpprc.a.col3   -bg $BGSCL
set ACOL1 $Wpprc.a.col1         ; set ACOL2 $Wpprc.a.col2          
                                  set ACOL3 $Wpprc.a.col3          

pack $Wpprc.a -side top
pack $ACOL1 $ACOL2 $ACOL3 -padx 10 -side left -anchor w


label $ACOL1.head  -text "old settings " -bg $BGOLD
pack  $ACOL1.head -anchor c 
label $ACOL2.head  -text "new settings " 
pack  $ACOL2.head -anchor c 

#=======================================================================================
#                              lattice parameter
#
set ALAT [expr 1.0 * $ALAT] ; # just to skip trailing 0's
set ALAT_0 $ALAT
set BOA_0  $BOA
set COA_0  $COA

frame $ACOL1.alat -bg $BGOLD
label $ACOL1.alat.lab  -text "lattice parameter  A  " -bg $BGOLD
entry $ACOL1.alat.ent  -font $FONT(GEN) -width 7      -bg $BGOLD \
      -textvariable ALAT_0  -relief flat \
      -highlightthickness 0 -state disabled 
label $ACOL1.alat.uni  -text "  \[a.u.\]" -bg $BGOLD

pack  $ACOL1.alat.lab $ACOL1.alat.ent $ACOL1.alat.uni -padx 5  -side left
pack  $ACOL1.alat -padx 3 -pady 7 -anchor w

frame $ACOL2.alat
label $ACOL2.alat.lab  -text "                   A   "
entry $ACOL2.alat.ent  -font $FONT(GEN) -width 10 -relief sunken \
      -textvariable ALAT -fg $COLOR(ENTRYFG) -bg $COLOR(ENTRY) \
      -highlightthickness 0 
label $ACOL2.alat.uni  -text "  \[a.u.\]"

pack  $ACOL2.alat.lab $ACOL2.alat.ent $ACOL2.alat.uni -padx 5  -side left
pack  $ACOL2.alat -padx 3 -pady 7

#=======================================================================================
#                              primitive vectors

# just to skip trailing 0's

for {set i 1} {$i <= 3} {incr i} {
	set RBASX($i) [expr 1.0 * $RBASX($i)]
	set RBASY($i) [expr 1.0 * $RBASY($i)]
	set RBASZ($i) [expr 1.0 * $RBASZ($i)]
}

for {set i 1} {$i <= 3} {incr i} {
	set RBASX_0($i) $RBASX($i)
	set RBASY_0($i) $RBASY($i)
	set RBASZ_0($i) $RBASZ($i)
}
if {$DIMENSION=="2D"} {
   if { $ZRANGE_TYPE=="extended" } {
      for {set i 1} {$i <= 3} {incr i} {
         set RBASX_L($i) [expr 1.0 * $RBASX_L($i)]
         set RBASY_L($i) [expr 1.0 * $RBASY_L($i)]
         set RBASZ_L($i) [expr 1.0 * $RBASZ_L($i)]
         set RBASX_R($i) [expr 1.0 * $RBASX_R($i)]
         set RBASY_R($i) [expr 1.0 * $RBASY_R($i)]
         set RBASZ_R($i) [expr 1.0 * $RBASZ_R($i)]
      }
      
      set N_prim_vec 5
   }
} else {
   for {set i 1} {$i <= 3} {incr i} {
      set RBASX_L($i) $RBASX($i)
      set RBASY_L($i) $RBASY($i)
      set RBASZ_L($i) $RBASZ($i)
      set RBASX_R($i) $RBASX($i)
      set RBASY_R($i) $RBASY($i)
      set RBASZ_R($i) $RBASZ($i)
   }

   set N_prim_vec 3
}

for {set i 1} {$i <= 3} {incr i} {
   set RBASX_L_0($i) $RBASX_L($i)
   set RBASY_L_0($i) $RBASY_L($i)
   set RBASZ_L_0($i) $RBASZ_L($i)
   set RBASX_R_0($i) $RBASX_R($i)
   set RBASY_R_0($i) $RBASY_R($i)
   set RBASZ_R_0($i) $RBASZ_R($i)
}


frame $ACOL1.pvec -bg $BGOLD 
pack  $ACOL1.pvec -side top
frame $ACOL2.pvec 
pack  $ACOL2.pvec -side top

for {set i 1} {$i <= $N_prim_vec} {incr i} {

   frame $ACOL1.pvec.cmp$i -bg $BGOLD

   if {$i<=3} {
      label $ACOL1.pvec.cmp$i.lab  -text "a$i \[A\]" -width $we_old -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASX_0($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -state disabled -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASY_0($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -state disabled -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASZ_0($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -state disabled -bg $BGOLD      
   } else {
      label $ACOL1.pvec.cmp$i.lab  -width $we_old -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -bg $BGOLD
      entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0 -bg $BGOLD    
      if {$i==4} {
         frame $ACOL1.pvec.space -bg $BGOLD
         pack  $ACOL1.pvec.space -pady 2
         $ACOL1.pvec.cmp$i.lab  configure -text "a3 (L)"
         $ACOL1.pvec.cmp$i.x configure -textvariable RBASX_L_0(3) -state normal
         $ACOL1.pvec.cmp$i.y configure -textvariable RBASY_L_0(3) -state normal
         $ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_L_0(3) -state normal
         $ACOL1.pvec.cmp$i.x insert 0 $RBASX_L_0(3)
         $ACOL1.pvec.cmp$i.y insert 0 $RBASY_L_0(3)
         $ACOL1.pvec.cmp$i.z insert 0 $RBASZ_L_0(3)

      } else {
         $ACOL1.pvec.cmp$i.lab  configure -text "a3 (R)"
         $ACOL1.pvec.cmp$i.x configure -textvariable RBASX_R_0(3)
         $ACOL1.pvec.cmp$i.y configure -textvariable RBASY_R_0(3)
         $ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_R_0(3)
         $ACOL1.pvec.cmp$i.x insert end $RBASX_R_0(3)
         $ACOL1.pvec.cmp$i.y insert end $RBASY_R_0(3)
         $ACOL1.pvec.cmp$i.z insert end $RBASZ_R_0(3)
      }        
      $ACOL1.pvec.cmp$i.x configure -state disabled
      $ACOL1.pvec.cmp$i.y configure -state disabled
      $ACOL1.pvec.cmp$i.z configure -state disabled
   }

   pack  $ACOL1.pvec.cmp$i.lab   $ACOL1.pvec.cmp$i.x $ACOL1.pvec.cmp$i.y  \
         $ACOL1.pvec.cmp$i.z     -side left
   pack  $ACOL1.pvec.cmp$i 


   frame $ACOL2.pvec.cmp$i
   if {$i<=3} {
      label $ACOL2.pvec.cmp$i.lab  -text "a$i \[A\]" -width $we_old
      entry $ACOL2.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASX($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
      entry $ACOL2.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASY($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
      entry $ACOL2.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
            -textvariable RBASZ($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
   } else {
      label $ACOL2.pvec.cmp$i.lab -width $we_old
      entry $ACOL2.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
      entry $ACOL2.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
      entry $ACOL2.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
            -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -highlightthickness 0
      if {$i==4} {
         frame $ACOL2.pvec.space
         pack  $ACOL2.pvec.space -pady 2
         $ACOL2.pvec.cmp$i.lab  configure -text "a3 (L)"
         $ACOL2.pvec.cmp$i.x configure -textvariable RBASX_L(3)
         $ACOL2.pvec.cmp$i.y configure -textvariable RBASY_L(3)
         $ACOL2.pvec.cmp$i.z configure -textvariable RBASZ_L(3)
      } else {						    
         $ACOL2.pvec.cmp$i.lab  configure -text "a3 (R)"    
         $ACOL2.pvec.cmp$i.x configure -textvariable RBASX_R(3)
         $ACOL2.pvec.cmp$i.y configure -textvariable RBASY_R(3)
         $ACOL2.pvec.cmp$i.z configure -textvariable RBASZ_R(3)
      }        
   }

   pack  $ACOL2.pvec.cmp$i.lab   $ACOL2.pvec.cmp$i.x $ACOL2.pvec.cmp$i.y  \
         $ACOL2.pvec.cmp$i.z     -side left
   pack  $ACOL2.pvec.cmp$i 

}
label $ACOL1.bot -bg $BGOLD
label $ACOL2.bot
pack $ACOL1.bot  $ACOL2.bot 

#---------------------------------------------------------------------------------------

label $ACOL3.head   -text "scaling of lattice vectors "  -bg $BGSCL
frame $ACOL3.tab -bg $BGSCL
pack  $ACOL3.head $ACOL3.tab
frame $ACOL3.tab.a  -bg $BGSCL
frame $ACOL3.tab.b  -bg $BGSCL
pack  $ACOL3.tab.a $ACOL3.tab.b -padx 10 -side left

#---------------------------------------------------------------------------------------

label $ACOL3.tab.a.lab1 -text "scaling factor"   -bg $BGSCL
entry $ACOL3.tab.a.entsf  -font $FONT(GEN) -width 8 -relief sunken \
      -textvariable scale_RBAS -bg $COLOR(ENTRY) -fg black \
      -highlightthickness 0 
label $ACOL3.tab.a.lab2 -text "expressions\nallowed" -bg $BGSCL
pack  $ACOL3.tab.a.lab1 $ACOL3.tab.a.entsf $ACOL3.tab.a.lab2 -anchor c
$ACOL3.tab.a.entsf delete 0 end
$ACOL3.tab.a.entsf insert end "1.0"
#---------------------------------------------------------------------------------------

label $ACOL3.tab.b.lab1 -text "apply scaling to"   -bg $BGSCL
pack  $ACOL3.tab.b.lab1 -anchor c
set wscal $ACOL3.tab.b

frame $wscal.l1 -bg $BGSCL
button $wscal.l1.a1  -text "a1"  -bg $BG_a -width 5  \
           -command "structure_per_pedes_scale_RBAS a1" -highlightthickness 0
button $wscal.l1.b_A  -text "A"  -bg $BG_A -width 10  \
           -command "structure_per_pedes_scale_RBAS A" -highlightthickness 0 
pack $wscal.l1 -anchor w
pack $wscal.l1.a1 -padx 6 -side left -anchor w
pack $wscal.l1.b_A -side left -padx 25

frame $wscal.l2 -bg $BGSCL
button $wscal.l2.a2  -text "a2"  -bg $BG_a -width 5  \
           -command "structure_per_pedes_scale_RBAS a2" -highlightthickness 0
label $wscal.l2.axis   -text " along cartesian axis" -bg $BGSCL
pack $wscal.l2 -anchor w
pack $wscal.l2.a2 -padx 6 -side left -anchor w
pack $wscal.l2.axis -side left -anchor w

frame $wscal.l3 -bg $BGSCL
button $wscal.l3.a3  -text "a3"  -bg $BG_a -width 5  \
           -command "structure_per_pedes_scale_RBAS a3" -highlightthickness 0 
button $wscal.l3.b_x  -text "x"  -bg $BG_x -width 5  \
           -command "structure_per_pedes_scale_RBAS x" -highlightthickness 0 
button $wscal.l3.b_y  -text "y"  -bg $BG_x -width 5  \
           -command "structure_per_pedes_scale_RBAS y" -highlightthickness 0 
button $wscal.l3.b_z  -text "z"  -bg $BG_x -width 5  \
           -command "structure_per_pedes_scale_RBAS z" -highlightthickness 0 

pack $wscal.l3 -anchor w
pack $wscal.l3.a3 -padx 6 -side left -anchor w
pack $wscal.l3.b_x $wscal.l3.b_y $wscal.l3.b_z -side left -anchor w

if {$DIMENSION=="2D"} {
   if { $ZRANGE_TYPE=="extended" } {
      frame $wscal.space -bg $BGSCL
      button $wscal.a3_L  -text "a3  bulk(L)"  -bg $BG_b  -width 15  \
           -command "structure_per_pedes_scale_RBAS L" -highlightthickness 0 
      button $wscal.a3_S  -text "a3  slab   "  -bg $BG_b  -width 15 \
           -command "structure_per_pedes_scale_RBAS S" -highlightthickness 0 
      button $wscal.a3_R  -text "a3  bulk(R)"  -bg $BG_b  -width 15 \
           -command "structure_per_pedes_scale_RBAS R" -highlightthickness 0 
      pack $wscal.space -anchor w -pady 2
      pack $wscal.a3_L $wscal.a3_S $wscal.a3_R -padx 6  -anchor w
   }
} 

label $wscal.l4 -bg $BGSCL
pack  $wscal.l4 -anchor w
#
#---------------------------------------------------------------------------------------
#                              per_pedes_input_mode
#
set per_pedes_input_mode cartesian

frame $Wpprc.inpmode ; pack  $Wpprc.inpmode
label $Wpprc.inpmode.lab1  -text "input mode for site coordinates   "
label $Wpprc.inpmode.lab2  -textvariable  per_pedes_input_mode

button $Wpprc.inpmode.but -text "change to:   crystallographic" \
       -command 	structure_per_pedes_change_input_mode  -bg $COLOR(LIGHTBLUE)  

pack  $Wpprc.inpmode.lab1 $Wpprc.inpmode.lab2 -pady 5  -side left 
pack  $Wpprc.inpmode.but  -padx 25  -side left 


#
#=======================================================================================
#                                set display size
#=======================================================================================
frame $Wpprc.disp_Q
pack $Wpprc.disp_Q -anchor w  -pady 5 
     
set NQ_display         12
set NQ_display_overlap  3

if {$NQ <= $NQ_display} {set NQ_display $NQ}

if {$NQ > $NQ_display} {
   label $Wpprc.disp_Q.label -text "switch lattice site display" -padx 30
   button $Wpprc.disp_Q.but_top -text "top"      -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc top"
   button $Wpprc.disp_Q.but_fwd -text "forward"  -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc forward"
   button $Wpprc.disp_Q.but_bwd -text "backward" -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc backward"
   button $Wpprc.disp_Q.but_bot -text "bottom"   -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc bottom" 
   label $Wpprc.disp_Q.range -text "displaying $NQ_display out of $NQ sites" -padx 30

   pack $Wpprc.disp_Q.label $Wpprc.disp_Q.but_top $Wpprc.disp_Q.but_fwd \
        $Wpprc.disp_Q.but_bwd  $Wpprc.disp_Q.but_bot $Wpprc.disp_Q.range -side left 

}
#---------------------------------------------------------------------------------------
if {$DIMENSION=="2D"} {
   if { $ZRANGE_TYPE=="extended" } {
      set w_color_field_1 8
      set w_color_field_2 8
   } else {
      set w_color_field_1 1
      set w_color_field_2 8
   }      
} else {
   set w_color_field_1 1
   set w_color_field_2 1
}
#=======================================================================================
#                                header of site table
#
frame $Wpprc.coord
pack  $Wpprc.coord  -fill both -pady 15

set i 0
frame $Wpprc.coord.line($i)
set TABLABX " x \[A\]"
set TABLABY " y \[A\]"
set TABLABZ " z \[A\]"

entry $Wpprc.coord.line($i).entry_0($i,1) -font $FONT(GEN) -width $we_old\
      -relief flat -textvariable TABLABX -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled
entry $Wpprc.coord.line($i).entry_0($i,2) -font $FONT(GEN) -width $we_old\
      -relief flat -textvariable TABLABY -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled
entry $Wpprc.coord.line($i).entry_0($i,3) -font $FONT(GEN) -width $we_old\
      -relief flat -textvariable TABLABZ -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled

label $Wpprc.coord.line($i).label($i) -text " " -width 8
label $Wpprc.coord.line($i).spacer    -text " " -width $w_color_field_1
label $Wpprc.coord.line($i).struc     -text " " -width $w_color_field_2
 
entry $Wpprc.coord.line($i).entry($i,1) -font $FONT(GEN) -width $we_new\
      -relief flat -textvariable TABLABX -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled
entry $Wpprc.coord.line($i).entry($i,2) -font $FONT(GEN) -width $we_new\
      -relief flat -textvariable TABLABY -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled
entry $Wpprc.coord.line($i).entry($i,3) -font $FONT(GEN) -width $we_new\
      -relief flat -textvariable TABLABZ -fg $COLOR(ENTRYFG) -highlightthickness 0\
      -state disabled
button $Wpprc.coord.line($i).occupy  -text " " -width 7 -relief flat
button $Wpprc.coord.line($i).copy    -text " " -width 7 -relief flat
button $Wpprc.coord.line($i).reset   -text " " -width 7 -relief flat
label  $Wpprc.coord.line($i).atoms  -width $we_atoms -textvariable per_pedes_dummy \
      -font $FONT(GEN) -fg $COLOR(ENTRYFG)
#...................................................................
    
if {$DIMENSION=="2D"} {
   pack  $Wpprc.coord.line($i).spacer -side left -fill y -padx 7
   pack  $Wpprc.coord.line($i).struc  -side left -fill y -padx 7
   $Wpprc.coord.line(0).struc  configure -text " struct"
} else {
   pack  $Wpprc.coord.line($i).spacer\
         $Wpprc.coord.line($i).struc -side left -expand yes -fill y
}
pack  $Wpprc.coord.line($i).entry_0($i,1)\
      $Wpprc.coord.line($i).entry_0($i,2)\
      $Wpprc.coord.line($i).entry_0($i,3)\
      $Wpprc.coord.line($i).label($i)\
      $Wpprc.coord.line($i).entry($i,1)\
      $Wpprc.coord.line($i).entry($i,2)\
      $Wpprc.coord.line($i).entry($i,3)\
      $Wpprc.coord.line($i).occupy\
      $Wpprc.coord.line($i).atoms -side left -expand yes
pack  $Wpprc.coord.line($i) -anchor w

#         $Wpprc.coord.line($i).copy\
#         $Wpprc.coord.line($i).reset\
    
#=======================================================================================
#                        loop over displayed IQ's
#=======================================================================================
for {set IQ 1} {$IQ <= $NQ} {incr IQ} { set per_pedes_occupation($IQ) "" }
#
#
set IQ_display_start 1
set IQ_display_end   $NQ_display

for {set i 1} {$i <= $NQ_display} {incr i} {
    set IQ $i

#        just to skip trailing 0's

     set RQX($IQ) [expr 1.0 * $RQX($IQ)]
     set RQY($IQ) [expr 1.0 * $RQY($IQ)]
     set RQZ($IQ) [expr 1.0 * $RQZ($IQ)]

    frame $Wpprc.coord.line($i)

    entry $Wpprc.coord.line($i).entry_0($i,1) -font $FONT(GEN)  \
           -width $we_old -relief sunken -bg $BGOLD -fg $COLOR(ENTRYFG)\
       -textvariable RQX_0($IQ) -highlightthickness 0 -state disabled
    entry $Wpprc.coord.line($i).entry_0($i,2) -font $FONT(GEN)  \
           -width $we_old -relief sunken -bg $BGOLD -fg $COLOR(ENTRYFG)\
       -textvariable RQY_0($IQ) -highlightthickness 0 -state disabled
    entry $Wpprc.coord.line($i).entry_0($i,3) -font $FONT(GEN)  \
           -width $we_old -relief sunken -bg $BGOLD -fg $COLOR(ENTRYFG)\
       -textvariable RQZ_0($IQ) -highlightthickness 0 -state disabled

    label $Wpprc.coord.line($i).label($i) -text "site $i: " -width 8
    label $Wpprc.coord.line($i).spacer    -text " " -width $w_color_field_1
    label $Wpprc.coord.line($i).struc     -text " " -width $w_color_field_2

    entry $Wpprc.coord.line($i).entry($i,1) -font $FONT(GEN)  \
           -width $we_new -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQX($IQ) -highlightthickness 0
    entry $Wpprc.coord.line($i).entry($i,2) -font $FONT(GEN)  \
           -width $we_new -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQY($IQ) -highlightthickness 0
    entry $Wpprc.coord.line($i).entry($i,3) -font $FONT(GEN)  \
           -width $we_new -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQZ($IQ) -highlightthickness 0
#    button $Wpprc.coord.line($i).occupy  -text "occupy" -bg $COLOR(GOON) -width 7 \
#	       -command "structure_per_pedes_read_coord_occupy_entry $i"
    button $Wpprc.coord.line($i).occupy  -text "occupy" -bg $COLOR(GOON) -width 7 \
	       -command "xxxxxxxxxxxxxxxxx"
    proc xxxxxxxxxxxxxxxxx {} {
give_warning "." \
     "WARNING \n\n SORRY - change of occupation not yet \n\n " 
    }

    button $Wpprc.coord.line($i).copy  -text "copy" -bg $COLOR(LIGHTBLUE) -width 7 \
	       -command "structure_per_pedes_read_coord_copy_entry $i"
    button $Wpprc.coord.line($i).reset  -text "reset" -bg $COLOR(CLOSE)  -width 7 \
	       -command "structure_per_pedes_read_coord_reset_entry $i" 
    label  $Wpprc.coord.line($i).atoms -justify left \
           -textvariable per_pedes_occupation($IQ) \
           -font $FONT(GEN) -fg $COLOR(ENTRYFG)

    if {$DIMENSION=="2D"} {
       pack  $Wpprc.coord.line($i).spacer -side left -fill y -padx 7
       pack  $Wpprc.coord.line($i).struc  -side left -fill y -padx 7
    } else {
       pack  $Wpprc.coord.line($i).spacer\
             $Wpprc.coord.line($i).struc -side left -expand yes -fill y
    }
    pack  $Wpprc.coord.line($i).entry_0($i,1)\
          $Wpprc.coord.line($i).entry_0($i,2)\
          $Wpprc.coord.line($i).entry_0($i,3)\
          $Wpprc.coord.line($i).label($i)\
          $Wpprc.coord.line($i).entry($i,1)\
          $Wpprc.coord.line($i).entry($i,2)\
          $Wpprc.coord.line($i).entry($i,3)\
          -side left -expand yes
#   $Wpprc.coord.line($i).occupy        $Wpprc.coord.line($i).copy

#   if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {
#	pack  $Wpprc.coord.line($i).reset -side left -expand yes
#   }
    pack  $Wpprc.coord.line($i).atoms -side left -expand yes

    pack  $Wpprc.coord.line($i) -anchor w

    if {$DIMENSION=="2D"} {
       $Wpprc.coord.line($i).entry($i,1) configure -state disabled
       $Wpprc.coord.line($i).entry($i,2) configure -state disabled
       $Wpprc.coord.line($i).entry($i,3) configure -state disabled
    }
}
#=======================================================================================

#
#---------------------------------- activate side bars in case of 2D
#

command_IQ_display $Wpprc top

    
#
#---------------------------------------- set up table of equivalent sites
#
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {set NQCL($ICL) 0}

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set ICL $ICLQ($IQ)
   incr NQCL($ICL)
   set IQECL($NQCL($ICL),$ICL) $IQ
}

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set ICL $ICLQ($IQ)
   set NQEQ($IQ) 0
   for {set JQ 1} {$JQ <= $NQ} {incr JQ} {
      set JCL $ICLQ($JQ)
      if { $ICL == $JCL } { incr NQEQ($IQ) }
   }
}
#
#---------------------------------------- initialize WYCKOFFQ
#
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set WYCKOFFQ($IQ) $WYCKOFFCL($ICLQ($IQ))
}
#
#---------------------------------------- initialize TXTT0
#
for {set IT 1} {$IT  <= $NT}        {incr IT } {
         regsub "_.*" $TXTT($IT) "" TXTT0($IT) 
}
#
#----------------------------------------- fill in initial occupation
#
for {set IQ  1} {$IQ  <= $NQ}        {incr IQ } {
   set txt2 "  "
   if { $NOQ($IQ) > 0 } {
      for {set i 1} {$i <= $NOQ($IQ)} {incr i} {
         set IT $ITOQ($i,$IQ)
         set TXT [string range "$TXTT0($IT)    " 0 1]
         set txt2 "$txt2[format "%2i %2s %4.2f" $IT $TXT $CONC($IT)]  "
      }
   }
   set per_pedes_occupation($IQ)   $txt2 
   set per_pedes_occupation_0($IQ) $txt2 
}

$Wpprc.head.goon configure -state normal

#=======================================================================================
#                                                         structure_per_pedes_scale_RBAS
#
proc structure_per_pedes_scale_RBAS { key  } {  

global per_pedes_input_mode 		      
global TABLABX TABLABY TABLABZ
global RBASX RBASY RBASZ  scale_RBAS ALAT

set PRC structure_per_pedes_scale_RBAS

#--------------------------------------- convert  scale_RBAS  to number
set scl [expr 1.0*$scale_RBAS]
set scale_RBAS "1.0"

if {[expr abs($scl-1)]<0.000001} { return }

if {$key=="A"} {
   set ALAT [expr $scl * $ALAT ]
   debug $PRC "scaling   ALAT by factor $scl"
   return
}
#
#-------------- convert to crystallographic units if necessary
#
if {$per_pedes_input_mode=="cartesian"} {			      
   debug $PRC "converting basis vectors to crystallographic units"
   convert_basis_vectors_RQ cart_to_cryst
}
#
#-------------- scale ONE of the primitive vectors  RBAS(i)
#
if {[string range $key 0 0] == "a" } {
   set i [string range $key 1 1]

   set RBASX($i) [expr $scl * $RBASX($i) ]
   set RBASY($i) [expr $scl * $RBASY($i) ]
   set RBASZ($i) [expr $scl * $RBASZ($i) ]
   debug $PRC "scaling prim. vector RBAS($i) by factor $scl"

#
#-------------- scale ALL primitive vectors  RBAS(i) along cartesian axis
#
} else {

   if {$key=="x"} {
      debug $PRC "scaling x-coord. of prim. vectors by factor $scl"
      set RBASX(1) [expr $scl * $RBASX(1) ]
      set RBASX(2) [expr $scl * $RBASX(2) ]
      set RBASX(3) [expr $scl * $RBASX(3) ]

   } elseif {$key=="y"} {
      debug $PRC "scaling y-coord. of prim. vectors by factor $scl"
      set RBASY(1) [expr $scl * $RBASY(1) ]
      set RBASY(2) [expr $scl * $RBASY(2) ]
      set RBASY(3) [expr $scl * $RBASY(3) ]

   } elseif {$key=="z"} {
      debug $PRC "scaling z-coord. of prim. vectors by factor $scl"
      set RBASZ(1) [expr $scl * $RBASZ(1) ]
      set RBASZ(2) [expr $scl * $RBASZ(2) ]
      set RBASZ(3) [expr $scl * $RBASZ(3) ]

   } elseif {$key=="L"} {
      debug $PRC "scaling prim. vector a3 for bulk(L) by factor $scl"
      give_warning "."  "WARNING \n\n still to be done  \n\n " ; return
   } elseif {$key=="R"} {
      debug $PRC "scaling prim. vector a3 for bulk(R) by factor $scl"
      give_warning "."  "WARNING \n\n still to be done  \n\n " ; return

   } elseif {$key=="S"} {
      debug $PRC "scaling prim. vector a3 for slab by factor $scl"
      give_warning "."  "WARNING \n\n still to be done  \n\n " ; return

   } else {
      give_warning "."  "WARNING \n\n option $key \n\n not available in $PRC\n\n "
      return
   } 

} 
#
#-------------- convert to cartesian units to apply scaling
#
   convert_basis_vectors_RQ cryst_to_cart
				      
} ; #		                                  END     structure_per_pedes_scale_RBAS
#=======================================================================================

#=======================================================================================
proc structure_per_pedes_change_input_mode { } {  
global per_pedes_input_mode Wpprc	      
global TABLABX TABLABY TABLABZ
global NQ_display NQ_display_overlap IQ_display_start IQ_display_end

if {$per_pedes_input_mode=="cartesian"} {			      
   $Wpprc.inpmode.but configure -text "change to:   cartesian" 
   set per_pedes_input_mode crystallographic      
   set TABLABX "u \[a1\]"
   set TABLABY "v \[a2\]"
   set TABLABZ "w \[a3\]"
   convert_basis_vectors_RQ cart_to_cryst
   for {set i 1} {$i <= $NQ_display} {incr i} {
      set IQ $i
      $Wpprc.coord.line($i).entry_0($i,1) configure -textvariable RQU_0($IQ)
      $Wpprc.coord.line($i).entry_0($i,2) configure -textvariable RQV_0($IQ)
      $Wpprc.coord.line($i).entry_0($i,3) configure -textvariable RQW_0($IQ)
      $Wpprc.coord.line($i).entry($i,1)   configure -textvariable RQU($IQ)
      $Wpprc.coord.line($i).entry($i,2)   configure -textvariable RQV($IQ)
      $Wpprc.coord.line($i).entry($i,3)   configure -textvariable RQW($IQ)
   }
} else {					      
   $Wpprc.inpmode.but configure -text "change to:   crystallographic" 
   set per_pedes_input_mode cartesian
   set TABLABX "x \[A\]"
   set TABLABY "y \[A\]"
   set TABLABZ "z \[A\]"
   convert_basis_vectors_RQ cryst_to_cart
   for {set i 1} {$i <= $NQ_display} {incr i} {
      set IQ $i
      $Wpprc.coord.line($i).entry_0($i,1) configure -textvariable RQX_0($IQ)
      $Wpprc.coord.line($i).entry_0($i,2) configure -textvariable RQY_0($IQ)
      $Wpprc.coord.line($i).entry_0($i,3) configure -textvariable RQZ_0($IQ)
      $Wpprc.coord.line($i).entry($i,1)   configure -textvariable RQX($IQ)
      $Wpprc.coord.line($i).entry($i,2)   configure -textvariable RQY($IQ)
      $Wpprc.coord.line($i).entry($i,3)   configure -textvariable RQZ($IQ)
   }
}						      

} ; #                                          END structure_per_pedes_change_input_mode
#=======================================================================================

#=======================================================================================
#                                                    structure_per_pedes_all-done-RETURN
#
#-------------------------- check whether all is done and leave
#-------------------------- check symmetry and calculate new BOA and COA
#
#
proc structure_per_pedes_all-done-RETURN { } {  

global Wpprc scale_RBAS per_pedes_input_mode
global ALAT_0 RBASX_0 RBASY_0 RBASZ_0 RQU_0 RQV_0 RQW_0 per_pedes_occupation_0 
global BOA_0 COA_0 sym_lowered
global RBASX RBASY RBASZ RQU RQV RQW per_pedes_occupation
global ALAT BOA COA NQ LATPAR LATANG RQX RQY RQZ

set PRC structure_per_pedes_all-done-RETURN

#------------------------------------------- scaling still to be done ?

if { [expr abs($scale_RBAS-1)] > 0.0001 } {
   give_warning "." \
   "WARNING \n\n scaling factor set to $scale_RBAS \n
            \n but no scaling done \n\n reset to 1 or execute scaling \n" 
   return
}

#                           ===================================
#                           ALL DONE - NOW COMPLETE INFORMATION
#                           ===================================
    
writescrd .d.tt $PRC "\n\nINFO from <$PRC>   -------- 
                  \nleaving the procedure  <modify_system_exec>
                  \nlast input mode:  $per_pedes_input_mode \n\n"

#----------------------- last modifications of site coordinates 
#----------------------- might still have to be accounted
#----------------------- update complementary representation

if {$per_pedes_input_mode=="cartesian"} {
   convert_basis_vectors_RQ cart_to_cryst
} else {
   convert_basis_vectors_RQ cryst_to_cart
}

#
#----------------------------------------------------- set_lattice_parameters_using_RBAS
#

set_lattice_parameters_using_RBAS


#----------------------- may symmetry be lowered by modifications ?

set TOL  0.00001
set sym_lowered 0

#------- vx, vy, vz != 0 indicates that primitive basis vectors have not simply be scaled

for {set i 1} {$i <= 3} {incr i} {
    set vx [ cross_prod $RBASX($i)   $RBASY($i)   $RBASZ($i) \
                        $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) "x" ]
    set vy [ cross_prod $RBASX($i)   $RBASY($i)   $RBASZ($i) \
                        $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) "y" ]
    set vz [ cross_prod $RBASX($i)   $RBASY($i)   $RBASZ($i) \
                        $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) "z" ]
    if {[expr abs($vx)]>$TOL || [expr abs($vy)]>$TOL || [expr abs($vz)]>$TOL} {
       set sym_lowered 1
       writescrd .d.tt $PRC "orientation of primitive vector $i has been modified \n"
    }
}

for {set i 1} {$i <= 3} {incr i} {
    set d00 [ dot_prod $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) \
                       $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) ]
    set d11 [ dot_prod $RBASX($i)   $RBASY($i)   $RBASZ($i) \
                       $RBASX($i)   $RBASY($i)   $RBASZ($i)   ]
    set d01 [ dot_prod $RBASX($i)   $RBASY($i)   $RBASZ($i) \
                       $RBASX_0($i) $RBASY_0($i) $RBASZ_0($i) ]
    set LRBAS_0($i) [expr pow($d00,0.5) ] 
    set LRBAS($i)   [expr pow($d11,0.5) ]
    set rat01($i)   [expr $d01 / ($LRBAS_0($i) * $LRBAS($i)) ]
}

if { [expr abs($BOA-$BOA_0)]>$TOL && [expr abs($BOA_0-1)]<$TOL } {
   set sym_lowered 1
   writescrd .d.tt $PRC "b/a - ratio has been changed from 1 to $BOA\n"
}
if { [expr abs($COA-$COA_0)]>$TOL && [expr abs($COA_0-1)]<$TOL } {
   set sym_lowered 1
   writescrd .d.tt $PRC "c/a - ratio has been changed from 1 to $COA\n"
}

#-------------------------- get RQU .. (cryst. units) and store without overwriting  RQX

if { $sym_lowered == 0 } {

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      if {[expr abs($RQU_0($IQ) - $RQU($IQ))]>$TOL || \
          [expr abs($RQV_0($IQ) - $RQV($IQ))]>$TOL || \
          [expr abs($RQW_0($IQ) - $RQW($IQ))]>$TOL} {
          set sym_lowered 1
          writescrd .d.tt $PRC "coordinates of site $IQ have been modified\n "
      }
      if {$per_pedes_occupation_0($IQ) != $per_pedes_occupation($IQ)} {
          set sym_lowered 1
          writescrd .d.tt $PRC "occupation of site $IQ has been modified\n "
      }
   }
}
#---------------------------------------------------------------------------------------

    if { $sym_lowered == 0 } {
       writescr .d.tt "the symmetry of the system have not be changed by modifications\n"
    } else {
       writescr .d.tt "the symmetry of the system might have changed by modifications\n"
    }

    destroy $Wpprc 
    structure_window_modify
}
#=======================================================================================

#=======================================================================================
proc modify_system_reset_entry {IQ} {
    global RQX RQY RQZ
    set RQX($IQ) "" 
    set RQY($IQ) "" 
    set RQZ($IQ) "" 
}

#=======================================================================================
proc modify_system_occupy_entry {IQ} {
   global RCLU RCLV RCLW RQX RQY RQZ 
   global NQCL WYCKOFFCL WYCKOFFQ

   if {$RQX($IQ)=="" || $RQY($IQ)=="" || $RQZ($IQ)=="" } {
        give_warning "." \
       "WARNING \n\n specify atomic positions first \n\n " ; return
   }

   set ICL $IQ
   set RCLU($ICL) $RQX($IQ)
   set RCLV($ICL) $RQY($IQ)
   set RCLW($ICL) $RQZ($IQ)
   set NQCL($ICL) 1

   sites_occupation $IQ 1

}

#=======================================================================================
proc modify_system_copy_entry {IQ} {
#
#---- this procedure scans all sites following IQ until
#     a site JQ is found that is not completely specified
#     then the missing information is copied from IQ to JQ

    set PRC "modify_system_copy_entry"

    global RWS ITOQ ZT CONC TXTT TXTT0 NQCL Wcsys NQ NLQ NCL NT NQCL
    global Wcsys structure_data_complete IQ1Q IQ2Q NLOCCMAX
    global NOQ ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG Wocc_OK_but
    global W_sites_list RQX RQY RQZ Wocc  WYCKOFFQ NCPA 
    global SPACEGROUP Wpprc   per_pedes_occupation
 
    global RQX RQY RQZ NQ NOQ

    set IQNEXT 0
    set flag_RQ  0
    set flag_OCC 0

    set JQ $IQ
    while {$JQ < $NQ} {
        incr JQ
#------ no position specified for site JQ
	if { $RQX($JQ)== "" } {
           if { $RQY($JQ)== "" } {
              if { $RQZ($JQ)== "" } {
                 set IQNEXT $JQ
                 set JQ $NQ
                 set flag_RQ 1
              }
           }
        } 
#------ no occupation specified for site JQ
        if {$NOQ($JQ)==0} {
           set IQNEXT $JQ
           set JQ $NQ
           set flag_OCC 1
        }
    }

    set JQ $IQNEXT

    debug $PRC "IQ $IQ   JQ $JQ   NQ $NQ"
    if {$IQNEXT!=0} {
       if {$flag_RQ == 1 } {
          set RQX($JQ) $RQX($IQ) 
          set RQY($JQ) $RQY($IQ) 
          set RQZ($JQ) $RQZ($IQ) 
       }
#
#---------------------- copy occupation information if site IQ is occupied
#
       if {$NOQ($IQ) > 0 } {
          set NOCC $NOQ($IQ)
          for {set i 1} {$i <= $NOCC} {incr i} {
            set IT $ITOQ($i,$IQ)
            set ZOCC($i)     $ZT($IT)
            set CONCOCC($i)  $CONC($IT)
            set TXTOCC($i)   $TXTT0($IT)
            set RWSOCC($i)   $RWS($IQ)
            set RWSOCCAVRG   $RWS($IQ)
            set NLOCCMAX     $NLQ($IQ)
            set NLOCC($i)    $NLQ($IQ)
          }          
        
          sites_occupation_done $JQ $NOCC 
 
       }
    }
} 
#            modify_system_copy_entry END
#---------------------------------------------------------


}
#                                                                      modify_system END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

########################################################################################
proc structure_window_modify { } {

global sysfile syssuffix sym_lowered
global BOA COA SPACEGROUP  STRUCTURE_TYPE SPACEGROUP_AP TABCHSYM iprint Wcsys
global system NQ NCL NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT TXTT0  RWS ZT
global RBASX RBASY RBASZ
global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS ALAT IQ1Q IQ2Q
global W_sites_list NT WYCKOFFQ WYCKOFFCL DIMENSION CHECK_TABLE
global editor edback edxterm edoptions xband_path 
global IQECL
global VISUAL
global COLOR WIDTH HEIGHT FONT
global LATTICE_TYPE ZRANGE_TYPE
global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
global NQ_bulk_L NQ_bulk_R IQ0_Q ICLQ
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R


    set PRC "structure_window_modify"

    set win $Wcsys
    if {[winfo exists $win] == 1} {destroy $win}
    toplevel $win -width 400 -height 300 -visual $VISUAL
    wm geometry     $win +20+20
    wm positionfrom $win user
    wm title        $win "MODIFY $DIMENSION-SYSTEM FILE  $sysfile  -- STEP 2"

    insert_topbuttons $Wcsys compile_h.hlp

    $Wcsys.a.l configure -state disabled

    bind $Wcsys.a.e <Button-1> {
#  if [compile_focus ""] {
#    writescr $Wcsys.d.tt "input accepted; press once more \"close\""
#  } else {
    destros $Wcsys; unlock_list ; Bend
#  }
}

bind $Wcsys.a.h <Button-1> { 
    execunixcmd "cat $xband_path/help/create_system_h.hlp"
}
#---------------------------------------------------------------------------------------
#                           initialize variables
#---------------------------------------------------------------------------------------
set MAG_DIR(1) 0 ;  set MAG_DIR(2) 0 ;  set MAG_DIR(3) 1
set CHECK_TABLE 0
   
set SPACEGROUP 0 
set SPACEGROUP_AP 0 
set BRAVAIS 0    
if {![info exists STRUCTURE_TYPE]} {set STRUCTURE_TYPE "UNKNOWN"}
    
#---------------------------------------------------------------------------------------
#                            section  "c"
#---------------------------------------------------------------------------------------
  frame $Wcsys.c -borderwidth {2} -height {30}  -relief {raised}
  pack $Wcsys.c -in $Wcsys -expand yes -fill x

#---------------------------------------------------------------------------------------

  frame $Wcsys.c.name -borderwidth {2} -height {30}

  label $Wcsys.c.name.label1 -text {name of system-file}

  entry $Wcsys.c.name.entry \
    -textvariable {sysfile} -font $FONT(GEN) -width {30} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  pack configure $Wcsys.c.name.label1  -expand true -fill x

  pack configure $Wcsys.c.name.entry  
#---------------------------------------------------------------------------------------
  button $Wcsys.c.close -text "CLOSE" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
         -command "destros $Wcsys; unlock_list "
  
  button $Wcsys.c.goon -text "DONE - RETURN" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(DONE) \
         -command "structure_window-done-RETURN" -state disabled

#---------------------------------------------------------------------------------------

    pack $Wcsys.c.name $Wcsys.c.goon $Wcsys.c.close  \
  	 -in $Wcsys.c  -side left -fill x -expand yes

    # structure information
    frame $Wcsys.struc -relief raised -borderwidth 2 

    frame $Wcsys.struc.mode

#---------------------------------------------------------------------------------------
    label $Wcsys.struc.sym_lowered \
     -text "\nThe symmetry of the system might be lowered by modifications made\n\
              the program <findsym> tried to find out new symmetry \n" \
     -bg yellow -fg black -relief flat
    pack  $Wcsys.struc.sym_lowered -expand yes -fill both
#---------------------------------------------------------------------------------------

    frame $Wcsys.struc.b

    set Latpar $Wcsys.struc.b.latpar
    frame $Latpar

    set widlab  5
    set wident 15

    frame $Latpar.info
    label $Latpar.info.a -text " lattice parameters          A = " -pady 5
    label $Latpar.info.b -width $wident -font $FONT(GEN) \
	    -textvariable ALAT
    label $Latpar.info.c -text "\[a.u.\]"
    pack $Latpar.info.a $Latpar.info.b $Latpar.info.c -side left

    frame $Latpar.first
    frame $Latpar.secon
    frame $Latpar.third

    label $Latpar.first.label1 -width $widlab -text "a = " -pady 5  -justify right
    label $Latpar.first.label2 -width $widlab -text "b = " -pady 5  -justify right
    label $Latpar.first.label3 -width $widlab -text "c = " -pady 5  -justify right

    label $Latpar.secon.label1 -width $widlab -text "      " -pady 5 -justify right
    label $Latpar.secon.label2 -width $widlab -text "b/a = " -pady 5 -justify right
    label $Latpar.secon.label3 -width $widlab -text "c/a = " -pady 5 -justify right
				      
    label $Latpar.third.label1 -width $widlab -font $FONT(SBL) \
	   -text "a = " -pady 5 -justify right
    label $Latpar.third.label2 -width $widlab -font $FONT(SBL) \
	   -text "b = " -pady 5 -justify right
    label $Latpar.third.label3 -width $widlab -font $FONT(SBL) \
	   -text "g = " -pady 5 -justify right

    label $Latpar.secon.a(1) -width $wident -font $FONT(GEN)
    label $Latpar.secon.a(2) -width $wident -font $FONT(GEN) \
	    -textvariable BOA
    label $Latpar.secon.a(3) -width $wident -font $FONT(GEN)  \
	    -textvariable COA
 
    for {set i 1} {$i <=3} {incr i} {
	label $Latpar.first.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATPAR($i) -justify left
	label $Latpar.third.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATANG($i) -justify left
	
        pack $Latpar.first.label$i  $Latpar.first.a($i) -side left
        pack $Latpar.secon.label$i  $Latpar.secon.a($i) -side left
        pack $Latpar.third.label$i  $Latpar.third.a($i) -side left
    }


    pack $Latpar -side left
    pack $Latpar.info $Latpar.first  $Latpar.secon  $Latpar.third  -fill both \
	   -expand yes -anchor c


    set Info $Wcsys.struc.b.info

    frame $Info
    pack $Info -side left

    set ww1 20
    set ww2 5
    set ww3 5

    frame $Info.f0
    label $Info.f0.c1 -width $ww1 -anchor w -justify left -text "number of sites"
    label $Info.f0.c2 -width $ww2 -anchor w -justify left -text "NQ"
    label $Info.f0.e  -width $ww3 -font $FONT(GEN) -textvariable NQ
		      
    frame $Info.f1    
    label $Info.f1.c1 -width $ww1 -anchor w -justify left -text "inequivalent sites"
    label $Info.f1.c2 -width $ww2 -anchor w -justify left -text "NCL"
    label $Info.f1.e  -width $ww3 -font $FONT(GEN) -textvariable NCL

    frame $Info.f2    
    label $Info.f2.c1 -width $ww1 -anchor w -justify left -text "atom types"
    label $Info.f2.c2 -width $ww2 -anchor w -justify left -text "NT"
    label $Info.f2.e  -width $ww3 -font $FONT(GEN) -textvariable NT

    frame $Info.f3    
    label $Info.f3.c1 -width $ww1 -anchor w -justify left -text "structure type"
    entry $Info.f3.e  -font $FONT(GEN) -textvariable STRUCTURE_TYPE \
                      -relief sunken

    if {$DIMENSION == "2D"} {
       frame $Info.f4    
       label $Info.f4.c1 -width $ww1 -anchor w -justify left -text "lattice type"
       label $Info.f4.e  -font $FONT(GEN) -textvariable LATTICE_TYPE
     
       frame $Info.f5    
       label $Info.f5.c1 -width $ww1 -anchor w -justify left -text "z-range"
       label $Info.f5.e  -font $FONT(GEN) -textvariable ZRANGE_TYPE

       set itop 5
    } else {
       set itop 3
    }

    for {set i 0} {$i <=$itop} {incr i} {
        pack $Info.f$i.c1 -side left -anchor w
        if {$i<3} {pack $Info.f$i.c2 -side left -anchor w}
        pack $Info.f$i.e  -side left -anchor w
        pack $Info.f$i    -side top  -anchor nw
    }
    
#---------------------------------------------------------------------------------------

    pack  $Wcsys.struc.mode  $Wcsys.struc.b  -expand yes -fill both
    pack  $Wcsys.struc -in   $Wcsys          -expand yes -fill both



#site specification --------------------------------------------------------------------
    if {$NQ != 0 } {

    frame  $Wcsys.site -relief raised -borderwidth 2

    set wI   5
    set wR   12
    set wT   7
    set width_TYP 15

    set ws $Wcsys.site
   
#.....................................................................................
#                                                      DON'T  initialize
#.....................................................................................


set IQ 0
for {set ICL  1} {$ICL  <= $NCL}        {incr ICL } {
for {set IQCL 1} {$IQCL <= $NQCL($ICL)} {incr IQCL} {

if {$IQ == 0} {

  frame $ws.th ; pack  $ws.th

  button $ws.th.rasmol -text "show structure" -width 15 -height $HEIGHT(BUT1)  \
	 -bg $COLOR(BUT1) -command "sites_graphik" -state disabled
  button $ws.th.spheres -text "calculate R_WS + \nempty spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2)  -state disabled \
         -command "run_spheres ; structure_window_update_site_table"
  button $ws.th.scale_R_WS -text "adjust R_WS\nby scaling" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "scale_wigner-seitz_radii ; structure_window_update_site_table " 
  button $ws.th.set_same_R_WS -text "set same R_WS\nfor all spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "set_same_wigner-seitz_radii ; structure_window_update_site_table " 
  button $ws.th.suppress_symmetry -text "suppress\nsymmetry" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "suppress_symmetry ; structure_window_update_site_table " 
  pack   $ws.th.rasmol $ws.th.spheres  $ws.th.scale_R_WS $ws.th.set_same_R_WS \
	 $ws.th.suppress_symmetry   \
	 -expand true -fill x -side right

  eval {scrollbar $ws.sby -background {grey77} \
	 -command [list $ws.li yview] -orient vertical}
  eval {scrollbar $ws.sbx -background {grey77} \
	 -command [list $ws.li xview] -orient horizontal}

  eval {listbox $ws.li -background $COLOR(LIGHTBLUE) -height {15}  -font $FONT(GEN) \
          -yscrollcommand [list $ws.sby set] \
          -xscrollcommand [list $ws.sbx set] } " "

  bind $ws.li <Double-Button-1> {Bend}
#  bind $ws.li <Leave> {Bend}
#  bind $ws.li <Triple-Button-1> {Bend}

  pack $ws.sbx  -fill x -side bottom
  pack $ws.sby  -fill y -side left
  pack $ws.li   -expand true -fill both -side right

  set W_sites_list $ws.li
  
  set li_he_txt1 " IQ CL WS        X           Y           Z"
  set li_he_txt2 "          R_WS    NLQ NOQ  IT TXTT     CONC"
  $ws.li insert end "$li_he_txt1$li_he_txt2"

}

incr IQ

  set aux [format "%3i%3i  %1s%18.12f%18.12f%18.12f" \
           $IQ $ICL $WYCKOFFQ($IQ) $RQX($IQ) $RQY($IQ) $RQZ($IQ)]

  $ws.li insert end $aux

}
}

pack $Wcsys.site -in $Wcsys -expand yes -fill both
}
#site specification --------------------------------------------------------------- END

#......................................................................................
#                                                               insert type information
set IQ_display_start 1
set IQ_display_end   $NQ

#                                                            
#=======================================================================================
#                                  0D - SYSTEM                                
#=======================================================================================
if {$DIMENSION == "0D" } {

    give_warning "."  "WARNING \n\n structure_window_modify \n
     doesn't work for   0D   case  " 

#                                                            
#=======================================================================================
#                                  2D - SYSTEM                                
#=======================================================================================
} elseif {$DIMENSION == "2D" } {

debug $PRC  "ZRANGE_TYPE $ZRANGE_TYPE "
#
#----------------------------------------------------------- store initial configuration
#
   set NQ_0 $NQ
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set NOQ_0($IQ) $NOQ($IQ)
      set NLQ_0($IQ) $NLQ($IQ)
      set RWS_0($IQ) $RWS($IQ)
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set  IT              $ITOQ($IO,$IQ)
         set  ITOQ_0($IO,$IQ) $ITOQ($IO,$IQ)
         set     TXTT0_0($IT)    $TXTT0($IT)
         set        ZT_0($IT)       $ZT($IT)
         set      CONC_0($IT)     $CONC($IT)
      }
      set RQX_0($IQ) $RQX($IQ) 
      set RQY_0($IQ) $RQY($IQ) 
      set RQZ_0($IQ) $RQZ($IQ) 
   }
   set RBASX_0(3) $RBASX(3)
   set RBASY_0(3) $RBASY(3)
   set RBASZ_0(3) $RBASZ(3)

#.......................................................................................
#.............................................................................. extended
if { $ZRANGE_TYPE=="extended" } {

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      if {$IQ <= $NQ_bulk_L} {
         set WYCKOFFQ($IQ) "L"
      } elseif {$IQ > [expr $NQ - $NQ_bulk_R]} {
         set WYCKOFFQ($IQ) "R"
      } else {
         set WYCKOFFQ($IQ) "S"
      } 
   }
#
#---------------------------------------------------------------------- deal with spacer
#
   debug $PRC "################################### dealing with spacer"

   run_findsym structure_via_table_run_findsym

   set ICL1_S $NQ
   set ICL2_S 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      if { $IQ0>$NQ_bulk_L &&  $IQ0<=[expr $NQ - $NQ_bulk_R] } {
         set ICLQ_S($IQ0) $ICLQ($IQ)
         if {$ICL1_S>$ICLQ_S($IQ0)} {set ICL1_S $ICLQ_S($IQ0)}
         if {$ICL2_S<$ICLQ_S($IQ0)} {set ICL2_S $ICLQ_S($IQ0)}
      }
   }
#
#--------------------------------------------------------------------- deal with bulk(L)
#
   debug $PRC "################################### dealing with bulk(L)"

   set RBASX(3) $RBASX_L(3)
   set RBASY(3) $RBASY_L(3)
   set RBASZ(3) $RBASZ_L(3)

   set NQ $NQ_bulk_L

   run_findsym structure_via_table_run_findsym

   set ICL1_L $NQ
   set ICL2_L 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      set ICLQ_L($IQ0) $ICLQ($IQ)
      if {$ICL1_L>$ICLQ_L($IQ0)} {set ICL1_L $ICLQ_L($IQ0)}
      if {$ICL2_L<$ICLQ_L($IQ0)} {set ICL2_L $ICLQ_L($IQ0)}
   }
#
#--------------------------------------------------------------------- deal with bulk(R)
#
   debug $PRC "################################### dealing with bulk(R)"

   set RBASX(3) $RBASX_R(3)
   set RBASY(3) $RBASY_R(3)
   set RBASZ(3) $RBASZ_R(3)

   set NQ $NQ_bulk_R
   set NQ_LPS [expr $NQ_0 - $NQ_bulk_R]
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 [expr $NQ_LPS + $IQ] 
      set NOQ($IQ) $NOQ_0($IQ0)
      set NLQ($IQ) $NLQ_0($IQ0)
      set RWS($IQ) $RWS_0($IQ0)
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set  IT            $ITOQ_0($IO,$IQ0)
         set  ITOQ($IO,$IQ) $ITOQ_0($IO,$IQ0)
         set     TXTT0($IT)     $TXTT0_0($IT)
         set        ZT($IT)        $ZT_0($IT)
         set      CONC($IT)      $CONC_0($IT)
      }
      set RQX($IQ) $RQX_0($IQ0) 
      set RQY($IQ) $RQY_0($IQ0) 
      set RQZ($IQ) $RQZ_0($IQ0) 
   }

   run_findsym structure_via_table_run_findsym

   set ICL1_R $NQ
   set ICL2_R 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 [expr $NQ_LPS + $IQ0_Q($IQ)]
      set ICLQ_R($IQ0) $ICLQ($IQ)
      if {$ICL1_R>$ICLQ_R($IQ0)} {set ICL1_R $ICLQ_R($IQ0)}
      if {$ICL2_R<$ICLQ_R($IQ0)} {set ICL2_R $ICLQ_R($IQ0)}
   }
#
} else {
#.................................................................................. slab
#  scale RBAS(3) to avoid spurious translational symmetry due to 3D-mode of findsym

   set scale 1.2
   set RBASX(3) [expr $RBASX(3)*$scale]
   set RBASY(3) [expr $RBASY(3)*$scale]
   set RBASZ(3) [expr $RBASZ(3)*$scale]

   run_findsym structure_via_table_run_findsym

   set NQ_bulk_L 0
   set NQ_bulk_R 0
   set ICL2_L    0
   set ICL1_S    1

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      set ICLQ_S($IQ0) $ICLQ($IQ)
   }
}
#.......................................................................................
#.......................................................................................
#
   set NQ $NQ_0

   set RBASX(3) $RBASX_0(3)
   set RBASY(3) $RBASY_0(3)
   set RBASZ(3) $RBASZ_0(3)

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

      set NOQ($IQ) $NOQ_0($IQ)
      set NLQ($IQ) $NLQ_0($IQ)
      set RWS($IQ) $RWS_0($IQ)

      set RQX($IQ) $RQX_0($IQ) 
      set RQY($IQ) $RQY_0($IQ) 
      set RQZ($IQ) $RQZ_0($IQ) 

      set KDONEQ($IQ) 0
   }

   set NT 0
   set NCL 0
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      if {$KDONEQ($IQ) == 0} {
         set KDONEQ($IQ) 1
         incr NCL 
         set WYCKOFFCL($NCL) "-"
         if { $IQ <= $NQ_bulk_L } {
            set ICLQ($IQ) $ICLQ_L($IQ)
         } elseif { $IQ<=[expr $NQ - $NQ_bulk_R] } {
            set ICLQ($IQ) [expr $ICL2_L + $ICLQ_S($IQ) - $ICL1_S + 1]
         } else { 
            set ICLQ($IQ) [expr $ICL2_S + $ICLQ_R($IQ) - $ICL1_R + 1]
         }

         for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
            incr NT
            set IT0           $ITOQ_0($IO,$IQ)
            set ITOQ($IO,$IQ) $NT
            set TXTT0($NT)    $TXTT0_0($IT0)
            set    ZT($NT)       $ZT_0($IT0)
            set  CONC($NT)     $CONC_0($IT0)
            for {set JQ [expr $IQ+1] } {$JQ <= $NQ} {incr JQ} {
               if { $JQ <= $NQ_bulk_L } {
                  set ICLQ($JQ) $ICLQ_L($JQ)
               } elseif { $JQ<=[expr $NQ - $NQ_bulk_R] } {
                  set ICLQ($JQ) [expr $ICL2_L + $ICLQ_S($JQ) - $ICL1_S + 1]
               } else { 
                  set ICLQ($JQ) [expr $ICL2_S + $ICLQ_R($JQ) - $ICL1_R + 1]
               }

	       if {$ICLQ($IQ)==$ICLQ($JQ)} {
                  set KDONEQ($JQ) 1
                  for {set JO 1} {$JO <= $NOQ($IQ)} {incr JO} {
                     set ITOQ($JO,$JQ) $NT                  
                  }
               }
            }
         }
      }
   }


#                                                            
#=======================================================================================
#                                  3D - SYSTEM                                
#=======================================================================================
} elseif {$DIMENSION == "3D" } {

   debug $PRC "################################### dealing with 3D-system"

   run_findsym structure_via_table_run_findsym
}
#=======================================================================================



#=======================================================================================
#                             ALL DONE 
#=======================================================================================
   for {set ICL 1} {$ICL <= $NCL} {incr ICL} {set NQCL($ICL) 0}

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set ICL $ICLQ($IQ)
      incr NQCL($ICL)
      set IQECL($NQCL($ICL),$ICL) $IQ
   }
#
#--------------------------------------------------------- deal with names of atom types
#

deal_with_names_of_atom_types

#
#--------------------------------------------------------------------- update site table
#

structure_window_update_site_table

$Wcsys.c.goon             configure -state normal
$ws.th.rasmol             configure -state normal
$ws.th.spheres            configure -state normal
$ws.th.scale_R_WS         configure -state normal
$ws.th.set_same_R_WS      configure -state normal
$ws.th.suppress_symmetry  configure -state normal

}
#                                                            structure_window_modify END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
