########################################################################################
#
proc CreateTEXT {w BG name TEXT} {
#
#  create a standard TEXT window   
#
#
#  e.g.  CreateENTRY  $wc1 $BGE   $TEXT
#
#  w    parent window name       
#  BG   back ground color
#  ww   window name         - > $w.$name
#  TEXT text to be displayed
#
    global COLOR

    set ww $w.$name

    label $ww  -text "$TEXT" -bg $BG -justify left -highlightthickness 0
    pack configure $ww -in $w -anchor n -side top  -expand 1 -fill x  -pady 2
    
}

########################################################################################
#
proc CreateENTRY {w BG W1 T1 WE TVN TVI FT W2 T2} {
#
#  create a standard ENTRY window      the entry is assigned to variable  TVN(TVI)
#
#  [  "T1"  ] [########] [  "T2"  ] 
#  |---W1---| |---WE---| |---W2---| 
#
#  e.g.  CreateENTRY  $wc1 $BGE   $wl "   EMIN "     $wee VAR EMIN   $fe 3 "Ry"
#
#  w    parent window name                        widget name
#  BG   back ground color
#  W1   width of leading text window            $w.w${TVN}${TVI}.1
#  T1   leading text
#  WE   width of entry field                    $w.w${TVN}${TVI}.2
#  TVN  variable name
#  TVI  variable index   
#  FT   font used for the entry field
#  W2   width of trailing text window           $w.w${TVN}${TVI}.3
#  T2   trailing text
#
    global COLOR

    set ww $w.w${TVN}${TVI}
    frame $ww -bg $BG
    pack configure $ww -in $w -anchor w -side top -fill x
    label $ww.1 -width $W1 -text "$T1"   -anchor w  -padx 2 -bg $BG
    entry $ww.2 -width $WE -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
            -textvariable "${TVN}(${TVI})" -font $FT
    label $ww.3 -width $W2 -text "$T2"   -anchor w  -padx 2 -bg $BG
    pack configure $ww.1 $ww.2 $ww.3 -in $ww -anchor w -side left  -pady 2
    
}

########################################################################################
#
proc CreateSCALE {w BG W1 T1 WS S1 S2 STI SD SRES TVN TVI FT} {
#
#  create a standard ENTRY window      the entry is assigned to variable  TVN(TVI)
#
#  [  "T1"  ] [################] 
#  |---W1---| |-------WS-------| 
#
#  e.g.  CreateSCALE $wc2 $BGK   $wl "   RMAX  "  $ws 0.0 9.9  0 3 0.05 VAR STRRMAX $fe
#
#  w    parent window name                        widget name
#  BG   back ground color
#  W1   width of leading text window            $w.w${TVN}${TVI}.1
#  T1   leading text
#  WS   width of scale field                    $w.w${TVN}${TVI}.2
#  S1   start for scale range
#  S2   end   for scale range
#  STI  tick interval for scale range
#       0 ===> no ticks are shown
#  SD   digits for displayed value               
#  SRES resolution for displayed value               
#  TVN  variable name
#  TVI  variable index   
#  FT   font NOT used 
#
#  if an atom type is selected by the SCALE its name TXTT is
#  displayed on the right hand side of the scale in addition
#

    set ww $w.w${TVN}${TVI}
    
    frame $ww -bg $BG
    pack configure $ww -in $w -anchor w -side top -fill x
    label $ww.1 -width $W1 -text "$T1" -anchor nw  -padx 2 -bg $BG
    pack configure $ww.1 -in $ww -anchor nw -side left 
    
    if {$TVI=="ITXRAY"} {
	
	label $ww.3 -width 4 -anchor nw  -padx 4 -bg $BG 
	
	scale $ww.2 -length $WS -from $S1 -to $S2 -tickinterval $STI -digits $SD \
		-resolution $SRES -bg $BG -variable ${TVN}(${TVI}) \
		-orient horizontal -width 10 -showvalue true -highlightthickness 0 \
		-command "CreateSCALE_add_TXTT $ww.3"
	pack configure $ww.2 -in $ww -anchor w -side left 
	pack configure $ww.3 -in $ww -anchor nw -side left 
	
    } else {
	
	scale $ww.2 -length $WS -from $S1 -to $S2 -tickinterval $STI -digits $SD \
		-resolution $SRES -bg $BG -variable ${TVN}(${TVI}) \
		-orient horizontal -width 10 -showvalue true -highlightthickness 0 
	pack configure $ww.2 -in $ww -anchor w -side left 
	
    }
    
    #-----------------------------------------------------------------
    proc CreateSCALE_add_TXTT {w x} { 
        # add TXTT(ITXRAY) to SCALE and check core quantum numbers n and l

	global TXTT VAR
	
	set IT $VAR(ITXRAY)
	$w configure -text $TXTT($IT)
	exec_RADIOBUTTON_cmd NCXRAY
	
    }
    #-----------------------------------------------------------------

}

########################################################################################
# 
proc CreateRADIOBUTTON {w BG W1 T1 TVN TVI EXT FT MODE I1 args} {
#
# the args list is numbered by I starting at  I1
# MODE = n   the numerical value I is stored in TVN($TVI)
# MODE = s   the string no. I in args is stored in TVN($TVI)
#

    regsub "exec\$" $EXT "" EXT0
       
    set ww $w.w${TVN}${TVI}$EXT
    frame $ww -bg $BG
    pack configure $ww -in $w -anchor w -side top -fill x
    label $ww.l -width $W1 -text "$T1"   -anchor w  -padx 2 -bg $BG
    pack configure $ww.l -in $ww -anchor w -side left  -pady 2
    
    set I $I1
    foreach item $args {
	if {$MODE=="n"} {
	    set VAL $I
	} else {
	    set VAL $item
	}
	radiobutton $ww.$I -variable ${TVN}(${TVI}) -text $item -value $VAL \
		-bg $BG -highlightthickness 0
	if {$EXT0!=$EXT} {
	    $ww.$I configure -command "exec_RADIOBUTTON_cmd ${TVI}" 
	}
   
	pack configure $ww.$I -in $ww -anchor w -side left  -pady 2
	incr I
    }

}

 
########################################################################################
#
proc CreateCHECKBUTTON {w BG W1 T1 TVN TVI T2} {
#
#  create a standard CHECK  button     the setting is assigned to variable  TVN(TVI)
#
#  [  "T1"  ] [########] [  "T2"  ] 
#  |---W1---| |---WE---| |---W2---| 
#
#  e.g.     CreateCHECKBUTTON $wc3 tomato $wl "   POTFILE" VAR OWRPOT   "OVERWRITE"
#
#  w    parent window name                        widget name
#  BG   back ground color
#  W1   width of leading text window            $w.w${TVN}${TVI}.1
#  T1   leading text
#       button                                  $w.w${TVN}${TVI}.2  
#  TVN  variable name
#  TVI  variable index   
#  T2   trailing text
#

    set ww $w.w${TVN}${TVI}
    frame $ww -bg $BG
    pack configure $ww -in $w -anchor w -side top -fill x
    label $ww.1 -width $W1 -text "$T1"   -anchor w  -padx 2 -bg $BG
    checkbutton $ww.2 -activebackground white -anchor w \
	    -text $T2 -variable "${TVN}(${TVI})" -bg $BG -highlightthickness 0
    pack configure $ww.1 $ww.2 -in $ww -anchor w -side left  -pady 2
}

#
# replace fullpath to homedir with string \$HOME
# to be listed in fileselection boxes
#
# especially under Cygwin file normalisation is required
#
proc collapse_dollar_home {dir} {

    global env tcl_version
 
    set home $env(HOME) 

    if {$env(OSTYPE) == "Cygwin" && $tcl_version >= 8.4} { 
       set home [file normalize $home]
       set dir  [file normalize $dir]
    }    

    if {[string first $home $dir] == 0} {
	set hl [string length $home]
	set newdir "\$HOME[string range $dir $hl end]"
    } else {
	set newdir $dir
    }
    
    return $newdir
    
}

#
# expands $HOME string in filename: replace with $env(HOME)
#
proc expand_dollar_home {dir} {
 
    global env

    if {$dir=="\$HOME"} {
	set newdir $env(HOME)
    } elseif {[string first "HOME/" $dir]==1} {
	set ll [expr [string length $dir] - 1];
	set newdir "$env(HOME)[string range $dir 5 $ll]"
    } else {
	set newdir $dir
    }           
    
    return $newdir
    
}

#
# update a directory list
#
proc update_dirlist {dirlist dirlistmaxindex newdir} {            
    
    if {[lsearch $dirlist $newdir]<0} {
	
	set ll [llength $dirlist]
	if {$ll >= $dirlistmaxindex} {
	    set ll [expr {$dirlistmaxindex - 1}]
	} else {
	    set ll [expr {$ll - 1}]
	}
	
	set result [concat $newdir [lrange $dirlist 0 $ll]] 
    } else {
	set result $dirlist
    }
    
    return $result
    
}

########################################################################################
proc give_warning {w warning} {#                    give a warning on the screen

    if { $w == "." } {
	set cw .warning
    } else {  
	set cw $w.warning
    }
    if [winfo exists $cw] {destroy $cw}
    
    toplevel $cw 

#  wm title    $w "WARNING"
#  wm iconname $w "WARNING"

    wm geometry $cw +300+50 

    label $cw.head -text $warning -bg yellow -fg blue -height 20 	     
    button $cw.close -text Close  -command "destroy $cw"  -height 2 -width 50 -bg tomato1
    pack $cw.head $cw.close -side top -expand y -fill x -pady 2               
    
}





