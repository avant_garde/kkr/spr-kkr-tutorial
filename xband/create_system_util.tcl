########################################################################################
########################################################################################
#
#                         COMMON PROCEDURES FOR   create_system*
#
########################################################################################


#***************************************************************************************
#                            init_struc_parameter
#***************************************************************************************
#                                                                   
proc reset_struc_parameter {} {

    global brav
    global CONC flag SPACEGROUP SPACEGROUP_AP
    global BOA COA RCLU RCLV RCLW NQEQ ICLQ WYCKOFFQ
    global LATANG NOQ CONC RWS
    global flagset MAG_DIR TXTT TXTT0
    global Wcsys
    global NQ         ; set NQ         0
    global NCL        ; set NCL        0
    global NQCL       ; set NQCL(1)    ""
    global IQ1Q       ; set IQ1Q(1)    ""
    global IQ2Q       ; set IQ2Q(1)    ""
    global NT         ; set NT         ""
    global NCPA       ; set NCPA       ""
    global LATPAR     ; set LATPAR(1)  ""
    global ALAT       ; set ALAT       ""
    global BRAVAIS    ; set BRAVAIS    ""
    global SPACEGROUP ; set SPACEGROUP ""
    global BOHRRAD    ; set BOHRRAD 0.529177249
    global STRUCTURE_TYPE; set STRUCTURE_TYPE "UNKNOWN"
    global international ; set international  " "
    global RASRAD     ; set RASRAD 0
    global RASSCL     ; set RASSCL 0
    
    set tcl_precision 17
    
    set TXTT(1) ""
    for {set i 1} {$i <= 103} {incr i} {
	set RWS($i) ""
    }
    
    for {set i 0} {$i <= 5} {incr i} {
	set flagset($i) 0
    }
    
    for {set i 1} {$i <=3} {incr i} { set LATPAR($i) "" }
    for {set i 1} {$i <=3} {incr i} { set LATANG($i) "" }
    set BOA ""
    set COA ""
    
}
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                              table_bravais
#***************************************************************************************
#                      
proc table_bravais {} {
    global TABLATANG_alpha TABLATANG_beta TABLATANG_gamma
    global TABLATPAR_a TABLATPAR_b TABLATPAR_c TABBRAVAIS

    set tcl_precision 17

    set TABLATANG_alpha(0)   90; set TABLATANG_beta(0)   90; set TABLATANG_gamma(0)   90
    set TABLATPAR_a(0)        0; set TABLATPAR_b(0)       0; set TABLATPAR_c(0)        0

    set TABLATANG_alpha(1)    0; set TABLATANG_beta(1)    0; set TABLATANG_gamma(1)    0
    set TABLATPAR_a(1)        0; set TABLATPAR_b(1)       0; set TABLATPAR_c(1)        0
    			        		         			
    set TABLATANG_alpha(2)   90; set TABLATANG_beta(2)   90; set TABLATANG_gamma(2)    0
    set TABLATPAR_a(2)        0; set TABLATPAR_b(2)       0; set TABLATPAR_c(2)        0
    			     			         			
    set TABLATANG_alpha(3)   90; set TABLATANG_beta(3)   90; set TABLATANG_gamma(3)    0
    set TABLATPAR_a(3)        0; set TABLATPAR_b(3)       0; set TABLATPAR_c(3)        0
    			     			         			
    set TABLATANG_alpha(4)   90; set TABLATANG_beta(4)   90; set TABLATANG_gamma(4)   90
    set TABLATPAR_a(4)        0; set TABLATPAR_b(4)       0; set TABLATPAR_c(4)        0
    			     			         			
    set TABLATANG_alpha(5)   90; set TABLATANG_beta(5)   90; set TABLATANG_gamma(5)   90
    set TABLATPAR_a(5)        0; set TABLATPAR_b(5)       0; set TABLATPAR_c(5)        0
    			     			         			
    set TABLATANG_alpha(6)   90; set TABLATANG_beta(6)   90; set TABLATANG_gamma(6)   90
    set TABLATPAR_a(6)        0; set TABLATPAR_b(6)       0; set TABLATPAR_c(6)        0
    			     			         			
    set TABLATANG_alpha(7)   90; set TABLATANG_beta(7)   90; set TABLATANG_gamma(7)   90
    set TABLATPAR_a(7)        0; set TABLATPAR_b(7)       0; set TABLATPAR_c(7)        0
    			     			         			
    set TABLATANG_alpha(8)   90; set TABLATANG_beta(8)   90; set TABLATANG_gamma(8)   90
    set TABLATPAR_a(8)        0; set TABLATPAR_b(8)       1; set TABLATPAR_c(8)        0
    			     			         			
    set TABLATANG_alpha(9)   90; set TABLATANG_beta(9)   90; set TABLATANG_gamma(9)   90
    set TABLATPAR_a(9)        0; set TABLATPAR_b(9)       1; set TABLATPAR_c(9)        0

    set TABLATANG_alpha(10)   0; set TABLATANG_beta(10)   0; set TABLATANG_gamma(10)   0
    set TABLATPAR_a(10)       0; set TABLATPAR_b(10)      1; set TABLATPAR_c(10)       0

    set TABLATANG_alpha(11)  90; set TABLATANG_beta(11)  90; set TABLATANG_gamma(11) 120
    set TABLATPAR_a(11)       0; set TABLATPAR_b(11)      1; set TABLATPAR_c(11)       0

    set TABLATANG_alpha(12)  90; set TABLATANG_beta(12)  90; set TABLATANG_gamma(12)  90
    set TABLATPAR_a(12)       0; set TABLATPAR_b(12)      1; set TABLATPAR_c(12)       1

    set TABLATANG_alpha(13)  90; set TABLATANG_beta(13)  90; set TABLATANG_gamma(13)  90
    set TABLATPAR_a(13)       0; set TABLATPAR_b(13)      1; set TABLATPAR_c(13)       1

    set TABLATANG_alpha(14)  90; set TABLATANG_beta(14)  90; set TABLATANG_gamma(14)  90
    set TABLATPAR_a(14)       0; set TABLATPAR_b(14)      1; set TABLATPAR_c(14)       1

    set TABBRAVAIS(0)  "unknown                               "
    set TABBRAVAIS(1)  "triclinic   primitive      -1     C_i "
    set TABBRAVAIS(2)  "monoclinic  primitive      2/m    C_2h"
    set TABBRAVAIS(3)  "monoclinic  base centered  2/m    C_2h"
    set TABBRAVAIS(4)  "orthorombic primitive      mmm    D_2h"
    set TABBRAVAIS(5)  "orthorombic base-centered  mmm    D_2h"
    set TABBRAVAIS(6)  "orthorombic body-centered  mmm    D_2h"
    set TABBRAVAIS(7)  "orthorombic face-centered  mmm    D_2h"
    set TABBRAVAIS(8)  "tetragonal  primitive      4/mmm  D_4h"
    set TABBRAVAIS(9)  "tetragonal  body-centered  4/mmm  D_4h"
    set TABBRAVAIS(10) "trigonal    primitive      -3m    D_3d"
    set TABBRAVAIS(11) "hexagonal   primitive      6/mmm  D_6h"
    set TABBRAVAIS(12) "cubic       primitive      m3m    O_h "
    set TABBRAVAIS(13) "cubic       face-centered  m3m    O_h "
    set TABBRAVAIS(14) "cubic       body-centered  m3m    O_h "

}
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                              table_RWS
#***************************************************************************************
#                                                                 
#        return the Wigner-Seitz-radii RWS for atomic number  Z 
#        experimental values taken from H.L. Skrivers' book       
#                                                                 
# fill the global TABRWS array 
# 
proc table_RWS { } {

    global TABRWS xband_path
    
    set TABRWS(0)    0.000 ; set TABRWS(1)    0.000 ; set TABRWS(2)    0.000
    set TABRWS(3)    0.000 ; set TABRWS(4)    0.000 ; set TABRWS(5)    0.000
    set TABRWS(6)    0.000 ; set TABRWS(7)    0.000 ; set TABRWS(8)    0.000
    set TABRWS(9)    0.000 ; set TABRWS(10)   0.000 ; set TABRWS(11)   0.000
    set TABRWS(12)   0.000 ; set TABRWS(13)   0.000 ; set TABRWS(14)   0.000
    set TABRWS(15)   0.000 ; set TABRWS(16)   0.000 ; set TABRWS(17)   0.000
    set TABRWS(18)   0.000 ; set TABRWS(19)   4.862 ; set TABRWS(20)   4.122
    set TABRWS(21)   3.427 ; set TABRWS(22)   3.052 ; set TABRWS(23)   2.818
    set TABRWS(24)   2.684 ; set TABRWS(25)   2.699 ; set TABRWS(26)   2.662
    set TABRWS(27)   2.621 ; set TABRWS(28)   2.602 ; set TABRWS(29)   2.669
    set TABRWS(30)   0.000 ; set TABRWS(31)   0.000 ; set TABRWS(32)   0.000
    set TABRWS(33)   0.000 ; set TABRWS(34)   0.000 ; set TABRWS(35)   0.000
    set TABRWS(36)   0.000 ; set TABRWS(37)   5.197 ; set TABRWS(38)   4.494
    set TABRWS(39)   3.761 ; set TABRWS(40)   3.347 ; set TABRWS(41)   3.071
    set TABRWS(42)   2.922 ; set TABRWS(43)   2.840 ; set TABRWS(44)   2.791
    set TABRWS(45)   2.809 ; set TABRWS(46)   2.873 ; set TABRWS(47)   3.005
    set TABRWS(48)   0.000 ; set TABRWS(49)   0.000 ; set TABRWS(50)   0.000
    set TABRWS(51)   0.000 ; set TABRWS(52)   0.000 ; set TABRWS(53)   0.000
    set TABRWS(54)   0.000 ; set TABRWS(55)   5.656 ; set TABRWS(56)   4.652
    set TABRWS(57)   3.920 ; set TABRWS(58)   3.800 ; set TABRWS(59)   3.818
    set TABRWS(60)   3.804 ; set TABRWS(61)   3.783 ; set TABRWS(62)   3.768
    set TABRWS(63)   4.263 ; set TABRWS(64)   3.764 ; set TABRWS(65)   3.720
    set TABRWS(66)   3.704 ; set TABRWS(67)   3.687 ; set TABRWS(68)   3.668
    set TABRWS(69)   3.649 ; set TABRWS(70)   4.052 ; set TABRWS(71)   3.624
    set TABRWS(72)   3.301 ; set TABRWS(73)   3.069 ; set TABRWS(74)   2.945
    set TABRWS(75)   2.872 ; set TABRWS(76)   2.825 ; set TABRWS(77)   2.835
    set TABRWS(78)   2.897 ; set TABRWS(79)   3.002 ; set TABRWS(80)   0.000
    set TABRWS(81)   0.000 ; set TABRWS(82)   0.000 ; set TABRWS(83)   0.000
    set TABRWS(84)   0.000 ; set TABRWS(85)   0.000 ; set TABRWS(86)   0.000
    set TABRWS(87)   5.900 ; set TABRWS(88)   4.790 ; set TABRWS(89)   3.900
    set TABRWS(90)   3.756 ; set TABRWS(91)   3.430 ; set TABRWS(92)   3.221
    set TABRWS(93)   3.140 ; set TABRWS(94)   3.181 ; set TABRWS(95)   3.614
    set TABRWS(96)   3.641 ; set TABRWS(97)   3.550 ; set TABRWS(98)   0.000
    set TABRWS(99)   0.000 ; set TABRWS(100)  0.000 ; set TABRWS(101)  0.000
    set TABRWS(102)  0.000 ; set TABRWS(103)  3.500 ; set TABRWS(104)  0.000
    
    #------------------------------------------------------------------------
    set file $xband_path/locals/rws.vst

    if {[file readable $file]} {
	
	set fcol [open $file r]
	
	while {[gets $fcol line] > -1} {
	    set line [string trim $line ]
	    set key  [string range $line 0 0]
	    
	    if { $key != "#" } {
		set ind [lindex $line 0]
		set val [string trim [lindex $line 1]]
		if {$val!= ""} {set TABRWS($ind) $val}
	    }
	}
	
	writescr .d.tt "\nINFO from <table_RWS>   ------------------ \n"
	writescr .d.tt "Wigner-Seitz-radii read from  $file  \n\n"
	
    } else {
	give_warning "." "WARNING \n\n table of Wigner-Seitz-radii \n\n $file  \
                \n\n is not available \n\n DEFAULTS will be used "
    }
    #------------------------------------------------------------------------
    
    
}
#                                                                          table_RWS END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                                  table_CHSYM
#***************************************************************************************
#
proc table_CHSYM { } {
    
    global TABCHSYM
    
    set TABCHSYM(0)  "Vc"
    set TABCHSYM(1)  "H "  ; set TABCHSYM(2)   "He"
    set TABCHSYM(3)  "Li"  ; set TABCHSYM(4)   "Be"  ; set TABCHSYM(5)   "B\ " ; set TABCHSYM(6)   "C\ "
    set TABCHSYM(7)  "N\ " ; set TABCHSYM(8)   "O\ " ; set TABCHSYM(9)   "F\ " ; set TABCHSYM(10)  "Ne"
    set TABCHSYM(11) "Na"  ; set TABCHSYM(12)  "Mg"  ; set TABCHSYM(13)  "Al"  ; set TABCHSYM(14)  "Si"
    set TABCHSYM(15) "P\ " ; set TABCHSYM(16)  "S\ " ; set TABCHSYM(17)  "Cl"  ; set TABCHSYM(18)  "Ar"
    set TABCHSYM(19) "K\ " ; set TABCHSYM(20)  "Ca"  ; set TABCHSYM(21)  "Sc"  ; set TABCHSYM(22)  "Ti"
    set TABCHSYM(23) "V\ " ; set TABCHSYM(24)  "Cr"  ; set TABCHSYM(25)  "Mn"  ; set TABCHSYM(26)  "Fe"
    set TABCHSYM(27) "Co"  ; set TABCHSYM(28)  "Ni"  ; set TABCHSYM(29)  "Cu"  ; set TABCHSYM(30)  "Zn"
    set TABCHSYM(31) "Ga"  ; set TABCHSYM(32)  "Ge"  ; set TABCHSYM(33)  "As"  ; set TABCHSYM(34)  "Se"
    set TABCHSYM(35) "Br"  ; set TABCHSYM(36)  "Kr"
    set TABCHSYM(37) "Rb"  ; set TABCHSYM(38)  "Sr"  ; set TABCHSYM(39)  "Y\ " ; set TABCHSYM(40)  "Zr"
    set TABCHSYM(41) "Nb"  ; set TABCHSYM(42)  "Mo"  ; set TABCHSYM(43)  "Tc"  ; set TABCHSYM(44)  "Ru"
    set TABCHSYM(45) "Rh"  ; set TABCHSYM(46)  "Pd"  ; set TABCHSYM(47)  "Ag"  ; set TABCHSYM(48)  "Cd"
    set TABCHSYM(49) "In"  ; set TABCHSYM(50)  "Sn"  ; set TABCHSYM(51)  "Sb"  ; set TABCHSYM(52)  "Te"
    set TABCHSYM(53) "I"   ; set TABCHSYM(54)  "Xe"
    set TABCHSYM(55) "Cs"  ; set TABCHSYM(56)  "Ba"
    set TABCHSYM(57) "La"  ; set TABCHSYM(58)  "Ce"  ; set TABCHSYM(59)  "Pr"  ; set TABCHSYM(60)  "Nd"
    set TABCHSYM(61) "Pm"  ; set TABCHSYM(62)  "Sm"  ; set TABCHSYM(63)  "Eu"  ; set TABCHSYM(64)  "Gd"
    set TABCHSYM(65) "Tb"  ; set TABCHSYM(66)  "Dy"  ; set TABCHSYM(67)  "Ho"  ; set TABCHSYM(68)  "Er"
    set TABCHSYM(69) "Tm"  ; set TABCHSYM(70)  "Yb"  ; set TABCHSYM(71)  "Lu"
    set TABCHSYM(72) "Hf"  ; set TABCHSYM(73)  "Ta"  ; set TABCHSYM(74)  "W\ " ; set TABCHSYM(75)  "Re"
    set TABCHSYM(76) "Os"  ; set TABCHSYM(77)  "Ir"  ; set TABCHSYM(78)  "Pt"  ; set TABCHSYM(79)  "Au"
    set TABCHSYM(80) "Hg"  ; set TABCHSYM(81)  "Tl"  ; set TABCHSYM(82)  "Pb"  ; set TABCHSYM(83)  "Bi"
    set TABCHSYM(84) "Po"  ; set TABCHSYM(85)  "At"  ; set TABCHSYM(86)  "Rn"
    set TABCHSYM(87) "Fr"  ; set TABCHSYM(88)  "Ra"
    set TABCHSYM(89) "Ac"  ; set TABCHSYM(90)  "Th"  ; set TABCHSYM(91)  "Pa"  ; set TABCHSYM(92)  "U\ "
    set TABCHSYM(93) "Np"  ; set TABCHSYM(94)  "Pu"  ; set TABCHSYM(95)  "Am"  ; set TABCHSYM(96)  "Cm"
    set TABCHSYM(97) "Bk"  ; set TABCHSYM(98)  "Cf"  ; set TABCHSYM(99)  "Es"  ; set TABCHSYM(100) "Fm"
    set TABCHSYM(101) "Md" ; set TABCHSYM(102) "No"  ; set TABCHSYM(103) "Lr"

}

#***************************************************************************************
#                                  table_PS
#***************************************************************************************
#
# only TXTOCC($IOCC) and ZOCC($IOCC) is changed 
#
# $next_command ist called with $IOCC as parameter
#
#
proc table_PS {IOCC next_command} {
    
    global FONT TABCHSYM TXTOCC ZOCC
    
    set win .win_PS
    
    toplevel_init $win "Periodic Table" 0 0
    
    wm geometry     $win +300+200
    wm positionfrom $win user
    wm sizefrom     $win ""
    wm minsize      $win 100 100
    
    frame $win.table   -relief raised -borderwidth 2
    frame $win.table.1 -relief raised -borderwidth 2
    frame $win.table.2 -relief raised -borderwidth 2
    frame $win.table.ok
    set top $win
    button  $win.table.ok.ok -text "Cancel" -command "destroy $win"
    pack    $win.table.ok.ok
    for {set i 0} {$i <= 7} {incr i} {
	frame $win.table.1.$i
	pack  $win.table.1.$i -fill both
    }
    frame $win.table.2.1
    pack  $win.table.2.1
    frame $win.table.2.2
    pack  $win.table.2.2
    button $win.table.1.0.0 -text $TABCHSYM(0) -font $FONT(GEN) -width 3 \
	    -command [list table_PS_exit $IOCC 0 $next_command] 
    pack $win.table.1.0.0 -side left -anchor w -fill both
    for {set i 1} {$i <= 2} {incr i} {
	button $win.table.1.1.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command] 
    }
    pack $win.table.1.1.1 -side left -anchor w -fill both
    pack $win.table.1.1.2            -anchor e
    for {set i 3} {$i <= 10} {incr i} {
	button $win.table.1.2.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
    }
    pack $win.table.1.2.3  -side left -anchor w
    pack $win.table.1.2.4  -side left -anchor w
    for {set i 5} {$i <= 10} {incr i} {
        set j [expr {15 - $i}]
	pack $win.table.1.2.$j -side right -anchor e
    }
    for {set i 11} {$i <= 18} {incr i} {
	button $win.table.1.3.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
    }
    pack $win.table.1.3.11  -side left -anchor w
    pack $win.table.1.3.12  -side left -anchor w
    for {set i 13} {$i <= 18} {incr i} {
        set j [expr {31 - $i}]
	pack $win.table.1.3.$j -side right -anchor e
    }
    for {set i 19} {$i <= 36} {incr i} {
	button $win.table.1.4.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
	pack $win.table.1.4.$i -side left
    }
    for {set i 37} {$i <= 54} {incr i} {
	button $win.table.1.5.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
	pack $win.table.1.5.$i -side left
    }
    for {set i 55} {$i <= 56} {incr i} {
	button $win.table.1.6.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
	pack $win.table.1.6.$i -side left
    }
    label $win.table.1.6.label -text "La" -foreground blue -font $FONT(GEN) -width 3
    pack $win.table.1.6.label -side left
    for {set i 72} {$i <= 86} {incr i} {
	button $win.table.1.6.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command] 
    }
    for {set i 72} {$i <= 86} {incr i} {
	set j [expr {158 - $i}]      	
	pack $win.table.1.6.$j  -side right -anchor e
    }
    for {set i 87} {$i <= 88} {incr i} {
	button $win.table.1.7.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]  
	pack $win.table.1.7.$i -side left
    }
    label $win.table.1.7.label -text "Ac" -foreground blue -width 3 \
	    -font $FONT(GEN)
    pack $win.table.1.7.label  -side left
    button $win.table.2.1.57 -text $TABCHSYM(57) -font $FONT(GEN) -width 3 \
	    -foreground blue \
	    -command [list table_PS_exit $IOCC 57 $next_command]
    
    pack $win.table.2.1.57  -side left
    for {set i 58} {$i <= 71} {incr i} {
	button $win.table.2.1.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]  
	pack $win.table.2.1.$i -side left
    }
    button $win.table.2.2.89 -text $TABCHSYM(89) -font $FONT(GEN) -width 3 \
	    -foreground blue \
	    -command [list table_PS_exit $IOCC 89 $next_command]
    
    pack $win.table.2.2.89 -side left
    for {set i 90} {$i <= 103} {incr i} {
	button $win.table.2.2.$i -text $TABCHSYM($i) -font $FONT(GEN) -width 3 \
		-command [list table_PS_exit $IOCC $i $next_command]
	pack $win.table.2.2.$i -side left
    }
    
    pack $win.table.1 $win.table.2 $win.table.ok
    
    pack $win.table
    
    #-------------------------------------------------------
    proc table_PS_exit {IOCC i next_command} {

	global TABCHSYM TXTOCC ZOCC

	set TXTOCC($IOCC) $TABCHSYM($i)
	set ZOCC($IOCC)   $i

	#debug [myname] "IOCC: $IOCC TXTOCC: $TXTOCC($IOCC) ZOCC: $i"
	
	destroy .win_PS
	eval $next_command $IOCC
    }
    #-------------------------------------------------------
    
}
#                                                                           table_PS END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                             is_transition_metal
#***************************************************************************************
#                                                                    
proc is_transition_metal {Z} {
    
    #          Ti          Ni
    if { $Z >= 22 && $Z <= 28 } {
	return 1
    #
    #                Zr          Pd
    } elseif { $Z >= 40 && $Z <= 46 } {
	return 1
    #
    #                Hf          Pt
    } elseif { $Z >= 72 && $Z <= 78 } {
	return 1
    } else {
	return 0
    }
}


#***************************************************************************************
#                                  is_rare_earth
#***************************************************************************************
#                                                                          
proc is_rare_earth {Z} {

    #          Ce          Lu
    if { $Z >= 58 && $Z <= 71 } {
	return 1
    } else {
	return 0
    }
}
#***************************************************************************************
#                           is_actinide
#***************************************************************************************
#                                                                            
proc is_actinide {Z} {
    
    #          Ac
    if { $Z >= 89 } {
	return 1
    } else {
	return 0
    }
}
#***************************************************************************************
#                                   is_f_element  
#***************************************************************************************
#                                                                           
proc is_f_element {Z} {
    
    #          Ce          Lu
    if { $Z >= 58 && $Z <= 71 } {
	return 1
    #                Ac
    } elseif { $Z >= 89 } {
	return 1
    } else {
	return 0
    }
}

#***************************************************************************************
#                               run_spheres      
#***************************************************************************************
#
proc run_spheres { } {

global Wcsys Wspheres
global WIDTH HEIGHT COLOR FONT
global VAR BGCLU DIMENSION
global sysfile syssuffix
global symline nsymline

set PRC "run_spheres"

if {$DIMENSION=="2D"} {
    give_warning "." "WARNING \n\n the program spheres \n \
                      \n dosn't work yet for 2D case \n\n try plain scaling  "
    return
}

set BGCLU  LightSkyBlue
set fe $FONT(GEN)
set wl 25
set ws 200

#
#-------------------------------------------- write a temporary system file  "xband_tmp"
#
set sysfile_sav $sysfile
set sysfile     "xband_tmp"

write_sysfile

set sysfile $sysfile_sav

#---------------------------------------------------------------------------------------

set strdat [open "struc.out" r]

set n 0
set found 0
while {[gets $strdat line] > -1 && $found==0} {

   set i [string first "Point symmetry operations for reciprocal sums" $line]

   if {$i>0} {
      set found 1
      set symline($n) $line
      for {set l 1} {$l <= 4} {incr l} {
         gets $strdat line
         set j [string first ";" $line]
         if {$j<0} {
            incr n
            set symline($n) $line
         }
      }
   }
}
set nsymline $n

#---------------------------------------- check
set ierr 0
if {$found=="0" } {incr ierr}
if {$n!="2" && $n!="4"} {incr ierr}
if {$ierr !="0"} {
  give_warning "." \
        "WARNING \n\n reading symmetry information from file  struc.out  failed \
         \n\n $found = $found \n\n n(symlines) = $n  "
  return
}
#---------------------------------------- check

set Wspheres    $Wcsys.spheres
toplevel_init   $Wspheres "Wigner-Seitz radii and empty spheres" 0 0
wm geometry     $Wspheres +300+200
wm positionfrom $Wspheres user
wm sizefrom     $Wspheres ""
wm minsize      $Wspheres


label $Wspheres.label -text "the Wigner-Seitz radii will be adjusted \n
      according to a guess for the charge density" -bg $COLOR(LIGHTBLUE)  \
      -font $FONT(GEN) -pady 15 -padx 10

pack  $Wspheres.label -expand yes -fill both

set VAR(add-ES) YES
set VAR(r_min-ES) 1.2
set VAR(r_max-ES) 2.5

CreateRADIOBUTTON $Wspheres $BGCLU 30  "   add empty spheres"    VAR add-ES  a $fe s 1 YES NO
CreateSCALE       $Wspheres $BGCLU $wl "   r_min (ES)"  $ws 1.0 2.5 0 3 0.1  VAR    r_min-ES  $fe
CreateSCALE       $Wspheres $BGCLU $wl "   r_max (ES)"  $ws 1.0 4.0 0 3 0.1  VAR    r_max-ES  $fe

label $Wspheres.patience  -text " " -fg black -relief flat -pady 5
pack  $Wspheres.patience -expand yes -fill both

frame $Wspheres.c
pack  $Wspheres.c

#---------------------------------------------------------------------------
  button  $Wspheres.c.button2 -text "CLOSE" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
         -command "destros $Wspheres "

  button $Wspheres.c.button3 -text "OK -- RUN" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
         -command "run_spheres_execute "

    pack $Wspheres.c.button2 $Wspheres.c.button3 \
  	 -in $Wspheres.c  -side left -fill x -expand yes
#---------------------------------------------------------------------------

}
#                                                                        run_spheres END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                             run_spheres_execute        
#***************************************************************************************
#
proc run_spheres_execute { } {

    global VAR symline nsymline SPHERES  Wspheres W_sites_list
    global sysfile syssuffix
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
    global system NQ NT ZT RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT TXTT0
    global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global NM IMT IMQ RWSM NAT QMTET QMPHI
    global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ
    global list_ALPHA    
    
    set PRC run_spheres_execute

    set ws $Wcsys.site


#
#-------------------------------------------------- create spheres.inp
#
    set sphinp [open "spheres.inp" w]
    if {$VAR(add-ES)== "YES" } {
	puts $sphinp        "0        /   add empty spheres"
    } else {
	puts $sphinp        "1        /   do NOT add empty spheres"
    }
    puts  $sphinp "$VAR(r_min-ES)     /   r_min(empty spheres) "
    puts  $sphinp "$VAR(r_max-ES)     /   r_max(empty spheres) "
    close $sphinp
    
#
#--------------------------- append temporary system file   xband_tmp
#
    eval set res [catch "exec cat xband_tmp$syssuffix >> spheres.inp ; \
	                 rm xband_tmp$syssuffix  " message]

    writescr0  .d.tt "info from $PRC \n $message \n"

#
#--------------------------- append symmetry information
#
    set sphinp [open "spheres.inp" a]
    for {set i 0} {$i <= $nsymline} {incr i} {
	regsub -all -- "-" $symline($i) " -" line
	puts $sphinp $line
    }
    close $sphinp

    eval set res [catch "exec cat spheres.inp  " message]
    writescr  .d.tt "\n$message \n\n "

#
#--------------------------- run spheres program
#
    patience $Wspheres 1
    writescr  .d.tt "info from $PRC \n running $SPHERES:\n"
    eval set res [catch "exec $SPHERES < spheres.inp > spheres.out  " message]
    writescr  .d.tt "$message \n"
    eval set res [catch "exec cat radii.out " message]
    writescr  .d.tt "\n$message \n\n "
    patience $Wspheres 0
#
#--------------------------- get information from file radii.out
#
    set frad [open radii.out r]

    gets $frad NQ_new
    set NL_min 20
    
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	gets $frad line
	set ICL      [lindex $line 0]
	set RWS($IQ) [lindex $line 4]
	if {$NLQ($IQ)<$NL_min} {set NL_min $NLQ($IQ)}
    }
    
    set NQ0  $NQ
    set NT0  $NT
    set NCL0 $NCL
    
#---------------------------------------- check
    if {$ICL != $NCL} {
	give_warning "." "WARNING \n\n classes not consictent in radii.out \
                \n\n ICL = $ICL  <>  NCL = $NCL \n\n xband will not continue "
	return
    }
#---------------------------------------- check

    set ICL0 $NCL
    set IT   $NT

#======================================================================= IQ-loop
    for {set IQ [expr $NQ+1]} {$IQ <= $NQ_new} {incr IQ} {

	gets $frad line
	set ICL      [lindex $line 0]
	set RQX($IQ) [lindex $line 1]
	set RQY($IQ) [lindex $line 2]
	set RQZ($IQ) [lindex $line 3]
	set RWS($IQ) [lindex $line 4]
	set NLQ($IQ) $NL_min
	set WYCKOFFQ($IQ) "-"
	set WYCKOFFCL($ICL) "-"
	set ICLQ($IQ) $ICL
	
	set aux [format "%3i%3i  %1s%12.6f%12.6f%12.6f" \
		$IQ $ICL $WYCKOFFQ($IQ) $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
	$ws.li insert end $aux
	
	set NOQ($IQ) 1
	if {$ICL != $ICL0} {
	    incr IT
	    incr NCL
	    set NQCL($ICL) 0
	}
	incr NQCL($ICL)
	set ZT($IT)      0
	set CONC($IT)   1.0
	set TXTT0($IT) "Vc"
	set ITOQ(1,$IQ) $IT
	
	set ICL0 $ICL
	
    }
#======================================================================= end IQ-loop

#
#--------------------- get cryst coordinates RQU RQV RQW
#

    convert_basis_vectors_RQ cart_to_cryst

#
#--------------------- shift lattice sites into unit cell
#

    shift_lattice_sites

    set NQ $NQ_new	
    set NT $IT

#
#---------------------------------------- set up table of equivalent sites
#
    set IQ 0
    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
	set IQ1 [expr $IQ + 1]
	set IQ2 [expr $IQ + $NQCL($ICL)]
	for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
	    incr IQ
	    set IQ1Q($IQ) $IQ1
	    set IQ2Q($IQ) $IQ2
	    set IQECL($IE,$ICL) $IQ
	    debug $PRC "ICL $ICL  NQCL $NQCL($ICL)  IQECL=IQ $IQECL($IE,$ICL) $IQ  $IQ1Q($IQ) ...  $IQ2Q($IQ) "
	}
    }

    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	set ICL $ICLQ($IQ)
	set NQEQ($IQ) 0
	for {set JQ 1} {$JQ <= $NQ} {incr JQ} {
	    set JCL $ICLQ($JQ)
	    if { $ICL == $JCL } { incr NQEQ($IQ) }
	}
    }
    
    deal_with_names_of_atom_types

    structure_window_update_site_table

    destros $Wspheres
}
#                                                                run_spheres_execute END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                                deal_with_names_of_atom_types     
#***************************************************************************************
#                   
# store name of element from TXTT0 in TXTT and add "underscore" + "letter" if 
# the same element exists more than once 
#                     
proc deal_with_names_of_atom_types { } {
# HE 08/07/25 change from OLD to NEW system  Fe_a, Fe_b ---> Fe_1, Fe_2    
    
    global NT TXTT0 TXTT list_ALPHA
    
    set PRC "deal_with_names_of_atom_types"
    
    set FLAG 0
    for {set IT 1} {$IT <= $NT} {incr IT} {
	set KDONE($IT)  0
	set TXTT($IT) [string trim $TXTT0($IT)]
    }
    
    for {set IT 1} {$IT <= $NT} {incr IT} {
#OLD	set IEXT 0
	set IEXT 1
	if { $KDONE($IT) != 1 } {
	    set TXT  $TXTT($IT)
	    for {set JT [expr $IT + 1]} {$JT <= $NT} {incr JT} {
		if { $TXT == $TXTT($JT) } {
		    set FLAG 1
		    incr IEXT
#OLD		    set TXTT($IT) "${TXT}_a"
		    set TXTT($IT) "${TXT}_1"
		    if { $IEXT <= 520000 }  {
#OLD			set TXTT($JT) "${TXT}_[lindex $list_ALPHA $IEXT]"
			set TXTT($JT) "${TXT}_$IEXT"
		    } else {
			set TXTT($JT) "${TXT}_>"
		    }
		    set KDONE($JT)  1
		}
	    }
	}
	set KDONE($IT)  1
    }
    
}
#                                                     deal_with_names_of_atom_types  END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                                    tkerror  
#***************************************************************************************
#
proc tkerror err {
    global errorInfo
    set info $errorInfo
    if {[tk_dialog .tkerrorDialog "Input Error" \
            "Error: $err" error 0 OK "HALLO"] == 0} {
        return
    }

    set w .tkerrorTrace
    catch {destroy $w}
    toplevel $w -class ErrorTrace
    wm minsize $w 10 10
    wm title $w "Stack Trace for Error"
    wm iconname $w "Stack Trace"
    button $w.ok -text OK -command "destroy $w"
    text $w.text -relief raised -bd 2 -yscrollcommand "$w.scroll set" \
            -setgrid true -width 40 -height 10
    scrollbar $w.scroll -relief flat -command "$w.text yview"
    pack $w.ok -side bottom -padx 3m -pady 3m -ipadx 2m -ipady 1m
    pack $w.scroll -side right -fill y
    pack $w.text -side left -expand yes -fill both
    $w.text insert 0.0 $info
    $w.text mark set insert 0.0

    # Center the window on the screen.

    wm withdraw $w
    update idletasks
    set x [expr [winfo screenwidth $w]/2 - [winfo reqwidth $w]/2 \
            - [winfo vrootx [winfo parent $w]]]
    set y [expr [winfo screenheight $w]/2 - [winfo reqheight $w]/2 \
            - [winfo vrooty [winfo parent $w]]]
    wm geom $w +$x+$y
    wm deiconify $w
}
#                                                                            tkerror END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                               coord_str_to_num      
#***************************************************************************************
#
proc coord_str_to_num {str_in} {
#
#            extract a number from the string   str_in
#
    set tcl_precision 17

    set str " [string trim $str_in]  "

    set itop [ expr [string length $str] - 1 ]

    set numeric {[0-9]}
    set dot     {.}

    set fnum "0"
    set fdot "0"
    set num ""

    for {set i 0} {$i < $itop} {incr i} {

        set c [string range $str $i  $i ]

        if { [string match $numeric $c]=="1" } {
           append num $c
           set fnum "1"
        } else {
           if { [string match $dot $c]=="1" } {
              set fdot "1"
              set fnum "1"
           } else {
	       if { "$fnum$fdot" == "10" } { append num ".0" }
               set fnum "0"
               set fdot "0"
           }
           append num $c
       }
   }
   
   expr 0.0 + $num
}
#                                                                   coord_str_to_num END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                                  structure_window   
#***************************************************************************************
#
proc structure_window {} {

global CHECK_TABLE
global structure_window_calls sysfile syssuffix
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint Wcsys
global system NQ NCL NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS ALAT IQ1Q IQ2Q
global W_sites_list NT WYCKOFFQ WYCKOFFCL
global editor edback edxterm edoptions xband_path
global SPACEGROUPS_FILE STRUCTURE_FILE STRUCTURE_SETUP_MODE IQECL
global create_structure_button_list create_structure_lock
global VISUAL sym_lowered DIMENSION
global COLOR WIDTH HEIGHT FONT
global optimize_basis_result

set PRC "structure_window"

if {$structure_window_calls > 0} {destroy $Wcsys}

incr structure_window_calls

set win $Wcsys
if {[winfo exists $win] == 1} {destroy $win}
toplevel $win -width 400 -height 300 -visual $VISUAL
wm geometry     $win +300+200
wm positionfrom $win user

#

set DIMENSION "3D"

    wm title        $win "CREATE 3D-SYSTEM"

    insert_topbuttons $Wcsys compile_h.hlp

    $Wcsys.a.l configure -state disabled

    bind $Wcsys.a.e <Button-1> {
#  if [compile_focus ""] {
#    writescr $Wcsys.d.tt "input accepted; press once more \"close\""
#  } else {
    destros $Wcsys; unlock_list ; Bend
#  }
}

    bind $Wcsys.a.h <Button-1> {
        execunixcmd "cat $xband_path/help/create_system_h.hlp"
    }

#---------------------------------------------------------------------------
#                            section  "c"
#---------------------------------------------------------------------------
  frame $Wcsys.c -borderwidth {2} -height {30}  -relief {raised}
  pack $Wcsys.c -in $Wcsys -expand yes -fill x

#---------------------------------------------------------------------------
  frame $Wcsys.c.mag -borderwidth {2} -height {30}

  label $Wcsys.c.mag.label1 -text {magnetisation (cart. coord.)}

  frame $Wcsys.c.mag.f2  -borderwidth {2}  -height {30}  -width {30}

  label $Wcsys.c.mag.f2.labM -text {M = }

  entry $Wcsys.c.mag.f2.e_Mx \
    -textvariable {MAG_DIR(1)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  entry $Wcsys.c.mag.f2.e_My \
    -textvariable {MAG_DIR(2)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  entry $Wcsys.c.mag.f2.e_Mz \
    -textvariable {MAG_DIR(3)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  pack configure $Wcsys.c.mag.label1  -expand true -fill x
  pack configure $Wcsys.c.mag.f2

  pack configure $Wcsys.c.mag.f2.labM  -expand true -fill x  -side left
  pack configure $Wcsys.c.mag.f2.e_Mx  -side left
  pack configure $Wcsys.c.mag.f2.e_My  -side left
  pack configure $Wcsys.c.mag.f2.e_Mz  -side left
#---------------------------------------------------------------------------

  if {"$sysfile$syssuffix" == ".sys"} {
     set sysfile "system"
  }

  frame $Wcsys.c.name -borderwidth {2} -height {30}

  label $Wcsys.c.name.label1 -text {name of system-file}

  entry $Wcsys.c.name.entry \
    -textvariable {sysfile} -font $FONT(GEN) -width {30} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  pack configure $Wcsys.c.name.label1  -expand true -fill x

  pack configure $Wcsys.c.name.entry
#---------------------------------------------------------------------------
  button $Wcsys.c.button0D -text "create 0D-system" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN)  \
         -command "destros $Wcsys; unlock_list ; create_system_0D  "

  button $Wcsys.c.button2D -text "create 2D-system" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN)  \
         -command "destros $Wcsys; unlock_list ; create_system_2D "

  button $Wcsys.c.button2 -text "CLOSE" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
         -command "destros $Wcsys; unlock_list "

  button $Wcsys.c.goon -text "DONE - RETURN" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
         -command "structure_window-done-RETURN" -state disabled
#

#---------------------------------------------------------------------------
    set create_structure_button_list [list $Wcsys.struc.mode.button1 \
                   $Wcsys.struc.mode.button2 $Wcsys.struc.mode.button3 \
                   $Wcsys.struc.mode.button4 ]

#   pack $Wcsys.c.mag $Wcsys.c.name $Wcsys.c.button2 $Wcsys.c.goon \
#   suppress magnetic stuff
    pack $Wcsys.c.name $Wcsys.c.button0D $Wcsys.c.button2D  \
	               $Wcsys.c.button2  $Wcsys.c.goon \
  	 -in $Wcsys.c  -side left -fill x -expand yes

    # structure information
    frame $Wcsys.struc -relief raised -borderwidth 2

    frame $Wcsys.struc.mode

    label $Wcsys.struc.mode.title -text "Specify structure via:  "
    button $Wcsys.struc.mode.button1 -text "Structure type" \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
	   -command "set create_structure_lock 1;  \
	             set STRUCTURE_SETUP_MODE \"STRUCTURE TABLE\" ;\
                     structure_via_table "

    button $Wcsys.struc.mode.button2 -text "Space group" \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
	   -command "set create_structure_lock 1;  \
	             set STRUCTURE_SETUP_MODE \"SPACE GROUP\" ;\
                     structure_via_table "

    button $Wcsys.struc.mode.button3 -text "Per pedes"  \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE) \
	   -command "set create_structure_lock 1;  \
	             set STRUCTURE_SETUP_MODE \"PER PEDES\" ;\
                     structure_per_pedes "

    button $Wcsys.struc.mode.button4 -text "multi layers" \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE) \
	   -command "set create_structure_lock 1;  \
	             set STRUCTURE_SETUP_MODE \"MULTI LAYERS\" ;\
                     structure_multilayer"

    button $Wcsys.struc.mode.unlock -text "unlock" \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE) \
	   -command "set create_structure_lock 0; structure_unlock"

    proc structure_unlock {} {
    global create_structure_button_list
       if {[llength $create_structure_button_list]>0} {
          foreach x $create_structure_button_list {$x configure -state normal}
       }
    }

    button $Wcsys.struc.mode.edit -text "edit \n database"  \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN) \
	   -command "exec $edxterm $editor $edoptions  \
                          $xband_path/locals/$SPACEGROUPS_FILE $edback ;   \
                     exec $edxterm $editor $edoptions  \
                          $xband_path/locals/$STRUCTURE_FILE $edback "

    pack $Wcsys.struc.mode.title $Wcsys.struc.mode.button1\
	    $Wcsys.struc.mode.button2 $Wcsys.struc.mode.button3 \
	    $Wcsys.struc.mode.button4 $Wcsys.struc.mode.unlock $Wcsys.struc.mode.edit  \
	    -side left -expand yes

   if {$create_structure_lock==1} {
       if {[llength $create_structure_button_list]>0} {
      	  foreach x $create_structure_button_list {$x configure -state disabled}
       }
    }

#---------------------------------------------------------------------------------------
    frame $Wcsys.struc.b

    set Latpar $Wcsys.struc.b.latpar
    frame $Latpar

    set widlab  5
    set wident 15

    frame $Latpar.info
    label $Latpar.info.a -text " lattice parameters          A = " -pady 5
    label $Latpar.info.b -width $wident -font $FONT(GEN) \
	    -textvariable ALAT
    label $Latpar.info.c -text "\[a.u.\]"
    pack $Latpar.info.a $Latpar.info.b $Latpar.info.c -side left

    frame $Latpar.first
    frame $Latpar.secon
    frame $Latpar.third

    label $Latpar.first.label1 -width $widlab -text "a = " -pady 5  -justify right
    label $Latpar.first.label2 -width $widlab -text "b = " -pady 5  -justify right
    label $Latpar.first.label3 -width $widlab -text "c = " -pady 5  -justify right

    label $Latpar.secon.label1 -width $widlab -text "      " -pady 5 -justify right
    label $Latpar.secon.label2 -width $widlab -text "b/a = " -pady 5 -justify right
    label $Latpar.secon.label3 -width $widlab -text "c/a = " -pady 5 -justify right
				
    label $Latpar.third.label1 -width $widlab -font $FONT(SBL) \
	   -text "a = " -pady 5 -justify right
    label $Latpar.third.label2 -width $widlab -font $FONT(SBL) \
	   -text "b = " -pady 5 -justify right
    label $Latpar.third.label3 -width $widlab -font $FONT(SBL) \
	   -text "g = " -pady 5 -justify right

    label $Latpar.secon.a(1) -width $wident -font $FONT(GEN)
    label $Latpar.secon.a(2) -width $wident -font $FONT(GEN) \
	    -textvariable BOA
    label $Latpar.secon.a(3) -width $wident -font $FONT(GEN)  \
	    -textvariable COA

    for {set i 1} {$i <=3} {incr i} {
	label $Latpar.first.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATPAR($i) -justify left -highlightthickness 0
	label $Latpar.third.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATANG($i) -justify left -highlightthickness 0
	
        pack $Latpar.first.label$i  $Latpar.first.a($i) -side left
        pack $Latpar.secon.label$i  $Latpar.secon.a($i) -side left
        pack $Latpar.third.label$i  $Latpar.third.a($i) -side left
    }


    pack $Latpar -side left
    pack $Latpar.info $Latpar.first  $Latpar.secon  $Latpar.third  -fill both \
	   -expand yes -anchor c


    set Info $Wcsys.struc.b.info

    frame $Info
    pack $Info -side left

    set ww1 20
    set ww2 5
    set ww3 5

    frame $Info.f0
    label $Info.f0.c1 -width $ww1 -anchor w -justify left -text "number of sites"
    label $Info.f0.c2 -width $ww2 -anchor w -justify left -text "NQ"
    label $Info.f0.e  -width $ww3 -font $FONT(GEN) -textvariable NQ
		
    frame $Info.f1
    label $Info.f1.c1 -width $ww1 -anchor w -justify left -text "inequivalent sites"
    label $Info.f1.c2 -width $ww2 -anchor w -justify left -text "NCL"
    label $Info.f1.e  -width $ww3 -font $FONT(GEN) -textvariable NCL

    frame $Info.f2
    label $Info.f2.c1 -width $ww1 -anchor w -justify left -text "atom types"
    label $Info.f2.c2 -width $ww2 -anchor w -justify left -text "NT"
    label $Info.f2.e  -width $ww3 -font $FONT(GEN) -textvariable NT

    frame $Info.f3
    label $Info.f3.c1 -width $ww1 -anchor w -justify left -text "space group"
    label $Info.f3.c2 -width $ww2 -anchor w -justify left -text ""
    label $Info.f3.e  -width $ww3 -font $FONT(GEN) -textvariable SPACEGROUP

    frame $Info.f4
    label $Info.f4.c1 -width 45 -anchor w -justify left  \
          -textvariable TABBRAVAIS($BRAVAIS)
    label $Info.f4.c2 -width 1  -anchor w -justify left -text ""
    label $Info.f4.e  -width 1  -font $FONT(GEN)

    for {set i 0} {$i <=4} {incr i} {
        pack $Info.f$i.c1 -side left -anchor w
        pack $Info.f$i.c2 -side left -anchor w
        pack $Info.f$i.e  -side left -anchor w
        pack $Info.f$i    -side top  -anchor nw
    }

#---------------------------------------------------------------------------------------

    pack  $Wcsys.struc.mode  $Wcsys.struc.b  -expand yes -fill both
    pack  $Wcsys.struc -in   $Wcsys          -expand yes -fill both



#site specification --------------------------------------------------------------------
    if {$NQ != 0 } {

    frame  $Wcsys.site -relief raised -borderwidth 2

    set wI   5
    set wR   12
    set wT   7
    set width_TYP 15

    set ws $Wcsys.site

#.....................................................................................
#                                                                           initialize
    if {$STRUCTURE_SETUP_MODE !="PER PEDES"      && \
        $STRUCTURE_SETUP_MODE != "MULTI LAYERS"  && \
        $STRUCTURE_SETUP_MODE != "2D LAYERS"     && \
        $STRUCTURE_SETUP_MODE != "3D SURFACE" } {

    set NT  0
    set NAT(1) 0
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
       set NOQ($IQ) 0
       set ITOQ(1,$IQ) 0
    }

    set IQ 0
    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
       set IQ1 [expr $IQ + 1]
       set IQ2 [expr $IQ + $NQCL($ICL)]
       for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
          incr IQ
          set IQ1Q($IQ) $IQ1
          set IQ2Q($IQ) $IQ2
          set IQECL($IE,$ICL) $IQ
#         debug $PRC  " IQ $IQ   $IQ1Q($IQ) ...  $IQ2Q($IQ)  SETUP_MODE $STRUCTURE_SETUP_MODE XXX "
       }
    }

    }
#.....................................................................................

set IQ 0
for {set ICL  1} {$ICL  <= $NCL}        {incr ICL } {
for {set IQCL 1} {$IQCL <= $NQCL($ICL)} {incr IQCL} {

if {$IQ == 0} {

  frame $ws.th ; pack  $ws.th

  frame $ws.th.left 
  pack  $ws.th.left -expand true -fill x -side left
  
  run_geometry optimisation_of_basis_vectors_3D

  if {![info exists optimize_basis_result]} {
    set optimize_basis_result "no onformation available"
  }

  label $ws.th.left.optimisation -width 45 -anchor w  -justify left  \
	-text "optimisation of basis vectors (3D): \n $optimize_basis_result"
  pack  $ws.th.left.optimisation  -expand true -fill x -side top

  label $ws.th.left.specify -width 45 -anchor w  -justify left  \
	-text "click line below to specify occupation"
  pack  $ws.th.left.specify  -expand true -fill x -side bottom


  button $ws.th.rasmol -text "show structure" -width 15 -height $HEIGHT(BUT1)  \
	 -bg $COLOR(BUT1) -command "sites_graphik" -state disabled
  button $ws.th.spheres -text "calculate R_WS + \nempty spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -command "run_spheres" -state disabled
  button $ws.th.set_same_R_WS -text "set same R_WS\nfor all spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "set_same_wigner-seitz_radii ; structure_window_update_site_table " 
  button $ws.th.suppress_symmetry -text "suppress\nsymmetry" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "suppress_symmetry ; structure_window_update_site_table " 
  button $ws.th.scale_R_WS -text "adjust R_WS\nby scaling" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "scale_wigner-seitz_radii ; structure_window_update_site_table " 
  pack   $ws.th.rasmol $ws.th.spheres  $ws.th.scale_R_WS $ws.th.set_same_R_WS  \
	 $ws.th.suppress_symmetry  \
	 -expand true -fill x -side right

  eval {scrollbar $ws.sby -background {grey77} \
	  -command [list $ws.li yview] -orient vertical}
  eval {scrollbar $ws.sbx -background {grey77}  \
	 -command [list $ws.li xview] -orient horizontal}

  eval {listbox $ws.li -background $COLOR(LIGHTBLUE) -height {15}  -font $FONT(GEN) \
          -yscrollcommand [list $ws.sby set] \
          -xscrollcommand [list $ws.sbx set] } " "
  bind $ws.li <Button-1> {if {$STRUCTURE_SETUP_MODE !="PER PEDES"     && \
                              $STRUCTURE_SETUP_MODE != "MULTI LAYERS" && \
                              $STRUCTURE_SETUP_MODE != "2D LAYERS"    && \
                              $STRUCTURE_SETUP_MODE != "3D SURFACE" } {
                              sites_occupation  \
	                     [getvalue [string trim [Selektion %W %y]] 0 " "] 1; Bend} }
  bind $ws.li <Double-Button-1> {Bend}
#  bind $ws.li <Leave> {Bend}
#  bind $ws.li <Triple-Button-1> {Bend}

  pack $ws.sbx  -fill x -side bottom
  pack $ws.sby  -fill y -side left
  pack $ws.li   -expand true -fill both -side right

  set W_sites_list $ws.li

  set li_he_txt1 " IQ CL WS        X           Y           Z"
  set li_he_txt2 "          R_WS    NLQ NOQ  IT TXTT     CONC"
  $ws.li insert end "$li_he_txt1$li_he_txt2"

}

incr IQ
set WYCKOFFQ($IQ) "-"
  set aux [format "%3i%3i  %1s%12.6f%12.6f%12.6f" \
           $IQ $ICL $WYCKOFFQ($IQ) $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
  $ws.li insert end $aux

# debug $PRC  " IQ $IQ   $IQ1Q($IQ) ...  $IQ2Q($IQ)  SETUP_MODE $STRUCTURE_SETUP_MODE  "

}
}

pack $Wcsys.site -in $Wcsys -expand yes -fill both
}
#site specification --------------------------------------------------------------- END

#......................................................................................
#                                          insert type information for input  PER PEDES

if {$STRUCTURE_SETUP_MODE =="PER PEDES"     || \
    $STRUCTURE_SETUP_MODE == "MULTI LAYERS" || \
    $STRUCTURE_SETUP_MODE == "2D LAYERS"    || \
    $STRUCTURE_SETUP_MODE == "3D SURFACE"  } {
       sites_occupation_done 0 0
}
#......................................................................................

#*************************************************
global QUICK QUICK_0D
if {$QUICK_0D==1} {
puts "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQUICK 0D"
  destros $Wcsys 
  create_system_0D 
}
#*************************************************

}
#                                                                   structure_window END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                                structure_window-done-RETURN     
#***************************************************************************************
#
proc structure_window-done-RETURN { } {

global Wcsys sysfile syssuffix ALAT
global list_ALPHA DIMENSION

set PRC structure_window-done-RETURN

debug $PRC WRHEAD
debug $PRC "dimension $DIMENSION"

#
#-------------------------- check consistency of wigner-seitz radii
#

set scale_RWS [consistency_of_wigner-seitz_radii]

if {$scale_RWS==1} {
   give_warning "." "WARNING \n\n the lattice parameter ALAT  \n\nwas set to 1 \n\n \
                     it has been adjusted to  $ALAT \n\n \
                     according to the Wigner-Seitz radii \n" 
} elseif {$scale_RWS!=0} {
   give_warning "." "WARNING \n\n lattice parameters and  \
                             \n\n Wigner-Seitz radii are \n\n INCONSISTENT  \
                             \n\n use one of the scaling procedures \n\n\n \
                              suggested scaling for R_WS \n\n $scale_RWS"  
   return
}

#
#-------------------------- check whether system file exists
#
set ff $sysfile$syssuffix

if {[file exists $ff]!=0} {

    set sysfile_new ${sysfile}
    set ff_new      ${sysfile_new}$syssuffix
    set IALPH -1

    while {[file exists $ff_new]!=0} {
       incr IALPH
       set sysfile_new "${sysfile}_[lindex $list_ALPHA $IALPH]"
       set ff_new      ${sysfile_new}$syssuffix
    }

    set cw $Wcsys.confirm
    if {[winfo exists $cw] == 1} {destroy $cw}
    toplevel_init $cw " " 0 0

    wm geometry $cw +300+50
	
    wm title    $cw "confirm action"
    wm iconname $cw "confirm action"

    set files "${sysfile}${syssuffix}"

    eval set res [catch "exec ls -l ${files}" message]

    label $cw.pot -text "the system file   $ff  exists \n\n$message" \
            -pady 10  -padx 30 -anchor w -justify left
    button $cw.overwrite -text "overwrite old file" -width 50 -height 5 -bg green \
        -command "structure_window-done-RETURN_quit overwrite $sysfile_new"
    button $cw.backup -text "make backup of old file" -width 50 -height 5 -bg green\
        -command "structure_window-done-RETURN_quit backup $sysfile_new"
    button $cw.newname -text "use name    $ff_new   for new file" \
        -width 50 -height 5 -bg green \
        -command "structure_window-done-RETURN_quit newname $sysfile_new"
    button $cw.close -text "dismiss" -width 50 -height 5 -bg red \
        -command "destros $cw"
    pack $cw.pot $cw.overwrite $cw.backup $cw.newname $cw.close

} else {

    write_sysfile
    destros $Wcsys
    unlock_list
}

} 
#                                                           structure_window-done-RETURN
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                               structure_window-done-RETURN_quit      
#***************************************************************************************
#
#
proc structure_window-done-RETURN_quit {key sysfile_new} {

    global Wcsys sysfile syssuffix

    set PRC structure_window-done-RETURN_quit
    
    set ff $sysfile$syssuffix
    
    if {$key=="overwrite"} {
	writescr .d.tt "\nINFO from <structure_window>   ------------------ \n"
	writescr .d.tt "system file   $ff  overwritten    \n\n"
    } elseif {$key=="backup"} {
	writescr .d.tt "\nINFO from <structure_window>   ------------------ \n"
	writescr .d.tt "old system file   $ff  moved to ${ff}.bak  \n\n"
	exec mv $ff ${ff}.bak
    } elseif {$key=="newname"} {
	writescr .d.tt "\nINFO from <structure_window>   ------------------ \n"
	writescr .d.tt "new system file named    $sysfile_new$syssuffix  \n\n"
	set sysfile $sysfile_new
    }
    
    write_sysfile
    destros $Wcsys
    unlock_list
    
}
#                                                      structure_window-done-RETURN_quit
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                           consistency_of_wigner-seitz_radii  
#***************************************************************************************

#     return   0:   V(RBAS) = V(RWS)
#              1:   ALAT = 1            ALAT  scaled according to  RWS
#              s:   V(RBAS) <> V(RWS)   suggested scaling factor  s  for RWS
#
proc consistency_of_wigner-seitz_radii { } {

    global NQ RBASX RBASY RBASZ ALAT RWS
    
    set PI   3.141592653589793238462643              
    set PRC  consistency_of_wigner-seitz_radii


    set V_RWS 0.0
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	set V_RWS [expr $V_RWS + pow($RWS($IQ),3)]
    }
    set V_RWS [expr $V_RWS * 4.0*$PI/3.0]
 
    set V_RBAS [ expr  $RBASX(1)*($RBASY(2)*$RBASZ(3)-$RBASY(3)*$RBASZ(2)) \
	             + $RBASX(2)*($RBASY(3)*$RBASZ(1)-$RBASY(1)*$RBASZ(3)) \
                     + $RBASX(3)*($RBASY(1)*$RBASZ(2)-$RBASY(2)*$RBASZ(1)) ]
    set V_RBAS [ expr  abs($V_RBAS) *  pow($ALAT,3) ]

#---------------------------------------------------------------------------------------
    if {[expr abs($ALAT-1.0)] < 0.000001} {                                   # ALAT = 1

	set ALAT [expr pow([expr $V_RWS/$V_RBAS],[expr 1.0/3.0]) ]
	writescrd .d.tt $PRC "\nthe lattice parameter ALAT has been set to  $ALAT"
	return 1   
	
#---------------------------------------------------------------------------------------
    } else {
   
	set scale_RWS [expr pow([expr $V_RBAS/$V_RWS],[expr 1.0/3.0]) ]

	if {[expr abs($scale_RWS-1.0)] < 0.00000001} {               #  V(RBAS) = V(RWS)

	    writescrd .d.tt $PRC  \
                      "\nlattice parameters and Wigner-Seitz radii consistent \n"
	    return 0
       
	} else {                                                   #  V(RBAS) <>  V(RWS)

	    writescrd .d.tt $PRC  \
                      "\nlattice parameters and Wigner-Seitz radii INCONSISTENT \
		      \nscaling factor for Wigner-Seitz radii $scale_RWS" 
	    return $scale_RWS
	    
	}                                     
    }
#---------------------------------------------------------------------------------------
}
#                                                      consistency_of_wigner-seitz_radii
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                                 scale_wigner-seitz_radii    
#***************************************************************************************
#
#       scale the Wigner-Seitz radii  conserving the volume fixed by lattice parameters 
#
proc scale_wigner-seitz_radii { } {

global NQ RBASX RBASY RBASZ ALAT RWS DIMENSION ZRANGE_TYPE
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R
    
set PI   3.141592653589793238462643              
set PRC  scale_wigner-seitz_radii

#---------------------------------------------------------------------------------------
if {$DIMENSION=="3D" || ($DIMENSION=="2D" && $ZRANGE_TYPE=="slab")} {
   
   set V_RWS 0.0
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set V_RWS [expr $V_RWS + pow($RWS($IQ),3)]
   }
   set V_RWS [expr $V_RWS * 4.0*$PI/3.0]
    
   set V_RBAS [ expr  $RBASX(1)*($RBASY(2)*$RBASZ(3)-$RBASY(3)*$RBASZ(2)) \
                    + $RBASX(2)*($RBASY(3)*$RBASZ(1)-$RBASY(1)*$RBASZ(3)) \
                    + $RBASX(3)*($RBASY(1)*$RBASZ(2)-$RBASY(2)*$RBASZ(1)) ]
   set V_RBAS [ expr  abs($V_RBAS) *  pow($ALAT,3) ]
   
   set scale_RWS [expr pow([expr $V_RBAS/$V_RWS],[expr 1.0/3.0]) ]

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set RWS($IQ) [expr $RWS($IQ)*$scale_RWS] 
   }

   writescrd .d.tt $PRC "Wigner-Seitz radii scaled by factor $scale_RWS" 

#---------------------------------------------------------------------------------------
} elseif {$DIMENSION=="2D"} {

   set V_RBAS_L [ expr  $RBASX_L(1)*($RBASY_L(2)*$RBASZ_L(3)-$RBASY_L(3)*$RBASZ_L(2)) \
                      + $RBASX_L(2)*($RBASY_L(3)*$RBASZ_L(1)-$RBASY_L(1)*$RBASZ_L(3)) \
                      + $RBASX_L(3)*($RBASY_L(1)*$RBASZ_L(2)-$RBASY_L(2)*$RBASZ_L(1)) ]
   set V_RBAS_L [ expr  abs($V_RBAS_L) *  pow($ALAT,3) ]
   writescrd .d.tt $PRC "  volume  V_RBAS_L  $V_RBAS_L"
    
   set V_RBAS_R [ expr  $RBASX_R(1)*($RBASY_R(2)*$RBASZ_R(3)-$RBASY_R(3)*$RBASZ_R(2)) \
                      + $RBASX_R(2)*($RBASY_R(3)*$RBASZ_R(1)-$RBASY_R(1)*$RBASZ_R(3)) \
                      + $RBASX_R(3)*($RBASY_R(1)*$RBASZ_R(2)-$RBASY_R(2)*$RBASZ_R(1)) ]
   set V_RBAS_R [ expr  abs($V_RBAS_R) *  pow($ALAT,3) ]
   writescrd .d.tt $PRC "  volume  V_RBAS_R  $V_RBAS_R"

   set V_RBAS [ expr  $RBASX(1)*($RBASY(2)*$RBASZ(3)-$RBASY(3)*$RBASZ(2)) \
                    + $RBASX(2)*($RBASY(3)*$RBASZ(1)-$RBASY(1)*$RBASZ(3)) \
                    + $RBASX(3)*($RBASY(1)*$RBASZ(2)-$RBASY(2)*$RBASZ(1)) ]
   set V_RBAS [ expr  abs($V_RBAS) *  pow($ALAT,3) ]
   writescrd .d.tt $PRC "  volume  V_RBAS  $V_RBAS"
    
   set V_RBAS_S [ expr  abs($V_RBAS) - $V_RBAS_L - $V_RBAS_R ]
   writescrd .d.tt $PRC "  volume  V_RBAS_S  $V_RBAS_S"
#
#--------------------------------------------------------------------- deal with bulk(L)
#
   set V_RWS 0.0
   for {set IQ 1} {$IQ <= $NQ_bulk_L} {incr IQ} {
      set V_RWS [expr $V_RWS + pow($RWS($IQ),3)]
   }
   set V_RWS [expr $V_RWS * 4.0*$PI/3.0]
   
   set scale_RWS [expr pow([expr $V_RBAS_L/$V_RWS],[expr 1.0/3.0]) ]

   for {set IQ 1} {$IQ <= $NQ_bulk_L} {incr IQ} {
      set RWS($IQ) [expr $RWS($IQ)*$scale_RWS] 
   }

   writescrd .d.tt $PRC "Wigner-Seitz radii for bulk(L) scaled by factor $scale_RWS" 
#
#---------------------------------------------------------------------- deal with spacer
#
   set V_RWS 0.0
   for {set IQ [expr $NQ_bulk_L + 1]} {$IQ <= [expr $NQ - $NQ_bulk_R]} {incr IQ} {
      set V_RWS [expr $V_RWS + pow($RWS($IQ),3)]
   }
   set V_RWS [expr $V_RWS * 4.0*$PI/3.0]
   
   set scale_RWS [expr pow([expr $V_RBAS_S/$V_RWS],[expr 1.0/3.0]) ]

   for {set IQ [expr $NQ_bulk_L + 1]} {$IQ <= [expr $NQ - $NQ_bulk_R]} {incr IQ} {
      set RWS($IQ) [expr $RWS($IQ)*$scale_RWS] 
   }

   writescrd .d.tt $PRC "Wigner-Seitz radii for spacer  scaled by factor $scale_RWS" 
#
#--------------------------------------------------------------------- deal with bulk(R)
#
   set V_RWS 0.0
   for {set IQ [expr $NQ - $NQ_bulk_R + 1]} {$IQ <= $NQ} {incr IQ} {
      set V_RWS [expr $V_RWS + pow($RWS($IQ),3)]
   }
   set V_RWS [expr $V_RWS * 4.0*$PI/3.0]
   
   set scale_RWS [expr pow([expr $V_RBAS_R/$V_RWS],[expr 1.0/3.0]) ]

   for {set IQ [expr $NQ - $NQ_bulk_R + 1]} {$IQ <= $NQ} {incr IQ} {
      set RWS($IQ) [expr $RWS($IQ)*$scale_RWS] 
   }

   writescrd .d.tt $PRC "Wigner-Seitz radii for bulk(R) scaled by factor $scale_RWS" 
   
}
#---------------------------------------------------------------------------------------
}
#                                                               scale_wigner-seitz_radii
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<





#***************************************************************************************
#                                 set_same_wigner-seitz_radii    
#***************************************************************************************
#
#           set the Wigner-Seitz radii all the same 
#           conserving the volume fixed by lattice parameters 
#
proc set_same_wigner-seitz_radii { } {

global NQ RBASX RBASY RBASZ ALAT RWS DIMENSION ZRANGE_TYPE
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R
    
set PI   3.141592653589793238462643              
set PRC  set_same_wigner-seitz_radii


    debug $PRC "  NQ $NQ"


   set V_RBAS [ expr  $RBASX(1)*($RBASY(2)*$RBASZ(3)-$RBASY(3)*$RBASZ(2)) \
                    + $RBASX(2)*($RBASY(3)*$RBASZ(1)-$RBASY(1)*$RBASZ(3)) \
                    + $RBASX(3)*($RBASY(1)*$RBASZ(2)-$RBASY(2)*$RBASZ(1)) ]
   set V_RBAS [ expr  abs($V_RBAS) *  pow($ALAT,3) ]
   
   set common_RWS [expr pow([expr 3.0*$V_RBAS/($NQ*4.0*$PI)],[expr 1.0/3.0]) ]

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      debug $PRC "  IQ $IQ  RWS = $RWS($IQ) --> $common_RWS"
      set RWS($IQ) $common_RWS 
   }

   writescrd .d.tt $PRC "all Wigner-Seitz radii set to $common_RWS" 

}
#                                                            set_same_wigner-seitz_radii
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<




#***************************************************************************************
#                                 suppress_symmetry    
#***************************************************************************************
#
#           suppress the symmetry of the system
#           i.e. set all sites to be inequivalent
#
proc suppress_symmetry { } {

global NQ NCL IQECL NQCL NOQ IQAT ITOQ NAT CONC TXTT TXTT0 ZT IMT IMQ RWS RWSM NT ICLQ
global set TABCHSYM


set PRC  suppress_symmetry


    debug $PRC "suppressing the symmetry of the system with NQ = $NQ"

#
#--------------------------------------------------------------------------------- sites
#
debug $PRC "  dealing with  sites "

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  set WYCKOFFQ($IQ) "-"
##  for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
##      append TXT [format "%3i" $ITOQ($IA,$IQ)]
##  }
}

#
#
#-------------------------------------------------------------------------- site classes
#
debug $PRC "  dealing with  site classes NCL"

set NCL $NQ

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  set ICL           $IQ
  set NQCL($ICL)      1
  set IQECL(1,$ICL) $IQ
  set ICLQ($IQ)     $ICL
}   

#
#------------------------------------------------------------------------- find NAT IQAT
#
set NT_NEW 0

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  for {set IOCC 1} {$IOCC <= $NOQ($IQ)} {incr IOCC} {
     set IT $ITOQ($IOCC,$IQ)
     incr NT_NEW
     set CONC_NEW($NT_NEW) $CONC($IT) 
     set TXTT_NEW($NT_NEW) $TABCHSYM($ZT($IT))
     set   ZT_NEW($NT_NEW)   $ZT($IT)
       set IQAT(1,$NT_NEW)       $IQ
       set ITOQ($IOCC,$IQ)       $NT_NEW
  }
}   

set NT $NT_NEW
for {set IT 1} {$IT <= $NT} {incr IT} {
     set   CONC($IT) $CONC_NEW($IT) 
     set   TXTT($IT) $TXTT_NEW($IT) 
     set  TXTT0($IT) $TXTT_NEW($IT) 
     set     ZT($IT)   $ZT_NEW($IT) 
     set    NAT($IT)             1    
debug $PRC "VORHER  $IT IT   $TXTT($IT)  $ZT($IT)  $CONC($IT)  $NAT($IT)"
}

deal_with_names_of_atom_types

for {set IT 1} {$IT <= $NT} {incr IT} {
debug $PRC "NACHHER  $IT IT   $TXTT($IT)  $ZT($IT)  $CONC($IT)  $NAT($IT)"
}
#
#----------------------------------------------------------------------- find mesh table
#
set NM 0
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   set IQ1 $IQECL(1,$ICL)
   incr NM
   set RWSM($NM) $RWS($IQ1)
   set IMQ($IQ1) $NM  
   for {set IE 2} {$IE <= $NQCL($ICL)} {incr IE} {
      set IQ $IQECL($IE,$ICL)
      set IMQ($IQ) $IMQ($IQ1)
   }
}   

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
     set IT $ITOQ($IO,$IQ)
     set IMT($IT) $IMQ($IQ)
  }
}

#
#------------------------------------------------------------------- 0D embedded cluster
#

##if {$CLUSTER_TYPE=="embedded"} {
##   puts "writting embedded cluster information"
##   puts $sysdat "------------------------ embedded cluster information -----------------##"
##   puts $sysdat "cluster type"
##   puts $sysdat  $CLUSTER_TYPE
##   puts $sysdat "host system file name"
##   puts $sysdat  $sysfile_host
##
##   set ITCLU 0
##   for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
##      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
##          incr ITCLU
##          set ITCLU_OQCLU($IO,$IQCLU) $ITCLU 
##
##          set NATCLU($ITCLU) 1
##          set IQCLU_ATCLU(1,$ITCLU) $IQCLU 
##          set   ZTCLU($ITCLU)    $Z_IQCLU($IO,$IQCLU)
##          set TXTTCLU($ITCLU)  $TXT_IQCLU($IO,$IQCLU)
##          set CONCCLU($ITCLU) $CONC_IQCLU($IO,$IQCLU)
##
##      } 
##   }
##   set NTCLU $ITCLU
##
##   puts $sysdat "number of cluster sites NQCLU"
##   puts $sysdat [format %4i $NQCLU]
##
##   set TXT " IQCLU       cluster vectors   (cart. coord.) \[A\]"
##   append TXT "                 N5VEC_CLU        IQ  NOCC ITCLU"  
##
##   puts $sysdat $TXT
##   for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
##
##      set TXT [format "%4i" $IQCLU ]
##      append TXT [format "%18.12f%18.12f%18.12f " \
##                 $RQCLU(1,$IQCLU) $RQCLU(2,$IQCLU) $RQCLU(3,$IQCLU)]
##      append TXT [format "%4i%4i%4i%4i%4i  %4i" \
##                 $N5VEC_IQCLU(1,$IQCLU) $N5VEC_IQCLU(2,$IQCLU) $N5VEC_IQCLU(3,$IQCLU) \
##                 $N5VEC_IQCLU(4,$IQCLU) $N5VEC_IQCLU(5,$IQCLU)  $IQ_IQCLU($IQCLU) ]
####
##      append TXT [format "%6i " $NOCC_IQCLU($IQCLU)]
##
##      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
##          append TXT [format "%5i" $ITCLU_OQCLU($IO,$IQCLU)]
##      } 
##      puts $sysdat $TXT
##   }
##
##   puts $sysdat "number of cluster atom types NTCLU"
##   puts $sysdat  [format %4i $NTCLU]
##   puts $sysdat " ITCLU  ZTCLU  TXTTCLU      NATCLU  CONCCLU  IQCLU_ATCLU (sites occupied)"
##   for {set ITCLU 1} {$ITCLU <= $NTCLU} {incr ITCLU} {
##     set TXT [string range "$TXTTCLU($ITCLU)       " 0 7]
##     set OUT [format " %5i%7i     %8s%8i%9.3f" \
##              $ITCLU $ZTCLU($ITCLU) $TXT $NATCLU($ITCLU) $CONCCLU($ITCLU)]
##     for {set IA 1} {$IA <= $NATCLU($ITCLU)} {incr IA} {
##        append OUT [format "%5i" $IQCLU_ATCLU($IA,$ITCLU)]
##     }
##     puts $sysdat $OUT
##   }
##
##}

   writescrd .d.tt $PRC "all symmetry relations have been suppressed " 

}
#                                                                      suppress_symmetry
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<






#***************************************************************************************
#                             structure_window_update_site_table        
#***************************************************************************************
#
proc structure_window_update_site_table {} {
#
#------------------------------------------------------ update site table
#
    global NOQ RWS NLQ ITOQ TXTT CONC WYCKOFFQ RQX RQY RQZ W_sites_list ICLQ NQ

    for {set IQ 1} {$IQ <= $NQ} {incr IQ } {

       set ICL $ICLQ($IQ)
       set txtr ""
       set txt2 ""

       if { $NOQ($IQ) > 0 } {
          set txtr [format " %12.6f  %2i  %2i  " $RWS($IQ) $NLQ($IQ) $NOQ($IQ)]
          for {set i 1} {$i <= $NOQ($IQ)} {incr i} {
             set IT $ITOQ($i,$IQ)
             set TXT [string range "$TXTT($IT)    " 0 7]
             set txt2 "$txt2[format " %2i %8s %4.2f" $IT $TXT $CONC($IT)]  "
          }
       }

       set txt1 [format "%3i%3i  %1s%12.6f%12.6f%12.6f" \
                              $IQ $ICL $WYCKOFFQ($IQ) $RQX($IQ) $RQY($IQ) $RQZ($IQ)]

       $W_sites_list delete $IQ
       $W_sites_list insert $IQ "$txt1$txtr$txt2"
    }

}
#                                                 structure_window_update_site_table END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                           sites_occupation          
#***************************************************************************************
#
# simple_occ_handling_mode is used to create a new occupation type in arbitrary cluster
# selection, no changes to all the internal global vars are carried out 
# maybe the RWS stuff should also be hidden in the menue 
#
proc sites_occupation {IQ NOCC {simple_occ_handling_mode 0}} {

    global CONC inscoo COLOR ITOQ NQ NQEQ
    global WYCKOFF_SYMBOL equicoord mark flag TABCHSYM RWS Wcsys TXTT
    global CONCOCC Wscale TXTOCC ZOCC OK_OCC RWSOCCAVRG Wocc_OK_but Wocc
    global RWSOCC pass_NOCC pass_IQ NLOCCMAX NLOCC NLOCCMAX
    global VISUAL
    global COLOR WIDTH HEIGHT FONT
    
    if { $IQ == "IQ" } {return}
    
    set ::tcl_precision 17   ; # this has to be global to have an effect
    
    set Wocc .w_sites_occupation
    set win $Wocc 
    if {[winfo exists $win]} {
	eval destroy [winfo children $win]  ; # there is a tkwait 
    } else {
	toplevel $win -width 400 -height 300 -visual $VISUAL
    }
    wm geometry     $win +300+200
    wm positionfrom $win user
    wm sizefrom     $win user
    wm minsize      $win  200 100
    wm title        $win "Specify occupation for site $IQ"
    
    label  $win.label -text "number of occupants (atom types) on site  $IQ"
    frame  $win.buts
    for {set i 1} {$i <= 5} {incr i} {
	button $win.buts.b$i -text "$i" -padx 10 -width 5 -height 2  -bg $COLOR(GOON) \
		-command  "sites_occupation $IQ $i $simple_occ_handling_mode"
	pack  $win.buts.b$i  -side left  -fill both -expand yes
    }
    $win.buts.b$NOCC configure -bg $COLOR(YELLOW)
    
    label $win.label2 -text "     " -height 3
    pack  $win.label $win.buts $win.label2
    
    set Wscale $win.tab
    
    pack [frame $win.tab]
    
    set pass_IQ    $IQ
    set pass_NOCC  $NOCC
    set NLOCCMAX   0
    set RWSOCCAVRG 0.0
    
    for {set IOCC 1} {$IOCC <= $NOCC} {incr IOCC} {
	set TXTOCC($IOCC) ""
	set ZOCC($IOCC)   ""
	set NLOCC($IOCC)  "3"
	set RWSOCC($IOCC) "0.0"
	set OK_OCC($IOCC) 0
	
	pack [frame $win.tab.o$IOCC]
	button $win.tab.o$IOCC.but -width 15 -height 2 -text "specify occupant $IOCC" \
		-bg $COLOR(LIGHTBLUE) -padx 10 -pady 5 -anchor w \
		-command [list table_PS $IOCC "table_ps_next_cmd $IQ $NOCC $simple_occ_handling_mode"] 

	# IOCC is automatically added in table_PS
	proc table_ps_next_cmd {IQ NOCC simple_occ_handling_mode IOCC} {
	    
	    global Wocc_OK_but

	    #debug [myname] "IQ: $IQ NOCC: $NOCC simple_occ_handling_mode: $simple_occ_handling_mode \
	    #	    IOCC: $IOCC"
	    
	    if {[sites_occupation_deal_with_occupant $IOCC]} {
		
		if {$simple_occ_handling_mode} {
		    if {[winfo exists $Wocc_OK_but]} {
			$Wocc_OK_but configure -state normal
		    }
		} else {
		    sites_occupation_done $IQ $NOCC
		} 
	    }
	}
	
	label $win.tab.o$IOCC.lab1 -width 4 -anchor w \
		-font $FONT(TAB) -textvariable TXTOCC($IOCC)
	label $win.tab.o$IOCC.labL -text "NL:" -padx 10 -pady 5 -anchor w
	label $win.tab.o$IOCC.labL1 -width 2 -anchor w \
		-font $FONT(TAB) -textvariable NLOCC($IOCC) 
	label $win.tab.o$IOCC.lab -text "R_WS:" -padx 10 -pady 5 -anchor w
	entry $win.tab.o$IOCC.ent2 -width 8 -relief sunken -highlightthickness 0 \
		-font $FONT(TAB) -fg $COLOR(ENTRYFG) -textvariable RWSOCC($IOCC)
	pack $win.tab.o$IOCC.but $win.tab.o$IOCC.lab1 \
		$win.tab.o$IOCC.labL $win.tab.o$IOCC.labL1 \
		$win.tab.o$IOCC.lab  $win.tab.o$IOCC.ent2  -side left -fill x -pady 5
	
	scale $win.tab.o$IOCC.sca -from 0.00 -to 1.00 -digits 3 -length 400 \
		-resolution 0.01 -tickinterval 0.20 \
		-orient horizontal -width 10 -showvalue true \
		-label "concentration of occupant  $IOCC " \
		-variable CONCOCC($IOCC) \
		-command "sites_occupation_check_CONCOCC $IOCC $NOCC"
	pack  $win.tab.o$IOCC.sca -side left -fill both -pady 5
    }

    # set all CONCOOC to 0.00, but the first which is set to 1.00
    for {set IOCC 1} {$IOCC <= $NOCC} {incr IOCC} { 
	$win.tab.o$IOCC.sca set 0.00 
    }
    $win.tab.o1.sca set 1.00
    
    frame $win.tab.oav
    pack  $win.tab.oav -side left
    button $win.tab.oav.but -width 15 -height 2 -text " " -state disabled \
	    -padx 10 -pady 5 -relief flat
    entry $win.tab.oav.ent1 -width 4 -relief flat -state disabled \
	    -font $FONT(TAB) -fg $COLOR(ENTRYFG) -highlightthickness 0
    
    label $win.tab.oav.labL -text "NL:" -padx 10 -pady 5
    entry $win.tab.oav.entL -width 2 -relief sunken -font $FONT(TAB) \
	    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -highlightthickness 0 \
	    -textvariable NLOCCMAX
    
    label $win.tab.oav.lab -text "R_WS:" -padx 10 -pady 5
    entry $win.tab.oav.ent2 -width 8 -relief sunken -font $FONT(TAB) \
	    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -highlightthickness 0 \
	    -textvariable RWSOCCAVRG
    label $win.tab.oav.lab2 -text "average volume value" -padx 10 -pady 5
    
    pack  $win.tab.oav.but $win.tab.oav.ent1 $win.tab.oav.labL $win.tab.oav.entL \
	    $win.tab.oav.lab $win.tab.oav.ent2 $win.tab.oav.lab2 -side left -pady 5
    
    #----------------------------------------------------------------------------------
    proc sites_occupation_check_CONCOCC {IOCC NOCC dummy} {

	global CONCOCC Wscale
	
	set sum 0.0
	for {set JOCC 1} {$JOCC <= $NOCC} {incr JOCC} {
	    set sum [expr {$sum + $CONCOCC($JOCC)}]
	}
	
	if {$sum > 1.0} {
	    set new [expr {1.0-($sum-$CONCOCC($IOCC))}]
	    set CONCOCC($IOCC) $new
	    $Wscale.o$IOCC.sca set $new
	}
    }
#----------------------------------------------------------------------------------

    frame  $win.site
    label  $win.site.space -text " "
    frame  $win.site.ok
    pack   $win.site.space

    if {$simple_occ_handling_mode} {
	set done_cmd "sites_occupation_shut $Wocc"
    } else {
	set done_cmd "sites_occupation_done $IQ $NOCC ; sites_occupation_shut $Wocc" 
    }
    
    set Wocc_OK_but $win.site.ok.goon
    button $win.site.ok.goon -text "all done --- GO ON" -bg $COLOR(DONE) \
	    -width 25 -height 3 -state disabled \
	    -command $done_cmd
    button $win.site.ok.cancel -text "CLOSE" -width 25 -height 3 -bg $COLOR(CLOSE) \
            -command "set pass_NOCC 0 ; sites_occupation_shut $Wocc"
    
    pack $win.site.ok.goon $win.site.ok.cancel -side left -fill both -expand yes
    pack $win.site.ok
    pack $win.site
    
    #----------------------------------------------------------------------------------
    proc sites_occupation_shut {w} {
	if {[winfo exists $w]} {destroy  $w}
    }
    #----------------------------------------------------------------------------------
    
}
#                                                                   sites_occupation END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                             sites_occupation_deal_with_occupant        
#***************************************************************************************
#
proc sites_occupation_deal_with_occupant {IOCC} {

    global ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG Wocc_OK_but
    global pass_NOCC pass_IQ NLOCC NLOCCMAX
    global TABRWS
    
    set NOCC $pass_NOCC
    set IQ   $pass_IQ
    
    set RWSOCC($IOCC) $TABRWS($ZOCC($IOCC))
    set NLOCC($IOCC) 3
    if { $ZOCC($IOCC) > 56 && $ZOCC($IOCC) < 71 } { set NLOCC($IOCC) 4}
    if { $ZOCC($IOCC) > 88 } { set NLOCC($IOCC) 4 }
    
    set OK_OCC($IOCC) 1
    
    set OK_sum 0
    for {set i 1} {$i <= $NOCC} {incr i} {
	set OK_sum [expr {$OK_sum + $OK_OCC($i)}]
	if {$NLOCC($IOCC) > $NLOCCMAX} {set NLOCCMAX $NLOCC($IOCC)}
    }
    
    if {$OK_sum == $NOCC} { 
	return 1     ; # then call sites_occupation_done $IQ $NOCC
    } else { 
	return 0 
    }

}
#                                                sites_occupation_deal_with_occupant END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                                sites_occupation_done     
#***************************************************************************************
#
proc sites_occupation_done {IQ NOCC} {

    global RWS ITOQ ZT CONC TXTT TXTT0 NQCL Wcsys NQ NLQ NCL NT NQCL
    global Wcsys structure_data_complete IQ1Q IQ2Q NLOCCMAX
    global NOQ ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG Wocc_OK_but
    global W_sites_list RQX RQY RQZ Wocc  WYCKOFFQ NCPA
    global SPACEGROUP Wpprc   per_pedes_occupation STRUCTURE_SETUP_MODE
    global OCCUPATION_OF_LAYERS
    global list_ALPHA   TABCHSYM  
    global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
    
    set PRC "sites_occupation_done"
    set tcl_precision 17
    set Wocc .w_sites_occupation
    
    debug $PRC "  IQ $IQ NOCC $NOCC   STRUCTURE_SETUP_MODE = $STRUCTURE_SETUP_MODE"
    
#=======================================================================================
# skip if occupations has been set in case of  PER PEDES indicated by IQ=0

    if {$IQ!=0} {
	
#        if {[info exists $Wocc_OK_but]} {
#           if {[winfo exists $Wocc_OK_but]} {$Wocc_OK_but configure -state normal}
#        }	

	set sum 0.0
	set RWS_flag 1
	for {set i 1} {$i <= $NOCC} {incr i} {
	    set sum [expr {$sum + $CONCOCC($i)}]
	    if {$RWSOCC($i) < "0.001" } {set RWS_flag 0}
	}
	
	if {$sum < 1.0} {
	    give_warning "." "WARNING \n\n sum of concentrations < 1  \n\n check entries " 
	    return
	}
	if {[string trim $RWSOCCAVRG] == "" } {set RWSOCCAVRG 0.0}
	if {$RWSOCCAVRG < "0.001" } {
	    if {$RWS_flag == "0"} {
               give_warning "." "WARNING \n\n average Wigner-Seitz radius not set  \n\n \
	       	set average value   \n\n   OR    \n\n give values for all occupants ! "
		return
	    } else {
		set s 0.0
		for {set i 1} {$i <= $NOCC} {incr i} {
		    set s [expr {$s + pow($RWSOCC($i),3)*$CONCOCC($i)}]
		}
		set RWSOCCAVRG [expr {pow($s,1.0/3.0)}]
	    }
	}
	#---------------------------------------->>>> occupation of site IQ is OK !!
	
	
	#
	#-------------------------------------------- store old type information
	#
	
        set NT 0
	for {set JQ 1} {$JQ <= $NQ} {incr JQ} {
	    for {set i 1} {$i <= $NOQ($JQ)} {incr i} {
		set ITOQ_aux($i,$JQ) $ITOQ($i,$JQ)
; # puts "************************* old type info: JQ $JQ  IO $i  IT $ITOQ_aux($i,$JQ)"
		if { $NT < $ITOQ($i,$JQ) } { set NT  $ITOQ($i,$JQ)}
	    }
	}
	for {set IT 1} {$IT <= $NT} {incr IT} {
	    set ZT_aux($IT)    $ZT($IT)
	    set CONC_aux($IT)  $CONC($IT)
            if {$STRUCTURE_SETUP_MODE != "3D SURFACE" } {
	       set TXTT0_aux($IT) $TXTT0($IT)
            } else {
               set TXTT0_aux($IT) $TABCHSYM($ZT_aux($IT))
            }
; # puts "************************* old type info: IT $IT  Z $ZT_aux($IT)  C $CONC_aux($IT) "
	}
	#
	#-------------------------------------------- store new type information
	#
; # puts "*************************  "
; # puts "************************* IQ   $IQ   $IQ1Q($IQ) -- $IQ2Q($IQ) "
; # puts "************************* NT   $NT    NOCC $NOCC"
; # puts "************************* NCL  $NCL"
	set NT_aux $NT
	for {set i 1} {$i <= $NOCC} {incr i} {
	    incr NT_aux
	    set ZT_aux($NT_aux) $ZOCC($i)
	    set CONC_aux($NT_aux) $CONCOCC($i)
	    set TXTT0_aux($NT_aux) $TXTOCC($i)
	    for {set JQ $IQ1Q($IQ)} {$JQ <= $IQ2Q($IQ)} {incr JQ} {
		set NOQ($JQ) $NOCC
		set RWS($JQ) $RWSOCCAVRG
		set NLQ($JQ) $NLOCCMAX
		set ITOQ_aux($i,$JQ) $NT_aux
	    }
	}
; # puts "************************* NT_aux   $NT_aux  "
	#
	#----------------------------------------- now update type dependent arrays
	#
	set JQ 0
	set NT 0
	for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
	    incr JQ
	    set JQ0 $JQ
; # puts "************************* ICL $ICL    JQ0 $JQ0   JQ $JQ    NOQ $NOQ($JQ)"
	    for {set i 1} {$i <= $NOQ($JQ)} {incr i} {
		set JT $ITOQ_aux($i,$JQ)
		incr NT
; # puts "************************* I $i  JT  $JT   NT  $NT"
		set ZT($NT)      $ZT_aux($JT)
		set CONC($NT)    $CONC_aux($JT)
		set TXTT0($NT)   $TXTT0_aux($JT)
		set ITOQ($i,$JQ) $NT
	    }
	    for {set j 2} {$j <= $NQCL($ICL)} {incr j} {
		incr JQ
		for {set i 1} {$i <= $NOQ($JQ)} {incr i} {
		    set ITOQ($i,$JQ) $ITOQ($i,$JQ0)
		}
	    }
	    
	}
	
    }
#                                 skip if occupations has been set in case of  PER PEDES
#=======================================================================================

#------------------------------------------------------------------------------
#                                                           deal with 2D layers
# for symmetric structures copy occupation of site JQ to JQSYM=NQ-JQ+1
# if this is unoccupied
# 
    if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
	
	if {![info exists OCCUPATION_OF_LAYERS]} {
	    set OCCUPATION_OF_LAYERS "NON-symmetric"
	}
	
	if {$OCCUPATION_OF_LAYERS=="symmetric"} {
	    
	    for {set JQ 1} {$JQ <= [expr {$NQ/2}]} {incr JQ} {
		if { $NOQ($JQ) > 0 } {
		    set JQSYM [expr {$NQ - $JQ + 1}]
		    
		    if { $NOQ($JQSYM) == 0 } {
			set NOQ($JQSYM) $NOQ($JQ)
			set NLQ($JQSYM) $NLQ($JQ)
			set RWS($JQSYM) $RWS($JQ)
			for {set i 1} {$i <= $NOQ($JQ)} {incr i} {
			    set ITOQ($i,$JQSYM) $ITOQ($i,$JQ)
			}
		    }
		}
	    }
	}
    }
    
#
#--------------------------------------------------------- deal with names of atom types
#
    
    deal_with_names_of_atom_types
    
#
#--------------------------------------------------------------------- update site table
#
    set JQ 0
    for {set ICL  1} {$ICL  <= $NCL}        {incr ICL } {
	for {set JQCL 1} {$JQCL <= $NQCL($ICL)} {incr JQCL} {
	    
	    incr JQ
	    set txtr ""
	    set txt2 "  "
	    
	    if { $NOQ($JQ) > 0 } {
		set txtr [format " %12.6f  %2i  %2i  " $RWS($JQ) $NLQ($JQ) $NOQ($JQ)]
		for {set i 1} {$i <= $NOQ($JQ)} {incr i} {
		    set IT $ITOQ($i,$JQ)
		    
		    # writing to "Coordinates of sites" or "structure window"
		    
		    if {[winfo exists $Wpprc.coord.line(1).atoms]} {
			
			#set txt2 "$txt2[format "%2i %2s %4.2f"  $IT $TXTT0($IT) $CONC($IT)]  "
			set txt2 "$txt2[format "%2s %4.2f"  $TXTT0($IT) $CONC($IT)]  "
			
		    } else {
			
		  	   set txt2 "$txt2[format "%2i %8s %4.2f" $IT $TXTT($IT) $CONC($IT)]  "
			
		    }
		}
	    }
	    
	    if {[winfo exists $Wpprc.coord.line(1).atoms] } {
		set per_pedes true
	    } else {
		set txt1 [format "%3i%3i  %1s%12.6f%12.6f%12.6f" \
			$JQ $ICL $WYCKOFFQ($JQ) $RQX($JQ) $RQY($JQ) $RQZ($JQ)]
	    }
	    
#------------------------------------- specify structure via SPACEGROUP or STRUCTURETYPE
	    if {$STRUCTURE_SETUP_MODE != "PER PEDES"    && \
		$STRUCTURE_SETUP_MODE != "MULTI LAYERS" && \
		$STRUCTURE_SETUP_MODE != "2D LAYERS"    && \
		$STRUCTURE_SETUP_MODE != "3D SURFACE" } {
		if {[winfo exists $W_sites_list]} {
		    $W_sites_list delete $JQ
		    $W_sites_list insert $JQ "$txt1$txtr$txt2"
		}
	    } else {
#----------------------------------------------------------- specify structure PER PEDES
		set per_pedes_occupation($JQ) $txt2
		
		if {[winfo exists $Wpprc.coord.line(1).atoms]} {
		    #
		    # check whether all sites are occupied and enable OK-button
		    #
		    set done 1
		    for {set IIQ 1} {$IIQ <= $NQ} {incr IIQ} {
			if {$per_pedes_occupation($IIQ)==""} {set done 0}
		    }
		    if {$done==1} {$Wpprc.head.goon configure -state normal}
		} else {
		    if {$JQ>=$IQ_display_start && $JQ<=$IQ_display_end } {
			$W_sites_list delete $JQ
			$W_sites_list insert $JQ "$txt1$txtr$txt2"
		    }
		}
	    }
	    
	}
    }
    
    ############### JAN doesn't like   "destroy  $Wocc"
    eval set res [catch "exec whoami " user]
    if {$user != "jm"} {if {[winfo exists $Wocc]} {destroy  $Wocc}}
#
#-------------------------------------------------------------------- all sites occupied
#
    set flag 1
    for {set JQ 1} {$JQ <= $NQ} {incr JQ} { 
	if {$NOQ($JQ) == 0} { set flag 0 }
    }
    
    if {$flag ==1} {
	if {[winfo exists $Wcsys.site.th.rasmol] } {
	    $Wcsys.site.th.rasmol configure -state normal
	}
	if {[winfo exists $Wcsys.site.th.spheres] } {
	    $Wcsys.site.th.spheres configure -state normal
	}
	if {[winfo exists $Wcsys.site.th.scale_R_WS] } {
	    $Wcsys.site.th.scale_R_WS configure -state normal
	}
	if {[winfo exists $Wcsys.site.th.set_same_R_WS] } {
	    $Wcsys.site.th.set_same_R_WS configure -state normal
	}
	if {[winfo exists $Wcsys.site.th.suppress_symmetry] } {
	    $Wcsys.site.th.suppress_symmetry configure -state normal
	}
	if {[winfo exists $Wcsys.c.goon] } {
	    $Wcsys.c.goon configure -state normal
	}
	set NCPA 0
	for {set JQ 1} {$JQ <= $NQ} {incr JQ} { 
	    if {$NOQ($JQ) != 1} { set NCPA 1 }
	}
	
	sites_occupation_get_system
	
    }
    
}
#                                                              sites_occupation_done END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                          sites_occupation_get_system
#***************************************************************************************
#
#               create a system file name derived from composition
#               if the name gets too long a short version is set up
#
proc sites_occupation_get_system { } {
    
    global ZT CONC TXTT TXTT0 NT NCPA sysfile syssuffix NAT NQ NOQ  ITOQ IQAT
    
    set lfn_max 40
    set PRC sites_occupation_get_system
    set tcl_precision 17
    
    for {set IT 1} {$IT <= $NT} {incr IT} { set NAT($IT) 0 }
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
	    set IT $ITOQ($IA,$IQ)
	    incr NAT($IT)
	    set IQAT($NAT($IT),$IT) $IQ
	}
    }
    
    set NCPA 0
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} { 
	if {$NOQ($IQ) != 1} { set NCPA 1 } 
    }
    
    #--------------------------------------------------------------------------------
    #                                                         now create system name
    set sysfile ""
    
    for {set IT 1} {$IT <= $NT} {incr IT} {
	set xNAT($IT)   $NAT($IT)
	set xCONC($IT)  $CONC($IT)
	set xTXTT0($IT) $TXTT0($IT)
    }
    
    #---------------------------------- combine types with the same name
    if {$NCPA==0} {
	for {set IT 1} {$IT <= $NT} {incr IT} {
	    for {set JT [expr $IT + 1] } {$JT <= $NT} {incr JT} {
		if { $xNAT($JT) != 0 } {
		    if {$xTXTT0($IT)==$xTXTT0($JT)} {
			set xNAT($IT) [expr $xNAT($IT) +  $xNAT($JT)]
			set xNAT($JT) 0
		    }
		}
	    }
	}
    }
    
    #---------------------------------- remove common factor in the stoichiometry
    set N 7
    for {set LOOP 1} {$LOOP <= 5} {incr LOOP} {
	set N [expr $N -1]
	set flag 1
	for {set IT 1} {$IT <= $NT} {incr IT} {
	    if {$xNAT($IT) != [expr $N*[expr $xNAT($IT)/$N]]} {set flag 0}
	}
	if {$flag == 1} {
	    for {set IT 1} {$IT <= $NT} {incr IT} {set xNAT($IT) [expr $xNAT($IT)/$N]}
	}
    }
    
    #---------------------------------------------------- create system name
    for {set IT 1} {$IT <= $NT} {incr IT} {
	if {$xNAT($IT) > 0 } {
	    
	    set sysfile [append sysfile [string trim $xTXTT0($IT)] ]
	    if {$xNAT($IT) > 1 } {set sysfile [append sysfile [string trim $xNAT($IT)] ]}
	    if {$xCONC($IT) < 1.0 } {
		if {$xNAT($IT) > 1 } {set sysfile [append sysfile _ ]}
		set c [format "%4.2f" $xCONC($IT)]
		if {[string range $c 3 3] == "0"} {set c [string range $c 0 2]}
		set sysfile [append sysfile $c]
	    }
	}
    }
    
    #=======================================================================================
    if {[string length $sysfile] < $lfn_max } { return }
    #=======================================================================================
    writescrd .d.tt $PRC "\nlength of standard file name ($lfn_max) exceeded \n"
    #--------------------------------------------------------------------------------
    #                                                    now create SHORT system name
    set sysfile [string trim $TXTT0(1)]
    
    for {set IT 2} {$IT <= $NT} {incr IT} {
	set done 0
	for {set JT 1} {$JT <= [expr $IT-1]} {incr JT} {
	    if {$TXTT0($IT)==$TXTT0($JT)} {set done 1}
	}
	
	if {$done==0} {
	    append sysfile "-[string trim $TXTT0($IT)]"
	}
    }
    
}
#                                                        sites_occupation_get_system END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                            command_IQ_display         
#***************************************************************************************
#
#               update the present   IQ  display
#
proc command_IQ_display { Wpprc cmd } {

global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
global NQ STRUCTURE_SETUP_MODE ZRANGE_TYPE SU_IQ SU_NAME BGSU 
global NQ_bulk_R NQ_bulk_L bulk_L_eq_bulk_R

set TXT0 "XX"

if {$cmd=="top"} {

   set IQ_display_start 1
   set IQ_display_end   $NQ_display

} elseif {$cmd=="forward"} {

   set IQ_display_end [expr $IQ_display_end + $NQ_display - $NQ_display_overlap]
   if {$IQ_display_end > $NQ} {set IQ_display_end $NQ}
   set IQ_display_start [expr $IQ_display_end - $NQ_display + 1]

} elseif {$cmd=="backward"} {

   set IQ_display_start [expr $IQ_display_start - $NQ_display + $NQ_display_overlap]
   if {$IQ_display_start < 1} {set IQ_display_start 1}
   set IQ_display_end [expr $IQ_display_start + $NQ_display - 1]

} elseif {$cmd=="bottom"} {

   set IQ_display_end $NQ
   set IQ_display_start [expr $IQ_display_end - $NQ_display + 1]

} else {
   return
}

#---------------------------------------------------------------------------------------
if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
   if { $ZRANGE_TYPE=="extended" } {
       if {$IQ_display_start <= $NQ_bulk_L} {
          set IQ1 [expr $NQ_bulk_L+1]
       } else {
          set IQ1 $IQ_display_start
       }
       if {$IQ_display_end > [expr $NQ - $NQ_bulk_R]} {
          set IQ2 [expr $NQ - $NQ_bulk_R]
       } else {
          set IQ2 $IQ_display_end
       }
       set IQ_spacer [expr ($IQ1+$IQ2)/2 ]
   }
}
#---------------------------------------------------------------------------------------
set IQ [expr $IQ_display_start - 1]
for {set i 1} {$i <= $NQ_display} {incr i} {
    incr IQ

    if {$IQ <= $IQ_display_end} {
        if [winfo exists $Wpprc.coord.line($i).entry_0($i,1)] {
	$Wpprc.coord.line($i).entry_0($i,1) configure -textvariable RQX_0($IQ)
	$Wpprc.coord.line($i).entry_0($i,2) configure -textvariable RQY_0($IQ)
	$Wpprc.coord.line($i).entry_0($i,3) configure -textvariable RQZ_0($IQ)
        }

	$Wpprc.coord.line($i).label($i)   configure -text "site $IQ: "
	$Wpprc.coord.line($i).entry($i,1) configure -textvariable RQX($IQ)
	$Wpprc.coord.line($i).entry($i,2) configure -textvariable RQY($IQ)
	$Wpprc.coord.line($i).entry($i,3) configure -textvariable RQZ($IQ)
        $Wpprc.coord.line($i).occupy  configure  -state normal \
	       -command "structure_per_pedes_read_coord_occupy_entry $IQ"
        if {$STRUCTURE_SETUP_MODE == "3D SURFACE" } {
           $Wpprc.coord.line($i).copy    configure  -state normal \
	          -command "structure_per_pedes_read_coord_vacuum_starts    $IQ"
        } else {
           $Wpprc.coord.line($i).copy    configure  -state normal \
	          -command "structure_per_pedes_read_coord_copy_entry   $IQ"
        }
        $Wpprc.coord.line($i).reset   configure  -state normal \
	       -command "structure_per_pedes_read_coord_reset_entry  $IQ" 
	$Wpprc.coord.line($i).atoms   configure \
           -textvariable per_pedes_occupation($IQ)


        if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || \
            $STRUCTURE_SETUP_MODE == "3D SURFACE" } {

	   if {$STRUCTURE_SETUP_MODE == "2D LAYERS" } {
              set TXT_LEFT     bulk(L)
              set TXT_SPACER   spacer
              set TXT_RIGHT    bulk(R)
           } else {
              # STRUCTURE_SETUP_MODE == "3D SURFACE"
              if { $bulk_L_eq_bulk_R == "YES" } {
                 set TXT_LEFT     vac(L)
              } else {
                 set TXT_LEFT     bulk(L)
              }
              set TXT_SPACER   surface
              set TXT_RIGHT    vac(R)
           }   

           set su $SU_IQ($IQ)
           $Wpprc.coord.line($i).struc  configure -bg $BGSU($su) -text " "

           set i_first [string first "(" $SU_NAME($su)]
           if {$i_first>0} {
              set TXT [string trim [string range $SU_NAME($su) 0 [expr $i_first-1]]]
	      if {$TXT!=$TXT0} {
                 $Wpprc.coord.line($i).struc  configure -text $TXT
                 set TXT0 $TXT
              }
           }                          

           if { $ZRANGE_TYPE=="extended" } {
              $Wpprc.coord.line($i).spacer configure -text " " 
              if {$IQ<=$NQ_bulk_L} {
                 $Wpprc.coord.line($i).spacer configure -bg green
		 if {$IQ==$IQ_display_start} {
		 $Wpprc.coord.line($i).spacer   configure -text $TXT_LEFT }
              } elseif {$IQ > [expr $NQ - $NQ_bulk_R] } {
                 if { $bulk_L_eq_bulk_R == "YES" } {
                    $Wpprc.coord.line($i).spacer  configure -bg green
                 } else {		
                    $Wpprc.coord.line($i).spacer  configure -bg red
                 }
		 if {$IQ==$IQ_display_end} {
		 $Wpprc.coord.line($i).spacer configure -text $TXT_RIGHT }
              } else {
                 $Wpprc.coord.line($i).spacer  configure -bg lightblue
              }
              if {$IQ==$IQ_spacer} {
		  $Wpprc.coord.line($i).spacer  configure  -text $TXT_SPACER }
           }                          
           
        }

    } else {
	$Wpprc.coord.line($i).label($i)   configure -text " "
	$Wpprc.coord.line($i).entry($i,1) configure -textvariable DUMMY
	$Wpprc.coord.line($i).entry($i,2) configure -textvariable DUMMY
	$Wpprc.coord.line($i).entry($i,3) configure -textvariable DUMMY
        $Wpprc.coord.line($i).occupy configure -state disabled
        $Wpprc.coord.line($i).copy   configure -state disabled
        $Wpprc.coord.line($i).reset  configure -state disabled 
    }

}
#---------------------------------------------------------------------------------------

} 
#                                                                proc command_IQ_display
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                             structure_per_pedes_read_coord_reset_entry        
#***************************************************************************************
#
proc structure_per_pedes_read_coord_reset_entry {IQ} {
    global RQX RQY RQZ
    set RQX($IQ) "" 
    set RQY($IQ) "" 
    set RQZ($IQ) "" 
}
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                         structure_per_pedes_read_coord_occupy_entry            
#***************************************************************************************
#
proc structure_per_pedes_read_coord_occupy_entry {IQ} {

    global RCLU RCLV RCLW RQX RQY RQZ 
    global NQCL WYCKOFFCL WYCKOFFQ
    
    if {$RQX($IQ)=="" || $RQY($IQ)=="" || $RQZ($IQ)=="" } {
        give_warning "." "WARNING \n\n specify atomic positions first \n\n "
	return
    }
    
    set ICL $IQ
    set RCLU($ICL) $RQX($IQ)
    set RCLV($ICL) $RQY($IQ)
    set RCLW($ICL) $RQZ($IQ)
    set NQCL($ICL) 1
    
    sites_occupation $IQ 1
    
}
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                           structure_per_pedes_read_coord_copy_entry          
#***************************************************************************************
#
proc structure_per_pedes_read_coord_copy_entry {IQ} {
#
#---- this procedure scans all sites following IQ until
#     a site JQ is found that is not completely specified
#     then the missing information is copied from IQ to JQ

global RWS ITOQ ZT CONC TXTT TXTT0 NQCL Wcsys NQ NLQ NCL NT NQCL
global Wcsys structure_data_complete IQ1Q IQ2Q NLOCCMAX
global NOQ ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG 
global W_sites_list RQX RQY RQZ Wocc  WYCKOFFQ NCPA 
global SPACEGROUP Wpprc   per_pedes_occupation STRUCTURE_SETUP_MODE
global list_ALPHA    
global RQX RQY RQZ NQ NOQ

    set PRC "structure_per_pedes_read_coord_copy_entry"

    set IQNEXT 0
    set flag_RQ  0
    set flag_OCC 0

    set JQ $IQ
    while {$JQ < $NQ} {
        incr JQ
#------ no position specified for site JQ
	if { $RQX($JQ)== "" } {
           if { $RQY($JQ)== "" } {
              if { $RQZ($JQ)== "" } {
                 set IQNEXT $JQ
                 set JQ $NQ
                 set flag_RQ 1
              }
           }
        } 
#------ no occupation specified for site JQ
        if {$NOQ($JQ)==0} {
           set IQNEXT $JQ
           set JQ $NQ
           set flag_OCC 1
        }
    }

    set JQ $IQNEXT

    debug $PRC "IQ $IQ   JQ $JQ   NQ $NQ"
    if {$IQNEXT!=0} {
       if {$flag_RQ == 1 } {
          set RQX($JQ) $RQX($IQ) 
          set RQY($JQ) $RQY($IQ) 
          set RQZ($JQ) $RQZ($IQ) 
       }
#
#---------------------- copy occupation information if site IQ is occupied
#
       if {$NOQ($IQ) > 0 } {
          set NOCC $NOQ($IQ)
          for {set i 1} {$i <= $NOCC} {incr i} {
            set IT $ITOQ($i,$IQ)
            set ZOCC($i)     $ZT($IT)
            set CONCOCC($i)  $CONC($IT)
            set TXTOCC($i)   $TXTT0($IT)
            set RWSOCC($i)   $RWS($IQ)
            set RWSOCCAVRG   $RWS($IQ)
            set NLOCCMAX     $NLQ($IQ)
            set NLOCC($i)    $NLQ($IQ)
          }          
        
          sites_occupation_done $JQ $NOCC 
 
       }
    }
} 
#                                          structure_per_pedes_read_coord_copy_entry END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 


#***************************************************************************************
#                           structure_per_pedes_read_coord_initialize_3D_surface
#***************************************************************************************
#
proc structure_per_pedes_read_coord_initialize_3D_surface { } {
#
#---- this procedure scans all sites following IQ until
#     a site JQ is found that is not completely specified
#     then the missing information is copied from IQ to JQ

global RWS ZT CONC TXTT TXTT0 NQCL Wcsys NQ NLQ NCL NT NQCL NOQ ITOQ
global Wcsys structure_data_complete IQ1Q IQ2Q NLOCCMAX
global ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG 
global W_sites_list Wocc  WYCKOFFQ NCPA 
global SPACEGROUP Wpprc   per_pedes_occupation STRUCTURE_SETUP_MODE
global list_ALPHA 

global NQ_bulk_L NQ_bulk_R SU_IQ SU_NAME run_geometry_task bulk_L_eq_bulk_R
global N_rep_SU_bulk_L N_rep_SU_bulk_S N_rep_SU_bulk_R 

global RBASX_3DSURF RBASY_3DSURF RBASZ_3DSURF NQ_3DSURF NOQ_3DSURF ITOQ_3DSURF
global   RQX_3DSURF   RQY_3DSURF   RQZ_3DSURF
global NT_3DSURF ZT_3DSURF TXTT_3DSURF CONC_3DSURF 
global IQ_3DSURF_IQ RWS_3DSURF NLQ_3DSURF
global ZT_3DSURF CONC_3DSURF TABCHSYM

set PRC "structure_per_pedes_read_coord_initialize_3D_surface"

set IQ 0

for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
   for {set I 1} {$I <= $N_rep_SU_bulk_L} {incr I} {
      incr IQ   
      set NOQ($IQ) $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
   for {set I 1} {$I <= $N_rep_SU_bulk_S} {incr I} {
      incr IQ   
      set NOQ($IQ) $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
   for {set I 1} {$I <= $N_rep_SU_bulk_R} {incr I} {
      incr IQ   
      set NOQ($IQ) $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

for {set IQ 1} {$IQ <= [expr $NQ - $NQ_bulk_R] } {incr IQ} {
   set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
   debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"

   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      set ZOCC($IO)       $ZT_3DSURF($IT)
      set CONCOCC($IO)  $CONC_3DSURF($IT)
      set TXTOCC($IO)   $TABCHSYM($ZT_3DSURF($IT))

      set RWSOCC($IO)   $RWS_3DSURF($IQ_3DSURF)
      set RWSOCCAVRG    $RWS_3DSURF($IQ_3DSURF)
      set NLOCCMAX      $NLQ_3DSURF($IQ_3DSURF)
      set NLOCC($IO)    $NLQ_3DSURF($IQ_3DSURF)

      set CONCOCC($IO)  1
   }

   sites_occupation_done $IQ $NOQ($IQ)
}

#--------------------------------------------- deal with vac(R)
#
for {set IQ [expr $NQ - $NQ_bulk_R + 1]} {$IQ <= $NQ } {incr IQ} {
   set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
   debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"

   set NOQ($IQ) 1
   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ($IO,$IQ) 
      set ZOCC($IO)     0
      set CONCOCC($IO)  1.0
      set TXTOCC($IO)   Vc
      set RWSOCC($IO)   $RWS_3DSURF($IQ_3DSURF)
      set RWSOCCAVRG    $RWS_3DSURF($IQ_3DSURF)
      set NLOCCMAX      $NLQ_3DSURF($IQ_3DSURF)
      set NLOCC($IO)    $NLQ_3DSURF($IQ_3DSURF)
   }

   sites_occupation_done $IQ $NOQ($IQ)

   if {[winfo exists $Wpprc.coord.line($IQ).occupy]} {
      $Wpprc.coord.line($IQ).occupy configure -state disabled
      $Wpprc.coord.line($IQ).copy   configure -state disabled
   }
}

#--------------------------------------------- deal with bulk(L)
#
if {$bulk_L_eq_bulk_R =="YES"} {
  for {set IQ 1 } {$IQ <= $NQ_bulk_L} {incr IQ} {
     set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
     debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"
     set NOQ($IQ) 1
     for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
        set ZOCC($IO)     0
        set CONCOCC($IO)  1.0
        set TXTOCC($IO)   Vc

        set RWSOCC($IO)   $RWS_3DSURF($IQ_3DSURF)
        set RWSOCCAVRG    $RWS_3DSURF($IQ_3DSURF)
        set NLOCCMAX      $NLQ_3DSURF($IQ_3DSURF)
        set NLOCC($IO)    $NLQ_3DSURF($IQ_3DSURF)
     }

     sites_occupation_done $IQ $NOQ($IQ)

     if {[winfo exists $Wpprc.coord.line($IQ).occupy]} {
        $Wpprc.coord.line($IQ).occupy configure -state disabled
        $Wpprc.coord.line($IQ).copy   configure -state disabled
     }

  }
}

write_actual_lattice_info $PRC

} 
#                               structure_per_pedes_read_coord_initialize_3D_surface END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 





#***************************************************************************************
#                           structure_per_pedes_read_coord_vacuum_starts
#***************************************************************************************
#
proc structure_per_pedes_read_coord_vacuum_starts {IQ_VAC_STARTS} {
#
#---- this procedure sets the occupation of a site IQ to vacuum
#

global RWS ITOQ ZT CONC TXTT TXTT0 NQCL Wcsys NQ NLQ NCL NT NQCL NQ_bulk_L
global Wcsys structure_data_complete IQ1Q IQ2Q NLOCCMAX
global NOQ ZOCC TXTOCC RWSOCC OK_OCC CONCOCC RWSOCCAVRG 
global W_sites_list RQX RQY RQZ Wocc  WYCKOFFQ NCPA 
global SPACEGROUP Wpprc per_pedes_occupation STRUCTURE_SETUP_MODE
global list_ALPHA  bulk_L_eq_bulk_R
global RQX RQY RQZ NQ NOQ IQ_3DSURF_IQ RWS_3DSURF NLQ_3DSURF

set PRC "structure_per_pedes_read_coord_vacuum_starts"

if { $IQ_VAC_STARTS <= $NQ_bulk_L } { 
   set IQ_VAC_BOT 1
   set IQ_VAC_TOP $NQ_bulk_L
} else {
   set IQ_VAC_BOT $IQ_VAC_STARTS
   set IQ_VAC_TOP $NQ
}

#--------------------------------------------- deal with vac starting at IQ_VAC_STARTS 
#
for {set IQ $IQ_VAC_BOT} {$IQ <= $IQ_VAC_TOP } {incr IQ} {
   set NOQ($IQ) 1
   set IO 1
   set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
   debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"

   set ZOCC($IO)     0
   set CONCOCC($IO)  1.0
   set TXTOCC($IO)   Vc
   set RWSOCC($IO)   $RWS_3DSURF($IQ_3DSURF)
   set RWSOCCAVRG    $RWS_3DSURF($IQ_3DSURF)
   set NLOCCMAX      $NLQ_3DSURF($IQ_3DSURF)
   set NLOCC($IO)    $NLQ_3DSURF($IQ_3DSURF)

   sites_occupation_done $IQ $NOQ($IQ)
}

#-------------------------------------------------------------------------- deal with VIV
#
if {$bulk_L_eq_bulk_R == "YES" } {
   set IQ_VAC_BOT 1
   set IQ_VAC_TOP [expr $NQ - $IQ_VAC_STARTS + 1]

for {set IQ $IQ_VAC_BOT} {$IQ <= $IQ_VAC_TOP } {incr IQ} {
   set NOQ($IQ) 1
   set IO 1
   set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
   debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"

   set ZOCC($IO)     0
   set CONCOCC($IO)  1.0
   set TXTOCC($IO)   Vc
   set RWSOCC($IO)   $RWS_3DSURF($IQ_3DSURF)
   set RWSOCCAVRG    $RWS_3DSURF($IQ_3DSURF)
   set NLOCCMAX      $NLQ_3DSURF($IQ_3DSURF)
   set NLOCC($IO)    $NLQ_3DSURF($IQ_3DSURF)

   sites_occupation_done $IQ $NOQ($IQ)
}

}


} 
#                                       structure_per_pedes_read_coord_vacuum_starts END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 

#***************************************************************************************
#                               run_findsym      
#***************************************************************************************
#
proc run_findsym {next_command} {

    set PRC "run_findsym"

    global CHECK_TABLE CHECK_FIL_FS
    global LATPAR LATANG BOA COA IT SPACEGROUP SPACEGROUP_AP international
    global system NCL RCLU RCLV RCLW RBASX RBASY RBASZ
    global iprint Wcsys xband_path NQCL  NT NLQ RWS IQECL NQ_bulk_L NQ_bulk_R
    global RQX RQY RQZ NQ RBASX RBASY RBASZ MAG_DIR
    global NCL NQCL SYMMETRY NQEQ ICLQ WYCKOFFQ WYCKOFFCL TXTT TXTT0 ZT
    global ITOQ NOQ CONC  per_pedes_occupation STRUCTURE_SETUP_MODE
    global IQECL IQ1Q IQ2Q IQ0_Q ZRANGE_TYPE present_SUBSYSTEM
    global TABLATANG_alpha TABLATANG_beta TABLATANG_gamma
    global TABLATPAR_a TABLATPAR_b TABLATPAR_c TABBRAVAIS BRAVAIS
    global RQU RQV RQW
    global xband_version

    set  MAG_DIR(1) 0
    set  MAG_DIR(2) 0
    set  MAG_DIR(3) 0

    set tcl_precision 17

    set errorflag 0

#----------------------------------- save original number of site classes
#----------------------------- because these may change due to magnetisation
#
set NCL0 $NCL
set NQ0  $NQ
set sum 0
for {set ICL 1} {$ICL <= $NCL0} {incr ICL} {
   set NQCL0($ICL)      $NQCL($ICL)
   set sum [expr $sum + $NQCL($ICL)]
   set WYCKOFFCL0($ICL) $WYCKOFFCL($ICL)
}
if {$NQ!=$sum} {debug $PRC "TROUBLE with NQ=$NQ  <> sum=$sum"}


if {$STRUCTURE_SETUP_MODE != "PER PEDES"    &&  \
    $STRUCTURE_SETUP_MODE != "MULTI LAYERS" && \
    $STRUCTURE_SETUP_MODE != "2D LAYERS"    && \
    $STRUCTURE_SETUP_MODE != "3D SURFACE" } {
#=======================================================================================
#                                                                the SPACEGROUP is known

set strdat [open "struc.inp" w]

set str_key [expr 10000 + $SPACEGROUP_AP]

puts $strdat "$str_key    SPACEGROUP_AP "
set aux [format "%18.12f %18.12f %18.12f %18.12f  %18.12f " \
         $BOA $COA  $LATANG(1) $LATANG(2) $LATANG(3) ]
puts $strdat "$aux"
puts $strdat "$NCL     NCL "

for {set ICL 1} {$ICL <= $NCL} {incr ICL} {

   set coord [format "%18.12f %18.12f %18.12f  tauX  tauY  tauZ    ICL:%3i   NQCL:%3i " \
                $RCLU($ICL) $RCLV($ICL) $RCLW($ICL)  $ICL $NQCL($ICL)]

   puts $strdat "$coord "
##  puts $strdat "$coord  tauX  tauY  tauZ  ICL: $ICL    NQCL: $NQCL($ICL)  "

}

puts $strdat " $MAG_DIR(1) $MAG_DIR(2) $MAG_DIR(3)    magnetization direction"

close $strdat

#=======================================================================================
#                                                              the SPACEGROUP is UNKNOWN
} else {

#------------------------ introduce first names   ISITE  for sites to identify atom types
set FLAG 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} { set KDONE($IQ)  0 }

debug $PRC "  "
debug $PRC " ISITE   IQ   NOQ    OCCUPATION "
set ISITE 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   if { $KDONE($IQ) != 1 } {
       set KDONE($IQ) 1
       incr ISITE
       set SITE_NAMEQ($IQ)  $ISITE

       set NOQ_SITE($ISITE) $NOQ($IQ)
       set NLQ_SITE($ISITE) $NLQ($IQ)
       set RWS_SITE($ISITE) $RWS($IQ)
       set TXT ""
       for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
          set IT $ITOQ($IO,$IQ)
          set TXTT_SITE($IO,$ISITE) $TXTT0($IT)
          set   ZT_SITE($IO,$ISITE)    $ZT($IT)
          set CONC_SITE($IO,$ISITE)  $CONC($IT)
          append TXT "$TXTT_SITE($IO,$ISITE) $ZT_SITE($IO,$ISITE) \
                      $CONC_SITE($IO,$ISITE)   "
       }
       set aux [format "%5i   %3i   %3i  " \
		$ISITE $IQ $NOQ_SITE($ISITE)]
       debug $PRC "$aux    $TXT"

#----------------------------- copy data to arrays ***_SITE_0

       set NOQ_SITE_0($ISITE) $NOQ_SITE($ISITE)
       set NLQ_SITE_0($ISITE) $NLQ_SITE($ISITE)
       set RWS_SITE_0($ISITE) $RWS_SITE($ISITE)
       for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
          set IT $ITOQ($IO,$IQ)
          set TXTT_SITE_0($IO,$ISITE) $TXTT_SITE($IO,$ISITE)
          set   ZT_SITE_0($IO,$ISITE)   $ZT_SITE($IO,$ISITE)
          set CONC_SITE_0($IO,$ISITE) $CONC_SITE($IO,$ISITE)
       }

       for {set JQ [expr $IQ + 1]} {$JQ <= $NQ} {incr JQ} {
          if { $NOQ($IQ)==$NOQ($JQ) } {
             set same 1
             for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
                set IT $ITOQ($IO,$IQ)
                set JT $ITOQ($IO,$JQ)
		 if {$TXTT0($IT)!=$TXTT0($JT)} {set same 0}
		 if {$ZT($IT)   !=$ZT($JT)   } {set same 0}
		 if {$CONC($IT) !=$CONC($JT) } {set same 0}
             }

             if {$same==1} {
                set KDONE($JQ) 1
                set SITE_NAMEQ($JQ) $SITE_NAMEQ($IQ)
             }
          }
       }
   }
}



#---------------------------------------------------------------------------------------
#                                       force L- and R-bulk to be inequivalent to spacer

if {$STRUCTURE_SETUP_MODE == "2D LAYERS"    || \
    $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
   if { $ZRANGE_TYPE=="extended" && $present_SUBSYSTEM=="spacer"} {

      for {set IQ 1} {$IQ <= $NQ_bulk_L} {incr IQ} {

	 set ISITE_OLD $SITE_NAMEQ($IQ)
	 set ISITE_NEW [expr $SITE_NAMEQ($IQ) + 1000]

         set NOQ_SITE($ISITE_NEW) $NOQ_SITE_0($ISITE_OLD)
         set NLQ_SITE($ISITE_NEW) $NLQ_SITE_0($ISITE_OLD)
         set RWS_SITE($ISITE_NEW) $RWS_SITE_0($ISITE_OLD)
         for {set IO 1} {$IO <= $NOQ_SITE($ISITE_NEW)} {incr IO} {
            set TXTT_SITE($IO,$ISITE_NEW) $TXTT_SITE_0($IO,$ISITE_OLD)
            set   ZT_SITE($IO,$ISITE_NEW)   $ZT_SITE_0($IO,$ISITE_OLD)
            set CONC_SITE($IO,$ISITE_NEW) $CONC_SITE_0($IO,$ISITE_OLD)
         }

	 set SITE_NAMEQ($IQ) $ISITE_NEW
      }

      for {set IQ [expr $NQ_bulk_L +1]} {$IQ <= [expr $NQ-$NQ_bulk_R]} {incr IQ} {

	 set ISITE_OLD $SITE_NAMEQ($IQ)
	 set ISITE_NEW [expr $SITE_NAMEQ($IQ) + 2000]

         set NOQ_SITE($ISITE_NEW) $NOQ_SITE_0($ISITE_OLD)
         set NLQ_SITE($ISITE_NEW) $NLQ_SITE_0($ISITE_OLD)
         set RWS_SITE($ISITE_NEW) $RWS_SITE_0($ISITE_OLD)
         for {set IO 1} {$IO <= $NOQ_SITE($ISITE_NEW)} {incr IO} {
            set TXTT_SITE($IO,$ISITE_NEW) $TXTT_SITE_0($IO,$ISITE_OLD)
            set   ZT_SITE($IO,$ISITE_NEW)   $ZT_SITE_0($IO,$ISITE_OLD)
            set CONC_SITE($IO,$ISITE_NEW) $CONC_SITE_0($IO,$ISITE_OLD)
         }

	 set SITE_NAMEQ($IQ) $ISITE_NEW
      }

      for {set IQ [expr $NQ-$NQ_bulk_R+1]} {$IQ <= [expr $NQ]} {incr IQ} {

	 set ISITE_OLD $SITE_NAMEQ($IQ)
	 set ISITE_NEW [expr $SITE_NAMEQ($IQ) + 3000]

         set NOQ_SITE($ISITE_NEW) $NOQ_SITE_0($ISITE_OLD)
         set NLQ_SITE($ISITE_NEW) $NLQ_SITE_0($ISITE_OLD)
         set RWS_SITE($ISITE_NEW) $RWS_SITE_0($ISITE_OLD)
         for {set IO 1} {$IO <= $NOQ_SITE($ISITE_NEW)} {incr IO} {
            set TXTT_SITE($IO,$ISITE_NEW) $TXTT_SITE_0($IO,$ISITE_OLD)
            set   ZT_SITE($IO,$ISITE_NEW)   $ZT_SITE_0($IO,$ISITE_OLD)
            set CONC_SITE($IO,$ISITE_NEW) $CONC_SITE_0($IO,$ISITE_OLD)
         }

	 set SITE_NAMEQ($IQ) $ISITE_NEW
      }

   }
}

#------------------------------------------------------ write the input file for findsym
#
debug $PRC "the SPACEGROUP is UNKNOWN "

set strdat [open "struc.inp" w]

puts $strdat "0            /  the SPACEGROUP is UNKNOWN"

for {set i 1} {$i <= 3} {incr i} {
   puts $strdat [format "%18.12f %18.12f %18.12f " $RBASX($i) $RBASY($i) $RBASZ($i) ]
}

puts $strdat "$NQ     NQ "
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

   set coord [format "%18.12f %18.12f %18.12f '%3s' / " $RQX($IQ) $RQY($IQ) $RQZ($IQ) $SITE_NAMEQ($IQ) ]

   puts $strdat "$coord          $IQ  tauX  tauY  tauZ   "
}

puts $strdat " $MAG_DIR(1) $MAG_DIR(2) $MAG_DIR(3)    magnetization direction"

close  $strdat
}
#=======================================================================================


debug $PRC "running $SYMMETRY   for STRUCTURE_SETUP_MODE = $STRUCTURE_SETUP_MODE "

          exec $SYMMETRY > findsym.out

## execunixcmd "cat struc.inp struc.out"

if {$CHECK_TABLE!=0} {
   set cfil  check_findsym_$CHECK_TABLE
   exec echo "---------------------------------------------------------------" >> $cfil
   exec cat struc.inp >> $cfil						     
   exec echo "---------------------------------------------------------------" >> $cfil
   exec cat struc.out >> $cfil						     
   exec echo "---------------------------------------------------------------" >> $cfil
   exec cat findsym.out >> $cfil
   set CHECK_FIL_FS [open $cfil a]
}


#---------------------------------------------------------------------------------------
#eval set res [catch "exec grep \"Strange... very strange\" findsym.out | wc -l " message]
catch {exec grep -c "Strange... very strange" findsym.out} message

if {[lindex $message 0]!=0} {
   debug $PRC "findsym gave STRANGE result "
   incr errorflag
   debug $PRC "ERROR FLAG: $errorflag     SG: $SPACEGROUP $SPACEGROUP_AP"
   give_warning "." "WARNING \n\n from <run_findsym> \n\n findsym gave STRANGE result
                                                     \n\n see output "
   execunixcmd "cat findsym.out "
   return
}
#---------------------------------------------------------------------------------------
#eval set res [catch "exec grep -i \"warning\" findsym.out | wc -l " message]
catch {exec grep -ic "warning" findsym.out} message

if {[lindex $message 0]!=0} {
   debug $PRC "findsym gave WARNING"
   incr errorflag
   debug $PRC "ERROR FLAG: $errorflag     SG: $SPACEGROUP $SPACEGROUP_AP"
   give_warning "." "WARNING \n\n from <run_findsym> \n\n findsym gave WARNING
                                                     \n\n see output "
   execunixcmd "cat findsym.out "
}
#---------------------------------------------------------------------------------------

set OK false
set loop 0

while { $OK == "false" } {

incr loop

set strdat [open "struc.out" r]

#gets $strdat line
#set version [lindex $line 2]
#if {$version>$xband_version} {
#   give_warning "." "ERROR MESSAGE \n\n from <run_findsym> \n
#                     \n findsym requires xband version $version or higher \n
#                     \n update your  xband  installation"
#   exit 
#}

gets $strdat line
debug $PRC "struc.out:   $line"

#------------------------------------------------------------------>  read basis vectors
for {set BV 1} {$BV <= 3} {incr BV} {
   gets $strdat line
   set RBASX($BV) [lindex $line 0 ]
   set RBASY($BV) [lindex $line 1 ]
   set RBASZ($BV) [lindex $line 2 ]
   debug $PRC "from struc.out basis vector $BV:  $RBASX($BV) $RBASY($BV) $RBASZ($BV)"
}

#------------------------------------------- skip info on magnetisation direction
if { $MAG_DIR(1)!=0 || $MAG_DIR(2)!=0 || $MAG_DIR(3)!=0 } {
   for {set i 5} {$i <= 7} {incr i} { gets $strdat line }
}

gets $strdat line
if { [string length $line] > 0 } {

   set NCL   [lindex $line 0 ]
   set NQ    [lindex $line 1 ]

   for {set ICL 1} {$ICL <= $NCL} {incr ICL} {set NQCL($ICL) 0 }

   debug $PRC "reading struc.out:  NCL=$NCL   NQ=$NQ  <---------  NQ0=$NQ0"

   for {set i 1} {$i <= $NQ} {incr i} {

      gets $strdat line
#
#---------------------------     IQ0_Q($IQ)  informs on  original order of sites
#
      set IQ $i
      set RQX($IQ)     [lindex $line 0 ]
      set RQY($IQ)     [lindex $line 1 ]
      set RQZ($IQ)     [lindex $line 2 ]
      set ICL          [lindex $line 3 ]
      set IQ0_Q($IQ)   [lindex $line 4 ]
      set ISITE_Q($IQ) [lindex $line 5 ]

      incr NQCL($ICL)

      debug $PRC "reading struc.out: IQ=$IQ ICL=$ICL"
      set ICLQ($IQ) $ICL

      set WYCKOFFQ($IQ) "-"
      set WYCKOFFCL0($IQ) "-"
      set WYCKOFFCL($IQ)  "-"

   }

   gets $strdat line
}

#
#----------------------------------------------------- get cryst coordinates RQU RQV RQW
#

convert_basis_vectors_RQ cart_to_cryst

#
#---------------------------------------------------- shift lattice sites into unit cell
#

shift_lattice_sites

if { [string first "Point symmetry operations for reciprocal sums" $line] != -1 } { set OK true}

debug $PRC "reading struc.out: OK=$OK"

if {$CHECK_TABLE!=0} {puts $CHECK_FIL_FS "reading struc.out: OK=$OK"}

if {$OK != "true"} {

   debug $PRC "reading struc.out failed     SG: $SPACEGROUP $SPACEGROUP_AP"
   incr errorflag
   debug $PRC "ERROR FLAG: $errorflag     SG: $SPACEGROUP $SPACEGROUP_AP"
   give_warning "." "WARNING \n\n from <run_findsym> \n\n
           reading struc.out failed   "
   return
} ; # end while

close $strdat
if [file exists findsym.out] {exec rm findsym.out}

#------------------------------------------------- set up proper table of atomic types
if {$STRUCTURE_SETUP_MODE == "PER PEDES"     ||  \
    $STRUCTURE_SETUP_MODE == "MULTI LAYERS"  ||  \
    $STRUCTURE_SETUP_MODE == "2D LAYERS"     ||  \
    $STRUCTURE_SETUP_MODE == "3D SURFACE" } {

#  NOTE: the following is based on the assumption that the symmetry program
#        lists all equivalent sites IQ of a class ICL without an inequivalent
#        site in between

    debug $PRC "STRUCTURE_SETUP_MODE = $STRUCTURE_SETUP_MODE"
    set NT 0
    set IQ0  0
    set ICL0 NIX

    debug $PRC "  "
    debug $PRC "   new list of occupation and types "
    debug $PRC "  "
    debug $PRC "   IQ   ISITE   ICL  NQCL   NOQ    IT "

    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
       set ISITE [string trim $ISITE_Q($IQ)]
       set NOQ($IQ) $NOQ_SITE($ISITE)
       set NLQ($IQ) $NLQ_SITE($ISITE)
       set RWS($IQ) $RWS_SITE($ISITE)
       set ICL      $ICLQ($IQ)
       if {$ICL!=$ICL0} {
          for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
             incr NT
             set  TXTT($NT) $TXTT_SITE($IO,$ISITE)
             set TXTT0($NT) $TXTT_SITE($IO,$ISITE)
             set    ZT($NT)   $ZT_SITE($IO,$ISITE)
             set  CONC($NT) $CONC_SITE($IO,$ISITE)
             set ITOQ($IO,$IQ) $NT
	  }
          set IQ0  $IQ
          set ICL0 $ICL
       } else {
          for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
             set ITOQ($IO,$IQ) $ITOQ($IO,$IQ0)
	  }
       }

       set TXT ""
       for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
           set IT $ITOQ($IO,$IQ)
           append TXT "$IT  $TXTT0($IT) $ZT($NT) $CONC($IT)   "
       }
	
       set aux [format "  %3i   %5i   %3i   %3i   %3i " \
		$IQ  $ISITE $ICL $NQCL($ICLQ($IQ))  $NOQ($IQ)]
       debug $PRC "$aux    $TXT"
    }

    debug $PRC "  "
    debug $PRC "  ICL  NQCL IQECL =  IQ   IQ1Q     IQ2Q"
    set IQ 0
    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
       set IQ1 [expr $IQ + 1]
       set IQ2 [expr $IQ + $NQCL($ICL)]
       for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
          incr IQ
          set IQ1Q($IQ) $IQ1
          set IQ2Q($IQ) $IQ2
          set IQECL($IE,$ICL) $IQ
          set aux [format "  %3i   %3i   %3i   %3i   %3i  ... %3i  " \
		       $ICL $NQCL($ICL) $IQECL($IE,$ICL) $IQ  $IQ1Q($IQ) $IQ2Q($IQ) ]
          debug $PRC $aux
       }
    }

}

#
#-------------------------------------------------------- table of equivalent sites
#
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set ICL $ICLQ($IQ)
   set NQEQ($IQ) 0
   for {set JQ 1} {$JQ <= $NQ} {incr JQ} {
      set JCL $ICLQ($JQ)
      if { $ICL == $JCL } { incr NQEQ($IQ) }
   }
}


#
#------------------- check consistency and  write the WYCKOFF symbols in proper order
#
debug $PRC "NQ0   $NQ0    NQ $NQ "
set NN [expr $NQ0 / $NQ]
if {$NN==0} {set NN 1;
             set TXT "NQ0 (ITXC-table) < NQ (findsym)"
             incr errorflag
             give_warning "." "WARNING \n\n from <run_findsym> \n\n $TXT"
             debug $PRC "$TXT   OK=false"
             if {$CHECK_TABLE!=0} {puts $CHECK_FIL_FS "$TXT   OK=false"}
             }

set key [string range $international 0 0]
if { $key=="P" } {
   set NQRAT 1
} elseif { $key=="F" } {
   set NQRAT 4
} elseif { $key=="I" } {
   set NQRAT 2
} elseif { $key=="A" } {
   set NQRAT 2
} elseif { $key=="C" } {
   set NQRAT 2
} elseif { $key=="R" } {
   if {[string last ":h" $international] > 0} {
      set NQRAT 3
   } elseif {[string last ":r" $international] > 0} {
      set NQRAT 1
   } else {
     set NQRAT 0
   }
} else {
   set NQRAT 0
}

if {$STRUCTURE_SETUP_MODE == "PER PEDES"    || \
    $STRUCTURE_SETUP_MODE == "MULTI LAYERS" || \
    $STRUCTURE_SETUP_MODE == "2D LAYERS"    ||  \
    $STRUCTURE_SETUP_MODE == "3D SURFACE"  } {
      set NQRAT 1
      set NCL0  $NCL
      for {set ICL 1} {$ICL <= $NCL0} {incr ICL} {set NQCL0($ICL) $NQCL($ICL)}
}


if {$NQRAT!=0} {
    if {$NN!=$NQRAT} {
         incr errorflag
         set TXT "NQ-reduction: NN=$NN != $NQRAT=NQRAT for SG-type $key"
         give_warning "." "WARNING \n\n from <run_findsym> \n\n $TXT"
         debug $PRC "$TXT   OK=false"
         if {$CHECK_TABLE!=0} {puts $CHECK_FIL_FS "$TXT   OK=false"}
    }
}

debug $PRC "NQ-reduction:  NN: $NN   NQRAT: $NQRAT   for SG-type $key"

set IQ 0
for {set ICL 1} {$ICL <= $NCL0} {incr ICL} {
   set NQCL0_red [expr $NQCL0($ICL) / $NN]
   debug $PRC "ICL $ICL  $NQCL0_red  $NQCL($ICL)"
   if {$NQCL0_red !=  $NQCL($ICL) } {
      incr errorflag
      set TXT "ICL: $ICL  $NQCL0_red=NQCL0/NQRAT != $NQCL($ICL)=NQCL(findsym)"
      debug $PRC "$TXT   OK=false"
      if {$CHECK_TABLE!=0} {puts $CHECK_FIL_FS "$TXT   OK=false"}
   }

   for {set I 1} {$I <= $NQCL0_red} {incr I} {
      incr IQ
      set WYCKOFFQ($IQ) $WYCKOFFCL0($ICL)
   }
}

if {$IQ !=$NQ} {
      incr errorflag
      set TXT "sum IQ = $IQ  !=  $NQ (findsym)"
      give_warning "." "WARNING \n\n from <run_findsym> \n\n $TXT  \n\n \
      WYCKOFF symbols not set properly  "
      debug $PRC "$TXT   OK=false"
      if {$CHECK_TABLE!=0} {puts $CHECK_FIL_FS "$TXT   OK=false"}
}

}


#---------------------------------------------------------------------------------------
if {$BRAVAIS>0} {
set TXT "  for  $TABBRAVAIS($BRAVAIS)   ($BRAVAIS)   OK=false"
if {$TABLATPAR_b($BRAVAIS) ==1} {
   if {$BOA!=1.0} {debug $PRC "BOA should be 1 $TXT"; incr errorflag }
}
if {$TABLATPAR_c($BRAVAIS) ==1} {
   if {$COA!=1.0} {debug $PRC "COA should be 1 $TXT"; incr errorflag }
 }
if {$TABLATANG_alpha($BRAVAIS) ==90} {
   if {$LATANG(1)!=90.0} {debug $PRC "ALPHA should be 90 $TXT"; incr errorflag }
}
if {$TABLATANG_beta($BRAVAIS) ==90} {
   if {$LATANG(1)!=90.0} {debug $PRC "ALPHA should be 90 $TXT"; incr errorflag }
}
if {$TABLATANG_gamma($BRAVAIS) ==90} {
   if {$LATANG(1)!=90.0} {debug $PRC "ALPHA should be 90 $TXT"; incr errorflag }
}
}
#---------------------------------------------------------------------------------------

debug $PRC "ERROR FLAG: $errorflag     SG: $SPACEGROUP $SPACEGROUP_AP"
if {$CHECK_TABLE!=0} {
   puts  $CHECK_FIL_FS "ERROR FLAG: $errorflag     SG: $SPACEGROUP $SPACEGROUP_AP"
   close $CHECK_FIL_FS
}

write_actual_lattice_info $PRC
#---------------------------- execute the next command depending on STRUCTURE_SETUP_MODE
#
$next_command

}
#                                                                        run_findsym END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                           structure_via_table_run_findsym           
#***************************************************************************************
#
proc structure_via_table_run_findsym  { } {

    set PRC "dummy"
    debug $PRC "------------------------------>  run_findsym  DONE"

}
#                                                    structure_via_table_run_findsym END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                         structure_set_lattice_parameter            
#***************************************************************************************
#                                                        
proc structure_set_lattice_parameter { struc } {
    global COLOR WIDTH HEIGHT FONT
    global SPACEGROUP SPACEGROUP_AP TABCHSYM iprint
    global system NQCL ALAT BOHRRAD
    global TABLATPAR_a TABLATPAR_b TABLATPAR_c BOA COA LATPAR
    global BRAVAIS LATANG STRUCTURE_SETUP_MODE
    global TABLATANG_alpha TABLATANG_beta TABLATANG_gamma TABBRAVAIS
    global CHECK_TABLE

    set tcl_precision 17
#    wm geometry $Latpar -20+0

    set Latpar .latpar
    set win .latpar

    toplevel_init $Latpar "set lattice parameter" 0 0

    wm positionfrom $Latpar user
    wm sizefrom $Latpar ""
    wm minsize $Latpar 100 100
    wm geometry $Latpar +400+300

    frame $Latpar.first
    frame $Latpar.secon
    frame $Latpar.third

    set widlab  5
    set wident 15

    for {set i 1} {$i <=3} {incr i} { set LATPAR($i) "" }
    set BOA ""
    set COA ""
    label $Latpar.info -text \
	    "\n Bravais lattice:  $TABBRAVAIS(${BRAVAIS}) \n\n \
                supply the lattice parameters and confirm with   <RETURN>   or   \[CONFIRM\]\n "

#-------------------------------------------------------

     button $Latpar.confirm -text "CONFIRM" \
            -width 25 -height 3 -bg $COLOR(GOON) \
            -command "structure_set_lattice_parameter_abcabg 0"

#-------------------------------------------------------

    label $Latpar.first.label1 -width $widlab -text "\   a " -pady 5
    label $Latpar.first.label2 -width $widlab -text "\   b " -pady 5
    label $Latpar.first.label3 -width $widlab -text "\   c " -pady 5

    label $Latpar.secon.label1 -width $widlab -text "\     " -pady 5
    label $Latpar.secon.label2 -width $widlab -text "\ b/a " -pady 5
    label $Latpar.secon.label3 -width $widlab -text "\ c/a " -pady 5
				
    label $Latpar.third.label1 -width $widlab -font $FONT(SBL) -text "\   a " -pady 5
    label $Latpar.third.label2 -width $widlab -font $FONT(SBL) -text "\   b " -pady 5
    label $Latpar.third.label3 -width $widlab -font $FONT(SBL) -text "\   g " -pady 5

    entry $Latpar.secon.a(2) -width $wident -relief sunken -textvariable BOA\
          -fg $COLOR(ENTRYFG) -bg $COLOR(ENTRY) -font $FONT(GEN) -highlightthickness 0
    entry $Latpar.secon.a(3) -width $wident -relief sunken -textvariable COA\
          -fg $COLOR(ENTRYFG) -bg $COLOR(ENTRY) -font $FONT(GEN) -highlightthickness 0

    for {set i 1} {$i <=3} {incr i} {
	entry $Latpar.first.a($i) -width $wident -relief sunken\
                      -textvariable LATPAR($i) -bg $COLOR(ENTRY)\
                      -fg $COLOR(ENTRYFG) -font $FONT(GEN) -highlightthickness 0
	entry $Latpar.third.a($i) -width $wident -relief sunken\
                      -textvariable LATANG($i) -bg $COLOR(ENTRY)\
                      -fg $COLOR(ENTRYFG) -font $FONT(GEN) -highlightthickness 0
        bind  $Latpar.first.a($i) <Key-Return> "structure_set_lattice_parameter_abcabg $i"
        bind  $Latpar.third.a($i) <Key-Return> "structure_set_lattice_parameter_abcabg [expr $i + 5]"

        if {$i !=1} {
        bind  $Latpar.secon.a($i) <Key-Return> "structure_set_lattice_parameter_abcabg [expr $i + 2]"
        } else {
	entry $Latpar.secon.a(1) -width $wident -relief flat -font $FONT(GEN)\
                    -fg $COLOR(ENTRYFG) -highlightthickness 0 -state disabled
        }

        pack $Latpar.first.label$i  $Latpar.first.a($i) -side left
        pack $Latpar.secon.label$i  $Latpar.secon.a($i) -side left
        pack $Latpar.third.label$i  $Latpar.third.a($i) -side left
    }

    button $Latpar.convert -height 3 \
     -text "convert lattice parameters from Angstroem to a.u. \n if necessary " \
     -bg $COLOR(BUT2) -command "structure_set_lattice_parameter_convert $Latpar.convert " \
     -state disabled

#-------------------------------------------------------
    proc structure_set_lattice_parameter_convert { w } {
	global LATPAR BOHRRAD
	
	set tcl_precision 17
	
	for {set i 1} {$i <=3} {incr i} { set LATPAR($i) [expr $LATPAR($i) / $BOHRRAD] }
	$w  configure -state disabled
    }
#-------------------------------------------------------

     button $Latpar.go_on -text "input OK --- GO ON" \
            -width 25 -height 3 -bg $COLOR(GOON) \
            -command "structure_set_lattice_parameter_check_input  $struc $Latpar "  \
            -state disabled

#-------------------------------------------------------
    proc structure_set_lattice_parameter_check_input {struc w} {
	global  LATPAR LATANG

	set flag 0
	for {set i 1} {$i <=3} {incr i} {
	    if {[string trim $LATPAR($i)]=="" } {set flag 1}
	    if {[string trim $LATANG($i)]=="" } {set flag 1}
	    if {[string trim $LATPAR($i)]=="0" } {set flag 1}
	    if {[string trim $LATANG($i)]=="0" } {set flag 1}
	}
	
	if {$flag == 1} {
	    give_warning "." "WARNING \n\n complete input  \n\n "
	    return
	}
	
	if {$struc!="per_pedes"} {
	    structure_via_table_show $struc
	} else {
	    structure_per_pedes_read_coord use_lattice_parameters
	}
	destroy $w
    }
#-------------------------------------------------------

     button $Latpar.close -text "CLOSE" \
                  -width 25 -height 3 -bg $COLOR(CLOSE) \
            -command "destroy $Latpar"

#-------------------------------------------------------

     pack $Latpar.info $Latpar.confirm $Latpar.first $Latpar.secon $Latpar.third  \
	  $Latpar.convert $Latpar.go_on $Latpar.close -fill both

#-------------------------------------------------------

     if {$STRUCTURE_SETUP_MODE == "PER PEDES" && $BRAVAIS==0} {

         button $Latpar.skip -text "SKIP -- supply primitive vectors instead" \
                -width 25 -height 3 -bg $COLOR(GOON) \
                -command " destroy $Latpar ; \
                           structure_per_pedes_read_coord use_primitive_vectors"

         pack $Latpar.skip -fill both

     }

#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
#                   for checking:   set dummy values for parameters not fixed by default
#
if {$CHECK_TABLE!=0} {
    for {set i 1} {$i <=3} {incr i} {
      if {[info exists $LATPAR($i)]==0 } {set LATPAR($i) 0}
      if {[string trim $LATPAR($i)]==""||[string trim $LATPAR($i)]=="0"} {
	   if { $i==1} {
              set  LATPAR($i) 4.44
           } elseif { $i==2} {
              set  LATPAR($i) 6.66
           } else {
              set  LATPAR($i) 7.77
           }
           $Latpar.first.a($i) configure -textvariable $LATPAR($i)
           structure_set_lattice_parameter_abcabg $i
       }
    }

    for {set i 1} {$i <=3} {incr i} {
      if {[info exists $LATANG($i)]==0 } {set LATANG($i) 0}
      if {[string trim $LATANG($i)]==""||[string trim $LATANG($i)]=="0"} {
	   if { $i==1} {
              set  LATANG($i) 66
           } elseif { $i==2} {
              set  LATANG($i) 77
           } else {
              set  LATANG($i) 88
           }
	   $Latpar.third.a($i) configure -textvariable $LATANG($i)
           structure_set_lattice_parameter_abcabg [expr $i + 5]
       }
    }

    structure_set_lattice_parameter_check_input  $struc $Latpar
}
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


}
#                                                    structure_set_lattice_parameter END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                         structure_set_lattice_parameter_abcabg            
#***************************************************************************************
#
proc structure_set_lattice_parameter_abcabg {key} {

    set PRC "structure_set_lattice_parameter_abcabg"

    global TABLATPAR_a TABLATPAR_b TABLATPAR_c BOA COA LATPAR BRAVAIS
    global LATANG TABLATANG_alpha TABLATANG_beta TABLATANG_gamma Wcsys
    global international

    set tcl_precision 17

    for {set i 1} {$i <=3} {incr i} {
	set LATPAR($i) [string trim $LATPAR($i)]
	set LATANG($i) [string trim $LATANG($i)]
    }	
    set BOA [string trim $BOA]
    set COA [string trim $COA]

    switch $key {
	{0} {
# .................................................................................
	    if {$LATPAR(1) != ""} {
               if {$TABLATPAR_b($BRAVAIS) == 1} {
                  set LATPAR(2) $LATPAR(1) ; set BOA 1.0
	       } else {
                  if {$LATPAR(2) != ""} {
                     set BOA [expr $LATPAR(2) / ($LATPAR(1)*1.0)]
                  } else {
                     if {$BOA != ""} { set LATPAR(2) [expr $LATPAR(1) * $BOA] }
                  }
               }
#
               if {$TABLATPAR_c($BRAVAIS) == 1} {
                  set LATPAR(3) $LATPAR(1) ; set COA 1.0
	       } else {
                  if {$LATPAR(3) != ""} {
                     set COA [expr $LATPAR(3) / ($LATPAR(1)*1.0)]
                  } else {
                     if {$COA != ""} { set LATPAR(3) [expr $LATPAR(1) * $COA] }
                  }
               }
               if {[string last ":h" $international] > 0} {
                  set LATPAR(2) $LATPAR(2)
                  set BOA 1.0
               }
               if {[string last ":r" $international] > 0} {
                  set LATPAR(2) $LATPAR(1)
                  set LATPAR(3) $LATPAR(1)
                  set BOA 1.0
                  set COA 1.0
               }
	    }
# .................................................................................
	    if {$LATPAR(2) != ""} {
               if {$TABLATPAR_b($BRAVAIS) == 1} {
                  set LATPAR(1) $LATPAR(2) ; set BOA 1.0
	       } else {
                  if {$LATPAR(1) != ""} {
                     set BOA [expr $LATPAR(2) / ($LATPAR(1)*1.0)]
                  } else {
                     if {$BOA != ""} { set LATPAR(1) [expr $LATPAR(2) / $BOA] }
                  }
               }
               if {[string last ":h" $international] > 0} {
                  set LATPAR(1) $LATPAR(2)
                  set BOA 1.0
               }
               if {[string last ":r" $international] > 0} {
                  set LATPAR(1) $LATPAR(2)
                  set LATPAR(3) $LATPAR(2)
                  set BOA 1.0
                  set COA 1.0
               }
	    }
# .................................................................................
	    if {$LATPAR(3) != ""} {
               if {$TABLATPAR_c($BRAVAIS) == 1} {
                  set LATPAR(1) $LATPAR(3) ; set COA 1.0
	       } else {
                  if {$LATPAR(1) != ""} {
                     set COA [expr $LATPAR(3) / ($LATPAR(1)*1.0)]
                  } else {
                     if {$COA != ""} { set LATPAR(1) [expr $LATPAR(3) / $COA] }
                  }
               }
               if {[string last ":r" $international] > 0} {
                  set LATPAR(1) $LATPAR(3)
                  set LATPAR(2) $LATPAR(3)
                  set BOA 1.0
                  set COA 1.0
               }
	    }
# .................................................................................

	}
#------------------------------------------------------------------ parameter a set
	{1} {
	    if {$TABLATPAR_b($BRAVAIS) == 1} { set LATPAR(2) $LATPAR(1) }
	    if {$TABLATPAR_c($BRAVAIS) == 1} { set LATPAR(3) $LATPAR(1) }
            if {[string last ":h" $international] > 0} { set LATPAR(2) $LATPAR(1) }
            if {[string last ":r" $international] > 0} { set LATPAR(2) $LATPAR(1) }
            if {[string last ":r" $international] > 0} { set LATPAR(3) $LATPAR(1) }

            if {$LATPAR(2) != ""} { set BOA [expr $LATPAR(2) / ($LATPAR(1)*1.0)] }
	    if {$LATPAR(3) != ""} { set COA [expr $LATPAR(3) / ($LATPAR(1)*1.0)] }
	}
#------------------------------------------------------------------ parameter b set
	{2} {
	    if {$TABLATPAR_b($BRAVAIS) == 1} { set LATPAR(1) $LATPAR(2) }
	    if {$TABLATPAR_c($BRAVAIS) == 1} { set LATPAR(3) $LATPAR(1) }
            if {[string last ":h" $international] > 0} { set LATPAR(1) $LATPAR(2) }
            if {[string last ":r" $international] > 0} { set LATPAR(1) $LATPAR(2) }
            if {[string last ":r" $international] > 0} { set LATPAR(3) $LATPAR(2) }

	    if {$LATPAR(1) != ""} { set BOA [expr $LATPAR(2) / ($LATPAR(1)*1.0)] }
	    if {$LATPAR(3) != ""} { set COA [expr $LATPAR(3) / ($LATPAR(1)*1.0)] }
	}
#------------------------------------------------------------------ parameter c set
	{3} {
	    if {$TABLATPAR_c($BRAVAIS) == 1} { set LATPAR(1) $LATPAR(3) }
	    if {$TABLATPAR_b($BRAVAIS) == 1} { set LATPAR(2) $LATPAR(1) }
            if {[string last ":r" $international] > 0} { set LATPAR(1) $LATPAR(3) }
            if {[string last ":r" $international] > 0} { set LATPAR(2) $LATPAR(3) }

	    if {$LATPAR(1) != ""} { set COA [expr $LATPAR(3) / ($LATPAR(1)*1.0)]
	    if {$LATPAR(2) != ""} { set BOA [expr $LATPAR(2) / ($LATPAR(1)*1.0)] } }
	}
#-------------------------------------------------------------------- c/a - ratio set
	{4} {
	    if {$TABLATPAR_b($BRAVAIS) == 1} {
                set BOA 1.000
	    } else {
		if {$LATPAR(1) != ""} { set LATPAR(2) [expr $LATPAR(1) * $BOA*1.0 ] }
	    }

	    if {$TABLATPAR_c($BRAVAIS) == 1} {
                set COA 1.000
		if {$LATPAR(1) != "" } {
                   set LATPAR(3) $LATPAR(1)
                } else {
                   set LATPAR(1) $LATPAR(3)
                }
            }
	}
#-------------------------------------------------------------------- c/a - ratio set
	{5} {
	    if {$TABLATPAR_c($BRAVAIS) == 1} {
		set COA 1.000
	    } else {
		if {$LATPAR(1) != "" } { set LATPAR(3) [expr $LATPAR(1) * $COA*1.0 ] }
	    }

	    if {$TABLATPAR_b($BRAVAIS) == 1} {
                set BOA 1.000
		if {$LATPAR(1) != "" } {
                   set LATPAR(2) $LATPAR(1)
                } else {
                   set LATPAR(1) $LATPAR(2)
                }
            }
	}
#-------------------------------------------------------------------- angle alpha set
	{6} {
	    if {$TABLATANG_alpha($BRAVAIS) != 0} \
                               { set LATANG(1) $TABLATANG_alpha($BRAVAIS) }
            if {[string last ":h" $international] > 0} {
               set LATANG(1)  90
               set LATANG(2)  90
               set LATANG(3) 120
            }
            if {[string last ":r" $international] > 0} {
               set LATANG(2) $LATANG(1)
               set LATANG(3) $LATANG(1)
            }
	}
#-------------------------------------------------------------------- angle beta set
	{7} {
	    if {$TABLATANG_beta($BRAVAIS)  != 0} \
                               { set LATANG(2) $TABLATANG_beta($BRAVAIS)  }
            if {[string last ":r" $international] > 0} {
               set LATANG(1) $LATANG(2)
               set LATANG(3) $LATANG(2)
            }
	}
#-------------------------------------------------------------------- angle beta set
	{8} {
	    if {$TABLATANG_gamma($BRAVAIS) != 0} \
                               { set LATANG(3) $TABLATANG_gamma($BRAVAIS) }
            if {[string last ":r" $international] > 0} {
               set LATANG(1) $LATANG(3)
               set LATANG(2) $LATANG(3)
            }
	}
    }
#-----------------------------------------------------------------------------------

    if {[string last ":h" $international] > 0} {
       set LATANG(1)  90
       set LATANG(2)  90
       set LATANG(3) 120
    }
#-----------------------------------------------------------------------------------

    set done 1
    for {set i 1} {$i <=3} {incr i} {
	if { [string trim $LATPAR($i)] == "" } { set done 0 }
	if { [string trim $LATANG($i)] == "" } { set done 0 }
    }

    if { $done == 1 } {
       set BOA [expr $LATPAR(2)*1.0 / ($LATPAR(1)*1.0)]
       set COA [expr $LATPAR(3)*1.0 / ($LATPAR(1)*1.0)]
       .latpar.convert configure -state normal
       .latpar.go_on   configure -state normal
       debug $PRC [format "a = %8.4f b= %8.4f c =%8.4f" $LATPAR(1) $LATPAR(2) $LATPAR(3)]
       debug $PRC [format "a = %8.4f b= %8.4f g =%8.4f" $LATANG(1) $LATANG(2) $LATANG(3)]
    }

}
#                                             structure_set_lattice_parameter_abcabg END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



#***************************************************************************************
#                           read_multilayers          
#***************************************************************************************
#
#                                            read data file for multilayer
proc read_multilayers {} {
#
global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ         ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select
global xband_path

set file "$xband_path/locals/2D_structure.dat" 
set NML1 0
set NML2 0

set PRC  read_multilayers

if {[file readable $file]} {

   set fml [open $file r]

   while {[gets $fml line] > -1} {
	set line [string trim $line ]
	set key  [string range $line 0 0]
	if { $key != "#" } {         ;# read entry

           gets $fml next_line
           set NSUBSYS [string trim [lindex $next_line 0] ]

           set C $NSUBSYS
           if {$C=="1"} {
              incr NML1
              set N $NML1
           } else {
              incr NML2
              set N $NML2
           }
           set ML_name($N,$C) $line
           debug $PRC "::: ==========================================="
           debug $PRC "::: $N $C $ML_name($N,$C)"

           gets $fml next_line
           set ML_AX($N,$C) [lindex $next_line 0]
           set ML_AY($N,$C) [lindex $next_line 1]
           set ML_AZ($N,$C) [lindex $next_line 2]

           gets $fml next_line
           set ML_BX($N,$C) [lindex $next_line 0]
           set ML_BY($N,$C) [lindex $next_line 1]
           set ML_BZ($N,$C) [lindex $next_line 2]

           gets $fml next_line
           set ML_CX($N,$C,1) [lindex $next_line 0]
           set ML_CY($N,$C,1) [lindex $next_line 1]
           set ML_CZ($N,$C,1) [lindex $next_line 2]
           debug $PRC "::: $ML_AX($N,$C)   $ML_AY($N,$C)  $ML_AZ($N,$C)"
           debug $PRC "::: $ML_BX($N,$C)   $ML_BY($N,$C)  $ML_BZ($N,$C)"
           debug $PRC "::: $ML_CX($N,$C,1)   $ML_CY($N,$C,1)  $ML_CZ($N,$C,1)"

           gets $fml next_line
           set ML_NLAY($N,$C,1) [lindex $next_line 0]

           for {set IL 1} {$IL <= $ML_NLAY($N,$C,1)} {incr IL} {
              gets $fml next_line
              set ML_NSITES($N,$C,$IL,1) [lindex $next_line 0]

              for {set IS 1} {$IS <= $ML_NSITES($N,$C,$IL,1)} {incr IS} {
                 gets $fml next_line
                 set ML_U1($N,$C,$IL,$IS) [lindex $next_line 0]
                 set ML_V1($N,$C,$IL,$IS) [lindex $next_line 1]
                 set ML_W1($N,$C,$IL,$IS) [lindex $next_line 2]
              }
           }

           # shift all sites to have the first layer in the basis plane

           set w0 $ML_W1($N,$C,1,1)

           for {set IL 1} {$IL <= $ML_NLAY($N,$C,1)} {incr IL} {
              for {set IS 1} {$IS <= $ML_NSITES($N,$C,$IL,1)} {incr IS} {
                 set ML_U1($N,$C,$IL,$IS) [expr $ML_U1($N,$C,$IL,$IS) - $w0]
                 set ML_V1($N,$C,$IL,$IS) [expr $ML_V1($N,$C,$IL,$IS) - $w0]
                 set ML_W1($N,$C,$IL,$IS) [expr $ML_W1($N,$C,$IL,$IS) - $w0]
              }
           }


#-----------------------------------------------------------------------
           if {$NSUBSYS=="2"} {

           gets $fml next_line
           set ML_CX($N,$C,2) [lindex $next_line 0]
           set ML_CY($N,$C,2) [lindex $next_line 1]
           set ML_CZ($N,$C,2) [lindex $next_line 2]

           gets $fml next_line
           set ML_NLAY($N,$C,2) [lindex $next_line 0]

           for {set IL 1} {$IL <= $ML_NLAY($N,$C,2)} {incr IL} {
              gets $fml next_line
              set ML_NSITES($N,$C,$IL,2) [lindex $next_line 0]

              for {set IS 1} {$IS <= $ML_NSITES($N,$C,$IL,2)} {incr IS} {
                 gets $fml next_line
                 set ML_U2($N,$C,$IL,$IS) [lindex $next_line 0]
                 set ML_V2($N,$C,$IL,$IS) [lindex $next_line 1]
                 set ML_W2($N,$C,$IL,$IS) [lindex $next_line 2]
              }
           }

           # shift all sites to have the first layer in the basis plane

           set w0 $ML_W2($N,$C,1,1)

           for {set IL 1} {$IL <= $ML_NLAY($N,$C,2)} {incr IL} {
              for {set IS 1} {$IS <= $ML_NSITES($N,$C,$IL,2)} {incr IS} {
                 set ML_U2($N,$C,$IL,$IS) [expr $ML_U2($N,$C,$IL,$IS) - $w0]
                 set ML_V2($N,$C,$IL,$IS) [expr $ML_V2($N,$C,$IL,$IS) - $w0]
                 set ML_W2($N,$C,$IL,$IS) [expr $ML_W2($N,$C,$IL,$IS) - $w0]
              }
           }

           gets $fml next_line
           set ML_ALAT2to1($N,$C) [lindex $next_line 0]

           }
#-----------------------------------------------------------------------

	}
    }

} else {
  give_warning "." "WARNING \n\n the multilayer data file \n\n $file  \
                \n\n is not available \n\n NO DEFAULTS AVAILABLE"
  return
}

set ML_NML(1) $NML1
set ML_NML(2) $NML2
set ML_NSU  $NSUBSYS
#===============================================================================

}
#                                                               read_multilayers END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                        structure_convert_ALAT             
#***************************************************************************************
#
proc structure_convert_ALAT { wbut } {#  convert ALAT to atomic units
    global ALAT BOHRRAD

    set tcl_precision 17
    
    if {[string trim $ALAT] ==""} {
	give_warning "." "WARNING \n\n specify lattice parameter   A  first \n\n "
	return
    }
    
    set ALAT [expr $ALAT / $BOHRRAD]
    
    if {[winfo exists $wbut]} { $wbut configure -state disabled }
}
#                                                                 structure_convert_ALAT
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                            convert_basis_vectors_RQ         
#***************************************************************************************
#
#                                                               
proc convert_basis_vectors_RQ {key} {
global RQX RQY RQZ
global RQU RQV RQW NQ
global RBASX RBASY RBASZ

set tcl_precision 17

set PRC "convert_basis_vectors_RQ"

set V1 [format "%14.8f%14.8f%14.8f"  $RBASX(1)  $RBASY(1)  $RBASZ(1)]
set V2 [format "%14.8f%14.8f%14.8f"  $RBASX(2)  $RBASY(2)  $RBASZ(2)]
set V3 [format "%14.8f%14.8f%14.8f"  $RBASX(3)  $RBASY(3)  $RBASZ(3)]

writescr .d.tt "INFO from <$PRC>   ----------------- $key \n"
writescr .d.tt "PRIM. VECTOR 1: $V1 \n"
writescr .d.tt "PRIM. VECTOR 2: $V2 \n"
writescr .d.tt "PRIM. VECTOR 3: $V3 \n"

#=======================================================================================
if {$key=="cryst_to_cart"} {

   for {set IQ 1} {$IQ <=$NQ} {incr IQ} {
      set U $RQU($IQ)
      set V $RQV($IQ)
      set W $RQW($IQ)
   
      set RQX($IQ) [expr $U*$RBASX(1) + $V*$RBASX(2) + $W*$RBASX(3) ]
      set RQY($IQ) [expr $U*$RBASY(1) + $V*$RBASY(2) + $W*$RBASY(3) ]
      set RQZ($IQ) [expr $U*$RBASZ(1) + $V*$RBASZ(2) + $W*$RBASZ(3) ]

      set V1 [format "%8.3f%8.3f%8.3f"  $U  $V  $W]
      set W1 [format "%8.3f%8.3f%8.3f"  $RQX($IQ)  $RQY($IQ)  $RQZ($IQ)]
      writescr .d.tt "site [format "%3i"  $IQ]: (u,v,w) $V1 --> (x,y,z) $W1 \n"
   }

#=======================================================================================
} elseif {$key=="cart_to_cryst"} {
#
#---------------------------------- find expansion coefficients  R = sum(i) p_i * RBAS_i
#
#
#------------------------------------------------ prepare basis expansion
#
   for {set B1 1} {$B1 <= 3} {incr B1} {
      for {set B2 1} {$B2 <= 3} {incr B2} {
         set aa($B1,$B2) [expr  ($RBASX($B1)*$RBASX($B2)) \
                              + ($RBASY($B1)*$RBASY($B2)) \
                              + ($RBASZ($B1)*$RBASZ($B2)) ]
      }
   }
   
   set a123 [expr  $aa(1,1)*$aa(2,2)*$aa(3,3) ]
   set a231 [expr  $aa(1,2)*$aa(2,3)*$aa(3,1) ]
   set a312 [expr  $aa(1,3)*$aa(2,1)*$aa(3,2) ]
   set b132 [expr  $aa(1,1)*$aa(2,3)*$aa(3,2) ]
   set b213 [expr  $aa(1,2)*$aa(2,1)*$aa(3,3) ]
   set b321 [expr  $aa(1,3)*$aa(2,2)*$aa(3,1) ]
   
   set det [expr  $a123 + $a231 + $a312 - $b132 - $b213 - $b321 ]

#-----------------------------------------------------------------------

   for {set IQ 1} {$IQ <=$NQ} {incr IQ} {
   
      for {set I 1} {$I <= 3} {incr I} {
        set bb($I) [expr $RQX($IQ)*$RBASX($I)+$RQY($IQ)*$RBASY($I)+$RQZ($IQ)*$RBASZ($I)]
      }

      set dbb(1) [expr $bb(1)  *$aa(2,2)*$aa(3,3) \
                      +$aa(1,2)*$aa(2,3)*$bb(3)   \
                      +$aa(1,3)*$bb(2)  *$aa(3,2) \
                      -$bb(1)  *$aa(2,3)*$aa(3,2) \
                      -$aa(1,2)*$bb(2)  *$aa(3,3) \
                      -$aa(1,3)*$aa(2,2)*$bb(3)  ]
   
      set dbb(2) [expr $aa(1,1)*$bb(2)  *$aa(3,3) \
                      +$bb(1)  *$aa(2,3)*$aa(3,1) \
                      +$aa(1,3)*$aa(2,1)*$bb(3)   \
                      -$aa(1,1)*$aa(2,3)*$bb(3)   \
                      -$bb(1)  *$aa(2,1)*$aa(3,3) \
                      -$aa(1,3)*$bb(2)  *$aa(3,1) ]
   
      set dbb(3) [expr $aa(1,1)*$aa(2,2)*$bb(3)   \
                      +$aa(1,2)*$bb(2)  *$aa(3,1) \
                      +$bb(1)  *$aa(2,1)*$aa(3,2) \
                      -$aa(1,1)*$bb(2)  *$aa(3,2) \
                      -$aa(1,2)*$aa(2,1)*$bb(3)   \
                      -$bb(1)  *$aa(2,2)*$aa(3,1) ]
   
      set scale 1000000.0

      for {set I 1} {$I <= 3} {incr I} {
         set pp($I) [expr $dbb($I) / $det ]
   
#                           remove numerical noise
#        set pp($I) [expr round($scale*$pp($I))/$scale]
	 if {[expr abs($pp($I))]<0.00000001} {set pp($I) 0.0} 
      }
   
      set RQU($IQ) $pp(1)
      set RQV($IQ) $pp(2)
      set RQW($IQ) $pp(3)
   
      set V1 [format "%8.3f%8.3f%8.3f"  $RQX($IQ)  $RQY($IQ)  $RQZ($IQ)]   
      set W1 [format "%8.3f%8.3f%8.3f"  $RQU($IQ)  $RQV($IQ)  $RQW($IQ)]
      writescr .d.tt "site [format "%3i"  $IQ]: (x,y,z) $V1 --> (u,v,w) $W1 \n"
   }

#=======================================================================================
} else {
   set TXT "\n WARNING \n\n key = $key \n\n not supported by procedure \n\n $PRC\n"
   give_warning "." $TXT
   writescr .d.tt $TXT
   return
}
#=======================================================================================

}
#                                                           convert_basis_vectors_RQ END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                             shift_lattice_sites        
#***************************************************************************************
#
#                                                     shift lattice sites into unit cell
proc shift_lattice_sites { } {
#

global RQX RQY RQZ
global RQU RQV RQW NQ
global RBASX RBASY RBASZ STRUCTURE_SETUP_MODE
   
for {set IQ 1} {$IQ <=$NQ} {incr IQ} {

      for {set I 1} {$I <= 3} {incr I} {

	if {$I==1} {
           set P $RQU($IQ)
        } elseif {$I==2} {
           set P $RQV($IQ)
        } else {
           set P $RQW($IQ)
        }

	set PP 0
	if { $P >  1 } { set PP  1 }
	if { $P >  2 } { set PP  2 }
	if { $P <  0 } { set PP -1 }
	if { $P < -1 } { set PP -2 }
        set P $PP

#------------------------------------------ ensure that order of layers is kept properly
        if {($STRUCTURE_SETUP_MODE == "2D LAYERS"    \
          || $STRUCTURE_SETUP_MODE == "3D SURFACE") && $I == 3 } {
	   if {$IQ == 1 } { 
              set shift $RQW(1)
              set P $shift
	   } else {
              if {$RQW($IQ)>$RQW(1)} {
                 set P $shift
	      } else {
                 set P [expr $shift - 1]
              } 
           }
        }             

        set RQX($IQ) [expr $RQX($IQ) - $P*$RBASX($I)]
        set RQY($IQ) [expr $RQY($IQ) - $P*$RBASY($I)]
        set RQZ($IQ) [expr $RQZ($IQ) - $P*$RBASZ($I)]
      }
}

}
#                                                                shift_lattice_sites END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                              set_lattice_parameters_using_RBAS       
#***************************************************************************************
#
#                                                      set_lattice_parameters_using_RBAS
proc set_lattice_parameters_using_RBAS { } {
#
global RBASX RBASY RBASZ LATPAR LATANG ALAT BOA COA

set PRC "set_lattice_parameters_using_RBAS"

set PI 3.141592653589793238462643

writescr .d.tt "INFO from <$PRC>   ---------------- \n"
writescr .d.tt "lattice parameters and angles will be updated  \n" 
debug $PRC "INFO from <$PRC>   ------------------ "
debug $PRC "lattice parameters and angles will be updated  "

debug $PRC "primitive vectors     (cart. coord.) \[A\]"
for {set I 1} {$I <= 3} {incr I} {
  debug $PRC [format "%18.12f%18.12f%18.12f" $RBASX($I) $RBASY($I) $RBASZ($I)]
}

for {set i 1} {$i <= 3} {incr i} {   
   set SQR [expr pow($RBASX($i),2)+pow($RBASY($i),2)+pow($RBASZ($i),2)]
   set LATPAR($i) [expr pow($SQR,0.5) ] 
}

set XX(1) [expr ($RBASX(2)*$RBASX(3) \
                +$RBASY(2)*$RBASY(3) \
                +$RBASZ(2)*$RBASZ(3))/($LATPAR(2)*$LATPAR(3))]
set XX(2) [expr ($RBASX(1)*$RBASX(3) \
                +$RBASY(1)*$RBASY(3) \
                +$RBASZ(1)*$RBASZ(3))/($LATPAR(1)*$LATPAR(3))]
set XX(3) [expr ($RBASX(2)*$RBASX(1) \
                +$RBASY(2)*$RBASY(1) \
                +$RBASZ(2)*$RBASZ(1))/($LATPAR(2)*$LATPAR(1))]

#----------- remove numerical noise
set scale 10000000.0

set TXT1 ""
set TXT2 ""
for {set i 1} {$i <= 3} {incr i} {
   set LATPAR($i) [expr $LATPAR($i)*$ALAT ] 
   set LATANG($i) [expr int(0.1+$scale*acos($XX($i))*180.0/$PI)/$scale]
   append TXT1 [format "   %18.12f" $LATPAR($i)]
   append TXT2 [format "   %18.12f" $LATANG($i)]
}
debug $PRC "lattice parameters   "
debug $PRC "$TXT1"
debug $PRC "lattice angles"
debug $PRC "$TXT2"

set ALAT [expr 1.0 * $ALAT] ; # just to skip trailing 0's
set BOA  [expr $LATPAR(2) / $LATPAR(1)]
set COA  [expr $LATPAR(3) / $LATPAR(1)]

debug $PRC "ALAT  $ALAT   BOA   $BOA    COA  $COA"

}
#                                                  set_lattice_parameters_using_RBAS END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<




#***************************************************************************************
#                                     cross_prod
#***************************************************************************************
#
#                                                                        proc cross_prod
proc cross_prod {ax ay az bx by bz c} {

    set PRC "cross_prod"
    set axtmp [expr $ax] 
    set aytmp [expr $ay]
    set aztmp [expr $az]
    set bxtmp [expr $bx]
    set bytmp [expr $by]
    set bztmp [expr $bz]
    if {$c=="x"} {
	set r [expr {$aytmp*$bztmp - $aztmp*$bytmp}]
    } elseif {$c=="y"} {
	set r [expr {$aztmp*$bxtmp - $axtmp*$bztmp}]
    } else {
	set r [expr {$axtmp*$bytmp - $aytmp*$bxtmp}]
    }
    return $r
}
#                                                                   proc cross_prod  END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#***************************************************************************************
#                                 dot_prod    
#***************************************************************************************
#
#                                                                          proc dot_prod
proc dot_prod {ax ay az bx by bz} {
    
    set PRC "dot_prod"
    set axtmp [expr $ax] 
    set aytmp [expr $ay]
    set aztmp [expr $az]
    set bxtmp [expr $bx]
    set bytmp [expr $by]
    set bztmp [expr $bz]
    
    set r [expr {$axtmp*$bxtmp + $aytmp*$bytmp + $aztmp*$bztmp}]
    
    return $r
}
#                                                                     proc dot_prod  END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


########################################################################################
#                        write actual lattice info
########################################################################################
#                                               
proc write_actual_lattice_info { calling_proc } {
return

global NQ NT NQ RQX RQY RQZ RWS IMQ NLQ NOQ ITOQ ZT TXTT CONC

puts " "
puts " ============================================================================= "
puts "                            actual lattice info   "
puts " ============================================================================= "
puts "  invoked from     $calling_proc "
puts " "

if {![info exists NQ] || [string trim $NQ] ==""} \
  { puts "  NQ:   NOT SET " ; puts " "; return }
if {![info exists NT] || [string trim $NT] ==""} \
  { puts "  NT:   NOT SET " ; puts " "; return }

puts "  NQ:   [format %3i $NQ]"
puts "  NT:   [format %3i $NT]"
puts " "
puts "  IQ     RQX    RQY    RQZ     RWS   IMQ NLQ NOQ   IT   Z  TXTT CONC"

for {set IQ 1} {$IQ <= $NQ } {incr IQ} {
   set txt1 [format " %3i  %7.3f%7.3f%7.3f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
   set txt2 [format "%7.3f %3i %3i %3i" $RWS($IQ) $IMQ($IQ) $NLQ($IQ) $NOQ($IQ)]
   set txt3 ""
   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ($IO,$IQ)
      set txtaux [format "%3i %3i %5s %4.2f"   $IT $ZT($IT) $TXTT($IT) $CONC($IT)] 
      set txt3 "$txt3  $txtaux "
   }
   puts "$txt1 $txt2 $txt3 "
}
puts " ============================================================================= "
puts " "

}
#                                                          write_actual_lattice_info END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
