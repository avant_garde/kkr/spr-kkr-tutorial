########################################################################################
########################################################################################
#                                                                         create_inpfile
#     
#     this procedure supplies the PACKAGE INDEPENDENT part of the inpfile set up and 
#     calls the specific procedures to set up the input mask and to write the input file
#     
proc create_inpfile {} {
    global structure_window_calls inpfile inpsuffix 
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VARLST NCPA
    global SPRKKR_EXPERT xband_path 
    global COLOR WIDTH HEIGHT FONT
    global TUTORIAL 

    set TUTORIAL 0

global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1 
global BGWR  ; set BGWR   lavender
global BGK   ; set BGK    SteelBlue1 
global BGCLU ; set BGCLU  LightSkyBlue 
global BGE   ; set BGE    moccasin 
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1  
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1 

## plum1            LightSkyBlue       azure1            
## thistle1	    bisque             SteelBlue1   
## SpringGreen	    seashell           DeepSkyBlue1 
## chartreuse	    linen              SlateGray1   
## GreenYellow	    ivory              NavajoWhite1 
## PaleGreen	    azure              LightSalmon1 
## gold		    lavender           LightPink1   
## wheat	    moccasin           orchid1      
## thistle	    khaki1             cornsilk1          
## seashell1	    LightGoldenrod1    ivory1        
## cyan		    yellow1            LavenderBlush1
## AntiqueWhite1    RosyBrown1        
## LemonChiffon1    orange1           


#===============================================================================================
if { "$sysfile$syssuffix" == ".sys" } {

    set win .c_s_sys
    toplevel_init $win "Create or select a system file" 0 0
    wm geometry $win +50+200
    wm positionfrom .c_s_sys user
    wm sizefrom .c_s_sys ""
    wm minsize .c_s_sys 

    set buttonbgcolor green1 
    set buttonheight  2

    label .c_s_sys.lab -text "create or select a system file first"
#
#---------------------------------------------------- CREATE SYSTEM 
#
    button .c_s_sys.csys -text "CREATE SYSTEM" -width 20 -height $buttonheight \
	-command {create_system; destroy .c_s_sys} -bg $buttonbgcolor
#
#---------------------------------------------------- SELECT SYSTEM 
#
     button .c_s_sys.ssys -text "SELECT SYSTEM" -width 20 -height $buttonheight \
	-command {select_system; destroy .c_s_sys} -bg $buttonbgcolor

#
#---------------------------------------------------- SELECT SYSTEM 
#
     button .c_s_sys.ende -text "CLOSE" -width 20 -height $buttonheight \
	-command {destroy .c_s_sys} -bg $COLOR(CLOSE)

     pack configure .c_s_sys.lab .c_s_sys.csys .c_s_sys.ssys .c_s_sys.ende \
            -anchor s  -side top -pady 15 -padx 30

#===============================================================================================
} else {

#===============================================================================================
#                                system file available
#===============================================================================================

    if [file exists $sysfile$syssuffix] {
       read_sysfile
       $W_rasmol_button configure -state normal
    } else {
        give_warning . "WARNING \n\n the system file \n\n $sysfile$syssuffix \n\n does not exist"
        return
    }             

    if [file exists struc.inp] {
       $W_findsym_button configure -state normal
    }

    set Wcinp .c_inp
    toplevel_init $Wcinp "Create input file" 0 0
    wm geometry $Wcinp +10+50
    wm positionfrom $Wcinp user
    wm sizefrom $Wcinp ""
    wm minsize $Wcinp 

    set buttonbgcolor green1 
    set buttonheight  2

    frame $Wcinp.a
#------------------------------------------------------------------------------------
    frame $Wcinp.a.sys 
    button $Wcinp.a.sys.ssys -text "SELECT SYSTEM" -width 20 -height $buttonheight \
	-command "select_system; destroy $Wcinp" -bg lightblue
#
    button $Wcinp.a.sys.esys -text "edit \n system file" -width 20 -height $buttonheight \
        -command "destroy $Wcinp ; edit_sysfile" -bg lightblue
    proc edit_sysfile { } {
    global editor edback edxterm edoptions sysfile syssuffix
    set ed_file $sysfile$syssuffix
    eval set res [catch "exec $edxterm $editor $edoptions $sysfile$syssuffix $edback" message]
    }
    pack configure $Wcinp.a.sys.ssys -anchor n  -side top -pady 1 -padx 30
    pack configure $Wcinp.a.sys.esys -anchor n  -side top -pady 1 -padx 30
#------------------------------------------------------------------------------------
    frame $Wcinp.a.fil 
    label $Wcinp.a.fil.lab -text "input file name"
#
    regsub "$syssuffix\$" $sysfile "" inpfile
    set inpfile $inpfile$inpsuffix

    entry $Wcinp.a.fil.ent -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -textvariable {inpfile} -width {29} \
          -font $FONT(GEN)
     pack configure $Wcinp.a.fil.lab $Wcinp.a.fil.ent  -anchor s  -side top -padx 30
#------------------------------------------------------------------------------------
    frame $Wcinp.a.aa
#
     button $Wcinp.a.aa.ende -text "CLOSE" -width 20 -height $buttonheight \
	-command "destroy $Wcinp" -bg $COLOR(CLOSE)

     pack configure $Wcinp.a.aa.ende \
            -anchor s  -side top -pady 1 -padx 5
#
     button $Wcinp.a.aa.edit -text "edit inpfile \n quit" -width 20 -height $buttonheight \
	-command "destroy $Wcinp ; edit_inpfile" -bg LightSalmon2
     proc edit_inpfile { } {
     global editor edback edxterm edoptions inpfile inpsuffix
     set ed_file $inpfile
     eval set res [catch "exec $edxterm $editor $edoptions $inpfile $edback" message]
     }

     pack configure $Wcinp.a.aa.edit \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
    frame $Wcinp.a.bb
#
     button $Wcinp.a.bb.write -text "write \ninpfile" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp nix " -bg PaleGreen

     button $Wcinp.a.bb.append -text "append to\ninpfile" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp append" -bg SeaGreen2

     pack configure $Wcinp.a.bb.write $Wcinp.a.bb.append \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
    frame $Wcinp.a.cc 
#
     button $Wcinp.a.cc.ok -text "write inpfile\nquit" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp nix ; destroy $Wcinp" -bg $buttonbgcolor
#
     button $Wcinp.a.cc.edit -text "write+edit inpfile \n quit" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp edit ; destroy $Wcinp" -bg YellowGreen

     pack configure $Wcinp.a.cc.ok $Wcinp.a.cc.edit \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
     pack configure $Wcinp.a.sys  $Wcinp.a.fil \
             $Wcinp.a.aa  $Wcinp.a.bb  $Wcinp.a.cc -side left -fill x
     pack configure $Wcinp.a
#------------------------------------------------------------------------------------

set w  $Wcinp
set wl 12
set we 30
set ws 160
set fe $FONT(GEN)

frame $w.cols 
pack configure $w.cols -in $w -anchor w -side top -fill x -expand y

frame $w.cols.col1 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col2 -bg $BGP -relief raised -borderwidth 2 
frame $w.cols.col3 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col4 -bg $BGP -relief raised -borderwidth 2 

set wc1 $w.cols.col1 ; set wc2 $w.cols.col2  
set wc3 $w.cols.col3 ; set wc4 $w.cols.col4  


set VAR(POTFIL) NIX
set VAR(OWRPOT) 0

#=======================================================================================
#                  call package specific procedure   create_inpfile_${PACKAGE}
#=======================================================================================
#
if {[file exists $xband_path/create_inpfile_${PACKAGE}.tcl] } {

    create_inpfile_${PACKAGE}

#=======================================================================================
#                                  PACKAGE NOT IMPLEMENTED 
#=======================================================================================
} else {
   give_warning "." "WARNING \n\n program package \n\n $PACKAGE \n\n \
   not implemented in \n\n <write_inpfile> \n\n empty input file created "
}
#=======================================================================================

} ; # system file available

} 
#                                                                     END create_inpfile
########################################################################################



########################################################################################
#                                                                       write_inpfile
proc write_inpfile {Wcinp tuwas} {
    global structure_window_calls inpfile inpsuffix potfile potsuffix
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
    global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT 
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global Wprog inpsuffixlist  VAR VARLST 
    global editor edtext edxterm edoptions edsyntaxhelp edback  
    global NM IMT IMQ RWSM QMTET QMPHI 
    global buttonlist_PROGS
    global NCL NQCL IQECL ICLQ xband_path

    set tcl_precision 17

#=======================================================================================
#                  call package specific procedure   write_inpfile_${PACKAGE}
#=======================================================================================
#
# NOTE:   write_inpfile_${PACKAGE} has to be in file    create_inpfile_${PACKAGE}.tcl
#
if {[file exists $xband_path/create_inpfile_${PACKAGE}.tcl] } {

    write_inpfile_${PACKAGE} $Wcinp $tuwas

#=======================================================================================
#                                  PACKAGE NOT IMPLEMENTED 
#=======================================================================================
} else {

   give_warning "." "WARNING \n\n program package \n\n $PACKAGE \n\n \
   not implemented in \n\n <write_inpfile> \n\n empty input file created "

   set file_name $inpfile
   if {$tuwas == "append" && [file exists $file_name]==1} {
      set inp [open $file_name a]
   } else {
      set inp [open $file_name w]
   }

   puts  $inp  "<write_inpfile> was not able to create an input file for the " 
   puts  $inp  "program package:         $PACKAGE"
   puts  $inp  " "
   puts  $inp  "create input yourself --- the system file   $sysfile$syssuffix follows"
   puts  $inp  " "
   puts  $inp  "***************************************************************************"
   close $inp
   exec cat $sysfile$syssuffix >> $inpfile
}
#=======================================================================================

lfuellen $Wprog.c.m.f.f.li $inpsuffixlist
prog_anzeige $Wprog                    
foreach i $buttonlist_PROGS { $i configure -state normal -bg green1 }
$Wprog.b.c1.m.mb configure -state normal -bg LightBlue

writescr0  .d.tt "\nINFO from <write_inpfile>: \
input file $inpfile written \n" 

eval set res [catch "exec cat $inpfile  " message] 
writescr  .d.tt "\n$message \n\n "

if {[file exists $VAR(POTFIL)]=="0" || $VAR(OWRPOT)=="1" } { 
   writescr  .d.tt "\nINFO from <write_inpfile>: \
   potential file $potfile written \n" 
}   


if {$tuwas=="edit"} {
    set ed_file $inpfile
    eval set res [catch "exec $edxterm $editor $edoptions $inpfile $edback" message] 
}

} 
#                                                                   END write_inpfile
########################################################################################



