      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER(NTMAX=200)
      PARAMETER(NRMAX=300)
      PARAMETER (NRAD1=251)
      parameter (MAXDIM=1 000 000)
      parameter (max2sort=NTMAX)
      integer W(MAXDIM)

      character*4 TXTT(NTMAX),fff*100
      dimension Z(NTMAX),NAT(NTMAX)
      dimension RWS(NTMAX)              ! - AS radii
      dimension BAS(3,NTMAX)            ! - sites cartesian coordinates
      dimension CONC(NTMAX),IMT(NTMAX),IMQ(NTMAX)
      dimension itop(48),iqa(48)
      dimension wsrest(max2sort)              ! - AS radii
      dimension R0site(max2sort),RHOsite(NRAD1,max2sort),isnew(max2sort)
     $     ,rat(NRAD1,max2sort),ZZ(max2sort)
      dimension taues(3,max2sort),Ses(max2sort),BASnew(3,max2sort)
      dimension av(3),bv(3),cv(3)    ! - basic translation vectors
      common/l2lat/av,bv,cv,qlat(3,3)
      common /emptys/nates,n1es,n2es,n3es,rmines,rmaxes

c
c
c   Temoporary arrays:
c
      dimension R0(NTMAX),jrmax(NTMAX),amtc(NRMAX,NTMAX)
      dimension wa(NRMAX),ro_inter(NRMAX),rs(NRMAX)

      logical db
 

c
c ==================================================
c
c  parameters which can be read from input
c     
      R0act=1.d-2
      NRADMAX=NRMAX-1
      dpas=0.05d0                       !exp. pass
      ndnev=14                          !interpolation
c
c
      db=.true.                         !debug
      n1es=24                           !cell division
      n2es=24
      n3es=24
      rmines=1.2                        !minimal ES
      rmaxes=2.5                        !maximal ES
      need_es=0                         !0 - activate search of es
c
c ==================================================
c



c
c ==================================================
c
c       READING INPUT DATA:
c
      ini=23
      call getarg(1,fff)
      if(fff.ne.' ')then
        open(ini,file=fff,status='old')
      else
        ini=5
        print*,'READING FROM THE STANDART INPUT:'
      endif
c----------------------------------------------------
      read(ini,*) need_es 
      read(ini,*) rmines
      read(ini,*) rmaxes
c----------------------------------------------------
      do i=1,9
      read(ini,*)
      end do
      read(ini,*)isp1,isp2
      if(db)print*,isp1,isp2
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*)alat
      if(db)print*,alat
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*)
      read(ini,*) (av(i),i=1,3)
      if(db)print*, (av(i),i=1,3)
      read(ini,*) (bv(i),i=1,3)
      read(ini,*) (cv(i),i=1,3)
      if(db)print*, (cv(i),i=1,3)
      read(ini,*)
      read(ini,*)NQ
      read(ini,*)
      if(db)print*,'NATOM:',NQ
      do i=1,NQ
        read(ini,*)idum,IMQ(i),(bas(j,i),j=1,3)!,RWS(IMQ(i)),nl,
      enddo
      read(ini,*)
      read(ini,*) NM
      read(ini,*)
      do i=1,nm
        read(ini,*)
      enddo
      read(ini,*)
      read(ini,*) nsort
      read(ini,*)
      nt=nsort
      do i=1,nsort
          read(ini,*)idum,Z(i),TXTT(i),nn,CONC(i),IMT(i)
      enddo
      read(ini,*)ng
      read(ini,*)(itop(i),i=1,ng)
      read(ini,*)(iqa(i),i=1,ng)
      if(db)then
        print*,'PARAMETERS:',NT,NQ,NM,'NT,NQ,NM,-- ALAT:',alat
        print*,'Z(i),TXTT(i),CONC(i),IMT(i)'
        do i=1,NT
          print*,Z(i),TXTT(i),CONC(i),IMT(i)
        enddo
        print*,'qx(i),qy(i),qz(i),IMQ(i)'
        do i=1,NQ
          print*,(bas(j,i),j=1,3),IMQ(i)
        enddo

        print*,'Lattice vectors:'
        print*,av
        print*,BV
        print*,CV
      endif
c
c ==================================================
c

      call wkinit(MAXDIM)

      call getgbasis

C
c amtc,dpas,r0,rom,rop,r0,rop,rom,
c

      call readro(NT,amtc,Z,dpas,r0,TXTT,RHOsite,NRADMAX,NRAD1)

c
c Producing new radial mesh:
c
      
      do it=1,nt
        do i=1,NRAD1
          rat(i,it)=r0(it)*exp(dble(i-1)*dpas)
        enddo
      enddo

      do i=1,nm
        wsrest(i)=0
        zz(i)=0
        R0site(i)=R0act
      enddo

      call dinit(RHOsite,NRAD1*max2sort)
      

      do itype=1,nt
c
c this is cycle over atomic types
c IMT(itype) - number of inequivalent lattice position, which is
c occupied by this atom
c
        isort=IMQ(IMT(itype))
        zn=Z(itype)
        call defwsr(ws,zn)
        if(db)print*,'type:',itype,' Z=',zn,' Rad=',ws,' conc='
     $       ,conc(itype)
        wsrest(isort)=wsrest(isort)+ws*conc(itype)
        zz(isort)=zz(isort)+zn*conc(itype)
        do ipnt=1,NRAD1
          rad=R0act*exp(dble(ipnt-1)*dpas)
          if(rad.lt.rat(nrad1-2,itype))then
            rint=max(0.d0,
     $           dnevmod(rat(1,itype),amtc(1,itype),rad,NRAD1,ndnev))
          else
            rint=0
          endif
          RHOsite(ipnt,isort)=RHOsite(ipnt,isort)+ conc(itype)*rint
        enddo
      enddo

      do i=1,nm
        if(db)print*,' Site:',i,'  RaD:',wsrest(i),' Z:',zz(i)
      enddo

c      stop

      nsort=NM
      natom=nq


      if(db)then
        print*,'av=',av
        print*,'bv=',bv
        print*,'cv=',cv
        print*,'imq',(imq(i),i=1,natom)
        print*,'BAS'
        do i=1,natom
          print*,(bas(j,i),j=1,3)
        enddo
        do ist=1,nsort
          print*,'R0S',R0site(ist),' ZZ',zz(ist),wsrest(ist)
        enddo
        print*,'dpas,alat,natom,nsort:',dpas,alat,natom,nsort
      endif
      
      call defmtr(w,nrad1,av,bv,cv,imq,bas,RHOsite,R0site,dpas,alat
     $     ,natom,nsort,zz,rws,need_es,nrad1,wsrest)

      natomnew=natom
      if(need_es.eq.0)then
        call emptyspheres(W,av,bv,cv,bas,imq,alat,natom,nsort,rws
     $       ,nsortes,taues,Ses,max2sort,itop,iqa,ng,isnew,basnew
     $       ,natomnew)
cccccccccccccccccccccccccccccccccccccccccccccccccccc
       else
         do iatom=1,natom
           isnew(iatom)=imq(iatom)
           do i=1,3
           basnew(i,iatom)=bas(i,iatom)
           enddo
         enddo
ccccccccccccccccccccccccccccccccccccccccccccccccccc
       endif

c
c ==================================================
c
c       WRITING OUTPUT DATA:
c

      open(22,file='radii.out')
      write(22,*)natomnew
      immax=-1
      do i=1,natomnew
        isr=isnew(i)
        if(i.le.natom)then
          write(22,22)isnew(i),(basnew(j,i),j=1,3),rws(imq(i)),' / A/'
        else
          write(22,22)isr,(basnew(j,i),j=1,3),ses(isr-nm), ' / E/'
        endif
 22     format(i4,2x,3f16.10,3x,f14.10,3x,a)
      enddo
      close(22)
      end
c
c ==================================================
c
c

      subroutine readro(nsort,ro,z,dpas,r0,txt,rom,NRADMAX,NRAD1)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      dimension ro(nradmax+1,*),rom(nrad1,*)
      dimension z(*),r0(*),txt(*)
      character txt*4
      open(8,status='scratch')
      do isort=1,nsort
        if(z(isort).lt.0)then
          iznuc=-10
          call qzzc(txt(isort),iznuc,izc)
          z(isort)=iznuc
        endif
        if(z(isort).lt.0.3d0)then
c emty sphere
          do i=1,nrad1
            ro(i,isort)=0.d0
            rom(i,isort)=0.d0
            r0(isort)=1d-5
          enddo
        else
          do isr=1,isort-1
            if(nint(z(isr)).eq.nint(z(isort)))then
              do i=1,nrad1
                ro(i,isort)=ro(i,isr)
                rom(i,isort)=rom(i,isr)
              enddo
              r0(isort)=r0(isr)
              goto 41
            endif
          enddo
c          write(buf,2)nint(z(isort)),nint(zc)
          write(ilun(16)
     $         ,'(/'' For atom '',a4,''    Z='',f4.0/)'
     $         )txt(isort),z(isort)
          izn=nint(z(isort))
          izc=0
          call rhfds(izn,izc,ro(1,isort),rom(1,isort),r0(isort))

 41       continue
        endif
      enddo
      close(8)
      end
      subroutine qzzc(txt,iz,izc)
      dimension izcor(105)
      character elements*210,txt*2,TX*2
      data izcor/
     >0,
     >0,0,
     >6*2,4,4,
     >6*10,12,12,
     >12*18,4*28,30,30,
     >12*36,4*46,48,48,
     >16*54,10*68,4*78,80,80,
     >18*86/
      elements=
     >'E H HeLiBeB C N O F NeNaMgAlSiP S ClArK CaScTiV CrMnFeCoNi'//
     >'CuZnGaGeAsSeBrKrRbSrY ZrNbMoTcRuRhPdAgCdInSnSbTeI XeCsBaLa'//
     >'CePrNdPmSmEuGdTbDyHoErTmYbLuHfTaW ReOsIrPtAuHgTlPbBiPoAtRn'//
     >'FrRaAcThPaU NpPuAmCmBkCfEsFmMdNoLwKu'
      if(iz.ge.0)then
        izc=izcor(iz+1)
        return
      endif
      TX=TXT
      if(TX(2:2).eq.'_')TX(2:2)=' '
      do i=1,105
c        print 1,elements(i*2-1:i*2),i-1,izcor(i)
        if(elements(i*2-1:i*2).eq.txt(1:2))then
          iz=i-1
          izc=izcor(i)
          Return
        endif
      enddo
      write(16,*)txt
      stop 'Error : Such Chemical element is ABSENT'
      end
      subroutine shortn(p,p1)
      implicit double precision (a-h,o-z)
      parameter (eps=1.d-7)
      common/l2lat/plat(3,3),qlat(3,3)
      dimension p(3),p1(3),x(3)
c
      do i=1,3
        x(i)=p(1)*qlat(1,i)+p(2)*qlat(2,i)+p(3)*qlat(3,i)
        x(i)=x(i)-nint(x(i)-eps)
c$$$        if(x(i).lt.-0.5d0+eps)x(i)=x(i)+1.d0
      enddo
      do i=1,3
        p1(i)=x(1)*plat(i,1)+x(2)*plat(i,2)+x(3)*plat(i,3)
      enddo
      end
      subroutine vec(v0,v1,v2,g1)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      dimension v0(3),v1(3),v2(3),g1(3,3)
      do i=1,3
        v0(i)=v1(i)
        do j=1,3
          v0(i)=v0(i)+v2(j)*g1(j,i)
        enddo
      enddo
      CALL SHORTN(V0,V0)
      end
      subroutine getgbasis
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      common /l2lat/ avc(3),bvc(3),cvc(3),fvc(3),pvc(3),hvc(3),volcell
      call cross(fvc,bvc,cvc)
      call cross(pvc,cvc,avc)
      call cross(hvc,avc,bvc)
      w1=avc(1)*fvc(1)+avc(2)*fvc(2)+avc(3)*fvc(3)
      volcell=abs(w1)

      if(volcell.lt.1.d-5)then
      print*,avc
      print*,bvc
      print*,cvc
        call endjob(10,'GETGBASIS: Lattice vectors are complanar!')
      endif
      do i=1,3
        fvc(i)=fvc(i)/w1
        pvc(i)=pvc(i)/w1
        hvc(i)=hvc(i)/w1
      enddo
      end
