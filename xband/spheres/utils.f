c
c     Neville -Algorithmus
c
      function dneville(x,y,t,x1,n)
      implicit double precision (a-h,o-z)
*-----------------------------------------------------------------------------
      dimension x(n),y(n),t(n)
c      call dcopy(n,y,1,t,1)
c      if((x1-x(n)).gt.1.d-5.or.(x(1)-x1).gt.1.d-5) then
c        dneville=0.d0
c        return
c      endif
*     
      do i=1,n
        t(i)=y(i)
      enddo
c
c It is not clear for me, what does it mean t(0)
c
      do i=1,n-1
C        do j = n,i,-1        !<===== It was!
	do j = n,i+1,-1       ! <=== My changes!!!
c         print*,'IJ',i,j,x(j),x(j-i)
          t(j) = t(j)+(x1-x(j))*(t(j)-t(j-1))/(x(j)-x(j-i))
        enddo                   ! j
      enddo                     ! i
      dneville = t(n)
      end
c
c     Neville -Algorithmus (modified)
c
      function dnevmod(x,y,x1,n,m)
      implicit double precision (a-h,o-z)
      parameter(mmax=20)
*-----------------------------------------------------------------------------
      dimension x(n),y(n),t(mmax)
      if(m.gt.mmax)stop ' DNEVMOD: m>mmax'
      call bisec(j,n,x1,x)
      mm=min(m,2*j,2*(n-j))
      mm=max(mm,8)
      jj=min(max(j-mm/2,1),n-mm+1)
c     print*,'JJ=',jj,x(jj),' N=',N
      dnevmod=dneville(x(jj),y(jj),t,x1,mm)
      end
C
      subroutine bisec(k,n,r,rmesh)
      implicit double precision(a-h,o-z)
      dimension rmesh(*)
      k=0
      if(r.lt.rmesh(1).or.r.gt.rmesh(n))return

      ib=1
      ie=n
      er = rmesh(ie)
      br = rmesh(ib)
c
 10   ih=ib+(ie-ib)/2
c
      hr = rmesh(ih)
      if(ie-ib.eq.1) then
        k=ib
        return 
      endif
      if (r.gt.hr) then
         ib=ih
         br=hr
         goto 10
      elseif (r.lt.hr) then
         ie=ih
         er=hr
         goto 10
      endif
      k=ih
      end
      
C
      SUBROUTINE QD(H,Y,Z,NDIM)
C
C       THE SUBROUTINE CALCULATES NDIM INTEGRALS
C       BETWEEN X1 AND XN OF FUNCTION Z ( N.LE.NDIM
C        XN=X1+(N-1)*H ) FOR THE SPECIAL CASE:
C       NDIM=2*ND+1 Y(1)=0 . Z(2*I+1) IS CALCULATED
C       BY THE SIMPSON'S METHOD AND Z(2*I+2)=Z(2*I+1)+
C       DELT(2*I+2) . TO INCREASE A ACCURACY IT IS
C       USED DELT(2*I+2)=DELT4(2*I+2)*(Z(2*I+3)-Z(2*I+1))/
C       (DELT4(2*I+2)+DELT4(2*I+3)) WHERE DELT4(2*I+2)
C       IS CALCULATED BY USING A CUBIC INTERPOLATION
C       BETWEEN Y(2*I),Y(2*I+1),Y(2*I+2),Y(2*I+3).
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION Y(*),Z(*)
C
      IF((NDIM/2+NDIM/2).EQ.NDIM)
     .     stop 'THE SUBPROGRAM QD WORKS ONLY IF NDIM=2*ND+1'
C
      c3=h/3.d0
      c24=h/24.d0
      z(1)=0.d0
      if(ndim.eq.1) go to 101
      y2=y(1)
      y3=y(2)
      y4=y(3)
      z(2)=c24*(10*y2+16*y3-2*y4)
      sum2=y2+4*y3+y4
      z(3)=c3*sum2
      if(ndim.eq.3) go to 101
      y5=y(4)
c
c the main loop of a integration
c
      do i=4,ndim-1,2
        y1=y3
        y2=y4
        y3=y5
        y4=y(i+1)
        delt3=y2+4*y3+y4
        sum2=sum2+delt3
        z(i+1)=c3*sum2
        delt4=-y1+13*y2+13*y3-y4
        if(i.lt.ndim-1)then
          y5=y(i+2)
          delt5=-y1+12*y2+26*y3+12*y4-y5
        else
          delt5=delt3*8
        endif
        if(delt5.gt.1.d-20)then
          delt=8*delt4*delt3/delt5
          z(i)=z(i-1)+c24*delt
        else
          z(i)=z(i-1)+c24*delt4
        end if
      enddo
 101  return
      end
      SUBROUTINE INTLOG(R1,DX,Y,Y0,S,YINT,RI)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION RI(*)
      DIMENSION Y(*)
      FI(P)=F0*P+.5D0*ALF*P**2+BET/3.D0*P**3+GAM/4.D0*P**4+DEL/5.D0*P**5
C  PERFORMS THE INTEGRAL OF Y IN THE INTERVAL (0,S)
C  USING SIMPSON'S RULE
C LOGARITHMIC NET RI(I)=R1*EXP((I-1)*DX)
C  IF (Y0.EQ.0.) Y(R)=A*R**ALF IS ASSUMED AND CALCULATED FROM
C  THE FIRST 2 NET POINTS
C     WRITE(6,*) S,R1,(Y(I),I=1,10)
      XS=LOG(S/R1)
      SS=1.D0+XS/DX+.00001d0
      I2=1+SS
      I1=I2-1
      I0=I1-1
      IM1=I1-2
      N=I1/4
      NMA=4*N+1
      IMA=NMA-3
      SUM=(Y(1)*R1-Y(NMA)*RI(NMA))*7.D0
c      print*,'START'
      DO  1  I=2,IMA,4
    1 SUM=SUM+32.D0*Y(I)*RI(I)+12.D0*Y(I+1)*RI(I+1)
     *  +32.D0*Y(I+2)*RI(I+2)+14.D0*Y(I+3)*RI(I+3)
      SUM=SUM*DX*2.D0/45.D0
c      print*,'CONT'
      IF(Y0.EQ.0.) GOTO 17
      FSR1=1.D0/DX*(-11.D0*Y(1)+18.D0*Y(2)-9.D0*Y(3)+2.D0*Y(4))/6.D0/
     * RI(1)
      SUM1=RI(1)*(Y0+Y(1))*.5D0-RI(1)**2/12.D0*FSR1
      GOTO 18
   17 SUM1=0.D0
      IF(Y(1).EQ.0.) GOTO 18
      ALF=LOG(Y(2)/Y(1))/DX
      RALF=R1**ALF
      A=Y(1)/RALF
      SUM1=A/(ALF+1.D0)*RALF*R1
C  19 FORMAT(' SUB. INTLOG: Y(1),Y(2),A,ALF,SUM1 =',1P5E12.5)
C     PRINT 19,Y(1),Y(2),A,ALF,SUM1
   18 CONTINUE
c      print*,'C33',I2,Y(I2),RI(I2)
      F2=Y(I2)*RI(I2)
c      print*,'C--'
      F1=Y(I1)*RI(I1)
      F0=Y(I0)*RI(I0)
c      print*,'C--'

      FM1=Y(IM1)*RI(IM1)
      FM2=Y(IM1-1)*RI(IM1-1)
      ALF=(2.D0*FM2-16.D0*FM1+16.D0*F1-2.D0*F2)/24.D0
      BET=(-FM2+16.D0*FM1-30.D0*F0+16.D0*F1-F2)/24.D0
      GAM=(-2.D0*FM2+4.D0*FM1-4.D0*F1+2.D0*F2)/24.D0
      DEL=(FM2-4.D0*FM1+6.D0*F0-4.D0*F1+F2)/24.D0
      XNMA=(NMA-1)*DX
      X0=(I0-1)*DX
      PNMA=(XNMA-X0)/DX
      PS=(XS-X0)/DX
      SUM2=(FI(PS)-FI(PNMA))*DX
      YINT=SUM+SUM1+SUM2
c      print*,'C44'
C     PRINT 101,XS,I2,NMA
*  101 FORMAT(' XS,I2,NMA : ',F15.10,2I5)
*  102 FORMAT(' RADII :',//,50(/5E15.8))
*  103 FORMAT(' INTEGRAND :',//,50(/5E15.8))
C     PRINT 102,RI
C     PRINT 103,Y
C     PRINT 104,F0,ALF,BET,GAM
*  104 FORMAT(' F0,ALF,BET,GAM,DEL : ',5E15.8)
*  105 FORMAT(' PNMA,PS ;',2E15.8)
C     PRINT 105,PNMA,PS
C     WRITE(6,100) SUM,SUM1,SUM2,YINT
      RETURN
*  100 FORMAT('SUM,SUM1,SUM2,YINT:',4D18.10)
      END

      double precision function au2a( )
      implicit double precision (a-h,o-z)
      parameter(a0=0.529177d0)
c
      au2a=a0
      end
      subroutine linit(array,leng)
C- Initializes logical array to zero
      logical array(leng)
c
      do i = 1,leng
        array(i) = .false.
      enddo
      end
      subroutine iinit(iarray,leng)
C- Initializes integer iarray to zero
      integer leng,iarray(leng)
c
      do i = 1,leng
        iarray(i) = 0
      enddo
      end
c
      subroutine ainit(array,leng)
C- Initializes real array to zero
      real array(leng)
c
      do i=1,leng
        array(i)=0.
      enddo
      end
c
      subroutine cinit(array,leng)
C- Initializes complex array to zero
      real array(2*leng)
C External calls:
      external  ainit

      call ainit(array,2*leng)
      return
      end
c
      subroutine dinit(array,leng)
C- Initializes double precision array to zero
      double precision array(leng)

      do i = 1,leng
        array(i) = 0.d0
      enddo
      end
c
      subroutine zinit(array,leng)
C- Initializes complex*16 array to zero
      complex*16 array(*)
c
      do i=1,leng
        array(i)=(0.d0,0.d0)
      enddo
      end
      subroutine incopy(n,x,incx,y,incy)
C- Copies an integer vector, x, to an integer vector, y.
C ----------------------------------------------------------------------
Ci Inputs:
Ci   n     :lenght of x
Ci   x     :vector to copy
Ci   incx  :incrementation for x
Ci   incy  :incrementation for y
Co Outputs:
Co   y     :result vector is stored in y
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer x(*),y(*)
      integer incx,incy,n
C Local parameters:
      integer i,ix,iy,m,mp1
c
      if(n.le.0) return
      if(incx.ne.1.or.incy.ne.1) then
c ----  code for unequal increments or equal increments not equal to 1
        ix = 1
        iy = 1
        if(incx.lt.0)ix = (-n+1)*incx + 1
        if(incy.lt.0)iy = (-n+1)*incy + 1
        do i = 1,n
          y(iy) = x(ix)
          ix = ix + incx
          iy = iy + incy
        enddo
      else
c ----  code for both increments equal to 1
        do i = 1,n
          y(i) = x(i)
        enddo
      endif
      end
c ------ ERRMSG ------
      subroutine errmsg(text,i)
c i<0 info
c i=0 warning
c i>0 fatal error
      character*(*)text
      character*12 fatal,warn
      data fatal/'Fatal error:'/,warn/'Warning:'/
c
      if(i.le.0)then
        print 1000,warn,text
c        if(iun.ne.6)write(iun,1000)warn,text
      else
        print 1000,fatal,text
c        if(iun.ne.6)write(iun,1000)fatal,text
        call endjob(-11,text)
      endif
 1000 format(/2x,a12,1x,a)
      end
c ------ INFOMSG ------
      subroutine infomsg(text)
      character*(*)text
c
      print 1000,text
c      if(iun.ne.6)write(iun,1000)text
 1000 format(2x,a)
      end
c ------ WARNING ------
      subroutine warning(a)
      character a*(*)
      print '(1x,a)',a
      end
      double precision function ddet33(a)
C- Determinant of 3x3 matrix
C ----------------------------------------------------------------------
Ci Inputs:
Ci   a :input matrix
Co Outputs:
Co   ddet33 :determinant
C ----------------------------------------------------------------------
      implicit double precision (a-h,o-z)
C Passed parameters:
      dimension a(*)
C Local parameters:
      dimension v(3)
c
      call cross(v,a(4),a(7))
      ddet33=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
      end
      double precision function trnt(a,b,c)
      implicit double precision (a-h,o-z)
      dimension a(3),b(3),c(3)
      trnt=a(1)*b(2)*c(3)+a(2)*b(3)*c(1)+a(3)*b(1)*c(2)
     $     -a(3)*b(2)*c(1)-a(2)*b(1)*c(3)-a(1)*b(3)*c(2)
      end
      SUBROUTINE EILANG(ANG,IG)
C      INCLUDE 'PREC.H'
      implicit double precision (a-h,o-z)
      PARAMETER (P=3.141592653589793D0)
      PARAMETER (Z=0.D0,H=P/2,H3=3*H,T=P/3,T2=T*2,T4=T*4,T5=T*5)
      DIMENSION C(3,32),ANG(3)
      DATA C/
     >Z,Z,Z, Z,P,Z, Z,P,P, P,Z,Z, P,H,H, Z,H,H3, P,H,H3, Z,H,H,
     >H,H,Z, H ,H ,P, H3,H,P, H3,H,Z, H3,P,Z, H,Z,Z, H3,Z,Z,
     >H,P,Z, Z,H,P, P,H,Z, Z,H,Z, P,H,P, H3,H,H3, H3,H,H, H,H,H,
     >H,H,H3,
     >T,Z,Z, T2,Z,Z, T4,Z,Z, T5,Z,Z, T4,P,Z, T5,P,Z, T,P,Z, T2,P,Z/
      ANG(1)=C(1,IG)
      ANG(2)=C(2,IG)
      ANG(3)=C(3,IG)
      end
      SUBROUTINE TURNM(OM,U)
C      INCLUDE 'PREC.H'
      implicit double precision (a-h,o-z)
      DIMENSION U(3,3),OM(3)
      SINA=SIN(OM(1))
      SINB=SIN(OM(2))
      SING=SIN(OM(3))
      COSA=COS(OM(1))
      COSB=COS(OM(2))
      COSG=COS(OM(3))
      U(1,1)=COSA*COSB*COSG-SINA*SING
      U(1,2)=-COSA*COSB*SING-SINA*COSG
      U(1,3)=COSA*SINB
      U(2,1)=SINA*COSB*COSG+COSA*SING
      U(2,2)=-SINA*COSB*SING+COSA*COSG
      U(2,3)=SINA*SINB
      U(3,1)=-SINB*COSG
      U(3,2)=SINB*SING
      U(3,3)=COSB
      END
      double precision function dnrm23(r)
C-  'norm'-square
      double precision r(3)

      dnrm23=r(1)*r(1)+r(2)*r(2)+r(3)*r(3)

      end

C*==lengths.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      INTEGER FUNCTION LENGTHS(A)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      CHARACTER*(*) A
C
C Local variables
C
      INTEGER J,L
      INTEGER LEN
C
C*** End of declarations rewritten by SPAG
C
C actual length of the string
      L = LEN(A)
      J = 0
      DO WHILE ( J.LT.L .AND. A(J+1:).NE.' ' )
         J = J + 1
      END DO
      LENGTHS = J
      END
C*==dpi.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
C
      DOUBLE PRECISION FUNCTION DPI()
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C Local variables
C
      LOGICAL FIRST
      DOUBLE PRECISION PI
      SAVE PI
C
C*** End of declarations rewritten by SPAG
C
      DATA FIRST/.TRUE./
C
      IF ( FIRST ) THEN
         FIRST = .FALSE.
         PI = ATAN(1.D0)*4.D0
C$$$        print *,pi
      END IF
      DPI = PI
      END
C*==dinv33.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      SUBROUTINE DINV33(MATRIX,IOPT,INVRSE,DET)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      DOUBLE PRECISION DET
      INTEGER IOPT
      DOUBLE PRECISION INVRSE(3,3),MATRIX(3,3)
C
C Local variables
C
      DOUBLE PRECISION DATAN
      DOUBLE PRECISION DDOT
      INTEGER I,J
      DOUBLE PRECISION XX
C
C*** End of declarations rewritten by SPAG
C
C- INVERTS 3X3 MATRIX
C ----------------------------------------------------------------
CI INPUTS
CI   INVERSE: INPUT MATRIX
CI   IOPT:  IF 0, USUAL INVERSE
CI             1, TRANSPOSE OF INVERSE
CI             2, 2*PI*INVERSE
CI             3, 2*PI*TRANSPOSE OF INVERSE
CO OUTPUTS
CO   INVERSE, AS MODIFIED ACCORDING TO IOPT
CO   DET:      DETERMINANT, OR DET/2*PI (SIGN OK ??)
CR REMARKS
CR   TO GENERATE RECIPROCAL LATTICE VECTORS, CALL DINV33(PLAT,3,RLAT)
C ----------------------------------------------------------------
C     IMPLICIT NONE
      CALL CROSS(INVRSE,MATRIX(1,2),MATRIX(1,3))
      CALL CROSS(INVRSE(1,2),MATRIX(1,3),MATRIX)
      CALL CROSS(INVRSE(1,3),MATRIX,MATRIX(1,2))
      DET = DDOT(3,MATRIX,1,INVRSE,1)
      IF ( DET.EQ.0.D0 ) CALL ENDJOB(10,'INV33: VANISHING DETERMINANT')
      IF ( IOPT.GE.2 ) DET = DET/(8*DATAN(1.D0))
      IF ( MOD(IOPT,2).EQ.0 ) THEN
         DO I = 1,3
            DO J = I + 1,3
               XX = INVRSE(I,J)
               INVRSE(I,J) = INVRSE(J,I)
               INVRSE(J,I) = XX
            END DO
         END DO
      END IF
      CALL DSCAL(9,1.D0/DET,INVRSE,1)
      END
C*==cross.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      SUBROUTINE CROSS(A,B,C)
C
C  cross product (ax,ay,az)=(bx,by,bz)*(cx,cy,cz)
C
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      DOUBLE PRECISION A(3),B(3),C(3)
C
C*** End of declarations rewritten by SPAG
C
C
      A(1) = B(2)*C(3) - B(3)*C(2)
      A(2) = B(3)*C(1) - B(1)*C(3)
      A(3) = B(1)*C(2) - B(2)*C(1)
C
      END
C*==dscal.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      SUBROUTINE DSCAL(N,DA,DX,INCX)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      DOUBLE PRECISION DA
      INTEGER INCX,N
      DOUBLE PRECISION DX(*)
C
C Local variables
C
      INTEGER I,M,MP1,NINCX
C
C*** End of declarations rewritten by SPAG
C
C
C     scales a vector by a constant.
C     uses unrolled loops for increment equal to one.
C     jack dongarra, linpack, 3/11/78.
C     modified 3/93 to return if incx .le. 0.
C     modified 12/3/93, array(1) declarations changed to array(*)
C
C
      IF ( N.LE.0 .OR. INCX.LE.0 ) RETURN
      IF ( INCX.EQ.1 ) THEN
C
C        code for increment equal to 1
C
C
C        clean-up loop
C
         M = MOD(N,5)
         IF ( M.NE.0 ) THEN
            DO I = 1,M
               DX(I) = DA*DX(I)
            END DO
            IF ( N.LT.5 ) RETURN
         END IF
         MP1 = M + 1
         DO I = MP1,N,5
            DX(I) = DA*DX(I)
            DX(I+1) = DA*DX(I+1)
            DX(I+2) = DA*DX(I+2)
            DX(I+3) = DA*DX(I+3)
            DX(I+4) = DA*DX(I+4)
         END DO
      ELSE
C
C        code for increment not equal to 1
C
         NINCX = N*INCX
         DO I = 1,NINCX,INCX
            DX(I) = DA*DX(I)
         END DO
         RETURN
      END IF
      END
C*==ddot.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      DOUBLE PRECISION FUNCTION DDOT(N,DX,INCX,DY,INCY)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER INCX,INCY,N
      DOUBLE PRECISION DX(*),DY(*)
C
C Local variables
C
      DOUBLE PRECISION DTEMP
      INTEGER I,IX,IY,M,MP1
C
C*** End of declarations rewritten by SPAG
C
C
C     forms the dot product of two vectors.
C     uses unrolled loops for increments equal to one.
C     jack dongarra, linpack, 3/11/78.
C     modified 12/3/93, array(1) declarations changed to array(*)
C
C
      DDOT = 0.0D0
      DTEMP = 0.0D0
      IF ( N.LE.0 ) RETURN
      IF ( INCX.EQ.1 .AND. INCY.EQ.1 ) THEN
C
C        code for both increments equal to 1
C
C
C        clean-up loop
C
         M = MOD(N,5)
         IF ( M.NE.0 ) THEN
            DO I = 1,M
               DTEMP = DTEMP + DX(I)*DY(I)
            END DO
            IF ( N.LT.5 ) THEN
               DDOT = DTEMP
               GOTO 99999
            END IF
         END IF
         MP1 = M + 1
         DO I = MP1,N,5
            DTEMP = DTEMP + DX(I)*DY(I) + DX(I+1)*DY(I+1) + DX(I+2)
     &              *DY(I+2) + DX(I+3)*DY(I+3) + DX(I+4)*DY(I+4)
         END DO
         DDOT = DTEMP
      ELSE
C
C        code for unequal increments or equal increments
C          not equal to 1
C
         IX = 1
         IY = 1
         IF ( INCX.LT.0 ) IX = (-N+1)*INCX + 1
         IF ( INCY.LT.0 ) IY = (-N+1)*INCY + 1
         DO I = 1,N
            DTEMP = DTEMP + DX(IX)*DY(IY)
            IX = IX + INCX
            IY = IY + INCY
         END DO
         DDOT = DTEMP
         RETURN
      END IF
99999 CONTINUE
      END
C*==ilun.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      FUNCTION ILUN(I)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER I
      INTEGER ILUN
C
C*** End of declarations rewritten by SPAG
C
      ILUN = 6
      END
C*==endjob.f    processed by SPAG 6.05Rc at 10:56 on  6 Mar 2001
      SUBROUTINE ENDJOB(J,TEXT)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER J
      CHARACTER*(*) TEXT
C
C*** End of declarations rewritten by SPAG
C
C-----------------
      WRITE (*,*) ' === STOP === ',J,' ',TEXT
      STOP
      END
      SUBROUTINE WKINIT(NSIZE)
      IMPLICIT  INTEGER(O)
c      include 'COMPUTER.H'
      double precision
     $ qdummy8
      COMMON /q_LMTO_/ qdummy8,ofree,LIMIT
      data  L_WORD/4/, L_WORD_I/1/, L_WORD_R/1/, L_WORD_RR/2/,
     $     L_WORD_C/2/, L_WORD_CC/4/

C ----- DEFINE STORAGE SIZE ------
C  START OF FIRST ARRAY AND MAX NUMBER TO BE DEFINED:
      LIMIT=NSIZE
      OFREE=5
      RETURN
C ------ SUBROUTINES TO DEFINE ARRAYS OF VARIOUS TYPES -----
      ENTRY DEFI(ONAME,LENG)
         LENGTH=LENG*L_WORD_I
         i8=0
         GOTO 10
      ENTRY DEFR(ONAME,LENG)
         LENGTH=LENG*L_WORD_R
         i8=0
         GOTO 10
      ENTRY DEFC(ONAME,LENG)
         LENGTH=LENG*L_WORD_C
         i8=0
         GOTO 10
      ENTRY DEFRR(ONAME,LENG)
         LENGTH=LENG*L_WORD_RR
         i8=1
         GOTO 10
      ENTRY DEFDR(ONAME,LENG)
         LENGTH=LENG*L_WORD_RR
         i8=1
         GOTO 10
      ENTRY DEFCC(ONAME,LENG)
         LENGTH=LENG*L_WORD_CC
         i8=1
  10  IF(LENGTH.LT.0) call endjob(10,'**** LENGTH OF ARRAY NEGATIVE')
      IF(LENGTH.EQ.0) LENGTH=1
      if(i8.eq.1.and.mod(ofree,2).eq.0)ofree=ofree+1
      oname=ofree
      ofree=ofree+length
c$$$      if(mod(ofree,2).eq.0)ofree=ofree+1
      if(ofree.ge.LIMIT)then
        print 1,'def0',ofree,LIMIT
        call endjob(5,' NO MEMORY')
      endif
1     format(/45('*')/'  In ',a,' memory pool exhausted',/
     >     '  N___=',I7,' NMAX_= ',i7,/45('*'))
      return
c
      ENTRY RLSE(ONAME)
      IF(ONAME.GT.LIMIT)
     .         call endjob(10,'**** RESET POINTER GT LIMIT')
      IF(ONAME.LT.3) call endjob(10,'**** RESET POINTER LT 3')
      OFREE=ONAME
      RETURN
      END
 
c ! 03-03-94 - PERLOV : NDP cann'n calculate (0.0)**(1./3.)
c !                         changed by (0.0 + 1.d-42)**(1./3.)
C **********************************************************************
C
C HARTREE FOCK DIRAC SLATER          J P DESCLAUX     CEA PARIS 1969
C MODIFIE JUILLET 1970
C
C **********************************************************************
      subroutine rhfds(iz,izcore,ro,rom,dr00)
      IMPLICIT DOUBLE PRECISION(D)
      DOUBLE PRECISION Z
      CHARACTER*4 BAR1,BAR2,BAR,TITRE
      COMMON/UKP/NCORB(30),NWF(30)
      COMMON/STAND/ ISTAND
      COMMON DEN(30),DQ1(30),DFL(30),NQN(30),NQL(30),NK(30),NMAX(30),NEL
     1(30),NORB,mag(30)
      COMMON/DIRA/DV(251),DR(251),DP(251),DQ(251),DPAS,Z,NSTOP,NES,TETS,
     1NP,NUC
      COMMON/PS2/DEXV,DEXE,DCOP,TITRE(30),BAR(10),TEST,TESTE,TESTY,TESTV
     1,NITER,ION,ICUT,IPRAT
      COMMON/DEUX/DVN(251),DVF(251),D(251),DC(251),DGC(251,30),DPC(251,3
     10)
      DOUBLE PRECISION SIGW(251),ro(251),rom(251)
      CHARACTER*40 buf
      DATA BAR1/'CORE'/,BAR2/'AMTC'/,SIGW/251*0.D0/
C
1000  FORMAT(L7)
 1001 FORMAT (5X,10A4/)
 1002 FORMAT (' ITER',4X,'DVMAX',10X,'R',14X,'VN-1',13X,'VN',10X,'DEMAX'
     1,6X,'DPMAX',9X,'PN-1',13X,'PN')
 1003 FORMAT (I5,1PE11.2,3(1PE16.6),2(1PE11.2),2(1PE16.6))
 1004 FORMAT (' Number of iterations is exceeded',i4)
c$$$ 1005 FORMAT ('OPb.  |HEPgiq      zAHqT. BAl.      R**2          R')
 1005 FORMAT ('Level  Energy      Occup. Val.      R**2          R')
 8001 FORMAT(I2,A2,1X,1PE14.7,2X,I2,3X,I2,2X,1PE14.7,2X,1PE14.7)
 1006 FORMAT (I3,A2,6(1PE18.7))
*1005 FORMAT (12X,'|HEPgiq ',12X,'(R4)',14X,'(R2)',14X,'(R)',15X,'(R-1)'
*    1,13X,'(R-3)'/)
 1007 FORMAT (2X,'ET=',1PE12.7,5X,'EC=',1PE12.7,5X,'EE=',1PE12.7,5X,
     1'EX=',1PE14.7,5X,'EN=',1PE14.7)
 1008 FORMAT (1H0,47X,'iHTEgPAly pEPEKPyTiq      '/)
 1009 FORMAT (34X,I1,A2,I3,A2,F19.7)
 1010 FORMAT ('  NSTOP=',I4,'  dlq OPbiTAli   ',I3,A2)
 1011 FORMAT (1P5E14.7)
 1012 FORMAT(11A4,I2,8X,1PE17.8,I1)
c 1012 FORMAT(2e15.8,2I3,11A4)
 1013 FORMAT(11A4,I2,I4,21X,I1)
 1014 FORMAT('EOD')
      NSTOP=1
 1    if(iz.ge.0)then
       CALL INSLD(NPCH,IZ,izcore)
      else
       CALL INSLD(NPCH,IZ,izcore)
      endif
 2    ITER=1
      DO 3 I=1,NP
        DO 3 J=1,NORB
          DGC(I,J)=0.D0
 3        DPC(I,J)=0.D0
      WRITE(8,1001) BAR
      write(8,1002)
      N=-(ION+1)
 4    DO 5 I=1,NP
 5      D(I)=0.D0
      TETS=TEST
      YMAX=0.
      VMAX=0.
      EMAX=0.
C PE{EHiE uPABHEHiq diPAKA dlq KAvdOj OPbiTAli
C **********************************************************************
      DO 25  J=1,NORB
      DE=DEN(J)
 16   CALL RESLD (NQN(J),NQL(J),NK(J),IMAX,DEN(J),DFL(J),DQ1(J),J)
      IF (NSTOP.EQ.0) GO TO 18
      IF (NSTOP.NE.362.OR.ITER.GE.10.OR.TETS.GT.TEST) GO TO 17
      TETS=TESTV
      GO TO 16
 17   WRITE(8,1010) NSTOP,NQN(J),TITRE(J)
      GO TO 1
 18   VAL=ABS((DEN(J)-DE)/DE)
      IF(VAL.GT.EMAX) EMAX=VAL
      NMAX(J)=IMAX
      DO 24  I=1,NP
      VAL=DGC(I,J)-DP(I)
      IF (ABS(DP(I)).GT.1.D0) VAL=VAL/DP(I)
      IF (ABS(VAL).LT.ABS(YMAX)) GO TO 21
      YMAX=VAL
      Y=DP(I)
      YN=DGC(I,J)
 21   VAL=DPC(I,J)-DQ(I)
      IF (ABS(DQ(I)).GT.1.D0) VAL=VAL/DQ(I)
      IF (ABS(VAL).LT.ABS(YMAX)) GO TO 23
      YMAX=VAL
      Y=DQ(I)
      YN=DPC(I,J)
 23   DGC(I,J)=DP(I)
      DPC(I,J)=DQ(I)
 24   D(I)=D(I)+NEL(J)*(DP(I)*DP(I)+DQ(I)*DQ(I))
 25   CONTINUE
      CALL POTSL (DC,D,DP,DR,DPAS,DEXV,Z,NP,ION,ICUT)
      IF (NUC.LE.0) GO TO 31
      DO 29 I=1,NUC
 29   DC(I)=DC(I)+Z/DR(I)+Z*((DR(I)/DR(NUC))**2-3.D0)/(DR(NUC)+DR(NUC))
 31   DO 33 I=1,NP
      DVAL=ABS(DC(I)-DV(I))
      IF ((DR(I)*DC(I)).LE.N) DVAL=-DVAL/DC(I)
      IF (DVAL.LE.VMAX) GO TO 33
      VMAX=DVAL
      J=I
 33   CONTINUE
      write(8,1003) ITER,VMAX,DR(J),DV(J),DC(J),EMAX,YMAX,YN,Y
c      print 1601,iter,vmax
1601  format(' Iteration :',i3,' Precision:',g10.3)
c$$$      call textout(20,20,0,7,40,buf)
      IF(TETS.LE.TEST.AND.EMAX.LE.TESTE.AND.VMAX.LE.TESTV.AND.YMAX.LE.TE
     1STY) GO TO 61
      ITER=ITER+1
      IF(ITER.LE.NITER) GO TO 35
      WRITE(8,1004) NITER
      NSTOP=2
      GO TO 61
C pOTEHciAl dlq ClEdu`}Ej iTEPAcii
C **********************************************************************
 35   DVAL=1.D0-DCOP
      DO 43 I=1,NP
        DVN(I)=DV(I)
        DVF(I)=DC(I)
 43     DV(I)=DVAL*DV(I)+DCOP*DC(I)
      GO TO 4
   61 IF(NPCH.nE.0)then
        WRITE (7,3004) BAR
 3004   FORMAT(10A4)
        WRITE(7,3002) (DR(KK),D(KK),KK=1,251)
 3002   FORMAT(4D15.8)
      endif
      WRITE(ilun(16),1005)
      WRITE(8,1005)
C CPEdHiE zHA~EHiq R
C **********************************************************************
      DO 67 I=1,NP
        DVF(I)=DC(I)
 67     DQ(I)=0.
      DVAL=0.
      DO 91 I=1,NORB
        IM=NMAX(I)
        DVAL=DVAL+NEL(I)*DEN(I)
        DO 73  J=1,IM
 73       DC(J)=DGC(J,I)*DGC(J,I)+DPC(J,I)*DPC(J,I)
        L=5
        IF (IABS(NK(I)).EQ.1) L=L-1
        DO 88 J=1,L
          DP(J)=DFL(I)+DFL(I)
          if(j.EQ.1)THEN
            N=4
          ELSEIF(J.EQ.2)THEN
            N=2
          ELSEIF(J.EQ.3)THEN
            N=1
          ELSEIF(J.EQ.4)THEN
            N=-1
          ELSE
            N=-3
          ENDIF
          CALL SOMM (DR,DC,DQ,DPAS,DP(J),N,IM)
 88     continue
      WRITE(8,8001)NQN(I),TITRE(I),DEN(I)*2,NEL(I),NCORB(I),
     >             (DP(J),J=2,3)
      WRITE(ilun(16),8001)NQN(I),TITRE(I),DEN(I)*2,NEL(I),NCORB(I),
     >             (DP(J),J=2,3)
 91   CONTINUE
C pOlHAq |HEPgiq uCPEdHEHHAq pO CfEPE
C **********************************************************************
      DC(1)=1.D0
      DO 95 I=1,NP
 95     DP(I)=D(I)/DR(I)
      IF (NUC.LE.0) GO TO 99
      DO 97 I=1,NUC
 97    DP(I)=D(I)*(3.D0-DR(I)*DR(I)/(DR(NUC)*DR(NUC)))/(DR(NUC)+DR(NUC))
      DC(1)=4.D0
 99   CALL SOMM (DR,DP,DQ,DPAS,DC(1),0,NP)
      DO 105  I=1,NP
        DP(I)=D(I)*DVF(I)
 105    D(I)=D(I)*((D(I)*DR(I)+1.d-42)**(1.D0/3.D0))
      DC(2)=3.D0
      DC(3)=1.D0
      IF (NUC.NE.0) DC(3)=4.D0
      CALL SOMM (DR,DP,DQ,DPAS,DC(3),0,NP)
      CALL SOMM (DR,D,DQ,DPAS,DC(2),-1,NP)
      DC(2)=-3.D0*DC(2)/(105.27578D0**(1.D0/3.D0))
      DC(1)=-Z*DC(1)
      DC(4)=DVAL-DC(3)
      DVAL=DVAL+(DC(1)-DC(3)+(DEXE-DEXV)*DC(2))/2.D0
      DC(3)=(DC(3)-DC(1)-DEXV*DC(2))/2.D0
      DC(2)=DC(2)*DEXE/2.D0
*     WRITE(8,1007) DVAL,DC(4),DC(3),DC(2),DC(1)
      IF(NORB.EQ.1) GO TO 205
*     WRITE(8,1001) BAR
*     WRITE(8,1008)
C iHTEgPAly pEPEKPyTiq
C **********************************************************************
      DO 115  I=2,NORB
      K=I-1
      DO 115  J=1,K
      IF(NQL(I).NE.NQL(J).OR.NK(I).NE.NK(J)) GO TO 115
      IM=NMAX(J)
      IF(NMAX(I).LT.IM) IM=NMAX(I)
      DO 111  L=1,IM
      DQ(L)=DPC(L,I)*DPC(L,J)
 111  DC(L)=DGC(L,I)*DGC(L,J)
      DVAL=DFL(I)+DFL(J)
      CALL SOMM (DR,DC,DQ,DPAS,DVAL,0,IM)
      IF(DVAL.GT.1.d-3)
     *WRITE(8,1009) NQN(I),TITRE(I),NQN(J),TITRE(J),DVAL
 115  CONTINUE
205	continue
c 205  IF (ISTAND.NE.0)CALL CDSLD (TITRE,BAR)
C **** PAC~ET OCTOBHOj  i pOlHOj plOTHOCTEj
C     WRITE(8,4003)
      DO 701 J=1,NORB
      IF(NWF(J).EQ.0) GO TO 701
      DENER=DEN(J)*2
      WRITE(41,7001) NQN(J),NQL(J),DENER,DR(1)
      WRITE(41,4002)(DGC(I,J),I=1,250)
701   CONTINUE
7001  FORMAT(I3,1X,I3,1X,G14.7,1X,G14.7)
4003  FORMAT(//' RADII AND CORE-CHARGE DENSITY'//)
      IFORM=1
      ICEL=0
      DO 404 J=1,NORB
      IF(NCORB(J).EQ.0) GO TO 404
      ICEL=ICEL+NEL(J)
  404 CONTINUE
*      WRITE(7,1012) BAR,BAR1,ICEL,DR(1),IFORM
C      WRITE(7) BAR,BAR1,ICEL,DR(1),IFORM
c      WRITE(7,1012) DR(1),dpas,ICEL,IFORM,BAR,BAR1
      DR00=DR(1)
      DO  I=1,251
c        ROC(I)=0.D0
        RO(I)=0.D0
        ROM(I)=0.D0
      ENDDO
      DO J=1,NORB
C       IF(NCORB(J).EQ.1) GO TO 400
        DO  I=1,251
c$$$          IF(NCORB(J).EQ.0)then
c$$$            ROC(I)=ROC(I)+NEL(J)*(DGC(I,J)*DGC(I,J)+DPC(I,J)*DPC(I,J))
c$$$          ENDIF
          RO(I)=RO(I)+NEL(J)*(DGC(I,J)*DGC(I,J)+DPC(I,J)*DPC(I,J))
          ROM(I)=ROM(I)+MAG(J)*(DGC(I,J)*DGC(I,J)+DPC(I,J)*DPC(I,J))
        ENDDO
      ENDDO
c$$$C 400 CONTINUE
c$$$C     WRITE(8,4001)(DR(I),SIGW(I),I=1,250)
c$$$ 4001 FORMAT(6(1PE16.9))
c$$$*      WRITE(7,4002)(SIGW(I),I=1,250)
c$$$c$$$      do i=1,251
c$$$c$$$        ro(i)=roc(i)
c$$$c$$$      enddo
c$$$c     WRITE(7)(SIGW(I),I=1,250)
 4002 FORMAT(5(1PE16.9))
c$$$C     WRITE(8,4004)
c$$$4004  FORMAT(//' RADII AND TOTAL CHARGE DENSITY'//)
c$$$      DO 402 J=1,NORB
c$$$      IF(NCORB(J).EQ.0) GO TO 402
c$$$      DO 403 I=1,251
c$$$  403 SIGW(I)=SIGW(I)+NEL(J)*(DGC(I,J)*DGC(I,J)+DPC(I,J)*DPC(I,J))
c$$$  402 CONTINUE
c$$$C     WRITE(8,4001)(DR(I),SIGW(I),I=1,250)
c$$$      WRITE(7) BAR,BAR2,ION,IZ,IFORM
c$$$*      WRITE(7,1013) BAR,BAR2,ION,IZ,IFORM
c$$$*      WRITE(7,4002)(SIGW(I),I=1,250)
c$$$      WRITE(7)(SIGW(I),I=1,250)
c$$$      do i=1,251
c$$$        SIGW(I)=0
c$$$      enddo
c$$$      do j=1,norb
c$$$        do i=1,251
c$$$          SIGW(I)=SIGW(I)+mag(J)*(DGC(I,J)*DGC(I,J)+DPC(I,J)*DPC(I,J))
c$$$        enddo
c$$$      enddo
c$$$      WRITE(7)(SIGW(I),I=1,250)
c$$$      WRITE(7)'EOD'
c$$$*      WRITE(7,1014)
c$$$      write(7)norb
c$$$*      write(7,4009)(nqn(j),nql(j),ncorb(j),den(j)*2,j=1,norb)
c$$$      write(7)(nqn(j),nql(j),ncorb(j),den(j)*2,j=1,norb)
c$$$4009  format(4(i2,i3,i2,g13.5))
C    **************************************************************
      END
      SUBROUTINE INSLD(NPCH,IZ,IZCORE)
C
C ~TEHiE CTAPTOBOgO pOTEHciAlA
C pOClEdHqq KAPTA dOlvHA COdEPvATx 4 zBEzdO~Ki
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      DOUBLE PRECISION Z
      CHARACTER*4 TTIRE,ITXCH,BAR1,ENDA,BAR,TITRE
      COMMON/HLS/RWAT,IWAT,IEX
      COMMON/UKP/NCORB(30),NWF(30)
      COMMON/STAND/ ISTAND
      COMMON DEN(30),DQ1(30),DFL(30),NQN(30),NQL(30),NK(30),NMAX(30),NEL
     1(30),NORB,mag(30)
      COMMON/DIRA/DV(251),DR(251),DP(251),DQ(251),DPAS,Z,NSTOP,NES,TETS,
     1NP,NUC
      COMMON/PS2/DEXV,DEXE,DCOP,TITRE(30),BAR(10),TEST,TESTE,TESTY,TESTV
     1,NITER,ION,ICUT,IPRAT
      DIMENSION TTIRE(9)
      DIMENSION ITXCH(3,2)
      DATA TTIRE/'S   ','P*  ','P   ','D*  ','D   ','F*  ','F   ','G*  '
     1,'G   '/,BAR1/'RHFS'/,ENDA/'****'/
      DATA ITXCH/'BART','H-HE','DIN ','X-AL','PHA ','    '/
 1000 FORMAT (7A4)
 1001 FORMAT(14I3)
 1101 FORMAT(14I5)
 1002 FORMAT (6E12.8)
 1003 FORMAT (E12.8,I1,3I2)
 1004 FORMAT (8F9.4)
 1005 FORMAT(2F10.6)
 1006 FORMAT(' Exchange: Barth-Hedin   ')
 2000 FORMAT(' Atomic number  ',I3,'   Ionicity  ',I2
     *     /1X,'Maximal number of iterations ',I4
     *     /' Precision in energy ',1PE9.2
     *     /14x,'wave function ',1PE9.2/14X,'potential',1PE9.2/)
 2001 FORMAT (' NORB=',I3,'too large ************** ')
 2002 FORMAT (' Input error       ',E15.8,I1,2I2)
 2003 FORMAT (' Erroneous number of electrons  **************')
 2004 FORMAT (5X,'Level ',5X,'Occupancy ',5X,'starting energy  '/)
 2005 FORMAT (7X,I1,A2,2I8,1PE23.7)
 2006 FORMAT (' Error in potential   ')
 2007 FORMAT (' Bad configuration ')
 2008 FORMAT (' Integration will be in ',i3,' points'/
     $     ' the first point is ',f7.4,'/',i2,'  step is ',f7.4,/)
c$$$ 2008 FORMAT ('  iHTEgPiPOBAHiE budET pPOBOdiTxCq B  ',I3,
c$$$     = ' TO4KAX ',/,'    pEPByj 4lEH PABEH    ',F7.4,'/'
c$$$     =,I2,' {Ag PABEH   ',F7.4,/)
 2009 FORMAT (' In the program RESLD the relative precision in energy',
     $     ' is ',1pe9.2,/' the number of attempts ',i3,/) !/,'
*    2IDEP=',I3,2X,'ICUT=',I3,2X,'IPRAT=',I3,2X,'ION=',I3,2X,'IWAT=',
*    1I3,2X,'RWAT=',E10.3,/)
c$$$ 2009 FORMAT ('       pPi  PE{EHii uPABHEHiq diPAKA ',
c$$$     */,'    OTHOCiTElxHAq TO4HOCTx pO |HEPgii ',1PE9.2,
c$$$     */,'                4iClO pOpyTOK         ',I3,/)
 2010 FORMAT ('  NP=',I3,' is too large *******')
 2011 FORMAT (20X,10A4,//,5X,'Starting potential * R'/,
     15(2X,F9.4))
 2012 FORMAT ('  Exchange: Slater X-alpha, DEXV=',F8.4 ,
     * '     DEXE=',F8.4,/)
 2013 FORMAT (' Error in  IDEP *********************')
 2014 FORMAT (' Pratt''s mixing is used  '/)
 2015 FORMAT (' Potential is mixed with  ADMIX=',1PE14.7,/)
 2016 FORMAT (10X,' Finite nucleus'/)
 2017 FORMAT (' Error in atomic weight  **************** ')
 2018 FORMAT (' The next case ')
*      IF (NSTOP.EQ.0) GO TO 2
    1 CONTINUE
      IF (NSTOP-1) 598,3,598
 3    DPAS=0.05D0
      DR1=1d-2!0.01D0
      NES=15
      NITER=50
      TESTE=5.d-06
      TESTY=1.d-05
      TESTV=1.d-05
      TEST=1.d-07
      NP=251
      NSTOP=30
      DEXV=1.D0
      DEXE=1.5D0
      DCOP=0.3
      DVC=137.0359895d0                 !137.0373D0
      DSAL=DVC+DVC
      IZ1=0
      ION1=0
      NUC1=-1
      ION=0
      IDEP=0
      ICUT=0
      IPRAT=1
      IEX=0
      I=0
      J=0
      L=0
      K=0
      NUC=0
      NPCH=0
      IWAT=0
      dval=0.d0
      do i=1,7
        bar(i)='----'
      enddo
c      READ (5,1000) (BAR(I),I=1,7)
C
C BAR TITRE COdEPvAT        24 CiMBOlA
C **********************************************************************
      IF (BAR(1).EQ.ENDA) CALL EXIT_A
      I=0
      call deforb(NQN,NK,NEL,NCORB,norb,iz,izcore,mag)
C
C IZ ATOMHyj HOMEP      ION=IZ-~iClO |lEKTPOHOB
C NORB ~iClO OPbiTAlEj    IDEP dOlvH byTx PABEH 0 ili 1
C IDEP=0 CTAPTOByj pOTHciAl =pOTEHciAl TOMACA-fEPMi
C IDEP=1 CTAPTOByj pOTEHciAl ~iTAETCq C KAPT
C ECli ICUT HOlx UL ON CORRIGE LE POTENTIEL EN -(ION+1)/R
C ECli IPRAT HOlx iCpOlxzyETCq pPOcEduPA pPATTA
C ECli IEX HOlx - ObMEH  BARTH-HEDIN'A
C I ~iClO TO~EK dlq iHTEgPiPOBAHiq (251 ECli I=0
C J ~iClO pOpyTOK pOdOgHATx |HEPgi`(15 ECli J=0
C K ~iClO iTEPAcij (50 ECli K=0
C L=0 CTAHdAPTHO BybPAHHAq TO~HOCTx
C KOHE~HyE PAzMEPy qdPA ECli NUC pOlOviTElEH
C **********************************************************************
      DO 30 ICH=8,10
        IMCH=ICH-7
   30   BAR(ICH)=ITXCH(IMCH,IEX+1)
      IF(NORB.LE.NSTOP) GO TO 4
      WRITE(8,2001) NORB
      GO TO 598
 4    IF (I.LE.0) GO TO 6
      I=2*(I/2)+1
      IF (I.LE.NP) GO TO 5
      WRITE(8,2010) I
      GO TO 598
 5    NP=I
 6    IF (J.GT.0) NES=J
      IF (K.GT.0) NITER=K
      IF (IEX.EQ.0) GO TO 7
*      READ (5,1005) DEXV,DEXE
C
C DEXV KO|fficiEHT pPi ObMEHHOM pOTEHciAlE      DEXV=1. dlq  SLATER
C DEXE KO|fficiEHT pPi ObMEHHOj |HEPgii
C DEXV dOlvEH byTx PABEH 2.*DEXE/3. dlq TEOPEMy BiPiAlA
C **********************************************************************
 7    IF (L.EQ.0) GO TO 8
*      READ (5,1002) DPAS,DR1,TEST,TESTE,TESTY,TESTV
C
C DPAS |KCp.{Ag    DR1 OpPEdElqET pEPBu` TO~Ku  =DR1/IZ
C TEST TO~HOCTx pO |HEPgii dlq RESLD
C TESTE KPiTEPij CAMOCOglACOBAHiq OdHO|lEKTPOHHyX |HEPgij
C TESTE KPiTEPij CAMOCOglACOBAHiq BOlHOByX fuHKcij
C TESTE KPiTEPij CAMOCOglACOBAHiq pOTEHciAlA
C **********************************************************************
 8    IF (IPRAT.EQ.0) GO TO 9
       DCOP=0.3
*      IF(ISTAND.NE.0)READ (5,1002) DCOP,RWAT
C
C VI(N+1)=(1.-DCOP)*VI(N)+DCOP*VF(N)
C **********************************************************************
 9    Z=DFLOAT(IZ)
      IF (NUC.LE.0) GO TO 16
*      IF (ISTAND.NE.0)READ (5,1002) DVAL
C
C DVAL ATOMHAq MACCA ECli NUC pOlOviTElEH
C **********************************************************************
      DVAL=Z*(DVAL**(1.D0/3.D0))*2.2677D-05/EXP(4.D0*DPAS)
      IF (DVAL-DR1) 11,11,12
 11   DR1=DVAL
      NUC=5
      GO TO 16
 12   DVAL=DVAL*EXP(4.D0*DPAS)
      DO 13 I=6,NP
      D1=DR1*EXP(DFLOAT(I-1)*DPAS)
      IF (D1.GE.DVAL) GO TO 14
 13   CONTINUE
      WRITE(8,2017)
      GO TO 598
 14   NUC=I
      DR1=DR1*DVAL/D1
 16   WRITE(8,2000) IZ,ION,NITER,TESTE,TESTY,TESTV
      WRITE(8,2008) NP,DR1,IZ,DPAS
***** WRITE(8,2009) TEST,NES,IDEP,ICUT,IPRAT,ION,IWAT,RWAT
      WRITE(8,2009) TEST,NES
      IF(IEX.EQ.0) WRITE(8,1006)
      IF(IEX.NE.0) WRITE(8,2012) DEXV,DEXE
      K=0
      DVAL=Z*Z/(DVC*DVC)
      IF (NUC.LE.0) GO TO 17
      WRITE(8,2016)
C
C dAHHyE pO OPbiTAlqM
C **********************************************************************
*17   WRITE(8,2004)
 17   CONTINUE
*      IF(ISTAND.EQ.0) READ(5,1000) BARRAB
      DO 24 I=1,NORB
*      DEN(I)=0.D0
*      IF(ISTAND.NE.0)READ (5,1003) DEN(I),NQN(I),NK(I),NEL(I),NCORB(I)
*      IF(ISTAND.EQ.0)READ (5,1101) NQN(I),NK(I),NEL(I),NCORB(I),NWF(I)
C
C DEN |HEPgiq OPbiTAli B Ed.lAHdAu (<0)
C NQN glABHOE KBAHTOBOE ~iClO      NK KBAHTOBOE ~iClO KAppA
C NEL zAHqTOCTx OPbiTAli NCORB pPizHAK BAlEHTHOCTi(=1 dlq BAl.
C **********************************************************************
      K=K+NEL(I)
*     IF (DEN(I)) 19,18,18
 18   DEN(I)=-Z*Z/(4.D0*NQN(I)*NQN(I))
 19   NQL(I)=IABS(NK(I))
      IF (NK(I).LT.0) NQL(I)=NQL(I)-1
      IF (NUC.GT.0) GO TO 21
      DFL(I)=NK(I)*NK(I)
      DFL(I)=SQRT(DFL(I)-DVAL)
      GO TO 22
   21 NKAI=IABS(NK(I))
      DFL(I)=DFLOAT(NKAI)
 22   L=2*IABS(NK(I))
      IF (NQL(I).LT.NQN(I).AND.NEL(I).LE.L.AND.NQN(I).GT.0.AND.NQL(I).LE
     1.4) GO TO 23
      WRITE(8,2002) DEN(I),NQN(I),NQL(I),J,NEL(I)
      GO TO 598
 23   J=NQL(I)+IABS(NK(I))
      TITRE(I)=TTIRE(J)
 24   write(8,2005) NQN(I),TITRE(I),NCORB(I),NEL(I),DEN(I)
      IF (K.EQ.(IZ-ION)) GO TO 25
      WRITE(8,2003)
      GO TO 598
 25   IF (IPRAT) 27,26,27
 26   WRITE(8,2014)
      GO TO 28
 27   WRITE(8,2015) DCOP
 28   IF (NUC.NE.NUC1) GO TO 29
      IF (IZ.EQ.IZ1.AND.ION.EQ.ION1) GO TO 101
      IF(IZ.EQ.IZ1) GO TO 35
 29   continue
      DR(1)=DR1/Z
      DO 31  I=2,NP
 31   DR(I)=DR(1)*EXP(DFLOAT(I-1)*DPAS)
C
C CTAPTOByj pOTEHciAl
C **********************************************************************
 35   VAL=-ION-1
      IF (IDEP.EQ.1) GO TO 58
      IF (IDEP.EQ.0) GO TO 45
      WRITE(8,2013)
      GO TO 598
 45   IF(IZ.EQ.IZ1.AND.ION.GT.ION1.AND.NUC.EQ.NUC1) GO TO 60
      DO47  I=1,NP
      R=DR(I)
 47   DV(I)=FPOT(R,Z,VAL)
      IF(IWAT.EQ.0) GO TO 68
      DO 70 I=1,NP
      IF(DR(I).GT.RWAT) GO TO 71
      DV(I)=DV(I)+ION/RWAT
      GO TO 70
   71 DV(I)=DV(I)+ION/DR(I)
   70 CONTINUE
   68 IF (NUC.LE.0) GO TO 101
      DO 48 I=1,NUC
 48   DV(I)=DV(I)+Z/DR(I)+Z*((DR(I)/DR(NUC))**2-3.D0)/(DR(NUC)+DR(NUC))
      GO TO 101
 58   continue
*      READ(5,1004) (DV(I),I=1,NP)
C
C ~TEHiE CTAPTOBOgO pOTEHciAlA   (EN U.A. ET NEGATIF) ECli IDEP=1
C **********************************************************************
      WRITE(8,2011) BAR,(DV(I),I=1,NP)
      DVAL=-Z/DV(1)
      IF (NUC.GT.0) DVAL=1.D0
      DO 59  I=1,NP
 59   DV(I)=DV(I)*DVAL/DR(I)
 60   IF (ICUT.NE.0) GO TO 67
      DO 65  I=1,NP
      IF ((DR(I)*DV(I)).GT.VAL) DV(I)=VAL/DR(I)
 65   CONTINUE
 67   VAL=Z+DV(1)*DR(1)
      IF (NUC.GT.0) VAL=Z+DV(NUC)*DR(NUC)
      IF (ABS(VAL).LT.0.1) GO TO 101
      WRITE(8,2006)
      GO TO 598
 101  IF(NORB.EQ.1) GO TO 107
      DO 105  I=2,NORB
      K=I-1
      DO 105  J=1,K
      IF (NQN(I).NE.NQN(J).OR.NK(I).NE.NK(J)) GO TO 105
      WRITE(8,2007)
      GO TO 598
 105  CONTINUE
 107  IZ1=IZ
      ION1=ION
      NUC1=NUC
      DO 119 I=1,NORB
      NMAX(I)=NP
      L=1
      J=NQN(I)-NQL(I)
      IF((J-2*(J/2)).EQ.0) L=-L
      NKAI=IABS(NK(I))
      DQ1(I)=DFLOAT(L*NK(I)/NKAI)
      IF (NUC) 111,119,111
 111  IF (NK(I)) 112,119,119
 112  DQ1(I)=DQ1(I)*(NK(I)-DFL(I))*DVC/Z
 119  CONTINUE
      RETURN
 598  WRITE(8,2018)
 599  continue
*    READ (5,1000) BAR(1)
      IF (BAR(1).EQ.ENDA) GO TO 601
 601  NSTOP=1
*      GO TO 1
      END
      SUBROUTINE SOMM (DR,DP,DQ,DPAS,DA,M,NP)
C
C iHTEgPiPOBAHiE METOdOM CiMpCOHA  (DP+DQ)*DR**M OT 0 dO R=DR(NP)
C DPAS |KCpOHEHciAlxHyj {Ag dlq R->0   (DP+DQ)=CTE*R**DA
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      DIMENSION DR(*),DP(*),DQ(*)
      MM=M+1
      D1=DA+DFLOAT(MM)
      DA=0.D0
      DB=0.D0
      DO 15 I=1,NP
      DL=DR(I)**MM
      IF (I.EQ.1.OR.I.EQ.NP) GO TO 5
      DL=DL+DL
      IF ((I-2*(I/2)).EQ.0) DL=DL+DL
 5    DC=DP(I)*DL
      IF(DC) 7,9,8
 7    DB=DB+DC
      GO TO 9
 8    DA=DA+DC
 9    DC=DQ(I)*DL
      IF(DC) 11,15,13
 11   DB=DB+DC
      GO TO 15
 13   DA=DA+DC
 15   CONTINUE
      DA=DPAS*(DA+DB)/3.D0
      DC=EXP(DPAS)-1.D0
      DB=D1*(D1+1.D0)*DC*EXP((D1-1.D0)*DPAS)
      DB=DR(1)*(DR(2)**M)/DB
      DC=(DR(1)**MM)*(1.D0+1.D0/(DC*(D1+1.D0)))/D1
      DA=DA+DC*(DP(1)+DQ(1))-DB*(DP(2)+DQ(2))
      RETURN
      END
      FUNCTION FPOT (R,Z,WA)
      DOUBLE PRECISION Z
C
C pOTEHciAl TOMACA-fEPMi B TO~KE  R  Z ATOMHyj HOMEP
C WA ~iClO |lEKTPOHOB-Z-1
C **********************************************************************
      S=Z
      WC=SQRT((R*(S+WA)**(1./3.))/0.8853)
      WD=WC*(0.60112*WC+1.81061)+1.
      WE=WC*(WC*(WC*(WC*(0.04793*WC+0.21465)+0.77112)+1.39515)+1.81061)+
     11.
      WC=(Z+WA)*(WD/WE)**2-WA
      FPOT=-WC/R
      RETURN
      END
      SUBROUTINE POTSL (DV,D,DP,DR,DPAS,DEXV,Z,NP,ION,ICUT)
C
C iHTEgPiPOBAHiE pOTEHciAlA pO 4 TO~KAM
C DV pOTEHciAl   D plOTHOCTx DP BLOC DE TRAVAIL  DR PAdiAlxHAq {KAlA
C DPAS |KCp.{Ag          DEXV MHOviTElx dlq ObMEHA
C Z ATOMHyj HOMEP     NP ~iClO TO~EK        ION=Z-~iClO |lEKTPOHOB
C SI ICUT EST NUL ON CORRIGE EVENTUELLEMENT LE POTENTIEL EN -(ION+1)/R
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      COMMON/HLS/RWAT,IWAT,IEX
      DOUBLE PRECISION Z
      DIMENSION DV(*),D(*),DP(*),DR(*)
      DAS=DPAS/24.D0
      DO 1 I=1,NP
 1    DV(I)=D(I)*DR(I)
      DLO=EXP(DPAS)
      DLO2=DLO*DLO
      DP(2)=DR(1)*(D(2)-D(1)*DLO2)/(12.D0*(DLO-1.D0))
      DP(1)=DV(1)/3.D0-DP(2)/DLO2
      DP(2)=DV(2)/3.D0-DP(2)*DLO2
      J=NP-1
      DO 3 I=3,J
 3    DP(I)=DP(I-1)+DAS*(13.D0*(DV(I)+DV(I-1))-(DV(I-2)+DV(I+1)))
      DP(NP)=DP(J)
      DV(J)=DP(J)
      DV(NP)=DP(J)
      DO 5 I=3,J
      K=NP+1-I
 5    DV(K)=DV(K+1)/DLO+DAS*(13.D0*(DP(K+1)/DLO+DP(K))-(DP(K+2)/DLO2+DP(
     1K-1)*DLO))
      DV(1)=DV(3)/DLO2+DPAS*(DP(1)+4.D0*DP(2)/DLO+DP(3)/DLO2)/3.D0
      DLO=-DFLOAT(ION+1)
      IF(IEX.EQ.0) GO TO 9
C
C     pOTEHciAl pO Cl|TTEPu
C
      DO 10 I=1,NP
      DV(I)=DV(I)-(Z+3.D0*DEXV*((DR(I)*D(I)/105.27578D0+1.d-42
     1 )**(1.D0/3.D0)))
      IF(ICUT.NE.0) GO TO 10
      IF(DV(I).GT.DLO) DV(I)=DLO
   10 DV(I)=DV(I)/DR(I)
      IF(IWAT.EQ.0) RETURN
      DO 20 I=1,NP
      IF(DR(I).GT.RWAT) GO TO 21
      DV(I)=DV(I)+ION/RWAT
      GO TO 20
   21 DV(I)=DV(I)+ION/DR(I)
   20 CONTINUE
      RETURN
    9 DO  I=1,NP
C
C     pOTEHciAl pO BARTH-HEDIN
C  *********************************************************************
      DR2=DR(I)**2
      DS1=(D(I)/(3.D0*DR2)+1.d-42)**(1.D0/3.D0)

      IF(ABS(DS1).LT.1.d-10)THEN
        DVXC=0.D0
      ELSE
      	DS=1.D0/DS1
      	DSF=DS/75.D0
      	DSF2=DSF*DSF
      	DSF3=DSF2*DSF
      	DSP=DS/30.D0
      	DSP2=DSP*DSP
      	DSP3=DSP2*DSP
      DCF=(1.D0+DSF3)*LOG(1.D0+1.D0/DSF)+0.5D0*DSF-DSF2-0.3333333333D0
      DCP=(1.D0+DSP3)*LOG(1.D0+1.D0/DSP)+0.5D0*DSP-DSP2-0.3333333333D0
      	DNY=5.1297628D0*(0.0504D0*DCP-0.0254D0*DCF)
      	DARS=-1.22177412D0/DS+DNY
      	DBRS=-0.0504D0*LOG(1.D0+30.D0/DS)-DNY
      	DVXC=DARS+DBRS
      ENDIF
      DV(I)=DV(I)-(Z-0.5D0*DR(I)*DVXC)
C  *********************************************************************
      IF (ICUT.NE.0) GO TO 7
      IF (DV(I).GT.DLO) DV(I)=DLO
 7    DV(I)=DV(I)/DR(I)
      enddo
      IF(IWAT.EQ.0) RETURN
      DO 22 I=1,NP
      IF(DR(I).GT.RWAT) GO TO 23
      DV(I)=DV(I)+ION/RWAT
      GO TO 22
   23 DV(I)=DV(I)+ION/DR(I)
   22 CONTINUE
      RETURN
      END
      subroutine deforb(XN,NK,xz,ival,norb,izn,izc,mag)
C=====================================================================
      DIMENSION XZ(30),XN(30),XL(35),XJ(35),mag(30)
      integer xn,nk(30),xz
C
C     STORED DATA FOR FULL ORBITALS (AS THEY ARE NUMBERED IN 'ATOMGL'):
C
C     STORED DATA FOR RELATIVISTIC ORBITALS:
      DIMENSION XXN(31),XXL(31),XXJ(31),XXZ(31),JFULR(105),JREL(105),
     *          IXZ1(105),IXZ2(105),IXZ3(105),IXZ4(105),IXZ5(105),
     *          ival(31)
      DATA XXN/ 1.0, 2.0, 2.0, 2.0, 3.0, 3.0, 3.0, 4.0,
     *          3.0, 3.0, 4.0, 4.0, 5.0, 4.0, 4.0, 5.0,
     *          5.0, 6.0, 4.0, 4.0, 5.0, 5.0, 6.0, 6.0,
     *          7.0, 5.0, 5.0, 6.0, 6.0, 7.0, 7.0/
      DATA XXL/ 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0,
     *          2.0, 2.0, 1.0, 1.0, 0.0, 2.0, 2.0, 1.0,
     *          1.0, 0.0, 3.0, 3.0, 2.0, 2.0, 1.0, 1.0,
     *          0.0, 3.0, 3.0, 2.0, 2.0, 1.0, 1.0/
      DATA XXJ/ 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 1.5, 0.5,
     *          1.5, 2.5, 0.5, 1.5, 0.5, 1.5, 2.5, 0.5,
     *          1.5, 0.5, 2.5, 3.5, 1.5, 2.5, 0.5, 1.5,
     *          0.5, 2.5, 3.5, 1.5, 2.5, 0.5, 1.5/
      DATA XXZ/ 2.0, 2.0, 2.0, 4.0, 2.0, 2.0, 4.0, 2.0,
     *          4.0, 6.0, 2.0, 4.0, 2.0, 4.0, 6.0, 2.0,
     *          4.0, 2.0, 6.0, 8.0, 4.0, 6.0, 2.0, 4.0,
     *          2.0, 6.0, 8.0, 4.0, 6.0, 2.0, 4.0/
      DATA JFULR/ 0,1,1,2,2,3,3,3,3,4,4,5,5,6,6,6,6,7,7,
     *            8,8,8,8,7,9,9,9,9,7,10,10,11,11,11,11,
     *            12,12,13,13,13,12,12,14,12,12,12,12,15,
     *            15,16,16,16,16,17,17,18,18,18,18,18,18,
     *            19,19,19,19,19,19,19,19,20,20,20,20,21,
     *            21,21,21,17,17,22,22,23,23,23,23,24,24,
     *            25,25,25,25,25,25,26,26,26,26,26,26,26,
     *            26,27,27,27,27/
      DATA JREL/  1,1,2,2,3,3,4,4,4,4,5,5,6,6,7,7,7,7,8,
     *            8,9,9,9,10,10,10,10,10,10,10,11,11,12,
     *            12,12,12,13,13,14,14,14,15,15,15,15,15,
     *            15,15,16,16,17,17,17,17,18,18,21,21,19,
     *            19,19,19,20,21,20,20,20,20,20,20,21,21,
     *            21,21,22,22,22,22,22,22,23,23,24,24,24,
     *            24,25,25,28,28,28,28,28,26,27,28,28,27,
     *            27,27,27,27,28,28,28/
      DATA IXZ1/ 1,0,1,0,1,0,1,2,3,0,1,0,1,0,1,2,3,0,1,
     *           0,1,2,3,1,1,2,3,4,1,0,1,0,1,2,3,0,1,0,
     *           1,2,1,1,1,1,1,0,1,0,1,0,1,2,3,0,1,0,0,
     *           1,3,4,5,0,1,1,3,4,5,6,7,0,1,2,3,0,1,2,
     *           3,1,1,0,1,0,1,2,3,0,1,0,0,0,2,3,4,0,1,
     *           1,2,4,5,6,7,0,1,2,3/
      DATA IXZ2/ 23*0,4,4*0,4,11*0,4,4,0,4,4,4,4,16*0,1,
     *           13*0,6,6,16*0,1,1,8*0/
      DATA IXZ3/ 23*0,1,4*0,6,12*0,1,0,3,4,6,6,9*0,1,1,
     *           19*0,8,8,9*0,1,2,1,1,1,12*0/
      DATA IXZ4/ 77*0,4,4,26*0/
      DATA IXZ5/ 77*0,5,6,26*0/
C=====================================================================
       zn=izn
      IZ=ZN+0.000001
      iZV=iZN-iZC
C------ J-REPRESENTATION ------
      JFUL=JFULR(IZ)

      J=JREL(IZ)
      DO 1070 I=1,JFUL
       XN(I)=nint(XXN(I))
       XL(I)=XXL(I)
       XJ(I)=XXJ(I)
 1070  XZ(I)=nint(XXZ(I))

      XZ(JFUL+1)=IXZ1(IZ)
      XZ(JFUL+2)=IXZ2(IZ)
      XZ(JFUL+3)=IXZ3(IZ)
      XZ(JFUL+4)=IXZ4(IZ)
      XZ(JFUL+5)=IXZ5(IZ)

 1118 CONTINUE
C--------------------------------
      IF(JFUL.EQ.J) GOTO 1000
      JF1=JFUL+1
      DO 1101  I=JF1,J
       XN(I)=nint(XXN(I))
       XL(I)=XXL(I)
       XJ(I)=XXJ(I)
C--------------------------------
 1101 CONTINUE
C=====================================================================
 1000 CONTINUE
      ixv=0
      ixt=0
      do i=j,1,-1
        ixt=ixt+xz(i)
        nk(i)=nint(xl(i))
        mag(i)=0
        xmag=0
        if(xj(i).gt.xl(i))nk(i)=nint(-xl(i)-1)
        if(ixv+xz(i).le.izv)then
          ival(i)=1
          ixv=ixv+xz(i)
          if(nk(i).gt.1)then
            xml=xz(i)+xz(i+1)
            nmag=nk(i)*2+1
            xmag=nmag-abs(xml-nmag)
            mag(i)=nint(xmag)
c            XMAG1=(nmag-1)*xmag/(2.*nmag)
c            XMAG2=xmag-xmag1
c            print*,xz(i),xz(i+1)
c            print*, i,nk(i),' l=',nint(xl(i)),' n='
c     $           ,xn(i),' val=',nint(XMAG),xmag1,xmag2
          endif
        else
          ival(i)=0
        endif
      enddo
      if(ixv.ne.izv) stop ' error - I can not make initial data '
      norb=j
      END
      SUBROUTINE EXIT_A
      return
      END
      SUBROUTINE RESLD (NQN,NQL,NK,IMAX,DE,DFL,DQ1,JC1)
C
C PE{EHiE uPABHEHiq diPAKA
C NQN NglABHOE KBAHTOBOE ~iClO   NQL OPbiTAlxHOE KBAHTOBOE ~iClO
C NK KBAHTOBOE ~iClO KAppA    IMAX pOClEdHqq TO~KA B TAbulqcii BOl-
C HOBOj fuHKcii     DE |HEPgiq   DFL pOKAzATElx CTEpEHi B PAzlOvEHii
C BOlHOBOj fuHKcii       DQ1 OTHO{EHiE DP K DQ B HA~AlE KOOPdiHAT
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      DOUBLE PRECISION Z
      COMMON/DIRA/DV(251),DR(251),DP(251),DQ(251),DPAS,Z,NSTOP,NES,TEST,
     1NP,NUC
C
C DV pOTEHciAl B AT.Ed.(<0)        DR PAdiAlxHAq CETKA
C DP bOlx{Aq KOMpOHEHTA  DQ MAlAq KOMpOHEHTA    DPAS |KCpOHEHc.{Ag
C Z ATOMHyj HOMEP     NSTOP KOHTPOlx ~iClEHHOgO iHTEgPiPOBAHiq
C NES MAKCiMAlxHOE ~iClO iTEPAcij pO OpPEdElEHi` |HEPgii
C TEST TO~HOCTx pOlu~EHiq |HEPgii          NP NMAKCiMAlxHOE ~iClO TO~EK
C KOHE~HyE PAzMEPy qdPA ECli    NUC HE   0
C **********************************************************************
      COMMON/PS1/DEP(5),DEQ(5),DB,DVC,DSAL,DK,DM
C
C DEP,DEQ pPOizBOdHyE DP i  DQ   DB=ENERGIE/DVC    DVC CKOPOCTx CBETA
C  B AT.Ed.         DSAL=2.*DVC   DK KBAHTOBOE ~iClO KAppA
C DM=|KCpOHEHciAlxHyj {Ag/720., DKOEF=1./720.
C **********************************************************************
      COMMON/TROIS/ DPNO(4,30),DQNO(4,30)
      DATA DKOEF/.1388888888888888D-2/
      JC=1
      IF(JC1.GT.0)JC=JC1
      NSTOP=0
      DVC=137.0373D0
      DSAL=DVC+DVC
      IMM=0
      IES=0
      DK=NK
      LLL=(NQL*(NQL+1))/2
      ND=0
      NOEUD=NQN-NQL
      IF (LLL.NE.0) GO TO 11
      ELIM=-Z*Z/(1.5*NQN*NQN)
      GO TO 19
 11   ELIM=DV(1)+LLL/(DR(1)*DR(1))
      DO 15 I=2,NP
      VAL=DV(I)+LLL/(DR(I)*DR(I))
      IF (VAL.LE.ELIM) ELIM=VAL
 15   CONTINUE
      IF(ELIM) 19,17,17
 17   NSTOP=17
C 2*V+L*(L+1)/R**2   BC`du pOlOviTElEH
C **********************************************************************
      RETURN
 19   IF(DE.LE.ELIM) DE=ELIM*0.5D0
 21   IF (IMM.EQ.1) GO TO 35
      DO 25 I=7,NP,2
      IMAT=NP+1-I
      IF ((DV(IMAT)+DFLOAT(LLL)/(DR(IMAT)*DR(IMAT))-DE).LE.0.D0) GO TO 2
     16
 25   CONTINUE
 26   IF (IMAT.GT.5) GO TO 35
      DE=DE*0.5D0
      IF(DE.LT.-TEST.AND.ND.LE.NOEUD) GO TO 21
 28   NSTOP=28
C 2*V+L*(L+1)/R**2-2*E BC`du pOlOviTElEH
C **********************************************************************
      RETURN
C HA~AlxHyE zHA~EHiq dlq iHTEgPiPOBAHiq pO BHuTPEHHEj OblACTi
C **********************************************************************
 35   DB=DE/DVC
      CALL INOUH (DP,DQ,DR,DQ1,DFL,DV(1),Z,TEST,NUC,NSTOP,JC)
      IF (NSTOP) 399,47,399
C     NSTOP=45
C HET CXOdiMOCTi B HA~AlE KOOPdiHAT
C **********************************************************************
 47   ND=1
      DO 51 I=1,5
      DVAL=DR(I)**DFL
      IF (I.EQ.1) GO TO 50
      IF (DP(I-1).EQ.0.D0) GO TO 50
      IF ((DP(I)/DP(I-1)).GT.0.D0) GO TO 50
      ND=ND+1
 50   DP(I)=DP(I)*DVAL
      DQ(I)=DQ(I)*DVAL
      DEP(I)=DEP(I)*DVAL
 51   DEQ(I)=DEQ(I)*DVAL
      K=-1+2*(NOEUD-2*(NOEUD/2))
      IF ((DP(1)*DFLOAT(K)).GT.0.D0) GO TO 54
 53   NSTOP=53
C O{ibKA B PAzlOvEHii B HEA~AlE KOOPdiHAT
C **********************************************************************
      RETURN
 54   IF ((DFLOAT(K)*DFLOAT(NK)*DQ(1)).LT.0.D0) GO TO 53
      DM=DPAS*DKOEF
C IiHTEgPiPOBAHiE pO BHuTPEHHEj OblACTi
C **********************************************************************
      DO 195 I=6,IMAT
      DP(I)=DP(I-1)
      DQ(I)=DQ(I-1)
      CALL INTH (DP(I),DQ(I),DV(I),DR(I))
      IF (DP(I-1).EQ.0.D0) GO TO 195
      IF ((DP(I)/DP(I-1)).GT.0.D0) GO TO 195
      ND=ND+1
      IF(ND.GT.NOEUD) GO TO 209
 195  CONTINUE
      IF (ND.EQ.NOEUD) GO TO 240
      DE=0.8D0*DE
      IF(DE.LT.-TEST) GO TO 21
 206  NSTOP=206
C ~iClO uzlOB Cli{KOM MAlO
C **********************************************************************
      RETURN
 209  DE=1.2D0*DE
      IF(DE.GT.ELIM) GO TO 21
 210  NSTOP=210
C ~iClO uzlOB Cli{KOM BEliKO
C **********************************************************************
      RETURN
C HA~AlHyE zHA~EHiq dlq iHTgPiPOBAHiq pO BHE{HEj OblACTi
C **********************************************************************
 240  DQM=DQ(IMAT)
      DPM=DP(IMAT)
      IF (IMM.EQ.1) GO TO 258
      DO 255 I=1,NP,2
      IMAX=NP+1-I
      IF (((DV(IMAX)-DE)*DR(IMAX)*DR(IMAX)).LE.300.D0) GO TO 258
 255  CONTINUE
 258  DD=SQRT(-DE*(2.D0+DB/DVC))
      DPQ=-DD/(DSAL+DB)
      DM=-DM
      DO 277 I=1,5
      J=IMAX+1-I
      DP(J)=EXP(-DD*DR(J))
      DEP(I)=-DD*DP(J)*DR(J)
      DQ(J)=DPQ*DP(J)
 277  DEQ(I)=DPQ*DEP(I)
      M=IMAX-5
C iHTEgPiPOBAHiE pO BHE{HEj OblACTi
C***********************************************************************
      DO 301 I=IMAT,M
      J=M+IMAT-I
      DP(J)=DP(J+1)
      DQ(J)=DQ(J+1)
 301  CALL INTH (DP(J),DQ(J),DV(J),DR(J))
C C{iBKA bOlx{Oj KOMpOHEHTy
C **********************************************************************
      DVAL=DPM/DP(IMAT)
      IF (DVAL.GT.0.D0) GO TO 313
      NSTOP=312
C O{ibKA B zHAKE bOlx{Oj KOMpOHEHTy
C **********************************************************************
      RETURN
 313  DO 315 I=IMAT,IMAX
      DP(I)=DP(I)*DVAL
 315  DQ(I)=DQ(I)*DVAL
C By~iClEHiE HOPMy
C **********************************************************************
      DSUM=3.D0*DR(1)*(DP(1)**2+DQ(1)**2)/(DPAS*(DFL+DFL+1.D0))
      DO 333 I=3,IMAX,2
 333  DSUM=DSUM+DR(I)*(DP(I)**2+DQ(I)**2)+4.D0*DR(I-1)*(DP(I-1)**2+DQ(I-
     11)**2)+DR(I-2)*(DP(I-2)**2+DQ(I-2)**2)
      DSUM=DPAS*(DSUM+DR(IMAT)*(DQM*DQM-DQ(IMAT)*DQ(IMAT)))
     1*0.3333333333333333D0
C izMEHEHiE |HEPgii
C **********************************************************************
      DBE=DP(IMAT)*(DQM-DQ(IMAT))*DVC/DSUM
      IMM=0
      VAL=ABS(DBE/DE)
      IF (VAL.LE.TEST) GO TO 365
 340  DVAL=DE+DBE
      IF(DVAL.LT.0.D0) GO TO 360
      DBE=DBE*0.5D0
      VAL=VAL*0.5D0
      IF (VAL.GT.TEST) GO TO 340
 345  NSTOP=345
C HulEBAq |HEPgiq
C **********************************************************************
      RETURN
 360  DE=DVAL
      IF (VAL.LE.0.1) IMM=1
      IES=IES+1
      IF(IES.LE.NES) GO TO 21
 362  NSTOP=362
C pPEBy{EH ~iClO iTEPAcij
C **********************************************************************
      RETURN
365   IF(JC1.GT.0.OR.IMAX.LE.(-JC1))GO TO 3365
      IMAX=-JC1
      DSUM=3.D0*DR(1)*(DP(1)**2+DQ(1)**2)/(DPAS*(DFL+DFL+1.D0))
      DO1333 I=3,IMAX,2
1333  DSUM=DSUM+DR(I)*(DP(I)**2+DQ(I)**2)+4.D0*DR(I-1)*(DP(I-1)**2+DQ(I-
     11)**2)+DR(I-2)*(DP(I-2)**2+DQ(I-2)**2)
      DSUM=DPAS*(DSUM+DR(IMAT)*(DQM*DQM-DQ(IMAT)*DQ(IMAT)))
     1*0.3333333333333333D0
3365  DSUM=SQRT(DSUM)
      DQ1=DQ1/DSUM
      DO 367 I=1,IMAX
      DP(I)=DP(I)/DSUM
 367  DQ(I)=DQ(I)/DSUM
      DO 368 I=1,4
      DPNO(I,JC)=DPNO(I,JC)/DSUM
 368  DQNO(I,JC)=DQNO(I,JC)/DSUM
      IF(IMAX.EQ.NP) GO TO 398
      J=IMAX+1
      DO 371 I=J,NP
      DP(I)=0.D0
 371  DQ(I)=0.D0
 398  NSTOP=0
 399  RETURN
      END
      SUBROUTINE INOUH (DP,DQ,DR,DQ1,DFL,DV,Z,TEST,NUC,NSTOP,JC)
C
C HA~AlxHyE zHA~EHiq dlq iHTEgPiPOBAHiq pO BHuTPEHHEj OblACTi
C DP bOlx{Aq KOMpOHEHTA   DQ MAlAq KOMpOHEHTA     DR PAdiAlxHAq.CETKA
C DQ1 OTHO{EHiE DP K DQ B HA~AlE KOOPd.DFL pOKAzATElx CTEpEHi glABHOgO
C ~lEHA PAzlOvEHiq B HA~AlE DV pOTEHciAl B pEPBOj TO~KE
C Z ATOMHyj HOMEP      TEST TO~HOCTx
C KOHE~HyE PAzMEPy qdPA ECli    NUC HE 0
C NSTOP KOHTPOlx CXOdiMOCTi pPi PAzlOvEHii B Pqd
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      DOUBLE PRECISION Z
      COMMON/PS1/DEP(5),DEQ(5),DD,DVC,DSAL,DK,DM
C
C DEP,DEQ pPOizBOdHyE DP i  DQ   DD=|HEPgiq/DVC    DVC CKOPOCTx CBETA
C  B AT. Ed.        DSAL=2.*DVC   DK KBAHTOBOE ~iClO KAppA
C DM=|KCp.{Ag/720.
C **********************************************************************
      COMMON/TROIS/ DPNO(4,30),DQNO(4,30)
      DIMENSION DP(*),DQ(*),DR(*)
      DO 3 I=1,10
      DP(I)=0.D0
 3    DQ(I)=0.D0
      IF (NUC) 5,5,21
 5    DVAL=Z/DVC
      DEVA1=-DVAL
      DEVA2=DV/DVC+DVAL/DR(1)-DD
      DEVA3=0.D0
      IF (DK) 9,9,11
 9    DBE=(DK-DFL)/DVAL
      GO TO 15
 11   DBE=DVAL/(DK+DFL)
 15   DQ(10)=DQ1
      DP(10)=DBE*DQ1
      GO TO 39
 21   DVAL=DV+Z*(3.D0-DR(1)*DR(1)/(DR(NUC)*DR(NUC)))/(DR(NUC)+DR(NUC))
      DEVA1=0.D0
      DEVA2=(DVAL-3.D0*Z/(DR(NUC)+DR(NUC)))/DVC-DD
      DEVA3=Z/(DR(NUC)*DR(NUC)*DR(NUC)*DSAL)
      IF (DK) 33,33,35
 33   DP(10)=DQ1
      GO TO 39
 35   DQ(10)=DQ1
 39   DO 40 I=1,5
      DP(I)=DP(10)
      DQ(I)=DQ(10)
      DEP(I)=DP(I)*DFL
 40   DEQ(I)=DQ(I)*DFL
      M=1
 41   DM=M+DFL
      DSUM=DM*DM-DK*DK+DEVA1*DEVA1
      DQR=(DSAL-DEVA2)*DQ(M+9)-DEVA3*DQ(M+7)
      DPR=DEVA2*DP(M+9)+DEVA3*DP(M+7)
      DVAL=((DM-DK)*DQR-DEVA1*DPR)/DSUM
      DSUM=((DM+DK)*DPR+DEVA1*DQR)/DSUM
      J=-1
      DO 44 I=1,5
      DPR=DR(I)**M
      DQR=DSUM*DPR
      DPR=DVAL*DPR
      IF (M.EQ.1) GO TO 43
      IF (ABS(DPR/DP(I)).LE.TEST.AND.ABS(DQR/DQ(I)).LE.TEST) J=1
 43   DP(I)=DP(I)+DPR
      DQ(I)=DQ(I)+DQR
      DEP(I)=DEP(I)+DPR*DM
 44   DEQ(I)=DEQ(I)+DQR*DM
      IF (J.EQ.1) GO TO 99
      DP(M+10)=DVAL
      DQ(M+10)=DSUM
      M=M+1
      IF (M.LE.20) GO TO 41
      NSTOP=45
 99   DO 101 I=1,4
      DPNO(I,JC)=DP(I+9)
 101  DQNO(I,JC)=DQ(I+9)
 999  RETURN
      END
      SUBROUTINE INTH (DP,DQ,DV,DR)
C
C iHTEgPiPOBAHiE pO METOdu AdAMCA pO 5-TO~KAM bOlx{Oj KOMpOHEHTy  DP i
C MAlOj KOMpOHEHTy DQ   B TO~KE DR  ; dv-pOTEHciAl B |TOj TO~KE
C **********************************************************************
      IMPLICIT DOUBLE PRECISION(D)
      COMMON/PS1/DEP(5),DEQ(5),DB,DVC,DSAL,DK,DM
C
C
C DEP,DEQ pPOizBOdHyE DP i  DQ   DD=|HEPgiq/DVC    DVC CKOPOCTx CBETA
C  B AT. Ed.        DSAL=2.*DVC   DK KBAHTOBOE ~iClO KAppA
C DM=|KCp.{Ag/720.
C DKOEF1=405./502., DKOEF2=27./502.
C **********************************************************************
      DATA DKOEF1/.9462151394422310D0/,DKOEF2/.5378486055776890D-1/
      DPR=DP+DM*((251.D0*DEP(1)+2616.D0*DEP(3)+1901.D0*DEP(5))-(1274.D0*
     1DEP(2)+2774.D0*DEP(4)))
      DQR=DQ+DM*((251.D0*DEQ(1)+2616.D0*DEQ(3)+1901.D0*DEQ(5))-(1274.D0*
     1DEQ(2)+2774.D0*DEQ(4)))
      DO 13 I=2,5
      DEP(I-1)=DEP(I)
 13   DEQ(I-1)=DEQ(I)
      DSUM=(DB-DV/DVC)*DR
      DEP(5)=-DK*DPR+(DSAL*DR+DSUM)*DQR
      DEQ(5)=DK*DQR-DSUM*DPR
      DP=DP+DM*((106.D0*DEP(2)+646.D0*DEP(4)+251.D0*DEP(5))-(19.D0*DEP(1
     1)+264.D0*DEP(3)))
      DQ=DQ+DM*((106.D0*DEQ(2)+646.D0*DEQ(4)+251.D0*DEQ(5))-(19.D0*DEQ(1
     1)+264.D0*DEQ(3)))
      DP=DKOEF1*DP+DKOEF2*DPR
      DQ=DKOEF1*DQ+DKOEF2*DQR
      DEP(5)=-DK*DP+(DSAL*DR+DSUM)*DQ
      DEQ(5)=DK*DQ-DSUM*DP
      RETURN
      END
      DOUBLE PRECISION FUNCTION DLAMCH( CMACH )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      CHARACTER          CMACH
*     ..
*
*  Purpose
*  =======
*
*  DLAMCH determines double precision machine parameters.
*
*  Arguments
*  =========
*
*  CMACH   (input) CHARACTER*1
*          Specifies the value to be returned by DLAMCH:
*          = 'E' or 'e',   DLAMCH := eps
*          = 'S' or 's ,   DLAMCH := sfmin
*          = 'B' or 'b',   DLAMCH := base
*          = 'P' or 'p',   DLAMCH := eps*base
*          = 'N' or 'n',   DLAMCH := t
*          = 'R' or 'r',   DLAMCH := rnd
*          = 'M' or 'm',   DLAMCH := emin
*          = 'U' or 'u',   DLAMCH := rmin
*          = 'L' or 'l',   DLAMCH := emax
*          = 'O' or 'o',   DLAMCH := rmax
*
*          where
*
*          eps   = relative machine precision
*          sfmin = safe minimum, such that 1/sfmin does not overflow
*          base  = base of the machine
*          prec  = eps*base
*          t     = number of (base) digits in the mantissa
*          rnd   = 1.0 when rounding occurs in addition, 0.0 otherwise
*          emin  = minimum exponent before (gradual) underflow
*          rmin  = underflow threshold - base**(emin-1)
*          emax  = largest exponent before overflow
*          rmax  = overflow threshold  - (base**emax)*(1-eps)
*
* =====================================================================
*
*     .. Parameters ..
      DOUBLE PRECISION   ONE, ZERO
      PARAMETER          ( ONE = 1.0D+0, ZERO = 0.0D+0 )
*     ..
*     .. Local Scalars ..
      LOGICAL            FIRST, LRND
      INTEGER            BETA, IMAX, IMIN, IT
      DOUBLE PRECISION   BASE, EMAX, EMIN, EPS, PREC, RMACH, RMAX, RMIN,
     $                   RND, SFMIN, SMALL, T
*     ..
*     .. External Functions ..
      LOGICAL            LSAME
      EXTERNAL           LSAME
*     ..
*     .. External Subroutines ..
      EXTERNAL           DLAMC2
*     ..
*     .. Save statement ..
      SAVE               FIRST, EPS, SFMIN, BASE, T, RND, EMIN, RMIN,
     $                   EMAX, RMAX, PREC
*     ..
*     .. Data statements ..
      DATA               FIRST / .TRUE. /
*     ..
*     .. Executable Statements ..
*
      IF( FIRST ) THEN
         FIRST = .FALSE.
         CALL DLAMC2( BETA, IT, LRND, EPS, IMIN, RMIN, IMAX, RMAX )
         BASE = BETA
         T = IT
         IF( LRND ) THEN
            RND = ONE
            EPS = ( BASE**( 1-IT ) ) / 2
         ELSE
            RND = ZERO
            EPS = BASE**( 1-IT )
         END IF
         PREC = EPS*BASE
         EMIN = IMIN
         EMAX = IMAX
         SFMIN = RMIN
         SMALL = ONE / RMAX
         IF( SMALL.GE.SFMIN ) THEN
*
*           Use SMALL plus a bit, to avoid the possibility of rounding
*           causing overflow when computing  1/sfmin.
*
            SFMIN = SMALL*( ONE+EPS )
         END IF
      END IF
*
      IF( LSAME( CMACH, 'E' ) ) THEN
         RMACH = EPS
      ELSE IF( LSAME( CMACH, 'S' ) ) THEN
         RMACH = SFMIN
      ELSE IF( LSAME( CMACH, 'B' ) ) THEN
         RMACH = BASE
      ELSE IF( LSAME( CMACH, 'P' ) ) THEN
         RMACH = PREC
      ELSE IF( LSAME( CMACH, 'N' ) ) THEN
         RMACH = T
      ELSE IF( LSAME( CMACH, 'R' ) ) THEN
         RMACH = RND
      ELSE IF( LSAME( CMACH, 'M' ) ) THEN
         RMACH = EMIN
      ELSE IF( LSAME( CMACH, 'U' ) ) THEN
         RMACH = RMIN
      ELSE IF( LSAME( CMACH, 'L' ) ) THEN
         RMACH = EMAX
      ELSE IF( LSAME( CMACH, 'O' ) ) THEN
         RMACH = RMAX
      END IF
*
      DLAMCH = RMACH
      RETURN
*
*     End of DLAMCH
*
      END
*
************************************************************************
*
      SUBROUTINE DLAMC1( BETA, T, RND, IEEE1 )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      LOGICAL            IEEE1, RND
      INTEGER            BETA, T
*     ..
*
*  Purpose
*  =======
*
*  DLAMC1 determines the machine parameters given by BETA, T, RND, and
*  IEEE1.
*
*  Arguments
*  =========
*
*  BETA    (output) INTEGER
*          The base of the machine.
*
*  T       (output) INTEGER
*          The number of ( BETA ) digits in the mantissa.
*
*  RND     (output) LOGICAL
*          Specifies whether proper rounding  ( RND = .TRUE. )  or
*          chopping  ( RND = .FALSE. )  occurs in addition. This may not
*          be a reliable guide to the way in which the machine performs
*          its arithmetic.
*
*  IEEE1   (output) LOGICAL
*          Specifies whether rounding appears to be done in the IEEE
*          'round to nearest' style.
*
*  Further Details
*  ===============
*
*  The routine is based on the routine  ENVRON  by Malcolm and
*  incorporates suggestions by Gentleman and Marovich. See
*
*     Malcolm M. A. (1972) Algorithms to reveal properties of
*        floating-point arithmetic. Comms. of the ACM, 15, 949-951.
*
*     Gentleman W. M. and Marovich S. B. (1974) More on algorithms
*        that reveal properties of floating point arithmetic units.
*        Comms. of the ACM, 17, 276-277.
*
* =====================================================================
*
*     .. Local Scalars ..
      LOGICAL            FIRST, LIEEE1, LRND
      INTEGER            LBETA, LT
      DOUBLE PRECISION   A, B, C, F, ONE, QTR, SAVEC, T1, T2
*     ..
*     .. External Functions ..
      DOUBLE PRECISION   DLAMC3
      EXTERNAL           DLAMC3
*     ..
*     .. Save statement ..
      SAVE               FIRST, LIEEE1, LBETA, LRND, LT
*     ..
*     .. Data statements ..
      DATA               FIRST / .TRUE. /
*     ..
*     .. Executable Statements ..
*
      IF( FIRST ) THEN
         FIRST = .FALSE.
         ONE = 1
*
*        LBETA,  LIEEE1,  LT and  LRND  are the  local values  of  BETA,
*        IEEE1, T and RND.
*
*        Throughout this routine  we use the function  DLAMC3  to ensure
*        that relevant values are  stored and not held in registers,  or
*        are not affected by optimizers.
*
*        Compute  a = 2.0**m  with the  smallest positive integer m such
*        that
*
*           fl( a + 1.0 ) = a.
*
         A = 1
         C = 1
*
*+       WHILE( C.EQ.ONE )LOOP
   10    CONTINUE
         IF( C.EQ.ONE ) THEN
            A = 2*A
            C = DLAMC3( A, ONE )
            C = DLAMC3( C, -A )
            GO TO 10
         END IF
*+       END WHILE
*
*        Now compute  b = 2.0**m  with the smallest positive integer m
*        such that
*
*           fl( a + b ) .gt. a.
*
         B = 1
         C = DLAMC3( A, B )
*
*+       WHILE( C.EQ.A )LOOP
   20    CONTINUE
         IF( C.EQ.A ) THEN
            B = 2*B
            C = DLAMC3( A, B )
            GO TO 20
         END IF
*+       END WHILE
*
*        Now compute the base.  a and c  are neighbouring floating point
*        numbers  in the  interval  ( beta**t, beta**( t + 1 ) )  and so
*        their difference is beta. Adding 0.25 to c is to ensure that it
*        is truncated to beta and not ( beta - 1 ).
*
         QTR = ONE / 4
         SAVEC = C
         C = DLAMC3( C, -A )
         LBETA = C + QTR
*
*        Now determine whether rounding or chopping occurs,  by adding a
*        bit  less  than  beta/2  and a  bit  more  than  beta/2  to  a.
*
         B = LBETA
         F = DLAMC3( B / 2, -B / 100 )
         C = DLAMC3( F, A )
         IF( C.EQ.A ) THEN
            LRND = .TRUE.
         ELSE
            LRND = .FALSE.
         END IF
         F = DLAMC3( B / 2, B / 100 )
         C = DLAMC3( F, A )
         IF( ( LRND ) .AND. ( C.EQ.A ) )
     $      LRND = .FALSE.
*
*        Try and decide whether rounding is done in the  IEEE  'round to
*        nearest' style. B/2 is half a unit in the last place of the two
*        numbers A and SAVEC. Furthermore, A is even, i.e. has last  bit
*        zero, and SAVEC is odd. Thus adding B/2 to A should not  change
*        A, but adding B/2 to SAVEC should change SAVEC.
*
         T1 = DLAMC3( B / 2, A )
         T2 = DLAMC3( B / 2, SAVEC )
         LIEEE1 = ( T1.EQ.A ) .AND. ( T2.GT.SAVEC ) .AND. LRND
*
*        Now find  the  mantissa, t.  It should  be the  integer part of
*        log to the base beta of a,  however it is safer to determine  t
*        by powering.  So we find t as the smallest positive integer for
*        which
*
*           fl( beta**t + 1.0 ) = 1.0.
*
         LT = 0
         A = 1
         C = 1
*
*+       WHILE( C.EQ.ONE )LOOP
   30    CONTINUE
         IF( C.EQ.ONE ) THEN
            LT = LT + 1
            A = A*LBETA
            C = DLAMC3( A, ONE )
            C = DLAMC3( C, -A )
            GO TO 30
         END IF
*+       END WHILE
*
      END IF
*
      BETA = LBETA
      T = LT
      RND = LRND
      IEEE1 = LIEEE1
      RETURN
*
*     End of DLAMC1
*
      END
*
************************************************************************
*
      SUBROUTINE DLAMC2( BETA, T, RND, EPS, EMIN, RMIN, EMAX, RMAX )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      LOGICAL            RND
      INTEGER            BETA, EMAX, EMIN, T
      DOUBLE PRECISION   EPS, RMAX, RMIN
*     ..
*
*  Purpose
*  =======
*
*  DLAMC2 determines the machine parameters specified in its argument
*  list.
*
*  Arguments
*  =========
*
*  BETA    (output) INTEGER
*          The base of the machine.
*
*  T       (output) INTEGER
*          The number of ( BETA ) digits in the mantissa.
*
*  RND     (output) LOGICAL
*          Specifies whether proper rounding  ( RND = .TRUE. )  or
*          chopping  ( RND = .FALSE. )  occurs in addition. This may not
*          be a reliable guide to the way in which the machine performs
*          its arithmetic.
*
*  EPS     (output) DOUBLE PRECISION
*          The smallest positive number such that
*
*             fl( 1.0 - EPS ) .LT. 1.0,
*
*          where fl denotes the computed value.
*
*  EMIN    (output) INTEGER
*          The minimum exponent before (gradual) underflow occurs.
*
*  RMIN    (output) DOUBLE PRECISION
*          The smallest normalized number for the machine, given by
*          BASE**( EMIN - 1 ), where  BASE  is the floating point value
*          of BETA.
*
*  EMAX    (output) INTEGER
*          The maximum exponent before overflow occurs.
*
*  RMAX    (output) DOUBLE PRECISION
*          The largest positive number for the machine, given by
*          BASE**EMAX * ( 1 - EPS ), where  BASE  is the floating point
*          value of BETA.
*
*  Further Details
*  ===============
*
*  The computation of  EPS  is based on a routine PARANOIA by
*  W. Kahan of the University of California at Berkeley.
*
* =====================================================================
*
*     .. Local Scalars ..
      LOGICAL            FIRST, IEEE, IWARN, LIEEE1, LRND
      INTEGER            GNMIN, GPMIN, I, LBETA, LEMAX, LEMIN, LT,
     $                   NGNMIN, NGPMIN
      DOUBLE PRECISION   A, B, C, HALF, LEPS, LRMAX, LRMIN, ONE, RBASE,
     $                   SIXTH, SMALL, THIRD, TWO, ZERO
*     ..
*     .. External Functions ..
      DOUBLE PRECISION   DLAMC3
      EXTERNAL           DLAMC3
*     ..
*     .. External Subroutines ..
      EXTERNAL           DLAMC1, DLAMC4, DLAMC5
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          ABS, MAX, MIN
*     ..
*     .. Save statement ..
      SAVE               FIRST, IWARN, LBETA, LEMAX, LEMIN, LEPS, LRMAX,
     $                   LRMIN, LT
*     ..
*     .. Data statements ..
      DATA               FIRST / .TRUE. / , IWARN / .FALSE. /
*     ..
*     .. Executable Statements ..
*
      IF( FIRST ) THEN
         FIRST = .FALSE.
         ZERO = 0
         ONE = 1
         TWO = 2
*
*        LBETA, LT, LRND, LEPS, LEMIN and LRMIN  are the local values of
*        BETA, T, RND, EPS, EMIN and RMIN.
*
*        Throughout this routine  we use the function  DLAMC3  to ensure
*        that relevant values are stored  and not held in registers,  or
*        are not affected by optimizers.
*
*        DLAMC1 returns the parameters  LBETA, LT, LRND and LIEEE1.
*
         CALL DLAMC1( LBETA, LT, LRND, LIEEE1 )
*
*        Start to find EPS.
*
         B = LBETA
         A = B**( -LT )
         LEPS = A
*
*        Try some tricks to see whether or not this is the correct  EPS.
*
         B = TWO / 3
         HALF = ONE / 2
         SIXTH = DLAMC3( B, -HALF )
         THIRD = DLAMC3( SIXTH, SIXTH )
         B = DLAMC3( THIRD, -HALF )
         B = DLAMC3( B, SIXTH )
         B = ABS( B )
         IF( B.LT.LEPS )
     $      B = LEPS
*
         LEPS = 1
*
*+       WHILE( ( LEPS.GT.B ).AND.( B.GT.ZERO ) )LOOP
   10    CONTINUE
         IF( ( LEPS.GT.B ) .AND. ( B.GT.ZERO ) ) THEN
            LEPS = B
            C = DLAMC3( HALF*LEPS, ( TWO**5 )*( LEPS**2 ) )
            C = DLAMC3( HALF, -C )
            B = DLAMC3( HALF, C )
            C = DLAMC3( HALF, -B )
            B = DLAMC3( HALF, C )
            GO TO 10
         END IF
*+       END WHILE
*
         IF( A.LT.LEPS )
     $      LEPS = A
*
*        Computation of EPS complete.
*
*        Now find  EMIN.  Let A = + or - 1, and + or - (1 + BASE**(-3)).
*        Keep dividing  A by BETA until (gradual) underflow occurs. This
*        is detected when we cannot recover the previous A.
*
         RBASE = ONE / LBETA
         SMALL = ONE
         DO 20 I = 1, 3
            SMALL = DLAMC3( SMALL*RBASE, ZERO )
   20    CONTINUE
         A = DLAMC3( ONE, SMALL )
         CALL DLAMC4( NGPMIN, ONE, LBETA )
         CALL DLAMC4( NGNMIN, -ONE, LBETA )
         CALL DLAMC4( GPMIN, A, LBETA )
         CALL DLAMC4( GNMIN, -A, LBETA )
         IEEE = .FALSE.
*
         IF( ( NGPMIN.EQ.NGNMIN ) .AND. ( GPMIN.EQ.GNMIN ) ) THEN
            IF( NGPMIN.EQ.GPMIN ) THEN
               LEMIN = NGPMIN
*            ( Non twos-complement machines, no gradual underflow;
*              e.g.,  VAX )
            ELSE IF( ( GPMIN-NGPMIN ).EQ.3 ) THEN
               LEMIN = NGPMIN - 1 + LT
               IEEE = .TRUE.
*            ( Non twos-complement machines, with gradual underflow;
*              e.g., IEEE standard followers )
            ELSE
               LEMIN = MIN( NGPMIN, GPMIN )
*            ( A guess; no known machine )
               IWARN = .TRUE.
            END IF
*
         ELSE IF( ( NGPMIN.EQ.GPMIN ) .AND. ( NGNMIN.EQ.GNMIN ) ) THEN
            IF( ABS( NGPMIN-NGNMIN ).EQ.1 ) THEN
               LEMIN = MAX( NGPMIN, NGNMIN )
*            ( Twos-complement machines, no gradual underflow;
*              e.g., CYBER 205 )
            ELSE
               LEMIN = MIN( NGPMIN, NGNMIN )
*            ( A guess; no known machine )
               IWARN = .TRUE.
            END IF
*
         ELSE IF( ( ABS( NGPMIN-NGNMIN ).EQ.1 ) .AND.
     $            ( GPMIN.EQ.GNMIN ) ) THEN
            IF( ( GPMIN-MIN( NGPMIN, NGNMIN ) ).EQ.3 ) THEN
               LEMIN = MAX( NGPMIN, NGNMIN ) - 1 + LT
*            ( Twos-complement machines with gradual underflow;
*              no known machine )
            ELSE
               LEMIN = MIN( NGPMIN, NGNMIN )
*            ( A guess; no known machine )
               IWARN = .TRUE.
            END IF
*
         ELSE
            LEMIN = MIN( NGPMIN, NGNMIN, GPMIN, GNMIN )
*         ( A guess; no known machine )
            IWARN = .TRUE.
         END IF
***
* Comment out this if block if EMIN is ok
         IF( IWARN ) THEN
            FIRST = .TRUE.
            WRITE( 6, FMT = 9999 )LEMIN
         END IF
***
*
*        Assume IEEE arithmetic if we found denormalised  numbers above,
*        or if arithmetic seems to round in the  IEEE style,  determined
*        in routine DLAMC1. A true IEEE machine should have both  things
*        true; however, faulty machines may have one or the other.
*
         IEEE = IEEE .OR. LIEEE1
*
*        Compute  RMIN by successive division by  BETA. We could compute
*        RMIN as BASE**( EMIN - 1 ),  but some machines underflow during
*        this computation.
*
         LRMIN = 1
         DO 30 I = 1, 1 - LEMIN
            LRMIN = DLAMC3( LRMIN*RBASE, ZERO )
   30    CONTINUE
*
*        Finally, call DLAMC5 to compute EMAX and RMAX.
*
         CALL DLAMC5( LBETA, LT, LEMIN, IEEE, LEMAX, LRMAX )
      END IF
*
      BETA = LBETA
      T = LT
      RND = LRND
      EPS = LEPS
      EMIN = LEMIN
      RMIN = LRMIN
      EMAX = LEMAX
      RMAX = LRMAX
*
      RETURN
*
 9999 FORMAT( / / ' WARNING. The value EMIN may be incorrect:-',
     $      '  EMIN = ', I8, /
     $      ' If, after inspection, the value EMIN looks',
     $      ' acceptable please comment out ',
     $      / ' the IF block as marked within the code of routine',
     $      ' DLAMC2,', / ' otherwise supply EMIN explicitly.', / )
*
*     End of DLAMC2
*
      END
*
************************************************************************
*
      DOUBLE PRECISION FUNCTION DLAMC3( A, B )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      DOUBLE PRECISION   A, B
*     ..
*
*  Purpose
*  =======
*
*  DLAMC3  is intended to force  A  and  B  to be stored prior to doing
*  the addition of  A  and  B ,  for use in situations where optimizers
*  might hold one of these in a register.
*
*  Arguments
*  =========
*
*  A, B    (input) DOUBLE PRECISION
*          The values A and B.
*
* =====================================================================
*
*     .. Executable Statements ..
*
      DLAMC3 = A + B
*
      RETURN
*
*     End of DLAMC3
*
      END
*
************************************************************************
*
      SUBROUTINE DLAMC4( EMIN, START, BASE )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      INTEGER            BASE, EMIN
      DOUBLE PRECISION   START
*     ..
*
*  Purpose
*  =======
*
*  DLAMC4 is a service routine for DLAMC2.
*
*  Arguments
*  =========
*
*  EMIN    (output) EMIN
*          The minimum exponent before (gradual) underflow, computed by
*          setting A = START and dividing by BASE until the previous A
*          can not be recovered.
*
*  START   (input) DOUBLE PRECISION
*          The starting point for determining EMIN.
*
*  BASE    (input) INTEGER
*          The base of the machine.
*
* =====================================================================
*
*     .. Local Scalars ..
      INTEGER            I
      DOUBLE PRECISION   A, B1, B2, C1, C2, D1, D2, ONE, RBASE, ZERO
*     ..
*     .. External Functions ..
      DOUBLE PRECISION   DLAMC3
      EXTERNAL           DLAMC3
*     ..
*     .. Executable Statements ..
*
      A = START
      ONE = 1
      RBASE = ONE / BASE
      ZERO = 0
      EMIN = 1
      B1 = DLAMC3( A*RBASE, ZERO )
      C1 = A
      C2 = A
      D1 = A
      D2 = A
*+    WHILE( ( C1.EQ.A ).AND.( C2.EQ.A ).AND.
*    $       ( D1.EQ.A ).AND.( D2.EQ.A )      )LOOP
   10 CONTINUE
      IF( ( C1.EQ.A ) .AND. ( C2.EQ.A ) .AND. ( D1.EQ.A ) .AND.
     $    ( D2.EQ.A ) ) THEN
         EMIN = EMIN - 1
         A = B1
         B1 = DLAMC3( A / BASE, ZERO )
         C1 = DLAMC3( B1*BASE, ZERO )
         D1 = ZERO
         DO 20 I = 1, BASE
            D1 = D1 + B1
   20    CONTINUE
         B2 = DLAMC3( A*RBASE, ZERO )
         C2 = DLAMC3( B2 / RBASE, ZERO )
         D2 = ZERO
         DO 30 I = 1, BASE
            D2 = D2 + B2
   30    CONTINUE
         GO TO 10
      END IF
*+    END WHILE
*
      RETURN
*
*     End of DLAMC4
*
      END
*
************************************************************************
*
      SUBROUTINE DLAMC5( BETA, P, EMIN, IEEE, EMAX, RMAX )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     October 31, 1992
*
*     .. Scalar Arguments ..
      LOGICAL            IEEE
      INTEGER            BETA, EMAX, EMIN, P
      DOUBLE PRECISION   RMAX
*     ..
*
*  Purpose
*  =======
*
*  DLAMC5 attempts to compute RMAX, the largest machine floating-point
*  number, without overflow.  It assumes that EMAX + abs(EMIN) sum
*  approximately to a power of 2.  It will fail on machines where this
*  assumption does not hold, for example, the Cyber 205 (EMIN = -28625,
*  EMAX = 28718).  It will also fail if the value supplied for EMIN is
*  too large (i.e. too close to zero), probably with overflow.
*
*  Arguments
*  =========
*
*  BETA    (input) INTEGER
*          The base of floating-point arithmetic.
*
*  P       (input) INTEGER
*          The number of base BETA digits in the mantissa of a
*          floating-point value.
*
*  EMIN    (input) INTEGER
*          The minimum exponent before (gradual) underflow.
*
*  IEEE    (input) LOGICAL
*          A logical flag specifying whether or not the arithmetic
*          system is thought to comply with the IEEE standard.
*
*  EMAX    (output) INTEGER
*          The largest exponent before overflow
*
*  RMAX    (output) DOUBLE PRECISION
*          The largest machine floating-point number.
*
* =====================================================================
*
*     .. Parameters ..
      DOUBLE PRECISION   ZERO, ONE
      PARAMETER          ( ZERO = 0.0D0, ONE = 1.0D0 )
*     ..
*     .. Local Scalars ..
      INTEGER            EXBITS, EXPSUM, I, LEXP, NBITS, TRY, UEXP
      DOUBLE PRECISION   OLDY, RECBAS, Y, Z
*     ..
*     .. External Functions ..
      DOUBLE PRECISION   DLAMC3
      EXTERNAL           DLAMC3
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MOD
*     ..
*     .. Executable Statements ..
*
*     First compute LEXP and UEXP, two powers of 2 that bound
*     abs(EMIN). We then assume that EMAX + abs(EMIN) will sum
*     approximately to the bound that is closest to abs(EMIN).
*     (EMAX is the exponent of the required number RMAX).
*
      LEXP = 1
      EXBITS = 1
   10 CONTINUE
      TRY = LEXP*2
      IF( TRY.LE.( -EMIN ) ) THEN
         LEXP = TRY
         EXBITS = EXBITS + 1
         GO TO 10
      END IF
      IF( LEXP.EQ.-EMIN ) THEN
         UEXP = LEXP
      ELSE
         UEXP = TRY
         EXBITS = EXBITS + 1
      END IF
*
*     Now -LEXP is less than or equal to EMIN, and -UEXP is greater
*     than or equal to EMIN. EXBITS is the number of bits needed to
*     store the exponent.
*
      IF( ( UEXP+EMIN ).GT.( -LEXP-EMIN ) ) THEN
         EXPSUM = 2*LEXP
      ELSE
         EXPSUM = 2*UEXP
      END IF
*
*     EXPSUM is the exponent range, approximately equal to
*     EMAX - EMIN + 1 .
*
      EMAX = EXPSUM + EMIN - 1
      NBITS = 1 + EXBITS + P
*
*     NBITS is the total number of bits needed to store a
*     floating-point number.
*
      IF( ( MOD( NBITS, 2 ).EQ.1 ) .AND. ( BETA.EQ.2 ) ) THEN
*
*        Either there are an odd number of bits used to store a
*        floating-point number, which is unlikely, or some bits are
*        not used in the representation of numbers, which is possible,
*        (e.g. Cray machines) or the mantissa has an implicit bit,
*        (e.g. IEEE machines, Dec Vax machines), which is perhaps the
*        most likely. We have to assume the last alternative.
*        If this is true, then we need to reduce EMAX by one because
*        there must be some way of representing zero in an implicit-bit
*        system. On machines like Cray, we are reducing EMAX by one
*        unnecessarily.
*
         EMAX = EMAX - 1
      END IF
*
      IF( IEEE ) THEN
*
*        Assume we are on an IEEE machine which reserves one exponent
*        for infinity and NaN.
*
         EMAX = EMAX - 1
      END IF
*
*     Now create RMAX, the largest machine number, which should
*     be equal to (1.0 - BETA**(-P)) * BETA**EMAX .
*
*     First compute 1.0 - BETA**(-P), being careful that the
*     result is less than 1.0 .
*
      RECBAS = ONE / BETA
      Z = BETA - ONE
      Y = ZERO
      DO 20 I = 1, P
         Z = Z*RECBAS
         IF( Y.LT.ONE )
     $      OLDY = Y
         Y = DLAMC3( Y, Z )
   20 CONTINUE
      IF( Y.GE.ONE )
     $   Y = OLDY
*
*     Now multiply by BETA**EMAX to get RMAX.
*
      DO 30 I = 1, EMAX
         Y = DLAMC3( Y*BETA, ZERO )
   30 CONTINUE
*
      RMAX = Y
      RETURN
*
*     End of DLAMC5
*
      END
      LOGICAL          FUNCTION LSAME( CA, CB )
*
*  -- LAPACK auxiliary routine (version 2.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     January 31, 1994
*
*     .. Scalar Arguments ..
      CHARACTER          CA, CB
*     ..
*
*  Purpose
*  =======
*
*  LSAME returns .TRUE. if CA is the same letter as CB regardless of
*  case.
*
*  Arguments
*  =========
*
*  CA      (input) CHARACTER*1
*  CB      (input) CHARACTER*1
*          CA and CB specify the single characters to be compared.
*
* =====================================================================
*
*     .. Intrinsic Functions ..
      INTRINSIC          ICHAR
*     ..
*     .. Local Scalars ..
      INTEGER            INTA, INTB, ZCODE
*     ..
*     .. Executable Statements ..
*
*     Test if the characters are equal
*
      LSAME = CA.EQ.CB
      IF( LSAME )
     $   RETURN
*
*     Now test for equivalence if both characters are alphabetic.
*
      ZCODE = ICHAR( 'Z' )
*
*     Use 'Z' rather than 'A' so that ASCII can be detected on Prime
*     machines, on which ICHAR returns a value with bit 8 set.
*     ICHAR('A') on Prime machines returns 193 which is the same as
*     ICHAR('A') on an EBCDIC machine.
*
      INTA = ICHAR( CA )
      INTB = ICHAR( CB )
*
      IF( ZCODE.EQ.90 .OR. ZCODE.EQ.122 ) THEN
*
*        ASCII is assumed - ZCODE is the ASCII code of either lower or
*        upper case 'Z'.
*
         IF( INTA.GE.97 .AND. INTA.LE.122 ) INTA = INTA - 32
         IF( INTB.GE.97 .AND. INTB.LE.122 ) INTB = INTB - 32
*
      ELSE IF( ZCODE.EQ.233 .OR. ZCODE.EQ.169 ) THEN
*
*        EBCDIC is assumed - ZCODE is the EBCDIC code of either lower or
*        upper case 'Z'.
*
         IF( INTA.GE.129 .AND. INTA.LE.137 .OR.
     $       INTA.GE.145 .AND. INTA.LE.153 .OR.
     $       INTA.GE.162 .AND. INTA.LE.169 ) INTA = INTA + 64
         IF( INTB.GE.129 .AND. INTB.LE.137 .OR.
     $       INTB.GE.145 .AND. INTB.LE.153 .OR.
     $       INTB.GE.162 .AND. INTB.LE.169 ) INTB = INTB + 64
*
      ELSE IF( ZCODE.EQ.218 .OR. ZCODE.EQ.250 ) THEN
*
*        ASCII is assumed, on Prime machines - ZCODE is the ASCII code
*        plus 128 of either lower or upper case 'Z'.
*
         IF( INTA.GE.225 .AND. INTA.LE.250 ) INTA = INTA - 32
         IF( INTB.GE.225 .AND. INTB.LE.250 ) INTB = INTB - 32
      END IF
      LSAME = INTA.EQ.INTB
*
*     RETURN
*
*     End of LSAME
*
      END
      integer function idamax(n,dx,incx)
c
c     finds the index of element having max. absolute value.
c     jack dongarra, linpack, 3/11/78.
c     modified 3/93 to return if incx .le. 0.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision dx(*),dmax
      integer i,incx,ix,n
c
      idamax = 0
      if( n.lt.1 .or. incx.le.0 ) return
      idamax = 1
      if(n.eq.1)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      ix = 1
      dmax = dabs(dx(1))
      ix = ix + incx
      do 10 i = 2,n
         if(dabs(dx(ix)).le.dmax) go to 5
         idamax = i
         dmax = dabs(dx(ix))
    5    ix = ix + incx
   10 continue
      return
c
c        code for increment equal to 1
c
   20 dmax = dabs(dx(1))
      do 30 i = 2,n
         if(dabs(dx(i)).le.dmax) go to 30
         idamax = i
         dmax = dabs(dx(i))
   30 continue
      return
      end
      subroutine  dcopy(n,dx,incx,dy,incy)
c
c     copies a vector, x, to a vector, y.
c     uses unrolled loops for increments equal to one.
c     jack dongarra, linpack, 3/11/78.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision dx(*),dy(*)
      integer i,incx,incy,ix,iy,m,mp1,n
c
      if(n.le.0)return
      if(incx.eq.1.and.incy.eq.1)go to 20
c
c        code for unequal increments or equal increments
c          not equal to 1
c
      ix = 1
      iy = 1
      if(incx.lt.0)ix = (-n+1)*incx + 1
      if(incy.lt.0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dy(iy) = dx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
c
c        code for both increments equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,7)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dy(i) = dx(i)
   30 continue
      if( n .lt. 7 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,7
        dy(i) = dx(i)
        dy(i + 1) = dx(i + 1)
        dy(i + 2) = dx(i + 2)
        dy(i + 3) = dx(i + 3)
        dy(i + 4) = dx(i + 4)
        dy(i + 5) = dx(i + 5)
        dy(i + 6) = dx(i + 6)
   50 continue
      return
      end
