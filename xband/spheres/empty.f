      subroutine emptyspheres(W,avc,bvc,cvc,bas,is,alat,natom,nsort,S
     $     ,nsortes,taues,ses,max2sort,itop,iqa,ng,isnew,basnew,
     $     natomnew)
      implicit real*8 (a-h,o-z)
      integer w(1)
      parameter (npatb0=200)
      parameter (npatb1=6000)
      dimension is(*),S(*),itop(*),iqa(*)
      dimension avc(3),bvc(3),cvc(3),taues(3,*),ses(*),bas(3,*)
      dimension gen(3,3,48),ang(3),vc(3,48),vv(3),isnew(*),basnew(*)
c
      MAXNPAT=max2sort                  !npatb0
      MAXNPATG=npatb1

      do i=1,3
        vv(i)=-bas(i,1)
      enddo

      DO Igc=1,ng
        ig=itop(igc)
	IGA=IG
        IF(IGA.GT.32)IGA=IGA-32
        CALL EILANG(ANG,IGA)
        CALL TURNM(ANG,GEN(1,1,IGc))
C                              !DEF TURN MATRIX
        IF(IG.NE.IGA)THEN
C                              !ADD INVERSION IF NEED
          DO  I=1,3
            DO  J=1,3
              GEN(I,J,IGc)=-GEN(I,J,IGc)
            ENDDO
          ENDDO
        ENDIF
        call vec(vc(1,igc),bas(1,iqa(igc)),vv,GEN(1,1,IGC))
c        print*,Igc,ig,(vc(i,igc),i=1,3)
      ENDDO

      do i=1,ng
        ig=itop(i)
        
      enddo
c      call defrr(i_basnew,MAXNPAT*3)
c      call defi(i_isnew,MAXNPAT)
      call defrr(i_snew,MAXNPAT)
      call defrr(i_s2new,MAXNPAT)
      call defi(i_isb,MAXNPATG)
      call defrr(i_tau,MAXNPATG*3)     
      call defi(i_nhsortes,maxnpat)
      call defi(i_iskip,maxnpat)
      call empty(ier,avc,bvc,cvc,bas,is,alat,natom,nsort,S,
     $     basnew,isnew,w(i_snew),w(i_s2new),
     $     w(i_isb),w(i_tau),MAXNPAT,MAXNPATG,nsortes,taues,ses
     $     ,w(i_nhsortes),w(i_iskip),gen,vc,ng,natomnew)
c      nsortes=iter-1
      write(ilun(16),*)' IRR. POSITIONS:'
      do i=1,nsortes
        write(ilun(16),"(3f14.8)")(taues(j,i),j=1,3)
      enddo
c      stop
      end
c
      subroutine empty(ier,avc,bvc,cvc,bas,is,alat,natom,nsort,S,
     $     basnew,isnew,snew,s2new,isb,tau,maxnpat,maxnpatg,nsortes
     $     ,taues,ses,nhsortes,iskip,gm,vc,ng,natomnew)
      implicit real*8 (a-h,o-z)
      dimension isb(*),S(*),snew(*),s2new(*),is(*),isnew(*),tau(3,*)
     $     ,bas(3,*),basnew(3,*)
      dimension avc(3),bvc(3),cvc(3),taues(3,*),ses(*),nhsortes(*)
     $     ,iskip(*)
      dimension gm(3,3,48),ang(3),vc(3,48),vv(3),ve(3,48)
     $     ,vshift(3)
      common /emptys/nates,n1es,n2es,n3es,rmines,rmaxes
c      common /genop/gm,vc,vshift,ng,it
      common /u/u(3,3),um1(3,3),idatastr
      common /tau2/ boa,coa
c
c--------------------------------------
      do i=1,3
        do j=1,3
          u(i,j)=0
          um1(i,j)=0
          gm(i,j,1)=0
        enddo
        u(i,i)=1
        um1(i,i)=1
        gm(i,i,1)=1
        vshift(i)=0
        vc(i,1)=0
      enddo
c      it(1)=1
c     ng=1
      boa=1
      coa=1
c---------------------------------------
      iun=ilun(16)
      nates=0
      ssm=0
      do i=1,nsort
        snew(i)=s(i)/alat
c        print*,snew(i),s(i)
        ssm=max(ssm,snew(i))
        s2new(i)=snew(i)**2
      enddo
      invmax=0
      if(rmaxes.lt.0)invmax=1
      if(rmines.lt.0)invmax=-invmax
      rmines=abs(rmines)
      rmaxes=abs(rmaxes)
      sm=rmaxes/alat+ssm
      smn=rmines/alat
      do iatom=1,natom
        isnew(iatom)=is(iatom)
        do i=1,3
          basnew(i,iatom)=bas(i,iatom)
        enddo
      enddo
      natomnew=natom
      nsortnew=nsort
      iter=0
      print*,'BOA,COA:',boa,coa,alat
      write(iun,*) '  Searching of Empty spheres  '
      write(*,*) '  Searching of Empty spheres  '
      write(iun,'(a,3i3)')' Mesh: ',n1es,n2es,n3es
      write(iun,1000)'Smin, Smax (a.u.): ',RMINES,RMAXES
 22   continue                          ! beginning of the cycle over iter
      iter=iter+1
      write(iun,*)'  Iteration ',iter
      write(*,*)'  Iteration ',iter
      call getepos(maxnpatg,avc,bvc,cvc,basnew,natomnew,isnew,tau,natb
     $     ,isb,Snew,S2new,n1es,n2es,n3es,pa,pb,pc,rad,ier,sm,iter)
      write(iun,1000)' Maximum possible sphere is ',rad*alat
      write(iun,1000)' Position in a,b,c:',pa,pb,pc
 1000 format(1x,a,3f14.8)
      if(rad.lt.smn)then
        write(iun,*)'skipped'
        print *,'RAD=',RAD*alat,' RMINES=',RMINES
        goto 30
      endif
      if(ier.ne.0)then
c        print*,'IER=',ier
        goto 30
      endif
      iskip(iter)=0
      if(rad.gt.rmaxes/alat)then
        if(invmax.eq.0)then
          write(iun,1000)'Contracted to be ',rmaxes
          rad=rmaxes/alat               ! rad is used later
        else
          iskip(iter)=invmax
          write(iun,1000)'Sphere is too large',rad*alat
          write(iun,1000)'Small auxiliary sphere is inserted instead'
        endif
      endif
c
c multiplication :
      ve(1,1)=pa*avc(1)+pb*bvc(1)+pc*cvc(1)-vshift(1)
      ve(2,1)=pa*avc(2)+pb*bvc(2)+pc*cvc(2)-vshift(2)
      ve(3,1)=pa*avc(3)+pb*bvc(3)+pc*cvc(3)-vshift(3)

      itav=0
 27   continue
      itav=itav+1
      in=1
      eps_angmax=0.d0
      call shortn(ve,ve)
c       print*,(ve(i,1),i=1,3)
      do ig=1,ng
        call vec(vv,vc(1,ig),ve,gm(1,1,ig))
c         print*,vv
        do iin=1,in
          ang(1)=ve(1,iin)-vv(1)
          ang(2)=ve(2,iin)-vv(2)
          ang(3)=ve(3,iin)-vv(3)
          call shortn(ang,ang)
          eps_ang=ang(1)**2+ang(2)**2+ang(3)**2
          if(eps_ang.lt.1.d-5)then
            eps_angmax=max(eps_ang,eps_angmax)
c              print*,iin,eps_ang
            goto 800
          endif
        enddo
        in=in+1
        ve(1,in)=vv(1)
        ve(2,in)=vv(2)
        ve(3,in)=vv(3)
c         print*,'in=',in,vv
 800    continue
      enddo
      vv(1)=0
      vv(2)=0
      vv(3)=0
      iodn=0
      demin=1.d10
      do iin=2,in
        ang(1)=ve(1,iin)-ve(1,1)
        ang(2)=ve(2,iin)-ve(2,1)
        ang(3)=ve(3,iin)-ve(3,1)
        call shortn(ang,ang)
        eps_ang=ang(1)**2+ang(2)**2+ang(3)**2
        eps_ang=sqrt(eps_ang)
        if(demin.gt.eps_ang)demin=eps_ang
        if(eps_ang.lt.rad*1.5d0)then
c$$$          print *,eps_ang*alat
c           print1212,iin,(ve(iu,iin),iu=1,3)
c           print1212,iin,ve(1,1)+ang(1),ve(2,1)+ang(2),ve(3,1)+ang(3)
c           print1212,-1000,ang
          vv(1)=vv(1)+ang(1)
          vv(2)=vv(2)+ang(2)
          vv(3)=vv(3)+ang(3)
          iodn=iodn+1
        endif
      enddo                             ! iin
      if(iodn.eq.0)then
        if(demin.lt.2.d0*rad)then       ! overlapping spheres
          write(iun,*)'radius is decreased from',rad,' to',demin/2.d0
          rad=demin/2.d0                ! decrease radius to touching
          if(rad.lt.smn)then
            write(iun,*)'sphere is less then rmines'
            write(iun,*)'rad=',rad*alat,' rmines=',rmines
            iskip(iter)=-1              ! will be skipped later
c$$$            goto 30
          endif
        endif
      else                              ! averaging
c$$$      if(iodn.gt.0)then
        write(iun,'(a,i3,2f14.8)')'a bit shifted position',iodn
     $       ,real(demin),real(rad)
        write(iun,1000)'OLD:',ve(1,1),ve(2,1)/boa,ve(3,1)/coa
        ve(1,1)=ve(1,1)+vv(1)/(iodn+1)
        ve(2,1)=ve(2,1)+vv(2)/(iodn+1)
        ve(3,1)=ve(3,1)+vv(3)/(iodn+1)
        write(iun,1000)'new:',ve(1,1),ve(2,1)/boa,ve(3,1)/coa
        call reget_re(tau,isb,snew,natb,vshift,ve(1,1),radnew)
        write(iun,1000)'rad,radtst',rad*alat,radnew*alat
        rad=radnew
        if(itav.gt.5)then               ! to avoide infinite cycle
          write(iun,*)'can not find averaged position'
          stop
        else
          goto 27
        endif
      endif
c        print*,'nsortnew=',nsortnew,'  iib=',in
      write(iun,'(a,i4,a)')'it will be ',in,' ES per unit cell:'
      if(natomnew+in.gt.maxnpat)then
        print *,'EMPTY: too many atoms',natomnew+in,' maxnpat=',maxnpat
        print *,'try to increase npatb0 in empty.f'
        stop
      endif
      do iatom=natomnew+1,natomnew+in
        isnew(iatom)=nsortnew+1
        do i=1,3
          basnew(i,iatom)=ve(i,iatom-natomnew)+vshift(i)
        enddo
c$$$        write(24)(basnew(i,iatom),i=1,3)
        write(iun,'(2x,3f15.8)')(basnew(i,iatom),i=1,3)
      enddo

      do k=1,3
        vv(k)=0
        do j=1,3
          vv(k)=vv(k)+ve(j,1)*um1(j,k)
        enddo
        taues(k,iter)=vv(k)
      enddo
c      write(iun,'(i3,3f15.8)')iter,vv
      nsortnew=nsortnew+1
      natomnew=natomnew+in
      nhsortes(iter)=in
      if(iskip(iter).eq.0)then
        Snew(nsortnew)=min(rad,rmaxes/alat)
      else
        Snew(nsortnew)=rmines/alat
      endif
      S2new(nsortnew)=Snew(nsortnew)**2
      ses(iter)=Snew(nsortnew)*alat
      print *,in,' Empty spheres added, S=',Snew(nsortnew)*alat
      nates=nates+in
c     do iatom=1,natomnew
c       print121,iatom,(basnew(i,iatom),i=1,3),isnew(iatom)
c121    format(i3,3f10.5,i6)
c     enddo
      goto 22
c
 30   continue                          ! exit from the iter cycle
      nsortes=iter-1
      if(invmax.ge.0)then
c check if some spheres should be skipped due to small radius
        do isort=1,nsortes
          if(iskip(isort).lt.0)invmax=iskip(isort)
        enddo
      endif
      if(invmax.lt.0)then
c skip auxiliary spheres
c$$$        write(iun,*)'nsortes,natomnew',nsortes,natomnew
c$$$        do isort=1,nsortes
c$$$          write(iun,'(i3,3f14.8,2i4,f14.8)')isort,(taues(k,isort),k=1,3)
c$$$     $         ,nhsortes(isort),iskip(isort),ses(isort)
c$$$        enddo
c$$$        do iatom=1,natomnew
c$$$          write(iun,'(i3,3f14.8)')iatom,(basnew(k,iatom),k=1,3)
c$$$        enddo
c$$$        write(iun,*)'rmines',rmines
        iatom=1
        isort=1
        do while(isort.le.nsortes)
          write(iun,*)'isort,nsortes,s',isort,nsortes,ses(isort)
          nats=nhsortes(isort)
          if(iskip(isort).lt.0)then
            write(iun,1000)'sphere is skipped',(taues(k,isort),k=1,3)
            do i=isort+1,nsortes
              nhsortes(i-1)=nhsortes(i)
              ses(i-1)=ses(i)
              iskip(i-1)=iskip(i)
              do k=1,3
                taues(k,i-1)=taues(k,i)
              enddo
            enddo
            do i=iatom+nats,natomnew
              do k=1,3
                basnew(k,i-nats)=basnew(k,i)
              enddo
            enddo
            nsortes=nsortes-1
            natomnew=natomnew-nats
          else
            iatom=iatom+nats
            isort=isort+1
          endif
        enddo
c$$$        write(iun,*)'nsortes,natomnew',nsortes,natomnew
c$$$        do isort=1,nsortes
c$$$          write(iun,'(i3,3f14.8,i4,f14.8)')isort,(taues(k,isort),k=1,3)
c$$$     $         ,nhsortes(isort),ses(isort)
c$$$        enddo
c$$$        do iatom=1,natomnew
c$$$          write(iun,'(i3,3f14.8)')iatom,(basnew(k,iatom),k=1,3)
c$$$        enddo
      endif
c$$$      if(nsortes.gt.0)then
c$$$        open(24,status='scratch',form='unformatted')
c$$$        do iatom=natom+1,natomnew
c$$$          write(24)(basnew(i,iatom),i=1,3)
c$$$c$$$        write(iun,'(2x,3f15.8,i4)')(basnew(i,iatom),i=1,3),isnew(iatom)
c$$$        enddo
c$$$      endif
c MT -> ASA spheres      
      pi43=4.d0*dpi()/3.d0
      svola=0.d0
      do iatom=1,natom
        svola=svola+pi43*s(is(iatom))**3
      enddo
      svole=0.d0
      do isort=1,nsortes
        svole=svole+nhsortes(isort)*pi43*ses(isort)**3
c        print*,'SES::',ses(isort),isort
      enddo
      cvol=trnt(avc,bvc,cvc)*alat**3
      cs=cvol/(svola+svole)
      svola=svola*cs
      svole=svole*cs
      cs=cs**(1.d0/3.d0)
      do isort=1,nsort
        s(isort)=cs*s(isort)
      enddo
      do isort=1,nsortes
        ses(isort)=cs*ses(isort)
      enddo
      write(iun,1000)'Vat, Ves, V',svola,svole,svola+svole
      write(iun,1000)'Vat/V, Ves/V',svola/cvol,svole/cvol
      end
c
      subroutine getepos(npat,a,b,c,bas,natom,is,tau,natb
     $     ,isb,s,s2,n1,n2,n3,pae,pbe,pce,rad,ier,sm,iter)
      implicit real*8 (a-h,o-z)
      dimension bas(3,*),is(npat)
      dimension tau(3,npat),isb(npat),a(3),b(3),c(3),S(*),S2(*)
c$$$      logical lg
c
      DIAG=max(
     $     (a(1)+b(1)+c(1))**2+(a(2)+b(2)+c(2))**2+(a(3)+b(3)+c(3))**2,
     $     (a(1)-b(1)+c(1))**2+(a(2)-b(2)+c(2))**2+(a(3)-b(3)+c(3))**2,
     $     (a(1)+b(1)-c(1))**2+(a(2)+b(2)-c(2))**2+(a(3)+b(3)-c(3))**2,
     $     (a(1)-b(1)-c(1))**2+(a(2)-b(2)-c(2))**2+(a(3)-b(3)-c(3))**2)
      ier=0
      DIAG=sqrt(DIAG)/2
c      print*,'DIAG:',DIAG
      DDMAX=(SM+DIAG)**2
 1    format(a,3f7.3,3i3)
c      print*,'MAX D=',sqrt(DDMAX)
c      print *,'natom',natom
c      do i=1,natom
c       print11,i,(bas(k,i),k=1,3),is(i) ,S(is(i))!,S(isb(i))
c      enddo
      natb=0
      DO I=-3,3
        DO J=-3,3
          DO K=-3,3
            DX=I*A(1)+J*B(1)+K*C(1)
            DY=I*A(2)+J*B(2)+K*C(2)
            DZ=I*A(3)+J*B(3)+K*C(3)
            do iatom=1,natom
              X=DX+bas(1,iatom)
              Y=DY+bas(2,iatom)
              Z=DZ+bas(3,iatom)
c              print*,X,Y,Z
              DD=X**2+Y**2+Z**2
              if(DD.lt.DDMAX)then
                natb=natb+1
                tau(1,natb)=X
                tau(2,natb)=Y
                tau(3,natb)=Z
                if(natb.gt.npat)then
                  ier=1
                  print*,'IER=',IER
                  print *,'GETEPOS: too many atoms are generated ',natb
     $                 ,' npat=',npat
                  print *,'try to increase npatb1 in empty.f'
c$$$        stop
                  return
                endif
                isb(natb)=is(iatom)
              endif
            enddo                       ! iatom
          ENDDO                         ! k
        ENDDO                           ! j
      ENDDO                             ! i
c$$$      print*,' I generated',natb,' atoms'
c$$$      print *,'npat',npat
c$$$      do i=1,natb
c$$$        if(isb(i).le.0)then
c$$$          print11,i,(tau(k,i),k=1,3),isb(i),S2(isb(i)),S(isb(i))
c$$$          stop
c$$$        endif
c$$$      enddo
 11   format(i3,3x,3f15.10,2x,i3,2f10.5)
      pae=0
      pbe=0
      pce=0
c$$$      iae=0
c$$$      ibe=0
c$$$      ice=0
c      print*,n1,n2,n3
      ana=0.5d0/n1
      anb=0.5d0/n2
      anc=0.5d0/n3
      DMAX=0
c      npnt=0
      do ia=-n1,n1-1,2
        pa=ana*ia
        do ib=-n2,n2-1,2
          pb=anb*ib
          do ic=-n3,n3-1,2
c            npnt=npnt+1
            pc=anc*ic
c$$$            lg=ia.eq.-24.and.ib.eq.0.and.ic.eq.-72
            x=pa*a(1)+pb*b(1)+pc*c(1)
            y=pa*a(2)+pb*b(2)+pc*c(2)
            z=pa*a(3)+pb*b(3)+pc*c(3)
c           if(lg)print*,'XYZ:',x,y,z
            DMIN=1.d10
            do i=1,natb
              DIST2=(x-tau(1,i))**2+(y-tau(2,i))**2+(z-tau(3,i))**2
c             if(lg.and.isb(i).eq.8)print*,'TTT:',tau(1,i),tau(2,i)
c    $             ,tau(3,i)
c$$$              print *,'ia,ib,ic,i',ia,ib,ic,i,natb
c$$$              print *,'dist2,isb(i),sq',dist2,isb(i),s2(isb(i))
              if(DIST2.lt.S2(ISB(I)))then
c$$$                PRINT*,I,DIST2,ISB(I),S2(ISB(I))
c$$$                print*, x,y,z,ia,ib,ic
c$$$                print*,tau(1,i),tau(2,i),tau(3,i)
                goto 10
              endif
              DD=sqrt(DIST2)-S(ISB(I))
              DMIN=min(DD,DMIN)
            enddo                       ! i
c            print*,DMIN
            if(DMIN.gt.DMAX)then
              DMAX=DMIN
              pae=pa
              pbe=pb
              pce=pc
c$$$              iae=ia
c$$$              ibe=ib
c$$$              ice=ic
            endif
 10         continue
          enddo                         ! ic
        enddo                           ! ib
      enddo                             ! ia
c      print*,iter,'MAX is:',DMAX,iae,ibe,ice,pae,pbe,pce
c      print*,iter,'npnt=',npnt
      rad=dmax
      end
c
      subroutine reget_re(tau,isb,s,natb,vshift,vs,rad)
      implicit real*8 (a-h,o-z)
      dimension tau(3,*),isb(*),s(*),vshift(*),vs(*)
      dimension v(3)
c
      do k=1,3
        v(k)=vs(k)+vshift(k)
      enddo
      dmin=1.d10
      do i=1,natb
        d=(v(1)-tau(1,i))**2+(v(2)-tau(2,i))**2+(v(3)-tau(3,i))**2
        d=sqrt(d)-s(isb(i))
        if(d.lt.0.d0)then
          print *,'REGET_RE: error'
          print *,i,d+s(isb(i)),isb(i),s(isb(i))
          print *,(v(k),k=1,3)
          print *,tau(1,i),tau(2,i),tau(3,i)
          return
        endif
        dmin=min(d,dmin)
      enddo                             ! natb
      rad=dmin
      end
