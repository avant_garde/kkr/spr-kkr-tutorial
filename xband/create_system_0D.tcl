#
# setting up the structure  for   0D systems
#
#
#   NQ       total number of lattice sites in unit cell (independent of occupation)
#   NCL      number of inequivalent lattice sites
#   NQCL     number of equivalent sites IQ for class ICL with ICL = 1,..,NCL  
#   NT       number of atom types 
#   NAT      number of equivalent sites IQ occupied by atom type IT with  IT = 1,..,NT
#   CONC     concentration of atom type IT (on a site)
#
#   for ordered compounds    NCL        == NT
#                            NQCL(ICL)  == NAT(IT)
#                            CONC(IT)   == 1
#
#
#
#
#
# Copyright (C) 2004 H. Ebert / S. Seidler
#
########################################################################################



proc create_system_0D {} {

global sysfile syssuffix COLOR FONT HEIGHT Wclua
global system 
global IQ_ordered NVEC_LAY W_basvec IQCNTR


#
#------------------------------------------------------------------------------------ ND
#
global structure_window_calls
global sysfile syssuffix 
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
global system NQ NT ZT RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global NM IMT IMQ RWSM NAT QMTET QMPHI 
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION DIMENSION_HOST
#
#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE  ZRANGE_TYPE  STRUCTURE_SEQUENCE
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R 

global run_geometry_task
global L_BOX anchor_mode NQBOX NLAYBOX
global CLURAD NSHLCLU clu_box_show clu_box_dist

global PRC

set PRC create_system_0D

set run_beta_version 0

#=======================================================================================
#                                system file  NOT  available
#=======================================================================================
if { "$sysfile$syssuffix" == ".sys" || "$sysfile$syssuffix" == "system.sys" } {

    set win .c_s_sys
    toplevel_init $win "Create or select a system file" 0 0
    wm geometry $win +50+200
    wm positionfrom .c_s_sys user
    wm sizefrom .c_s_sys ""
    wm minsize .c_s_sys 

    set buttonbgcolor green1 
    set buttonheight  2

    label .c_s_sys.lab -text "create or select a system file first"
#
#---------------------------------------------------- CREATE SYSTEM 
#
    button .c_s_sys.csys -text "CREATE SYSTEM" -width 20 -height $buttonheight \
	-command {create_system; destroy .c_s_sys} -bg $buttonbgcolor
#
#---------------------------------------------------- SELECT SYSTEM 
#
     button .c_s_sys.ssys -text "SELECT SYSTEM" -width 20 -height $buttonheight \
	-command {select_system; destroy .c_s_sys} -bg $buttonbgcolor

#
#---------------------------------------------------- SELECT SYSTEM 
#
     button .c_s_sys.ende -text "CLOSE" -width 20 -height $buttonheight \
	-command {destroy .c_s_sys} -bg $COLOR(CLOSE)

     pack configure .c_s_sys.lab .c_s_sys.csys .c_s_sys.ssys .c_s_sys.ende \
            -anchor s  -side top -pady 15 -padx 30

#=======================================================================================

     return
} 



#=======================================================================================
#                                system file available
#=======================================================================================


if [file exists $sysfile$syssuffix] {
	read_sysfile
} else {
	give_warning . "WARNING \n\n the system file \n\n $sysfile$syssuffix \n\n does not exist"
	return
}             


if {$DIMENSION=="0D"} {
   give_warning . "WARNING \n\n select 2D or 3D host system \n\n 
           $sysfile$syssuffix \n\n contains 0D system information \n"
	return
}             

set DIMENSION_HOST $DIMENSION
set DIMENSION      "0D"


if {![info exists run_geometry_task]} { set run_geometry_task cr_rec_regime }
; # "cr_rec_regime" "cr_sph_regime"

if {![info exists IQCNTR]}      {set IQCNTR    1}
if {![info exists IQANCHOR]}    {set IQANCHOR  1}
if {![info exists CLURAD]}      {set CLURAD    0}
if {![info exists NSHLCLU]}     {set NSHLCLU   2}

if {![info exists L_BOX(1)]}    {set L_BOX(1) 3.0}
if {![info exists L_BOX(2)]}    {set L_BOX(2) 3.0}
if {![info exists L_BOX(3)]}    {set L_BOX(3) 3.0}

if {![info exists NVEC_LAY(1)]} {set NVEC_LAY(1) 0}
if {![info exists NVEC_LAY(2)]} {set NVEC_LAY(2) 0}
if {![info exists NVEC_LAY(3)]} {set NVEC_LAY(3) 1}

if {![info exists IQ_ordered(3)]} {
	for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	    set IQ_ordered($IQ) $IQ
	}
}
    
set Wclua .w_clua 
if {[winfo exists $Wclua]} {destroy $Wclua} 
toplevel_init $Wclua "create 0-dimensional system  STEP A" 0 0
wm sizefrom $Wclua ""
wm minsize  $Wclua 100 100

# top button frame
frame $Wclua.head

pack $Wclua.head -fill both -expand yes

button $Wclua.head.reset -text "RESET" \
	    -width 25 -height $HEIGHT(BUT1) -bg tomato  \
	    -command "create_system_0D" 
button $Wclua.head.cancel -text "CLOSE" \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	    -command "read_sysfile ; destroy $Wclua" 
button $Wclua.head.rasmol -text "SHOW STRUCTURE"  \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	    -bg $COLOR(GREEN) -command "sites_graphik"

pack $Wclua.head.rasmol $Wclua.head.reset $Wclua.head.cancel  \
	    -side left -expand yes -pady 5

#=======================================================================================

set BG1 linen                
set BG2 SteelBlue1 
set BG3 ivory1
set BG4 LightSalmon1
set BG5 lightgreen

global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1 
global BGWR  ; set BGWR   lavender
global BGCLU ; set BGCLU  LightSkyBlue 
global BGE   ; set BGE    moccasin 
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1  
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1 

set BGSCL orange     
set BG_A khaki1
set BG_a gold1
set BG_x yellow1
set BG_b goldenrod1
set we_old 6
set we_new 8
set we_atoms 30

# lower mainframe
frame $Wclua.a 
pack $Wclua.a -in $Wclua -side top -expand yes

set ACOL1 $Wclua.a.col1 ; frame $ACOL1 -bg $BG1  
set ACOL2 $Wclua.a.col2 ; frame $ACOL2 -bg $BG2                                 

pack $ACOL2 $ACOL1 -side right -anchor n  -fill x 
 
#=======================================================================================
#=======================================================================================
#                                     COLUMN 1
#=======================================================================================
#=======================================================================================
#                              lattice parameter
#

set ALAT [expr 1.0 * $ALAT] ; # just to skip trailing 0's

frame $ACOL1.sys -bg $BG1

label $ACOL1.sys.txt  -text "system  "      -bg $BG1
label $ACOL1.sys.nam  -textvariable sysfile -bg $BG1
pack  $ACOL1.sys.txt $ACOL1.sys.nam  -padx 5  -side left
pack  $ACOL1.sys -padx 3 -pady 12 -anchor nw

frame $ACOL1.alat -bg $BG1
label $ACOL1.alat.lab  -text "lattice parameter  A  " -bg $BG1

entry $ACOL1.alat.ent  -font $FONT(GEN) -width 7      -bg $BG1 \
	    -textvariable ALAT  -relief flat \
	    -highlightthickness 0 -state disabled 
label $ACOL1.alat.uni  -text "  \[a.u.\]" -bg $BG1

pack  $ACOL1.alat.lab $ACOL1.alat.ent $ACOL1.alat.uni -padx 5  -side left
pack  $ACOL1.alat -padx 3 -pady 7 -anchor w

#=======================================================================================
#                              primitive vectors

label $ACOL1.head  -text "primitive vectors " -bg $BG1
pack  $ACOL1.head -anchor c 

# just to skip trailing 0's

for {set i 1} {$i <= 3} {incr i} {
	set RBASX($i) [expr 1.0 * $RBASX($i)]
	set RBASY($i) [expr 1.0 * $RBASY($i)]
	set RBASZ($i) [expr 1.0 * $RBASZ($i)]
}
    
if {$DIMENSION_HOST=="2D"} {
	if { $ZRANGE_TYPE=="extended" } {
	    for {set i 1} {$i <= 3} {incr i} {
		debug $PRC "RBASZ_L:  $RBASX_L($i) $RBASY_L($i) $RBASZ_L($i)"
		debug $PRC "RBASZ_R:  $RBASX_R($i) $RBASY_R($i) $RBASZ_R($i)"
		set RBASX_L($i) [expr 1.0 * $RBASX_L($i)]
		set RBASY_L($i) [expr 1.0 * $RBASY_L($i)]
		set RBASZ_L($i) [expr 1.0 * $RBASZ_L($i)]
		set RBASX_R($i) [expr 1.0 * $RBASX_R($i)]
		set RBASY_R($i) [expr 1.0 * $RBASY_R($i)]
		set RBASZ_R($i) [expr 1.0 * $RBASZ_R($i)]
	    }
	    
	    set N_prim_vec 5
	}
} else {
	for {set i 1} {$i <= 3} {incr i} {
	    set RBASX_L($i) $RBASX($i)
	    set RBASY_L($i) $RBASY($i)
	    set RBASZ_L($i) $RBASZ($i)
	    set RBASX_R($i) $RBASX($i)
	    set RBASY_R($i) $RBASY($i)
	    set RBASZ_R($i) $RBASZ($i)
	}
	
	set N_prim_vec 3
}

frame $ACOL1.pvec -bg $BG1 
pack  $ACOL1.pvec -side top
    
for {set i 1} {$i <= $N_prim_vec} {incr i} {
	
	frame $ACOL1.pvec.cmp$i -bg $BG1
	
	if {$i<=3} {
	    label $ACOL1.pvec.cmp$i.lab  -text "a$i \[A\]" -width $we_old -bg $BG1
	    entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASX($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1
	    entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASY($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1
	    entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASZ($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1      
	} else {
	    label $ACOL1.pvec.cmp$i.lab  -width $we_old -bg $BG1
	    entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1
	    entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1
	    entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1    
	    if {$i==4} {
		frame $ACOL1.pvec.space -bg $BG1
		pack  $ACOL1.pvec.space -pady 2
		$ACOL1.pvec.cmp$i.lab  configure -text "a3 (L)"
		$ACOL1.pvec.cmp$i.x configure -textvariable RBASX_L(3) -state normal
		$ACOL1.pvec.cmp$i.y configure -textvariable RBASY_L(3) -state normal
		$ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_L(3) -state normal
		
	    } else {
		$ACOL1.pvec.cmp$i.lab  configure -text "a3 (R)"
		$ACOL1.pvec.cmp$i.x configure -textvariable RBASX_R(3)
		$ACOL1.pvec.cmp$i.y configure -textvariable RBASY_R(3)
		$ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_R(3)
	    }        
	    $ACOL1.pvec.cmp$i.x configure -state disabled
	    $ACOL1.pvec.cmp$i.y configure -state disabled
	    $ACOL1.pvec.cmp$i.z configure -state disabled
	}
	
	pack  $ACOL1.pvec.cmp$i.lab   $ACOL1.pvec.cmp$i.x $ACOL1.pvec.cmp$i.y  \
		$ACOL1.pvec.cmp$i.z     -side left
	pack  $ACOL1.pvec.cmp$i 
	
}
#

label $ACOL1.space1 -text " " -bg $BG1
label $ACOL1.space2 -text "ordered basis vectors" -bg $BG1
pack $ACOL1.space1 $ACOL1.space2 -fill both -expand true  -anchor c

#=======================================================================================
#                                         basis vectors    
set W_basvec $ACOL1.basis
frame $W_basvec -bg $BG1
pack $W_basvec -padx 4 -fill both -expand true -side top

scrollbar $W_basvec.sby -bg grey77 -command [list $W_basvec.li yview] -orient vertical
scrollbar $W_basvec.sbx -bg grey77 -command [list $W_basvec.li xview] -orient horizontal

listbox $W_basvec.li -bg $COLOR(LIGHTBLUE) -height 20 -width 35 -font $FONT(GEN) \
	    -xscrollcommand [list $W_basvec.sbx set] -yscrollcommand [list $W_basvec.sby set]

pack $W_basvec     -side bottom -expand 1 -fill both -padx 5 -pady 5
pack $W_basvec.sbx -side bottom -expand 0 -fill x
pack $W_basvec.sby -side left   -expand 0 -fill y
pack $W_basvec.li  -side right  -expand 1 -fill both 

#-----------------------------------------------------------------
    proc display_basis_vectors { } {
	
	global W_basvec NQ RQX RQY RQZ TXTT NOQ ITOQ IQ_ordered
	global DIMENSION_HOST ZRANGE_TYPE NQ_bulk_L NQ_bulk_R
	
	run_geometry IQ_NLAY_order 
	
	$W_basvec.li delete 0 end
	$W_basvec.li insert 0 "   IQ     X     Y     Z      TXTT"
	
	for {set JQ 1} {$JQ <= $NQ} {incr JQ} {
	    
	    if {$DIMENSION_HOST=="2D" && $ZRANGE_TYPE=="extended" } {
		if {$JQ<=$NQ_bulk_L} {
		    set txt0 "LB"
		} elseif {$JQ>[expr $NQ - $NQ_bulk_R]} {
		    set txt0 "RB"
		} else {
		    set txt0 "IZ"        
		}
	    } else {
		set txt0 "  "
	    }
	    set IQ $IQ_ordered($JQ)
	    set txt1 [format "%3i  %6.2f%6.2f%6.2f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
	    set txt2 "  "
	    
	    for {set i 1} {$i <= $NOQ($IQ)} {incr i} {
		set IT $ITOQ($i,$IQ)
		
		set txt2 "$txt2[format "%4s " $TXTT($IT)]"
	    }
	    $W_basvec.li insert $JQ "$txt0$txt1$txt2"
	}
	
    } 
#-----------------------------------------------------------------
    
display_basis_vectors

#
#---------------------------------------------------------------------------------------
#

##label $ACOL1.bot -text " QQQQQ " -bg $BG1 -height 5
##pack $ACOL1.bot -fill both -expand true -side bottom -anchor s

#
#=======================================================================================
#=======================================================================================
#                                     COLUMN 2
#=======================================================================================
#=======================================================================================

    pack [frame $ACOL2.row1 -bg $BG2] -ipady 5 
    #-----------------------------------------------------------------
# radiobuttons cluster type: embedded or free
set f $ACOL2.row1.cluster_type
frame $f -height 30 -width 30 -bg $BG2
label $f.label -text "           cluster type" -bg $BG2
radiobutton $f.rad1 -variable cluster_type -text "embedded" \
	    -value "embedded" -highlightthickness 0 -bg $BG2
radiobutton $f.rad2 -variable cluster_type -text "free"  \
	    -value "free"  -highlightthickness 0 -bg $BG2
$f.rad1 select

pack $f.label -anchor n -expand 1 -side left
pack $f.rad1 $f.rad2 -anchor w -expand 1 -side left

if {$run_beta_version !=0} { pack $f -fill both }

#-------------------------------------------------------------------    
# layer normal vector line
frame $ACOL2.row1.nvec -bg $BG2  
if {$run_beta_version !=0} { pack  $ACOL2.row1.nvec -side top }

label $ACOL2.row1.nvec.lab -text "layer normal vector n  " -bg $BG2
pack  $ACOL2.row1.nvec.lab -anchor c -pady 7

for {set i 1} {$i <= 3} {incr i} {

   set NVEC_LAY($i) [expr {1.0 * $NVEC_LAY($i)}] ; # just do get rid of trailing 0's

   entry $ACOL2.row1.nvec.comp$i -font $FONT(GEN) -width $we_old -relief sunken \
	 -highlightthickness 0 -bg $BG2 -state normal -textvariable NVEC_LAY($i)
}

button $ACOL2.row1.nvec.reorder -text "reorder basis vectors" -bg green1 -height 2 \
	    -borderwidth 2 -command display_basis_vectors 

eval pack [winfo children $ACOL2.row1.nvec] -side left

#label $ACOL2.nvec_coord  -text "         (cartesian coordinates)" -bg $BG3
#pack  $ACOL2.nvec_coord -anchor c 

if {$DIMENSION_HOST == "2D"} {
	$ACOL2.row1.nvec.reorder configure -state disabled
	$ACOL2.row1.nvec.comp1   configure -state disabled
	$ACOL2.row1.nvec.comp2   configure -state disabled
	$ACOL2.row1.nvec.comp3   configure -state disabled
}

#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
global QUICK_0D 
if {$QUICK_0D==1} {
    set IQCNTR 6
}
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ


# -----------------------------------------------------------------------
# lattice site to anchor IQ_SEL  
set f $ACOL2.row1.anc
frame $f -height 30 -width 30 -bg $BG2
label $f.label -text "lattice site to anchor IQ_SEL" -bg $BG2 
scale $f.scale -orient horizontal -highlightthickness 0 -bg $BG2 \
	    -sliderlength 40 -from 1 -to $NQ -variable IQCNTR

pack $f.scale -anchor w -expand 1 -fill x -side right
pack $f.label -expand 1 -fill both -side right -padx 30
pack $f -side top -fill both

#################################################################################
#   row 2      sections "spherical regime" and "rectangular regime" 
#################################################################################
#
frame $ACOL2.row2 
pack $ACOL2.row2 -side top -fill both ; # -fill x 

set fsph $ACOL2.row2.sph ; frame $fsph -bg $BG4 ; # spherical   regime
set frec $ACOL2.row2.rec ; frame $frec -bg $BG3 ; # rectangular regime

if {$run_beta_version !=0} { 
pack $fsph -side left  -anchor n -fill both -ipady 10 -ipadx 15
pack $frec -side right -anchor n -fill both -ipady 10
} else {
pack $frec -side right -anchor n -fill both -expand 1
}
#---------------------------------------------------------------------

#================================================================================
#   section "spherical regime"
#================================================================================
#   
radiobutton $fsph.rad -text "spherical regime around site IQ_SEL" \
	-highlightthickness 0 -bg $BG4 -value "cr_sph_regime" -variable run_geometry_task \
	-command "set_run_geometry_task cr_sph_regime "

pack $fsph.rad

#------------------------------------------------------------------------
# NSHLCLU scale
set f $fsph.f1
frame $f -borderwidth 2 -height 30 -width 30 -bg $BG4
label $f.label -text "NSHLCLU" -bg $BG4 -width 5
scale $f.scale -from 0 -orient horizontal \
	    -sliderlength 40 -to 20 -variable NSHLCLU -bg $BG4 \
	    -highlightthickness 0 -command "reset_CLURAD "
pack $f.scale -anchor w -expand 1 -fill x -side right
pack $f.label -expand 1 -fill x -side right

proc reset_CLURAD {{x dummy}} {global CLURAD ; set CLURAD 0.0 }
    
    #---------------------------------------------------------------------
# CLURAD scale
set f $fsph.f2
frame $f -height 30 -width 30 -bg $BG4 ; # -borderwidth 2
label $f.label -text "CLURAD" -bg $BG4 -width 5
scale $f.scale -from 0 -orient horizontal -resolution 0.1 \
	    -sliderlength 40 -to 25.0 -variable CLURAD -bg $BG4 \
	    -highlightthickness 0 -command "reset_NSHLCLU "
pack $f.scale -anchor w -expand 1  -fill x -side right
pack $f.label -expand 1 -fill x -side right

proc reset_NSHLCLU {{x dummy}} {
	global NSHLCLU CLURAD
	if {$CLURAD==0.0} {
	    set NSHLCLU 2
	} else {
	    set NSHLCLU 0 
	}
}

#----------------------------------------------------------------------

if {$DIMENSION_HOST != "2D"} {set LATTICE_TYPE "homogeneous"}

# dynamically hide "NSHLCLU scale"
if {$LATTICE_TYPE != "heterogeneous"} { pack $fsph.f1  -fill x -anchor n }
pack $fsph.f2 -fill x -anchor n

if {$LATTICE_TYPE != "heterogeneous"} {
	if {$NSHLCLU==0 && $CLURAD==0.0} {set NSHLCLU 2}
} else {
	if {$CLURAD==0.0} {set CLURAD 1.5 ; $fsph.f2.scale set $CLURAD } ; # activate & cmd etc.
}

#-----------------------------------------------------------------------------------
# impurity type

set f [frame $fsph.f3 -bg $BG4]
radiobutton $f.rad1 -text "substitutional impurity" \
	    -highlightthickness 0 -bg $BG4 -value "substitutional" -variable impurity_type 

radiobutton $f.rad2 -text "impurity cluster" \
	    -highlightthickness 0 -bg $BG4 -value "cluster" -variable impurity_type

$f.rad2 select     ; # do not forget to make var impurity_type global
eval pack [winfo children $f] -side top -anchor w
pack $f -side top -pady 10

#--------------------------------------------------
checkbutton $fsph.box -text "show surrounding box" -background $BG4 \
	    -highlightthickness 0 -variable clu_box_show 
$fsph.box select
pack $fsph.box -side top

#------------------------------------------------------------------------------------
set f $fsph.f4
frame $f -bg $BG4
label $f.lab -text "with distance" -bg $BG4
scale $f.dist -orient horizontal -highlightthickness 0 -sliderlength 20 -bg $BG4 \
	    -resolution 0.1 -from 0 -to 2.0 -variable clu_box_dist   
$f.dist set 0.5
pack $f.lab $f.dist -side left -anchor w  -fill x -padx 5
pack $f -side top

#================================================================================
#   section "rectangular regime"
#================================================================================
#   
radiobutton $frec.rad -text "rectangular regime anchored at IQ_SEL" \
    -highlightthickness 0 -bg $BG3 -value "cr_rec_regime" -variable run_geometry_task \
	-command "set_run_geometry_task cr_rec_regime "

proc set_run_geometry_task {x} {
	global run_geometry_task
    set run_geometry_task $x
    puts "WWW set_run_geometry_task: $run_geometry_task   "
}

pack $frec.rad

$frec.rad invoke

#-------------------------------------------------------------
# anchor centered type
set f [frame $frec.f1 -bg $BG3 -borderwidth 5]

label $f.lab -text "anchor:  " -bg $BG3
radiobutton $f.rad1 -text "xy-centered" \
	    -highlightthickness 0 -bg $BG3 -value "xy-centered" -variable anchor_mode

radiobutton $f.rad2 -text "xyz-centered" \
	    -highlightthickness 0 -bg $BG3 -value "xyz-centered" -variable anchor_mode 

$f.rad2 select

eval pack [winfo children $f] -side left
pack $f

#------------------------------------------------------------------
# choose rectangular box LX/LY/LZ scales
set f [frame $frec.f2 -bg $BG3] 
pack $f -fill x -padx 10

# L_BOX(1..3) are named LX_BOX LY_BOX LZ_BOX in rasmolx module

label $f.lab -text "rectangular box" -bg $BG3
frame $f.fx -bg $BG3
label $f.fx.llx -text "LX: " -bg $BG3
scale $f.fx.slx -orient horizontal -from 1 -to 20 -resolution 0.2  \
            -variable L_BOX(1) -bg $BG3 -length 200 \
	    -highlightthickness 0
pack  $f.fx.llx $f.fx.slx -side left 

frame $f.fy -bg $BG3
label $f.fy.lly -text "LY: " -bg $BG3
scale $f.fy.sly -orient horizontal -from 1 -to 20 -resolution 0.2  \
            -variable L_BOX(2) -bg $BG3 -length 200 \
	    -highlightthickness 0
pack  $f.fy.lly $f.fy.sly -side left 

frame $f.fz -bg $BG3
label $f.fz.llz -text "LZ: " -bg $BG3
scale $f.fz.slz -orient horizontal -from 1 -to 20 -resolution 0.2  \
            -variable L_BOX(3) -bg $BG3 -length 200 \
	    -highlightthickness 0
pack  $f.fz.llz $f.fz.slz -side left

eval pack [winfo children $f] -side top -fill x

#---------------------------------------------------------------------
# info text and buttons
pack [frame $ACOL2.row3] -side top ; # -pady 10

#------------------------------------------------------------
# info line

pack [frame $ACOL2.row3.bckinfo -bg $BG5] -fill both
set f $ACOL2.row3.bckinfo.info
pack [frame $f -bg $BG5] -side top
label $f.info1 -bg $BG5 -text "sites generated:" 
label $f.info2 -bg $BG5 -textvariable NQBOX
label $f.info3 -bg $BG5 -text "          number of layers:"  
label $f.info4 -bg $BG5 -textvariable NLAYBOX
eval pack [winfo children $f] -side left -expand 1 -pady 5

# frames .info and .patience in $ACOL2.row2.bckinfo are used for patience_repack
global Wpatience0D
set Wpatience0D $ACOL2.row3.bckinfo

set f [frame $Wpatience0D.patience] ; # not yet packed
pack [label $f.patience] -fill both -ipady 5 

#--------------------------------------------------------------
# button line 

set f $ACOL2.row3.but
pack [frame $f] -side top -fill both -expand 1

#  $HEIGHT(BUT1)
button $f.setup -text "setup" -width 25 -height 2 -bg tomato  \
	-command " [list run_geometry $run_geometry_task] ; \
   puts \"*************** run_geometry with  $run_geometry_task ******************* \" "

button $f.show3d -text "Show 3D" -width 25 -height 2 -bg tomato1  \
   -command {

	switch $run_geometry_task {
	    cr_rec_regime -
	    default        {set Krasmol 2}	
	    
	    cr_sph_regime {set Krasmol 3}
	}

	patience_repack $Wpatience0D 1
	
	# 0's are dummies used only in other rasmoltasks

	run_visualizer $visualizer $Krasmol 0 0 0 \
		$L_BOX(1) $L_BOX(2) $L_BOX(3) \
		$IQCNTR $CLURAD $NSHLCLU \
		0 0 0 0 0 0 0 0 \
		$show_site_labels $show_unit_cell $show_cube $show_axis $show_empty_spheres \
		$sphere_color
	
	patience_repack $Wpatience0D 0
   } 

button $f.occupy -text "Occupy" -width 25 -height 2 -bg $COLOR(GREEN) \
	    -command { run_geometry $run_geometry_task
	               destroy $Wclua
                       create_system_0D_arb_clu }

eval pack [winfo children $f] -side left -expand yes ; # -pady 5

#-----------------------------------------------------------------
# display parameter frame
set f $ACOL2.row4
pack [frame $f] -side top -fill both

build_visualizer_parameter_controls $f narrow


##################    run_geometry $run_geometry_task
puts "******************* call >>  run_geometry $run_geometry_task << suppressed"

#
#---------------------------------------------------------------------------------------
#

#  label $ACOL2.bot -bg $BG2 -text " "
#  pack $ACOL2.bot -side bottom -fill y -expand 1 

#---------------------------------------------------------------------------------------
#
#label $ACOL3.bot -bg $BG3
#pack $ACOL3.bot -fill both -expand true -side bottom

#-------------------------------------------------- QUICK
##run_geometry $run_geometry_task
##	               destroy $Wclua
##                       create_system_0D_arb_clu
#-------------------------------------------------- QUICK
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
global QUICK QUICK_0D
if {$QUICK_0D==1} {
  puts "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQUICK 0D"
  run_geometry $run_geometry_task
  destroy $Wclua
  create_system_0D_arb_clu
}
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
    
   
}

