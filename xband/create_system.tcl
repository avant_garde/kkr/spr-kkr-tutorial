# setting up the structure
#
#
#   NQ       total number of lattice sites in unit cell (independent of occupation)
#   NCL      number of inequivalent lattice sites
#   NQCL     number of equivalent sites IQ for class ICL with ICL = 1,..,NCL  
#   NT       number of atom types 
#   NAT      number of equivalent sites IQ occupied by atom type IT with  IT = 1,..,NT
#   CONC     concentration of atom type IT (on a site)
#
#   for ordered compounds    NCL        == NT
#                            NQCL(ICL)  == NAT(IT)
#                            CONC(IT)   == 1
#
#
#
#
#
# Copyright (C) 2001, 2002 H. Ebert
#


proc create_system {} {

########################################################################################
########################################################################################
#
#                         CREATE AND UPDATE WINDOWS  -  MAIN MENUE
#
#  allow for 4 modes to set up the structure:
#
#         STRUCTURE_SETUP_MODE =    "STRUCTURE TABLE",      "SPACE GROUP" 
#                                   "PER PEDES"        or   "MULTI LAYERS"
#
#  mode 1 and 2 are dealt with by:   structure_via_table
#  mode 3 and 4 are dealt with by:   structure_per_pedes and structure_multilayer
#
########################################################################################


########################################################################################
########################################################################################
#               STRUCTURE_SETUP_MODE  = "STRUCTURE TABLE"  or  "SPACE GROUP" 
########################################################################################
########################################################################################


########################################################################################
#                                     enter structure via structure table or space group 
#
#  if CHECK_TABLE<> 0 the datafiles will be scanned and a check will be performed
#
proc structure_via_table { } {
    global xband_path .wstrst
    global STRUCTURE_SETUP_MODE
    global SPACEGROUPS_FILE STRUCTURE_FILE
    global COLOR WIDTH HEIGHT FONT
    
    global CHECK_TABLE CHECK_FIL_FS
    global SPACEGROUP SPACEGROUP_AP BOA COA LATPAR LATANG
    global NCL RCLU RCLV RCLW NQCL
    global BRAVAIS NQ RQX RQY RQZ NCL NQCL
    global RBASX RBASY RBASZ

    set PRC structure_via_table
   
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    if {$CHECK_TABLE==1} {
	set STRUCTURE_SETUP_MODE "SPACE GROUP"
	set nloop 274  
#       set nloop  20

	set chkfil [open "check_struc.inp" w]
	set CHECK_FIL_FS [open "check_findsym_1" w]
	puts $CHECK_FIL_FS "checking SPACE GROUPs"
	set CHECK_FIL_PP [open "check_per_pedes" w]
	puts $CHECK_FIL_PP "$nloop  entries tabulated "
    } else {
	set nloop 1 
    }
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

#=======================================================================================
    for {set iloop 1} {$iloop <= $nloop} {incr iloop} { 
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	if {$CHECK_TABLE!=0} {
	    set CHECK_FIL_FS [open "check_findsym_1" a]
	    debug $PRC " "
	    debug $PRC " "
	    debug $PRC " "
	    debug $PRC "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
	    debug $PRC "cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
	    debug $PRC " $STRUCTURE_SETUP_MODE                                     entry  $iloop"
	    reset_struc_parameter
	}
	
	if { $STRUCTURE_SETUP_MODE == "STRUCTURE TABLE" } {
	    set data_base "$xband_path/locals/$STRUCTURE_FILE"
	    set header    "Input via structure table"
	    set wintitle  "Structure set-up via structure table"
	} elseif { $STRUCTURE_SETUP_MODE == "SPACE GROUP" } {
	    set data_base "$xband_path/locals/$SPACEGROUPS_FILE"
	    set header    "Input via space group"
	    set wintitle  "Structure set-up via space group"
	} else {
	    give_warning "." "WARNING \n\n STRUCTURE_SETUP_MODE= \
		    $STRUCTURE_SETUP_MODE \n\n not implemented" 
	    return       
	}

	set flag 2

	reset_struc_parameter

#---------------------------------------------------------------------------------------
	set structure_list [list]

	set symdat [open "$data_base" r]
    
	while {[gets $symdat line] > -1} {
	    set kw0 [lindex $line 0]
	    set kwdum [string index $kw0 0]
	    
	    if { $kwdum == "*" } {
		gets $symdat line
		set line [string trim $line ] 
		
		if { $STRUCTURE_SETUP_MODE == "STRUCTURE TABLE" } {
		    if {[string length $line]>0} {
			lappend structure_list $line
		    }
		    
		} else {
		    set i  [format "%4i   " [lindex $line 0] ]
		    set s1 [lindex $line 1]
		    set s1 [string range "$s1                          " 0 18 ] 
		    gets $symdat line
		    set s2 [string range " [lindex $line 0]        " 0 9 ] 
		    lappend structure_list $i$s1$s2
		}
	    }
	}

	close $symdat
	
	if { $STRUCTURE_SETUP_MODE == "STRUCTURE TABLE" } {
	    set structure_list [lsort -index 2 $structure_list]
	}
	
	#---------------------------------------------------------------------------------------
	set win .wstrst
	toplevel_init $win $wintitle 0 0
	
	wm positionfrom $win user
	wm sizefrom $win ""
	wm minsize $win 100 100
	wm geometry $win  +400+300
	
	button $win.close -text "CLOSE" -width $WIDTH(BUT2) -height $HEIGHT(BUT2) \
		-bg $COLOR(CLOSE) -command "destroy $win" 
	pack $win.close -side bottom  
	
	set ws [frame $win.d]
	scrollbar $ws.sby -bg grey77 -command [list $ws.li yview] -orient vertical
	scrollbar $ws.sbx -bg grey77 -command [list $ws.li xview] -orient horizontal
	
	listbox $ws.li -bg $COLOR(LIGHTBLUE) -height 20 -width 35 -font $FONT(GEN) \
		-xscrollcommand [list $ws.sbx set] -yscrollcommand [list $ws.sby set]

	pack $ws     -side bottom -expand 1 -fill both
	pack $ws.sbx -side bottom -expand 0 -fill x
	pack $ws.sby -side left   -expand 0 -fill y
	pack $ws.li  -side right  -expand 1 -fill both 
	
	bind $ws.li <Button-1> {
	    structure_via_table_search [Selektion %W %y]
	    destroy .wstrst
	    Bend
	}
	bind $ws.li <Double-Button-1> {Bend}
	
	foreach x $structure_list { 
	    $ws.li insert end "$x"
	}
	#---------------------------------------------------------------------------------------

#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	if {$CHECK_TABLE!=0} {
	    structure_via_table_search  [lindex $structure_list [expr $iloop-1]]
	    
	    set str_key [expr 10000 + $SPACEGROUP_AP]
	    set aux [format "%12.8f %12.8f %12.8f %12.8f  %12.8f " \
		    $BOA $COA  $LATANG(1) $LATANG(2) $LATANG(3) ]
	    
	    puts $chkfil "---------------------------------------------------------------------"
	    puts $chkfil "$str_key    SPACEGROUP_AP   iloop: $iloop   SPACEGROUP: $SPACEGROUP      "
	    puts $chkfil "$aux"
	    puts $chkfil "$NCL     NCL "
	    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
		set coord [format "%12.8f %12.8f %12.8f" $RCLU($ICL) $RCLV($ICL) $RCLW($ICL) ]
		puts $chkfil "$coord          $ICL    NQCL: $NQCL($ICL)  tauX  tauY  tauZ   "
	    }
	    puts $chkfil "0 0 0 mag"
	    debug $PRC "-------------------------------------------------------------------"
	    debug $PRC  "$str_key    SPACEGROUP_AP   iloop: $iloop   SPACEGROUP: $SPACEGROUP      "
	    debug $PRC  "$aux"
	    debug $PRC  "$NCL     NCL "
	    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
		set coord [format "%12.8f %12.8f %12.8f" $RCLU($ICL) $RCLV($ICL) $RCLW($ICL) ]
		debug $PRC  "$coord          $ICL $NQCL($ICL)  tauX  tauY  tauZ   "
	    }
	    
	    
	    puts $CHECK_FIL_FS " "
	    puts $CHECK_FIL_FS "****************************************************************"
	    puts $CHECK_FIL_FS "iloop: $iloop    SPACEGROUP: $SPACEGROUP  SPACEGROUP_AP: $SPACEGROUP_AP"
	    close $CHECK_FIL_FS
	    
	    run_findsym structure_via_table_run_findsym
	    
	    
	    puts $CHECK_FIL_PP " "
	    puts $CHECK_FIL_PP "****************************************************************"
	    puts $CHECK_FIL_PP "iloop: $iloop    SPACEGROUP: $SPACEGROUP  SPACEGROUP_AP: $SPACEGROUP_AP"
	    puts $CHECK_FIL_PP "$BRAVAIS $NCL $NQ     BRAVAIS NCL NQ" 
	    for {set i 1} {$i <= 3} {incr i} {
		puts $CHECK_FIL_PP [format "%12.8f %12.8f %12.8f " $RBASX($i) $RBASY($i) $RBASZ($i) ]
	    }
	    puts $CHECK_FIL_PP [format "%12.8f %12.8f %12.8f" $LATPAR(1) $LATPAR(2) $LATPAR(3)]
	    puts $CHECK_FIL_PP [format "%12.8f %12.8f %12.8f" $LATANG(1) $LATANG(2) $LATANG(3)]
	    
	    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
		puts $CHECK_FIL_PP [format "%4i"  $NQCL($ICL) ]
	    }
	    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
		puts $CHECK_FIL_PP [format "%12.8f %12.8f %12.8f" $RQX($IQ) $RQY($IQ) $RQZ($IQ) ]
	    }
	    
	    
	}
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    }
#========================================================================================

} 
#                                                                structure_via_table END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

########################################################################################
#                                                             structure_via_table_search
proc structure_via_table_search {auswahl} {

    set PRC "structure_via_table_search"
    
    global schoenflies SPACEGROUP SPACEGROUP_AP international .wstrst
    global positions NQCL WYCKOFF_SYMBOL POINT_GROUP equicoord  
    global BRAVAIS TABLATANG_alpha TABLATANG_beta TABLATANG_gamma
    global LATANG xband_path
    global STRUCTURE_SETUP_MODE STRUCTURE_TYPE
    global SPACEGROUPS_FILE STRUCTURE_FILE
    global CHECK_TABLE
    set tcl_precision 17
    
    set found "false"
    
    debug $PRC "auswahl  $auswahl"
    
    if { $STRUCTURE_SETUP_MODE == "STRUCTURE TABLE" } {

	set symdat [open "$xband_path/locals/$STRUCTURE_FILE" r]

	set key_pos 2
	set keyword [string trim $auswahl]   ; # take whole line, no real keyword exists
	#set keyword [lindex $keyword $key_pos]
	set STRUCTURE_TYPE $auswahl
	
    } elseif { $STRUCTURE_SETUP_MODE == "SPACE GROUP" } {
	
	set SPACEGROUP [lindex $auswahl 0]
	
	if {$SPACEGROUP > 230||$SPACEGROUP < 1} {
	    give_warning "." "WARNING \n\n There are only 230 space groups \n\n \
		    No.  $SPACEGROUP  not allowed \n\n Change Space Group No. ! " 
	    return
	}

	set symdat [open "$xband_path/locals/$SPACEGROUPS_FILE" r]
	set key_pos 1
	set keyword [lindex $auswahl $key_pos]
	set STRUCTURE_TYPE UNKNOWN
	
    } else {
	give_warning "." "WARNING \n\n STRUCTURE_SETUP_MODE= $STRUCTURE_SETUP_MODE \n \
		\n not implemented  " 
	return       
    }

    debug $PRC "keyword:  $keyword"
 
    while {[gets $symdat line] > -1 && $found == "false"} {
	set kw0   [lindex $line 0]
	set kwdum [string index $kw0 0]
	
	if { $kwdum == "*" } {
	    gets $symdat line

	    if {$STRUCTURE_SETUP_MODE == "STRUCTURE TABLE"} {
		set checkstring [string trim $line] ; # take whole line, no keyword exists
	    } else {
		set checkstring [lindex $line $key_pos]
	    }
	    
	    if {$checkstring == $keyword} {
                if { $STRUCTURE_SETUP_MODE == "STRUCTURE TABLE" } {gets $symdat line}
    		set SPACEGROUP    [lindex $line 0]
		set international [lindex $line 1]
        	gets $symdat schoenflies
                set schoenflies [string trim $schoenflies]
        	gets $symdat SPACEGROUP_AP
		
		gets $symdat line
		set positions [lindex $line 0]
		for {set i 1} {$i <= $positions} {incr i} {
                    gets $symdat line
		    set WYCKOFF_SYMBOL($i)     [lindex $line 0]
		    set equicoord($i,1) [lindex $line 1]
		    set equicoord($i,2) [lindex $line 2]
		    set equicoord($i,3) [lindex $line 3]
		    set NQCL($i)        [lindex $line 4]
		    set POINT_GROUP($i) [lindex $line 5]
		}
		
                set tmp [string index $international 0]
                if {$SPACEGROUP <= 2} then {
		    set BRAVAIS 1
                } else {
		    if {$SPACEGROUP <= 15} then {
			if { $tmp == "P" } then {
			    set BRAVAIS 2
			} else {
			    set BRAVAIS 3
			}
		    } else {
			if {$SPACEGROUP <= 74} then {
			    switch $tmp {
				{P} {set BRAVAIS 4}
				{I} {set BRAVAIS 6}
				{F} {set BRAVAIS 7}
				default {set BRAVAIS 5}
			    }
			} else {
			    if {$SPACEGROUP <= 142} then {
				if { $tmp == "P" } then {
				    set BRAVAIS 8
				} else {
				    set BRAVAIS 9
				}
			    } else {
				if {$SPACEGROUP <= 167} then {
				    set BRAVAIS 10
				} else {
				    if {$SPACEGROUP <= 194} then {
					set BRAVAIS 11
				    } else {
					switch $tmp {
					    {P} {set BRAVAIS 12}
					    {F} {set BRAVAIS 13}
					    {I} {set BRAVAIS 14}
					}
				    }
				}
			    }
			}
		    }
                }
                set LATANG(1) $TABLATANG_alpha($BRAVAIS)
                set LATANG(2) $TABLATANG_beta($BRAVAIS)
                set LATANG(3) $TABLATANG_gamma($BRAVAIS)
		
		if {$STRUCTURE_SETUP_MODE == "STRUCTURE TABLE"} {
		    # now non unique keyword is ok, and single word required
		    set keyword [lindex $keyword $key_pos]
		}
		
          	structure_set_lattice_parameter $keyword
		
		set found "true"
	    }
	}
    }
    
    if {$CHECK_TABLE==0} {structure_window}
    
}
#                                                         structure_via_table_search END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                               structure_via_table_show
proc structure_via_table_show {struc} {

    set PRC "structure_via_table_show"

    set tcl_precision 17

    global SPACEGROUP SPACEGROUP_AP 
    global schoenflies international ALAT  LATPAR
    global positions NQCL WYCKOFF_SYMBOL POINT_GROUP WYCKOFFCL
    global equicoord NQ NAP
    global NCL Wcsys NCLOK RCLU RCLV RCLW xband_path STRUCTURE_SETUP_MODE
    global CHECK_TABLE
    global COLOR WIDTH HEIGHT FONT

    set ALAT $LATPAR(1)
    set NQ   0
    set NCL 0

    toplevel_init .sg " " 0 0 
    wm positionfrom .sg user
    wm sizefrom .sg ""
    wm minsize .sg 100 100

    if { $STRUCTURE_SETUP_MODE  == "STRUCTURE TABLE" } {
       wm title .sg "set up via Structure Type"
       set header "structure type:  $struc"
       set advice "supply required information"
    } elseif { $STRUCTURE_SETUP_MODE == "SPACE GROUP" } {
       wm title .sg "set up via Space Group"
       set header " "
       set advice "select desired sites"
    } else {
         give_warning "." "WARNING \n\n STRUCTURE_SETUP_MODE= $STRUCTURE_SETUP_MODE \n \
                          \nnot implemented  " 
         return       
    }
    wm geometry .sg  +400+300

    frame .sg.line1
    frame .sg.line2
    frame .sg.line3
    frame .sg.line4
    frame .sg.ok
    set  top "sg"
    button .sg.ok.goon   -text "all done --- GO ON"  -height 3 -bg $COLOR(DONE) \
             -command "destroy .$top ;  run_findsym structure_window"
    label .sg.line0        -font $FONT(GEN) -text "$header"
    label .sg.line1.label  -font $FONT(GEN) -text "Spacegroup:  $SPACEGROUP"
    label .sg.line1.label1 -font $FONT(GEN) -text "Schoenflies: $schoenflies"
    label .sg.line1.label2 -font $FONT(GEN) -text "International: $international"
    label .sg.line2.label  -text "$advice"
    label .sg.line2.label1 -text "occupied sites"
    label .sg.line2.label2 -textvariable {NQ} -font $FONT(GEN) -width {3}\
                           -fg $COLOR(ENTRYFG)
    set lt [format "No. of pos. WYCKOFF  symmetry        coordinates      " ]
    label .sg.line3.label -text $lt -font $FONT(GEN)

    for {set IP 1} {$IP <=$positions} {incr IP} {
       set NAP($IP) $NQCL($IP)
    }
    set NP   $positions
    set NQ   0
    set NCLOK 0 

    if { $STRUCTURE_SETUP_MODE  == "STRUCTURE TABLE" } {
       .sg.ok.goon configure -state disabled
       set NCL   $positions
    } else {
       set NCL 0
    }

    for {set IP 1} {$IP <= $positions} {incr IP} {

        set mark($IP) $IP
        if { $STRUCTURE_SETUP_MODE  == "STRUCTURE TABLE" } { set NQ [expr "$NQ + $NAP($IP)"] }

        set  lt [format "    %3s         %1s     %6s   %6s   %6s   %6s " \
                $NAP($IP) $WYCKOFF_SYMBOL($IP) $POINT_GROUP($IP)  \
 		$equicoord($IP,1) $equicoord($IP,2) $equicoord($IP,3)]

     	frame  .sg.line4.button$IP
     	button .sg.line4.button$IP.but -font $FONT(GEN) -text $lt \
		-command " structure_via_table_get_coordinates $IP $NP $NAP($IP)"
        label  .sg.line4.button$IP.ok -bitmap @$xband_path/nil.bmp

    	pack .sg.line4.button$IP 
    	pack .sg.line4.button$IP.but .sg.line4.button$IP.ok -side left
 
        set xyz no
        for {set k 1} {$k <=3} {incr k} {
	   if {[lsearch -regexp $equicoord($IP,$k) x ] > -1} {set xyz "yes"}   
	   if {[lsearch -regexp $equicoord($IP,$k) y ] > -1} {set xyz "yes"}
	   if {[lsearch -regexp $equicoord($IP,$k) z ] > -1} {set xyz "yes"}
        }

#--------------------------------------------- nothing to do -- all coordinates fixed !!
        if {$xyz=="no" && $STRUCTURE_SETUP_MODE  == "STRUCTURE TABLE"} {
           incr NCLOK
           if {$NCLOK==$NCL} {.sg.ok.goon configure -state normal}
           .sg.line4.button$IP.but configure -state disabled
           .sg.line4.button$IP.ok  configure -bitmap @$xband_path/ok.bmp
           set RCLU($IP) [coord_str_to_num $equicoord($IP,1) ]
           set RCLV($IP) [coord_str_to_num $equicoord($IP,2) ]
           set RCLW($IP) [coord_str_to_num $equicoord($IP,3) ]
           set WYCKOFFCL($IP) $WYCKOFF_SYMBOL($IP)
           debug $PRC "IPOS:  $IP  $NAP($IP) $WYCKOFFCL($IP)  u: $RCLU($IP) v: $RCLV($IP) w: $RCLW($IP) "
        }
#--------------------------------------------- nothing to do -- all coordinates fixed !!

    }
#------------------------------------------------------------------- END loop over IP

    pack .sg.line1.label .sg.line1.label1 .sg.line1.label2 \
                               -side left -expand yes -fill both
    pack .sg.line2.label .sg.line2.label1 .sg.line2.label2 -side left -expand yes 
    pack .sg.line3.label 
    pack .sg.ok.goon
    pack .sg.line0 .sg.line1 .sg.line2 .sg.line3 .sg.line4 .sg.ok -fill both

#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
#                   for checking:   set dummy values for parameters not fixed by default 
#
if {$CHECK_TABLE!=0} {
    for {set IP 1} {$IP <= $positions} {incr IP} {
         structure_via_table_get_coordinates $IP $NP $NAP($IP)
    }
}
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


}
#                                                           structure_via_table_show END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


########################################################################################
#                                                    structure_via_table_get_coordinates
#
proc structure_via_table_get_coordinates {IP NP NAP_IP} {

global equicoord NQ NQCL get mark WYCKOFF_SYMBOL Wcsys WYCKOFFCL

global inscoo RCLU RCLV RCLW NCLOK NCL NQ NQCL xband_path
global STRUCTURE_SETUP_MODE
global COLOR WIDTH HEIGHT FONT
global CHECK_TABLE

set PRC "structure_via_table_get_coordinates"

set tcl_precision 17

set get(1) "false"
set get(2) "false"
set get(3) "false"
set xyz   0
for {set i 1} {$i <=3} {incr i} {
  if {[lsearch -regexp $equicoord($IP,$i) x ] > -1} {set get(1) "true"; set xyz 1}
  if {[lsearch -regexp $equicoord($IP,$i) y ] > -1} {set get(2) "true"; set xyz 1}
  if {[lsearch -regexp $equicoord($IP,$i) z ] > -1} {set get(3) "true"; set xyz 1}
}

.sg.line4.button$IP.ok  configure -bitmap @$xband_path/ok.bmp
if { $STRUCTURE_SETUP_MODE  == "STRUCTURE TABLE" } {
   incr NCLOK
   if {$NCLOK==$NCL} {.sg.ok.goon configure -state normal} 
   set ICL $IP
} elseif { $STRUCTURE_SETUP_MODE == "SPACE GROUP" } {
   set NQ [expr "$NQ + $NAP_IP"]
   incr NCL
   set ICL $NCL
   set NQCL($NCL) $NAP_IP
   debug $PRC "SG-MODE: NQ $NQ NCL $NCL ICL $ICL NQCL $NQCL($NCL) NP $NP IP $IP NAP(IP) $NAP_IP "
   if {$xyz == "0"} {
       set RCLU($NCL) [coord_str_to_num $equicoord($IP,1) ]
       set RCLV($NCL) [coord_str_to_num $equicoord($IP,2) ]
       set RCLW($NCL) [coord_str_to_num $equicoord($IP,3) ]
       set WYCKOFFCL($NCL) $WYCKOFF_SYMBOL($IP)
   }
} else {
   give_warning "." "WARNING \n\n STRUCTURE_SETUP_MODE= $STRUCTURE_SETUP_MODE \n\n \
                    not implemented  " 
   return       
}

    if {$xyz == "1"} {

       set win .sg.top
       toplevel_init $win "Input: x y z" 0 0
       wm positionfrom $win user
       wm sizefrom $win ""
#      wm minsize  $win 400 400
       wm geometry $win  +400+250
#       wm geometry $win  +4+3       
       
       frame  $win.line1
       frame  $win.ok
       button $win.ok.ok -text "OK"  -width 15 -height 3 -bg $COLOR(GOON) \
                                  -command "structure_via_table_get_xyz $win $IP $ICL"
       set  top "sg.top"
       button $win.ok.cancel -text "Cancel"  -width 15 -height 3 -bg $COLOR(CLOSE) \
                                  -command "destroy $win"
      
       label $win.line1.label1 -text "x: "
       label $win.line1.label2 -text "y: "
       label $win.line1.label3 -text "z: "
       entry $win.line1.entry1 -width 12 -relief sunken -font $FONT(TAB) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
       entry $win.line1.entry2 -width 12 -relief sunken -font $FONT(TAB) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
       entry $win.line1.entry3 -width 12 -relief sunken -font $FONT(TAB) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
       for {set i 1} {$i <= 3} {incr i} {
           if {$get($i) == "true"} {
        	pack $win.line1.label$i $win.line1.entry$i -side left
           }
       }
       pack $win.ok.ok $win.ok.cancel -side left -fill both -expand yes
       pack $win.line1 $win.ok  -fill both


#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       if {$CHECK_TABLE!=0} {
          for {set i 1} {$i <= 3} {incr i} {
             if {$get($i) == "true"} {

                if { $i==1} {
                   $win.line1.entry$i insert 0 "0.1357"
                } elseif { $i==2} {
                   $win.line1.entry$i insert 0 "0.2468"
                } else { 		
                   $win.line1.entry$i insert 0 "0.3579"
                }  
             }
          }
          structure_via_table_get_xyz $win $IP $ICL
       }
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


    }

}
#                                                structure_via_table_get_coordinates END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
proc structure_via_table_get_xyz {w IP ICL} {

    global mark get inscoo equicoord Wcsys
    global RCLU RCLV RCLW  WYCKOFFCL WYCKOFF_SYMBOL

    set tcl_precision 17

    set x1 "x"
    set x2 "y"
    set x3 "z"

    for {set i 1} {$i <=3} {incr i} {
	set R($i) $equicoord($IP,$i)
    }	
  
    for {set i 1} {$i <=3} {incr i} {
	if {$get($i) == "true"} {
	    set x$i " [.sg.top.line1.entry$i get] "
	}
    }	

    for {set i 1} {$i <=3} {incr i} {
 	regsub 2x $R($i) "x*2" tmp0
 	regsub x  $tmp0   $x1  tmp1
	regsub y  $tmp1   $x2  tmp2
	regsub z  $tmp2   $x3  R($i)
	if {[string trim $R($i)] == ""} {set R($i) "0.0" }
    }

    for {set i 1} {$i <=3} {incr i} {
	set R($i) [coord_str_to_num $R($i) ]
    }	


    for {set i 1} {$i <=3} {incr i} {
	if { $R($i) > 1 } { set R($i) [expr $R($i) - int($R($i))]}
	if { $R($i) < 0 } { set R($i) [expr $R($i) - int($R($i))]}
    }	

    set V1 [format "ICL %3i  cryst. coord.:" $ICL]
    set V2 [format "  u: %14.8f v: %14.8f w: %14.8f" $R(1) $R(2) $R(3)]
    writescr .d.tt "INFO from <structure_via_table_get_xyz>   ------------------ \n"
    writescr .d.tt "$V1 $V2 \n" 

    set RCLU($ICL) $R(1)
    set RCLV($ICL) $R(2)
    set RCLW($ICL) $R(3)
    set WYCKOFFCL($ICL) $WYCKOFF_SYMBOL($IP)

    destroy $w 
}
#                                                        structure_via_table_get_xyz END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<





########################################################################################
########################################################################################
#               STRUCTURE_SETUP_MODE =  "PER PEDES",  "MULTI LAYERS" or "3D SURFACE"
########################################################################################
########################################################################################



########################################################################################
#                         STRUCTURE INPUT PER PEDES
########################################################################################
#                                                              enter structure per pedes
proc structure_per_pedes { } {
    global BRAVAIS NQ LATPAR LATANG flag Wcsys
    global TABBRAVAIS  W_sites_list
    global COLOR WIDTH HEIGHT FONT
    global CHECK_TABLE CHECK_FIL_PP CHECK_FIL_FS 
    global STRUCTURE_SETUP_MODE LATPAR LATANG 
    global CHECK_NCL CHECK_NQ CHECK_LATPAR CHECK_LATANG CHECK_NQCL
    global CHECK_RQX CHECK_RQY CHECK_RQZ
    global CHECK_RBASX CHECK_RBASY CHECK_RBASZ
    global RBASX RBASY RBASZ NCL NQCL
    global international

    set PRC structure_per_pedes
    set flag 3 
    set win .w_spp
    toplevel_init $win "Structure set-up per pedes" 0 0
    wm positionfrom $win user
    wm sizefrom $win ""
    wm minsize  $win 100 100
    set nb 14
    
    set tcl_precision 17

    reset_struc_parameter

    if {[winfo exists $W_sites_list]} {$W_sites_list delete 0 end}

    frame $win.bravais  -relief raised -borderwidth 2
    frame $win.bravais.bravais -relief raised -borderwidth 2
    frame $win.bravais.type -relief raised -borderwidth 2
    frame $win.bravais.site -relief raised -borderwidth 2
    frame $win.bravais.ok
    button $win.bravais.ok.ok -text "input OK --- GO ON"  \
           -width 25 -height $HEIGHT(BUT2) -bg $COLOR(GOON) \
           -command "structure_per_pedes_GOON  $win"

#-------------------------------------------------------------
    proc structure_per_pedes_GOON {w} {
    global BRAVAIS LATANG  TABLATANG_alpha TABLATANG_beta TABLATANG_gamma

    set B [string trim $BRAVAIS]
    if {$B==""} { 
        set ierr 1 
    } else {
	if {$B<0 || $B>14} {
           set ierr 1
        } else { 
           set ierr 0
        }  
    }
    if {$ierr=="1"} { 
       give_warning "." "WARNING \n\n  specify BRAVAIS LATTICE first  " 
       return       
    }

    set a $TABLATANG_alpha($BRAVAIS) ; if { $a!=0 && $a!="" } {set LATANG(1) $a}
    set b $TABLATANG_beta($BRAVAIS)  ; if { $b!=0 && $b!="" } {set LATANG(2) $b}
    set g $TABLATANG_gamma($BRAVAIS) ; if { $g!=0 && $g!="" } {set LATANG(3) $g}
    
    structure_set_lattice_parameter per_pedes

    if {[winfo exists $w] == 1} {destroy $w}

    }
#-------------------------------------------------------------

    button $win.bravais.ok.cancel -text "CLOSE"  \
           -width 25 -height $HEIGHT(BUT2) -bg $COLOR(CLOSE) \
           -command "destroy $win" 
    label  $win.bravais.bravais.title -text "Bravais lattice"
    label  $win.bravais.site.label -text "Number of sites  NQ: "

    scale $win.bravais.site.scale -from {1} -orient {horizontal} -length 200  \
    -sliderlength {20} -to {200} -resolution 1 -variable {NQ} \
    -highlightthickness 0 
    pack $win.bravais.bravais.title

    for {set i 0} {$i < 15} {incr i} {
	radiobutton $win.bravais.type.radio($i) -relief flat -font $FONT(GEN)\
		-variable BRAVAIS -value $i -text "$TABBRAVAIS($i)" 
	pack $win.bravais.type.radio($i) -anchor w -expand yes
    }
    pack $win.bravais.site.label $win.bravais.site.scale -side left
    pack $win.bravais.ok.ok  $win.bravais.ok.cancel -side left -expand yes
    pack $win.bravais.bravais $win.bravais.type $win.bravais.site\
	    $win.bravais.ok  -fill both -expand yes
    pack $win.bravais -expand yes




#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
if {$CHECK_TABLE==2} {
   set CHECK_FIL_FS [open "check_findsym_2" w]
   puts $CHECK_FIL_FS "checking PER PEDES input"
   set CHECK_FIL_PP [open "check_per_pedes" r]
   gets $CHECK_FIL_PP line
   set nloop [lindex $line 0]
   debug $PRC " nuber of entries in file check_per_pedes:  $nloop"

   for {set iloop 1} {$iloop <= $nloop} {incr iloop} { 

      reset_struc_parameter
      set STRUCTURE_SETUP_MODE "PER PEDES" 
      set international " "
 
      gets $CHECK_FIL_PP line
      gets $CHECK_FIL_PP line
      gets $CHECK_FIL_PP head_line ;#iloop: $iloop    SPACEGROUP: $SPACEGROUP  SPACEGROUP_AP: $SPACEGROUP_AP"
      debug $PRC " "
      debug $PRC " "
      debug $PRC " "
      debug $PRC "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
      debug $PRC "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
      debug $PRC $head_line
      gets $CHECK_FIL_PP line
      set CHECK_BRAVAIS [lindex $line 0]
      set CHECK_NCL     [lindex $line 1]
      set CHECK_NQ      [lindex $line 2]
      for {set i 1} {$i <= 3} {incr i} {
         gets $CHECK_FIL_PP line
         set CHECK_RBASX($i) [lindex $line 0]
         set CHECK_RBASY($i) [lindex $line 1]
         set CHECK_RBASZ($i) [lindex $line 2]
      }
      gets $CHECK_FIL_PP line
      set CHECK_LATPAR(1) [lindex $line 0]
      set CHECK_LATPAR(2) [lindex $line 1]
      set CHECK_LATPAR(3) [lindex $line 2]
      gets $CHECK_FIL_PP line
      set CHECK_LATANG(1) [lindex $line 0]
      set CHECK_LATANG(2) [lindex $line 1]
      set CHECK_LATANG(3) [lindex $line 2]
      debug $PRC "iloop: $iloop       NCL: $CHECK_NCL   NQ: $CHECK_NQ"
      for {set ICL 1} {$ICL <= $CHECK_NCL} {incr ICL} {
         gets $CHECK_FIL_PP line
         set CHECK_NQCL($ICL) [lindex $line 0]
      }
      for {set IQ 1} {$IQ <= $CHECK_NQ} {incr IQ} {
         gets $CHECK_FIL_PP line
         set CHECK_RQX($IQ) [lindex $line 0]
         set CHECK_RQY($IQ) [lindex $line 1]
         set CHECK_RQZ($IQ) [lindex $line 2]
      }

      set BRAVAIS $CHECK_BRAVAIS
      set NQ      $CHECK_NQ


      structure_per_pedes_GOON  .DUMMY
  

      debug $PRC "VGL  "
      debug $PRC "VGL -------------------------------------------------------"
      debug $PRC "VGL $head_line"
      debug $PRC "VGL BRAVAIS $BRAVAIS"
      if {[expr abs($CHECK_NCL-$NCL)]>0} {
         set FLAGNCL 1
      } else {
         set FLAGNCL 0
      } 
      debug $PRC "VGL NCL:   $CHECK_NCL $NCL"
      set TXT " "
      set NQ1 0 
      for {set ICL 1} {$ICL <= $CHECK_NCL} {incr ICL} { 
         append TXT "[format %3i $CHECK_NQCL($ICL)]"
      set NQ1 [expr $NQ1 + $CHECK_NQCL($ICL)] 
      }
      debug $PRC "VGL NQCL (in): $TXT"
      set TXT " "
      set NQ2 0 
      for {set ICL 1} {$ICL <= $NCL} {incr ICL} {  
         append TXT "[format %3i $NQCL($ICL)]"
         set NQ2 [expr $NQ2 + $NQCL($ICL)] 
      }
      debug $PRC "VGL NQCL     : $TXT"
      debug $PRC "VGL NQ:    $CHECK_NQ $NQ1 $NQ2"
      if { $FLAGNCL==1} {
         set FLAGNQCL 1
      } else {
         set FLAGNQCL 0
         for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
	     if   {[expr abs($NQCL($ICL)-$CHECK_NQCL($ICL))]>0} { set FLAGNQCL 1 }
         }          
      }   
      set FLAGRBAS 0
      for {set i 1} {$i <= 3} {incr i} {
         debug $PRC [format "VGL A%1i (in)  : %12.8f %12.8f %12.8f " $i $CHECK_RBASX($i) $CHECK_RBASY($i) $CHECK_RBASZ($i) ]
         debug $PRC [format "VGL A%1i       : %12.8f %12.8f %12.8f " $i $RBASX($i) $RBASY($i) $RBASZ($i) ]
         debug $PRC "VGL  "
         if   {[expr abs($RBASX($i)-$CHECK_RBASX($i))]>0.000001} { set FLAGRBAS 1 }
         if   {[expr abs($RBASY($i)-$CHECK_RBASY($i))]>0.000001} { set FLAGRBAS 1 }
         if   {[expr abs($RBASZ($i)-$CHECK_RBASZ($i))]>0.000001} { set FLAGRBAS 1 }
      }
      set FLAGLPAR 0
      set FLAGLANG 0
      debug $PRC [format "VGL LPAR (in): %12.8f %12.8f %12.8f " $CHECK_LATPAR(1) $CHECK_LATPAR(2) $CHECK_LATPAR(3) ]
      debug $PRC [format "VGL LPAR     : %12.8f %12.8f %12.8f " $LATPAR(1) $LATPAR(2) $LATPAR(3) ]
      debug $PRC "VGL  "
      debug $PRC [format "VGL LANG (in): %12.8f %12.8f %12.8f " $CHECK_LATANG(1) $CHECK_LATANG(2) $CHECK_LATANG(3) ]
      debug $PRC [format "VGL LANG     : %12.8f %12.8f %12.8f " $LATANG(1) $LATANG(2) $LATANG(3) ]
      debug $PRC "VGL  "
      for {set i 1} {$i <= 3} {incr i} {
         if   {[expr abs($CHECK_LATPAR($i)-$LATPAR($i))]>0.000001} { set FLAGLPAR 1 }
         if   {[expr abs($CHECK_LATANG($i)-$LATANG($i))]>0.000001} { set FLAGLANG 1 }
      }

      set ierr 0
      if { $CHECK_NQ!=$NQ1 || $CHECK_NQ!=$NQ2 } {
                           incr ierr; debug $PRC "VGL   NQ    differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!!"}
      if { $FLAGNCL ==1} { incr ierr; debug $PRC "VGL   NCL   differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!!"}
      if { $FLAGNQCL==1} { incr ierr; debug $PRC "VGL   NQCL  differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!!"}
      if { $FLAGRBAS==1} { incr ierr; debug $PRC "VGL   RBAS  differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!! BRAVAIS $BRAVAIS"}
      if { $FLAGLPAR==1} { incr ierr; debug $PRC "VGL   LPAR  differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!!"}
      if { $FLAGLANG==1} { incr ierr; debug $PRC "VGL   LANG  differ  !!!!!!!!!!!!!!!!!!!!!!!!!!!!"}
      if { $FLAGNQCL ==1 || $FLAGNCL ==1} {
	  if { $FLAGRBAS==0} { debug $PRC "VGL   RBAS OK but trouble *********************************"}
      }
      if {$ierr>0} {debug $PRC "VGL   $head_line" }
   }
##   exit
}
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


}
#                                                                structure_per_pedes END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


########################################################################################
#                                                         structure_per_pedes_read_coord
proc structure_per_pedes_read_coord {key} {

global RQX RQY RQZ  Wpprc
global NQ BRAVAIS
global TABLATANG_alpha TABLATANG_beta TABLATANG_gamma
global TABLATPAR_a TABLATPAR_b TABLATPAR_c LATANG LATPAR
global NT NAT NOQ ITOQ NCL NQCL IQ1Q IQ2Q IQECL
global RCLU RCLV RCLW WYCKOFFCL WYCKOFFQ STRUCTURE_SETUP_MODE
global SPACEGROUP SPACEGROUP_AP STRUCTURE_TYPE  per_pedes_occupation
global COLOR WIDTH HEIGHT FONT
global RBASX RBASY RBASZ BOA COA ALAT 
global TABLABX TABLABY TABLABZ
global per_pedes_input_mode set per_pedes_RBASX per_pedes_RBASY per_pedes_RBASZ

global NLQ ZT RWS TXTT0 CONC
global CHECK_TABLE
global CHECK_NCL CHECK_NQ CHECK_LATPAR CHECK_LATANG CHECK_NQCL
global CHECK_RQX CHECK_RQY CHECK_RQZ
global NQ_SU1 NQ_SU2 SU_IQ
global LATTICE_TYPE ZRANGE_TYPE
global NQ_bulk_L NQ_bulk_R NLAY_bulk_L NLAY_bulk_R 
global BGSU
global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
global PRC

    set PRC structure_per_pedes_read_coord
    
    set tcl_precision 17

    if { $BRAVAIS<0 || $BRAVAIS>14 } {give_warning "." \
         "WARNING \n\n specify Bravais lattice first \n\n " ; return}

debug $PRC "******************* STRUCTURE_SETUP_MODE = $STRUCTURE_SETUP_MODE "

#========================================================================= MULTI LAYERS
if { $STRUCTURE_SETUP_MODE != "PER PEDES" } {

   set per_pedes_input_mode  "cartesian" 

#========================================================================= PER PEDES
} else {
#
#------------------------------ standard setting that eventually will be overwritten 
#
    if {$key=="use_primitive_vectors" } {
       for {set i 1} {$i <= 3} {incr i} {
	  set LATPAR($i)  1
	  set LATANG($i) 90
       }
       set BOA 1
       set COA 1
    }

    set ALAT $LATPAR(1)

    set RBASX(1) 1.0 ; set RBASY(1) 0.0  ; set RBASZ(1) 0.0
    set RBASX(2) 0.0 ; set RBASY(2) $BOA ; set RBASZ(2) 0.0
    set RBASX(3) 0.0 ; set RBASY(3) 0.0  ; set RBASZ(3) $COA

    set PI 3.141592653589793238462643

    set cosa [expr cos($LATANG(1)*$PI/180.0) ]
    set sinahalf [expr sin($LATANG(1)*$PI/90.0) ]
    set cosb [expr cos($LATANG(2)*$PI/180.0) ]
    set cosg [expr cos($LATANG(3)*$PI/180.0) ]
    set sing [expr sin($LATANG(3)*$PI/180.0) ]

    switch $BRAVAIS {
        {0}  {# unknown
             set RBASX(2) [expr $BOA * $cosg]
             set RBASY(2) [expr $BOA * $sing]
             set RBASZ(2) 0.0   
           
             set RBASX(3) [expr $COA * ($cosb-$cosa*$cosg) / $sing]
             set RBASY(3) [expr -$COA * $cosa ]
             set RBASZ(3) [expr pow([expr pow($COA,2) -pow($RBASX(3),2) -pow($RBASY(3),2)],0.5)]
             }
        {1}  {# triclinic   primitive      -1     C_i
	    set RBASX(1)       1.0           ; set RBASY(1)        0.0          ;         set RBASZ(1) 0.0
	    set RBASX(2) [expr $BOA * $cosg] ; set RBASY(2) [expr $BOA * $sing] ;         set RBASZ(2) 0.0
            set RBASX(3) [expr $COA * ($cosb-$cosa*$cosg) / $sing]
            set RBASY(3) [expr -$COA * $cosa ]
            set RBASZ(3) [expr pow([expr pow($COA,2) -pow($RBASX(3),2) -pow($RBASY(3),2)],0.5)]
             }
        {2}  {# monoclinic  primitive      2/m    C_2h						     
	    set RBASX(1)       1.0           ; set RBASY(1)        0.0          ; set RBASZ(1) 0.0
	    set RBASX(2) [expr $BOA * $cosg] ; set RBASY(2) [expr $BOA * $sing] ; set RBASZ(2) 0.0
	    set RBASX(3)       0.0           ; set RBASY(3)        0.0          ; set RBASZ(3) $COA
	     }											     
        {3}  {# monoclinic  base-centered  2/m    C_2h						     
	    set RBASX(1) [expr  0.5+0.5*$BOA*$cosg] ; set RBASY(1) [expr 0.5*$BOA*$sing] ; set RBASZ(1)  0.0
	    set RBASX(2) [expr -0.5+0.5*$BOA*$cosg] ; set RBASY(2) [expr 0.5*$BOA*$sing] ; set RBASZ(2)  0.0
	    set RBASX(3)        0.0                 ; set RBASY(3)        0.0            ; set RBASZ(3) $COA
              }											     
        {4}  {# orthorombic primitive      mmm    D_2h 						     
              set RBASX(1) 1.0 ; set RBASY(1) 0.0  ; set RBASZ(1) 0.0
              set RBASX(2) 0.0 ; set RBASY(2) $BOA ; set RBASZ(2) 0.0
              set RBASX(3) 0.0 ; set RBASY(3) 0.0  ; set RBASZ(3) $COA              
              }											     
        {5}  {# orthorombic base-centered  mmm    D_2h						     
              set RBASX(1)  0.5 ; set RBASY(1) [expr 0.5*$BOA] ; set RBASZ(1) 0.0 
              set RBASX(2) -0.5 ; set RBASY(2) [expr 0.5*$BOA] ; set RBASZ(2) 0.0 
              set RBASX(3)  0.0 ; set RBASY(3)       0.0       ; set RBASZ(3) $COA
              }											     
        {6}  {# orthorombic body-centered  mmm    D_2h						     
              set RBASX(1) -0.5 ; set RBASY(1) [expr  0.5*$BOA] ; set RBASZ(1) [expr  0.5*$COA]
              set RBASX(2)  0.5 ; set RBASY(2) [expr -0.5*$BOA] ; set RBASZ(2) [expr  0.5*$COA]
              set RBASX(3)  0.5 ; set RBASY(3) [expr  0.5*$BOA] ; set RBASZ(3) [expr -0.5*$COA]
              }											     
        {7}  {# orthorombic face-centered  mmm    D_2h	
              set RBASX(1) 0.0 ; set RBASY(1) [expr  0.5*$BOA] ; set RBASZ(1) [expr  0.5*$COA]
              set RBASX(2) 0.5 ; set RBASY(2)        0.0       ; set RBASZ(2) [expr  0.5*$COA]
              set RBASX(3) 0.5 ; set RBASY(3) [expr  0.5*$BOA] ; set RBASZ(3)        0.0
              }											     
        {8}  {# tetragonal  primitive      4/mmm  D_4h						     
              set RBASX(1) 1.0 ; set RBASY(1) 0.0  ; set RBASZ(1) 0.0
              set RBASX(2) 0.0 ; set RBASY(2) 1.0  ; set RBASZ(2) 0.0
              set RBASX(3) 0.0 ; set RBASY(3) 0.0  ; set RBASZ(3) $COA
              }											     
        {9}  {# tetragonal  body-centered  4/mmm  D_4h						     
              set RBASX(1) -0.5 ; set RBASY(1)  0.5 ; set RBASZ(1) [expr  0.5*$COA]
              set RBASX(2)  0.5 ; set RBASY(2) -0.5 ; set RBASZ(2) [expr  0.5*$COA]
              set RBASX(3)  0.5 ; set RBASY(3)  0.5 ; set RBASZ(3) [expr -0.5*$COA]
              }											     
        {10} {# trigonal (rhombohedral)   primitive      -3m    D_3d 
              set costheta [expr pow([expr 2.0/3.0 * (1-$cosa)],0.5) ]
              set sintheta [expr pow([expr 1 - pow($costheta,2)],0.5)]
              set RBASX(1) [expr  0.5 * $costheta] 
              set RBASY(1) [expr -0.866025403784439 * $costheta] 
              set RBASZ(1) $sintheta
              set RBASX(2) [expr  0.5 * $costheta] 
              set RBASY(2) [expr  0.866025403784439 * $costheta] 
              set RBASZ(2) $sintheta
              set RBASX(3) -$costheta  
              set RBASY(3) 0.0               
              set RBASZ(3) $sintheta
              }
        {11} {# hexagonal   primitive      6/mmm  D_6h
              set RBASX(1) 0.866025403784439 ; set RBASY(1) -0.5 ; set RBASZ(1) 0.0
              set RBASX(2) 0.0000000000      ; set RBASY(2)  1.0 ; set RBASZ(2) 0.0
              set RBASX(3) 0.0000000000      ; set RBASY(3)  0.0 ; set RBASZ(3) $COA
             }
        {12} {# cubic       primitive      m3m    O_h
              set RBASX(1) 1.0 ; set RBASY(1) 0.0  ; set RBASZ(1) 0.0
              set RBASX(2) 0.0 ; set RBASY(2) 1.0  ; set RBASZ(2) 0.0
              set RBASX(3) 0.0 ; set RBASY(3) 0.0  ; set RBASZ(3) 1.0
              }
        {13} {# cubic       face-centered  m3m    O_h 
              set RBASX(1) 0.0 ; set RBASY(1) 0.5 ; set RBASZ(1) 0.5
              set RBASX(2) 0.5 ; set RBASY(2) 0.0 ; set RBASZ(2) 0.5
              set RBASX(3) 0.5 ; set RBASY(3) 0.5 ; set RBASZ(3) 0.0
              }
        {14} {# cubic       body-centered  m3m    O_h
              set RBASX(1) -0.5 ; set RBASY(1)  0.5 ; set RBASZ(1)  0.5
              set RBASX(2)  0.5 ; set RBASY(2) -0.5 ; set RBASZ(2)  0.5
              set RBASX(3)  0.5 ; set RBASY(3)  0.5 ; set RBASZ(3) -0.5
              }
    }

    set TOL 1E-14
    for {set i 1} {$i <= 3} {incr i} {
	if {[expr abs($RBASX($i))] < $TOL} { set RBASX($i) 0.0 }
	if {[expr abs($RBASY($i))] < $TOL} { set RBASY($i) 0.0 }
	if {[expr abs($RBASZ($i))] < $TOL} { set RBASZ($i) 0.0 }
    }
}
#========================================================================= PER PEDES END


#
#---------------------------------------------------------------------------------------
#                                                           initialize
    set SPACEGROUP 0
    set SPACEGROUP_AP 0
    if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {set STRUCTURE_TYPE "UNKNOWN"}

    set NCL $NQ
    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
       set NQCL($ICL) 1
       set WYCKOFFQ($ICL)  "-"   
       set WYCKOFFCL($ICL) "-"
    }

    set NT  0
    set NAT(1) 0
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
       set NOQ($IQ) 0
       set ITOQ(1,$IQ) 0
    }

    set IQ 0
    for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
       set IQ1 [expr $IQ + 1]
       set IQ2 [expr $IQ + $NQCL($ICL)]
       for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
          incr IQ
          set IQ1Q($IQ) $IQ1
          set IQ2Q($IQ) $IQ2
          set IQECL($IE,$ICL) $IQ
       }
    }   
#---------------------------------------------------------------------

    set Wpprc .w_pprc 

    if {[winfo exists $Wpprc]} {destroy $Wpprc } 

    set win $Wpprc
    if {$STRUCTURE_SETUP_MODE =="PER PEDES" } {
       toplevel_init $Wpprc "Structure set-up per pedes: Coordinates of sites" 0 0
    } elseif {$STRUCTURE_SETUP_MODE == "MULTI LAYERS" } {
       toplevel_init $Wpprc "Structure set-up for multi layers: Coordinates of sites" 0 0
    } else {
       toplevel_init $Wpprc "Structure set-up for 2D layered systems: Coordinates of sites" 0 0
    }
    wm sizefrom $Wpprc ""
    wm minsize  $Wpprc 100 100
    frame $Wpprc.head
    frame $Wpprc.alat
    frame $Wpprc.pvec
    frame $Wpprc.disp_Q
    frame $Wpprc.coord
    frame $Wpprc.coord.abcl
  
    pack $Wpprc.head   -fill both
    pack $Wpprc.alat   -side top
    pack $Wpprc.pvec   -side top
    pack $Wpprc.disp_Q -anchor w  -pady 5 
    pack $Wpprc.coord  -fill both -pady 15

    if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       button $Wpprc.head.goon -text "2D all done --- GO ON" \
              -width 25 -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
              -command " structure_per_pedes_save_coord " \
              -state disabled 
    } else {
       button $Wpprc.head.goon -text "all done --- GO ON" \
              -width 25 -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
              -command " structure_per_pedes_save_coord ; structure_window " \
              -state disabled 
    }  
    button $Wpprc.head.cancel -text "CLOSE" \
           -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	   -command "destroy $Wpprc" 

    label $Wpprc.coord.abcl.label \
		-text "Lattice parameters (CARTESIAN COORDINATES)" 

    pack $Wpprc.head.goon $Wpprc.head.cancel -side right  -pady 5

#---------------------------------------------------------------------
#                              lattice parameter
       label $Wpprc.alat.lab  -text "lattice parameter    A "
       entry $Wpprc.alat.ent  -font $FONT(GEN) -width 10 -relief sunken \
             -textvariable ALAT -fg $COLOR(ENTRYFG) -bg $COLOR(ENTRY) \
             -highlightthickness 0 

       if {$STRUCTURE_SETUP_MODE == "3D SURFACE" } {
           $Wpprc.alat.ent configure -state disabled
       } 

       label $Wpprc.alat.uni  -text "  \[a.u.\]"
       button $Wpprc.alat.but -height 1 \
             -text "convert A to a.u." \
             -bg $COLOR(BUT2) -command "structure_convert_ALAT $Wpprc.alat.but"

       pack  $Wpprc.alat.lab $Wpprc.alat.ent $Wpprc.alat.uni -padx 5  -pady 7 -side left
       if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {
       pack  $Wpprc.alat.but                                 -padx 80 -pady 7 -side left
       }

       if {$key!="use_primitive_vectors" } {
          $Wpprc.alat.but configure -state disabled
          $Wpprc.alat.ent configure -state disabled 
       }

#---------------------------------------------------------------------
#                              primitive vectors

    for {set i 1} {$i <= 3} {incr i} {
	set per_pedes_RBASX($i) $RBASX($i)
	set per_pedes_RBASY($i) $RBASY($i)
	set per_pedes_RBASZ($i) $RBASZ($i)
    }

    for {set i 1} {$i <= 3} {incr i} {
       frame $Wpprc.pvec.cmp$i
       label $Wpprc.pvec.cmp$i.lab  -text "a$i \[A\]" -width 10
       entry $Wpprc.pvec.cmp$i.x    -font $FONT(GEN) -width 10 -relief sunken \
             -textvariable RBASX($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
             -highlightthickness 0 
       entry $Wpprc.pvec.cmp$i.y    -font $FONT(GEN) -width 10 -relief sunken \
             -textvariable RBASY($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
             -highlightthickness 0 
       entry $Wpprc.pvec.cmp$i.z    -font $FONT(GEN) -width 10 -relief sunken \
             -textvariable RBASZ($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
             -highlightthickness 0 
       pack  $Wpprc.pvec.cmp$i.lab $Wpprc.pvec.cmp$i.x $Wpprc.pvec.cmp$i.y  \
             $Wpprc.pvec.cmp$i.z -side left
       pack  $Wpprc.pvec.cmp$i 
    }

    if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       for {set i 1} {$i <= 3} {incr i} {
          $Wpprc.pvec.cmp$i.x configure -state disabled
          $Wpprc.pvec.cmp$i.y configure -state disabled
          $Wpprc.pvec.cmp$i.z configure -state disabled
       }
    }

#---------------------------------------------------------------------
#                              per_pedes_input_mode
#
    if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {
    frame $Wpprc.inpmode ; pack  $Wpprc.inpmode
    label $Wpprc.inpmode.lab  -text "input mode for site coordinates"
    radiobutton $Wpprc.inpmode.radio_cart -relief flat -font $FONT(GEN)\
		-variable per_pedes_input_mode -value cartesian -text "cartesian" \
                -command "structure_per_pedes_read_coord_set_label cartesian"

    radiobutton $Wpprc.inpmode.radio_crys -relief flat -font $FONT(GEN)\
		-variable per_pedes_input_mode -value crystallographic -text "crystallographic" \
                -command "structure_per_pedes_read_coord_set_label crystallographic"
    pack  $Wpprc.inpmode.lab $Wpprc.inpmode.radio_cart $Wpprc.inpmode.radio_crys -side left -pady 5 
    $Wpprc.inpmode.radio_cart select
    }
 
    proc structure_per_pedes_read_coord_set_label {key} {
    global TABLABX TABLABY TABLABZ
    if {$key=="cartesian"} {
       global TABLABX TABLABY TABLABZ
       set TABLABX "   x \[A\]"
       set TABLABY "   y \[A\]"
       set TABLABZ "   z \[A\]"
    } else {
       set TABLABX "   u \[a1\]"
       set TABLABY "   v \[a2\]"
       set TABLABZ "   w \[a3\]"
    }
    }

#
#=======================================================================================
#                           set display size
#=======================================================================================
     
set NQ_display         20
set NQ_display_overlap  3

if {$NQ <= $NQ_display} {set NQ_display $NQ}

if {$NQ > $NQ_display} {
   label $Wpprc.disp_Q.label -text "switch lattice site display" -padx 30
   button $Wpprc.disp_Q.but_top -text "top"      -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc top"
   button $Wpprc.disp_Q.but_fwd -text "forward"  -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc forward"
   button $Wpprc.disp_Q.but_bwd -text "backward" -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc backward"
   button $Wpprc.disp_Q.but_bot -text "bottom"   -width 10 -height 2 -padx 20 -bg green\
                 -relief raised -command "command_IQ_display $Wpprc bottom" 
   label $Wpprc.disp_Q.range -text "displaying $NQ_display out of $NQ sites" -padx 30

   pack $Wpprc.disp_Q.label $Wpprc.disp_Q.but_top $Wpprc.disp_Q.but_fwd \
        $Wpprc.disp_Q.but_bwd  $Wpprc.disp_Q.but_bot $Wpprc.disp_Q.range -side left 

}
#---------------------------------------------------------------------------------------
if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
   if { $ZRANGE_TYPE=="extended" } {
      set w_color_field_1 8
      set w_color_field_2 8
   } else {
      set w_color_field_1 1
      set w_color_field_2 8
   }      
} else {
   set w_color_field_1 1
   set w_color_field_2 1
}
#---------------------------------------------------------------------------------------
# header created not very efficiently ..............................

set i 0
frame $Wpprc.coord.line($i)
label $Wpprc.coord.line($i).label($i) -text " " -width 8
set TABLABX "   x \[A\]"
set TABLABY "   y \[A\]"
set TABLABZ "   z \[A\]"
label $Wpprc.coord.line($i).spacer -text " " -width $w_color_field_1
label $Wpprc.coord.line($i).struc  -text " " -width $w_color_field_2
entry $Wpprc.coord.line($i).entry($i,1) -font $FONT(GEN) -width 10\
           -relief flat -textvariable TABLABX -fg $COLOR(ENTRYFG) -highlightthickness 0
$Wpprc.coord.line($i).entry($i,1) config -state disabled
entry $Wpprc.coord.line($i).entry($i,2) -font $FONT(GEN) -width 10\
           -relief flat -textvariable TABLABY -fg $COLOR(ENTRYFG) -highlightthickness 0
$Wpprc.coord.line($i).entry($i,2) config -state disabled
entry $Wpprc.coord.line($i).entry($i,3) -font $FONT(GEN) -width 10\
           -relief flat -textvariable TABLABZ -fg $COLOR(ENTRYFG) -highlightthickness 0
$Wpprc.coord.line($i).entry($i,3) config -state disabled
label  $Wpprc.coord.line($i).atoms  -width 50 -font $FONT(GEN) -fg $COLOR(ENTRYFG)

#...................................................................
    
if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
   pack  $Wpprc.coord.line($i).spacer -side left -expand yes -padx 15
   pack  $Wpprc.coord.line($i).struc  -side left -expand yes
   $Wpprc.coord.line(0).struc  configure -text " struct"
} else {
   pack  $Wpprc.coord.line($i).spacer\
         $Wpprc.coord.line($i).struc -side left -expand yes
}
pack  $Wpprc.coord.line($i).label($i)\
      $Wpprc.coord.line($i).entry($i,1)\
      $Wpprc.coord.line($i).entry($i,2)\
      $Wpprc.coord.line($i).entry($i,3)\
      $Wpprc.coord.line($i).atoms -side left -expand yes
pack  $Wpprc.coord.line($i) -anchor w




#=======================================================================================
#                        loop over displayed IQ's
#=======================================================================================
for {set IQ 1} {$IQ <= $NQ} {incr IQ} { set per_pedes_occupation($IQ) "" }
#
#
set IQ_display_start 1
set IQ_display_end   $NQ_display

for {set i 1} {$i <= $NQ_display} {incr i} {
    set IQ $i

    if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {
       set RQX($IQ) ""
       set RQY($IQ) ""
       set RQZ($IQ) ""
    }

    frame $Wpprc.coord.line($i)
    label $Wpprc.coord.line($i).label($i) -text "site $i: " -width 8
    label $Wpprc.coord.line($i).spacer -text " " -width $w_color_field_1
    label $Wpprc.coord.line($i).struc  -text " " -width $w_color_field_2
    entry $Wpprc.coord.line($i).entry($i,1) -font $FONT(GEN)  \
           -width 10 -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQX($IQ) -highlightthickness 0
    entry $Wpprc.coord.line($i).entry($i,2) -font $FONT(GEN)  \
           -width 10 -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQY($IQ) -highlightthickness 0
    entry $Wpprc.coord.line($i).entry($i,3) -font $FONT(GEN)  \
           -width 10 -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)\
       -textvariable RQZ($IQ) -highlightthickness 0
    button $Wpprc.coord.line($i).occupy  -text "occupy" -bg $COLOR(GOON) -width 7 \
	       -command "structure_per_pedes_read_coord_occupy_entry $i"
    if {$STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       button $Wpprc.coord.line($i).copy  -text "vac starts"  \
	       -bg $COLOR(LIGHTBLUE) -width 9 \
	   	   -command "structure_per_pedes_read_coord_vacuum_starts $i"
    } else {
       button $Wpprc.coord.line($i).copy  -text "copy" -bg $COLOR(LIGHTBLUE) -width 7 \
	       -command "structure_per_pedes_read_coord_copy_entry $i"
           $Wpprc.alat.ent configure -state disabled
    } 
    button $Wpprc.coord.line($i).reset  -text "reset" -bg $COLOR(CLOSE)  -width 7 \
	       -command "structure_per_pedes_read_coord_reset_entry $i" 
    label  $Wpprc.coord.line($i).atoms -justify left \
           -textvariable per_pedes_occupation($IQ) \
           -font $FONT(GEN) -fg $COLOR(ENTRYFG)

    if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       pack  $Wpprc.coord.line($i).spacer -side left -expand yes -fill y -padx 15
       pack  $Wpprc.coord.line($i).struc  -side left -expand yes -fill y
    } else {
       pack  $Wpprc.coord.line($i).spacer\
             $Wpprc.coord.line($i).struc -side left -expand yes -fill y
    }
    pack  $Wpprc.coord.line($i).label($i)\
          $Wpprc.coord.line($i).entry($i,1)\
          $Wpprc.coord.line($i).entry($i,2)\
          $Wpprc.coord.line($i).entry($i,3)\
          $Wpprc.coord.line($i).occupy\
          $Wpprc.coord.line($i).copy -side left -expand yes

    if { $STRUCTURE_SETUP_MODE == "PER PEDES" } {
	pack  $Wpprc.coord.line($i).reset -side left -expand yes
    }
    pack  $Wpprc.coord.line($i).atoms -side left -expand yes

    pack  $Wpprc.coord.line($i) -anchor w

    if {$STRUCTURE_SETUP_MODE == "2D LAYERS" || $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       $Wpprc.coord.line($i).entry($i,1) configure -state disabled
       $Wpprc.coord.line($i).entry($i,2) configure -state disabled
       $Wpprc.coord.line($i).entry($i,3) configure -state disabled
    }

}
#


 
command_IQ_display $Wpprc top

if { $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
   structure_per_pedes_read_coord_initialize_3D_surface
} 



#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
if {$CHECK_TABLE==2} {

   set NT $NCL
   set IQ 0
   for {set ICL 1} {$ICL <= $CHECK_NCL} {incr ICL} {

      set IT          $ICL
      set ZT($IT)     $IT
      set CONC($IT)   1.0
      set TXTT0($IT)  [lindex $list_ALPHA [expr 25+$IT]]

      for {set I 1} {$I <= $CHECK_NQCL($ICL)} {incr I} {
         incr IQ 
         set RWS($IQ) 1.0
         set NLQ($IQ) 3
         set RQX($IQ) $CHECK_RQX($IQ)
         set RQY($IQ) $CHECK_RQY($IQ)
         set RQZ($IQ) $CHECK_RQZ($IQ) 
         set NOQ($IQ) 1
         set ITOQ(1,$IQ) $IT
      }
   }

   structure_per_pedes_save_coord

}
#ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc



}
#                                                     structure_per_pedes_read_coord END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<




########################################################################################
#                                                         structure_per_pedes_save_coord
proc structure_per_pedes_save_coord {} {
    global LATPAR BRAVAIS TABLATPAR_a TABLATPAR_b TABLATPAR_c BOA COA
    global TABLATANG_alpha TABLATANG_beta TABLATANG_gamma Wcsys
    global LATANG NCL NQ NQCL WYCKOFFCL WYCKOFFQ
    global RCLU RCLV RCLW RQX RQY RQZ ALAT   Wpprc  STRUCTURE_SETUP_MODE
    global SPACEGROUP SPACEGROUP_AP STRUCTURE_TYPE RBASX RBASY RBASZ BOA COA
    global per_pedes_input_mode set per_pedes_RBASX per_pedes_RBASY per_pedes_RBASZ
    global CHECK_TABLE

    set PRC "structure_per_pedes_save_coord"

debug $PRC WRHEAD
debug $PRC "XXXXXXXXXXXXXXXXXXXXXXXXXX  STRUCTURE_SETUP_MODE = $STRUCTURE_SETUP_MODE "
debug $PRC "XXXXXXXXXXXXXXXXXXXXXXXXXX  per_pedes_input_mode = $per_pedes_input_mode "
#--------------------------------------------------------------------------
# recalculate LATPAR and LATANG if primitive basis has been modified

    set per_pedes_RBAS_flag 0
    set TOL 1E-10
    for {set i 1} {$i <= 3} {incr i} {
	if { [expr abs($per_pedes_RBASX($i)-$RBASX($i))] > $TOL} {set per_pedes_RBAS_flag 1}
	if { [expr abs($per_pedes_RBASY($i)-$RBASY($i))] > $TOL} {set per_pedes_RBAS_flag 1}
	if { [expr abs($per_pedes_RBASZ($i)-$RBASZ($i))] > $TOL} {set per_pedes_RBAS_flag 1}
    }

    debug $PRC ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    for {set i 1} {$i <= 3} {incr i} {
       debug $PRC "LATPAR($i)  $LATPAR($i)"
    }
    for {set i 1} {$i <= 3} {incr i} {
       debug $PRC "LATANG($i)  $LATANG($i)"
    }

    if {$per_pedes_RBAS_flag!=0  ||  $STRUCTURE_SETUP_MODE == "3D SURFACE" } {
       writescr .d.tt "INFO from <structure_per_pedes_save_coord>   ---------------- \n"
       writescr .d.tt "lattice parameters and angles will be updated  \n" 
       debug $PRC "INFO from <structure_per_pedes_save_coord>   ------------------ "
       debug $PRC "lattice parameters and angles will be updated  "

       set PI 3.141592653589793238462643
       for {set i 1} {$i <= 3} {incr i} {
          debug $PRC "LATPAR($i)  $LATPAR($i)"
          set SQR [expr pow($RBASX($i),2)+pow($RBASY($i),2)+pow($RBASZ($i),2)]
          set LATPAR($i) [expr pow($SQR,0.5)] 
          debug $PRC "LATPAR($i)  $LATPAR($i)"
       }
       set XX(1) [expr ($RBASX(2)*$RBASX(3)+$RBASY(2)*$RBASY(3)+$RBASZ(2)*$RBASZ(3))/($LATPAR(2)*$LATPAR(3))]
       set XX(2) [expr ($RBASX(1)*$RBASX(3)+$RBASY(1)*$RBASY(3)+$RBASZ(1)*$RBASZ(3))/($LATPAR(1)*$LATPAR(3))]
       set XX(3) [expr ($RBASX(2)*$RBASX(1)+$RBASY(2)*$RBASY(1)+$RBASZ(2)*$RBASZ(1))/($LATPAR(2)*$LATPAR(1))]
#----------- remove numerical noise
       set scale 1000000.0
       for {set i 1} {$i <= 3} {incr i} {
          set LATANG($i) [expr int(0.1+$scale*acos($XX($i))*180.0/$PI)/$scale]
          debug $PRC "LATANG($i)  $LATANG($i)"
       }

       set V1 [format "%14.8f%14.8f%14.8f"  $RBASX(1)  $RBASY(1)  $RBASZ(1)]
       set V2 [format "%14.8f%14.8f%14.8f"  $RBASX(2)  $RBASY(2)  $RBASZ(2)]
       set V3 [format "%14.8f%14.8f%14.8f"  $RBASX(3)  $RBASY(3)  $RBASZ(3)]
       writescr .d.tt "cryst. PRIM. VECTOR 1: $V1 \n" 
       writescr .d.tt "cryst. PRIM. VECTOR 2: $V2 \n"
       writescr .d.tt "cryst. PRIM. VECTOR 3: $V3 \n"
       set V1 [format "%14.8f%14.8f%14.8f"  $LATANG(1)  $LATANG(2)  $LATANG(3)]
       writescr .d.tt "angles               : $V1 \n\n"
       writescr .d.tt "after normalisation          \n"

       if {$STRUCTURE_SETUP_MODE != "3D SURFACE" } {
          for {set i 1} {$i <= 3} {incr i} {
      	     set RBASX($i) [expr $RBASX($i)/$LATPAR(1)]
             set RBASY($i) [expr $RBASY($i)/$LATPAR(1)]
             set RBASZ($i) [expr $RBASZ($i)/$LATPAR(1)]
          }
       }
       set V1 [format "%14.8f%14.8f%14.8f"  $RBASX(1)  $RBASY(1)  $RBASZ(1)]
       set V2 [format "%14.8f%14.8f%14.8f"  $RBASX(2)  $RBASY(2)  $RBASZ(2)]
       set V3 [format "%14.8f%14.8f%14.8f"  $RBASX(3)  $RBASY(3)  $RBASZ(3)]
       writescr .d.tt "cryst. PRIM. VECTOR 1: $V1 \n" 
       writescr .d.tt "cryst. PRIM. VECTOR 2: $V2 \n"
       writescr .d.tt "cryst. PRIM. VECTOR 3: $V3 \n"

    }

    set BOA  [expr $LATPAR(2)/$LATPAR(1)]
    set COA  [expr $LATPAR(3)/$LATPAR(1)]

    set NCL $NQ
 
    destroy $Wpprc

    if {$per_pedes_input_mode!="cartesian"} { convert_basis_vectors_RQ cryst_to_cart }


#--------------------------- if the bravais-lattice was not specified -- try to find out
#
set TOL 0.0000001

if {$BRAVAIS=="0"} {
#
# in doubt use:   "triclinic   primitive      -1     C_i"
#
set BRAVAIS 1

if {[expr abs($LATANG(1)-$LATANG(2))]<$TOL} {
#---------------------------------------------------------------------------------------
# alfa = beta

   if {[expr abs($LATANG(1)-90.0)]<$TOL} {
#---------------------------------------------------------------------------------------
#     alfa = beta = 90
      if {[expr abs($LATANG(3)-90.0)]<$TOL} {
#---------------------------------------------------------------------------------------
#        alfa = beta = gamma = 90
         if {[expr abs($LATPAR(1)-$LATPAR(2))]<$TOL} {
#---------------------------------------------------------------------------------------
#           a = b
            if {[expr abs($LATPAR(1)-$LATPAR(3))]<$TOL} {
#---------------------------------------------------------------------------------------
#              a = b = c   "cubic       primitive      m3m    O_h "
               set BRAVAIS 12
            } else {
#---------------------------------------------------------------------------------------
#              a = b <> c  "tetragonal  primitive      4/mmm  D_4h"
               set BRAVAIS 8
            }
         } else {
#---------------------------------------------------------------------------------------
#           a <> b  "orthorombic primitive      mmm    D_2h"
            set BRAVAIS 4  
         }
      } else {
#---------------------------------------------------------------------------------------
#        alfa = beta = 90 <> gamma 
         if {[expr abs($LATPAR(1)-$LATPAR(2))]<$TOL} {
#---------------------------------------------------------------------------------------
#           a = b
            if {[expr abs($LATANG(3)-120.0)]<$TOL} {
#---------------------------------------------------------------------------------------
#              gamma = 120  "hexagonal   primitive      6/mmm  D_6h"
               set BRAVAIS 11 
            } else {
#---------------------------------------------------------------------------------------
#              gamma <> 120  "orthorombic base-centered  mmm    D_2h"
               set BRAVAIS 5
            }
         } else {
#---------------------------------------------------------------------------------------
#           a <> b  "monoclinic  primitive      2/m    C_2h"
            set BRAVAIS 2
         }         
      }

   } else {
#---------------------------------------------------------------------------------------
#     alfa = beta <> 90
      if {[expr abs($LATANG(1)-$LATANG(3))]<$TOL} {
#---------------------------------------------------------------------------------------
#        alfa = beta = gamma <> 90
         if {[expr abs($LATPAR(1)-$LATPAR(2))]<$TOL&&\
             [expr abs($LATPAR(1)-$LATPAR(3))]<$TOL} {
#---------------------------------------------------------------------------------------
#           a = b = c
            if {[expr abs($LATANG(1)-60)]<$TOL} {
#              "cubic       face-centered  m3m    O_h "
               set BRAVAIS 13
            } else {
               set ANGBCC [expr acos(-1.0/3.0)*180.0/$PI)]
               if {[expr abs($LATANG(1)-$ANGBCC)]<$TOL} {
#                 "cubic       body-centered  m3m    O_h "
                  set BRAVAIS 14
               } else {
#                 "trigonal    primitive      -3m    D_3d"
                  set BRAVAIS 10 
               }
            }
         }
#---------------------------------------------------------------------------------------
      }
   }
    
} else {
#---------------------------------------------------------------------------------------
# alfa <> beta
   debug $PRC "alfa <> beta"
}

}
#------------------------------------------------------------------------ $BRAVAIS=="0"


#
#------------------------------------------------------------------- call next procedure
#   in case of "2D LAYERS":    run_findsym will be called by structure_window_2D 
#
    if {$CHECK_TABLE==0} {

       if {$STRUCTURE_SETUP_MODE != "2D LAYERS" && \
           $STRUCTURE_SETUP_MODE != "3D SURFACE"} {
          debug $PRC "******************* calling  run_findsym structure_window "
          run_findsym structure_window
       } else {
          debug $PRC "******************* calling   structure_window_2D "
          structure_window_2D
       }

    } else {
       run_findsym structure_via_table_run_findsym
    }
}
#                                                     structure_per_pedes_save_coord END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<








########################################################################################
#                         STRUCTURE MULTI LAYERS
########################################################################################
#                                                       enter structure for multilayer
proc structure_multilayer {} {

global BRAVAIS NCL LATPAR LATANG flag Wcsys
global TABBRAVAIS BOHRRAD ALAT 
global Wcrml xband_path
global COLOR WIDTH HEIGHT FONT

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name 
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ          ML_START_IL1 ML_START_IL2
global ML_NLAY 
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select

global N_rep_SU_a N_rep_SU_b SU_NAME

set N_rep_SU_a 1 ; set N_rep_SU_b 1 ; set SU_NAME(1) "SU1" ; set SU_NAME(2) "SU2" 

    set tcl_precision 17
    set PRC structure_multilayer
#
#-------------------------------------------------------- read 2D_structure.dat 
#
    read_multilayers

    set NML1    $ML_NML(1)   
    set NML2    $ML_NML(2)   
    set NSUBSYS $ML_NSU

    if  { $NML1 =="0" } {return}
#------------------------------------------------------------------------------

    set flag 3 
    set Wcrml .w_crml
    toplevel_init $Wcrml "Structure set-up for multilayers" 0 0
    wm positionfrom $Wcrml user
    wm sizefrom $Wcrml ""
    
    set tcl_precision 17

    reset_struc_parameter

    set BGhead $COLOR(LIGHTBLUE)
    set BGCOL1 $COLOR(LIGHTSALMON1)
    set BGCOL2 $COLOR(STEELBLUE1) 
     
    frame $Wcrml.head -relief raised -borderwidth 2 -bg $BGhead
    frame $Wcrml.head.a -bg $BGhead
    label $Wcrml.head.a.lab1 -text "   lattice parameter  A"   \
          -bg $BGhead -pady 10
    entry $Wcrml.head.a.entry -width 15 -relief sunken -font $FONT(GEN)  \
          -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -textvariable ALAT
    button $Wcrml.head.a.convert -width 17 -height 3 \
          -text "Angstroem to a.u. \n if necessary " \
          -bg $COLOR(ORANGE) -command "structure_convert_ALAT $Wcrml.head.a.convert" -pady 5
    button $Wcrml.head.a.cancel  \
          -text "CLOSE " -width 17 -height 3 -bg $COLOR(CLOSE) \
            -command "destroy $Wcrml" -pady 5

    pack $Wcrml.head.a.lab1  -padx 5 -side left 
    pack $Wcrml.head.a.entry -padx 5 -side left
    pack $Wcrml.head.a.convert -padx 5  -side left
    pack $Wcrml.head.a.cancel  -padx 25  -side right -fill x
    pack $Wcrml.head.a  -side top -expand yes -fill x
    pack $Wcrml.head -side top -expand yes -fill x

    frame  $Wcrml.col1 
    frame  $Wcrml.col2

    pack  $Wcrml.col1 $Wcrml.col2 -side left -fill y -anchor n

    set COL1 $Wcrml.col1
    set COL2 $Wcrml.col2

    set wlb2 7
    set wlb3 5
    set wbut 3

    set wlab1 10
    set wlab2 18
    set lslid 180

    set NMLMAX 60
#===============================================================================
#                column 1   ------   homogenous lattices
#===============================================================================

    set LIMIT 25.0

    frame $COL1.scale_c -bg $BGCOL1
    pack  $COL1.scale_c -side top -anchor n -fill x
    label $COL1.scale_c.lab1 -text "scale c  (%)" -width $wlab1 -bg $BGCOL1 -padx 20 
    scale $COL1.scale_c.scale1 -from [expr -$LIMIT] -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C \
       -bg $BGCOL1 -highlightthickness 0 
    pack $COL1.scale_c.lab1 $COL1.scale_c.scale1  -side left

    for {set ML 1} {$ML <= $NML1} {incr ML} {
       set C 1
       frame $COL1.ml$ML -relief raised -borderwidth 2 -bg $BGCOL1
       pack  $COL1.ml$ML -side top
    
       label $COL1.ml$ML.lab1 -text "$ML_name($ML,$C)"  -width $wlab1 -bg $BGCOL1 -padx 20
       scale $COL1.ml$ML.scale -from $ML_NLAY($ML,$C,1) -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -resolution $ML_NLAY($ML,$C,1) -variable ML_Ntot($ML,$C) \
       -bg $BGCOL1 -highlightthickness 0 
       pack $COL1.ml$ML.lab1 $COL1.ml$ML.scale -side left
       button $COL1.ml$ML.but1 -text "OK" -padx 20 -width $wbut -height 2  -bg $COLOR(GOON) \
                  -command  "set ML_select $ML ; structure_multilayer_create 3D 1 DUMMY"
       pack $COL1.ml$ML.but1  -side right
    }
#===============================================================================
#                column 2   ------   heterogenous lattices
#===============================================================================
    set ML_SUBSYS_REF 1
    set ML_SCALE_ISD  0.0

    frame $COL2.subsys_ref -bg $BGCOL2
    pack  $COL2.subsys_ref -side top -anchor n -fill x
    label $COL2.subsys_ref.lab1 -text "ALAT refers to sub system"  -width 30 -bg $BGCOL2 -padx 20 
    radiobutton $COL2.subsys_ref.rad1 -relief flat -font $FONT(GEN) -bg $BGCOL2 \
		-variable ML_SUBSYS_REF -value 1 -text "1"  \
                -highlightthickness 0 	
    radiobutton $COL2.subsys_ref.rad2 -relief flat -font $FONT(GEN) -bg $BGCOL2 \
		-variable ML_SUBSYS_REF -value 2 -text "2"  \
                -highlightthickness 0 
    label $COL2.subsys_ref.lab2 -text "of heterogeneous lattice"  -width 30 -bg $BGCOL2
    pack $COL2.subsys_ref.lab1 $COL2.subsys_ref.rad1 $COL2.subsys_ref.rad2 \
         $COL2.subsys_ref.lab2 -side left

    frame $COL2.relax_ISD -bg $BGCOL2
    pack  $COL2.relax_ISD -side top -anchor n -fill x
    label $COL2.relax_ISD.lab1 -text "scale inter-system distance (%)" -width 35 -bg $BGCOL2 -padx 20 
    scale $COL2.relax_ISD.scale1 -from [expr -$LIMIT] -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_ISD \
       -bg $BGCOL2 -highlightthickness 0 
    pack $COL2.relax_ISD.lab1 $COL2.relax_ISD.scale1  -side left

    frame $COL2.scale_c -bg $BGCOL2
    pack  $COL2.scale_c -side top -anchor n -fill x
    label $COL2.scale_c.lab1 -text "scale c  (%)" -width $wlab2 -bg $BGCOL2 -padx 20 
    scale $COL2.scale_c.scale1 -from [expr -$LIMIT] -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C1 \
       -bg $BGCOL2 -highlightthickness 0 
    scale $COL2.scale_c.scale2 -from [expr -$LIMIT] -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C2 \
       -bg $BGCOL2 -highlightthickness 0 
    pack $COL2.scale_c.lab1 $COL2.scale_c.scale1  $COL2.scale_c.scale2  -side left

#
#---------------------- $ML_NLAY($ML,$C,1)==2 && $ML_NLAY($ML,$C,2)==2
# 
    set C 2
    for {set ML 1} {$ML <= $NML2} {incr ML} {
#-------------------------------------------------------------------------------
       if { $ML_NLAY($ML,$C,1)==2 && $ML_NLAY($ML,$C,2)==2 } {
#
#------------------------- case a    n * 2 / m * 2 
#
       frame $COL2.mla$ML -relief raised -borderwidth 2 -bg $BGCOL2
       pack  $COL2.mla$ML -side top -anchor n
    
       label $COL2.mla$ML.lab1 -text "$ML_name($ML,$C)"  -width $wlab2 -bg $BGCOL2 -padx 20
       scale $COL2.mla$ML.scale1 -from $ML_NLAY($ML,$C,1) -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -resolution $ML_NLAY($ML,$C,1) -variable ML_Na1($ML) \
       -bg $BGCOL2 -highlightthickness 0 

       scale $COL2.mla$ML.scale2 -from $ML_NLAY($ML,$C,2) -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -resolution $ML_NLAY($ML,$C,2) -variable ML_Na2($ML) \
       -bg $BGCOL2 -highlightthickness 0 

       pack $COL2.mla$ML.lab1 $COL2.mla$ML.scale1 $COL2.mla$ML.scale2 -side left
       button $COL2.mla$ML.but1 -text "OK" -padx 20 -width $wbut -height 2  -bg $COLOR(GOON) \
                  -command  "set ML_select $ML ; structure_multilayer_create 3D 2 a"
       pack $COL2.mla$ML.but1  -side right

#
#------------------------- case b    (2*n+1) * 2 / (2*m+1) * 2
#
       frame $COL2.mlb$ML -relief raised -borderwidth 2 -bg $BGCOL2
       pack  $COL2.mlb$ML -side top -anchor n
    
       label $COL2.mlb$ML.lab1 -text "$ML_name($ML,$C)"  -width $wlab2 -bg $BGCOL2 -padx 20
       scale $COL2.mlb$ML.scale1 -from 1 -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX  -variable ML_Nb1($ML) \
       -bg $BGCOL2 -highlightthickness 0 \
       -command "structure_multilayer_adjust_ML_Nb 1 $ML"

       #----------------------------------------------------------------------------
       proc structure_multilayer_adjust_ML_Nb {k ML XX} {
           global ML_Nb1 ML_Nb2
	       if { $k==1 } {
		   if { [expr fmod($ML_Nb1($ML),2)]< 0.0001 } { 
                      set ML_Nb1($ML) [expr $ML_Nb1($ML) -1 ]
                   }
               } else {
		   if { [expr fmod($ML_Nb2($ML),2)]< 0.0001 } { 
                      set ML_Nb2($ML) [expr $ML_Nb2($ML) -1 ]
                   }
               }     
	   }
       #----------------------------------------------------------------------------
 
       scale $COL2.mlb$ML.scale2 -from 1 -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -variable ML_Nb2($ML) \
       -bg $BGCOL2 -highlightthickness 0 \
       -command "structure_multilayer_adjust_ML_Nb 2 $ML" 

       pack $COL2.mlb$ML.lab1 $COL2.mlb$ML.scale1 $COL2.mlb$ML.scale2 -side left
       button $COL2.mlb$ML.but1 -text "OK" -padx 20 -width $wbut -height 2  -bg $COLOR(GOON) \
                  -command  "set ML_select $ML ; structure_multilayer_create 3D 2 b"
       pack $COL2.mlb$ML.but1  -side right

       }
    }

    set LIM1 1
    set LIM2 1
    set C 2
    for {set ML 1} {$ML <= $NML2} {incr ML} {
        if { $ML_NLAY($ML,$C,1)!=2 || $ML_NLAY($ML,$C,2)!=2 } {
	   if { $LIM1 < $ML_NLAY($ML,$C,1)} { set LIM1 $ML_NLAY($ML,$C,1) }
	   if { $LIM2 < $ML_NLAY($ML,$C,2)} { set LIM2 $ML_NLAY($ML,$C,2) }
        }
    }

    frame $COL2.start_il -bg $BGCOL2
    pack  $COL2.start_il -side top -anchor n -fill x
    label $COL2.start_il.lab1 -text "start with sublayer" -width $wlab2 -bg $BGCOL2 -padx 20 
    scale $COL2.start_il.scale1 -from 1 -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIM1 -resolution 1 -variable ML_START_IL1 \
       -bg $BGCOL2 -highlightthickness 0 
    scale $COL2.start_il.scale2 -from 1 -orient {horizontal} -length $lslid  \
         -sliderlength {20} -to $LIM2 -resolution 1 -variable ML_START_IL2 \
       -bg $BGCOL2 -highlightthickness 0 
    pack $COL2.start_il.lab1 $COL2.start_il.scale1  $COL2.start_il.scale2  -side left


#
#---------------------- $ML_NLAY($ML,$C,1)!=2 || $ML_NLAY($ML,$C,2)!=2
# 
    set C 2
    for {set ML 1} {$ML <= $NML2} {incr ML} {
#-------------------------------------------------------------------------------
       if { $ML_NLAY($ML,$C,1)!=2 || $ML_NLAY($ML,$C,2)!=2 } {

       set ML_resol 1
#
       frame $COL2.mla$ML -relief raised -borderwidth 2 -bg $BGCOL2
       pack  $COL2.mla$ML -side top -anchor n
    
       label $COL2.mla$ML.lab1 -text "$ML_name($ML,$C)"  -width $wlab2 -bg $BGCOL2 -padx 20
       scale $COL2.mla$ML.scale1 -from 1 -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -resolution $ML_resol -variable ML_Na1($ML) \
       -bg $BGCOL2 -highlightthickness 0 

       scale $COL2.mla$ML.scale2 -from 1 -orient {horizontal} -length $lslid  \
       -sliderlength {20} -to $NMLMAX -resolution $ML_resol -variable ML_Na2($ML) \
       -bg $BGCOL2 -highlightthickness 0 

       pack $COL2.mla$ML.lab1 $COL2.mla$ML.scale1 $COL2.mla$ML.scale2 -side left
       button $COL2.mla$ML.but1 -text "OK" -padx 20 -width $wbut -height 2  -bg $COLOR(GOON) \
                  -command  "set ML_select $ML ; structure_multilayer_create 3D 2 a"
       pack $COL2.mla$ML.but1  -side right

       }
    }


}
#                                                               structure_multilayer END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<





########################################################################################
#                                                                                   MAIN
########################################################################################
global CONC NCL flag BOA COA RCLU RCLV RCLW
global LATPAR LATANG NQCL NOQ CONC RWS 
global flagset TABBRAVAIS MAG_DIR TXTT
global NQ  NCL  BRAVAIS  SPACEGROUP SPACEGROUP_AP
global Wcsys Krasmol Wpprc STRUCTURE_SETUP_MODE  W_sites_list
global CHECK_TABLE

set STRUCTURE_SETUP_MODE ""
set W_sites_list ""

global structure_window_calls
    
set structure_window_calls     0

set MAG_DIR(1) 0
set MAG_DIR(2) 0
set MAG_DIR(3) 0

set flag 1
set tcl_precision 17
global create_structure_lock     

set create_structure_lock 0
set Krasmol 1
set Wpprc .dummy

reset_struc_parameter

table_bravais

table_CHSYM 
    
table_RWS

structure_window

set CHECK_TABLE 0
if {$CHECK_TABLE==1} {structure_via_table}
if {$CHECK_TABLE==2} {structure_per_pedes}

## 
##create_system_2D

}


