########################################################################################
########################################################################################
#                                                                         tutorial
#     
#     this procedure supplies the PACKAGE INDEPENDENT part of the inpfile set up and 
#     calls the specific procedures to set up the input mask and to write the input file
#     
proc tutorial {} {
    global structure_window_calls inpfile inpsuffix 
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VARLST NCPA
    global SPRKKR_EXPERT xband_path 
    global COLOR WIDTH HEIGHT FONT
    global TUTORIAL 

    set TUTORIAL 1

global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1 
global BGWR  ; set BGWR   lavender
global BGK   ; set BGK    SteelBlue1 
global BGCLU ; set BGCLU  LightSkyBlue 
global BGE   ; set BGE    moccasin 
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1  
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1 

## plum1            LightSkyBlue       azure1            
## thistle1	    bisque             SteelBlue1   
## SpringGreen	    seashell           DeepSkyBlue1 
## chartreuse	    linen              SlateGray1   
## GreenYellow	    ivory              NavajoWhite1 
## PaleGreen	    azure              LightSalmon1 
## gold		    lavender           LightPink1   
## wheat	    moccasin           orchid1      
## thistle	    khaki1             cornsilk1          
## seashell1	    LightGoldenrod1    ivory1        
## cyan		    yellow1            LavenderBlush1
## AntiqueWhite1    RosyBrown1        
## LemonChiffon1    orange1           


#===============================================================================================
#                                system file available
#===============================================================================================


    set Wcinp .tutorial
    toplevel_init $Wcinp "tutorial for programm package $PACKAGE " 0 0
    puts "******************************** tutorial for programm package $PACKAGE "
    wm geometry $Wcinp +10+50
    wm positionfrom $Wcinp user
    wm sizefrom $Wcinp ""
    wm minsize $Wcinp 

    set buttonbgcolor green1 
    set buttonheight  2

    frame $Wcinp.a
#------------------------------------------------------------------------------------
    frame $Wcinp.a.sys 
    button $Wcinp.a.sys.ssys -text "SELECT SYSTEM" -width 20 -height $buttonheight \
	-command "select_system; destroy $Wcinp" -bg lightblue
#
    button $Wcinp.a.sys.esys -text "edit \n system file" -width 20 -height $buttonheight \
        -command "destroy $Wcinp ; edit_sysfile" -bg lightblue
    proc edit_sysfile { } {
    global editor edback edxterm edoptions sysfile syssuffix
    set ed_file $sysfile$syssuffix
    eval set res [catch "exec $edxterm $editor $edoptions $sysfile$syssuffix $edback" message]
    }
    pack configure $Wcinp.a.sys.ssys -anchor n  -side top -pady 1 -padx 30
    pack configure $Wcinp.a.sys.esys -anchor n  -side top -pady 1 -padx 30
#------------------------------------------------------------------------------------
    frame $Wcinp.a.fil 
    label $Wcinp.a.fil.lab -text "input file name"
#
    regsub "$syssuffix\$" $sysfile "" inpfile
    set inpfile $inpfile$inpsuffix

    entry $Wcinp.a.fil.ent -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -textvariable {inpfile} -width {29} \
          -font $FONT(GEN)
     pack configure $Wcinp.a.fil.lab $Wcinp.a.fil.ent  -anchor s  -side top -padx 30
#------------------------------------------------------------------------------------
    frame $Wcinp.a.aa
#
     button $Wcinp.a.aa.ende -text "CLOSE" -width 20 -height $buttonheight \
	-command "destroy $Wcinp" -bg $COLOR(CLOSE)

     pack configure $Wcinp.a.aa.ende \
            -anchor s  -side top -pady 1 -padx 5
#
     button $Wcinp.a.aa.edit -text "edit inpfile \n quit" -width 20 -height $buttonheight \
	-command "destroy $Wcinp ; edit_inpfile" -bg LightSalmon2
     proc edit_inpfile { } {
     global editor edback edxterm edoptions inpfile inpsuffix
     set ed_file $inpfile
     eval set res [catch "exec $edxterm $editor $edoptions $inpfile $edback" message]
     }

     pack configure $Wcinp.a.aa.edit \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
    frame $Wcinp.a.bb
#
     button $Wcinp.a.bb.write -text "write \ninpfile" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp nix " -bg PaleGreen

     button $Wcinp.a.bb.append -text "append to\ninpfile" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp append" -bg SeaGreen2

     pack configure $Wcinp.a.bb.write $Wcinp.a.bb.append \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
    frame $Wcinp.a.cc 
#
     button $Wcinp.a.cc.ok -text "write inpfile\nquit" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp nix ; destroy $Wcinp" -bg $buttonbgcolor
#
     button $Wcinp.a.cc.edit -text "write+edit inpfile \n quit" -width 20 -height $buttonheight \
	-command "write_inpfile $Wcinp edit ; destroy $Wcinp" -bg YellowGreen

     pack configure $Wcinp.a.cc.ok $Wcinp.a.cc.edit \
            -anchor s  -side top -pady 1 -padx 5
#------------------------------------------------------------------------------------
     pack configure $Wcinp.a.sys  $Wcinp.a.fil \
             $Wcinp.a.aa  $Wcinp.a.bb  $Wcinp.a.cc -side left -fill x
     pack configure $Wcinp.a
#------------------------------------------------------------------------------------

set w  $Wcinp
set wl 12
set we 30
set ws 160
set fe $FONT(GEN)

frame $w.cols 
pack configure $w.cols -in $w -anchor w -side top -fill x -expand y

frame $w.cols.colA -bg $BGP -relief raised -borderwidth 2
frame $w.cols.colB -bg $BGP -relief raised -borderwidth 2 

frame $w.cols.col1 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col2 -bg $BGP -relief raised -borderwidth 2 
frame $w.cols.col3 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col4 -bg $BGP -relief raised -borderwidth 2 

set wc1 $w.cols.col1 ; set wc2 $w.cols.col2  
set wc3 $w.cols.col3 ; set wc4 $w.cols.col4  


set VAR(POTFIL) NIX
set VAR(OWRPOT) 0

table_bravais

#=======================================================================================
#                  call package specific procedure   tutorial_${PACKAGE}
#=======================================================================================
#
if {[file exists $xband_path/tutorial_${PACKAGE}.tcl] } {

    tutorial_${PACKAGE}

#=======================================================================================
#                                TUTORIAL not available
#=======================================================================================
} else {
   give_warning "." "WARNING \n\n tutorial \n\n for $PACKAGE \n\n \
   not available "
}
#=======================================================================================

} 
#                                                                     END tutorial
########################################################################################



