


      parameter (nprocmax=2000, nchmax=250) 

      character*(nchmax) proc_nam(nprocmax),line
      character*(nchmax) proc_fil(nprocmax)
      integer            proc_lin(nprocmax)
      integer            proc_lfil(nprocmax)
      integer            proc_lnam(nprocmax)


C-----------------------------------------------------------------------
       open(1,file='tcltree_proc_def')
       do ip=1,nprocmax
          read(1,*,end=10) proc_fil(ip),proc_lin(ip),proc_nam(ip)
          nproc = ip
          proc_lfil(ip) = LNGSTRING(proc_fil(ip),nchmax)
          proc_lnam(ip) = LNGSTRING(proc_nam(ip),nchmax)
          write(*,*) proc_nam(ip)(1:proc_lnam(ip)),' ### ',
     &               proc_fil(ip)(1:proc_lfil(ip))
       end do
 10    continue
C-----------------------------------------------------------------------

       stop
       end 
 
      FUNCTION LNGSTRING(STRING,LSTRMAX)
C
C   ********************************************************************
C   *                                                                  *
C   *  find position of last non-blank character in STRING(1:LSTRMAX)  *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C Dummy arguments
C
      INTEGER LSTRMAX
      CHARACTER*(*) STRING
      INTEGER LNGSTRING
C
C Local variables
C
      CHARACTER C
      INTEGER I
      INTEGER ICHAR
C
      LNGSTRING = 0
      DO I = LSTRMAX,1, - 1
         C = STRING(I:I)
         IF ( C.NE.' ' .AND. ICHAR(C).GT.0 ) THEN
            LNGSTRING = I
            RETURN
         END IF
      END DO
      END
