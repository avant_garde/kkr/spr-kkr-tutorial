########################################################################################
#                                    SPR-KKR
########################################################################################
#                                                                         tutorial
#
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and
#     adds the corresponding entries to the input mask created by   tutorial
#
#     HE 21/05/07 write SPRKKR input and pot files for new format version 6
#                 including 2D slab and extended (LIR) structures dealt in TB mode
#

proc tutorial_SPR-KKR {} {

    set PRC "tutorial_SPR-KKR"

    global structure_window_calls
    global sysfile syssuffix  PACKAGE sysfile_host potfile
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NL
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS DIMENSION
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE

    global CLUSTER_TYPE
    global NQCLU NTCLU IQ_IQCLU N5VEC_IQCLU RQCLU
    global NOCC_IQCLU ZTCLU TXTTCLU CONCCLU ITCLU_OQCLU

    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VAR0 VARLST NCPA
    global NVALT_DEFAULT
    global tutorial_SPRKKR_wc1
    global tutorial_SPRKKR_wc2
    global tutorial_SPRKKR_wc3
    global inpfile inpsuffix sysfile syssuffix
    global COLOR WIDTH HEIGHT FONT
    global TABCQNTOP_N
    global QUICK TABNCORE 
    global VARARRAYINITIALIZED

#****************************************************************************************
global NM IMQ RWSM QMTET QMPHI
global TUT_TEXT


set system   "TUT1"
set sysfile  "TUT1"
set inpfile  "TUT1.inp"
set TUT_TEXT "non-magnetic Cu in scalar relativistic mode"

set VAR(NONMAG)     1
set VAR(HAMILTONIAN) "SREL"

set DIMENSION "3D"
set BRAVAIS 13
set RBASX(1) 0.000000000000 ; set RBASY(1) 0.500000000000 ; set RBASZ(1) 0.500000000000
set RBASX(2) 0.500000000000 ; set RBASY(2) 0.000000000000 ; set RBASZ(2) 0.500000000000
set RBASX(3) 0.500000000000 ; set RBASY(3) 0.500000000000 ; set RBASZ(3) 0.000000000000
set ALAT      6.829644644208
set RWS(1)    2.669000000000
set NL 3 
set NQ 1 
set RQX(1) 0.000000000000 ; set RQY(1) 0.000000000000 ; set RQZ(1) 0.000000000000
set QMTET(1) 0.0          ; set QMPHI(1) 0.0
set IMQ(1) 1
set NLQ(1) 3
set NOQ(1) 1
set NM 1
set RWSM(1)   $RWS(1)

set NT 1
set ZT(1) 29
set TXTT(1) "Cu"
set NAT(1) 1
set CONC(1) 1.0

set ITOQ(1,1) 1
set IQAT(1,1) 1


#****************************************************************************************


    set tutorial_SPRKKR_wc1 ".DUMMY"
    set tutorial_SPRKKR_wc2 ".DUMMY"
    set tutorial_SPRKKR_wc3 ".DUMMY"

    if {![info exists CLUSTER_TYPE] } {set CLUSTER_TYPE "NONE"}

    set AKHE 1

    set TABNCORE [list 0 0 0 2 2 2 2 2 2 2 2 10 10 10 10 10 10 10 10 18 18 18  \
                       18 18 18 18 18 18 18 18 28 28 28 28 28 28 28 36 36 36 36 36   \
                       36 36 36 36 36 36 46 46 46 46 46 46 46 54 54 54 54 54 54 54   \
                       54 54 54 54 54 54 54 54 54 54 68 68 68 68 68 68 68 68 78 78   \
                       78 78 78 78 78 86 86 86 86 86 86 86 86 86 86 86 86 86 86 86   \
                       86 86 86 86 86 86 86 86 86 ]

# highest n quantum number of core state

    set TABCQNTOP_N [list 0 0 0 1 1 1 1 1 1 1 1  2 2 2 2 2 2 2 2 3 3 \
                            3 3 3 3 3 3 3 3 3 3  3 3 3 3 3 3 4 4 4 4 \
                            4 4 4 4 4 4 4 4 4 4  4 4 4 4 5 5 5 5 5 5 \
                            5 5 5 5 5 5 5 5 5 5  5 5 5 5 5 5 5 5 5 5 \
                            5 5 5 5 5 5 6 6 6 6  6 6 6 6 6 6 6 6 6 6 \
                            6 6 6 6 6 6 6 6 6 6  6 6 6 6 6 6 6 6 6 6 \
                           ]

#
#----------------------------------------------- deactivate unused top buttons
#
pack forget $Wcinp.a.aa.edit
pack forget $Wcinp.a.bb.write
pack forget $Wcinp.a.bb.append
#
#----------------------------------------------- reset command for extit buttons
#
$Wcinp.a.cc.ok    configure -command "create_inpfile_SPR-KKR_check ok"
$Wcinp.a.cc.edit  configure -command "create_inpfile_SPR-KKR_check edit"
#
#--------------------------------------------------------------------------------------
#
global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1
global BGWR  ; set BGWR   lavender
global BGKKR ; set BGKKR  LightSkyBlue1 
global BGK   ; set BGK    SteelBlue1
global BGCLU ; set BGCLU  LightSkyBlue
global BGE   ; set BGE    moccasin
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1
global BGMOD ; set BGMOD  cornsilk1
global BGMAG ; set BGMAG  lavender
global BGLEX ; set BGLEX  LightPink1

set buttonbgcolor green1
set buttonheight  2

set w  $Wcinp
set wl 12
set wl1 7
set wl2 5
set we 30
set ws 160
set ws1 100
set fe $FONT(GEN)

set wcA $w.cols.colA
set wcB $w.cols.colB

set wc1 $w.cols.col1 
set wc2 $w.cols.col2
set wc3 $w.cols.col3 

########################################################################################

#--------------------------------------------------------------------------------
#              set up list of variable names and initialize
#--------------------------------------------------------------------------------

set VARLST [list IPRINT ITEST WRTAU RDTAU WRTAUMQ RDTAUMQ NOHFF NOCORE FSOHFF EFG \
                 NOWRDOS WRCPA WRKAPDOS WRPOLAR ]
lappend VARLST   DATASET ADSI POTFIL
lappend VARLST   TAUMAT TAUEXP SURFACE IMPURITY SLAB IQCNTR ITCNTR NSHLCLU \
                 CLURAD NLOUT STRUC NQSLAB NSURF PATHLEN NLEGXX ISYM
lappend VARLST   SREL NREL OP BEXT BREITINT ORBPOL MALF MBET MGAM
lappend VARLST   GRID NE EMIN EMAX ImE SEARCHEF EF
lappend VARLST   CPANITER CPATOL
lappend VARLST   STRETA STRRMAX STRGMAX
lappend VARLST   SCFVXC SCFALG SCFNITER SCFMIX SCFTOL SCFISTBRY SCFITDEPT \
                 SCFMIXOP SCFCOREHOLE SCFITHOLE SCFNQNHOLE SCFLQNHOLE SCFSIM\
                 FULLPOT SPHERCELL POTFMT SEMICORE LLOYD VTMZ QIONSCL 

#lappend VARLST   SURFACE IMPURITY SLAB   \
#                   STRUC NQSLAB NSURF PATHLEN NLEGXX ISYM


# only init VAR(...) when called first, otherwise remember values
if {! [info exists VARARRAYINITIALIZED]} {
    
    set VARARRAYINITIALIZED 1
    foreach V $VARLST { set VAR($V) "" }
    

    set VAR(IPRINT)   0
    set VAR(ITEST)    0
    set VAR(WRTAU)    0
    set VAR(RDTAU)    0
    set VAR(WRTAUMQ)  0
    set VAR(RDTAUMQ)  0
##  set VAR(NONMAG)     0
    set VAR(NOHFF)    0
    set VAR(NOCORE)   0
    set VAR(FSOHFF)   0
    set VAR(EFG)      0
    set VAR(NOWRDOS)  0
    set VAR(WRLMLDOS) 0
    set VAR(WRKAPDOS) 0
    set VAR(WRPOLAR)  0

##  set VAR(HAMILTONIAN) "REL"

    set VAR(CPAMODE)  "SINGLE SITE"
    set VAR(WRCPA)    0
    set VAR(CPANITER) 20
    set VAR(CPATOL)   0.00001
    set VAR(NLCPA-LVL)    1
    set VAR(NLCPA-WRCFG)  0
    set VAR0(CPAMODE)     $VAR(CPAMODE)
    set VAR0(CPATOL)      $VAR(CPATOL)
    set VAR0(CPANITER)    $VAR(CPANITER)
    set VAR0(NLCPA-LVL)   $VAR(NLCPA-LVL)
    set VAR0(NLCPA-WRCFG) $VAR(NLCPA-WRCFG)
    
    set VAR(TAUIO) "AUTO"

    set VAR(SCFVXC)    VWN
    set VAR(SCFALG)    BROYDEN2
    set VAR(SCFNITER)  200
    set VAR(SCFTOL)    0.00001
    set VAR(SCFMIX)    0.20
    set VAR(SCFMIXOP)  0.20
    set VAR(SCFISTBRY) 1
    set VAR(SCFITDEPT) 40
    set VAR(SCFSIM)    0.0
    set VAR(BREITINT)  F
    set VAR(ORBPOL)    NONE
    set VAR(BEXT)      0.0
    set VAR(EXTFIELD)  F
    set VAR(BLCOUPL)   F
    set VAR(KMROT)     0
    set VAR(MDIR-x)    0
    set VAR(MDIR-y)    0
    set VAR(MDIR-z)    1
    set VAR(QMVEC-x)   0.0
    set VAR(QMVEC-y)   0.0
    set VAR(QMVEC-z)   0.0
    set VAR(IBAS)      0
    
    set VAR(USENVALT)  0

    #----------------------------------------------- DOS
    set VAR(GRID)      3
    set VAR(NE)       50
    set VAR(EMIN)    -0.2
    set VAR(EMAX)     1.0
    set VAR(ImE)      0.01
    set VAR(EF)       ""
    set VAR(SEARCHEF)   0
    #----------------------------------------------- SCF
    set VAR(GRID)      5
    set VAR(NE)       30
    set VAR(EMIN)    -0.2
    set VAR(EMAX)     ""
    set VAR(ImE)      0.0
    set VAR(EF)       ""
    set VAR(SEARCHEF)   0
    set VAR(VMTZ)     0.0
    #-----------------------------------------------

    set VAR(KKRMODE) STANDARD
    set VAR(IBZINT)  POINTS
    set VAR(NKMIN)     400
    set VAR(NKMAX)     900
    set VAR(NKTAB)     250
    set VAR(NF)          6
    set VAR(STRETA)    0.0
    set VAR(STRRMAX)   2.9
    set VAR(STRGMAX)   3.3
    
    set VAR(CLUALG)      TAUMAT
    set VAR(IQCNTR)      1
    set VAR(ITCNTR)      0
    set VAR(CLU-SPEC)    NSH
    set VAR(CLURAD)      1.5
    set VAR(CLURAD_JXC)      1.5
    set VAR(NLOUT)       3
    
    set VAR(NEPHOT)      1
    set VAR(EPHOTMIN)    100
    set VAR(EPHOTMAX)    100
    set VAR(transition)  core-band
    set VAR(MEPLOT_l)    all
    set VAR(MEPLOT_NONMAG) YES
    set VAR(ReERYDI)     0.5
    set VAR(ImERYDI)     0.01
    set VAR(ReERYDF)     1.8
    set VAR(ImERYDF)     0.01
    
    set VAR(CORE_VB)     VB
    set VAR(VB_l)        d
    
    set VAR(ITXRAY)      1  ; # ITXRAY used throughout for any selected atom type  IT
    set VAR(NCXRAY)      2  ; # NCXRAY used throughout for quantum number n of selected atom type  IT
    set VAR(LCXRAY)      p  ; # LCXRAY used throughout for quantum number l of selected atom type  IT
    
    set VAR(USETAUNN)    0
    set VAR(USETSS)    1
    set VAR(AESNEME)    10
    set VAR(AESNEME-DEF) $VAR(AESNEME)
    
    set VAR(XMONE3)       40
    set VAR(XMONE3-DEF)   $VAR(XMONE3)
    set VAR(XMOEMAX3)     8.0
    set VAR(XMOEMAX3-DEF) $VAR(XMOEMAX3)
    
    set VAR(COMPTON-NPP)     50
    set VAR(COMPTON-PPMAX)  10.0
    set VAR(COMPTON-NPN)     50
    set VAR(COMPTON-PNMAX)   5.0
    set VAR(COMPTON-PNVECx)  0.0
    set VAR(COMPTON-PNVECy)  0.0
    set VAR(COMPTON-PNVECz)  1.0
    
    set VAR(TAUJMIN)     0.0
    set VAR(TAUJPLS)     0.0
    set VAR(RHOME)       NAB
    set VAR(XRAYME)      ADA
    set VAR(TSELECT)     0
    set VAR(IREL)        3
    
    set VAR(FMAG)        0
    
    set VAR(OWRPOT)      0
    set VAR(WROPTS)      0

    set VAR(MATTHEISS)  DEFAULT
    set VAR(USEMSPIN)    0
    set VAR(NOSSITER)    1
    if  {$DIMENSION == "2D" } {
	set VAR(QIONSCL)    0.2 
    } else {
	set VAR(QIONSCL)    1.0
    }

    set VAR(CHIPRINT)    "0"

    set VAR(EMINDISP) "-0.1"
    set VAR(EMAXDISP) "1.0"
    set VAR(NEDISP)   "100"
    set VAR(NKDISP)   "51"
    set VAR(KA1x)     "0.0"
    set VAR(KA1y)     "0.0"
    set VAR(KA1z)     "0.0"
    set VAR(KE1x)     "1.0"
    set VAR(KE1y)     "0.0"
    set VAR(KE1z)     "0.0"


    set VAR(MODEBSF) "1"
    set VAR(NKBSF)   "51"
    set VAR(KA1x)     "0.0"
    set VAR(KA1y)     "0.0"
    set VAR(KA1z)     "0.0"
    set VAR(KE1x)     "1.0"
    set VAR(KE1y)     "0.0"
    set VAR(KE1z)     "0.0"
    set VAR(NK1BSF)   "60"
    set VAR(NK2BSF)   "60"
    set VAR(KBSF1x)     "1.0"
    set VAR(KBSF1y)     "0.0"
    set VAR(KBSF1z)     "0.0"
    set VAR(KBSF2x)     "0.0"
    set VAR(KBSF2y)     "1.0"
    set VAR(KBSF2z)     "0.0"

    set VAR(TASK)        SCF

    set VAR(SCL_ALAT)          0
    set VAR(SCL_ALAT-SCL1)  0.95
    set VAR(SCL_ALAT-SCL2)  1.05
    set VAR(SCL_ALAT-NSCL)     9

    set VAR(plot1D-POT)   0
    set VAR(plot1D-RHO)   0
    set VAR(plot1D-SFN)   0

    set VAR(plot2D-POT) 0
    set VAR(plot2D-RHO) 0
    set VAR(plot2D-NA)  50
    set VAR(plot2D-NB)  50
    set VAR(plot2D-AVECx)   1
    set VAR(plot2D-AVECy)   0
    set VAR(plot2D-AVECz)   0
    set VAR(plot2D-BVECx)   0
    set VAR(plot2D-BVECy)   1
    set VAR(plot2D-BVECz)   0
    set VAR(plot2D-CVECx)   0
    set VAR(plot2D-CVECy)   0
    set VAR(plot2D-CVECz)   0

    set VAR(FULLPOT)    0
    set VAR(SPHERCELL)  0
    set VAR(SEMICORE)   0
    set VAR(LLOYD)      0

    set VAR(POTFMT)     "6"
   
}
# following VAR() depend on other global vars, so init/update always

set NL 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    if {$NL< $NLQ($IQ)} { set NL $NLQ($IQ) }
}
set VAR(NLUSER)  $NL


for {set IT 1} {$IT <= $NT} {incr IT} {
    set NVALT_DEFAULT($IT) [expr $ZT($IT) - [lindex $TABNCORE $ZT($IT)] ]
    debug $PRC "IT $IT   Z $ZT($IT)  NVAL(def) $NVALT_DEFAULT($IT)"
    
    for {set IL 1} {$IL <= $NL} {incr IL} {
	set INDNTL "nef$IT{_}$IL"
	set VAR($INDNTL) 0.0
    }
    
    set VAR(Brooks-OP-$IT) -
    set VAR(UHub-$IT) "0.0"
    set VAR(JHub-$IT) "0.0"
    set VAR(EHub-$IT) "0.0"
    set VAR(LDA+U-$IT) "NO"
    set LOP 3
    for {set ml -$LOP} {$ml <= $LOP} {incr ml} {
	set VAR(LDA+U-$IT-$ml-DN) 0.0
	set VAR(LDA+U-$IT-$ml-UP) 0.0
    }
}

set VAR(KPATH) 1
switch $BRAVAIS {
    {0}  {# unknown
    }
    {1}  {# triclinic   primitive      -1     C_i
    }
    {2}  {# monoclinic  primitive      2/m    C_2h
    }							
    {3}  {# monoclinic  base-centered  2/m    C_2h		
    }							
    {4}  {# orthorombic primitive      mmm    D_2h 		
	set VAR(KPATH) 4					
    }							
    {5}  {# orthorombic base-centered  mmm    D_2h		
    }							
    {6}  {# orthorombic body-centered  mmm    D_2h		
    }							
    {7}  {# orthorombic face-centered  mmm    D_2h		
    }							
    {8}  {# tetragonal  primitive      4/mmm  D_4h		
    }							
    {9}  {# tetragonal  body-centered  4/mmm  D_4h		
    }							
    {10} {# trigonal (rhombohedral)   primitive      -3m    D_3d
    }
    {11} {# hexagonal   primitive      6/mmm  D_6h
	set VAR(KPATH) 4
    }
    {12} {# cubic       primitive      m3m    O_h
	set VAR(KPATH) 4
    }
    {13} {# cubic       face-centered  m3m    O_h
	set VAR(KPATH) 4
    }
    {14} {# cubic       body-centered  m3m    O_h
	set VAR(KPATH) 5
    }
}

regsub "$inpsuffix\$" $inpfile "" dataset
set VAR(DATASET) $dataset
#   $CLUSTER_TYPE == "NONE" 
if {$DIMENSION != "0D" } {
   set VAR(POTFIL)  "$dataset.pot"
   set potfile $VAR(POTFIL)
} else {
   set VAR(POTFIL)      "${sysfile_host}.pot"
   set VAR(POTFIL_CLU)  "$dataset.pot"
   set potfile $VAR(POTFIL_CLU)

   if {![file exists $VAR(POTFIL)]} {
      writescrd .d.tt $PRC "\n\nINFO from <tutorial_SPR-KKR>: \
                           \n\n default host potential file \
                           \n\n $VAR(POTFIL) \
                           \n\n does not exist  \
                           \n\n copy or rename file \n"
      give_warning "." "WARNING \n\n INFO from <tutorial_SPR-KKR>: \
                           \n\n default host potential file \
                           \n\n $VAR(POTFIL) \
                           \n\n does not exist  \
                           \n\n copy or rename file \n "
   }
}

set VAR(ADSI)    $VAR(TASK)

set VAR(TUT-TASK)   TUT

regsub "$syssuffix\$" $sysfile "" inpfile
set inpfile "$inpfile\_$VAR(TUT-TASK)$inpsuffix"





#****************************************************************************************

     set KKRSCF kkrscf6.1.4
     set KKRGEN kkrscf6.1.4

puts "XXXXXXXXXXXXXX KKRSCF  $KKRSCF"
puts "XXXXXXXXXXXXXX KKRGEN  $KKRGEN"
puts "XXXXXXXXXXXXXX potfile ${potfile}"

     if {[file exists ${potfile}_new]==1} {
         set pot_new "${potfile}_new"
         eval set res [catch "exec mv ${potfile}     ${potfile}_sav" message]
         eval set res [catch "exec mv ${potfile}_new ${potfile}"     message]
         puts "XXXXXXXXXXXXXX NEW potential file  ${potfile}_new  moved \n\n"
         set VAR(TASK)        TUTORIAL
     }

     if {[file exists ${potfile}]==1} {
         set pot_new ""
         puts "XXXXXXXXXXXXXX potential file  ${potfile} available \n\n"
         set VAR(TASK)        TUTORIAL
     }

#****************************************************************************************











#=======================================================================================
#                             MAIN INPUT WINDOW
#
#                        supply 3 colums to be filled in
#=======================================================================================
pack configure $wcA $wcB  \
               -in $w.cols -side left -fill both -expand y

#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN B
#=======================================================================================
#
set wtut 30

CreateRADIOBUTTON $wcB $BGTSK $wtut "spherical harmonics             " VAR TUT-TASK aexec $fe s 1  YLM
CreateRADIOBUTTON $wcB $BGTSK $wtut "spherical functions j and n     " VAR TUT-TASK bexec $fe s 1  jlx
CreateRADIOBUTTON $wcB $BGTSK $wtut "asymptotic behavior of j and n  " VAR TUT-TASK cexec $fe s 1  jlx-asympt
CreateRADIOBUTTON $wcB $BGTSK $wtut "charge and potential            " VAR TUT-TASK dexec $fe s 1  chr-pot
CreateRADIOBUTTON $wcB $BGTSK $wtut "potential - decomposition       " VAR TUT-TASK eexec $fe s 1  pot-decomp
CreateRADIOBUTTON $wcB $BGTSK $wtut "potential - xc parametrisations " VAR TUT-TASK fexec $fe s 1  pot-xc
CreateRADIOBUTTON $wcB $BGTSK $wtut "radial wave functions  (E fixed)" VAR TUT-TASK gexec $fe s 1  RWF-l
CreateRADIOBUTTON $wcB $BGTSK $wtut "radial wave functions  (l fixed)" VAR TUT-TASK hexec $fe s 1  RWF-E
CreateRADIOBUTTON $wcB $BGTSK $wtut "E and (g,f) for all core states " VAR TUT-TASK iexec $fe s 1  core-all


#CreateRADIOBUTTON $wcB $BGTSK $wtut "radial wave functions  (E fixed)" VAR TUT-TASK fexec $fe s 1  RWF-l
#CreateRADIOBUTTON $wcB $BGTSK $wtut "radial wave functions  (l fixed)" VAR TUT-TASK gexec $fe s 1  RWF-E
#CreateRADIOBUTTON $wcB $BGTSK $wtut "phase shift                     " VAR TUT-TASK hexec $fe s 1  phase 
#CreateRADIOBUTTON $wcB $BGTSK $wtut "single site DOS                 " VAR TUT-TASK iexec $fe s 1  SS-DOS





#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 1
#=======================================================================================
#
set tutorial_SPRKKR_wc1 $wc1
CreateENTRY       $wc1 $BGFIL $wl "   DATASET "   $we VAR DATASET  $fe 1 " "
CreateENTRY       $wc1 $BGFIL $wl "   ADSI "      $we VAR ADSI     $fe 1 " "
CreateENTRY       $wc1 $BGFIL $wl "   POTFIL"     $we VAR POTFIL   $fe 1 " "
if {$DIMENSION == "0D" } {
CreateENTRY       $wc1 $BGFIL $wl "   POTFIL-CLU" $we VAR POTFIL_CLU   $fe 1 " "
}

CreateRADIOBUTTON $wc1 $BGTSK $wl "   TASK " VAR TASK aexec $fe s 1  SCF DOS

CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK bexec $fe s 1  WFPLOT PSHIFT SOCPAR
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK cexec $fe s 1  JXC FMAG
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK dexec $fe s 1  EKREL BLOCHSF
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK eexec $fe s 1  MEPLOT MECHECK
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK fexec $fe s 1  VBXPS CLXPS ; #BIS
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK gexec $fe s 1  AES NRAES APS
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK hexec $fe s 1  XAS XMO ; #XRS XES
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK iexec $fe s 1  COMPTON POSANI ; #T1 RHO
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK jexec $fe s 1  CHI CHIXAS SIGMA MOKE

set wee 17
CreateRADIOBUTTON $wc1 $BGE   $wl "   E-GRID "   VAR GRID     a $fe n 1 REAL RECTANG STRAIGHT
CreateRADIOBUTTON $wc1 $BGE   $wl "        "     VAR GRID     b $fe n 4 RECT-LOG ARC XAS XMO
CreateSCALE       $wc1 $BGE   $wl "   NE  "      $ws 0        250   0 3 1        VAR NE  $fe
CreateENTRY       $wc1 $BGE   $wl "   EMIN "     $wee VAR EMIN   $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   EMAX "     $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   EF   "     $wee VAR EF     $fe 3 "Ry"
    CreateCHECKBUTTON $wc1 $BGE   $wl " " VAR SEARCHEF    "search EF"
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

#---------------------------------------------------------- dummy label to fill space
if {$NCPA ==0} {
  label $wc1.col1_bot    -text "" -bg $BGE
  pack configure $wc1.col1_bot -in $wc1 -fill both -expand y
}
   if {$NCPA !=0} {
   CreateSCALE       $wc1 $BGCPA $wl "   CPANITER " $ws 0        30   0 2  1      VAR CPANITER $fe
   CreateSCALE       $wc1 $BGCPA $wl "   CPATOL "   $ws 0.00001 0.001 0 3 0.00001 VAR CPATOL   $fe
   CreateRADIOBUTTON $wc1 $BGCPA $wl "   CPA MODE"   VAR CPAMODE  a $fe s 1  "SINGLE SITE" "NON LOCAL"
   CreateSCALE       $wc1 $BGCPA $wl "   NL-LEVEL" $ws 1         2   0 1  1      VAR NLCPA-LVL $fe
   CreateCHECKBUTTON $wc1 $BGCPA $wl "   NL-WRITE"    VAR NLCPA-WRCFG    "CONFIG "
   }

#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 2
#=======================================================================================
#
set tutorial_SPRKKR_wc2 $wc2

    CreateRADIOBUTTON $wc2 $BGKKR $wl "   KKRMODE "    VAR KKRMODE    a     $fe s 2  STANDARD TB CLUSTER  
    label $wc2.bz-parameter -text "   parameters for BZ-integration" -bg $BGK -anchor w
    pack  $wc2.bz-parameter -anchor w -fill x

    CreateRADIOBUTTON $wc2 $BGK   $wl "   TAU CALC"    VAR IBZINT     aexec $fe s 1  WEYL POINTS TET ;##ZOOM


CreateSCALE       $wc2 $BGK   $wl "   W NKMIN" $ws 0       4000   0 4  50  VAR NKMIN $fe
CreateSCALE       $wc2 $BGK   $wl "   W NKMAX" $ws 0       4000   0 4  50  VAR NKMAX $fe
CreateSCALE       $wc2 $BGK   $wl "   P NKTAB" $ws 0       4000   0 4  50  VAR NKTAB $fe
CreateSCALE       $wc2 $BGK   $wl "   T/Z  NF" $ws 1         20   0 2 1    VAR NF      $fe
label $wc2.lab_KKRSTRCONST -text "   KKR structure constants calculation" -anchor w -bg $BGK
pack  $wc2.lab_KKRSTRCONST -fill x
CreateSCALE       $wc2 $BGK   $wl "   ETA   "  $ws 0.0      1.0   0 3 0.05 VAR STRETA  $fe
CreateSCALE       $wc2 $BGK   $wl "   RMAX  "  $ws 0.0      9.9   0 3 0.05 VAR STRRMAX $fe
CreateSCALE       $wc2 $BGK   $wl "   GMAX  "  $ws 0.0      9.9   0 3 0.05 VAR STRGMAX $fe

label $wc2.cluster-set-up -text "   parameters for cluster set-up" -bg $BGCLU -anchor w
pack  $wc2.cluster-set-up -anchor w -fill x

CreateSCALE       $wc2 $BGCLU $wl "   IQCNTR"  $ws 0        $NQ   0 [PREC_INT $NQ] 1    VAR    IQCNTR  $fe
frame $wc2.clu1   -bg $BGCLU ; pack $wc2.clu1 -fill both -expand y
frame $wc2.clu1.a -bg $BGCLU ; frame $wc2.clu1.b -bg $BGCLU
 pack $wc2.clu1.a $wc2.clu1.b -fill both -expand y -side left
CreateSCALE       $wc2.clu1.a $BGCLU $wl "   NSHLCLU" $ws1 0         30   0 2 1    VAR    NSHLCLU $fe
CreateRADIOBUTTON $wc2.clu1.b $BGCLU 1 " "    VAR CLU-SPEC  a $fe s 1 NSH
frame $wc2.clu2   -bg $BGCLU ; pack $wc2.clu2 -fill both -expand y
frame $wc2.clu2.a -bg $BGCLU; frame $wc2.clu2.b  -bg $BGCLU
 pack $wc2.clu2.a $wc2.clu2.b -fill both -expand y -side left
CreateSCALE       $wc2.clu2.a $BGCLU $wl "   CLURAD"  $ws1 0         20   0 3 0.1  VAR    CLURAD  $fe
CreateRADIOBUTTON $wc2.clu2.b $BGCLU 1 " "    VAR CLU-SPEC  b $fe s 2 RAD

CreateSCALE       $wc2 $BGCLU $wl "   NLOUT"   $ws 0          6   0 1 1    VAR    NLOUT   $fe

widget_disable  $wc2.wVARNKMIN.2
widget_disable  $wc2.wVARNKMAX.2
#DEFAULT! widget_disable  $wc2.wVARNKTAB.2
widget_disable  $wc2.wVARNF.2
widget_disable  $wc2.wVARIQCNTR.2
widget_disable  $wc2.wVARITCNTR.2
widget_disable  $wc2.wVARNSHLCLU.2
widget_disable  $wc2.wVARCLURAD.2
widget_disable  $wc2.wVARNLOUT.2

   label $wc2.lex -text "   angular momentum expansion NL = l_max + 1"  -bg $BGLEX -anchor w
   pack $wc2.lex -anchor w -fill x
   CreateSCALE       $wc2 $BGLEX $wl "   NL         " $ws 1  12   0 2  1      VAR NLUSER $fe

   label $wc2.col2_bot    -text "" -bg $BGLEX
   pack configure $wc2.col2_bot -in $wc2 -fill both -expand y


#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 3
#=======================================================================================
#
set tutorial_SPRKKR_wc3 $wc3

#
   set wlsmall 1

   frame $wc3.crl1    -bg $BGCRL ; pack $wc3.crl1 -fill x
   frame $wc3.crl1.a  -bg $BGCRL
   frame $wc3.crl1.b  -bg $BGCRL
   pack  $wc3.crl1.a  $wc3.crl1.b -side left -anchor n -expand y -fill x
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      "   CONTROL" VAR SEMICORE  "SEMICORE"
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      " "          VAR FULLPOT   "FULLPOT "
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      " "          VAR NONMAG    "NONMAG    "
   CreateCHECKBUTTON $wc3.crl1.b $BGCRL  $wlsmall " "          VAR LLOYD     "LLOYD"
   CreateCHECKBUTTON $wc3.crl1.b $BGCRL  $wlsmall " "          VAR SPHERCELL "SPHERCELL"
 
   CreateRADIOBUTTON $wc3 $BGMOD $wl "   Hamiltonian" VAR HAMILTONIAN a1     $fe s 0  NREL SREL SP-SREL
   CreateRADIOBUTTON $wc3 $BGMOD $wl "             "  VAR HAMILTONIAN a2exec $fe s 0  REL  Brooks-OP LDA+U
   CreateRADIOBUTTON $wc3 $BGMOD $wl "   manipulate"  VAR HAMILTONIAN b1exec $fe s 4  scale-c scale-SOC
   CreateRADIOBUTTON $wc3 $BGMOD $wl "       SOC   "  VAR HAMILTONIAN b2exec $fe s 4  SOC-xy SOC-zz

   set VAR(KMROT) "0"
   CreateRADIOBUTTON $wc3 $BGMAG $wl "  orientation"  VAR KMROT aexec $fe n 0 z-axis "common orientation"
   CreateRADIOBUTTON $wc3 $BGMAG $wl "  of magnet. "  VAR KMROT bexec $fe n 2 "non-collinear"
   CreateRADIOBUTTON $wc3 $BGMAG $wl "             "  VAR KMROT cexec $fe n 3 "orthogonal spin-spiral" "spin-spiral"

   CreateRADIOBUTTON $wc3 $BGP $wl "   PRINT   "     VAR IPRINT   a $fe n 0  0 1 2 3 4 5
   CreateRADIOBUTTON $wc3 $BGP $wl "   TEST "        VAR ITEST    a $fe n 0  0 1 2 3 4

   frame $wc3.out1    -bg $BGWR ; pack $wc3.out1 -fill x
   frame $wc3.out1.a  -bg $BGWR
   frame $wc3.out1.b  -bg $BGWR
   pack  $wc3.out1.a  $wc3.out1.b -side left -expand y -fill x
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      "   OUTPUT" VAR NOWRDOS  "NOWRDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRLMLDOS "WRLMLDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRKAPDOS "WRKAPDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRPOLAR  "WRPOLAR"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR NOHFF    "NOHFF"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR NOCORE   "NOCORE"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR FSOHFF   "FSOHFF"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR EFG      "EFG"
   if {$NCPA !=0} {
      CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl   "  "        VAR WRCPA    "WRCPA"
   }


   frame $wc3.plot    -bg $BGWR ; pack $wc3.plot -fill x
   frame $wc3.plot.a  -bg $BGWR
   frame $wc3.plot.b  -bg $BGWR
   pack  $wc3.plot.a  $wc3.plot.b -side left -expand y -fill x
   CreateCHECKBUTTON $wc3.plot.a $BGWR  $wl      "   PLOT  " VAR plot-POT "potential"
   CreateCHECKBUTTON $wc3.plot.b $BGWR  $wlsmall " "         VAR plot-RHO "density"

   frame $wc3.plotSFN    -bg $BGWR ; pack $wc3.plotSFN -fill x
   frame $wc3.plotSFN.a  -bg $BGWR
   frame $wc3.plotSFN.b  -bg $BGWR
   pack  $wc3.plotSFN.a  $wc3.plotSFN.b -side left -expand y -fill x
   CreateCHECKBUTTON $wc3.plotSFN.a $BGWR  $wl      " "         VAR plot-SFN "shape function"

   frame $wc3.plot3D    -bg $BGWR ; pack $wc3.plot3D -fill x
   frame $wc3.plot3D.a  -bg $BGWR
   frame $wc3.plot3D.b  -bg $BGWR
   pack  $wc3.plot3D.a  $wc3.plot3D.b -side left -expand y -fill x
   CreateCHECKBUTTON $wc3.plot3D.a $BGWR  $wl      "   PLOT 3D" VAR plot3D-V   "potential"
   CreateCHECKBUTTON $wc3.plot3D.b $BGWR  $wlsmall " "          VAR plot3D-RHO "density"


   set VAR(TAUIO) "AUTO"
   frame $wc3.tauio    -bg $BGTAU ; pack $wc3.tauio -fill x
   CreateRADIOBUTTON $wc3.tauio   $BGTAU $wl "   TAU-MAT"    VAR TAUIO a $fe s 0 "AUTO"

   frame $wc3.tauio.a  -bg $BGTAU
   frame $wc3.tauio.b  -bg $BGTAU
   pack  $wc3.tauio.a  $wc3.tauio.b -side left -expand y -fill x

   CreateRADIOBUTTON $wc3.tauio.a $BGTAU $wl      "   WRITE"    VAR TAUIO d $fe s 0 "WRTAUMQ"
   CreateRADIOBUTTON $wc3.tauio.a $BGTAU $wl      "   READ "    VAR TAUIO e $fe s 0 "RDTAUMQ"
   CreateRADIOBUTTON $wc3.tauio.b $BGTAU $wlsmall " "           VAR TAUIO b $fe s 0 "WRTAU"
   CreateRADIOBUTTON $wc3.tauio.b $BGTAU $wlsmall " "           VAR TAUIO c $fe s 0 "RDTAU"

   CreateCHECKBUTTON $wc3 tomato $wl "   POTFILE" VAR OWRPOT   "OVERWRITE"
   widget_disable     $wc3.wVAROWRPOT.2
   CreateRADIOBUTTON $wc3 tomato $wl "   POTFMT   "     VAR POTFMT   a $fe n 5  5 6

   frame  $wc3.col3_bot

pack  $wc3.col3_bot -expand y -fill both -anchor c


#--------------------------------------------------------------------------------



########################################################################################
#                                                 exec_RADIOBUTTON_cmd  for SPR-KKR
proc exec_RADIOBUTTON_cmd {KEY} {
global VAR NT TXTT CONC ZT NL BRAVAIS NCPA NQ
global BGCPA BGSCF BGTSK BGTAU BGWR BGCLU BGP BGK BGE BGX BGMAG
global BGCRL BGMOD NVALT_DEFAULT NL
global tutorial_SPRKKR_wc1
global tutorial_SPRKKR_wc2
global tutorial_SPRKKR_wc3
global inpfile inpsuffix sysfile syssuffix
global COLOR WIDTH HEIGHT FONT
global TABCQNTOP_N

set ws1 100


puts "******************************exec_RADIOBUTTON_cmd KEY      $KEY"
puts "******************************exec_RADIOBUTTON_cmd VAR      $VAR(TUT-TASK)"
puts "******************************exec_RADIOBUTTON_cmd NL       $NL"


#--------------------------------------------------------------------------------
#                             fixed photon energy
#
if {$KEY == "EPHOT-fixed" } {
    if {$VAR(EPHOT-fixed) == "He I" } {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 20.1; set VAR(EPHOTMAX) 20.1}
    if {$VAR(EPHOT-fixed) == "He II"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 48.1; set VAR(EPHOTMAX) 48.1}
    if {$VAR(EPHOT-fixed) == "Al Ka"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 1111; set VAR(EPHOTMAX) 1111}
    if {$VAR(EPHOT-fixed) == "Mg Ka"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 1311; set VAR(EPHOTMAX) 1311}
    return
}
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
#                              check NCXRAY and LCXRAY 
#          restrict quantum number n to be compatible with atomic number Z
#          restrict quantum number l to hold  l < n
#
if {$KEY == "NCXRAY" || $KEY == "LCXRAY" } {
    set CQNTOP_N [lindex $TABCQNTOP_N $ZT($VAR(ITXRAY))]
    if {$VAR(NCXRAY) > $CQNTOP_N} {
       set VAR(NCXRAY) $CQNTOP_N
    }

    set I 0
    foreach X [list s p d f] { incr I ; set IL($X) $I }
    if {$IL($VAR(LCXRAY)) > $VAR(NCXRAY)} {
       foreach X [list s p d f] { if {$IL($X)<=$VAR(NCXRAY)} {set VAR(LCXRAY) $X}  }
    }
    return
}
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
#                              check the TAU-calculation mode scales
if {$KEY == "IBZINT"} {

widget_disable  $tutorial_SPRKKR_wc2.wVARNKMIN.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNKMAX.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNKTAB.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNKTAB.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNF.2
widget_disable  $tutorial_SPRKKR_wc2.wVARIQCNTR.2
widget_disable  $tutorial_SPRKKR_wc2.wVARITCNTR.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNSHLCLU.2
widget_disable  $tutorial_SPRKKR_wc2.wVARCLURAD.2
widget_disable  $tutorial_SPRKKR_wc2.wVARNLOUT.2

widget_enable  $tutorial_SPRKKR_wc2.wVARSTRETA.2
widget_enable  $tutorial_SPRKKR_wc2.wVARSTRRMAX.2
widget_enable  $tutorial_SPRKKR_wc2.wVARSTRGMAX.2

set CASE $VAR(IBZINT)

if {$CASE == "WEYL"} {
    widget_enable $tutorial_SPRKKR_wc2.wVARNKMIN.2
    widget_enable $tutorial_SPRKKR_wc2.wVARNKMAX.2
    return			
} elseif {$CASE == "POINTS"} {	
    widget_enable $tutorial_SPRKKR_wc2.wVARNKTAB.2
    return			
} elseif {$CASE == "TET"} {	
    widget_enable $tutorial_SPRKKR_wc2.wVARNF.2
    return			
} elseif {$CASE == "ZOOM"} {	
    widget_enable $tutorial_SPRKKR_wc2.wVARNF.2
    return			
} elseif {$CASE == "CLUSTER"} {	
    widget_disable  $tutorial_SPRKKR_wc2.wVARSTRETA.2
    widget_disable  $tutorial_SPRKKR_wc2.wVARSTRRMAX.2
    widget_disable  $tutorial_SPRKKR_wc2.wVARSTRGMAX.2

    widget_enable $tutorial_SPRKKR_wc2.wVARIQCNTR.2
    widget_enable $tutorial_SPRKKR_wc2.wVARITCNTR.2
    widget_enable $tutorial_SPRKKR_wc2.wVARNSHLCLU.2
    widget_enable $tutorial_SPRKKR_wc2.wVARCLURAD.2
    widget_enable $tutorial_SPRKKR_wc2.wVARNLOUT.2
    return
}

}
#--------------------------------------------------------------------------------


    set Wrbcmd .c_rad_but_cmd
    if {[winfo exists $Wrbcmd] == 1} {destroy $Wrbcmd}
    toplevel_init $Wrbcmd "supply information for   $KEY" 0 0
    wm geometry $Wrbcmd +300+200
    wm positionfrom $Wrbcmd user
    wm sizefrom $Wrbcmd ""
    wm minsize $Wrbcmd

    set buttonbgcolor green1
    set buttonheight  2

    set w  $Wrbcmd
    set wl 12
    set we 30
    set ws 160
    set fe $FONT(GEN)
    set wee 17

    label $Wrbcmd.header
    pack configure $Wrbcmd.header -side top -anchor n -pady 14

    frame $Wrbcmd.a
#------------------------------------------------------------------------------------
    frame $Wrbcmd.a.but
#
     button $Wrbcmd.a.but.execute -text "EXECUTE" -width 20 -height $buttonheight \
	-command "exec_RADIOBUTTON_TUTORIAL $Wrbcmd $KEY" -bg green1
     button $Wrbcmd.a.but.close   -text "CLOSE"   -width 20 -height $buttonheight \
	-command "destros $Wrbcmd"                        -bg tomato
#
     pack configure $Wrbcmd.a.but.execute $Wrbcmd.a.but.close \
                   -anchor s -side left -pady 1 -padx 30 -pady 15
#------------------------------------------------------------------------------------
     pack configure $Wrbcmd.a.but -side left -fill x
     pack configure $Wrbcmd.a -side bottom -anchor s
#------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------
#                             execute KKRGEN and display results
#------------------------------------------------------------------------------------
proc exec_RADIOBUTTON_TUTORIAL {w KEY} {

global inpfile outfile PACKAGE wishcall xband_path env VAR system TUT_TEXT

set PROG_PATH  ~/bin/
set PROG       kkrgen
set PROG_VERS  6.1
set PROG_OPTION($PACKAGE) " "
set PROG_DIRECT($PACKAGE) "<"
set outfile  "OUT"
set working_directory [pwd]

set TUTTASK $VAR(TUT-TASK)

create_inpfile_SPR-KKR_quit "ok"

	 set START_DATE [exec date]

	 set   ia [open xband_aux w] 
	 puts  $ia "\# interactive script created by   xband "
	 puts  $ia "\# "

	 # for security reasons LD_LIBRARY_PATH is not transmitted to subshell if 
	 # called via xterm -e, set explicitly 
	 if {[info exists env(LD_LIBRARY_PATH)]} { 
	     puts  $ia "LD_LIBRARY_PATH=$env(LD_LIBRARY_PATH)"
	     puts  $ia "export LD_LIBRARY_PATH"
	 }
	 
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      starting interactive run of $PROG_PATH$PROG$PROG_VERS \" "
	 puts  $ia "echo \"      input file:   $inpfile  \" "
	 puts  $ia "echo \"      output will be appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "cd $working_directory "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    execution via xband:    $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) \"  >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "$PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) | tee -a  $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    execution via xband:    $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE)  \"  >> $outfile "
	 puts  $ia "echo \"    run started at: $START_DATE  \"      >> $outfile "
	 puts  $ia "echo \"    and ended   at: `date`       \"      >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      interactive run  \" "
	 puts  $ia "echo \"      program call:\" "
	 puts  $ia "echo \"      $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) | tee -a $outfile\" "
	 puts  $ia "echo \"      input file:          $inpfile  \" "
	 puts  $ia "echo \"      output appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "

   if { $TUTTASK == "YLM" } {
      puts  $ia "gnuplot TUT1_YLM.gnp "
      puts  $ia "exit "      
   } elseif { $TUTTASK == "jlx" } {
      puts  $ia "xmgrace TUT1_jlx_sphfun.agr  "      
      puts  $ia "exit "      
   } elseif { $TUTTASK == "jlx-asympt" } {
      puts  $ia "xmgrace TUT1_jlx-asympt_sphfun.agr  "      
      puts  $ia "exit "      
   } elseif { $TUTTASK == "chr-pot" } {
      puts  $ia "xmgrace TUT1_V.agr & "      
      puts  $ia "xmgrace TUT1_RHO.agr  "
      puts  $ia "exit "      
   } elseif { $TUTTASK == "pot-decomp" } {
      puts  $ia "xmgrace TUT1_Vdecomp.agr "
      puts  $ia "exit "      
   } elseif { $TUTTASK == "pot-xc" } {
      puts  $ia "xmgrace TUT1_Vxc.agr"
      puts  $ia "exit "      
   } elseif { $TUTTASK == "RWF-l" } {
      puts  $ia "xmgrace TUT1_RWF-l_max.agr"
      puts  $ia "exit "      
   } elseif { $TUTTASK == "RWF-E" } {
      puts  $ia "xmgrace TUT1_RWF-E_max.agr"
      puts  $ia "exit "      
   } elseif { $TUTTASK == "core-all" } {
      puts  $ia "xmgrace TUT1_core-all.agr"
      puts  $ia "exit "      
   }


	 puts  $ia "$wishcall -f $xband_path/message.tcl -n message 1 \$* & "
	 puts  $ia "exec bash "
	 close $ia
	 writescr0 .d.tt  "\nINFO from <run_prog> \n\nrunning $PROG_PATH$PROG$PROG_VERS interactively \
		 \noutput will be appended to   $outfile " 
	 exec chmod 755 xband_aux
	 eval set res [catch "exec xterm -T \"run $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) \" \
		 -geometry 82x40+100+100 -bg yellow -fg black -e ./xband_aux  &" message]
	 

#-------------------------------------------
########  destroy $w
}
#------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------
#                     supply column to be filled in
#--------------------------------------------------------------------------------
frame $w.cols
pack configure $w.cols -in $w -anchor w -side top -fill x -expand y

frame $w.cols.col1 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col2 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col3 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col4 -bg $BGP -relief raised -borderwidth 2

pack configure $w.cols.col1 -in $w.cols -side left -fill both -expand y
#pack configure $w.cols.col2 -in $w.cols -side left -fill both -expand y
#pack configure $w.cols.col3 -in $w.cols -side left -fill both -expand y
#pack configure $w.cols.col4 -in $w.cols -side left -fill both -expand y

set wc1 $w.cols.col1 ; set wc2 $w.cols.col2
set wc3 $w.cols.col3 ; set wc4 $w.cols.col4

#
#------------------------------- to be used for TASK EKREL and BLOCHSF
#
    set KPLAB(1) "   " ;  set KPLAB(2) "   " ;  set KPLAB(3) "   "
    set KPLAB(4) "   " ;  set KPLAB(5) "   "
	
    switch $BRAVAIS {
        {0}  {# unknown
              set KPATHTOP 0
             }
        {1}  {# triclinic   primitive      -1     C_i
              set KPATHTOP 0
             }
        {2}  {# monoclinic  primitive      2/m    C_2h
              set KPATHTOP 0
	     }											
        {3}  {# monoclinic  base-centered  2/m    C_2h
              set KPATHTOP 0
              }											
        {4}  {# orthorombic primitive      mmm    D_2h 	
              set KPATHTOP 4
              set KPLAB(1) "G-Y-T-Z-G-Y-T-Z + X-S-Y + U-R-T + S-T"
              set KPLAB(2) "G-Y-T-Z-G-Y-T-Z"
              set KPLAB(3) "G-Y-T-Z-G"
              set KPLAB(4) "G-Y-T-Z"
              }											
        {5}  {# orthorombic base-centered  mmm    D_2h
              set KPATHTOP 0
              }											
        {6}  {# orthorombic body-centered  mmm    D_2h
              set KPATHTOP 0
              }											
        {7}  {# orthorombic face-centered  mmm    D_2h	
              set KPATHTOP 0
              }											
        {8}  {# tetragonal  primitive      4/mmm  D_4h
              set KPATHTOP 0
              }											
        {9}  {# tetragonal  body-centered  4/mmm  D_4h
              set KPATHTOP 0
              }											
        {10} {# trigonal (rhombohedral)   primitive      -3m    D_3d
              set KPATHTOP 0
              }
        {11} {# hexagonal   primitive      6/mmm  D_6h
              set KPATHTOP 5
              set KPLAB(1) "G-M-K-G-A-L-H-A + M-L + K-H"
              set KPLAB(2) "G-M-K-G-A-L-H-A"
              set KPLAB(3) "G-M-K-G-A"
              set KPLAB(4) "G-M"
              set KPLAB(5) "K-G"
             }
        {12} {# cubic       primitive      m3m    O_h
              set KPATHTOP 5
              set KPLAB(1) "G-X-M-R-G-M"
              set KPLAB(2) "G-X-M-R-G"
              set KPLAB(3) "G-X-M-R"
              set KPLAB(4) "G-X-M"
              set KPLAB(5) "G-X + G-Y + G-Z"
            }
        {13} {# cubic       face-centered  m3m    O_h
              set KPATHTOP 6
              set KPLAB(1) "X-G-L-W-K-G + L-U-X-W-U"
              set KPLAB(2) "X-G-L-W-K-G" ;
              set KPLAB(3) "X-G-L" ; set KPLAB(4) "G-X" ;  set KPLAB(5) "G-L"
              set KPLAB(6) "G-X + G-Y + G-Z"
              }
        {14} {# cubic       body-centered  m3m    O_h
              set KPATHTOP 6
              set KPLAB(1) "G-H-N-G-P-H + N-P"
              set KPLAB(2) "G-H-N-G-P-H" ; set KPLAB(3) "G-H-N-G-P"
              set KPLAB(4) "G-H-N-G"     ; set KPLAB(5) "G-H"
              set KPLAB(6) "G-X + G-Y + G-Z"
              }
    }
#
#--------------------------------------------------------------------------------
#                             supply additional information for HAMILTONIAN
if {$KEY == "HAMILTONIAN"} {
#
#--------------------------------------- deactivate columns 2,3,4
#
pack forget $w.cols.col2
pack forget $w.cols.col3
pack forget $w.cols.col4

if { $VAR(HAMILTONIAN) == "Brooks-OP" || $VAR(HAMILTONIAN) == "LDA+U" } {
   $Wrbcmd.header configure -text "modified relativistic Hamiltonian   "
} else {
   $Wrbcmd.header configure -text "manipulation of the spin-orbit coupling "
}

#
#------------------------------------------------------------------------ standard REL
if { $VAR(HAMILTONIAN) == "REL" } {
   destros $Wrbcmd
   return
#
#------------------------------------------------------------------------ Brooks-OP
} elseif { $VAR(HAMILTONIAN) == "Brooks-OP" } {

label $wc1.headtxt  -text "add Brooks orbital polarisation (OP) term"  \
                    -anchor n  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x

for {set IT 1} {$IT <= $NT} {incr IT} {
   if {[is_transition_metal $ZT($IT)]} {
      set VAR(Brooks-OP-$IT) "d"
      CreateRADIOBUTTON $wc1 $BGMOD $wl "   $TXTT($IT)" VAR Brooks-OP-$IT a $fe s 0  - d
   } elseif { [is_f_element $ZT($IT)]} {
      set VAR(Brooks-OP-$IT) "f"
      CreateRADIOBUTTON $wc1 $BGMOD $wl "   $TXTT($IT)" VAR Brooks-OP-$IT a $fe s 0  - f
   } else {
      set VAR(Brooks-OP-$IT) "-"
   }
}
#
#------------------------------------------------------------------------ LDA+U
} elseif { $VAR(HAMILTONIAN) == "LDA+U"    } {

label $wc1.headtxt  -text "add LDA+U-term to the Hamiltonian"  \
                     -anchor n -padx 30 -bg $BGMOD

frame $wc1.head_dc -bg $BGMOD
label $wc1.head_dc.lab1 -text "double counting mode" -bg $BGMOD
radiobutton $wc1.head_dc.amf -variable VAR(LDA+U-DC) -text "AMF" -value "MF" \
            -bg $BGMOD -highlightthickness 0  
radiobutton $wc1.head_dc.aal -variable VAR(LDA+U-DC) -text "AAL" -value "AL" \
            -bg $BGMOD -highlightthickness 0  
pack configure $wc1.head_dc.lab1 $wc1.head_dc.amf $wc1.head_dc.aal \
     -in $wc1.head_dc -anchor nw -side left -fill x
$wc1.head_dc.aal select

pack configure $wc1.headtxt $wc1.head_dc -in $wc1 -anchor nw -side top -fill x

for {set IT 1} {$IT <= $NT} {incr IT} {
   if {[is_transition_metal $ZT($IT)] ||  [is_f_element $ZT($IT)]} {

      frame $wc1.frame$IT -bg $BGMOD
      set FRIT $wc1.frame$IT
      label $FRIT.lab1 -text " $TXTT($IT)" -width 12 -bg $BGMOD
      radiobutton $FRIT.rad1 -variable VAR(LDA+U-$IT) -text "NO" -value "NO" \
                  -bg $BGMOD -highlightthickness 0  \
                  -command "cinp_SPR-KKR_LDA_U $BGMOD $IT \-"

      pack  $FRIT.lab1 $FRIT.rad1 -side left

      if {[is_transition_metal $ZT($IT)] } {

         radiobutton $FRIT.rad2 -variable VAR(LDA+U-$IT) -text "d" -value "d" \
                     -bg $BGMOD -highlightthickness 0  \
                      -command "cinp_SPR-KKR_LDA_U $BGMOD $IT d"

         pack  $FRIT.rad2 -side left

      } else {

         radiobutton $FRIT.rad3 -variable VAR(LDA+U-$IT) -text "f" -value "f" \
                      -bg $BGMOD -highlightthickness 0  \
                      -command "cinp_SPR-KKR_LDA_U $BGMOD $IT f"

         pack  $FRIT.rad3 -side left
      }

      label $FRIT.lab2U -text "         U = " -width 6 -bg $BGMOD
      label $FRIT.lab3U -textvariable VAR(UHub-$IT) -width 4 -bg $BGMOD
      label $FRIT.lab4U -text " eV" -width 2 -bg $BGMOD
      pack  $FRIT.lab2U $FRIT.lab3U $FRIT.lab4U  -side left
      label $FRIT.lab2J -text "         J = " -width 6 -bg $BGMOD
      label $FRIT.lab3J -textvariable VAR(JHub-$IT) -width 4 -bg $BGMOD
      label $FRIT.lab4J -text " eV" -width 2 -bg $BGMOD
      pack  $FRIT.lab2J $FRIT.lab3J $FRIT.lab4J  -side left
      label $FRIT.lab2E -text "         E = " -width 6 -bg $BGMOD
      label $FRIT.lab3E -textvariable VAR(EHub-$IT) -width 4 -bg $BGMOD
      label $FRIT.lab4E -text " Ry" -width 2 -bg $BGMOD
      pack  $FRIT.lab2E $FRIT.lab3E $FRIT.lab4E  -side left
      label $FRIT.space -text "   " -width 4 -bg $BGMOD
      pack  $FRIT.space  -side left
      pack  $wc1.frame$IT -fill x
   }
}

#--------------------------------------------------------------- proc cinp_SPR-KKR_LDA_U
proc cinp_SPR-KKR_LDA_U {BG IT KEY} {
    global VAR TXTT
    global COLOR

    set w .cinp_LDA_U
    if {[winfo exists $w] == 1} {destroy $w}
    toplevel_init $w "supply information for  LDA+U  calculation" 0 0
    wm geometry $w +300+200
    wm positionfrom $w user
    wm sizefrom $w ""
    wm minsize $w

    set buttonbgcolor green1
    set buttonheight  2

    set wl 12
    set we 30
    set ws 160
    set wee 17

    if {$KEY =="-"} {
       set VAR(LDA+U-$IT) "NO"
       destros $w
       return
    } elseif {$KEY =="d"} {
       set LOP 2
       set VAR(EHub-$IT) "0.70"
    } else {
       set LOP 3
       set VAR(EHub-$IT) "0.42"
    }

    label $w.header -text "supply information for atom type  $IT  $TXTT($IT)" \
             -bg $BG

    pack configure $w.header -side top -anchor n -fill both

#----------------------------------
label $w.space0U  -text " "  -bg $BG
label $w.hubbU  -text "screened Coulomb parameter "  -bg $BG
pack configure $w.space0U $w.hubbU  -side top -anchor n -fill both

set FRU $w.frameU
frame $FRU -bg $BG
pack  $FRU -fill x
label $FRU.lab1 -text "      U" -width 10 -bg $BG
scale $FRU.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
            -resolution 0.01 -bg $BG -variable VAR(UHub-$IT) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRU.lab2 -text " eV" -width 2 -bg $BG
pack  $FRU.lab1 $FRU.sca $FRU.lab2 -side left

label $w.space0J  -text " "  -bg $BG
label $w.hubbJ  -text "exchange parameter "  -bg $BG
pack configure $w.space0J $w.hubbJ  -side top -anchor n -fill both

set FRJ $w.frameJ
frame $FRJ -bg $BG
pack  $FRJ -fill x
label $FRJ.lab1 -text "      J" -width 10 -bg $BG
scale $FRJ.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
            -resolution 0.01 -bg $BG -variable VAR(JHub-$IT) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRJ.lab2 -text " eV" -width 2 -bg $BG
pack  $FRJ.lab1 $FRJ.sca $FRJ.lab2 -side left

label $w.space0E  -text " "  -bg $BG
label $w.hubbE  -text "reference energy   "  -bg $BG
pack configure $w.space0E $w.hubbE  -side top -anchor n -fill both

set FRE $w.frameE
frame $FRE -bg $BG
pack  $FRE -fill x
label $FRE.lab1 -text "      E" -width 10 -bg $BG
scale $FRE.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
            -resolution 0.01 -bg $BG -variable VAR(EHub-$IT) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRE.lab2 -text " Ry" -width 2 -bg $BG
pack  $FRE.lab1 $FRE.sca $FRE.lab2 -side left

#---------------------------------------------------------------------------------------

label $w.foot -text " " -bg $BG

pack configure $w.foot -side top -anchor n -fill both


frame $w.frameEND -bg $BG
pack  $w.frameEND -fill both
button $w.frameEND.return -text "RETURN"  -width 20 -height $buttonheight -bg $buttonbgcolor \
       -command "destros $w ; return"
pack  $w.frameEND.return

}
#--------------------------------------------------------------- proc cinp_SPR-KKR_LDA_U

#
#------------------------------------------------------------------------ scale-c
} elseif { $VAR(HAMILTONIAN) == "scale-c" } {

label $wc1.headtxt  -text "   by scaling the speed of light " -anchor w  -padx 80 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(scale-c$IT) 1.0
CreateSCALE       $wc1 $BGMOD $wl "   $TXTT($IT)" $ws 0.0  2.0   0 3 0.01  VAR scale-c$IT  $fe
}
CreateTEXT        $wc1 $BGX info \
" 
 manipulate ALL relativsitic effects by scaling 1/c^2

      0:    NON-relativistic limit  (Schroedinger)   
      1:    relativistic case          (Dirac)
     >1:   enhancement of relativistic effects

"
#
#------------------------------------------------------------------------ scale-SOC
} elseif { $VAR(HAMILTONIAN) == "scale-SOC" } {

label $wc1.headtxt  -text "   by scaling the spin-orbit coupling strength" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(scale-SOC$IT) 1.0
CreateSCALE       $wc1 $BGMOD $wl "   $TXTT($IT)" $ws 0.0  2.0   0 3 0.01  VAR scale-SOC$IT  $fe
}
CreateTEXT        $wc1 $BGX info \
" 
 manipulate the strength of the spin-orbit coupling
 all other relativistic effects are unchanged

      0:    scalar-relativistic mode 
      1:    relativistic case       (Dirac)
     >1:   enhancement of spin-orbit coupling

"

#
#------------------------------------------------------------------------ use SOC-xy
} elseif { $VAR(HAMILTONIAN) == "SOC-xy"    } {
label $wc1.headtxt  -text "   by using only the xy-part of the spin-orbit coupling" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(SOC-xy$IT) 1
CreateCHECKBUTTON $wc1 $BGMOD $wl " " VAR SOC-xy$IT    "$TXTT($IT)"
}
#
#------------------------------------------------------------------------ use SOC-zz
} elseif { $VAR(HAMILTONIAN) == "SOC-zz"    } {
label $wc1.headtxt  -text "   by using only the zz-part of the spin-orbit coupling" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(SOC-zz$IT) 1
CreateCHECKBUTTON $wc1 $BGMOD $wl " " VAR SOC-zz$IT    "$TXTT($IT)"
}
#--------------------------------------------------------------------------------
} ; # if-else----

} ; # KEY == HAMILTONIAN
#                             supply additional information for HAMILTONIAN
#--------------------------------------------------------------------------------
#


#
#--------------------------------------------------------------------------------
#                             supply additional information for KMROT
if {$KEY == "KMROT"} {

#
#--------------------------------------- deactivate columns 2,3,4
#
pack forget $w.cols.col2
pack forget $w.cols.col3
pack forget $w.cols.col4

$Wrbcmd.header configure -text "orientation of the magnetisation "


#
#------------------------------------------------------------------------ KMROT = 0 z-axis
if { $VAR(KMROT) == "0" } {
   destros $w
   return
#
#------------------------------------------------------------------------ KMROT = 1 global rotation
} elseif { $VAR(KMROT) == "1" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "common orientation vector   MDIR   (Cart. units)" -padx 30 -bg $BGMAG
pack $wc1.headtxt  -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(MDIR-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(MDIR-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(MDIR-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x   -padx 2 -pady 30
#CreateSCALE       $wc1 $BGMAG $wl "        x" $ws -2.0  2.0   0 3 0.01  VAR MDIR-x  $fe
#CreateSCALE       $wc1 $BGMAG $wl "        y" $ws -2.0  2.0   0 3 0.01  VAR MDIR-y  $fe
#CreateSCALE       $wc1 $BGMAG $wl "        z" $ws -2.0  2.0   0 3 0.01  VAR MDIR-z  $fe
#
#------------------------------------------------------------------------ KMROT = 2 non-collinear spin configuration
} elseif { $VAR(KMROT) == "2" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "non-collinear spin configuration   MDIRQ  (Cart. units)" -padx 2 -bg $BGMAG
pack $wc1.headtxt  -fill x -anchor n -pady 2 -side top
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  frame $wc1.line$IQ -bg $BGMAG
  label $wc1.line$IQ.labs -text "site  $IQ    " -anchor w  -padx 2 -bg $BGMAG -width 7
  label $wc1.line$IQ.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.entx -textvariable  VAR(MDIR-x$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
  label $wc1.line$IQ.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.enty -textvariable  VAR(MDIR-y$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
  label $wc1.line$IQ.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.entz -textvariable  VAR(MDIR-z$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line$IQ.labs $wc1.line$IQ.labx $wc1.line$IQ.entx \
     $wc1.line$IQ.laby $wc1.line$IQ.enty $wc1.line$IQ.labz $wc1.line$IQ.entz \
     -side left  -fill x
pack $wc1.line$IQ  -fill x -padx 30
set VAR(MDIR-x$IQ) 0
set VAR(MDIR-y$IQ) 0
set VAR(MDIR-z$IQ) 1
}
#
#------------------------------------------------------------------------ KMROT = 3 orthogonal spin-spiral
} elseif { $VAR(KMROT) == "3" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "orthogonal spin-spiral   (Theta=90)"         -padx 2 -bg $BGMAG
label $wc1.headtxt2  -text "spin spiral vector   QMVEC  (Cart. units)"  -padx 2 -bg $BGMAG
pack $wc1.headtxt $wc1.headtxt2 -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(QMVEC-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(QMVEC-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(QMVEC-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x   -padx 30 -pady 5
set VAR(QMVEC-x) 0
set VAR(QMVEC-y) 0
set VAR(QMVEC-z) 1
#
#------------------------------------------------------------------------ KMROT = 4 general spin-spiral
} elseif { $VAR(KMROT) == "4" } {
pack $wc2
$wc2 configure -bg $BGMAG
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "spin-spiral with arbitrary tilt angle Theta"         -padx 30 -bg $BGMAG
label $wc1.headtxt2  -text "spin spiral vector   QMVEC  (Cart. units)"  -padx 2 -bg $BGMAG
pack $wc1.headtxt $wc1.headtxt2 -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(QMVEC-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(QMVEC-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(QMVEC-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x  -padx 30
set VAR(QMVEC-x) 0
set VAR(QMVEC-y) 0
set VAR(QMVEC-z) 1

label $wc2.headtet  -text "site-dependent tilt angle Theta (deg.)" -padx 30 -bg $BGMAG
pack $wc2.headtet -fill x -anchor n -pady 2 -side top
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  frame $wc2.line$IQ -bg $BGMAG
  label $wc2.line$IQ.labx -text "         site  $IQ   " -width 15 -anchor w  -padx 2 -bg $BGMAG
  entry $wc2.line$IQ.entx -textvariable  VAR(QMTET$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc2.line$IQ.labx $wc2.line$IQ.entx -side left  -fill x
pack $wc2.line$IQ  -fill x
set VAR(QMTET$IQ) 90
} ; # IQ



} ; # if-else----

} ; # KEY == KMROT
#                             supply additional information for KMROT
#--------------------------------------------------------------------------------
#



if {$KEY == "TUT-TASK" } {


set TUT-TASK $VAR(TUT-TASK)

regsub "$syssuffix\$" $sysfile "" inpfile
set inpfile "$inpfile\_$VAR(TUT-TASK)$inpsuffix"
set VAR(ADSI)    "${TUT-TASK}"

puts "****************************** KEY      $KEY"
puts "****************************** TUT-TASK ${TUT-TASK}"
puts "****************************** inpfile  $inpfile"

global system TUT_TEXT

$Wrbcmd.header configure -text "Tutorial   $system \n\n \
                                $TUT_TEXT \n\n \
                                parameters for TUT-TASK   ${TUT-TASK} "

#----------------------------------------------- disable OWRPOT
set VAR(OWRPOT) 0
widget_disable   $tutorial_SPRKKR_wc3.wVAROWRPOT.2

#----------------------------------------------- set default for energy scale
$tutorial_SPRKKR_wc1.wVARNE.2 configure -from 0 -to 250 -digits 3 -resolution 1

#
#================================================================================= YLM
#
if {${TUT-TASK} == "YLM" } {
    
set lmax [expr $NL - 1] 
set lmax 12

CreateTEXT        $wc1 $BGX info \
" 
 plot the real spherical harmonics Y_lm(r) for given l and m

"
CreateSCALE  $wc1 $BGCLU $wl "   l"   $ws 0      $lmax 0 1 1  VAR  YLM-l  $fe
$wc1.wVARYLM-l.2 set 2

CreateSCALE  $wc1 $BGCLU $wl "   m"   $ws -$lmax $lmax 0 1 1  VAR  YLM-m  $fe
$wc1.wVARYLM-m.2 set 0

$wc1.wVARYLM-l.2 configure -command "check_VARYLM_lm  $wc1.wVARYLM-l.2 $wc1.wVARYLM-m.2"
$wc1.wVARYLM-m.2 configure -command "check_VARYLM_lm  $wc1.wVARYLM-l.2 $wc1.wVARYLM-m.2"

#---------------------------------------------------- proc check_VARYLM_lm --- START
proc check_VARYLM_lm { wl wm xxx } {
 global VAR
## puts "XXXXXXXXXXXXXXXXXXX  check_VARYLM_lm  VOR:  l m  $VAR(YLM-l) $VAR(YLM-m) [expr abs($VAR(YLM-m))] "

    if { [expr abs($VAR(YLM-m))] >= $VAR(YLM-l) } {
       if { $VAR(YLM-m) >= 0 } {
          $wm set $VAR(YLM-l)
       } else {
          $wm set -$VAR(YLM-l)
       }
     }
## puts "XXXXXXXXXXXXXXXXXXX  check_VARYLM_lm  NACH: l m  $VAR(YLM-l) $VAR(YLM-m)  "
}
#---------------------------------------------------- proc check_VARYLM_lm --- END

#
#================================================================================= jlx
#
} elseif {${TUT-TASK} == "jlx" } {

CreateTEXT        $wc1 $BGX info \
" 
 plot the spherical Bessel and Neumann functions j_l(x) and n_l(x)

 for l = 0 .. lmax  and  x = xmin ... xmax  with  Nx mesh points 

"
CreateSCALE  $wc1 $BGCLU $wl "   lmax"   $ws  0   20  0 1   1  VAR  jlx-lmax  $fe
$wc1.wVARjlx-lmax.2 set 3
CreateSCALE  $wc1 $BGCLU $wl "   Nx  "   $ws 50  400  0 1  50  VAR  jlx-NX    $fe
$wc1.wVARjlx-NX.2 set   400
CreateSCALE  $wc1 $BGCLU $wl "   xmin"   $ws  0 50.0  0 3 0.2  VAR  jlx-xmin  $fe
$wc1.wVARjlx-xmin.2 set  0.0
CreateSCALE  $wc1 $BGCLU $wl "   xmax"   $ws  0 50.0  0 3 0.2  VAR  jlx-xmax  $fe
$wc1.wVARjlx-xmax.2 set  20.0


$wc1.wVARjlx-xmin.2 configure -command "check_VARjlx_xminmax  $wc1.wVARjlx-xmin.2 $wc1.wVARjlx-xmax.2"
$wc1.wVARjlx-xmax.2 configure -command "check_VARjlx_xminmax  $wc1.wVARjlx-xmin.2 $wc1.wVARjlx-xmax.2"

#---------------------------------------------------- proc check_VARjlx_xminmax --- START
proc check_VARjlx_xminmax { wmin wmax xxx } {
 global VAR
## puts "XXXXXXXXXXXXXXXXXXX  check_VARjlx_xminmax  VOR:  max min $VAR(jlx-xmin)  $VAR(jlx-xmax) "

    if { $VAR(jlx-xmin) >= $VAR(jlx-xmax) } {  $wmax set [expr $VAR(jlx-xmin) + 0.2] }

    if { $VAR(jlx-xmin) >= $VAR(jlx-xmax) } {  $wmin set [expr $VAR(jlx-xmax) - 0.2] }

## puts "XXXXXXXXXXXXXXXXXXX  check_VARjlx_xminmax  NACH: max min $VAR(jlx-xmin)  $VAR(jlx-xmax)  "
}
#---------------------------------------------------- proc check_VARjlx_xminmax --- END

#
#================================================================================= jlx-asympt
#
} elseif {${TUT-TASK} == "jlx-asympt" } {

CreateTEXT        $wc1 $BGX info \
" 
 plot the spherical Bessel and Neumann functions j_l(x) and n_l(x)

 and their asymptotic behaviour for small and large x, respectively

 for l = 0 .. lmax  and  x = xmin ... xmax  with  Nx mesh points 

"
CreateSCALE  $wc1 $BGCLU $wl "   lmax"   $ws  0    4  0 1   1  VAR  jlx-lmax  $fe
$wc1.wVARjlx-lmax.2 set 3
CreateSCALE  $wc1 $BGCLU $wl "   Nx  "   $ws 50  400  0 1  50  VAR  jlx-NX    $fe
$wc1.wVARjlx-NX.2 set   400
CreateSCALE  $wc1 $BGCLU $wl "   xmin"   $ws  0 50.0  0 3 0.2  VAR  jlx-xmin  $fe
$wc1.wVARjlx-xmin.2 set  0.0
CreateSCALE  $wc1 $BGCLU $wl "   xmax"   $ws  0 50.0  0 3 0.2  VAR  jlx-xmax  $fe
$wc1.wVARjlx-xmax.2 set   2.0
CreateRADIOBUTTON $wc1 $BGCLU $wl "   range for x:   " VAR jlx-x-range xxx $fe s 1  small large
$wc1.wVARjlx-x-rangexxx.1 select


$wc1.wVARjlx-xmin.2 configure -command "check_VARjlx_xminmax  $wc1.wVARjlx-xmin.2 $wc1.wVARjlx-xmax.2"
$wc1.wVARjlx-xmax.2 configure -command "check_VARjlx_xminmax  $wc1.wVARjlx-xmin.2 $wc1.wVARjlx-xmax.2"

#---------------------------------------------------- proc check_VARjlx_xminmax --- START
proc check_VARjlx_xminmax { wmin wmax xxx } {
 global VAR
## puts "XXXXXXXXXXXXXXXXXXX  check_VARjlx_xminmax  VOR:  max min $VAR(jlx-xmin)  $VAR(jlx-xmax) "

    if { $VAR(jlx-xmin) >= $VAR(jlx-xmax) } {  $wmax set [expr $VAR(jlx-xmin) + 0.2] }

    if { $VAR(jlx-xmin) >= $VAR(jlx-xmax) } {  $wmin set [expr $VAR(jlx-xmax) - 0.2] }

## puts "XXXXXXXXXXXXXXXXXXX  check_VARjlx_xminmax  NACH: max min $VAR(jlx-xmin)  $VAR(jlx-xmax)  "
}
#---------------------------------------------------- proc check_VARjlx_xminmax --- END

#
#================================================================================= chr-pot
#
} elseif {${TUT-TASK} == "chr-pot" } {

CreateTEXT        $wc1 $BGX info \
" 
 plot the spherical part of the potenial and charge density


 NOTE:    

- for the potential  r V(r)      is plotted

- for the charge     r^2 rho(r)  is plotted


"

#
#================================================================================= pot-decomp
#
} elseif {${TUT-TASK} == "pot-decomp" } {

CreateTEXT        $wc1 $BGX info \
" 
 plot the decoposition of the potential into its various contributions:

 - V_tot total electronic potential 
 - V_nuc Coulomb potential of the nucleus -Ze^2/r
 - V_H    Hartree potential due to the Coulomb interaction with electronic charge density
 - V_xc   exchange-correlation potential  (using MJW parametrisation)
 - V_x     exchange part of V_xc


 NOTE:    

-  the potential is dominated by V_nuc 
-  to see the xc and x terms clearly one has to zoom in 
   or hide V_nuc via the xmgrace menue \"set appearance\"  

"

#
#================================================================================= pot-xc
#
} elseif {${TUT-TASK} == "pot-xc" } {

CreateTEXT        $wc1 $BGX info \
" 

 plot the spherical part of the potenial and charge density


 NOTE:    

- for the potential  r V(r)      is plotted

- for the charge     r^2 rho(r)  is plotted
"

#
#================================================================================= RWF-l
#
} elseif {${TUT-TASK} == "RWF-l" } {
    set E_default      0.5
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      $E_default   
    set VAR(GRID)      3    ;    set VAR(EMAX)      $E_default 
    set VAR(NE)        1    ;    set VAR(ImE)       0.0

CreateTEXT        $wc1 $BGX info \
" 
 plot electronic wave functions for band states
   
 for a fixed energy  E  and  l = 0 .. lmax 

"
CreateSCALE  $wc1 $BGCLU $wl "   E (Ry)"   $ws  -1.0 50.0  0 3 0.1  VAR  RWF-l-E     $fe
$wc1.wVARRWF-l-E.2    set  $E_default
CreateSCALE  $wc1 $BGCLU $wl "   lmax  "   $ws     1    4  0 1   1  VAR  RWF-l-lmax  $fe
$wc1.wVARRWF-l-lmax.2 set 3

$wc1.wVARRWF-l-lmax.2 configure -command "check_VARRWF-l-lmax  "

$wc1.wVARRWF-l-E.2 configure -command "check_VARRWF-l-E  "

#---------------------------------------------------- proc check_VARRWF-l-E --- START
proc check_VARRWF-l-E { xxx } {
 global VAR

 set VAR(EMIN)  $VAR(RWF-l-E)
 set VAR(EMAX)  $VAR(RWF-l-E)
}
#---------------------------------------------------- proc check_VARRWF-l-E --- END

#---------------------------------------------------- proc check_VARRWF-l-lmax --- START
proc check_VARRWF-l-lmax { xxx } {
 global VAR NLQ

 set VAR(NLUSER)  [expr $VAR(RWF-l-lmax) + 1]
 set NLQ(1)       [expr $VAR(RWF-l-lmax) + 1]
}
#---------------------------------------------------- proc check_VARRWF-l-lmax --- END


#
#================================================================================= RWF-E
#
} elseif {${TUT-TASK} == "RWF-E" } {
    set E_default      0.5
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      $E_default   
    set VAR(GRID)      3    ;    set VAR(EMAX)      $E_default 
    set VAR(NE)        1    ;    set VAR(ImE)       0.0

CreateTEXT        $wc1 $BGX info \
" 
 plot electronic wave functions for band states
   
 for NE energies within the range  Emin  and  Emax
   
 for a fixed angular momentum  l

"
CreateSCALE  $wc1 $BGCLU $wl "   NE    "      $ws  1   10     0 1   1  VAR  RWF-NE      $fe
$wc1.wVARRWF-NE.2 set 10
CreateSCALE  $wc1 $BGCLU $wl "   Emin (Ry)"   $ws  -1.0 50.0  0 3 0.1  VAR  RWF-Emin    $fe
$wc1.wVARRWF-Emin.2    set  0.1
CreateSCALE  $wc1 $BGCLU $wl "   Emin (Ry)"   $ws  -1.0 50.0  0 3 0.1  VAR  RWF-Emax    $fe
$wc1.wVARRWF-Emax.2    set  1.0
CreateSCALE  $wc1 $BGCLU $wl "   l     "      $ws  1    4     0 1   1  VAR  RWF-E-l  $fe
$wc1.wVARRWF-E-l.2 set 3
$wc1.wVARRWF-E-l.2 configure -command "check_VARRWF-E-l  "

$wc1.wVARRWF-NE.2   configure -command "check_VARRWF-E-E  "
$wc1.wVARRWF-Emin.2 configure -command "check_VARRWF-E-E  "
$wc1.wVARRWF-Emax.2 configure -command "check_VARRWF-E-E  "

#---------------------------------------------------- proc check_VARRWF-E-E --- START
proc check_VARRWF-E-E { xxx } {
 global VAR

 set VAR(NE)    $VAR(RWF-NE)
 set VAR(EMIN)  $VAR(RWF-Emin)
 set VAR(EMAX)  $VAR(RWF-Emax)
}
#---------------------------------------------------- proc check_VARRWF-E-E --- END

#---------------------------------------------------- proc check_VARRWF-E-l --- START
proc check_VARRWF-E-l { xxx } {
 global VAR NLQ

 set VAR(NLUSER)  [expr $VAR(RWF-E-l) + 1]
 set NLQ(1)       [expr $VAR(RWF-E-l) + 1]
}
#---------------------------------------------------- proc check_VARRWF-E-l --- END


#
##================================================================================= core-all
#
} elseif {${TUT-TASK} == "core-all" } {

CreateTEXT        $wc1 $BGX info \
" 

 plot for a relativistic (Dirac) calculation of all core states:

 - energy eigenvalues 

 - radial wave functions  (g,f) 

 - spin-independent potential V(r)


 HINT:    

- to see the wave functions for individulal states clearly
  one has to zoom in - eventually one has to suppress the
  display for other wave functions 

- the crossing of an energy level and the potential V(r)
  marks the classical turning point

- for the shells with common principle quantum number n
  the degeneracy with respect to l 
  for a pure Coulomb potential is removed

- shells with l>0 are split via spin-orbit coupling


 NOTE:    

- the magnetic field B will be set to 0
  forcing a non-magnetic calculation
"


#
#---------------------------------------------------------------- DOS
#
} elseif {${TUT-TASK} == "DOS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)       50    ;    set VAR(ImE)       0.01
    destroy $Wrbcmd
    widget_enable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
#
#---------------------------------------------------------------- EKREL
#
} elseif {${TUT-TASK} == "EKREL" } {
#    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
#    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
#    set VAR(NE)     1000    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2


label $wc1.energy    -text "energy range" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.energy   -in $wc1  -anchor nw -side top -fill x
CreateSCALE  $wc1 $BGTSK $wl "  E_min" $ws -4.0 4.0  0 2 0.1  VAR EMINDISP $fe
CreateSCALE  $wc1 $BGTSK $wl "  E_max" $ws -4.0 4.0  0 2 0.1  VAR EMAXDISP $fe
CreateSCALE  $wc1 $BGTSK $wl "  NE   " $ws    1 2000 0 4 20   VAR NEDISP   $fe
label $wc1.kpoints   -text "number of k-points" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.kpoints  -in $wc1  -anchor nw -side top -fill x
CreateSCALE  $wc1 $BGTSK $wl "  NK   " $ws    1 2000 0 4 20   VAR NKDISP   $fe
label $wc1.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.ekrel_bot   -in $wc1  -anchor nw -side top -fill both -expand y

label $wc2.ekrel    -text "path in k-space" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrel   -in $wc2  -anchor nw -side top -fill x

for {set IKPATH 1} {$IKPATH <= $KPATHTOP} {incr IKPATH} {
    CreateRADIOBUTTON $wc2 $BGTSK $wl "KPATH   $IKPATH" \
                VAR KPATH $IKPATH  $fe n $IKPATH "$KPLAB($IKPATH)  "
}


##label $wc2.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
##pack configure $wc2.ekrel_bot   -in $wc2  -anchor nw -side top -fill both -expand y

CreateRADIOBUTTON $wc2 $BGTSK $wl "     "  VAR KPATH  user  $fe n -1  "user defined   (set below)"
label $wc2.ekrela    -text "start vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrela   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  k_x  " $ws -2.0 2.0  0 2 0.1  VAR KA1x $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_y  " $ws -2.0 2.0  0 2 0.1  VAR KA1y $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_z  " $ws -2.0 2.0  0 2 0.1  VAR KA1z $fe
label $wc2.ekrele    -text "end vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrele   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  k_x  " $ws -2.0 2.0  0 2 0.1  VAR KE1x $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_y  " $ws -2.0 2.0  0 2 0.1  VAR KE1y $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_z  " $ws -2.0 2.0  0 2 0.1  VAR KE1z $fe
label $wc2.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrel_bot   -in $wc2  -anchor nw -side top -fill both -expand y

#
#---------------------------------------------------------------- BLOCHSF
#
} elseif {${TUT-TASK} == "BLOCHSF" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      200    ;    set VAR(ImE)       0.0
    if {$NCPA == 0} {set VAR(ImE) 0.001}
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
#----------------------------------------------- set default for energy scale
    $tutorial_SPRKKR_wc1.wVARNE.2 configure -from 1 -to 2001 -digits 4 -resolution 20
#
#------------------------------------ column 1
#
label $wc1.mode    -text "E-k-plot for BSF A(E,k)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.mode   -in $wc1  -anchor nw -side top -fill x
CreateRADIOBUTTON $wc1 $BGTSK $wl "     "  VAR MODEBSF  a  $fe n 1  "select"

#set wradbut1 $wc1.wVARMODEBSFa.1
#set wradbut2 $wc1.wVARMODEBSFb.2

#$wradbut1 configure -command  exec_RADIOBUTTON_cmd_MODEBSF
#$wradbut2 configure -command  exec_RADIOBUTTON_cmd_MODEBSF

#global exec_RADIOBUTTON_cmd_wc1
#set exec_RADIOBUTTON_cmd_wc1 $wc1

#proc exec_RADIOBUTTON_cmd_MODEBSF {} {
#global VAR exec_RADIOBUTTON_cmd_wc1
#set wc1 $exec_RADIOBUTTON_cmd_wc1
#set MODE $VAR(MODEBSF)
#if {[winfo exists $wc1.bsf_bot]} {
#   if {$MODE==1} {
#      $wc1.energy configure -text "energy range"
#   } else {
#      $wc1.energy configure -text "fixed energy"
#   }
#}
#}

label $wc1.space    -text " " -anchor w  -padx 2 -bg $BGTSK -height 5
pack configure $wc1.space    -in $wc1  -anchor nw -side top -fill x

label $wc1.energy    -text "energy range" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.energy   -in $wc1  -anchor nw -side top -fill x
CreateENTRY    $wc1 $BGTSK $wl "   EMIN "     $wee VAR EMIN   $fe 3 "Ry"
CreateENTRY    $wc1 $BGTSK $wl "   EMAX "     $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY    $wc1 $BGTSK $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"
CreateSCALE    $wc1 $BGTSK $wl "   NE   " $ws    1 2001 0 4 20   VAR NE   $fe

label $wc1.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.bsf_bot   -in $wc1  -anchor nw -side top -fill both -expand y

#
#------------------------------------ column 2
#
label $wc2.head   -text "k-parameters for E-k-plot" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.head     -in $wc2  -anchor nw -side top -fill x
label $wc2.kpoints   -text "number of k-points" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.kpoints  -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  NK   " $ws    1 2000 0 4 20   VAR NKBSF   $fe
label $wc2.bsf    -text "path in k-space" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsf   -in $wc2  -anchor nw -side top -fill x

for {set IKPATH 1} {$IKPATH <= $KPATHTOP} {incr IKPATH} {
    CreateRADIOBUTTON $wc2 $BGTSK $wl "KPATH   $IKPATH" \
                VAR KPATH $IKPATH  $fe n $IKPATH "$KPLAB($IKPATH)  "
}

CreateRADIOBUTTON $wc2 $BGTSK $wl "     "  VAR KPATH  user  $fe n -1  "user defined   (below)"
label $wc2.bsfa    -text "start vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsfa   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "   k_x  " $ws -2.0 2.0  0 2 0.1  VAR KA1x $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_y  " $ws -2.0 2.0  0 2 0.1  VAR KA1y $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_z  " $ws -2.0 2.0  0 2 0.1  VAR KA1z $fe
label $wc2.bsfe    -text "end vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsfe   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "   k_x  " $ws -2.0 2.0  0 2 0.1  VAR KE1x $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_y  " $ws -2.0 2.0  0 2 0.1  VAR KE1y $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_z  " $ws -2.0 2.0  0 2 0.1  VAR KE1z $fe
label $wc2.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsf_bot   -in $wc2  -anchor nw -side top -fill both -expand y


#
#------------------------------------ column 3
#
label $wc3.mode-k-k    -text "k-k-plot for BSF A(E,k)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.mode-k-k   -in $wc3  -anchor nw -side top -fill x
CreateRADIOBUTTON $wc3 $BGK $wl "     "  VAR MODEBSF  b  $fe n 2  "select"

label $wc3.bsf_fill    -text " " -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsf_fill   -in $wc3  -anchor nw -side top -fill both -expand y


label $wc3.energy-k-k    -text "fixed energy" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.energy-k-k   -in $wc3  -anchor nw -side top -fill x
CreateENTRY    $wc3 $BGK  $wl "   E_fix "    $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY    $wc3 $BGK  $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"

#
###label $wc3.head   -text "k-parameters for k-k-plot (constant E)" -anchor w  -padx 2 -bg $BGK
###pack configure $wc3.head     -in $wc3  -anchor nw -side top -fill x
#
label $wc3.bsfa    -text "spanning vector k1 (2 pi/a)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsfa   -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "   k1_x  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1x $fe
CreateSCALE  $wc3 $BGK $wl "   k1_y  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1y $fe
CreateSCALE  $wc3 $BGK $wl "   k1_z  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1z $fe
#
label $wc3.lnk1 -text "number of k-points along vector k1" -anchor w -padx 2 -bg $BGK
pack configure $wc3.lnk1  -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "  NK1  " $ws    1 2000 0 4 20   VAR NK1BSF   $fe
#
label $wc3.bsfe    -text "spanning vector k2 (2 pi/a)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsfe   -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "   k2_x  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2x $fe
CreateSCALE  $wc3 $BGK $wl "   k2_y  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2y $fe
CreateSCALE  $wc3 $BGK $wl "   k2_z  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2z $fe
#
label $wc3.lnk2 -text "number of k-points along vector k2" -anchor w -padx 2 -bg $BGK
pack configure $wc3.lnk2  -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "  NK2  " $ws    1 2000 0 4 20   VAR NK2BSF   $fe
#
label $wc3.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsf_bot   -in $wc3  -anchor nw -side top -fill both -expand y

#
#---------------------------------------------------------------- WFPLOT
#
} elseif {${TUT-TASK} == "WFPLOT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.5   
    set VAR(GRID)      3    ;    set VAR(EMAX)      0.5 
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl " core / VB"    VAR CORE_VB    a     $fe s 1  CORE VB       
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateRADIOBUTTON $wc1 $BGMAG $wl "   VB   l "    VAR VB_l       a     $fe s 1  s p d f

CreateTEXT        $wc1 $BGX info \
" 
 plot electronic wave functions for core or band states  

     core states
     - specify core-level 

     band states 
     - select l-character                       
     - set energy via standard E-mask 

     DEFAULT: mj=+1/2
     you can modify mj in the input file
"
#
#
#---------------------------------------------------------------- PSHIFT
#
} elseif {${TUT-TASK} == "PSHIFT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0001
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      100    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#---------------------------------------------------------------- SOCPAR
#
} elseif {${TUT-TASK} == "SOCPAR" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0001
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      100    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#---------------------------------------------------------------- JXC
#
} elseif {${TUT-TASK} == "JXC"   } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

CreateSCALE       $wc1 $BGTSK $wl "   CLURAD"  200  0         20   0 3 0.1  VAR    CLURAD_JXC  $fe

CreateTEXT        $wc1 $BGX info \
" 
 calculation of the exchange coupling parameter J_ij     

     - specify the radius CLURAD            
       all atoms j within the sphere of radius CLURAD
       around atom i are considered            
"
#
#---------------------------------------------------------------- FMAG
#
} elseif {${TUT-TASK} == "FMAG"   } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#
#---------------------------------------------------------------- MEPLOT
#
} elseif {${TUT-TASK} == "MEPLOT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0   
    set VAR(GRID)      3    ;    set VAR(EMAX)      4   
    set VAR(NE)        100  ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "transition"   VAR transition    a     $fe s 1  core-band band-band 
CreateRADIOBUTTON $wc1 $BGTSK $wl "paramagnetic" VAR MEPLOT_NONMAG a     $fe s 1  YES NO
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "   VAR NCXRAY        aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "   VAR LCXRAY        aexec $fe s 1  s p d f
CreateRADIOBUTTON $wc1 $BGMAG $wl "   VB   l "   VAR MEPLOT_l      a     $fe s 1  all s p d f
CreateTEXT        $wc1 $BGE phot  "photon energy range"
CreateSCALE       $wc1 $BGE   $wl "   NE  "      $ws 1        250   0 3 1        VAR NEPHOT  $fe
CreateRADIOBUTTON $wc1 $BGE   $wl "   fixed  "   VAR EPHOT-fixed   aexec $fe s 1  {He I} {He II} {Al Ka} {Mg Ka}
CreateENTRY       $wc1 $BGE   $wl "   EMIN "     $wee VAR EPHOTMIN       $fe 3 "eV"
CreateENTRY       $wc1 $BGE   $wl "   EMAX "     $wee VAR EPHOTMAX       $fe 3 "eV"

CreateTEXT        $wc1 $BGX info \
" 
 plot E-dependence of dipole transition matrix elements  

     core-band
     - specify core-level 
     - specify VB E-range via standard E-mask  

     band-band for fixed photon energy
     - set initial state E-range via standard E-mask
     - specify photon energy EPHOT with NEPHOT = 1 
     - eventually restrict l for initial state 

     band-band for variable photon energy
     - set single initial state E via standard E-mask
     - specify photon energy range
     - eventually restrict l for initial state

     PARAMAGNETIC = YES:
     the exchange splitting will be suppressed
     to avoid states with mixed j-character
"
#
#
#---------------------------------------------------------------- MECHECK
#
} elseif {${TUT-TASK} == "MECHECK" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0   
    set VAR(GRID)      3    ;    set VAR(EMAX)      4   
    set VAR(NE)        100  ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "transition"  VAR transition  a     $fe s 1  core-band band-band 
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "  VAR NCXRAY      aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "  VAR LCXRAY      aexec $fe s 1  s p d f
CreateTEXT        $wc1 $BGE wERYDI  "initial band state energy "
CreateENTRY       $wc1 $BGE   $wl "   Re E(ini) "     $wee VAR ReERYDI  $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im E(ini) "     $wee VAR ImERYDI  $fe 3 "Ry"
CreateTEXT        $wc1 $BGE wERYDF  "final band state energy "
CreateENTRY       $wc1 $BGE   $wl "   Re E(fin) "     $wee VAR ReERYDF  $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im E(fin) "     $wee VAR ImERYDF  $fe 3 "Ry"

CreateTEXT        $wc1 $BGX info \
" 
 check the dipole transition matrix elements  

     core-band
     - specify core-level 
     - set final band state energy 

     band-band
     - set initial band state energy
     - set final band state energy 

"
#
#---------------------------------------------------------------- VBXPS
#
} elseif {${TUT-TASK} == "VBXPS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    set VAR(EPHOT)  1253.6
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

CreateENTRY       $wc1 $BGTSK $wl "   EPHOT"     $wee VAR EPHOT     $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   USETAUNN"       VAR USETAUNN  a $fe n 0  NO YES

CreateTEXT        $wc1 $BGX info \
" 
 calculate valence band photo emission spectra 

     - specify photon energy

"

#
#---------------------------------------------------------------- BIS
#
} elseif {${TUT-TASK} == "BIS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      [expr $VAR(EF) + 4]
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    set VAR(EPHOT)  1253.6
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

CreateENTRY       $wc1 $BGTSK   $wl "   EPHOT"     $wee VAR EPHOT  $fe 3 "eV"
#
#---------------------------------------------------------------- CLXPS
#
} elseif {${TUT-TASK} == "CLXPS" } {
     set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
     set VAR(GRID)      3    ;    set VAR(EMAX)      ""
     set VAR(NE)        1    ;    set VAR(ImE)       0.01
     set VAR(EPHOT)  1253.6
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateENTRY       $wc1 $BGTSK $wl "   EPHOT"     $wee VAR EPHOT  $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   USETSS"     VAR USETSS  a $fe n 1  YES NO

#
#---------------------------------------------------------------- COMPTON
#
} elseif {${TUT-TASK} == "COMPTON" } {
     set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      "-0.2"
     set VAR(GRID)      5    ;    set VAR(EMAX)      ""
     set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

label $wc1.lnk1 -text "   PNVEC  normal vector for Compton profile" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk1  -in $wc1  -anchor nw -side top -fill x
CreateENTRY       $wc1 $BGTSK $wl "   x    "     $wee VAR COMPTON-PNVECx  $fe 3 "  "
CreateENTRY       $wc1 $BGTSK $wl "   y    "     $wee VAR COMPTON-PNVECy  $fe 3 "  "
CreateENTRY       $wc1 $BGTSK $wl "   z    "     $wee VAR COMPTON-PNVECz  $fe 3 "  "
label $wc1.lnk2 -text "   parameters for grid along PNVEC" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk2  -in $wc1  -anchor nw -side top -fill x
CreateSCALE       $wc1 $BGTSK $wl "   NPN    " $ws 1  200  0   3   1  VAR COMPTON-NPN   $fe
CreateSCALE       $wc1 $BGTSK $wl "   PNMAX "  $ws 0   40  0   3 0.1  VAR COMPTON-PNMAX $fe
label $wc1.lnk3 -text "   parameters for grid perpendicular to PNVEC" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk3  -in $wc1  -anchor nw -side top -fill x
CreateSCALE       $wc1 $BGTSK $wl "   NPP    " $ws 1  200  0   3   1  VAR COMPTON-NPP   $fe
CreateSCALE       $wc1 $BGTSK $wl "   PPMAX "  $ws 0   40  0   3 0.1  VAR COMPTON-PPMAX $fe

#
#---------------------------------------------------------------- AES
#
} elseif {${TUT-TASK} == "AES" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateSCALE       $wc1 $BGTSK $wl "   NEME   " $ws 2 30    0      2 1  VAR AESNEME $fe
#PRINTME
#
#---------------------------------------------------------------- NRAES
#
} elseif {${TUT-TASK} == "NRAES" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      [read_keyed_value $VAR(POTFIL) "EF"]
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
#CALCLS
#OUTGNU
#
#---------------------------------------------------------------- APS
#
} elseif {${TUT-TASK} == "APS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      [read_keyed_value $VAR(POTFIL) "EF"]
    set VAR(GRID)      3
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
#CALCLS
#OUTGNU
#BRDDOS
#GAMMAV
#
#---------------------------------------------------------------- XAS / XMO / XES / XRS
#
} elseif {${TUT-TASK} == "XAS" || ${TUT-TASK} == "XMO"|| ${TUT-TASK} == "XES" || ${TUT-TASK} == "XRS"  } {
    set VAR(SEARCHEF)  0    ;    set VAR(ImE)       0.01
    set VAR(NE)      180    ;    set VAR(GRID)  3
    if {${TUT-TASK} == "XES" } {
       set VAR(EMIN) -0.2
       set VAR(EMAX)  ""
    } else {
       set VAR(EMIN)  ""
       set VAR(EMAX)  4.0
    }
    if {${TUT-TASK} == "XAS" } {set VAR(GRID)  6}
    if {${TUT-TASK} == "XMO"|| ${TUT-TASK} == "XRS"} {set VAR(GRID)  7}
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
## CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "      VAR XRAYME     a $fe s 1 ADA NAB GRV
if {${TUT-TASK} == "XMO"|| ${TUT-TASK} == "XRS"} {
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(-)"   $wee VAR TAUJMIN $fe 3 "eV"
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(+)"   $wee VAR TAUJPLS $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   TSELECT"  VAR TSELECT    a $fe n 0 0 1 2
CreateSCALE       $wc1 $BGTSK $wl "   NE3 "       $ws 1 400  0 3 1    VAR XMONE3    $fe
CreateSCALE       $wc1 $BGTSK $wl "   EMAX3"      $ws 0  50  0 3 0.1  VAR XMOEMAX3  $fe
}

#MECHECK
#OUTPUT
#
#---------------------------------------------------------------- CHI
#
} elseif {${TUT-TASK} == "CHI" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      "-0.2"
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       50    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2

set wcl(1) $wc1 ; set TXTL(1) "s"
set wcl(2) $wc2 ; set TXTL(2) "p"
set wcl(3) $wc3 ; set TXTL(3) "d"
set wcl(4) $wc4 ; set TXTL(4) "f"
####set wcl(5) $wc5 ; set TXTL(5) "g"

###label $wcl(1).dosef    -text "density of states at the Fermi level" -anchor w  -padx 2 -bg $BGTSK
###pack configure $wcl(1).dosef   -in $wcl(1) -anchor nw -side top -fill x
for {set IL 1} {$IL <= $NL} {incr IL} {
   label $wcl($IL).dosef    -text "density of states at the Fermi level" -anchor w  -padx 2 -bg $BGTSK
   pack configure $wcl($IL).dosef   -in $wcl($IL) -anchor nw -side top -fill x
}

for {set IT 1} {$IT <= $NT} {incr IT} {
  for {set IL 1} {$IL <= $NL} {incr IL} {
     set INDNTL "nef$IT{_}$IL"
     set VAR($INDNTL) 0.0
     CreateSCALE  $wcl($IL) $BGTSK $wl "   $TXTT($IT)   $TXTL($IL)" $ws 0.0 40.0 0 4 0.01  VAR $INDNTL $fe
  }
}

CreateSCALE       $wc1 $BGTSK $wl "   PRINT  " $ws 0 5  0  1  1 VAR CHIPRINT  $fe

#
#---------------------------------------------------------------- T1
#
} elseif {${TUT-TASK} == "T1" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
#
#
#---------------------------------------------------------------- SIGMA
#
} elseif {${TUT-TASK} == "SIGMA" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
## CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "    VAR RHOME     a $fe s 1 ADA NAB GRV

    destroy $Wrbcmd ; return
#
#
#---------------------------------------------------------------- MOKE
#
} elseif {${TUT-TASK} == "MOKE" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)       80    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
## CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "    VAR RHOME     a $fe s 1 ADA NAB GRV

    destroy $Wrbcmd ; return
#
#
#---------------------------------------------------------------- RHO
#
} elseif {${TUT-TASK} == "RHO" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      $VAR(EF)
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "    VAR RHOME     a $fe s 1 ADA NAB GRV
#
#
#---------------------------------------------------------------- SCF
#
} elseif {$VAR(TUT-TASK) == "SCF" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       0.0
    widget_disable  $tutorial_SPRKKR_wc1.wVARSEARCHEF.2
    widget_enable   $tutorial_SPRKKR_wc3.wVAROWRPOT.2

#
#------------------------------------------------- column 1   SCF parameters
#
CreateRADIOBUTTON $wc1 $BGTSK $wl "   SCFVXC "    VAR SCFVXC     a $fe s 1  VWN JWM VBH PBE
CreateRADIOBUTTON $wc1 $BGTSK $wl "   SCFALG "    VAR SCFALG     a $fe s 1  TCHEBY BROYDEN2
CreateSCALE       $wc1 $BGTSK $wl "   SCFNITER " $ws 0        300   0 3 10       VAR SCFNITER  $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFTOL "   $ws 0.000001 0.001 0 4 0.000001 VAR SCFTOL    $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFMIX "   $ws 0.001    0.80  0 3 0.001    VAR SCFMIX    $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFMIXOP " $ws 0.001    0.80  0 3 0.001    VAR SCFMIXOP  $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFISTBRY" $ws 1         20   0 2 1        VAR SCFISTBRY $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFITDEPT" $ws 1         80   0 2 1        VAR SCFITDEPT $fe
set SIM 0
for {set IT 1} {$IT <= $NT} {incr IT} { if {$CONC($IT)<0.999} {set SIM 1 } }
if {$SIM==1} {
CreateSCALE       $wc1 $BGTSK $wl "   SCFSIM"    $ws 0.00     0.80  0 3 0.001    VAR SCFSIM    $fe
}
#

#
#------------------------------------------------- column 2 + 3    SCF START
#
label $wc2.headtxt  -text "   setting up initial charge density"         -anchor w  -padx 2 -bg $BGTSK
label $wc2.qiontxt  -text "   QION   guess for ionicity"                 -anchor w  -padx 2 -bg $BGTSK
label $wc3.headtxt  -text "   setting up initial magnetisation density"  -anchor w  -padx 2 -bg $BGTSK
label $wc3.mspintxt -text "   MSPIN  guess for spin moment"              -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.headtxt $wc2.qiontxt  -in $wc2 -anchor nw -side top -fill x
pack configure $wc3.headtxt $wc3.mspintxt -in $wc3 -anchor nw -side top -fill x


frame $wc2.d -relief sunken -height 20
text  $wc2.d.tt -width 42  \
	-borderwidth 2 -relief raised -setgrid true \
 	-yscroll "$wc2.d.scroll set" -bg $BGTSK
scrollbar $wc2.d.scroll -command "$wc2.d.tt yview"

pack $wc2.d.scroll -side right  -fill y
pack $wc2.d.tt     -side left   -fill both 


if { $NT > 10 } {
   set MAX_IT 10
} else {
   set MAX_IT $NT
}


; #
for {set IT 1} {$IT <= $MAX_IT} {incr IT} {
CreateSCALE       $wc2 $BGTSK $wl "   $TXTT($IT)" $ws -2.0    +2.0   0 3 0.02  VAR QION$IT  $fe
CreateSCALE       $wc3 $BGTSK $wl "   $TXTT($IT)" $ws -7.0    +7.0   0 3 0.05  VAR MSPIN$IT $fe
}
set wlX2 [expr $wl*2]
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use QION (as set above)" VAR MATTHEISS a $fe s 1  "QION"
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use defaults           " VAR MATTHEISS c $fe s 0  "DEFAULT"
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use V(Mattheiss)       " VAR MATTHEISS b $fe s 2  "VMATT"

CreateCHECKBUTTON $wc2 $BGTSK $wl " " VAR NOSSITER    "NO rho(Mattheis) iteration"
CreateSCALE       $wc2 $BGTSK $wl "   QIONSCL "   $ws 0.001    0.80  0 2 0.01    VAR QIONSCL    $fe


CreateCHECKBUTTON $wc3 $BGTSK $wl " " VAR USEMSPIN    "use MSPIN  as set above  "
label $wc1.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
label $wc2.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
label $wc3.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.fill     -in $wc1 -anchor nw -side top -fill both -expand y
pack configure $wc2.fill     -in $wc2 -anchor nw -side top -fill both -expand y
pack configure $wc3.fill     -in $wc3 -anchor nw -side top -fill both -expand y

if { $NT <= 1 } {pack forget $wc2}

#
#------------------------------------------------- column 4 NVALT
#
label $wc4.headtxt  -text "valence band electrons"   -anchor w  -padx 2 -bg $BGTSK
label $wc4.nvalttxt -text "NVALT"              -anchor w  -padx 2 -bg $BGTSK
pack configure $wc4.headtxt $wc4.nvalttxt -in $wc4 -anchor nw -side top -fill x
set wlX2 [expr $wl/2]
set wsX2 [expr $ws/2]
for {set IT 1} {$IT <= $MAX_IT} {incr IT} {
CreateSCALE       $wc4 $BGTSK $wlX2 "$TXTT($IT)" $wsX2 0 18   0 2 1  VAR NVAL$IT $fe
set VAR(NVAL$IT) $NVALT_DEFAULT($IT)
}
set wlX2 1
CreateRADIOBUTTON $wc4 $BGTSK $wlX2 " " VAR USENVALT a $fe n 0  DEFAULT
CreateRADIOBUTTON $wc4 $BGTSK $wlX2 " " VAR USENVALT b $fe n 1  "as set above"
label $wc4.fill     -text " "   -anchor w  -bg $BGTSK
pack configure $wc4.fill     -in $wc4 -anchor nw -side top -fill both -expand y
set VAR(USENVALT) 0

}
#
}
#============================================================= end KEY
#
}
#                                         END exec_RADIOBUTTON_cmd_TUT-TASK  for SPR-KKR
########################################################################################

}
#                                                             END tutorial_SPR-KKR
########################################################################################



