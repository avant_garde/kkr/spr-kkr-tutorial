# execunixcmd - run a UNIX program and log the output
# compilation of programs
#
#
# Copyright (C) 1998 H. Ebert

proc execunixcmd {command} {

global unixcmd unixcmd_but log w

set unixcmd $command


#echo \"########################\\n\" ; \
#echo \"##  COMMAND EXECUTED  ##\\n\" ; \
#echo \"########################\\n\" "


########################################################################################
# Run the program and arrange to read its input
proc Run {w} {
 	global unixcmd input log unixcmd_but
	if [catch {open "|$unixcmd |& cat"} input] {
	        $w.t.log insert end $input\n
	} else {
		fileevent $input readable "Log $w"
                $w.t.log insert end "#######################################################\n"
                $w.t.log insert end "###  executing the unix command: $unixcmd\n" 
                $w.t.log insert end "#######################################################\n"
                $w.t.log insert end "###  Wait for the message     EXECUTION COMPLETED   ###\n"
                $w.t.log insert end "###  stop execution using red   STOP  button        ###\n"
                $w.t.log insert end "###  or  <Control-c>  in output field               ###\n"
                $w.t.log insert end "#######################################################\n"
		$unixcmd_but config -text Stop -command Stop -bg red1 -width 10
	}

}

########################################################################################
# Read and log output from the program
proc Log {w} {
	global input log
	if [eof $input] {
		Stop
	} else {
		gets $input line
		$w.t.log insert end $line\n
		$w.t.log see end
	}
}
########################################################################################
# Stop the program and fix up the button
proc Stop {} {
	global input unixcmd_but w
	catch {close $input}
     	$unixcmd_but config -text "Run it" -command "Run $w" -bg green1 -width 10
        $w.t.log insert end "#######################################################\n"
        $w.t.log insert end "###             EXECUTION COMPLETED                 ###\n"
        $w.t.log insert end "#######################################################\n"
}

########################################################################################


set w .execcmd

toplevel_init $w "EXECUTE COMMAND" 0 0

# Create a frame for buttons and entry.
frame $w.top -borderwidth 10
pack  $w.top -side top -fill x

# Create the command buttons.
button $w.top.quit -text Quit     -command "destroy $w" -bg tomato1 -width 10
set unixcmd_but [button $w.top.run  -text "Run it" -command "Run $w" -bg green1 -width 10]
pack $w.top.quit $w.top.run -side right

# Create a labeled entry for the command
label $w.top.l -text Command: -padx 0
entry $w.top.cmd -width 20  -relief sunken \
	-textvariable unixcmd
pack $w.top.l   -side left
pack $w.top.cmd -side left -fill x -expand true

# Set up keybinding equivalents to the buttons
bind $w.top.cmd <Return> "Run $w"
bind $w.top.cmd <Control-c> Stop
focus $w.top.cmd

# Create a text widget to log the output
frame $w.t
text $w.t.log -width 80 -height 60 \
	-borderwidth 2 -relief raised -setgrid true \
 	-yscroll "$w.t.scroll set"
scrollbar $w.t.scroll -command "$w.t.log yview"

bind $w.t.log <Control-c> Stop
focus $w.t.log


pack $w.t.scroll -side right -fill y
pack $w.t.log -side left -fill both -expand true
pack $w.t -side top -fill both -expand true
 
Run $w

}
