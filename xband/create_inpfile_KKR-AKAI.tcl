########################################################################################
#                                    KKR-AKAI (CPA2002)
########################################################################################
#                                                                         create_inpfile
#     
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and 
#     adds the corresponding entries to the input mask created by   create_inpfile
#     
#     modified by Takao Kotani to deal with version of 2002
#     - supress right column of input menue
#     - allow comments indicated by #
#     - add l_max to the input parameters
#     - default values modief 
#
proc create_inpfile_KKR-AKAI { } {
    global structure_window_calls inpfile inpsuffix 
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VARLST NCPA
    global COLOR WIDTH HEIGHT FONT

global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1 
global BGWR  ; set BGWR   lavender
global BGK   ; set BGK    SteelBlue1 
global BGCLU ; set BGCLU  LightSkyBlue 
global BGE   ; set BGE    moccasin 
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1  
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1 

set w  $Wcinp
set wl 12
set we 30
set ws 160
set fe $FONT(GEN)

set wc1 $w.cols.col1 ; set wc2 $w.cols.col2  
set wc3 $w.cols.col3 ; set wc4 $w.cols.col4  

#=======================================================================================
#----------------------------------------------------------------------
#     --- KKR-CPA version ---
#
#     The following input data are read in.
#
#     go:      process is to be executed or not (go/ngo/dos/dsp).
#     file:    file name used.
#     brvtyp:  type of the bravais lattice (fcc/bcc/hcp/sc/bct/st
#              /fco/bco/bso/so/bsm/sm/trc/rhb/fct/trg).
#     a:       lattice constants in atomic unit along a axis.
#     c/a:     c/a ratio.
#     b/a:     b/a ratio.
#     alpha:   angle between b and c axis in degree.
#     beta:    angle between c and a axis in degree.
#     gamma:   angle between a and b axis in degree.
#     edelt:   small imaginary part attached to the Fermi energy.
#     ewidth:   width of the energy window covered by the energy contour.
#     reltyp:  type of relativistic treatment (nrl/sra/nrlls/srals).
#     sdftyp:  type of the parametrization of xc energy (vbh/mjw/vwn
#              /lmmjw/lmvbh/pymjw/pyvbh/pyvwn). Switch 'asa' can be
#              added, e.g., 'mjwasa' means that the atomic sphere
#              approximation (ASA) is to be exploited.
#     magtyp:  magnetic state (mag/nmag/rvrs/-mag/kick) rvrs and -mag
#              are equvalent. kick is used to kick off the system
#              to be magnetic.
#     record:  record used as input potential (init/1st/2nd).
#     outtyp:  whether to update potential file or not (update/quit).
#     bzqlty:  quality of BZ mesh (t/l/m/h/u or any integer number).
#     maxitr:  maximum number of the iteration.
#     pmix:    mixing parameter.
#
#     ntyp:    number of inequivalent sites called 'type'.
#     type:    name, starting by alphabet, specifing the type.
#     ncmp:    number of component atoms on this site
#     rmt:     muffin-tin radius for this site.
#     field:   external local magnetic field on this site.
#     lmxtyp:  maximum l value considered for this site.
#     anclr:   nuclear charge of the component atom.
#     conc:    relative probability of the occupation of the component
#              atom at the site.
#                     .........
#              above two data are repeated untill all the component
#              atoms on this site are given.
#              Then the set of data (type, ncmp, rmt, field,
#              anclr, conc, anclr, conc,...) must be repeated
#              for all the types.
#
#     natm:    number of atoms in the unit cell.
#     atmicx:  atomic position (x,y,z) in the unit cell (in unit of a).
#              or (x*v1, y*v2, z*v3), where v1,v2,or v3 are a,b, or c.
#              here a,b, and c are the primitive vectors. e.g.
#              0.5 0.5 0.5  or 0.5x 0.5y 0.5z means (0.5,0.5,0.5), but
#              0.5a 0.5b 0.5c means 0.5a+0.5b+0.5c. also
#              0.5 0.5 0.5c and 0.5b 0.5a 0.5c, etc. are allowed.
#     atmtyp:  name of the type given above.
#                     .........
#              Above two data must be repeated until all the atoms
#              in the unit cell are completed.
#
#     Return 1 is excuted at eof.
#     coded by H.Akai, April 1992, Osaka
#     KKR-CPA version coded by H.Akai, 7 Sep. 1996. Osaka
#----------------------------------------------------------------------
#------------- set the defaults
#
set VAR(go)        go
set VAR(file)      "data/$sysfile"
set VAR(edelt)     0.001
set VAR(ewidth)    1.2
set VAR(reltyp)    sra
set VAR(sdftyp)    vwn
set VAR(magtyp)    nmag
set VAR(record)    2nd
set VAR(outtyp)    update
set VAR(bzqlty)    5
set VAR(maxitr)    100
set VAR(pmix)      0.020 

#--------------------------------------------------------------------------------
#                        supply 2 colums to be filled in
#--------------------------------------------------------------------------------
pack configure $w.cols.col1 $w.cols.col2 \
               -in $w.cols -side left -fill both -expand y

#--------------------------------------------------------------------------------
#                                   COLUMN 1
#--------------------------------------------------------------------------------
CreateRADIOBUTTON $wc1 $BGSCF $wl "   CONTROL  "      VAR go      a $fe s 1 go ngo dos dsp
CreateENTRY       $wc1 $BGSCF $wl "   DATAFILE "  $we VAR file      $fe 1 " "
CreateENTRY       $wc1 $BGSCF $wl "   Im (E)   "  $we VAR edelt     $fe 3 "Ry"
CreateENTRY       $wc1 $BGSCF $wl "   E-width  "  $we VAR ewidth    $fe 3 "Ry"
CreateRADIOBUTTON $wc1 $BGSCF $wl "   RELTYP   "      VAR reltyp  a $fe s 1 nrl sra nrlls srals
CreateRADIOBUTTON $wc1 $BGSCF $wl "   SDFTYP   "      VAR sdftyp  a $fe s 1 vbh mjw vwn lmmjw lmvbh pymjw pyvbh pyvwn
CreateRADIOBUTTON $wc1 $BGSCF $wl "            "      VAR sdftyp  b $fe s 1 vbhasa mjwasa vwnasa lmmjwasa
CreateRADIOBUTTON $wc1 $BGSCF $wl "            "      VAR sdftyp  c $fe s 1 lmvbhasa pymjwasa pyvbhasa pyvwnasa
CreateRADIOBUTTON $wc1 $BGSCF $wl "   MAGTYP   "      VAR magtyp  a $fe s 1 mag nmag rvrs -mag kick
CreateRADIOBUTTON $wc1 $BGSCF $wl "   RECORD   "      VAR record  a $fe s 1 init 1st 2nd
CreateRADIOBUTTON $wc1 $BGSCF $wl "   OUTTYP   "      VAR outtyp  a $fe s 1 init update quit
CreateRADIOBUTTON $wc1 $BGSCF $wl "   BZQLTY   "      VAR bzqlty  a $fe s 1 t l m h u
CreateRADIOBUTTON $wc1 $BGSCF $wl "            "      VAR bzqlty  b $fe s 6 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
CreateSCALE       $wc1 $BGSCF $wl "   MAXITR   "  $ws 0  1000 0 3   10  VAR maxitr $fe
CreateENTRY       $wc1 $BGSCF $wl "   PMIX     "  $we VAR pmix      $fe 1 " "
#--------------------------------------------------------------------------------
#                                   COLUMN 2
#--------------------------------------------------------------------------------
#CreateSCALE       $wc2 $BGP   $wl "   FIELD: "   $ws 0   0.4   0 3 0.005 VAR FIELD  $fe
#CreateSCALE       $wc2 $BGP   $wl "   RTR: "     $ws 0.8 1.005 0 4 0.005 VAR RTR  $fe
#CreateRADIOBUTTON $wc2 $BGP   $wl "   IGO:"          VAR IGO  a $fe n 0  0 1
#CreateENTRY       $wc2 $BGP   $wl "   FILE: "    $we VAR FILE     $fe 1 " "
#CreateRADIOBUTTON $wc2 $BGP   $wl "   IREC:"         VAR IREC a $fe n 1  1 2
#CreateRADIOBUTTON $wc2 $BGP   $wl "   IOUT:"         VAR IOUT a $fe n 0  0 1
#CreateENTRY       $wc2 $BGSCF $wl "   SCF-TOL:"  $we VAR TOL      $fe 1 " "
#CreateRADIOBUTTON $wc2 $BGSCF $wl "   ITRTYP:"       VAR ITRTYP a $fe n 1  Chebychev Broyden
#CreateSCALE       $wc2 $BGSCF $wl "   MAXITR: "  $ws 0  900 0 3   10  VAR MAXITR $fe
#CreateSCALE       $wc2 $BGSCF $wl "   ALPHA: "   $ws 0  0.4 0 3 0.005 VAR ALPHA  $fe
#--------------------------------------------------------------------------------


} 
#                                                                     END create_inpfile
########################################################################################



########################################################################################
#                                                                       write_inpfile
proc write_inpfile_KKR-AKAI {Wcinp tuwas} {
    global structure_window_calls inpfile inpsuffix potfile potsuffix
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
    global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT 
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global Wprog inpsuffixlist  VAR VARLST 
    global editor edtext edxterm edoptions edsyntaxhelp edback  
    global NM IMT IMQ RWSM QMTET QMPHI 
    global buttonlist_PROGS
    global NCL NQCL IQECL ICLQ

    set tcl_precision 17

   set inpsuffix ".inp"   

   set seperator "----------------------------------"

   set file_name $inpfile
   if {$tuwas == "append" && [file exists $file_name]==1} {
      set inp [open $file_name r]
      set num_entry -1
      while {[gets $inp line] >= 0 } {
         if {[string range $line 0 15]== "#---------------"} {incr num_entry}
      }
      close $inp 
      set inp [open $file_name a]
      puts $inp "#$seperator $num_entry $seperator"
   } else {
      set inp [open $file_name w]
      puts $inp "#$seperator 0 $seperator"
   }

#-----------------------------------------------------
#    auxilary procedure trim_num to
#    trim the number  n0  and delete trailing zeros
#
# 09/02/04: deal with case that there is no "." in number; i.e. n0 is integer
# 
   proc trim_num {n0} {
      set n [string trim $n0]
      set i1 [string last "." $n]
      if {$i1==-1} {
         set i1 1 
         return $n
      } else { 
         set i1 [expr $i1 + 2]
      }
      set i2 [expr [string length $n] - 1]
      set j $i2
      for {set i $i1} {$i <= $i2} {incr i} {
	 if {$j > 0 } {
	     if {[string range $n $j $j]=="0"} {
                 set j [expr $j - 1]
                 set n [string range $n 0 $j]  
             } else {
                 set j -1
             }
         } 
      }
      return $n
   } 
#-----------------------------------------------------

#
#--------- find the value of  brvtyp   (KKR-AKAI)
#--------- corresponding to   BRAVAIS  (xband)
#
#    set TABBRAVAIS(1)  "triclinic   primitive      -1     C_i " 
#    set TABBRAVAIS(2)  "monoclinic  primitive      2/m    C_2h"
#    set TABBRAVAIS(3)  "monoclinic  base centered  2/m    C_2h"
#    set TABBRAVAIS(4)  "orthorombic primitive      mmm    D_2h"
#    set TABBRAVAIS(5)  "orthorombic base-centered  mmm    D_2h"
#    set TABBRAVAIS(6)  "orthorombic body-centered  mmm    D_2h"
#    set TABBRAVAIS(7)  "orthorombic face-centered  mmm    D_2h"
#    set TABBRAVAIS(8)  "tetragonal  primitive      4/mmm  D_4h"
#    set TABBRAVAIS(9)  "tetragonal  body-centered  4/mmm  D_4h"
#    set TABBRAVAIS(10) "trigonal    primitive      -3m    D_3d"
#    set TABBRAVAIS(11) "hexagonal   primitive      6/mmm  D_6h"
#    set TABBRAVAIS(12) "cubic       primitive      m3m    O_h "
#    set TABBRAVAIS(13) "cubic       face-centered  m3m    O_h "
#    set TABBRAVAIS(14) "cubic       body-centered  m3m    O_h "

switch -exact -- $BRAVAIS {
   13  { set VAR(brvtyp) fcc }  
   14  { set VAR(brvtyp) bcc }  
   11  { set VAR(brvtyp) hcp }  # ????
   12  { set VAR(brvtyp) sc  }  
    9  { set VAR(brvtyp) bct }  
    8  { set VAR(brvtyp) st  }  
    7  { set VAR(brvtyp) fco }  
    6  { set VAR(brvtyp) bco }  
    5  { set VAR(brvtyp) bso }  
    4  { set VAR(brvtyp) so  }  
    3  { set VAR(brvtyp) bsm }  
    2  { set VAR(brvtyp) sm  }  
    1  { set VAR(brvtyp) trc }  
#      { set VAR(brvtyp) rhb }  
#      { set VAR(brvtyp) fct }  
   10  { set VAR(brvtyp) trg }  
}

#######################################
   puts  $inp  "#  now # can be used as comment lines as well"
   puts  $inp  "#--------------------------------------------"

   set TXT "$VAR(go) $VAR(file) $VAR(brvtyp)"
   append TXT "  "  [trim_num $ALAT] "," [trim_num $COA] "," [trim_num $BOA] "," 
   append TXT [trim_num $LATANG(1)] "," [trim_num $LATANG(2)] "," [trim_num $LATANG(3)] "," 
   puts  $inp  $TXT

   set TXT "$VAR(edelt)  $VAR(ewidth)  $VAR(reltyp)"
   puts  $inp   "$TXT  $VAR(sdftyp)  $VAR(magtyp)  $VAR(record)"
   puts  $inp   "$VAR(outtyp)  $VAR(bzqlty)  $VAR(maxitr)  $VAR(pmix)"

#
#-------------- loop over the  NCL  inequivalent lattice sites
#
   puts  $inp   "$NCL"

#######################################
   puts  $inp  "#--------------------------------------------"
   puts  $inp  "#  name of type, number of components, rtr,  "
   puts  $inp  "#  field, lmax, atomic number, concentration "
   puts  $inp  "#--------------------------------------------"

   for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
      set IQ $IQECL(1,$ICL)
      set TXT ""
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set IT $ITOQ($IO,$IQ)
#         regsub "_.*\$" $TXTT($IT) "" TMP !takao
         set  TMP $TXTT($IT) 
         append TXT $TMP
      }   
      set site_type($ICL) $TXT
# takao
      set LMAX  [expr int($NLQ($IQ)-1)]    
      append TXT "  $NOQ($IQ)  0  0.0  $LMAX" ; # 2nd variable=0 >> r_mt will be set by program
      regsub -all "." $TXT " " SPACE
      
      set IT $ITOQ(1,$IQ)
      set ICONC  [expr int(100*$CONC($IT))]
      set sum_ICONC  $ICONC
      puts  $inp   " $TXT[format %5i%8i $ZT($IT) $ICONC]"
      for {set IO 2} {$IO <= $NOQ($IQ)} {incr IO} {
         set IT $ITOQ($IO,$IQ)
         set ICONC  [expr int(100*$CONC($IT))]
         set sum_ICONC  [expr $sum_ICONC + $ICONC]
         puts  $inp   "$SPACE[format %5i%8i $ZT($IT) $ICONC]"
      }   
      if {$sum_ICONC!=100} {
        give_warning . "WARNING \n\n the sum over concentrations \n\n \
              for site type $ICL   \n\n does not give   1   \n\n \
              correct later in input file \n\n $inpfile "
      }      
   }

#
#-------------- write the table of  NQ  atomic positions in cartesian units
#
   puts  $inp   "$NQ"

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set XYZ [format "%14.8f%14.8f%14.8f" $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
      puts  $inp   "$XYZ  $site_type($ICLQ($IQ))"
   }

   close $inp

} 
#                                                                   END write_inpfile
########################################################################################
