########################################################################################
#
#                    print a file according to its suffix
#
#                    dtt is a screen for messages    
#
########################################################################################
proc print_file {file suffix dtt} {#                  
global PRINTPS 
 
set PRC print_file

debug $PRC "called for file  $file  with suffix  $suffix"


#------------------------------------------------------------------------ exclude files
set exclude_list [list tar gz tar.gz bmp gif xmgr agr mpk mtv]

foreach suff $exclude_list { 
   if {$suff==$suffix} { 

     if {[winfo exists $dtt]} {writescr $dtt "\n\n cannot print $file !!!! \n"} 
     debug $PRC " cannot print $file"

     return 
   }
}


#------------------------------------------------------------------------ ps- and eps-files
if {$suffix=="ps" || $suffix=="eps" } {
    if {$dtt!=""} {writescr $dtt "\n\n printing  $file  as post-script file \n"} 
    debug $PRC " printing  post-script  $file"
 
    eval set res [catch "exec $PRINTPS $file" message]

#------------------------------------------------------------------------ dvi-files
} elseif {$suffix=="dvi" || $suffix=="eps" } {
    if {$dtt!=""} {writescr $dtt "\n\n printing  $file  as dvi-file \n"} 
    debug $PRC " printing  dvi-file  $file"
 
    eval set res [catch "exec dvips $file | $PRINTPS " message]

#------------------------------------------------------------------------ ascii-files
} else {
    if {$dtt!=""} {writescr $dtt "\n\n printing  $file  as ascii-file \n"} 
    debug $PRC " printing  ascii-file $file"
 
##    eval set res [catch "exec a2ps $file | $PRINTPS " message]
}


}
########################################################################################
