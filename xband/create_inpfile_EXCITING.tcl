########################################################################################
#                                    LMTO-AP
########################################################################################
#                                                                         create_inpfile
#     
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and 
#     adds the corresponding entries to the input mask created by   create_inpfile
#     
#  
proc create_inpfile_EXCITING {} {

global structure_window_calls inpfile inpsuffix 
global sysfile syssuffix  PACKAGE
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
global Wprog inpsuffixlist VAR VARLST NCPA
global COLOR WIDTH HEIGHT FONT
#
   pack forget $Wcinp.a.aa.edit
   pack forget $Wcinp.a.bb.write
   pack forget $Wcinp.a.bb.append

   if {$NCPA>0} {
      give_warning "." "WARNING \n\n \
      NCPA > 0   in  <create_inpfile> \n\n\n \
      program package \n\n $PACKAGE \n\n \
      cannot deal with non-stoichiometric compounds \n\n "
      destroy $Wcinp      
      set sysfile ""
      create_inpfile     
      return
    }
    set inpsuffix ".in"   
} 
#                                                                     create_inpfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                                       write_inpfile
proc write_inpfile_EXCITING {Wcinp tuwas} {

global structure_window_calls inpfile inpsuffix potfile potsuffix
global sysfile syssuffix  PACKAGE
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT 
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global Wprog inpsuffixlist  VAR VARLST 
global editor edtext edxterm edoptions edsyntaxhelp edback  
global NM IMT IMQ RWSM QMTET QMPHI 
global buttonlist_PROGS
global NCL NQCL IQECL ICLQ
global RQU RQV RQW

set tcl_precision 17
set PI 3.141592653589793238462643              

#
set file_name $inpfile
if {$tuwas == "append" && [file exists $file_name]==1} {
   set inp [open $file_name a]
} else {
   set inp [open $file_name w]
}


#
#--------------------- get cryst coordinates RQU RQV RQW
#
convert_basis_vectors_RQ cart_to_cryst
puts $inp "tasks"
puts $inp "0"
puts $inp ""

puts $inp "scale"
puts  $inp  " [format %20.10f $ALAT]"
puts $inp ""

puts $inp "sppath"
puts $inp " './'"
puts $inp ""

puts $inp "avec"
puts $inp 


for {set I 1} {$I <= 3} {incr I} {
   set TXT "[format %20.10f $RBASX($I)]"
   append TXT "[format %20.10f $RBASY($I)]"
   append TXT "[format %20.10f $RBASZ($I)]"
   puts  $inp $TXT
}
puts $inp ""

puts $inp "atoms"
puts  $inp  "[format %10i    $NT]    : nspecies"
for {set IT 1} {$IT <= $NT} {incr IT} {
    puts $inp " '$TXTT($IT).in'"
    puts $inp "[format %10i    $NAT($IT)]    : natoms"
    for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
     set IQ  $IQAT($IA,$IT)
     set TXT 0.0
     set UVW [format "%12.7f%12.7f%12.7f%12.7f%12.7f%12.7f" $RQU($IQ) $RQV($IQ) $RQW($IQ) $TXT $TXT $TXT]
      puts  $inp "$UVW"
}
}
puts $inp ""



puts $inp ""

puts $inp "ngridk"
puts $inp " 10 10 10"
puts $inp ""


puts $inp "autormt"
puts $inp " .true."
puts $inp ""

puts $inp "rmtapm"
puts $inp " 30.0  1.0"



#=======================================================================

   close $inp

} 
#  write_inpfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

