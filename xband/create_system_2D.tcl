# setting up the structure  for   2D systems
#
#
#   NQ       total number of lattice sites in unit cell (independent of occupation)
#   NCL      number of inequivalent lattice sites
#   NQCL     number of equivalent sites IQ for class ICL with ICL = 1,..,NCL  
#   NT       number of atom types 
#   NAT      number of equivalent sites IQ occupied by atom type IT with  IT = 1,..,NT
#   CONC     concentration of atom type IT (on a site)
#
#   for ordered compounds    NCL        == NT
#                            NQCL(ICL)  == NAT(IT)
#                            CONC(IT)   == 1
#
#
# Copyright (C) 2002 H. Ebert
#
########################################################################################



proc create_system_2D { } {
########################################################################################
########################################################################################
#
#                         CREATE AND UPDATE WINDOWS  -  MAIN MENUE
#
#
########################################################################################
########################################################################################
proc structure_window_2D { } {

global structure_window_calls sysfile syssuffix  sysfile_3DSURF  
global BOA COA SPACEGROUP  STRUCTURE_TYPE SPACEGROUP_AP TABCHSYM iprint Wcsys
global system NQ NCL NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT TXTT0  RWS ZT
global RBASX RBASY RBASZ present_SUBSYSTEM
global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS ALAT IQ1Q IQ2Q
global W_sites_list NT WYCKOFFQ WYCKOFFCL
global editor edback edxterm edoptions xband_path 
global SPACEGROUPS_FILE STRUCTURE_FILE STRUCTURE_SETUP_MODE IQECL
global create_structure_button_list create_structure_lock
global VISUAL
global COLOR WIDTH HEIGHT FONT
global LATTICE_TYPE ZRANGE_TYPE BGSU SU_NAME DIMENSION
global NQ_display NQ_display_overlap IQ_display_start IQ_display_end
global NQ_bulk_L NQ_bulk_R IQ0_Q ICLQ
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R

set PRC "structure_window_2D"

debug $PRC WRHEAD

if {$structure_window_calls > 0} {destroy $Wcsys}

incr structure_window_calls

set DIMENSION "2D"

    set win $Wcsys
    if {[winfo exists $win] == 1} {destroy $win}
    toplevel $win -width 400 -height 300 -visual $VISUAL
    wm geometry     $win +300+200
    wm positionfrom $win user
    wm title        $win "CREATE 2D-SYSTEM"

    insert_topbuttons $Wcsys compile_h.hlp

    $Wcsys.a.l configure -state disabled

    bind $Wcsys.a.e <Button-1> {
#  if [compile_focus ""] {
#    writescr $Wcsys.d.tt "input accepted; press once more \"close\""
#  } else {
    destros $Wcsys; unlock_list ; Bend
#  }
}

    bind $Wcsys.a.h <Button-1> { 
        execunixcmd "cat $xband_path/help/create_system_h.hlp"
    }

debug $PRC "################################### section  c"
#---------------------------------------------------------------------------
#                            section  "c"
#---------------------------------------------------------------------------
  frame $Wcsys.c -borderwidth {2} -height {30}  -relief {raised}
  pack $Wcsys.c -in $Wcsys -expand yes -fill x

#---------------------------------------------------------------------------
  frame $Wcsys.c.mag -borderwidth {2} -height {30} 

  label $Wcsys.c.mag.label1 -text {magnetisation (cart. coord.)}

  frame $Wcsys.c.mag.f2  -borderwidth {2}  -height {30}  -width {30}

  label $Wcsys.c.mag.f2.labM -text {M = }

  entry $Wcsys.c.mag.f2.e_Mx \
    -textvariable {MAG_DIR(1)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  entry $Wcsys.c.mag.f2.e_My \
    -textvariable {MAG_DIR(2)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  entry $Wcsys.c.mag.f2.e_Mz \
    -textvariable {MAG_DIR(3)} -font $FONT(GEN) -width {3} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  pack configure $Wcsys.c.mag.label1  -expand true -fill x
  pack configure $Wcsys.c.mag.f2

  pack configure $Wcsys.c.mag.f2.labM  -expand true -fill x  -side left
  pack configure $Wcsys.c.mag.f2.e_Mx  -side left
  pack configure $Wcsys.c.mag.f2.e_My  -side left
  pack configure $Wcsys.c.mag.f2.e_Mz  -side left
#---------------------------------------------------------------------------

##  if {"$sysfile$syssuffix" == ".sys"} {
     set sysfile "system"
##  }

  frame $Wcsys.c.name -borderwidth {2} -height {30}

  label $Wcsys.c.name.label1 -text {name of system-file}

  entry $Wcsys.c.name.entry \
    -textvariable {sysfile} -font $FONT(GEN) -width {30} -bg $COLOR(ENTRY)\
    -fg $COLOR(ENTRYFG)

  pack configure $Wcsys.c.name.label1  -expand true -fill x

  pack configure $Wcsys.c.name.entry  
#---------------------------------------------------------------------------
  button $Wcsys.c.button0D -text "create 0D-system" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN)  \
         -command "destros $Wcsys; unlock_list ; create_system_0D  "
  
  button $Wcsys.c.button3D -text "create 3D-system" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN)  \
         -command "destros $Wcsys; unlock_list ; create_system "
  
  button $Wcsys.c.close -text "CLOSE" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
         -command "destros $Wcsys; unlock_list "
  
  button $Wcsys.c.goon -text "DONE - RETURN" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(DONE) \
         -command "structure_window-done-RETURN" -state disabled

#---------------------------------------------------------------------------
    set create_structure_button_list \
            [list  $Wcsys.struc.mode.button1 \
                   $Wcsys.struc.mode.button2 \
                   $Wcsys.struc.mode.button3 \
                   $Wcsys.struc.mode.button4 ]

#   pack $Wcsys.c.mag $Wcsys.c.name $Wcsys.c.close $Wcsys.c.goon \
#   suppress magnetic stuff
    pack $Wcsys.c.name $Wcsys.c.close $Wcsys.c.button0D $Wcsys.c.button3D  \
	               $Wcsys.c.goon   \
  	 -in $Wcsys.c  -side left -fill x -expand yes

    # structure information
    frame $Wcsys.struc -relief raised -borderwidth 2 

    frame $Wcsys.struc.mode


    label $Wcsys.struc.mode.title -text "Specify structure via:  "

    button $Wcsys.struc.mode.button0 -text "surface \n of 3D-system" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
         -command "set create_structure_lock 1; set STRUCTURE_SETUP_MODE \"3D SURFACE\";\
                   set LATTICE_TYPE homogeneous ; set ZRANGE_TYPE extended ; \          
                   structure_specify_3D_surface "

    button $Wcsys.struc.mode.button1 -text "homogeneous\n extended lattices" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
         -command "set create_structure_lock 1; set STRUCTURE_SETUP_MODE \"2D LAYERS\";\
                   set LATTICE_TYPE homogeneous ; set ZRANGE_TYPE extended ; \          
                   structure_specify_2D_lattice "

    button $Wcsys.struc.mode.button2 -text "heterogeneous\n extended lattices" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
         -command "set create_structure_lock 1; set STRUCTURE_SETUP_MODE \"2D LAYERS\";\
                   set LATTICE_TYPE heterogeneous ; set ZRANGE_TYPE extended ; \
                   structure_specify_2D_lattice"

    button $Wcsys.struc.mode.button3 -text "homogeneous\n slab lattices" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
         -command "set create_structure_lock 1; set STRUCTURE_SETUP_MODE \"2D LAYERS\";\
                   set LATTICE_TYPE homogeneous ; set ZRANGE_TYPE slab ; \          
                   structure_specify_2D_lattice "

    button $Wcsys.struc.mode.button4 -text "heterogeneous\n slab lattices" \
         -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE)  \
         -command "set create_structure_lock 1; set STRUCTURE_SETUP_MODE \"2D LAYERS\";\
                   set LATTICE_TYPE heterogeneous ; set ZRANGE_TYPE slab ; \          
                   structure_specify_2D_lattice"

    button $Wcsys.struc.mode.unlock -text "unlock" \
           -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(LIGHTBLUE) \
	   -command "set create_structure_lock 0; structure_unlock"

    proc structure_unlock {} {
    global create_structure_button_list
       if {[llength $create_structure_button_list]>0} { 
          foreach x $create_structure_button_list {$x configure -state normal} 
       }
    }

    button $Wcsys.struc.mode.edit -text "edit \n database"  \
          -width $WIDTH(BUT1) -height $HEIGHT(BUT1) -bg $COLOR(GREEN) \
	  -command "exec $edxterm $editor $edoptions $xband_path/locals/2D_structure.dat" 

#    pack $Wcsys.struc.mode.title $Wcsys.struc.mode.button1\
#	    $Wcsys.struc.mode.button2 $Wcsys.struc.mode.button3 \

    pack $Wcsys.struc.mode.title      \
	    $Wcsys.struc.mode.button0 $Wcsys.struc.mode.button1 \
	    $Wcsys.struc.mode.button2 $Wcsys.struc.mode.button3 \
	    $Wcsys.struc.mode.button4 $Wcsys.struc.mode.unlock $Wcsys.struc.mode.edit  \
	    -side left -expand yes

   if {$create_structure_lock==1} {
       if {[llength $create_structure_button_list]>0} {
      	  foreach x $create_structure_button_list {$x configure -state disabled}
       }
    }
    
#---------------------------------------------------------------------------------------
debug $PRC "################################### section  b"

    frame $Wcsys.struc.b

    set Latpar $Wcsys.struc.b.latpar
    frame $Latpar

    set widlab  5
    set wident 15

    frame $Latpar.info
    label $Latpar.info.a -text " lattice parameters          A = " -pady 5
    label $Latpar.info.b -width $wident -font $FONT(GEN) \
	    -textvariable ALAT
    label $Latpar.info.c -text "\[a.u.\]"
    pack $Latpar.info.a $Latpar.info.b $Latpar.info.c -side left

    frame $Latpar.first
    frame $Latpar.secon
    frame $Latpar.third

    label $Latpar.first.label1 -width $widlab -text "a = " -pady 5  -justify right
    label $Latpar.first.label2 -width $widlab -text "b = " -pady 5  -justify right
    label $Latpar.first.label3 -width $widlab -text "c = " -pady 5  -justify right

    label $Latpar.secon.label1 -width $widlab -text "      " -pady 5 -justify right
    label $Latpar.secon.label2 -width $widlab -text "b/a = " -pady 5 -justify right
    label $Latpar.secon.label3 -width $widlab -text "c/a = " -pady 5 -justify right
				      
    label $Latpar.third.label1 -width $widlab -font $FONT(SBL) \
	   -text "a = " -pady 5 -justify right
    label $Latpar.third.label2 -width $widlab -font $FONT(SBL) \
	   -text "b = " -pady 5 -justify right
    label $Latpar.third.label3 -width $widlab -font $FONT(SBL) \
	   -text "g = " -pady 5 -justify right

    label $Latpar.secon.a(1) -width $wident -font $FONT(GEN)
    label $Latpar.secon.a(2) -width $wident -font $FONT(GEN) \
	    -textvariable BOA
    label $Latpar.secon.a(3) -width $wident -font $FONT(GEN)  \
	    -textvariable COA
 
    for {set i 1} {$i <=3} {incr i} {
	label $Latpar.first.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATPAR($i) -justify left
	label $Latpar.third.a($i) -width $wident -font $FONT(GEN) \
	    -textvariable LATANG($i) -justify left
	
        pack $Latpar.first.label$i  $Latpar.first.a($i) -side left
        pack $Latpar.secon.label$i  $Latpar.secon.a($i) -side left
        pack $Latpar.third.label$i  $Latpar.third.a($i) -side left
    }


    pack $Latpar -side left
    pack $Latpar.info $Latpar.first  $Latpar.secon  $Latpar.third  -fill both \
	   -expand yes -anchor c


    set Info $Wcsys.struc.b.info

    frame $Info
    pack $Info -side left

    set ww1 20
    set ww2 5
    set ww3 5

    frame $Info.f0
    label $Info.f0.c1 -width $ww1 -anchor w -justify left -text "number of sites"
    label $Info.f0.c2 -width $ww2 -anchor w -justify left -text "NQ"
    label $Info.f0.e  -width $ww3 -font $FONT(GEN) -textvariable NQ
		      
    frame $Info.f1    
    label $Info.f1.c1 -width $ww1 -anchor w -justify left -text "inequivalent sites"
    label $Info.f1.c2 -width $ww2 -anchor w -justify left -text "NCL"
    label $Info.f1.e  -width $ww3 -font $FONT(GEN) -textvariable NCL

    frame $Info.f2    
    label $Info.f2.c1 -width $ww1 -anchor w -justify left -text "atom types"
    label $Info.f2.c2 -width $ww2 -anchor w -justify left -text "NT"
    label $Info.f2.e  -width $ww3 -font $FONT(GEN) -textvariable NT

    frame $Info.f3    
    label $Info.f3.c1 -width $ww1 -anchor w -justify left -text "structure type"
    label $Info.f3.e  -font $FONT(GEN) -textvariable STRUCTURE_TYPE

    frame $Info.f4    
    label $Info.f4.c1 -width $ww1 -anchor w -justify left -text "lattice type"
    label $Info.f4.e  -font $FONT(GEN) -textvariable LATTICE_TYPE

    frame $Info.f5    
    label $Info.f5.c1 -width $ww1 -anchor w -justify left -text "z-range"
    label $Info.f5.e  -font $FONT(GEN) -textvariable ZRANGE_TYPE


    set itop 5

    for {set i 0} {$i <=$itop} {incr i} {
        pack $Info.f$i.c1 -side left -anchor w
        if {$i<3} {pack $Info.f$i.c2 -side left -anchor w}
        pack $Info.f$i.e  -side left -anchor w
        pack $Info.f$i    -side top  -anchor nw
    }
    
#---------------------------------------------------------------------------------------

    pack  $Wcsys.struc.mode  $Wcsys.struc.b  -expand yes -fill both
    pack  $Wcsys.struc -in   $Wcsys          -expand yes -fill both


#=======================================================================================
#                                    site specification                     
#=======================================================================================
debug $PRC "################################### site specification for NQ=$NQ"

    if {$NQ != 0 } {

    frame  $Wcsys.site -relief raised -borderwidth 2

    set wI   5
    set wR   12
    set wT   7
    set width_TYP 15

    set ws $Wcsys.site
   
#.....................................................................................
#                                                      DON'T  initialize
#.....................................................................................


set IQ 0
for {set ICL  1} {$ICL  <= $NCL}        {incr ICL } {
for {set IQCL 1} {$IQCL <= $NQCL($ICL)} {incr IQCL} {

if {$IQ == 0} {
  debug $PRC "################################### site specification init for IQ=0"

  frame $ws.th ; pack  $ws.th
#  label $ws.th.lab -width 45 -anchor w  -justify left -text "click line to specify occupation"
#  pack  $ws.th.lab -expand true -fill x -side left


  button $ws.th.rasmol -text "show structure" -width 15 -height $HEIGHT(BUT1)  \
	 -bg $COLOR(BUT1) -command "sites_graphik" -state disabled
  button $ws.th.spheres -text "calculate R_WS + \nempty spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -command "run_spheres" -state disabled
  button $ws.th.scale_R_WS -text "adjust R_WS\nby scaling" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "scale_wigner-seitz_radii ; structure_window_update_site_table " 
  button $ws.th.set_same_R_WS -text "set same R_WS\nfor all spheres" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "set_same_wigner-seitz_radii ; structure_window_update_site_table " 
  button $ws.th.suppress_symmetry -text "suppress\nsymmetry" -width 15  \
	 -height $HEIGHT(BUT1) -bg $COLOR(BUT2) -state disabled \
	 -command "suppress_symmetry ; structure_window_update_site_table " 
  pack   $ws.th.rasmol $ws.th.spheres  $ws.th.scale_R_WS $ws.th.set_same_R_WS   \
	 $ws.th.suppress_symmetry   \
	 -expand true -fill x -side right


  eval {scrollbar $ws.sby -background {grey77} \
	 -command [list $ws.li yview] -orient vertical}
  eval {scrollbar $ws.sbx -background {grey77} \
	 -command [list $ws.li xview] -orient horizontal}

  eval {listbox $ws.li -background $COLOR(LIGHTBLUE) -height {15}  -font $FONT(GEN) \
          -yscrollcommand [list $ws.sby set] \
          -xscrollcommand [list $ws.sbx set] } " "

## do not allow overwriting of occupation !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##bind $ws.li <Button-1> {if {$STRUCTURE_SETUP_MODE !="PER PEDES" &&  $STRUCTURE_SETUP_MODE != "MULTI LAYERS"} {
##                           sites_occupation [getvalue [string trim [Selektion %W %y]] 0 " "] 1; Bend} } 
  bind $ws.li <Double-Button-1> {Bend}
#  bind $ws.li <Leave> {Bend}
#  bind $ws.li <Triple-Button-1> {Bend}

  pack $ws.sbx  -fill x -side bottom
  pack $ws.sby  -fill y -side left
  pack $ws.li   -expand true -fill both -side right

  set W_sites_list $ws.li
  
  set li_he_txt1 " IQ CL WS        X           Y           Z"
  set li_he_txt2 "          R_WS    NLQ NOQ  IT TXTT     CONC"
  $ws.li insert end "$li_he_txt1$li_he_txt2"

}

incr IQ

  set aux [format "%3i%3i  %1s%12.6f%12.6f%12.6f" \
           $IQ $ICL $WYCKOFFQ($IQ) $RQX($IQ) $RQY($IQ) $RQZ($IQ)]

  $ws.li insert end $aux

# debug $PRC  " IQ $IQ   $IQ1Q($IQ) ...  $IQ2Q($IQ)  SETUP_MODE $STRUCTURE_SETUP_MODE  "

}
}

pack $Wcsys.site -in $Wcsys -expand yes -fill both
}
#----------------------------------------------------------  site specification --- END


#=======================================================================================
#                      site information available  --  insert type information
#=======================================================================================
#                                                             
set IQ_display_start 1
set IQ_display_end   $NQ

if {$NQ > 0} {

   write_actual_lattice_info $PRC

   debug $PRC "################################### insert type information for NQ=$NQ"
   debug $PRC "################################### STRUCTURE_SETUP_MODE=$STRUCTURE_SETUP_MODE"
   debug $PRC  "ZRANGE_TYPE $ZRANGE_TYPE "

   if {$STRUCTURE_SETUP_MODE == "3D SURFACE" } { 
      debug $PRC  "sysfile (old) $sysfile"
      debug $PRC  "sysfile (new) $sysfile_3DSURF"
      debug $PRC  "dimension     $DIMENSION"
      set sysfile $sysfile_3DSURF
   }
#
#----------------------------------------------------------- store initial configuration
#
   set NQ_0 $NQ
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set NOQ_0($IQ) $NOQ($IQ)
      set NLQ_0($IQ) $NLQ($IQ)
      set RWS_0($IQ) $RWS($IQ)
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set  IT              $ITOQ($IO,$IQ)
         set  ITOQ_0($IO,$IQ) $ITOQ($IO,$IQ)
         set     TXTT0_0($IT)    $TXTT0($IT)
         set        ZT_0($IT)       $ZT($IT)
         set      CONC_0($IT)     $CONC($IT)
      }
      set RQX_0($IQ) $RQX($IQ) 
      set RQY_0($IQ) $RQY($IQ) 
      set RQZ_0($IQ) $RQZ($IQ) 
   }
   set RBASX_0(3) $RBASX(3)
   set RBASY_0(3) $RBASY(3)
   set RBASZ_0(3) $RBASZ(3)

#.......................................................................................
#.............................................................................. extended
if { $ZRANGE_TYPE=="extended" } {

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      if {$IQ <= $NQ_bulk_L} {
         set WYCKOFFQ($IQ) "L"
      } elseif {$IQ > [expr $NQ - $NQ_bulk_R]} {
         set WYCKOFFQ($IQ) "R"
      } else {
         set WYCKOFFQ($IQ) "S"
      } 
   }
#
#---------------------------------------------------------------------- deal with spacer
#
write_actual_lattice_info $PRC

   set present_SUBSYSTEM spacer
   debug $PRC "################################### dealing with $present_SUBSYSTEM"

   run_findsym structure_via_table_run_findsym

   set ICL1_S $NQ
   set ICL2_S 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      if { $IQ0>$NQ_bulk_L &&  $IQ0<=[expr $NQ - $NQ_bulk_R] } {
         set ICLQ_S($IQ0) $ICLQ($IQ)
         if {$ICL1_S>$ICLQ_S($IQ0)} {set ICL1_S $ICLQ_S($IQ0)}
         if {$ICL2_S<$ICLQ_S($IQ0)} {set ICL2_S $ICLQ_S($IQ0)}
      }
   }
#
#--------------------------------------------------------------------- deal with bulk(L)
#
write_actual_lattice_info $PRC

   set present_SUBSYSTEM "bulk(L)"
   debug $PRC "################################### dealing with $present_SUBSYSTEM"

   set RBASX(3) $RBASX_L(3)
   set RBASY(3) $RBASY_L(3)
   set RBASZ(3) $RBASZ_L(3)

   set NQ $NQ_bulk_L

   run_findsym structure_via_table_run_findsym

   set ICL1_L $NQ
   set ICL2_L 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      set ICLQ_L($IQ0) $ICLQ($IQ)
      if {$ICL1_L>$ICLQ_L($IQ0)} {set ICL1_L $ICLQ_L($IQ0)}
      if {$ICL2_L<$ICLQ_L($IQ0)} {set ICL2_L $ICLQ_L($IQ0)}
   }
#
#--------------------------------------------------------------------- deal with bulk(R)
#
write_actual_lattice_info $PRC

   set present_SUBSYSTEM "bulk(R)"
   debug $PRC "################################### dealing with $present_SUBSYSTEM"

   set RBASX(3) $RBASX_R(3)
   set RBASY(3) $RBASY_R(3)
   set RBASZ(3) $RBASZ_R(3)

   set NQ $NQ_bulk_R
   set NQ_LPS [expr $NQ_0 - $NQ_bulk_R]
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 [expr $NQ_LPS + $IQ] 
      set NOQ($IQ) $NOQ_0($IQ0)
      set NLQ($IQ) $NLQ_0($IQ0)
      set RWS($IQ) $RWS_0($IQ0)
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set  IT            $ITOQ_0($IO,$IQ0)
         set  ITOQ($IO,$IQ) $ITOQ_0($IO,$IQ0)
         set     TXTT0($IT)     $TXTT0_0($IT)
         set        ZT($IT)        $ZT_0($IT)
         set      CONC($IT)      $CONC_0($IT)
      }
      set RQX($IQ) $RQX_0($IQ0) 
      set RQY($IQ) $RQY_0($IQ0) 
      set RQZ($IQ) $RQZ_0($IQ0) 
   }

   run_findsym structure_via_table_run_findsym

   set ICL1_R $NQ
   set ICL2_R 1
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 [expr $NQ_LPS + $IQ0_Q($IQ)]
      set ICLQ_R($IQ0) $ICLQ($IQ)
      if {$ICL1_R>$ICLQ_R($IQ0)} {set ICL1_R $ICLQ_R($IQ0)}
      if {$ICL2_R<$ICLQ_R($IQ0)} {set ICL2_R $ICLQ_R($IQ0)}
   }
#

write_actual_lattice_info $PRC

} else {
#.................................................................................. slab
#  scale RBAS(3) to avoid spurious translational symmetry due to 3D-mode of findsym

   set scale 1.2
   set RBASX(3) [expr $RBASX(3)*$scale]
   set RBASY(3) [expr $RBASY(3)*$scale]
   set RBASZ(3) [expr $RBASZ(3)*$scale]

   run_findsym structure_via_table_run_findsym

   set NQ_bulk_L 0
   set NQ_bulk_R 0
   set ICL2_L    0
   set ICL1_S    1

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IQ0 $IQ0_Q($IQ)
      set ICLQ_S($IQ0) $ICLQ($IQ)
   }
}
#.......................................................................................
#.......................................................................................
#
   set NQ $NQ_0

   set RBASX(3) $RBASX_0(3)
   set RBASY(3) $RBASY_0(3)
   set RBASZ(3) $RBASZ_0(3)

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

      set NOQ($IQ) $NOQ_0($IQ)
      set NLQ($IQ) $NLQ_0($IQ)
      set RWS($IQ) $RWS_0($IQ)

      set RQX($IQ) $RQX_0($IQ) 
      set RQY($IQ) $RQY_0($IQ) 
      set RQZ($IQ) $RQZ_0($IQ) 

      set KDONEQ($IQ) 0
   }

   set NT 0
   set NCL 0
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      if {$KDONEQ($IQ) == 0} {
         set KDONEQ($IQ) 1
         incr NCL 
         set WYCKOFFCL($NCL) "-"
         if { $IQ <= $NQ_bulk_L } {
            set ICLQ($IQ) $ICLQ_L($IQ)
         } elseif { $IQ<=[expr $NQ - $NQ_bulk_R] } {
            set ICLQ($IQ) [expr $ICL2_L + $ICLQ_S($IQ) - $ICL1_S + 1]
         } else { 
            set ICLQ($IQ) [expr $ICL2_S + $ICLQ_R($IQ) - $ICL1_R + 1]
         }

         for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
            incr NT
            set IT0           $ITOQ_0($IO,$IQ)
            set ITOQ($IO,$IQ) $NT
            set TXTT0($NT)    $TXTT0_0($IT0)
            set    ZT($NT)       $ZT_0($IT0)
            set  CONC($NT)     $CONC_0($IT0)
            for {set JQ [expr $IQ+1] } {$JQ <= $NQ} {incr JQ} {
               if { $JQ <= $NQ_bulk_L } {
                  set ICLQ($JQ) $ICLQ_L($JQ)
               } elseif { $JQ<=[expr $NQ - $NQ_bulk_R] } {
                  set ICLQ($JQ) [expr $ICL2_L + $ICLQ_S($JQ) - $ICL1_S + 1]
               } else { 
                  set ICLQ($JQ) [expr $ICL2_S + $ICLQ_R($JQ) - $ICL1_R + 1]
               }

	       if {$ICLQ($IQ)==$ICLQ($JQ)} {
                  set KDONEQ($JQ) 1
                  for {set JO 1} {$JO <= $NOQ($IQ)} {incr JO} {
                     set ITOQ($JO,$JQ) $NT                  
                  }
               }
            }
         }
      }
   }


   for {set ICL 1} {$ICL <= $NCL} {incr ICL} {set NQCL($ICL) 0}

   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set ICL $ICLQ($IQ)
      incr NQCL($ICL)
      set IQECL($NQCL($ICL),$ICL) $IQ
   }
#
#--------------------------------------------------------- deal with names of atom types
#
for {set IT 1} {$IT <= $NT} {incr IT} {
 puts "XXX aaaa $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
}

deal_with_names_of_atom_types

#
#--------------------------------------------------------------------- update site table
#

structure_window_update_site_table

write_actual_lattice_info $PRC

#
#----------------------------------------------------------- sites_occupation_get_system
#
if {$STRUCTURE_SETUP_MODE != "3D SURFACE" } { sites_occupation_get_system }

for {set IT 1} {$IT <= $NT} {incr IT} {
 puts "XXX bbbb $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
}


$Wcsys.c.goon             configure -state normal
$ws.th.rasmol             configure -state normal
$ws.th.spheres            configure -state normal
$ws.th.scale_R_WS         configure -state normal
$ws.th.set_same_R_WS      configure -state normal
$ws.th.suppress_symmetry  configure -state normal

for {set IT 1} {$IT <= $NT} {incr IT} {
 puts "XXX cccc $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
}

}
#=======================================================================================

#*************************************************
global QUICK
if {$QUICK==1} {

set create_structure_lock 1
set STRUCTURE_SETUP_MODE "2D LAYERS"
set LATTICE_TYPE homogeneous 
set ZRANGE_TYPE extended         

puts "******************************** "
puts "******************************** $Wcsys"

destros $Wcsys
set QUICK 0
structure_specify_3D_surface 

}
#*************************************************

}
#                                                                structure_window_2D END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                         STRUCTURE 2D LAYERS
########################################################################################
#                                                enter structure for homogeneous lattice
proc structure_specify_2D_lattice {  } {
global BRAVAIS NCL LATPAR LATANG flag Wcsys
global TABBRAVAIS BOHRRAD ALAT 
global Wcrml xband_path
global COLOR WIDTH HEIGHT FONT

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name 
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ          ML_START_IL1 ML_START_IL2
global ML_NLAY 
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select STRUCTURE_SETUP_MODE
global struc_L_eq_struc_R bulk_L_eq_bulk_R OCCUPATION_OF_LAYERS
global NSUBSYS_min
global LATTICE_TYPE ZRANGE_TYPE
global NSUBSYS   STRUCTURE_SEQUENCE
global N_rep_SU_bulk_L N_rep_SU_bulk_R
global N_rep_SU_a N_rep_SU_b

set tcl_precision 17
set PRC structure_specify_2D_lattice

#
#-------------------------------------------------------- read 2D_structures.dat
#
read_multilayers

set NML1    $ML_NML(1)   
set NML2    $ML_NML(2)   
set NSUBSYS $ML_NSU

if { $NML1 =="0" } {return}

#=======================================================================================
#                            variable list
#
#  LATTICE_TYPE              homogeneous        heterogeneous
#  OCCUPATION_OF_LAYERS      symmetric          NON-symmetric
#  bulk_L_eq_bulk_R            YES                NO
#  struc_L_eq_struc_R          YES                NO
#  STRUCTURE_SEQUENCE        "1|2|1..."         "2|1|2..."
#

set bulk_L_eq_bulk_R   YES
set struc_L_eq_struc_R YES

set ML_SUBSYS_REF 1
set ML_SCALE_ISD  0.0
set ML_SCALE_C1   0.0
set ML_SCALE_C2   0.0
#=======================================================================================
  
set TXT "Structure set-up for layered system with $LATTICE_TYPE $ZRANGE_TYPE lattice"
set flag 3 
set Wcrml .w_crml
toplevel_init $Wcrml "$TXT" 0 0
wm positionfrom $Wcrml user
wm sizefrom $Wcrml ""

set tcl_precision 17

reset_struc_parameter

set BGhead $COLOR(LIGHTBLUE)
set BGCOL1 $COLOR(LIGHTSALMON1)
set BGCOL2 $COLOR(LIGHTSALMON1)
set BGCOL3 $COLOR(STEELBLUE1) 
 
frame $Wcrml.head -relief raised -borderwidth 2 -bg $BGhead
frame $Wcrml.head.a -bg $BGhead
label $Wcrml.head.a.lab1 -text "   lattice parameter  A"   \
      -bg $BGhead -pady 10
entry $Wcrml.head.a.entry -width 15 -relief sunken -font $FONT(GEN)  \
      -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) -textvariable ALAT
button $Wcrml.head.a.convert -width 17 -height 3 \
      -text "Angstroem to a.u. \n if necessary " -pady 5 \
      -bg $COLOR(ORANGE) -command "structure_convert_ALAT $Wcrml.head.a.convert" 
button $Wcrml.head.a.cancel  \
      -text "CLOSE " -width 17 -height 3 -bg $COLOR(CLOSE) \
        -command "destroy $Wcrml" -pady 5

frame  $Wcrml.head.a.subsys_ref

if {$LATTICE_TYPE=="heterogeneous"} {

   set w_subsys_ref $Wcrml.head.a.subsys_ref
   
   label $w_subsys_ref.lab1 -text \
    "ALAT refers to structure unit"  -bg $BGhead 
   frame $w_subsys_ref.sel -bg $BGhead
   radiobutton $w_subsys_ref.sel.rad1 -relief flat -font $FONT(GEN) -bg $BGhead \
   	       -variable ML_SUBSYS_REF -value 1 -text "1"  \
               -highlightthickness 0 	
   radiobutton $w_subsys_ref.sel.rad2 -relief flat -font $FONT(GEN) -bg $BGhead \
   	       -variable ML_SUBSYS_REF -value 2 -text "2"  \
               -highlightthickness 0 
   pack $w_subsys_ref.sel.rad1 $w_subsys_ref.sel.rad2 \
   	        -side left -anchor c -fill x -expand yes
   label $w_subsys_ref.lab2 -text "of heterogeneous lattice"  -bg $BGhead
   pack $w_subsys_ref.lab1 -side top -anchor w
   pack $w_subsys_ref.sel  -side top -anchor c -fill x -expand yes
   pack $w_subsys_ref.lab2 -side top -anchor w -fill x -expand yes
}   


pack $Wcrml.head.a.lab1        -padx 5  -side left 
pack $Wcrml.head.a.entry       -padx 5  -side left
pack $Wcrml.head.a.convert     -padx 5  -side left
pack $Wcrml.head.a.subsys_ref  -padx 5  -side left -pady 2 -anchor c
pack $Wcrml.head.a.cancel      -padx 25 -side right -fill x
pack $Wcrml.head.a -side top   -expand yes -fill x
pack $Wcrml.head -side top     -expand yes -fill x

frame  $Wcrml.col1 -bg $BGCOL1
frame  $Wcrml.col2 -bg $BGCOL2
frame  $Wcrml.col3 -bg $BGCOL3

pack  $Wcrml.col1 $Wcrml.col2 $Wcrml.col3 -side left -fill y -anchor n

set COL1 $Wcrml.col1
set COL2 $Wcrml.col2
set COL3 $Wcrml.col3

set wlb2 7
set wlb3 5
set wbut 3

set wlab1 10
set lslid 180

set NMLMAX 150




#=======================================================================================
#                                     column 1  
#=======================================================================================

frame $COL1.head -bg $BGCOL1
label $COL1.head.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20  -pady 5
label $COL1.head.lab1 -text "specify layer system " -bg $BGCOL1 -padx 20   -pady 5
label $COL1.head.lab2 -text "bulk(L) ||||||||| bulk(R)" -bg $BGCOL1 -padx 20 -pady 5
pack $COL1.head.space1 $COL1.head.lab1 -anchor w -side top  
pack $COL1.head.lab2                   -anchor c -side top  
pack $COL1.head  -fill x

if { $ZRANGE_TYPE=="slab" } {$COL1.head.lab2 configure -text "vac ||||||||| vac"}

frame $COL1.sym -bg $BGCOL1
label $COL1.sym.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20 
label $COL1.sym.lab1 -text "occupation of layers" -bg $BGCOL1 -padx 20 
radiobutton $COL1.sym.a  -relief flat  -highlightthickness 0 -font $FONT(GEN)\
            -variable OCCUPATION_OF_LAYERS -value symmetric -text "symmetric"\
            -bg $BGCOL1 -padx 20 \
            -command "command_OCCUPATION_OF_LAYERS symmetric $COL1 $COL2"
#-----------------------------------------
proc command_OCCUPATION_OF_LAYERS { OCCUPATION_OF_LAYERS COL1 COL2} {
global bulk_L_eq_bulk_R struc_L_eq_struc_R

if { $OCCUPATION_OF_LAYERS == "symmetric" } {
 
   set bulk_L_eq_bulk_R YES
 
   set struc_L_eq_struc_R YES
   command_struc_L_eq_struc_R YES $COL1 $COL2

}

}
#-----------------------------------------

radiobutton $COL1.sym.b  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
		-variable OCCUPATION_OF_LAYERS -value NON-symmetric \
	        -text "NON-symmetric" -bg $BGCOL1 -padx 20
pack $COL1.sym.space1 $COL1.sym.lab1 $COL1.sym.a $COL1.sym.b  -anchor w -side top  
pack $COL1.sym  -fill both
$COL1.sym.a select

frame $COL1.bulk -bg $BGCOL1
label $COL1.bulk.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20 
label $COL1.bulk.lab1 -text "bulk(L) = bulk(R)" -bg $BGCOL1 -padx 20 
radiobutton $COL1.bulk.a  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
		-variable bulk_L_eq_bulk_R -value YES -text "YES"  -padx 20 -bg $BGCOL1\
                -command "command_bulk_L_eq_bulk_R YES $COL1 $COL2"
radiobutton $COL1.bulk.b  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
		-variable bulk_L_eq_bulk_R -value NO  -text "NO"   -padx 20 -bg $BGCOL1\
                -command "command_bulk_L_eq_bulk_R NO  $COL1 $COL2"
#-----------------------------------------
proc command_bulk_L_eq_bulk_R { bulk_L_eq_bulk_R COL1 COL2 } {
global OCCUPATION_OF_LAYERS struc_L_eq_struc_R

if {$bulk_L_eq_bulk_R == "NO" } {
   set OCCUPATION_OF_LAYERS NON-symmetric
} else {
   set struc_L_eq_struc_R YES
   command_struc_L_eq_struc_R YES $COL1 $COL2
}

}
#-----------------------------------------
   #-----------------------------------------
proc command_struc_L_eq_struc_R { struc_L_eq_struc_R COL1 COL2 } {
global OCCUPATION_OF_LAYERS bulk_L_eq_bulk_R
global STRUCTURE_SEQUENCE NSUBSYS LATTICE_TYPE

if {$struc_L_eq_struc_R=="NO"} {
    set OCCUPATION_OF_LAYERS NON-symmetric
    set bulk_L_eq_bulk_R "NO"       
}

if {[winfo exists $COL1.struni] == 1} {
   if {$struc_L_eq_struc_R=="NO"} {
      $COL1.struni.scale  configure -bigincrement 2 -from 2 -to 10
      $COL1.struni.scale  set 2     
   } else {			       
      $COL1.struni.scale  configure -bigincrement 2 -from 1 -to  9
      $COL1.struni.scale  set 3
   }
}

if {[winfo exists $COL2.uc.lab1_L] == 1 && $LATTICE_TYPE=="heterogeneous"} {
   set i  $STRUCTURE_SEQUENCE   
   $COL2.uc.lab1_L configure -text "repeat structure unit $i along c"
   set i  [expr $STRUCTURE_SEQUENCE + $NSUBSYS -1]   
   set i  [expr $i - 2*($i/2)]
   if {$i==0} {set i 2}
   $COL2.uc.lab1_R configure -text "repeat structure unit $i along c"
}
   
}
#-----------------------------------------

pack $COL1.bulk.space1 $COL1.bulk.lab1 $COL1.bulk.a $COL1.bulk.b -anchor w -side top
if { $ZRANGE_TYPE=="extended" } {pack $COL1.bulk  -fill both }
$COL1.bulk.a select

#---------------------------------------------------------------------------------------
if {$LATTICE_TYPE=="heterogeneous"} {
   frame $COL1.struc -bg $BGCOL1 
   label $COL1.struc.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20 
   label $COL1.struc.lab1 -text "struc(L) = struc(R)" -bg $BGCOL1 -padx 20 
   radiobutton $COL1.struc.a  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
	   	-variable struc_L_eq_struc_R -value YES -text "YES" -padx 20 \
                -bg $BGCOL1 -command "command_struc_L_eq_struc_R YES $COL1 $COL2"
   radiobutton $COL1.struc.b  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
 		-variable struc_L_eq_struc_R -value NO  -text "NO"   -padx 20 \
                -bg $BGCOL1 -command "command_struc_L_eq_struc_R NO $COL1 $COL2"
   pack $COL1.struc.space1 $COL1.struc.lab1 $COL1.struc.a $COL1.struc.b \
 		   -anchor w -side top
   pack $COL1.struc  -fill both
   $COL1.struc.a select
   
   set NSUBSYS_min  1
   set NSUBSYS_max  9
   set NSUBSYS_step 2
   frame $COL1.struni -bg $BGCOL1
   label $COL1.struni.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20 
   label $COL1.struni.lab1 -text "number of sub systems " \
                       -bg $BGCOL1 -padx 20
   scale $COL1.struni.scale -from $NSUBSYS_min -orient {horizontal}   \
         -sliderlength {20} -to $NSUBSYS_max    -variable NSUBSYS \
         -length 80 -bg $BGCOL1 -highlightthickness 0  \
         -command "command_scale_adjust_NSUBSYS "
   $COL1.struni.scale  set 3
   pack $COL1.struni.space1 $COL1.struni.lab1   -anchor w -side top
   pack $COL1.struni.scale  -anchor c -side top
   pack $COL1.struni  -fill both
   #-----------------------------------------
   proc command_scale_adjust_NSUBSYS { x } {
   global NSUBSYS struc_L_eq_struc_R

      if {$struc_L_eq_struc_R=="NO"} {
         set NSUBSYS [expr 2*($NSUBSYS/2)]
	 if {$NSUBSYS<2} {set NSUBSYS 2}
      } else {
         set NSUBSYS [expr 2*($NSUBSYS/2)+1]
      }
   }
   #-----------------------------------------

   frame $COL1.seq -bg $BGCOL1
   label $COL1.seq.space1 -text " " -width $wlab1 -bg $BGCOL1 -padx 20 
   label $COL1.seq.lab1 -text "sequence of structure units" -bg $BGCOL1 -padx 20 
   radiobutton $COL1.seq.a  -relief flat  -highlightthickness 0 -font $FONT(GEN)\
               -variable STRUCTURE_SEQUENCE -value "1" -text "1|2|1..."\
               -bg $BGCOL1 -padx 20
   radiobutton $COL1.seq.b  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
	       -variable STRUCTURE_SEQUENCE -value "2" \
	       -text "2|1|2..." -bg $BGCOL1 -padx 20
   pack $COL1.seq.space1 $COL1.seq.lab1 $COL1.seq.a $COL1.seq.b  -anchor w -side top  
   pack $COL1.seq  -fill both
   $COL1.seq.a select
   
}
#---------------------------------------------------------------------------------------


label $COL1.bot    -text " " -bg $BGCOL1 -pady 5
pack configure $COL1.bot -fill both -expand y


#=======================================================================================
#                                     column 2  
#=======================================================================================
set wlab2 30

frame $COL2.ab -bg $BGCOL2 
label $COL2.ab.space1 -text " " -width $wlab2 -bg $BGCOL2
label $COL2.ab.lab1_a -text "repeat structure unit along a" -bg $BGCOL2
scale $COL2.ab.scale_a -from 1 -to 15 -orient {horizontal}   \
      -sliderlength {20}  -variable N_rep_SU_a \
      -length 80 -bg $BGCOL2 -highlightthickness 0 
label $COL2.ab.lab1_b -text "repeat structure unit along b" -bg $BGCOL2 
scale $COL2.ab.scale_b -from 1 -to 15 -orient {horizontal}   \
      -sliderlength {20}  -variable N_rep_SU_b \
      -length 80 -bg $BGCOL2 -highlightthickness 0  

pack $COL2.ab.space1 $COL2.ab.lab1_a -anchor w -side top
pack $COL2.ab.scale_a -anchor c -side top
pack $COL2.ab.lab1_b  -anchor w -side top
pack $COL2.ab.scale_b -anchor c -side top
pack $COL2.ab  -fill both  
#
#---------------------------------------------------------------------------------------
#
if { $ZRANGE_TYPE!="slab" } {
   if {$LATTICE_TYPE=="heterogeneous"} {
      $COL1.seq.a configure -command "command_STRUCTURE_SEQUENCE $COL2"
      $COL1.seq.b configure -command "command_STRUCTURE_SEQUENCE $COL2"
      set TXT_L " 1"
      set TXT_R " 1"
   } else {
      set TXT_L "  "
      set TXT_R "  "
   }
   frame $COL2.uc -bg $BGCOL2 
   label $COL2.uc.space1 -text " " -width $wlab2 -bg $BGCOL2
   label $COL2.uc.lab1_L -text "repeat structure unit $TXT_L along c" -bg $BGCOL2

#  ------------------------------------------------------------------
   proc command_STRUCTURE_SEQUENCE {COL2} {
   global STRUCTURE_SEQUENCE NSUBSYS

   set i  $STRUCTURE_SEQUENCE   
   $COL2.uc.lab1_L configure -text "repeat structure unit $i along c"
   set i  [expr $STRUCTURE_SEQUENCE + $NSUBSYS -1]   
   set i  [expr $i - 2*($i/2)]
   if {$i==0} {set i 2}
   $COL2.uc.lab1_R configure -text "repeat structure unit $i along c"
   }
#  ------------------------------------------------------------------

   label $COL2.uc.lab2_L -text "to generate unit cell of bulk(L)"     -bg $BGCOL2
   scale $COL2.uc.scale_L -from 1 -to 15 -orient {horizontal}   \
         -sliderlength {20}  -variable N_rep_SU_bulk_L \
         -length 80 -bg $BGCOL2 -highlightthickness 0 
   label $COL2.uc.lab1_R -text "repeat structure unit $TXT_R along c" -bg $BGCOL2 
   label $COL2.uc.lab2_R -text "to generate unit cell of bulk(R)"     -bg $BGCOL2 
   scale $COL2.uc.scale_R -from 1 -to 15 -orient {horizontal}   \
         -sliderlength {20}  -variable N_rep_SU_bulk_R \
         -length 80 -bg $BGCOL2 -highlightthickness 0  
   
   pack $COL2.uc.space1 $COL2.uc.lab1_L $COL2.uc.lab2_L -anchor w -side top
   pack $COL2.uc.scale_L -anchor c -side top
   pack $COL2.uc.lab1_R $COL2.uc.lab2_R -anchor w -side top
   pack $COL2.uc.scale_R -anchor c -side top
   pack $COL2.uc  -fill both
   
}
   

set LIMIT 25.0

if {$LATTICE_TYPE=="homogeneous"} {

   frame $COL2.scale_c -bg $BGCOL2
   pack  $COL2.scale_c -side top -anchor n -fill x
   label $COL2.scale_c.space1 -text " " -width $wlab2 -bg $BGCOL2 
   label $COL2.scale_c.lab1 -text "scale lattice parameter c  (%)" -bg $BGCOL2 
   scale $COL2.scale_c.scale1 -from [expr -$LIMIT] -orient {horizontal} \
      -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C \
      -length $lslid -bg $BGCOL2 -highlightthickness 0
   pack $COL2.scale_c.space1 $COL2.scale_c.lab1 $COL2.scale_c.scale1  -side top
   
   frame $COL2.foot -bg $BGCOL2
   pack $COL2.foot  -fill both -expand yes
    
} else {

   frame $COL2.relax_ISD -bg $BGCOL2
   pack  $COL2.relax_ISD -side top -anchor n -fill x
   label $COL2.relax_ISD.space1 -text " " -bg $BGCOL2 
   label $COL2.relax_ISD.lab1 -text "scale inter-system distance (%)" -bg $BGCOL2 
   scale $COL2.relax_ISD.scale1 -from [expr -$LIMIT] -orient {horizontal}  \
         -length $lslid -bg $BGCOL2 -highlightthickness 0 \
         -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_ISD
       
   pack $COL2.relax_ISD.space1 $COL2.relax_ISD.lab1   -side top -anchor w
   pack $COL2.relax_ISD.scale1 -side top -anchor c
   
   frame $COL2.scale_c -bg $BGCOL2
   pack  $COL2.scale_c -side top -anchor n -fill x
   label $COL2.scale_c.space1 -text " " -bg $BGCOL2 
   label $COL2.scale_c.lab1 -text "scale c of structure unit 1  (%)" -bg $BGCOL2 
   scale $COL2.scale_c.scale1 -from [expr -$LIMIT] -orient {horizontal} \
         -length $lslid -bg $BGCOL2 -highlightthickness 0 \
        -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C1
   label $COL2.scale_c.lab2 -text "scale c of structure unit 2  (%)" -bg $BGCOL2 
   scale $COL2.scale_c.scale2 -from [expr -$LIMIT] -orient {horizontal} \
         -length $lslid -bg $BGCOL2 -highlightthickness 0 \
        -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C2
   pack $COL2.scale_c.space1 $COL2.scale_c.lab1   -side top -anchor w
   pack $COL2.scale_c.scale1 -side top -anchor c
   pack $COL2.scale_c.lab2   -side top -anchor w
   pack $COL2.scale_c.scale2 -side top -anchor c

}

label $COL2.bot    -text " " -bg $BGCOL2 -pady 5
pack configure $COL2.bot -fill both -expand y

#=====================================================================================
#                column 3   ------   homogeneous lattices
#=====================================================================================
set ML_SCALE_C   0 

if {$LATTICE_TYPE=="homogeneous"} {
    
    set C 1
    set ML 0
    frame $COL3.ml$ML -relief flat -borderwidth 2 -bg $BGCOL3
    pack  $COL3.ml$ML -side top -fill x -expand yes -anchor w
    label $COL3.ml$ML.lab1 -bg $BGCOL3 -text "layers in basic\n  structure unit"
    if { $ZRANGE_TYPE=="extended" } {
       label $COL3.ml$ML.lab2 -bg $BGCOL3 \
           -text "total number of layers\nincluding bulk(L) and bulk(R)" -width 30 
    } else {

       label $COL3.ml$ML.lab2 -bg $BGCOL3 \
           -text "total number of layers" -width 30 
    }
    label $COL3.ml$ML.lab3 -bg $BGCOL3 -text " select"
    pack  $COL3.ml$ML.lab1 $COL3.ml$ML.lab2 $COL3.ml$ML.lab3 -side left -anchor w

    for {set ML 1} {$ML <= $NML1} {incr ML} {
       frame $COL3.ml$ML -relief raised -borderwidth 2 -bg $BGCOL3
       pack  $COL3.ml$ML -side top
    
       label $COL3.ml$ML.lab2 -text "$ML_NLAY($ML,$C,1)" \
                           -width $wlab1 -bg $BGCOL3 -padx 15
       if { $ZRANGE_TYPE=="extended" } {
          set N_from [expr 2*$ML_NLAY($ML,$C,1)]
       } else {
          set N_from 1
       }
       scale $COL3.ml$ML.scale -from $N_from -orient {horizontal} \
             -sliderlength {20} -to $NMLMAX -variable ML_Ntot($ML,$C) \
             -length $lslid -bg $BGCOL3 -highlightthickness 0 
       pack $COL3.ml$ML.lab2 $COL3.ml$ML.scale -side left
       button $COL3.ml$ML.but1 -text "$ML_name($ML,$C)" -padx 20 -width $wlab1  \
              -height 2  -bg $COLOR(GOON) \
              -command  "set SU_NAME(1) \"[string trim $ML_name($ML,1)]\" ; \
                         set ML_select $ML ; \
                         command_settings_OK_but1 $Wcrml"
       pack $COL3.ml$ML.but1  -side right
    }


    if { $ZRANGE_TYPE=="extended" } {
       $COL2.uc.scale_L configure -command "command_update_scale_from $COL3 $NML1"
       $COL2.uc.scale_R configure -command "command_update_scale_from $COL3 $NML1"
    }
#---------------------------------------------------
proc command_update_scale_from {COL3 NML1 x} {
global N_rep_SU_bulk_L N_rep_SU_bulk_R ML_NLAY ML_Ntot

    for {set ML 1} {$ML <= $NML1} {incr ML} {

       set N [expr ($N_rep_SU_bulk_L + $N_rep_SU_bulk_R) * $ML_NLAY($ML,1,1)]

       $COL3.ml$ML.scale configure -from $N
       set  ML_Ntot($ML,1) $N

    }
}


#---------------------------------------------------
proc command_settings_OK_but1 {w} {
global NLAY_SUBSYS NSUBSYS N_rep_SU_bulk_L N_rep_SU_bulk_R ML_select ML_Ntot
global NLAY_bulk_L NLAY_bulk_R ZRANGE_TYPE ML_NLAY ALAT

if {[string trim $ALAT] ==""} {give_warning "." \
              "WARNING \n\n specify lattice parameter   A  first \n\n " ; return}

if { $ZRANGE_TYPE=="extended" } {

   set NLAY_bulk [expr ($N_rep_SU_bulk_L + $N_rep_SU_bulk_R) * $ML_NLAY($ML_select,1,1)]

   if {$ML_Ntot($ML_select,1) <= $NLAY_bulk } {
       give_warning "." \
       "WARNING \n
       \n number of layers in bulk (L+R) = $NLAY_bulk >= \n
       \n total number of layers  $ML_Ntot($ML_select,1) \n
       \n no layers left for potential relaxation" 
       return    
   }

}

destroy $w

structure_multilayer_create 2D 1 DUMMY
}
#---------------------------------------------------


#=====================================================================================
#                column 3   ------   heterogeneous lattices
#=====================================================================================
} else {

    set wbut3 30
    set C 2
    set ML 0
    frame $COL3.ml$ML -relief flat -borderwidth 2 -bg $BGCOL3
    pack  $COL3.ml$ML -side top -fill x -anchor w -pady 15
    label $COL3.ml$ML.lab1 -bg $BGCOL3 -text "layers in basic\n structure units" \
          -padx 10
    label $COL3.ml$ML.lab3 -bg $BGCOL3 -text "select" -padx 100
    pack  $COL3.ml$ML.lab1 $COL3.ml$ML.lab3 -side left -anchor w

    for {set ML 1} {$ML <= $NML2} {incr ML} {
       frame $COL3.ml$ML -relief raised -borderwidth 2 -bg $BGCOL3
       pack  $COL3.ml$ML -side top
    
       label $COL3.ml$ML.lab2_1 -text "$ML_NLAY($ML,$C,1)" -bg $BGCOL3 -padx 22
       label $COL3.ml$ML.lab2_sep -text "/" -bg $BGCOL3
       label $COL3.ml$ML.lab2_2 -text "$ML_NLAY($ML,$C,2)" -bg $BGCOL3 -padx 22
       pack $COL3.ml$ML.lab2_1 $COL3.ml$ML.lab2_sep $COL3.ml$ML.lab2_2 -side left
       button $COL3.ml$ML.but1 -text "$ML_name($ML,$C)" -padx 20 -width $wbut3  \
              -height 2  -bg $COLOR(GOON) \
              -command  "set ML_select $ML ; \
                         destros $Wcrml ;\
                         structure_specify_2D_lattice_hetero "
       pack $COL3.ml$ML.but1  -side right
    }

}

frame $COL3.bot -bg $BGCOL3 
pack $COL3.bot -fill both -expand true -side bottom


}
#                                                     structure_specify_2D_lattice END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                    structure_specify_2D_lattice_hetero
proc structure_specify_2D_lattice_hetero { } {

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select
global LATANG LATPAR ALAT BOA COA NQ NCL SPACEGROUP SPACEGROUP_AP
global NQCL WYCKOFFCL RCLU RCLV RCLW BRAVAIS STRUCTURE_TYPE
global RQX RQY RQZ RBASX RBASY RBASZ NQ BRAVAIS
global NQ_SU1 NQ_SU2
global COLOR WIDTH HEIGHT FONT
global LATTICE_TYPE  ZRANGE_TYPE
global NSUBSYS     STRUCTURE_SEQUENCE
global N_rep_SU_bulk_L N_rep_SU_bulk_R
global u_mid_can a_can v_spc_can dxc_can dyc_can dxb_can dyb_can 
global scl_can arad_can u0_can v0ac_can v0ab_can BGSU
global SU_SUBSYS IL_START_SUBSYS NLAY_SUBSYS
global SU_TOP_LAYER SU_NAME
global NQ_bulk_L NQ_bulk_R
global NLAY_bulk_L NLAY_bulk_R


set tcl_precision 17
set PRC "structure_specify_2D_lattice_hetero"

#---------------------------------------------------------------------------------------
set TXT "Structure set-up for layered system with heterogeneous $ZRANGE_TYPE lattice"
  
set flag 3 
set w .w_crml_2D_hetero
toplevel_init $w "$TXT" 0 0
wm positionfrom $w user
wm sizefrom $w ""

set BGhead $COLOR(LIGHTBLUE)
set BGCOL1 $COLOR(LIGHTSALMON1)
set BGCOL2 $COLOR(LIGHTSALMON1)
set BGCOL3 $COLOR(STEELBLUE1) 
set BGCOL4 white

frame $w.head -relief raised -borderwidth 2 -bg $BGhead
frame $w.head.a -bg $BGhead

button $w.head.a.cancel  \
      -text "CLOSE " -width 17 -height 3 -bg $COLOR(CLOSE) \
        -command "destroy $w" -pady 5

button $w.head.a.goon  \
      -text "settings OK \n GO ON" -width 17 -height 3 -bg $COLOR(GOON) \
        -command "command_settings_OK_goon $w" -pady 5 

#---------------------------------------------------
proc command_settings_OK_goon {w} {
global NLAY_SUBSYS NSUBSYS
global NLAY_bulk_L NLAY_bulk_R ZRANGE_TYPE ALAT

if {[string trim $ALAT] ==""} {give_warning "." \
              "WARNING \n\n specify lattice parameter   A  first \n\n " ; return}

for {set isubsys 1} {$isubsys <= $NSUBSYS} {incr isubsys} {
    if {$NLAY_SUBSYS($isubsys)==0 } { 
       give_warning "." \
       "WARNING 
       \n\n number of layers \n\n not specified for \n\n sub system $isubsys\n" 
       return
    }
}

if { $ZRANGE_TYPE=="extended" } {
   if {$NLAY_SUBSYS(1) <= $NLAY_bulk_L } {
       give_warning "." \
       "WARNING \n
       \n number of layers in bulk(L) = \n\n number of layers in subsystem 1\n
       \n no layers left for potential relaxation" 
       return    
   }

   if {$NLAY_SUBSYS($NSUBSYS) <= $NLAY_bulk_R } {
       give_warning "." \
       "WARNING \n\n number of layers in bulk(R) = \n
       \n number of layers in subsystem $NSUBSYS  (last)\n
       \n no layers left for potential relaxation" 
       return    
   }
}

destroy $w

structure_multilayer_create 2D 2 DUMMY
}
#---------------------------------------------------

pack $w.head.a.goon $w.head.a.cancel -padx 25  -side right -fill x
pack $w.head.a  -side top -expand yes -fill x
pack $w.head -side top  -fill x

frame  $w.col1 -bg $BGCOL1
frame  $w.col2 -bg $BGCOL2
frame  $w.col3 -bg $BGCOL3
frame  $w.col4 -bg $BGCOL4

pack  $w.col1 $w.col2 $w.col3 $w.col4 -side left -fill y -anchor n

set COL1 $w.col1
set COL2 $w.col2
set COL3 $w.col3
set COL4 $w.col4

set C 2
set ML $ML_select

set w_can 5
set h_can 18
set u_spc_can [expr $w_can/6.0]
set v_spc_can [expr $h_can/6.0]
set u_mid_can [expr $w_can/2.0]
set v_mid_can [expr $h_can/2.0]
set arad_can   0.5
set acol  red
lappend color_list red green blue magenta cyan1 orange1 maroon1 yellow

#
#=======================================================================================
proc draw_circle { c u0 v0  n1 n2 a d h scl r circle_color t} {

set du [expr $scl*($n1*$a + $n2*$d)]
set dv [expr $scl*(         $n2*$h)]

set u [expr $u0+$du] 
set v [expr $v0-$dv] 

$c create oval [expr $u-$r]c [expr $v-$r]c [expr $u+$r]c [expr $v+$r]c \
     -fill $circle_color -width 3 -tag $t
}
#=======================================================================================
proc transform {u0 v0  n1 n2 a d h scl } {

set du [expr $scl*($n1*$a + $n2*$d)]
set dv [expr $scl*(         $n2*$h)]

set coord "[expr $u0+$du]c [expr $v0-$dv]c"

return $coord  
}
#=======================================================================================
#                                     column 1  +  2
#=======================================================================================
#
#-------------------------------------------------------- try to get structure unit name
#
set i_first [string first "/" $ML_name($ML,$C) ]
set i_last  [string last  "/" $ML_name($ML,$C) ]
set i_end   [expr [string length $ML_name($ML,$C)] - 1 ]

if {$i_first==$i_last && $i_first> 0 } {
   set SU_NAME(1) [string trim [string range $ML_name($ML,$C) 0 [expr $i_first-1]]]
   set SU_NAME(2) [string trim [string range $ML_name($ML,$C) [expr $i_first+1] $i_end]]
} 
#---------------------------------------------------------------------------------------

set COLSU(1) $w.col1
set COLSU(2) $w.col3
      
set ax $ML_AX($ML,$C) ; set bx $ML_BX($ML,$C)
set ay $ML_AY($ML,$C) ; set by $ML_BY($ML,$C)
set az $ML_AZ($ML,$C) ; set bz $ML_BZ($ML,$C)
   
set a   [expr pow( [dot_prod $ax $ay $az $ax $ay $az ], 0.5 ) ]
set x   [cross_prod  $ax $ay $az  $bx $by $bz x]
set y   [cross_prod  $ax $ay $az  $bx $by $bz y]
set z   [cross_prod  $ax $ay $az  $bx $by $bz z]
set dyb_can [expr pow( [dot_prod $x $y $z  $x $y $z ], 0.5 ) / $a ]
set dxb_can [expr [dot_prod $ax $ay $az $bx $by $bz ] / $a ]

if {$dxb_can<0} {
   set x0off_a $dxb_can
   set x0off_b 0
} else {
   set x0off_a 0
   set x0off_b $dxb_can
} 
 
for {set su 1} {$su <=2} {incr su} {

   set cx $ML_CX($ML,$C,$su)
   set cy $ML_CY($ML,$C,$su)
   set cz $ML_CZ($ML,$C,$su)

   set x   [cross_prod  $ax $ay $az  $cx $cy $cz x]
   set y   [cross_prod  $ax $ay $az  $cx $cy $cz y]
   set z   [cross_prod  $ax $ay $az  $cx $cy $cz z]
   set dyc_can($su) [expr pow( [dot_prod $x $y $z  $x $y $z ], 0.5 ) / $a ]
   set dxc_can($su) [expr [dot_prod $ax $ay $az  $cx $cy $cz ] / $a ]

   if {$dxc_can($su)<0} {
      if {$dxc_can($su)<$x0off_a} { set x0off_a $dxc_can($su) }
   } else {
      if {$dxc_can($su)>$x0off_b} { set x0off_b $dxc_can($su) }
   } 
   set dy_eff($su) [expr $dyc_can($su) + $dyb_can ]
}

set x0off_a [expr abs($x0off_a) ]

set dx_eff [expr $x0off_a + $a + $x0off_b ]

set scl_can [ expr ($w_can-2*$u_spc_can) / $dx_eff ]

for {set su 1} {$su <=2} {incr su} {
   set ry [ expr ($h_can-3*$v_spc_can) / $dy_eff($su) ]
   if {$scl_can>$ry} {set scl_can $ry}
}

debug $PRC "scl_can  $scl_can"
debug $PRC "a        $a   "
debug $PRC "dxb_can      $dxb_can        dyb_can       $dyb_can"
debug $PRC "dxc_can1     $dxc_can(1)        dyc_can1      $dyc_can(1)"
debug $PRC "dxc_can2     $dxc_can(2)        dyc_can2      $dyc_can(2)"
debug $PRC "dx_eff   $dx_eff     "
debug $PRC "dy_eff   $dy_eff(1)  $dy_eff(2) "

set u0_can [expr ($w_can-$scl_can*$dx_eff)/2 + $scl_can*$x0off_a]

#---------------------------------------------------------------------------------------
for {set su 1} {$su <=2} {incr su} {
   set wc $COLSU($su).can 
   canvas $wc -width ${w_can}c -height ${h_can}c -bg $BGSU($su)
   pack $wc -side left
   set v_txt [expr 0.5*$v_spc_can]
   $wc create text ${u_mid_can}c ${v_txt}c -text "structure unit $su  \n    $SU_NAME($su)"
#---------------------------------------------------------------------------------------
#                                                                              a-c plane
   set v0 [expr $h_can - $scl_can*$dyb_can - 2*$v_spc_can]
   set v0ac_can $v0
   set seq ""
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 0 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 1 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 1 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   $wc create line $seq -width 3

   set v_txt [expr $v0+$v_spc_can/2.0]
   $wc create text ${u_mid_can}c ${v_txt}c -text "projection along b\n    on a-c plane"
#---------------------------------------------------------------------------------------
#                                                                              a-b plane
   set v0 [expr $h_can - $v_spc_can]
   set v0ab_can $v0
   set seq ""
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 0 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 1 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 1 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyb_can $scl_can]"
   $wc create line $seq -width 3

   set v_txt [expr $h_can - 0.5*$v_spc_can]
   $wc create text ${u_mid_can}c ${v_txt}c -text "projection along c\n    on a-b plane"

#---------------------------------------------------------------------------------------

   if {$su==1} {
      set ILTOP $ML_NLAY($ML,$C,1)
   } else {
      set ILTOP $ML_NLAY($ML,$C,2)
   }

   for {set IL 1} {$IL <= $ILTOP } {incr IL} {
      if {$su==1} {
         set ISTOP $ML_NSITES($ML,$C,$IL,1)
      } else {
         set ISTOP $ML_NSITES($ML,$C,$IL,2)
      }

      set circle_color [lindex $color_list [expr $IL - 1] ] 
   
      for {set IS 1} {$IS <= $ISTOP} {incr IS} {
         if {$su==1} {
            set na $ML_U1($ML,$C,$IL,$IS)
            set nb $ML_V1($ML,$C,$IL,$IS)
            set nc $ML_W1($ML,$C,$IL,$IS)
         } else {
            set na $ML_U2($ML,$C,$IL,$IS)
            set nb $ML_V2($ML,$C,$IL,$IS)
            set nc $ML_W2($ML,$C,$IL,$IS)
         }
      
#-------------------------------------------------------------- create all sites in cell
         for {set ia 0} {$ia <= 1} {incr ia} {
         set ja [expr $na + $ia]
	 if {$ja<1.0001} {

         for {set ic 0} {$ic <= 1} {incr ic} {
         set jc [expr $nc + $ic]
	 if {$jc<1.0001} {
         draw_circle $wc $u0_can $v0ac_can  $ja $jc  $a $dxc_can($su) $dyc_can($su) \
                     $scl_can $arad_can $circle_color fixed
         }
         }

         for {set ib 0} {$ib <= 1} {incr ib} {
         set jb [expr $nb + $ib]
	 if {$jb<1.0001} {
         draw_circle $wc $u0_can $v0ab_can  $ja $jb  $a $dxb_can      $dyb_can \
                     $scl_can $arad_can $circle_color fixed
         }
         }

         }
         }
#-------------------------------------------------------------- create all sites in cell

      }
   }
   
}
#---------------------------------------------------------------------------------------

frame $COL1.bot -bg $BGSU(1)
pack $COL1.bot -fill both -expand true -side bottom

frame $COL2.bot -bg $BGSU(2)
pack $COL2.bot -fill both -expand true -side bottom


#=======================================================================================
#                                     column 3 
#=======================================================================================
label $COL3.title -bg $BGCOL3  -text "$ML_name($ML,$C)" -pady 10
pack $COL3.title  -side top -anchor c

set isubsys 0
frame $COL3.su_$isubsys  -bg $BGCOL3
pack  $COL3.su_$isubsys -side top -fill x
label $COL3.su_$isubsys.lab0  -text " sub\n sys"  -width 5 -bg $BGCOL3 -padx 5
label $COL3.su_$isubsys.lab1  -text " struc\n  unit"  -width 5 -bg $BGCOL3 -padx 2
label $COL3.su_$isubsys.lab2  -text " number of layers\n in sub system" \
      -width 18 -bg $BGCOL3 -padx 5
label $COL3.su_$isubsys.lab3  -text " start sub system \n with s-u layer" \
      -width 15 -bg $BGCOL3 -padx 5
label $COL3.su_$isubsys.lab4  -text "s-u top \n layer" \
      -bg $BGCOL3 -padx 5
pack  $COL3.su_$isubsys.lab0 $COL3.su_$isubsys.lab1 \
      $COL3.su_$isubsys.lab2 $COL3.su_$isubsys.lab3 \
      $COL3.su_$isubsys.lab4 \
       -side left -anchor w

set su [expr $STRUCTURE_SEQUENCE - 1]
for {set isubsys 1} {$isubsys <= $NSUBSYS} {incr isubsys} {
   incr su ; if {$su==3} {set su 1}
   if {$su == 1} {
      set ILAY_TOP $ML_NLAY($ML,$C,1)
   } else {
      set ILAY_TOP $ML_NLAY($ML,$C,2)
   }

   set SU_SUBSYS($isubsys) $su

   set BGC $BGSU($su)

   if {$ZRANGE_TYPE == "extended"} {
      if {$isubsys==1} {
         set NLAY_SUBSYS_BOT [expr $N_rep_SU_bulk_L * $ILAY_TOP]
         set NLAY_bulk_L $NLAY_SUBSYS_BOT
         label $COL3.su_L_$isubsys -bg $BGC  \
         -text "sub system  $isubsys  includes  $NLAY_SUBSYS_BOT \
                layers to represent bulk(L)"
      } elseif {$isubsys==$NSUBSYS} {
         set NLAY_SUBSYS_BOT [expr $N_rep_SU_bulk_R * $ILAY_TOP]
         set NLAY_bulk_R $NLAY_SUBSYS_BOT
         label $COL3.su_R_$isubsys -bg $BGC  \
         -text "sub system  $isubsys  includes  $NLAY_SUBSYS_BOT \
                layers to represent bulk(R)"
      }
   } else {
      set NLAY_SUBSYS_BOT  1
   }

   frame $COL3.su_$isubsys -relief raised -borderwidth 2 -bg $BGC
   pack  $COL3.su_$isubsys -side top -fill x 

   if {$ZRANGE_TYPE == "extended"} {
      if {$isubsys==1} {
         pack  $COL3.su_L_$isubsys -side top -anchor c -fill x
      } elseif {$isubsys==$NSUBSYS} {
         pack  $COL3.su_R_$isubsys -side top -anchor c -fill x
      }
   }

   label $COL3.su_$isubsys.lab0  -text "$isubsys"  -width 5 -bg $BGC -padx 5 -pady 10
   label $COL3.su_$isubsys.lab1  -text "$su"   -width 5 -bg $BGC -padx 5 -pady 10
   pack  $COL3.su_$isubsys.lab0 $COL3.su_$isubsys.lab1 -side left -anchor w
 
   scale $COL3.su_$isubsys.scale1 -from $NLAY_SUBSYS_BOT -orient {horizontal} \
         -sliderlength {20} -to 40 -variable NLAY_SUBSYS($isubsys) \
         -length 120 -bg $BGC -highlightthickness 0 \
         -command "command_adjust_SU_TOP_LAYER  $isubsys $ML $C"
   pack  $COL3.su_$isubsys.scale1 -side left -anchor w -pady 10 -padx 8

   scale $COL3.su_$isubsys.scale2 -from 1 -orient {horizontal} \
         -sliderlength {20} -to $ILAY_TOP -variable IL_START_SUBSYS($isubsys) \
         -length 60 -bg $BGC -highlightthickness 0 \
         -command "command_adjust_SU_TOP_LAYER  $isubsys $ML $C"
   pack  $COL3.su_$isubsys.scale2 -side left -anchor w -pady 10 -padx 20
#
   label $COL3.su_$isubsys.sutop -textvariable SU_TOP_LAYER($isubsys) \
         -width 5 -bg $BGC -padx 5
   pack  $COL3.su_$isubsys.sutop -side left -anchor w

   if {$isubsys < $NSUBSYS} {
       frame $COL3.su_inter_$isubsys
       button $COL3.su_inter_$isubsys.but1 \
             -text "view interface $isubsys - [expr $isubsys+1] " -bg green \
             -command "command_view_interface $COL4.can_interface $isubsys $ML $C" 
       button $COL3.su_inter_$isubsys.but2 \
             -text "use rasmol" -bg green \
             -command "command_view_interface $COL4.can_interface $isubsys $ML $C ; \
                       command_view_interface_rasmol $isubsys $ML $C  " -width 10

       pack  $COL3.su_inter_$isubsys.but1 -side left -fill x -expand yes
       pack  $COL3.su_inter_$isubsys.but2              
       pack  $COL3.su_inter_$isubsys -side top -anchor c -fill x -expand yes
   }
   
}

frame $COL3.bot -bg $BGCOL3 
pack $COL3.bot -fill both -expand true -side bottom

#---------------------------------------------------------------------------------------
#                                adjust the top layer number according to the present SU
#                 adjust parameters for bulk(L) and bulk(R) in case that bulk(L)=bulk(R)
#
proc command_adjust_SU_TOP_LAYER {isubsys ML C x} {
global SU_TOP_LAYER NLAY_SUBSYS SU_SUBSYS NSUBSYS
global ML_NLAY IL_START_SUBSYS ZRANGE_TYPE bulk_L_eq_bulk_R

   set su $SU_SUBSYS($isubsys)

   if {$su==1} {
      set ILTOP $ML_NLAY($ML,$C,1)
   } else {
      set ILTOP $ML_NLAY($ML,$C,2)
   }

   set i [expr $NLAY_SUBSYS($isubsys) + $IL_START_SUBSYS($isubsys) - 1 ]
   set SU_TOP_LAYER($isubsys)  [expr $i - ([expr $i-1]/$ILTOP)*$ILTOP ]

   if {$ZRANGE_TYPE == "extended"} {
      if { $bulk_L_eq_bulk_R=="YES"} {
	  if {$isubsys==1} {
##             set SU_TOP_LAYER($NSUBSYS) $SU_TOP_LAYER(1)
             set NLAY_SUBSYS($NSUBSYS)  $NLAY_SUBSYS(1)
          } elseif {$isubsys==$NSUBSYS} {
##             set SU_TOP_LAYER(1) $SU_TOP_LAYER($NSUBSYS)
             set NLAY_SUBSYS(1)  $NLAY_SUBSYS($NSUBSYS)
          }
      } 
   }
}
#---------------------------------------------------------------------------------------


#
#=======================================================================================
#                                     column 4
#=======================================================================================
set a_can   $a 

set wc_if $COL4.can_interface

canvas $wc_if -width ${w_can}c -height ${h_can}c -bg $BGCOL4
pack $wc_if
set v_txt [expr 0.3*$v_spc_can]
$wc_if create text ${u_mid_can}c ${v_txt}c -text "view interface" -tag last

#
#-------------------------------------------------------- draw lines for both subsystems
#
for {set su 1} {$su <=2} {incr su} {
#---------------------------------------------------------------------------------------
#                                                                              a-c plane
   set v0 [expr $h_can - $scl_can*$dyb_can - $v_spc_can*1.4]
   set v0ac_can $v0
   set seq ""
   set seq "$seq  [transform $u0_can $v0 1 0.0 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 1.6 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   $wc_if create line $seq -width 3

   set seq ""
   set seq "$seq  [transform $u0_can $v0 0 1.6 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 0.0 $a $dxc_can($su) $dyc_can($su) $scl_can]"
   $wc_if create line $seq -width 3

   set v_txt [expr $v0+$v_spc_can*0.3]
   $wc_if create text ${u_mid_can}c ${v_txt}c -text "projection along b\n    on a-c plane"
#---------------------------------------------------------------------------------------
#                                                                              a-b plane
   set v0 [expr $h_can - $v_spc_can*0.6]
   set v0ab_can $v0
   set seq ""
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 0 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 1 1 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 1 $a $dxc_can($su) $dyb_can $scl_can]"
   set seq "$seq  [transform $u0_can $v0 0 0 $a $dxc_can($su) $dyb_can $scl_can]"
   $wc_if create line $seq -width 3

   set v_txt [expr $h_can - 0.2*$v_spc_can]
   $wc_if create text ${u_mid_can}c ${v_txt}c -text "interface layers || a-b plane"

#---------------------------------------------------------------------------------------
}

frame $COL4.bot -bg $BGCOL4
pack $COL4.bot -fill both -expand true -side bottom

}
#                                                structure_specify_2D_lattice_hetero END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

########################################################################################
#                                                                command_view_interface
proc command_view_interface {wc isubsys0 ML C} {

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select
global LATANG LATPAR ALAT BOA COA NQ NCL SPACEGROUP SPACEGROUP_AP
global NQCL WYCKOFFCL RCLU RCLV RCLW BRAVAIS STRUCTURE_TYPE
global RQX RQY RQZ RBASX RBASY RBASZ NQ BRAVAIS
global NQ_SU1 NQ_SU2
global COLOR WIDTH HEIGHT FONT
global LATTICE_TYPE  ZRANGE_TYPE
global NSUBSYS     STRUCTURE_SEQUENCE
global N_rep_SU_bulk_L N_rep_SU_bulk_R
global u_mid_can a_can v_spc_can dxc_can dyc_can dxb_can dyb_can 
global scl_can arad_can u0_can v0ac_can v0ab_can BGSU
global SU_SUBSYS IL_START_SUBSYS NLAY_SUBSYS
global SU_TOP_LAYER


set tcl_precision 17
set PRC "command_view_interface"
debug $PRC "view interface $isubsys0 - [expr $isubsys0+1] "

#---------------------------------------------------------------------------------------
$wc delete -tag last

set a    $a_can

#---------------------------------------------------------------------------------------
set v_txt [expr 0.3*$v_spc_can]
$wc create text ${u_mid_can}c ${v_txt}c -text \
       "view interface $isubsys0 - [expr $isubsys0+1] " -tag last
#---------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------
for {set isubsys $isubsys0} {$isubsys <=[expr $isubsys0+1]} {incr isubsys} {
   set su $SU_SUBSYS($isubsys)

   if {$su==1} {
      set ILTOP $ML_NLAY($ML,$C,1)
   } else {
      set ILTOP $ML_NLAY($ML,$C,2)
   }

   if {$isubsys==$isubsys0 } {
      set IL_SEL1 $SU_TOP_LAYER($isubsys)
      set IL_SEL2 [expr $IL_SEL1-1]
      if {$IL_SEL2<1} {set IL_SEL2 [expr $IL_SEL2 + $ILTOP]}
   } else {
      set IL_SEL1 $IL_START_SUBSYS($isubsys)
      set IL_SEL2 [expr $IL_SEL1+1]
      if {$IL_SEL2>$ILTOP} {set IL_SEL2 1}
   }

   if {$su==1} {
      set nc1 $ML_W1($ML,$C,$IL_SEL1,1)
      set nc2 $ML_W1($ML,$C,$IL_SEL2,1)
   } else { 
      set nc1 $ML_W2($ML,$C,$IL_SEL1,1)
      set nc2 $ML_W2($ML,$C,$IL_SEL2,1)
   }
   if {$isubsys==$isubsys0 } {
      if {$nc2>$nc1} {set nc2 [expr $nc2 - 1]}
      set add [expr ($nc1-$nc2)/2.0]
      set nctab($isubsys,$IL_SEL1) [expr $add + ($nc1-$nc2)]
      set nctab($isubsys,$IL_SEL2) $add
      set ncif [expr $nctab($isubsys,$IL_SEL1) + $add ]
      set dvif [expr $scl_can*($ncif*$dyc_can($su))]
      set v0ac $v0ac_can
      set seq      "[transform $u0_can $v0ac 0 $ncif $a  \
                     $dxc_can($su) $dyc_can($su) $scl_can]"
      set seq "$seq [transform $u0_can $v0ac 1 $ncif $a  \
                     $dxc_can($su) $dyc_can($su) $scl_can]"
      $wc create line $seq -width 2 -tag last

   } else {
      if {$nc2<$nc1} {set nc2 [expr $nc2 + 1]}
      set add [expr ($nc2-$nc1)/2.0]
      set nctab($isubsys,$IL_SEL1) $add
      set nctab($isubsys,$IL_SEL2) [expr $add + ($nc2-$nc1)]
      set v0ac [expr $v0ac_can - $dvif]
   }


   debug $PRC "sub system $isubsys layers N $NLAY_SUBSYS($isubsys) \
               rep $ILTOP  start $IL_START_SUBSYS($isubsys) >> select $IL_SEL1"
   debug $PRC "sub system $isubsys su $su nctab $nctab($isubsys,$IL_SEL1) \
                                                $nctab($isubsys,$IL_SEL2) add $add"

   for {set IL 1} {$IL <= $ILTOP } {incr IL} {
      if {$su==1} {
         set ISTOP $ML_NSITES($ML,$C,$IL,1)
      } else {
         set ISTOP $ML_NSITES($ML,$C,$IL,2)
      }

      set circle_color $BGSU($su)
   
      for {set IS 1} {$IS <= $ISTOP} {incr IS} {
         if {$su==1} {
            set na $ML_U1($ML,$C,$IL,$IS)
            set nb $ML_V1($ML,$C,$IL,$IS)
            set nc $ML_W1($ML,$C,$IL,$IS)
         } else {
            set na $ML_U2($ML,$C,$IL,$IS)
            set nb $ML_V2($ML,$C,$IL,$IS)
            set nc $ML_W2($ML,$C,$IL,$IS)
         }
      
#-------------------------------------------------------------- create all sites in cell

         for {set ia 0} {$ia <= 1} {incr ia} {
         set ja [expr $na + $ia]
	 if {$ja<1.0001} {

         if {$IL==$IL_SEL1 || $IL==$IL_SEL2} {
         set jc $nctab($isubsys,$IL)
	 if {$jc<1.0001} {
         draw_circle $wc $u0_can $v0ac  $ja $jc  $a $dxc_can($su) $dyc_can($su) \
                     $scl_can $arad_can $circle_color last
         }
         }

         if {$IL==$IL_SEL1} {
         for {set ib 0} {$ib <= 1} {incr ib} {
         set jb [expr $nb + $ib]
	 if {$jb<1.0001} {
         draw_circle $wc $u0_can $v0ab_can  $ja $jb  $a $dxb_can      $dyb_can \
                     $scl_can $arad_can $circle_color last
         }
         }
         }

         }
         }
#-------------------------------------------------------------- create all sites in cell

      }
   }
   
}
#---------------------------------------------------------------------------------------


}
#                                                             command_view_interface END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                          command_view_interface_rasmol
proc command_view_interface_rasmol {isubsys0 ML C} {

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select
global LATANG LATPAR ALAT BOA COA NQ NCL SPACEGROUP SPACEGROUP_AP
global NQCL WYCKOFFCL RCLU RCLV RCLW BRAVAIS STRUCTURE_TYPE
global RQX RQY RQZ RBASX RBASY RBASZ NQ BRAVAIS
global NQ_SU1 NQ_SU2
global COLOR WIDTH HEIGHT FONT
global LATTICE_TYPE  ZRANGE_TYPE
global STRUCTURE_SEQUENCE
global N_rep_SU_bulk_L N_rep_SU_bulk_R
global u_mid_can a_can v_spc_can dxc_can dyc_can dxb_can dyb_can 
global scl_can arad_can u0_can v0ac_can v0ab_can BGSU
global SU_SUBSYS IL_START_SUBSYS NLAY_SUBSYS
global SU_TOP_LAYER RASMOL


set tcl_precision 17
set PRC "command_view_interface_rasmol"
debug $PRC "view interface $isubsys0 - [expr $isubsys0+1] "

#---------------------------------------------------------------------------------------

set a    $a_can
set IQIF  0
set IQIF0 0

#---------------------------------------------------------------------------------------
for {set isubsys $isubsys0} {$isubsys <=[expr $isubsys0+1]} {incr isubsys} {
   set su $SU_SUBSYS($isubsys)

   if {$su==1} {
      set ILTOP $ML_NLAY($ML,$C,1)
   } else {
      set ILTOP $ML_NLAY($ML,$C,2)
   }

   if {$isubsys==$isubsys0 } {
      set IL_SEL1 $SU_TOP_LAYER($isubsys)
      set IL_SEL2 [expr $IL_SEL1-1]
      if {$IL_SEL2<1} {set IL_SEL2 [expr $IL_SEL2 + $ILTOP]}
   } else {
      set IL_SEL1 $IL_START_SUBSYS($isubsys)
      set IL_SEL2 [expr $IL_SEL1+1]
      if {$IL_SEL2>$ILTOP} {set IL_SEL2 1}
   }

   if {$su==1} {
      set nc1 $ML_W1($ML,$C,$IL_SEL1,1)
      set nc2 $ML_W1($ML,$C,$IL_SEL2,1)
   } else { 
      set nc1 $ML_W2($ML,$C,$IL_SEL1,1)
      set nc2 $ML_W2($ML,$C,$IL_SEL2,1)
   }

   if {$isubsys==$isubsys0 } {
      if {$nc2>$nc1} {set nc2 [expr $nc2 - 1]}
      set add [expr ($nc1-$nc2)/2.0]
      set nctab($isubsys,$IL_SEL1) [expr $add + ($nc1-$nc2)]
      set nctab($isubsys,$IL_SEL2) $add
      set ncif [expr $nctab($isubsys,$IL_SEL1) + $add ]
   } else {
      if {$nc2<$nc1} {set nc2 [expr $nc2 + 1]}
      set add [expr ($nc2-$nc1)/2.0]
      set nctab($isubsys,$IL_SEL1) $add
      set nctab($isubsys,$IL_SEL2) [expr $add + ($nc2-$nc1)]
   }

   for {set IL 1} {$IL <= $ILTOP } {incr IL} {
      if {$su==1} {
         set ISTOP $ML_NSITES($ML,$C,$IL,1)
      } else {
         set ISTOP $ML_NSITES($ML,$C,$IL,2)
      }

      for {set IS 1} {$IS <= $ISTOP} {incr IS} {
         if {$su==1} {
            set na $ML_U1($ML,$C,$IL,$IS)
            set nb $ML_V1($ML,$C,$IL,$IS)
            set nc $ML_W1($ML,$C,$IL,$IS)
         } else {
            set na $ML_U2($ML,$C,$IL,$IS)
            set nb $ML_V2($ML,$C,$IL,$IS)
            set nc $ML_W2($ML,$C,$IL,$IS)
         }
      
#-------------------------------------------------------------- create all sites in cell
         set nrep 3

         if {$IL==$IL_SEL1 || $IL==$IL_SEL2} {
            set jc $nctab($isubsys,$IL)
	    if {$jc<1.0001} {
               for {set ia 0} {$ia <= $nrep} {incr ia} {
                  set ja [expr $na + $ia]
	          if {$ja<[expr $nrep+0.0001]} {
                     for {set ib 0} {$ib <= $nrep} {incr ib} {
                        set jb [expr $nb + $ib]
	                if {$jb<[expr $nrep+0.0001]} {
                           incr IQIF

                           set RIFX($IQIF) [expr $ja*$ML_AX($ML,$C)+$jb*$ML_BX($ML,$C)]
                           set RIFY($IQIF) [expr $ja*$ML_AY($ML,$C)+$jb*$ML_BY($ML,$C)]
                           set RIFZ($IQIF) [expr $ja*$ML_AZ($ML,$C)+$jb*$ML_BZ($ML,$C)]
   
                           if {$isubsys==$isubsys0 } {
                              incr IQIF0
                              set RIFX($IQIF) [expr $RIFX($IQIF)+$jc*$ML_CX($ML,$C,$su)]
                              set RIFY($IQIF) [expr $RIFY($IQIF)+$jc*$ML_CY($ML,$C,$su)]
                              set RIFZ($IQIF) [expr $RIFZ($IQIF)+$jc*$ML_CZ($ML,$C,$su)]
                           } else {
                              set sup [expr 3 - $su]
                              set X [expr $RIFX($IQIF)+$ncif*$ML_CX($ML,$C,$sup)]
                              set Y [expr $RIFY($IQIF)+$ncif*$ML_CY($ML,$C,$sup)]
                              set Z [expr $RIFZ($IQIF)+$ncif*$ML_CZ($ML,$C,$sup)]
                              set RIFX($IQIF) [expr $X+$jc*$ML_CX($ML,$C,$su)]
                              set RIFY($IQIF) [expr $Y+$jc*$ML_CY($ML,$C,$su)]
                              set RIFZ($IQIF) [expr $Z+$jc*$ML_CZ($ML,$C,$su)]
                           }
                        }  
                     }
                  }
               }
            }
         }
#-------------------------------------------------------------- create all sites in cell

      }
   }
   
}
#---------------------------------------------------------------------------------------



#-------------------------------------------------------------------------------
proc write_coord {f s i j x y z t} {

 set PRC "write_coord"

if {$s == "a" } {
   set ATOM  "ATOM  "
} else {
   set ATOM  "HETATM"
}
puts $f [format "%6s %4i           %4i    %8.3f%8.3f%8.3f  0.00%8.3f" \
              $ATOM $i $j $x $y $z $t]
}
#-------------------------------------------------------------------------------
set strdat [open "interface_xband.pdb" w]
puts $strdat "HEADER    interface-control plot  "
puts $strdat "SOURCE    X-band    "
puts $strdat "AUTHOR    H. Ebert  "
puts $strdat "REMARK    None      "

set S 4.0

for {set i 1} {$i <= $IQIF } {incr i} {
    if {$i <= $IQIF0} {
       set color 1
    } else {
       set color 2    
    }

    write_coord $strdat "a"  $i $i [expr $S*$RIFX($i)] [expr $S*$RIFY($i)] \
                                   [expr $S*$RIFZ($i)] $color
}

puts $strdat "END"
close $strdat

#
#--------------------------------------------------- write RASMOL script
#
set   strdir  [open "interface_xband.ras" w]
puts  $strdir "load  interface_xband.pdb "
puts  $strdir "color temperature"
puts  $strdir "set axes on "
puts  $strdir "cpk 150 "
close $strdir


eval set res [catch "exec $RASMOL -script interface_xband.ras &" message]


}
#                                                      command_view_interface_rasmol END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<






########################################################################################
#                                                            structure_multilayer_create
proc structure_multilayer_create {dimension type case} {

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select

global Wcrml
global LATANG LATPAR ALAT BOA COA NQ NCL SPACEGROUP SPACEGROUP_AP
global NQCL WYCKOFFCL RCLU RCLV RCLW BRAVAIS STRUCTURE_TYPE
global RQX RQY RQZ RBASX RBASY RBASZ NQ BRAVAIS
global NQ_SU1 NQ_SU2  NSUBSYS SU_SUBSYS SU_IQ
global N_rep_SU_bulk_L N_rep_SU_bulk_R ZRANGE_TYPE
global IL_START_SUBSYS NLAY_SUBSYS SU_TOP_LAYER
global NQ_bulk_L NQ_bulk_R NLAY_bulk_L NLAY_bulk_R 
global N_rep_SU_a N_rep_SU_b N_SU SU_NAME
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R

set tcl_precision 17
set PRC "structure_multilayer_create"

if {[string trim $ALAT] ==""} {give_warning "." \
              "WARNING \n\n specify lattice parameter   A  first \n\n " ; return}

set C     $type
set N_SU  $C
set ML    $ML_select

if {$C=="1"} { set SU_NAME(1) [lindex [string trim $ML_name($ML,$C)] 0]}

set STRUCTURE_TYPE [string trim $ML_name($ML,$C)]
if {$dimension=="2D"} {set STRUCTURE_TYPE "2D $STRUCTURE_TYPE"}


for {set su 1} {$su <= $N_SU } {incr su} {
   puts ">>>>>>>>>>>>>>SU_NAME     $SU_NAME($su) "
}

#=======================================================================================
#                              use general indices
#=======================================================================================
if {$dimension!="2D"||$C=="1"} {

set NSUBSYS $C

#
# --------------------------------------------------- fix start index for layer sequence
#
if {$C=="1"} {
   set IL_START_SUBSYS(1) 1
} else {
   if { $ML_NLAY($ML,$C,1)==2 && $ML_NLAY($ML,$C,2)==2 } {
      if {$case=="a"} {
         set IL_START_SUBSYS(1) 1
         set IL_START_SUBSYS(2) 1
      } else {
          set IL_START_SUBSYS(1) 1
          set IL_START_SUBSYS(2) 2
      }
   } else {
          set IL_START_SUBSYS(1) $ML_START_IL1
          set IL_START_SUBSYS(2) $ML_START_IL2
   }
}
#
# -------------------------------------------- fix the upper limit for the layer indices
#
if {$C=="1"} {
   set NLAY_SUBSYS(1) $ML_Ntot($ML,$C)
} else {
    if {$case=="a"} {
       set NLAY_SUBSYS(1) $ML_Na1($ML)
       set NLAY_SUBSYS(2) $ML_Na2($ML)
    } else {
       set NLAY_SUBSYS(1) $ML_Nb1($ML)
       set NLAY_SUBSYS(2) $ML_Nb2($ML)
    }
}

set SU_SUBSYS(1) 1
set SU_SUBSYS(2) 2

}
#=======================================================================================
#
set NLAY_SU(1) $ML_NLAY($ML,$C,1)
for {set IL 1} {$IL <= $NLAY_SU(1) } {incr IL} {
    set NSITES_SU($IL,1) $ML_NSITES($ML,$C,$IL,1)
}   
if {$C==2} {
set NLAY_SU(2) $ML_NLAY($ML,$C,2)
for {set IL 1} {$IL <= $NLAY_SU(2) } {incr IL} {
    set NSITES_SU($IL,2) $ML_NSITES($ML,$C,$IL,2)
}   
}

#
#----------------------------------------------- find number of sites in structure units
#
set NQ_SU1 0
set NQ_SU2 0
for {set IL 1} {$IL <= $ML_NLAY($ML,$C,1) } {incr IL} {
    for {set IS 1} {$IS <= $ML_NSITES($ML,$C,$IL,1)} {incr IS} { incr NQ_SU1 }
}
if {$C==2} {
for {set IL 1} {$IL <= $ML_NLAY($ML,$C,2) } {incr IL} {
    for {set IS 1} {$IS <= $ML_NSITES($ML,$C,$IL,2)} {incr IS} { incr NQ_SU2 }
}
}
#
#----------------------------------------------------------- scale distances and vectors
#
set SCALE_ISD [expr (100.0+$ML_SCALE_ISD)/100.0]
set SCALE_C   [expr (100.0+$ML_SCALE_C  )/100.0]
set SCALE_C1  [expr (100.0+$ML_SCALE_C1 )/100.0]
set SCALE_C2  [expr (100.0+$ML_SCALE_C2 )/100.0]

if {$C==1} {
   debug $PRC "scaling factor C  $SCALE_C"
   set ML_CX($ML,$C,1) [expr $SCALE_C * $ML_CX($ML,$C,1)]
   set ML_CY($ML,$C,1) [expr $SCALE_C * $ML_CY($ML,$C,1)]
   set ML_CZ($ML,$C,1) [expr $SCALE_C * $ML_CZ($ML,$C,1)]
} else {
   debug $PRC "scaling factor C1  $SCALE_C1"
   set ML_CX($ML,$C,1) [expr $SCALE_C1 * $ML_CX($ML,$C,1)]
   set ML_CY($ML,$C,1) [expr $SCALE_C1 * $ML_CY($ML,$C,1)]
   set ML_CZ($ML,$C,1) [expr $SCALE_C1 * $ML_CZ($ML,$C,1)]

   debug $PRC "scaling factor C2  $SCALE_C2"
   set ML_CX($ML,$C,2) [expr $SCALE_C2 * $ML_CX($ML,$C,2)]
   set ML_CY($ML,$C,2) [expr $SCALE_C2 * $ML_CY($ML,$C,2)]
   set ML_CZ($ML,$C,2) [expr $SCALE_C2 * $ML_CZ($ML,$C,2)]

   set CHECK [expr abs($ML_CY($ML,$C,1)*$ML_CZ($ML,$C,2)  \
                      -$ML_CZ($ML,$C,1)*$ML_CY($ML,$C,2)) \
                  +abs($ML_CZ($ML,$C,1)*$ML_CX($ML,$C,2)  \
                      -$ML_CX($ML,$C,1)*$ML_CZ($ML,$C,2)) \
                  +abs($ML_CX($ML,$C,1)*$ML_CY($ML,$C,2)  \
                      -$ML_CY($ML,$C,1)*$ML_CX($ML,$C,2))]
                  
   if {$CHECK>0.0000001} {give_warning "." \
              "WARNING \n\n vectors C1 and C2  \n\n  are not collinear \n\n " ; return}
#
# -------------------- modify ALAT if input refers to subsystem 2
#
   if {$ML_SUBSYS_REF == 2 } { set ALAT  [expr $ALAT * $ML_ALAT2to1($ML,$C)] }

}  ;#------------------------------------------------------------------------------- C=2

#
#------------------------------------------------------------ find NQ_bulk and NLAY_bulk
#
if {$dimension=="2D"} {
if {$ZRANGE_TYPE == "extended"} {

   for {set loop 1} {$loop <= 2 } {incr loop} {

      if {$loop==1} {
         set su $SU_SUBSYS(1)
      } else {
         set su $SU_SUBSYS($NSUBSYS)
      }

      set IQ   0 
      set ILAY 0
      for {set IL 1} {$IL <= $NLAY_SU($su) } {incr IL} {
          set IQ [expr $IQ + $NSITES_SU($IL,$su)]
      }

      if {$loop==1} {
         set NQ_bulk_L   [ expr $N_rep_SU_bulk_L * $IQ ]
         set NLAY_bulk_L [ expr $N_rep_SU_bulk_L * $NLAY_SU($su) ]
         set RBASX_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CX($ML,$C,$su) ]
         set RBASY_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CY($ML,$C,$su) ]
         set RBASZ_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CZ($ML,$C,$su) ]
      } else {
         set NQ_bulk_R   [ expr $N_rep_SU_bulk_R * $IQ ]
         set NLAY_bulk_R [ expr $N_rep_SU_bulk_R * $NLAY_SU($su) ]
         set RBASX_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CX($ML,$C,$su) ]
         set RBASY_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CY($ML,$C,$su) ]
         set RBASZ_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CZ($ML,$C,$su) ]
      }
    
   } 

}}
#
# ------------- find the thickness LDIST($IL,1) associated with layer $IL in subsystem 1
#
set ILTOP1 $ML_NLAY($ML,$C,1)
set WTOP  [expr 1.0+$ML_W1($ML,$C,1,1) ]
set sum_LDIST 0
set sum_NLDIST 0
if {$ML_NLAY($ML,$C,1)==1} {
    set LDIST(1,1) 1.0
    set NLDIST(1,1) 1.0
    set sum_LDIST  1.0
    set sum_NLDIST 1.0
} else {
for {set IL 1} {$IL <= $ILTOP1 } {incr IL} {
    set ILp [expr $IL + 1]
    set ILm [expr $IL - 1]
    if { $IL==1 } {
       set LDIST($IL,1) [expr 0.5*($ML_W1($ML,$C,$ILp,1)-$ML_W1($ML,$C,1,1) \
                                                  +$WTOP-$ML_W1($ML,$C,$ILTOP1,1))]
       set NLDIST($IL,1) [expr 1.0-$ML_W1($ML,$C,$ILTOP1,1)]
    } elseif { $IL==$ILTOP1 } {
       set LDIST($IL,1) [expr 0.5*($WTOP-$ML_W1($ML,$C,$ILm,1))]
       set NLDIST($IL,1) [expr abs($ML_W1($ML,$C,$IL,1)-$ML_W1($ML,$C,$ILm,1))]
    } else {
       set LDIST($IL,1) [expr 0.5*($ML_W1($ML,$C,$ILp,1)-$ML_W1($ML,$C,$ILm,1))]
       set NLDIST($IL,1) [expr abs($ML_W1($ML,$C,$IL,1)-$ML_W1($ML,$C,$ILm,1))]
    }
    set sum_LDIST [expr $sum_LDIST + $LDIST($IL,1)]
    set sum_NLDIST [expr $sum_NLDIST + $NLDIST($IL,1)]
}
}
if { [expr abs($sum_LDIST-1.0)] > 0.0000001 } {give_warning "." \
              "WARNING from <structure_multilayer_create> \n \
               \n sum interlayer distances /= 1 \n\n \
               for sub-system 1" ; return}
debug $PRC "SUM 1   $sum_LDIST "
puts "NSUM 1   $sum_NLDIST"

#
# ------------- find the thickness LDIST($IL,2) associated with layer $IL in subsystem 2
#
if {$C==2} {
set ILTOP2 $ML_NLAY($ML,$C,2)
set WTOP  [expr 1.0+$ML_W2($ML,$C,1,1) ]
set sum_LDIST 0
set sum_NLDIST 0
if {$ML_NLAY($ML,$C,1)==1} {
    set LDIST(1,2) 1.0
    set NLDIST(1,2) 1.0
    set sum_LDIST  1.0
} else {
for {set IL 1} {$IL <= $ILTOP2 } {incr IL} {
    set ILp [expr $IL + 1]
    set ILm [expr $IL - 1]
    if { $IL==1 } {
       set LDIST($IL,2) [expr 0.5*($ML_W2($ML,$C,$ILp,1)-$ML_W2($ML,$C,1,1) \
                                                 +$WTOP-$ML_W2($ML,$C,$ILTOP2,1))]
       set NLDIST($IL,2) [expr 1.0-$ML_W2($ML,$C,$ILTOP2,1)]
    } elseif { $IL==$ILTOP2 } {
       set LDIST($IL,2) [expr 0.5*($WTOP-$ML_W2($ML,$C,$ILm,1))]
       set NLDIST($IL,2) [expr abs($ML_W2($ML,$C,$IL,1)-$ML_W2($ML,$C,$ILm,1))]
    } else {
       set LDIST($IL,2) [expr 0.5*($ML_W2($ML,$C,$ILp,1)-$ML_W2($ML,$C,$ILm,1))]
       set NLDIST($IL,2) [expr abs($ML_W2($ML,$C,$IL,1)-$ML_W2($ML,$C,$ILm,1))]
    }
    set sum_LDIST [expr $sum_LDIST + $LDIST($IL,2)]
    set sum_NLDIST [expr $sum_NLDIST + $NLDIST($IL,2)]
}
}
if { [expr abs($sum_LDIST-1.0)] > 0.0000001 } {give_warning "." \
              "WARNING from <structure_multilayer_create> \n\n  \
               sum interlayer distances /= 1 \n\n \
               for sub-system 1" ; return}
debug $PRC "SUM 2   $sum_LDIST "
puts "NSUM 2   $sum_NLDIST"
}

#=======================================================================================
#
# ----------------------------------- get the atomic positions  RQ  in subsystem isubsys
#
#SW: set first layer at z=0 if no scaling is applied
set IQ 0
set wsu(1) 0.0
set wsu(2) 0.0

for {set isubsys 1} {$isubsys <= $NSUBSYS } {incr isubsys} {

   set su $SU_SUBSYS($isubsys)

   set IL [expr $IL_START_SUBSYS($isubsys) - 1]

   for {set ILAY 1} {$ILAY <= $NLAY_SUBSYS($isubsys) } {incr ILAY} {
      incr IL
      if {$IL>$NLAY_SU($su)} {set IL 1} 
 
      if {$ILAY==1} {
         if {$SCALE_ISD > 1.000} {
            set dw [expr 0.5*$LDIST($IL,$su)*$SCALE_ISD]
         } else {
            set dw 0.0
         }
      } else {
         set dw $NLDIST($IL,$su) 
      }
      set wsu($su) [expr $wsu($su) + $dw]
 
      for {set IS 1} {$IS <= $NSITES_SU($IL,$su)} {incr IS} {
         if {$su==1} {
         set u0 $ML_U1($ML,$C,$IL,$IS)
         set v0 $ML_V1($ML,$C,$IL,$IS)
         } else {
         set u0 $ML_U2($ML,$C,$IL,$IS)
         set v0 $ML_V2($ML,$C,$IL,$IS)
         }
   
         for {set I_rep_SU_a 1} {$I_rep_SU_a <= $N_rep_SU_a} {incr I_rep_SU_a} {
         for {set I_rep_SU_b 1} {$I_rep_SU_b <= $N_rep_SU_b} {incr I_rep_SU_b} {

	 set u [expr $u0 + $I_rep_SU_a - 1]
	 set v [expr $v0 + $I_rep_SU_b - 1]
        
         incr IQ
         set SU_IQ($IQ)  $su 

         set RQX($IQ) [expr $u*$ML_AX($ML,$C) + $v*$ML_BX($ML,$C)]
         set RQY($IQ) [expr $u*$ML_AY($ML,$C) + $v*$ML_BY($ML,$C)]
         set RQZ($IQ) [expr $u*$ML_AZ($ML,$C) + $v*$ML_BZ($ML,$C)]
   
         for {set sup 1} {$sup <= $C } {incr sup} {
            set RQX($IQ) [expr $RQX($IQ) + $wsu($sup)*$ML_CX($ML,$C,$sup)]
            set RQY($IQ) [expr $RQY($IQ) + $wsu($sup)*$ML_CY($ML,$C,$sup)]
            set RQZ($IQ) [expr $RQZ($IQ) + $wsu($sup)*$ML_CZ($ML,$C,$sup)]
         }
   
         debug $PRC "isubsys $isubsys SU $su ILAY $ILAY IQ $IQ IL $IL IS $IS w $wsu($su)"
         set aux [format "%3i  %12.6f%12.6f%12.6f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
         debug $PRC "coord $aux"

         }
         } 

      }
   
   
      if {$ILAY==$NLAY_SUBSYS($isubsys)} {
         if {$SCALE_ISD > 1.0} {
            set dw [expr 0.5*$LDIST($IL,$su)*$SCALE_ISD]
         } else {
#           set dw 0.0
            set dw [expr $LDIST($IL,$su)*$SCALE_ISD]
         }
      } else {
       set dw 0.0
      }
      set wsu($su) [expr $wsu($su) + $dw]
   }

}

#=======================================================================================

set NQ $IQ

set w1 $wsu(1) 
set w2 $wsu(2) 

set RBASX(1) [expr $N_rep_SU_a * $ML_AX($ML,$C)]
set RBASY(1) [expr $N_rep_SU_a * $ML_AY($ML,$C)]
set RBASZ(1) [expr $N_rep_SU_a * $ML_AZ($ML,$C)]
		    	       	 
set RBASX(2) [expr $N_rep_SU_b * $ML_BX($ML,$C)]
set RBASY(2) [expr $N_rep_SU_b * $ML_BY($ML,$C)]
set RBASZ(2) [expr $N_rep_SU_b * $ML_BZ($ML,$C)]

if {$C=="1"} {
   set RBASX(3) [expr $w1*$ML_CX($ML,$C,1)]
   set RBASY(3) [expr $w1*$ML_CY($ML,$C,1)]
   set RBASZ(3) [expr $w1*$ML_CZ($ML,$C,1)]
} else {     	      
   set RBASX(3) [expr $w1*$ML_CX($ML,$C,1) + $w2*$ML_CX($ML,$C,2)]
   set RBASY(3) [expr $w1*$ML_CY($ML,$C,1) + $w2*$ML_CY($ML,$C,2)]
   set RBASZ(3) [expr $w1*$ML_CZ($ML,$C,1) + $w2*$ML_CZ($ML,$C,2)]
}

for {set i 1} {$i <= 2} {incr i} {
   set RBASX_L($i) $RBASX($i)
   set RBASY_L($i) $RBASY($i)
   set RBASZ_L($i) $RBASZ($i)
   		             
   set RBASX_R($i) $RBASX($i)
   set RBASY_R($i) $RBASY($i)
   set RBASZ_R($i) $RBASZ($i)
}   

#
#----------------------------------------------------- set_lattice_parameters_using_RBAS
#

set_lattice_parameters_using_RBAS


#===============================================================================
destroy $Wcrml

set BRAVAIS 0

structure_per_pedes_read_coord use_primitive_vectors

}   
#                                                        structure_multilayer_create END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                                                   MAIN
########################################################################################
global CONC NCL flag BOA COA RCLU RCLV RCLW ALAT
global LATPAR LATANG NQCL NOQ CONC RWS 
global flagset TABBRAVAIS MAG_DIR TXTT
global NQ  NCL  BRAVAIS  SPACEGROUP SPACEGROUP_AP
global Wcsys Krasmol Wpprc STRUCTURE_SETUP_MODE  W_sites_list
global CHECK_TABLE

set STRUCTURE_SETUP_MODE ""
set W_sites_list ""

global structure_window_calls; set structure_window_calls     0 

set MAG_DIR(1) 0
set MAG_DIR(2) 0
set MAG_DIR(3) 0

set flag 1
set tcl_precision 17
global create_structure_lock     

set create_structure_lock 0
set Krasmol 1
set Wpprc .dummy

reset_struc_parameter

table_bravais

table_RWS

structure_window_2D


#
#-------------------------------------------------
###set QUICK 0
###if {$QUICK==1} { 
###  set ALAT 6
###  global LATTICE_TYPE ZRANGE_TYPE
###  set LATTICE_TYPE heterogeneous ; set ZRANGE_TYPE slab
###  structure_specify_2D_lattice 
###}
#-------------------------------------------------


} ; # CREATE SYSTEM 2D
