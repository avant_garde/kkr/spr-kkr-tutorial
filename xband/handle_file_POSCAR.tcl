########################################################################################
proc handle_file_POSCAR {} {
#
#  read       POSCAR   file
#
global sysfile syssuffix NCPA
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT TXTT0
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global NM IMT IMQ RWSM NAT QMTET QMPHI
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION
#
set tcl_precision 17

set file "POSCAR"

set PRC handle_file_POSCAR

if {[file exists $file]} {
  writescrd .d.tt $PRC "\n\nINFO from <handle_file_POSCAR>: \
  reading  POSCAR-file $file  \n \n" 
} else {
  writescrd .d.tt $PRC "\n  POSCAR-file $file does not exist \n "
  return
}
    
set datfil [open "$file" r]


set DIMENSION "3D"
set sysfile   POSCAR
#
#--------------------------- supply table
#
table_bravais

#=======================================================================================
#                        read     POSCAR   file
#=======================================================================================
#

gets $datfil system               
if {[string trim $system] == ""} {
  writescrd .d.tt $PRC "\n system file $sysfile$syssuffix is empty \n "
  return
}
set pos [string first "!" $system]
if {$pos>=0} {set system [string range $system 0 [expr $pos-1]]} 

gets $datfil line
set ALAT [lindex $line 0]
set BOA 1
set COA 1
writescrd .d.tt $PRC "\n$line\n[format %18.12f $ALAT]\n"
#writescrd .d.tt $PRC "\n$line\n[format %18.12f%18.12f $BOA $COA]\n"

set BRAVAIS 0

set SPACEGROUP    0
set SPACEGROUP_AP 0

set STRUCTURE_TYPE "UNKNOWN"

set LATPAR(1) $ALAT
set LATPAR(2) $ALAT
set LATPAR(3) $ALAT
writescrd .d.tt $PRC "\nlattice patrameters \n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATPAR(1) $LATPAR(2) $LATPAR(3)]\n"

set LATANG(1) 90
set LATANG(2) 90
set LATANG(3) 90
writescrd .d.tt $PRC "\nangles \n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATANG(1) $LATANG(2) $LATANG(3)]\n"

for {set I 1} {$I <= 3} {incr I} {
   gets $datfil line
   set RBASX($I) [lindex $line 0]
   set RBASY($I) [lindex $line 1]
   set RBASZ($I) [lindex $line 2]
   writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $RBASX($I) $RBASY($I) $RBASZ($I)]\n"
}
#
#--------------------------------------------------------------------------------- sites
#
gets $datfil line
set NQ [lindex $line 0]
writescrd .d.tt $PRC "\n$line\n[format %3i $NQ]\n"

gets $datfil MODE
set pos [string first " " $MODE]
if {$pos>=0} {set MODE [string range $MODE 0 [expr $pos-1]]} 

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   gets $datfil line
   set ICLQ($IQ) $IQ

   set RQX($IQ)  [lindex $line 0]
   set RQY($IQ)  [lindex $line 1]
   set RQZ($IQ)  [lindex $line 2]
   set RWS($IQ) 1
   set NLQ($IQ) 3
   set NOQ($IQ) 1
   set ITOQ(1,$IQ) 1
}

close $datfil

#
#--------------------------------------------------------------------------------- types
#
set NT 1

for {set IT 1} {$IT <= $NT} {incr IT} {
   set   ZT($IT) 1
   set TXTT($IT) "XX"
   set TXTT0($IT) "XX"
   set  NAT($IT) $NQ
   set CONC($IT) 1

   for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
      set IQAT($IA,$IT) $IQ
   }
}


#=======================================================================================
#                        read     CONVCAR   file
#=======================================================================================
#
set file "CONVCAR"

if {[file exists $file]} {
  writescrd .d.tt $PRC "\n\nINFO from <handle_file_POSCAR>: \
  reading  CONVCAR-file $file  \n \n" 
    
set datfil [open "$file" r]

gets $datfil line 
gets $datfil line

for {set I 1} {$I <= 3} {incr I} {
   gets $datfil line
}
#
#--------------------------------------------------------------------------------- sites
#
gets $datfil line
set NQ [lindex $line 0]
writescrd .d.tt $PRC "\n$line\n[format %3i $NQ]\n"

gets $datfil MODE
set pos [string first " " $MODE]
if {$pos>=0} {set MODE [string range $MODE 0 [expr $pos-1]]} 

for {set IQ [expr $NQ+1]} {$IQ <= [expr $NQ+$NQ]} {incr IQ} {
   gets $datfil line
   set ICLQ($IQ) $IQ

   set RQX($IQ)  [lindex $line 0]
   set RQY($IQ)  [lindex $line 1]
   set RQZ($IQ)  [lindex $line 2]
   set RWS($IQ) 1
   set NLQ($IQ) 3
   set NOQ($IQ) 1
   set ITOQ(1,$IQ) 2
}
set NQ0 $NQ
set NQ [expr $NQ+$NQ]

close $datfil
#
#--------------------------------------------------------------------------------- types
#
set NT 2

for {set IT 1} {$IT <= $NT} {incr IT} {
   set   ZT($IT) 1
   set TXTT($IT) "XX"
   set TXTT0($IT) "XX"
   set  NAT($IT) $NQ0
   set CONC($IT) 1

   for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
      set IQAT($IA,$IT) $IQ
   }
}



} else {
  writescrd .d.tt $PRC "\n  POSCAR-file $file does not exist \n "
}
#=======================================================================================



#
#-------------------------------------------------------------------------- site classes
#
set NCL $NQ

for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   set WYCKOFFCL($ICL) "-"
   set NQCL($ICL) 1

   for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
      set IQECL($IE,$ICL)  $IQ
   }
}

set MAG_DIR(1) 0
set MAG_DIR(2) 0
set MAG_DIR(3) 1

set NCPA 0
#
#----------------------------------------------------------------------- find mesh table
#
set NM 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  set IMQ($IQ) 0
  for {set IM 1} {$IM <= $NM} {incr IM} {
    if {$RWS($IQ)==$RWSM($IM)} {set IMQ($IQ) $IM}
  }

  if {$IMQ($IQ)==0} {
     incr NM
     set RWSM($NM) $RWS($IQ)
     set IMQ($IQ) $NM  
  }

  for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
     set IT $ITOQ($IO,$IQ)
     set IMT($IT) $IMQ($IQ)
  }
}
#
#---------------------------------------------------------------------- init QMTET QMPHI
#
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set QMTET($IQ) 0.0
   set QMPHI($IQ) 0.0
}

writescrd .d.tt $PRC "  \n\nsystem file $file read in \n "
sites_graphik 

write_sysfile

}  
#                                                                handle_file_POSCAR END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
