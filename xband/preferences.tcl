 ########################################################################################
 #                                SET_PREFERENCES
 ########################################################################################
 #                                          
 #
 # do not forget to check vst* procs in ut.tcl
 #
 proc set_preferences { } {
     global COLOR key_COLOR key_GEOMETRY
     global FONT RASMOL PLOTPROG
     global DATAEXPLORER 
     global COLOR WIDTH HEIGHT SYMMETRY PRINTPS 
     global PROG_PATH0 PROG_DIRECT PROG_OPTION
     global GREP1 GREP2 GREP3 DEBUG
     global NPACKAGE PACKAGE_NAME PROGS_NAME_LIST_TAB
     global XCRYSMODE

     set PRC "set_preferences"
     set wtext   30
     set wentry  30

     set win .win_set_preferences

     toplevel_init $win "SET PREFERENCES" 0 0
     wm geometry $win +300+200  
 #---------------------------------------------------------------------------
     frame $win.color -relief raised -borderwidth 2

     label $win.color.label -text "colors" -width $wtext -justify left  -width 20

     radiobutton $win.color.col1 -variable key_COLOR -width 20 \
		 -text "colored" -value 1 -highlightthickness 0                    
     radiobutton $win.color.col2 -variable key_COLOR -width 20 \
		 -text "grey"    -value 2 -highlightthickness 0

     pack $win.color -expand true -anchor w  -fill x
     pack $win.color.label $win.color.col1 $win.color.col2 -side left -anchor w
     $win.color.col$key_COLOR select

 #---------------------------------------------------------------------------
     frame $win.geo -relief raised -borderwidth 2

     label $win.geo.label -text "geometry" -width $wtext -justify left -width 20

     radiobutton $win.geo.geo1 -variable key_GEOMETRY -width 20 \
		 -text "standard" -value 1 -highlightthickness 0                       
     radiobutton $win.geo.geo2 -variable key_GEOMETRY -width 20 \
		 -text "small" -value 2 -highlightthickness 0                       

     pack $win.geo -expand true -anchor w  -fill x
     pack $win.geo.label $win.geo.geo1 $win.geo.geo2 -side left -anchor w
     $win.geo.geo$key_GEOMETRY select 


 #---------------------------------------------------------------------------
     frame $win.debug -relief raised -borderwidth 2

     label $win.debug.label -text "debugging" -width $wtext -justify left -width 20

     radiobutton $win.debug.debug1 -variable DEBUG -width 20 \
		 -text "NO info" -value 0 -highlightthickness 0                       
     radiobutton $win.debug.debug2 -variable DEBUG -width 20 \
		 -text "print info" -value 1 -highlightthickness 0                       

     pack $win.debug -expand true -anchor w  -fill x
     pack $win.debug.label $win.debug.debug1  $win.debug.debug2 -side left -anchor w

 #---------------------------------------------------------------------------
     set Wfonts $win.fonts
     frame $Wfonts -relief raised -borderwidth 2
     label $Wfonts.label -text "fonts settings" -width $wtext -justify left 
     button $Wfonts.button -command "font_select set" -width $wentry \
			    -text modify 
     pack  $Wfonts -expand true -anchor w -fill x
     pack  $Wfonts.label $Wfonts.button -side left


 #---------------------------------------------------------------------------
     set Weditor $win.editor
     frame $Weditor -relief raised -borderwidth 2
     label $Weditor.label -text "editor settings" -width $wtext -justify left 
     button $Weditor.button -command ev -width $wentry \
			    -text modify 
     pack  $Weditor -expand true -anchor w -fill x
     pack  $Weditor.label $Weditor.button -side left

 #---------------------------------------------------------------------------
     set Wprint $win.print
     frame $Wprint -relief raised -borderwidth 2
     label $Wprint.label -text "print-command for ps-files" -width $wtext -justify left
     entry $Wprint.entry -textvariable PRINTPS -width $wentry
     pack  $Wprint -expand true -anchor w -fill x
     pack  $Wprint.label $Wprint.entry -side left

 #---------------------------------------------------------------------------
     set Wrasmol $win.rasmol
     frame $Wrasmol -relief raised -borderwidth 2
     label $Wrasmol.label -text "rasmol-version" -width $wtext -justify left
     entry $Wrasmol.entry -textvariable RASMOL -width $wentry
     pack  $Wrasmol -expand true -anchor w -fill x
     pack  $Wrasmol.label $Wrasmol.entry -side left

 #---------------------------------------------------------------------------

   if {!$XCRYSMODE} {    
     set Wdx $win.dx
     frame $Wdx -relief raised -borderwidth 2
     label $Wdx.label -text "dataexplorer-version" -width $wtext -justify left
     entry $Wdx.entry -textvariable DATAEXPLORER -width $wentry
     pack  $Wdx -expand true -anchor w -fill x
     pack  $Wdx.label $Wdx.entry -side left

 #---------------------------------------------------------------------------
 # -> automatic setting      set Wplot $win.plot
 # -> automatic setting      frame $Wplot -relief raised -borderwidth 2
 # -> automatic setting      label $Wplot.label -text "plot - full path" -width $wtext -justify left
 # -> automatic setting      entry $Wplot.entry -textvariable PLOTPROG -width $wentry
 # -> automatic setting      pack  $Wplot -expand true -anchor w -fill x
 # -> automatic setting      pack  $Wplot.label $Wplot.entry -side left

 #---------------------------------------------------------------------------
 # -> automatic setting      set Wsymm $win.symm
 # -> automatic setting      frame $Wsymm -relief raised -borderwidth 2
 # -> automatic setting      label $Wsymm.label -text "symmetry - full path" -width $wtext -justify left
 # -> automatic setting      entry $Wsymm.entry -textvariable SYMMETRY -width $wentry
 # -> automatic setting      pack  $Wsymm -expand true -anchor w -fill x
 # -> automatic setting      pack  $Wsymm.label $Wsymm.entry -side left

 #---------------------------------------------------------------------------
     set Wprogcall $win.progcall
     frame $Wprogcall -relief raised -borderwidth 2
     pack  $Wprogcall -expand true -anchor w -fill x
     label $Wprogcall.label -text "PROGRAM CALLS" -width $wtext -justify left
     pack  $Wprogcall.label 

     for {set ipack 1} {$ipack<=$NPACKAGE} {incr ipack} {

	set PACKAGE $PACKAGE_NAME($ipack)
	set PROG   [lindex $PROGS_NAME_LIST_TAB($PACKAGE) 0]
       set wpack $Wprogcall.f_$PACKAGE
       frame $wpack 
       pack  $wpack 
       label $wpack.lab1  -text "$PACKAGE" -width $wtext -justify left
       label $wpack.lab2  -text "$PROG"      -justify left -width 6
       entry $wpack.ent1  -textvariable PROG_DIRECT($PACKAGE) -width 1
       label $wpack.lab3  -text "inputfile" -justify left
       entry $wpack.ent2  -textvariable PROG_OPTION($PACKAGE) -width 10

       pack  $wpack.lab1 $wpack.lab2 $wpack.ent1 $wpack.lab3 $wpack.ent2  -anchor w -fill x -side left
    }

###    debug $PRC "PROG_DIRECT:  \[$PROG_DIRECT\]    PROG_OPTION: \[$PROG_OPTION\] "
#---------------------------------------------------------------------------
    set Wprogpath $win.progpath
    frame $Wprogpath -relief raised -borderwidth 2
    label $Wprogpath.label -text "PROG_PATH0 - default path" -width $wtext -justify left
    entry $Wprogpath.entry -textvariable PROG_PATH0 -width $wentry
    pack  $Wprogpath -expand true -anchor w -fill x
    pack  $Wprogpath.label $Wprogpath.entry -side left

#---------------------------------------------------------------------------

    for {set i 1} {$i<=3} {incr i} {
	set Wgrep $win.grep$i
	frame $Wgrep -relief raised -borderwidth 2
	label $Wgrep.label -text "grep string $i" -width $wtext -justify left
	entry $Wgrep.entry -textvariable GREP$i -width $wentry
	pack  $Wgrep -expand true -anchor w -fill x
	pack  $Wgrep.label $Wgrep.entry -side left
    }
} ; # end XCRYSMODE    
#---------------------------------------------------------------------------
  frame $win.buts

  button $win.buts.button2 -text "CLOSE" -width 15 -height 3  \
  	   -command "destros $win "
  
  button $win.buts.button3 -text "STORE" -width 15 -height 3  \
	  -command {destros .win_set_preferences ; store_preferences }

  pack $win.buts
  pack $win.buts.button2 $win.buts.button3 \
  	 -in $win.buts  -side left -fill x -expand yes

#---------------------------------------------------------------------------
proc  store_preferences { } {
    global COLOR key_COLOR key_GEOMETRY env
    global PROG_PATH PROG_PATH0
    
    set vst $env(HOME)/.xband.vst
    set i [expr [string length $PROG_PATH0] -1 ]
    if {[string range $PROG_PATH0 $i $i]!="/"} {set PROG_PATH0 "$PROG_PATH0/"}
    set PROG_PATH $PROG_PATH0     
    
    read_preferences     ; # from $xband_path/locals/
    vst_write_file $vst  ; # store all present settings
    
    eval set res [catch "exec cat $vst " message]
    writescr0 .d.tt "\nnew settings in file $vst  \n\n" 
    writescr  .d.tt "\n$message \n\n "                                  
}                                                           
#---------------------------------------------------------------------------
#


}

########################################################################################
#                                READ_PREFERENCES
########################################################################################
#                                          
proc  read_preferences { } {

    set PRC "read_preferences"
    
    global COLOR WIDTH HEIGHT FONT
    global key_COLOR key_GEOMETRY
    global xband_path
    
#=======================================================================================
    set file $xband_path/locals/colors.vst

    if {[file readable $file]==1} {

	set fcol [open $file r]

	while {[gets $fcol line] > -1} {
	    set line [string trim $line ] 
	    set key  [string range $line 0 0]
	    
	    if { $key != "#" } {
		set ind [lindex $line 0]
		set val [string trim [lindex $line $key_COLOR]]
		if {$val!= ""} {set COLOR($ind) $val}
		debug $PRC "from colors.vst >>>>>>> COLOR($ind) $val"
	    } 
	}

    } else {
	give_warning "." "WARNING \n\n the color table file \n\n $file  \
		\n\n is not available \n\n DEFAULTS will be used "
    }
    
#=======================================================================================
    set file $xband_path/locals/geometry.vst

    if {[file readable $file]==1} {

	set fcol [open $file r]
	
	set kgeo  [expr $key_GEOMETRY + 1]

	while {[gets $fcol line] > -1} {
	    set line [string trim $line ] 
	    set key  [string range $line 0 0]
	    
	    if { $key != "#" } {
		set var [lindex $line 0]
		set ind [lindex $line 1]
		set val [string trim [lindex $line $kgeo]]
		if {$val!= ""} {set [string trim $var]($ind) $val}
		debug $PRC "from geometry.vst >>>>>>> [string trim $var]($ind) $val"
	    } 
	}
	
    } else {
	give_warning "." "WARNING \n\n the color table file \n\n $file  \
                \n\n is not available \n\n DEFAULTS will be used "
    }
    

#=======================================================================================
    set file $xband_path/locals/fonts.vst

    if {[file readable $file]==1} {
    
	set fcol [open $file r]

	while {[gets $fcol line] > -1} {
	    set line [string trim $line ] 
	    set key  [string range $line 0 0]
	    
	    if { $key != "#" } {
		set ind [string trim [lindex $line 0]]
		if { $ind != "" } {
		    set val [string trim [lindex $line 1]]
		    if {$val!= ""} {set FONT($ind) $val}
		    debug $PRC "from fonts.vst >>>>>>> FONT($ind) $val"
		}
	    } 
	}
	
    } else {
	give_warning "." "WARNING \n\n the font table file \n\n $file  \
                \n\n is not available \n\n DEFAULTS will be used "
    }
    
    
#=======================================================================================
}

