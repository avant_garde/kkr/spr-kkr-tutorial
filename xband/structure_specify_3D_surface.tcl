########################################################################################
#                         STRUCTURE 3D SURFACE
########################################################################################
#                                                enter structure for 3D SURFACE
proc structure_specify_3D_surface {  } {

global sysfile syssuffix COLOR FONT HEIGHT Wsurfa
global system
global W_basvec 
#
#------------------------------------------------------------------------------------ ND
#
global structure_window_calls
global sysfile syssuffix sysfile_3DBULK 
global TABCHSYM Wcsys
global system NQ NT ZT CONC RQX RQY RQZ NOQ NLQ TXTT
global ITOQ BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT
global NM IMT IMQ RWSM NAT
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION
global IQ_3DSURF h_3DSURF k_3DSURF l_3DSURF o_3DSURF d_3DSURF
#
#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE 
global struc_L_eq_struc_R bulk_L_eq_bulk_R 
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global N_rep_SU_bulk_L N_rep_SU_bulk_L N_rep_SU_bulk_R

global PRC

set PRC structure_specify_3D_surface

debug $PRC WRHEAD


#=======================================================================================
#               check whether system file   sysfile_3DBULK   is set 
#=======================================================================================
if { $sysfile_3DBULK=="UNKNOWN" } {

    set win .c_s_sys
    toplevel_init $win "Create or select a system file" 0 0
    wm geometry $win +50+200
    wm positionfrom .c_s_sys user
    wm sizefrom .c_s_sys ""
    wm minsize .c_s_sys 

    set buttonbgcolor green1 
    set buttonheight  2

    label .c_s_sys.lab -text "create or select a system file first"
#
#---------------------------------------------------- CREATE SYSTEM 
#
    button .c_s_sys.csys -text "CREATE SYSTEM" -width 20 -height $buttonheight \
	-command {create_system; destroy .c_s_sys} -bg $buttonbgcolor
#
#---------------------------------------------------- SELECT SYSTEM 
#
     button .c_s_sys.ssys -text "SELECT SYSTEM" -width 20 -height $buttonheight \
	-command {select_system; destroy .c_s_sys} -bg $buttonbgcolor

#
#---------------------------------------------------- CLOSE
#
     button .c_s_sys.ende -text "CLOSE" -width 20 -height $buttonheight \
	-command {destroy .c_s_sys} -bg $COLOR(CLOSE)

     pack configure .c_s_sys.lab .c_s_sys.csys .c_s_sys.ssys .c_s_sys.ende \
            -anchor s  -side top -pady 15 -padx 30

}
#=======================================================================================


#=======================================================================================
#                                system file available
#=======================================================================================

set  sysfile  $sysfile_3DBULK

debug $PRC  "system file  $sysfile$syssuffix"

if [file exists $sysfile$syssuffix] {

   read_sysfile

} else {
    give_warning . "WARNING \n\n the system file \n\n $sysfile$syssuffix \
            \n\n does not exist"
    return
}             

set struc_L_eq_struc_R YES

#---------------------------------------------------------------------------------------
#                            store 3D bulk unit cell info
#---------------------------------------------------------------------------------------
  global RBASX_3DBULK RBASY_3DBULK RBASZ_3DBULK NQ_3DBULK NOQ_3DBULK ITOQ_3DBULK
  global   RQX_3DBULK   RQY_3DBULK   RQZ_3DBULK RWS_3DBULK NLQ_3DBULK
  global NT_3DBULK ZT_3DBULK TXTT_3DBULK CONC_3DBULK IMQ_3DBULK 

  for {set I 1} {$I <= 3} {incr I} {
     set RBASX_3DBULK($I) $RBASX($I)
     set RBASY_3DBULK($I) $RBASY($I)
     set RBASZ_3DBULK($I) $RBASZ($I)
     puts "3DBULK RBAS $I  $RBASX_3DBULK($I) $RBASY_3DBULK($I) $RBASZ_3DBULK($I)  "
  }

  set NQ_3DBULK $NQ

  for {set IQ 1} {$IQ <= $NQ_3DBULK} {incr IQ} {
     set RQX_3DBULK($IQ) $RQX($IQ)
     set RQY_3DBULK($IQ) $RQY($IQ)
     set RQZ_3DBULK($IQ) $RQZ($IQ)
     puts "3DBULK RQ $IQ $RQX_3DBULK($IQ) $RQY_3DBULK($IQ) $RQZ_3DBULK($IQ)  "
  }

  for {set IQ 1} {$IQ <= $NQ_3DBULK} {incr IQ} {
     set RWS_3DBULK($IQ) $RWS($IQ)
     set NLQ_3DBULK($IQ) $NLQ($IQ)
     puts "3DBULK $IQ  NL $NLQ_3DBULK($IQ)   RWS $RWS_3DBULK($IQ)"
  }

  for {set IQ 1} {$IQ <= $NQ_3DBULK} {incr IQ} {
     set IMQ_3DBULK($IQ)  $IMQ($IQ)  
     set NOQ_3DBULK($IQ)  $NOQ($IQ)  
     for {set IO 1} {$IO <= $NOQ_3DBULK($IQ)} {incr IO} {
	 set ITOQ_3DBULK($IO,$IQ)  ITOQ($IO,$IQ)
     }
     puts "3DBULK NOQ  $NOQ_3DBULK($IQ)  $ITOQ_3DBULK(1,$IQ)     "
  }

  set NT_3DBULK $NT

  for {set IT 1} {$IT <= $NT_3DBULK} {incr IT} {
     set   ZT_3DBULK($IT)   $ZT($IT)
     set TXTT_3DBULK($IT) $TXTT($IT)
     set CONC_3DBULK($IT) $CONC($IT)
  }

#---------------------------------------------------------------------------------------

    set Wsurfa .w_surfa 
    if {[winfo exists $Wsurfa]} {destroy $Wsurfa} 
    toplevel_init $Wsurfa "create surface of 3-dimensional system  STEP A" 0 0
    wm sizefrom $Wsurfa ""
    wm minsize  $Wsurfa 100 100
    
    # top button frame
    frame $Wsurfa.head

    pack $Wsurfa.head -fill both -expand yes
    
    button $Wsurfa.head.cancel -text "CLOSE" \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	    -command "read_sysfile ; destroy $Wsurfa" 
    button $Wsurfa.head.reset -text "RESET" \
	    -width 25 -height $HEIGHT(BUT1) -bg tomato  \
	    -command "read_sysfile ; create_system_0D" 
    button $Wsurfa.head.rasmol -text "SHOW STRUCTURE"  \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	    -bg $COLOR(GREEN) -command "sites_graphik"
    button $Wsurfa.head.goon -text "GO ON" \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(DONE)  \
	    -command " structure_specify_3D_surface_goon ; destroy $Wsurfa" 
    
    ### ;$Wsurfa.head.cancel $Wsurfa.head.reset $Wsurfa.head.rasmol 
    pack $Wsurfa.head.cancel                    $Wsurfa.head.rasmol  \
	 $Wsurfa.head.goon  \
	    -side left -expand yes -pady 5

#=======================================================================================

    set BG1 linen                
    set BG2 SteelBlue1 
    set BG3 ivory1
    set BG4 LightSalmon1
    set BG5 lightgreen

    global BGFIL ; set BGFIL  LightPink1
    global BGTSK ; set BGTSK  LightSalmon1
    global BGCPA ; set BGCPA  wheat
    global BGTAU ; set BGTAU  orange1 
    global BGWR  ; set BGWR   lavender
    global BGE   ; set BGE    moccasin 
    global BGP   ; set BGP    ivory1
    global BGX   ; set BGX    cornsilk1  
    global BGSCF ; set BGSCF  LemonChiffon1
    global BGCRL ; set BGCRL  orange1 
    
    set BGSCL orange     
    set BG_A khaki1
    set BG_a gold1
    set BG_x yellow1
    set BG_b goldenrod1
    set we_old 6
    set we_new 8
    set we_atoms 30
    
    # lower mainframe
    frame $Wsurfa.a 
    pack $Wsurfa.a -in $Wsurfa -side top -expand yes
    
    set ACOL1 $Wsurfa.a.col1 ; frame $ACOL1 -bg $BG1  
    set ACOL2 $Wsurfa.a.col2 ; frame $ACOL2 -bg $BG2                                 
    set ACOL3 $Wsurfa.a.col3 ; frame $ACOL3 -bg $BG3                                 
    
    pack $ACOL3 $ACOL2 $ACOL1 -side right -anchor n -fill both -expand yes
 
#=======================================================================================
#=======================================================================================
#                                     COLUMN 1
#=======================================================================================
#=======================================================================================
#                              lattice parameter
#
    set ALAT [expr 1.0 * $ALAT] ; # just to skip trailing 0's

    frame $ACOL1.sys -bg $BG1

    label $ACOL1.sys.txt  -text "3D bulk system  "      -bg $BG1
    label $ACOL1.sys.nam  -textvariable sysfile -bg $BG1
    pack  $ACOL1.sys.txt $ACOL1.sys.nam  -padx 5  -side left
    pack  $ACOL1.sys -padx 3 -pady 12 -anchor nw
    
    frame $ACOL1.alat -bg $BG1
    label $ACOL1.alat.lab  -text "lattice parameter  A  " -bg $BG1
    
    entry $ACOL1.alat.ent  -font $FONT(GEN) -width 7      -bg $BG1 \
	    -textvariable ALAT  -relief flat \
	    -highlightthickness 0 -state disabled 
    label $ACOL1.alat.uni  -text "  \[a.u.\]" -bg $BG1
    
    pack  $ACOL1.alat.lab $ACOL1.alat.ent $ACOL1.alat.uni -padx 5  -side left
    pack  $ACOL1.alat -padx 3 -pady 7 -anchor w
    
    #=======================================================================================
    #                              primitive vectors

    label $ACOL1.head  -text "primitive vectors " -bg $BG1
    pack  $ACOL1.head -anchor c 
    
    # just to skip trailing 0's
    
    for {set i 1} {$i <= 3} {incr i} {
	set RBASX($i) [expr 1.0 * $RBASX($i)]
	set RBASY($i) [expr 1.0 * $RBASY($i)]
	set RBASZ($i) [expr 1.0 * $RBASZ($i)]
    }
    
    for {set i 1} {$i <= 3} {incr i} {
        set RBASX_L($i) $RBASX($i)
        set RBASY_L($i) $RBASY($i)
        set RBASZ_L($i) $RBASZ($i)
        set RBASX_R($i) $RBASX($i)
        set RBASY_R($i) $RBASY($i)
        set RBASZ_R($i) $RBASZ($i)
    }
	
    set N_prim_vec 3
    
    frame $ACOL1.pvec -bg $BG1 
    pack  $ACOL1.pvec -side top
    
    for {set i 1} {$i <= $N_prim_vec} {incr i} {
	
	frame $ACOL1.pvec.cmp$i -bg $BG1
	
	if {$i<=3} {
	    label $ACOL1.pvec.cmp$i.lab  -text "a$i \[A\]" -width $we_old -bg $BG1
	    entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASX($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1
	    entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASY($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1
	    entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
		    -textvariable RBASZ($i) -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -state disabled -bg $BG1      
	} else {
	    label $ACOL1.pvec.cmp$i.lab  -width $we_old -bg $BG1
	    entry $ACOL1.pvec.cmp$i.x    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1
	    entry $ACOL1.pvec.cmp$i.y    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1
	    entry $ACOL1.pvec.cmp$i.z    -font $FONT(GEN) -width $we_old -relief sunken \
		    -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG) \
		    -highlightthickness 0 -bg $BG1    
	    if {$i==4} {
		frame $ACOL1.pvec.space -bg $BG1
		pack  $ACOL1.pvec.space -pady 2
		$ACOL1.pvec.cmp$i.lab  configure -text "a3 (L)"
		$ACOL1.pvec.cmp$i.x configure -textvariable RBASX_L(3) -state normal
		$ACOL1.pvec.cmp$i.y configure -textvariable RBASY_L(3) -state normal
		$ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_L(3) -state normal
		
	    } else {
		$ACOL1.pvec.cmp$i.lab  configure -text "a3 (R)"
		$ACOL1.pvec.cmp$i.x configure -textvariable RBASX_R(3)
		$ACOL1.pvec.cmp$i.y configure -textvariable RBASY_R(3)
		$ACOL1.pvec.cmp$i.z configure -textvariable RBASZ_R(3)
	    }        
	    $ACOL1.pvec.cmp$i.x configure -state disabled
	    $ACOL1.pvec.cmp$i.y configure -state disabled
	    $ACOL1.pvec.cmp$i.z configure -state disabled
	}
	
	pack  $ACOL1.pvec.cmp$i.lab   $ACOL1.pvec.cmp$i.x $ACOL1.pvec.cmp$i.y  \
		$ACOL1.pvec.cmp$i.z     -side left
	pack  $ACOL1.pvec.cmp$i 
	
    }
    #
    
    label $ACOL1.space1 -text " " -bg $BG1
    label $ACOL1.space2 -text "original basis vectors" -bg $BG1
    pack $ACOL1.space1 $ACOL1.space2 -fill both -expand true  -anchor c
#=======================================================================================
#                                         basis vectors    
    set W_basvec $ACOL1.basis
    frame $W_basvec -bg $BG1
    pack $W_basvec -padx 4 -fill both -expand true -side top
    
    scrollbar $W_basvec.sby -bg grey77 -command [list $W_basvec.li yview] -orient vertical
    scrollbar $W_basvec.sbx -bg grey77 -command [list $W_basvec.li xview] -orient horizontal
    
    listbox $W_basvec.li -bg $COLOR(LIGHTBLUE) -height 20 -width 35 -font $FONT(GEN) \
	    -xscrollcommand [list $W_basvec.sbx set] -yscrollcommand [list $W_basvec.sby set]
    
    pack $W_basvec     -side bottom -expand 1 -fill both -padx 5 -pady 5
    pack $W_basvec.sbx -side bottom -expand 0 -fill x
    pack $W_basvec.sby -side left   -expand 0 -fill y
    pack $W_basvec.li  -side right  -expand 1 -fill both 
    
    #-----------------------------------------------------------------
    proc display_basis_vectors { } {
	
	global W_basvec NQ RQX RQY RQZ TXTT NOQ ITOQ 
	
	$W_basvec.li delete 0 end
	$W_basvec.li insert 0 "   IQ     X     Y     Z      TXTT"
	
	for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	    
	    set txt0 "  "
	    set txt1 [format "%3i  %6.2f%6.2f%6.2f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
	    set txt2 "  "
	    
	    for {set i 1} {$i <= $NOQ($IQ)} {incr i} {
		set IT $ITOQ($i,$IQ)
		
		set txt2 "$txt2[format "%4s " $TXTT($IT)]"
	    }
	    $W_basvec.li insert $IQ "$txt0$txt1$txt2"
	}
	
    } 
#-----------------------------------------------------------------
    
    display_basis_vectors
    
#
#---------------------------------------------------------------------------------------
#
    

set wlb2 7
set wlb3 5
set wbut 3
set wlab1 10
set lslid 180
set NMLMAX 150
set BGCOL2 $COLOR(LIGHTSALMON1)
set BGCOL3 $COLOR(MOCCASIN)

#
#=======================================================================================
#=======================================================================================
#                                     COLUMN 2
#=======================================================================================
#=======================================================================================

frame $ACOL2.head -bg $BGCOL2
label $ACOL2.head.space1 -text " " -width $wlab1 -bg $BGCOL2 -padx 20  -pady 5
label $ACOL2.head.lab1 -text "specify surface system " -bg $BGCOL2 -padx 20   -pady 5
label $ACOL2.head.lab2 -text "bulk(L) ||||||||| vac" -bg $BGCOL2 -padx 20 -pady 5
pack $ACOL2.head.space1 $ACOL2.head.lab1 -anchor w -side top  
pack $ACOL2.head.lab2                   -anchor c -side top  
pack $ACOL2.head  -fill x

frame $ACOL2.bulk -bg $BGCOL2
label $ACOL2.bulk.space1 -text " " -width $wlab1 -bg $BGCOL2 -padx 20 
label $ACOL2.bulk.lab1 -text "bulk(L) = vac" -bg $BGCOL2 -padx 20 
set bulk_L_eq_bulk_R "NO" 
radiobutton $ACOL2.bulk.b  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
 		-variable bulk_L_eq_bulk_R -value NO  -text "NO   bulk |surf| vac" \
		   -padx 20 -bg $BGCOL2
radiobutton $ACOL2.bulk.a  -relief flat  -highlightthickness 0 -font $FONT(GEN) \
		-variable bulk_L_eq_bulk_R -value YES -text "YES  vac  |slab| vac" \
		  -padx 20 -bg $BGCOL2

pack $ACOL2.bulk.space1 $ACOL2.bulk.lab1 $ACOL2.bulk.a $ACOL2.bulk.b -anchor w -side top
pack $ACOL2.bulk  -fill both
$ACOL2.bulk.b select

#---------------------------------------------------------------------------------------
label $ACOL2.bot    -text " " -bg $BGCOL2 -pady 5
pack configure $ACOL2.bot -fill both -expand y

#--------------------------------------------------------------------------
  if {![info exists IQ_3DSURF]}     {set IQ_3DSURF    1}
  if {![info exists h_3DSURF]}      {set h_3DSURF    1}
  if {![info exists k_3DSURF]}      {set k_3DSURF    1}
  if {![info exists l_3DSURF]}      {set l_3DSURF    1}
  if {![info exists d_3DSURF]}      {set d_3DSURF    cryst}
  if {![info exists o_3DSURF]}      {set o_3DSURF    up}

  # IQ_3DSURF
  frame $ACOL2.frame10 -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $ACOL2.frame10.label5 -text {IQ (SURF)} -bg $BG4 -width 5
  scale $ACOL2.frame10.scale6 -from 1 -orient horizontal \
	  -sliderlength 40 -to $NQ -variable IQ_3DSURF -bg $BG4 \
	  -highlightthickness 0

  # h_3DSURF
  frame $ACOL2.frameH -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $ACOL2.frameH.label5 -text "h" -bg $BG4 -width 5
  scale $ACOL2.frameH.scale6 -from 0 -orient horizontal \
	  -sliderlength 40 -to 10 -variable h_3DSURF -bg $BG4 \
	  -highlightthickness 0 
  #if {$h_3DSURF!=""} {$ACOL2.frameH.scale6 set $h_3DSURF}

  # k_3DSURF
  frame $ACOL2.frameK -borderwidth 2 -height 30 -width 30 -bg $BG4 
  label $ACOL2.frameK.label5 -text "k" -bg $BG4 -width 5
  scale $ACOL2.frameK.scale6 -from 0 -orient horizontal \
    -sliderlength 40 -to 10 -variable k_3DSURF -bg $BG4 \
    -highlightthickness 0
  #if {$k_3DSURF!=""} {$ACOL2.frameK.scale6 set $k_3DSURF}

  # l_3DSURF
  frame $ACOL2.frameL -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $ACOL2.frameL.label5 -text "l" -bg $BG4 -width 5
  scale $ACOL2.frameL.scale6 -from 0 -orient horizontal \
	  -sliderlength 40 -to 10 -variable l_3DSURF -bg $BG4 \
	  -highlightthickness 0 
  #if {$l_3DSURF!=""} {$ACOL2.frameL.scale6 set $l_3DSURF}

  # def_3DSURF
  frame $ACOL2.frameD -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $ACOL2.frameD.label5 -text "definition " -bg $BG4
  radiobutton $ACOL2.frameD.rad1 -variable d_3DSURF -text "prim " \
	  -value prim  -bg $BG4 -highlightthickness 0 
  radiobutton $ACOL2.frameD.rad2 -variable d_3DSURF -text "cryst"  \
	  -value cryst -bg $BG4 -highlightthickness 0
  $ACOL2.frameD.rad2 select

  # o_3DSURF
  frame $ACOL2.frameO -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $ACOL2.frameO.label5 -text "orientation" -bg $BG4
  radiobutton $ACOL2.frameO.rad1 -variable o_3DSURF -text "down " \
	  -value down -bg $BG4 -highlightthickness 0 
  radiobutton $ACOL2.frameO.rad2 -variable o_3DSURF -text "up   "  \
	  -value up   -bg $BG4 -highlightthickness 0
  $ACOL2.frameO.rad2 select

  
  pack configure $ACOL2.frameH.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameH.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frameK.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameK.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frameL.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameL.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frame10.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frame10.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frameD.rad1 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameD.rad2 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameD.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frameO.rad1 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameO.rad2 -anchor w -expand 1 -fill x -side right
  pack configure $ACOL2.frameO.label5 -expand 1 -fill x -side right

  pack configure $ACOL2.frame10 -fill x -side top
  pack configure $ACOL2.frameH  -fill x -side top
  pack configure $ACOL2.frameK  -fill x -side top
  pack configure $ACOL2.frameL  -fill x -side top
  pack configure $ACOL2.frameD  -fill x -side top
  pack configure $ACOL2.frameO  -fill x -side top

#
#=======================================================================================
#=======================================================================================
#                                     COLUMN 3
#=======================================================================================
#=======================================================================================


set wlab2 30

frame $ACOL3.ab -bg $BGCOL3 
label $ACOL3.ab.space1 -text " " -width $wlab2 -bg $BGCOL3
label $ACOL3.ab.lab1_a -text "repeat unit cell along a" -bg $BGCOL3
scale $ACOL3.ab.scale_a -from 1 -to 15 -orient {horizontal}   \
      -sliderlength {20}  -variable N_rep_SU_a \
      -length 80 -bg $BGCOL3 -highlightthickness 0 
label $ACOL3.ab.lab1_b -text "repeat unit cell along b" -bg $BGCOL3 
scale $ACOL3.ab.scale_b -from 1 -to 15 -orient {horizontal}   \
      -sliderlength {20}  -variable N_rep_SU_b \
      -length 80 -bg $BGCOL3 -highlightthickness 0  

pack $ACOL3.ab.space1 $ACOL3.ab.lab1_a -anchor w -side top
pack $ACOL3.ab.scale_a -anchor c -side top
pack $ACOL3.ab.lab1_b  -anchor w -side top
pack $ACOL3.ab.scale_b -anchor c -side top
pack $ACOL3.ab  -fill both  

#
#---------------------------------------------------------------------------------------
#
   frame $ACOL3.uc -bg $BGCOL3 
   label $ACOL3.uc.space1 -text " " -width $wlab2 -bg $BGCOL3
   label $ACOL3.uc.lab1_L -text "repeat unit cell along c" -bg $BGCOL3
   label $ACOL3.uc.lab2_L -text "to generate unit cell of bulk(L)"     -bg $BGCOL3
   scale $ACOL3.uc.scale_L -from 1 -to 15 -orient {horizontal}   \
         -sliderlength {20}  -variable N_rep_SU_bulk_L \
         -length 80 -bg $BGCOL3 -highlightthickness 0 
   label $ACOL3.uc.lab1_S -text "repeat unit cell along c" -bg $BGCOL3
   label $ACOL3.uc.lab2_S -text "to generate surface/slab regime"     -bg $BGCOL3
   scale $ACOL3.uc.scale_S -from 1 -to 200 -orient {horizontal}   \
         -sliderlength {20}  -variable N_rep_SU_bulk_S \
         -length 80 -bg $BGCOL3 -highlightthickness 0 
   label $ACOL3.uc.lab1_R -text "repeat unit cell along c" -bg $BGCOL3 
   label $ACOL3.uc.lab2_R -text "to generate unit cell of vac(R)"     -bg $BGCOL3 
   scale $ACOL3.uc.scale_R -from 1 -to 15 -orient {horizontal}   \
         -sliderlength {20}  -variable N_rep_SU_bulk_R \
         -length 80 -bg $BGCOL3 -highlightthickness 0  
   
   pack $ACOL3.uc.space1 $ACOL3.uc.lab1_L $ACOL3.uc.lab2_L -anchor w -side top
   pack $ACOL3.uc.scale_L -anchor c -side top
   pack $ACOL3.uc.space1 $ACOL3.uc.lab1_S $ACOL3.uc.lab2_S -anchor w -side top
   pack $ACOL3.uc.scale_S -anchor c -side top
   pack $ACOL3.uc.lab1_R $ACOL3.uc.lab2_R -anchor w -side top
   pack $ACOL3.uc.scale_R -anchor c -side top
   pack $ACOL3.uc  -fill both
#
#---------------------------------------------------------------------------------------
#
   
set LIMIT 25.0

   frame $ACOL3.scale_c -bg $BGCOL3
#######################################   pack  $ACOL3.scale_c -side top -anchor n -fill x
   label $ACOL3.scale_c.space1 -text " " -width $wlab2 -bg $BGCOL3 
   label $ACOL3.scale_c.lab1 -text "scale lattice parameter c  (%)" -bg $BGCOL3 
   scale $ACOL3.scale_c.scale1 -from [expr -$LIMIT] -orient {horizontal} \
      -sliderlength {20} -to $LIMIT -resolution 0.1 -variable ML_SCALE_C \
      -length $lslid -bg $BGCOL3 -highlightthickness 0
   pack $ACOL3.scale_c.space1 $ACOL3.scale_c.lab1 $ACOL3.scale_c.scale1  -side top
   
   frame $ACOL3.foot -bg $BGCOL3
   pack $ACOL3.foot  -fill both -expand yes


label $ACOL3.bot    -text " " -bg $BGCOL3 
pack configure $ACOL3.bot -side bottom -fill both -expand yes
    
#
#=======================================================================================
#=======================================================================================
#                                   END OF  COLUMNS
#=======================================================================================
#=======================================================================================

#structure_specify_3D_surface_goon 
}
#                                                       structure_specify_3D_surface END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


########################################################################################
#                        continue    STRUCTURE 3D SURFACE
########################################################################################
#                                               
proc structure_specify_3D_surface_goon {  } {

global NQ RQX RQY RQZ RBASX RBASY RBASZ NOQ ITOQ MAG_DIR NLQ RWS STRUCTURE_SETUP_MODE
global IMQ CONC ZT TXTT TXT0 TABCHSYM

global NQ_bulk_L NQ_bulk_R SU_IQ SU_NAME run_geometry_task STRUCTURE_TYPE
global N_rep_SU_bulk_L N_rep_SU_bulk_S N_rep_SU_bulk_R 

global RBASX_3DBULK RBASY_3DBULK RBASZ_3DBULK NQ_3DBULK NOQ_3DBULK ITOQ_3DBULK
global   RQX_3DBULK   RQY_3DBULK   RQZ_3DBULK RWS_3DBULK NLQ_3DBULK IMQ_3DBULK
global NT_3DBULK ZT_3DBULK TXTT_3DBULK CONC_3DBULK 
global h_3DSURF k_3DSURF l_3DSURF

global RBASX_3DSURF RBASY_3DSURF RBASZ_3DSURF NQ_3DSURF NOQ_3DSURF ITOQ_3DSURF
global   RQX_3DSURF   RQY_3DSURF   RQZ_3DSURF RWS_3DSURF NLQ_3DSURF IQ_3DBULK_IQ_3DSURF
global NT_3DSURF ZT_3DSURF TXTT_3DSURF CONC_3DSURF IQ_3DSURF_IQ IMQ_3DSURF

global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R  DIMENSION
global PRC
global sysfile_3DBULK sysfile_3DSURF sysfile
global NSUBSYS N_SU

set PRC structure_specify_3D_surface_goon

set NSUBSYS 1
set N_SU    1
set DIMENSION "2D"
set STRUCTURE_SETUP_MODE "3D SURFACE"
#set  STRUCTURE_SETUP_MODE "2D LAYERS"


set hkl "[string trim $h_3DSURF][string trim $k_3DSURF][string trim $l_3DSURF]"

set sysfile_3DSURF "${sysfile}_${hkl}"

set  STRUCTURE_TYPE "2D (${hkl})-surface of [lindex $STRUCTURE_TYPE 0] "

#---------------------------------------------------------------------------------------
#               get new (hkl)-oriented unit cell suitable for 3D surface
#---------------------------------------------------------------------------------------

set run_geometry_task cr_3D_surface 

run_geometry $run_geometry_task


set NT_3DSURF $NT_3DBULK

for {set IT 1} {$IT <= $NT_3DBULK} {incr IT} {
   set   ZT_3DSURF($IT)   $ZT_3DBULK($IT)
   set TXTT_3DSURF($IT) $TXTT_3DBULK($IT)
   set CONC_3DSURF($IT) $CONC_3DBULK($IT)
}

for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
   set IQ_3DBULK $IQ_3DBULK_IQ_3DSURF($IQ_3DSURF)
   set RWS_3DSURF($IQ_3DSURF) $RWS_3DBULK($IQ_3DBULK)
   set NLQ_3DSURF($IQ_3DSURF) $NLQ_3DBULK($IQ_3DBULK)
   set IMQ_3DSURF($IQ_3DSURF) $IMQ_3DBULK($IQ_3DBULK)
}

#---------------------------------------------------------------------------------------

for {set I 1} {$I <= 2} {incr I} {
   set RBASX($I) $RBASX_3DSURF($I)
   set RBASY($I) $RBASY_3DSURF($I)
   set RBASZ($I) $RBASZ_3DSURF($I)

   set RBASX_L($I) $RBASX_3DSURF($I)
   set RBASY_L($I) $RBASY_3DSURF($I)
   set RBASZ_L($I) $RBASZ_3DSURF($I)

   set RBASX_R($I) $RBASX_3DSURF($I)
   set RBASY_R($I) $RBASY_3DSURF($I)
   set RBASZ_R($I) $RBASZ_3DSURF($I)
}

   set I 3
   set N_rep_SU [expr $N_rep_SU_bulk_L + $N_rep_SU_bulk_S + $N_rep_SU_bulk_R]
   set RBASX($I)   [expr $RBASX_3DSURF($I) * $N_rep_SU]
   set RBASY($I)   [expr $RBASY_3DSURF($I) * $N_rep_SU]
   set RBASZ($I)   [expr $RBASZ_3DSURF($I) * $N_rep_SU]

   set RBASX_L($I) [expr $RBASX_3DSURF($I) * $N_rep_SU_bulk_L]
   set RBASY_L($I) [expr $RBASY_3DSURF($I) * $N_rep_SU_bulk_L]
   set RBASZ_L($I) [expr $RBASZ_3DSURF($I) * $N_rep_SU_bulk_L]

   set RBASX_R($I) [expr $RBASX_3DSURF($I) * $N_rep_SU_bulk_R]
   set RBASY_R($I) [expr $RBASY_3DSURF($I) * $N_rep_SU_bulk_R]
   set RBASZ_R($I) [expr $RBASZ_3DSURF($I) * $N_rep_SU_bulk_R]

set MAG_DIR(1) 0
set MAG_DIR(2) 0
set MAG_DIR(3) 0

set NQ_bulk_L [expr $NQ * $N_rep_SU_bulk_L]
set NQ_bulk_R [expr $NQ * $N_rep_SU_bulk_R]

set ROFFX 0.0
set ROFFY 0.0
set ROFFZ 0.0

set IQ 0

for {set I 1} {$I <= $N_rep_SU_bulk_L} {incr I} {
   for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
      incr IQ   
      set IQ_3DSURF_IQ($IQ) $IQ_3DSURF
      set RQX($IQ) [expr $RQX_3DSURF($IQ_3DSURF) + $ROFFX + ($I-1)*$RBASX_3DSURF(3)]
      set RQY($IQ) [expr $RQY_3DSURF($IQ_3DSURF) + $ROFFY + ($I-1)*$RBASY_3DSURF(3)] 
      set RQZ($IQ) [expr $RQZ_3DSURF($IQ_3DSURF) + $ROFFZ + ($I-1)*$RBASZ_3DSURF(3)] 
      set IMQ($IQ)       $IMQ_3DSURF($IQ_3DSURF) 
      set NOQ($IQ)       $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

set ROFFX [expr $ROFFX + $N_rep_SU_bulk_L * $RBASX_3DSURF(3)]
set ROFFY [expr $ROFFY + $N_rep_SU_bulk_L * $RBASY_3DSURF(3)]
set ROFFZ [expr $ROFFZ + $N_rep_SU_bulk_L * $RBASZ_3DSURF(3)]

for {set I 1} {$I <= $N_rep_SU_bulk_S} {incr I} {
   for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
      incr IQ   
      set IQ_3DSURF_IQ($IQ) $IQ_3DSURF
      set RQX($IQ) [expr $RQX_3DSURF($IQ_3DSURF) + $ROFFX + ($I-1)*$RBASX_3DSURF(3)]
      set RQY($IQ) [expr $RQY_3DSURF($IQ_3DSURF) + $ROFFY + ($I-1)*$RBASY_3DSURF(3)] 
      set RQZ($IQ) [expr $RQZ_3DSURF($IQ_3DSURF) + $ROFFZ + ($I-1)*$RBASZ_3DSURF(3)] 
      set IMQ($IQ)       $IMQ_3DSURF($IQ_3DSURF) 
      set NOQ($IQ)       $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

set ROFFX [expr $ROFFX + $N_rep_SU_bulk_S * $RBASX_3DSURF(3)]
set ROFFY [expr $ROFFY + $N_rep_SU_bulk_S * $RBASY_3DSURF(3)]
set ROFFZ [expr $ROFFZ + $N_rep_SU_bulk_S * $RBASZ_3DSURF(3)]

for {set I 1} {$I <= $N_rep_SU_bulk_R} {incr I} {
   for {set IQ_3DSURF 1} {$IQ_3DSURF <= $NQ_3DSURF} {incr IQ_3DSURF} {
      incr IQ   
      set IQ_3DSURF_IQ($IQ) $IQ_3DSURF
      set RQX($IQ) [expr $RQX_3DSURF($IQ_3DSURF) + $ROFFX + ($I-1)*$RBASX_3DSURF(3)]
      set RQY($IQ) [expr $RQY_3DSURF($IQ_3DSURF) + $ROFFY + ($I-1)*$RBASY_3DSURF(3)] 
      set RQZ($IQ) [expr $RQZ_3DSURF($IQ_3DSURF) + $ROFFZ + ($I-1)*$RBASZ_3DSURF(3)] 
      set NOQ($IQ)       $NOQ_3DSURF($IQ_3DSURF) 
      for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
         set ITOQ($IO,$IQ) $ITOQ_3DSURF($IO,$IQ_3DSURF) 
      }
   }
}

set NQ $IQ

for {set IQ 1} {$IQ <= $NQ } {incr IQ} {
   set IQ_3DSURF $IQ_3DSURF_IQ($IQ)
   debug $PRC "  IQ  $IQ    IQ_3DSURF  $IQ_3DSURF"
   set IMQ($IQ)   $IMQ_3DSURF($IQ_3DSURF) 
   set RWS($IQ)   $RWS_3DSURF($IQ_3DSURF)
   set NLQ($IQ)   $NLQ_3DSURF($IQ_3DSURF)
   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ($IO,$IQ)
      set CONC($IT)  $CONC_3DSURF($IT)
      set   ZT($IT)    $ZT_3DSURF($IT)
      set TXTT($IT)  $TXTT_3DSURF($IT)
      set TXTT0($IT) $TABCHSYM($ZT($IT)) 
      set TXTT($IT)  $TXTT0($IT)
   }
}

set SU_NAME(1) "bulk"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set SU_IQ($IQ) 1
}

write_actual_lattice_info $PRC

structure_per_pedes_read_coord use_primitive_vectors

}
#                                                  structure_specify_3D_surface_goon END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



