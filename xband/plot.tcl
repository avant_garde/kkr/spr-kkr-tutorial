# plot data
#
#
# Copyright (C) 1998 H. Ebert

proc plot_data {chgdir_allowed_in} {

########################################################################################
 proc plot_sel_d {d} {
  global chgdir_allowed
  global Wplot

  if {$chgdir_allowed==0} {
       give_warning "." "WARNING \n\n change of directory not allowed in plot \n\n \
                                      go back to calling window and change  \n "
       return
  }

  update
  plot_focus $Wplot
  $Wplot.c.d.f.f.li delete 0 end
  update
  dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default

  global Wplot working_directory datafile liste
  global pinsuffixlist  pinfile  datasuffixlist
  global xmgrsuffixlist xmgrfile sysdirlist

  set cdir [expand_dollar_home $d]

  if {[file isdirectory $cdir]==0} {
     writescr0 .d.tt "\n the directory  $cdir  does not exist"
     give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
     return
  }

  if [winfo exists $Wplot] {set w $Wplot.d.tt} else {set w .d.tt}

  if {[catch {cd $cdir} m]!=0} {
      set working_directory [pwd]
      writescr0 $w "error while changing directory!\nerror message:\n$m\n"
      return 0
  } else {
    writescr0 $w "new directory chosen: [pwd]\n\n"
    plot_refresh_top_panel
    set working_directory [pwd]
    set datafile ""
    set pinfile   "" 
    set xmgrfile  "" 
    if {[winfo exists $Wplot]} {
      plot_anzeige $Wplot

      $Wplot.c.d.f.f.li delete 0 end
      $Wplot.c.d.f.f.li insert end "\$HOME" ".."
      foreach x $sysdirlist { $Wplot.c.d.f.f.li insert end "$x"}
      foreach k [lsort [glob -nocomplain -- *]] {
	  if [file isdirectory $k] {
	      $Wplot.c.d.f.f.li insert end $k
	  }   
      }
      lfuellen $Wplot.c.m.f.f.li $datasuffixlist
      lfuellen $Wplot.c.e.f.f.li $pinsuffixlist
      lfuellen $Wplot.c.s.f.f.li $xmgrsuffixlist
    }
    return 1
  }
}


########################################################################################
 proc plot_dir_update {} {
    global Wplot liste working_directory sysdirlist sysdirlistmaxindex

    set diraux [collapse_dollar_home $working_directory]

    if {[llength $sysdirlist]>0} {
       set sysdirlist [update_dirlist $sysdirlist $sysdirlistmaxindex $diraux]
    } else {
       lappend sysdirlist $diraux
    }
    $Wplot.c.d.f.f.li delete 0 end
    $Wplot.c.d.f.f.li insert end "\$HOME" ".."
    foreach x $sysdirlist { $Wplot.c.d.f.f.li insert end "$x"}

 }

########################################################################################
 proc plot_refresh_top_panel {} {

  global Wplot filter datafile pinfile xmgrfile

  set filter   "*"
  set datafile ""
  set pinfile  ""
  set xmgrfile ""
  $Wplot.b.c1.m.but configure -state disabled -bg grey
  $Wplot.b.c1.e.but configure -state disabled -bg grey
  $Wplot.b.c1.s.but configure -state disabled -bg grey
 
  plot_anzeige $Wplot

 }


########################################################################################
 proc plot_sel_m {f} {

  global Wplot datafile sorttype PLOTPROG
  plot_focus $Wplot

  if {[file exists $f]} {
    set datafile $f
    writescr0 $Wplot.d.tt "new datafile selected $datafile\n\n"; plot_anzeige $Wplot
    $Wplot.b.c1.m.but  configure -state normal -bg LightBlue1
    set    pin [open "zzzzzz_pin" w] 
    puts  $pin "DATASET    $f"
    close $pin
    patience $Wplot.a 1
    writescr  $Wplot.d.tt "asking for patience\n"
    eval set res [catch "exec $PLOTPROG < zzzzzz_pin " message]
    patience $Wplot.a 0
    writescr  $Wplot.d.tt "$message \n"
    eval set res [catch "exec rm zzzzzz_pin" message]
    plot_sel_order $sorttype
    plot_dir_update
  }
 }

########################################################################################
 proc plot_sel_m2 {f} {

  global Wplot sorttype PLOTPROG
  global pinfile datafile VAR

#
#---------------------------------------------- initialize VAR for all variablenames
#
  set VARLST [list EXP_FILE_NAME EXP_ES EXP_BLS EXP_SCL EXP_NC EXP_NPOL \
                   EMIN EMAX DE DY SCALE MERGE MLDIC WLOR1 WLOR2 WGAUSS \
                   TABLE JDOS NOHEADER USERYD CUTOFF MTV ]

  foreach V $VARLST { set VAR($V) "" }
 
  set VAR(EXP_ES)    0
  set VAR(EXP_BLS)   0
  set VAR(EXP_SCL)   1
  set VAR(EXP_NC)    1
  set VAR(EXP_NPOL)  1
  set VAR(CUTOFF)   300

  plot_focus $Wplot

  if {[file exists $f]} {
    set datafile $f
    set pinfile "${f}.pin"
    writescr0 $Wplot.d.tt "new datafile selected $datafile\n\n"
    writescr  $Wplot.d.tt "new pinfile  selected $pinfile\n\n" ; plot_anzeige $Wplot
    $Wplot.b.c1.m.but  configure -state normal -bg LightBlue1
    $Wplot.b.c1.e.but  configure -state normal -bg LightBlue1
#
#--------------------------------------------------- find KEYWORD in datafile
#
    set KEYWORD NONE
    set fil [open $f r]

    while {[gets $fil line] > -1 && $KEYWORD=="NONE"} {
	set HEAD [lindex $line 0]
 	if { $HEAD == "KEYWORD" } {
           set KEYWORD [lindex $line 1]
	} 
    }
    close $fil

# SP-AES RXAS CLXPS DOS DOS-KAPPA POLAR DISPERSION BSF COMPTON  
# NRAES NRAPS NRAPSDEC SPR-VB-XPS XAS XES XMO XRS 

#
#--------------------------------------------------- core level spectra
#
    if { $KEYWORD=="RXAS"  || $KEYWORD=="CLXPS" || $KEYWORD=="NRAES"    || \
         $KEYWORD=="NRAPS" || $KEYWORD=="NRAPS" || $KEYWORD=="NRAPSDEC" || \
         $KEYWORD=="XAS"   || $KEYWORD=="XES"   || $KEYWORD=="XMO"      || \
         $KEYWORD=="XRS"   || $KEYWORD=="SP-AES" } {

      if { [string match "*_K_*"  $pinfile]==1 || \
           [string match "*_L1_*" $pinfile]==1 || \
           [string match "*_M1_*" $pinfile]==1 || \
           [string match "*_N1_*" $pinfile]==1 || \
           [string match "*_O1_*" $pinfile]==1 } {
           set CL_SPEC 1
      } else {
           set CL_SPEC 2
      }
       set spectrum   1
       set experiment 1

    } else {
       set CL_SPEC 0
       set spectrum   0
       set experiment 0
    }

    if { $KEYWORD=="SPR-VB-XPS" || $KEYWORD=="SPR-VB-PES" || $KEYWORD=="COMPTON" || $KEYWORD=="SPR-VB-ARP"  || $KEYWORD=="VB-PES" || $KEYWORD=="VB-AR-PES"} {
       set spectrum   1
    }
   
    set WR_PLOT_SECTION 1
    if { $KEYWORD=="BSF" } {set WR_PLOT_SECTION 0}

set w .wpcp

toplevel_init $w "Create pin-file" 0 0

set wl 22
set we  8
set fe "-Adobe-Helvetica-Bold-R-Normal--*-120-*-*-*-*-*-*"

frame $w.a 
pack configure $w.a -in $w -anchor w -side top
label $w.a.1 -width $wl -text "   pin-file:"  -anchor w -padx 2
label $w.a.2 -width 50                        -anchor w -padx 2
$w.a.2 configure -text "$pinfile"
pack configure $w.a.1 $w.a.2 -in $w.a -anchor w -side left -pady 8


frame $w.cols 
pack configure $w.cols -in $w -anchor w -side top -fill x -expand y

set EBG LightSalmon  
set PBG LightSkyBlue
frame $w.cols.col2 -bg $EBG -relief raised -borderwidth 2 
frame $w.cols.col1 -bg $PBG -relief raised -borderwidth 2
pack configure $w.cols.col1 $w.cols.col2 -in $w.cols -anchor n -side top -fill x -expand y

set wc1 $w.cols.col1 
set wc2 $w.cols.col2  

#-------------------------------------------------- WR_PLOT_SECTION START
if {$WR_PLOT_SECTION==1} {

    CreateENTRY $wc1 $PBG $wl "   EMIN:"  $we VAR EMIN   $fe 2 "eV"
 
    CreateENTRY $wc1 $PBG $wl "   EMAX:"  $we VAR EMAX   $fe 2 "eV"
 
    ##  CreateENTRY $wc1 $PBG $wl "   DE:"    $we VAR DE     $fe 2 "eV"
 
    ## if { $KEYWORD=="DOS" } {
    ## CreateENTRY $wc1 $PBG $wl "   D(DOS)" $we VAR DY     $fe 2 "1/eV"
    ## }

    CreateENTRY $wc1 $PBG $wl "   SCALE:" $we VAR SCALE  $fe 2 "  "
}
#-------------------------------------------------- WR_PLOT_SECTION END


#--------------------------------------------------------------------- 
set VAR(NVAL) "-1"

if {$CL_SPEC!=0} {

    if {$CL_SPEC == 2} {
	CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR MERGE "merge spectra"
    }

    CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR MLDIC "magnetic linear dichroism"
    CreateSCALE        $wc1 $PBG $wl "   NVAL  "  160 -1  11  0 2 1  VAR NVAL  $fe
    
}


#--------------------------------------------------------------------- 
if {$CL_SPEC > 0 && $KEYWORD!="NRAES" && $KEYWORD!="SP-AES" } {
   if {$CL_SPEC == 2} { 
   CreateENTRY $wc1 $PBG $wl "   W(Lorentz):  (j=l-1/2)"  $we VAR WLOR1   $fe 2 "eV"
   }
   CreateENTRY $wc1 $PBG $wl "   W(Lorentz):  (j=l+1/2)"  $we VAR WLOR2   $fe 2 "eV"
}

#--------------------------------------------------------------------- 
if { $KEYWORD=="NRAES" || $KEYWORD=="SP-AES" } {
   CreateENTRY $wc1 $PBG $wl "   W(Lorentz)            A:"  $we VAR WLOR1   $fe 2 "eV"
   CreateENTRY $wc1 $PBG $wl "   W=A+B*(EF-E)      B:"  $we VAR WLOR2   $fe 2 "eV"
}

#--------------------------------------------------------------------- 
if { $KEYWORD=="SPR-VB-XPS" ||$KEYWORD=="SPR-VB-PES" || $KEYWORD=="SPR-VB-ARP" || $KEYWORD=="VB-PES" ||  $KEYWORD=="VB-AR-PES"} {
   CreateENTRY $wc1 $PBG $wl "   W(Lorentz)            A:"  $we VAR WLOR1   $fe 2 "eV"
   CreateENTRY $wc1 $PBG $wl "   W=A+B*(EF-E)      B:"  $we VAR WLOR2   $fe 2 "eV"
}

#--------------------------------------------------------------------- 
if {$spectrum == 1} {
   CreateENTRY $wc1 $PBG $wl "   W(Gauss):"               $we VAR WGAUSS  $fe 2 "eV"
}

#--------------------------------------------------------------------- 
   CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR NOHEADER "suppress xmgr-header"

#--------------------------------------------------------------------- 
if {$spectrum == 1 && $CL_SPEC > 0} {
   CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR TABLE "tabulate spectra in file"
}

#--------------------------------------------------------------------- 
if { $KEYWORD=="DOS" } {
   CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR TABLE  "tabulate DOS in file"
   CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR JDOS   "tabulate JDOS in file"
   CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR USERYD "use Rydberg units"
}

#--------------------------------------------------------------------- 
if { $KEYWORD=="BSF" || $KEYWORD=="SPR-VB-ARP" || $KEYWORD=="VB-AR-PES"} {
    CreateCHECKBUTTON  $wc1 $PBG $wl " " VAR MTV      "output in mtv-format (default)"
    CreateENTRY $wc1 $PBG $wl " cutoff for z-value        "  $we VAR CUTOFF  $fe 2 "  "
}
#--------------------------------------------------------------------- 
if {$experiment==1} {
   label $wc2.exp -text "   Give information on experimental data set (if available)"  \
        -anchor w  -padx 2  -bg $EBG
   pack configure $wc2.exp -in $wc2 -anchor w -side top

   CreateENTRY $wc2 $EBG $wl "   file name:"                50 VAR EXP_FILE_NAME $fe 0 ""
   CreateENTRY $wc2 $EBG $wl "   energy shift:"            $we VAR EXP_ES        $fe 0 ""
   CreateENTRY $wc2 $EBG $wl "   base line shift:"         $we VAR EXP_BLS       $fe 0 ""
   CreateENTRY $wc2 $EBG $wl "   scaling factor:"          $we VAR EXP_SCL       $fe 0 ""
   CreateENTRY $wc2 $EBG $wl "   number of curves:"        $we VAR EXP_NC        $fe 0 ""
   CreateENTRY $wc2 $EBG $wl "   number of polarisations:" $we VAR EXP_NPOL      $fe 0 ""
}


#--------------------------------------------------------------------- 
button $w.run  -text " OK ?" -height 2 -width 10 -bg green1 \
	-command " plot_run_new_pinfile $experiment $spectrum $CL_SPEC $KEYWORD $WR_PLOT_SECTION $w " 
	
button $w.close -text Close  -height 2  -width 10 -bg tomato1 -command "destroy $w"

pack $w.run $w.close -side top -expand y -fill x -pady 2


  }
}


#########################################################################
proc plot_run_new_pinfile {experiment spectrum CL_SPEC KEYWORD WR_PLOT_SECTION w} {

global pinfile datafile VAR Wplot sorttype PLOTPROG

set    pin [open $pinfile w] 
puts  $pin "DATASET    $datafile"
puts  $pin " "
puts  $pin "# remove leading # of sections and parameters to activate and insert values "
puts  $pin " "

set cc1(0) "#"
set cc1(1) ""
set cc2(0) ""
set cc2(1) " "
set  c1(0) "#"
set  c1(1) " "
set  c1(2) " "

##if {$VAR(NVAL) ==0} {set VAR(NVAL) ""}

set kplot    [laux "$VAR(EMIN)$VAR(EMIN)$VAR(SCALE)$VAR(MERGE)$VAR(MLDIC)"] 
set kbroaden [laux "$VAR(WLOR1)$VAR(WLOR2)$VAR(WGAUSS)$VAR(NVAL)"]
set koutput  [laux "$VAR(NOHEADER)$VAR(TABLE)$VAR(JDOS)$VAR(CUTOFF)$VAR(MTV)"]

#-------------------------------------------------- WR_PLOT_SECTION START
if {$WR_PLOT_SECTION==1} {
puts  $pin "$cc1($kplot)PLOT$cc2($kplot)                            #  plot parameters  "
puts  $pin  "$c1([laux $VAR(EMIN)])     EMIN  = [faux $VAR(EMIN)  f 10]         #  E plot range "
puts  $pin  "$c1([laux $VAR(EMAX)])     EMAX  = [faux $VAR(EMAX)  f 10]         #  E plot range "
##puts  $pin    "$c1([laux $VAR(DE)])     DE    = [faux $VAR(DE)    f 10]         #  E increment"
##if { $KEYWORD=="DOS" } {
##puts  $pin    "$c1([laux $VAR(DY)])     DY    = [faux $VAR(DY)    f 10]         #  DOS increment"
##}
puts  $pin "$c1([laux $VAR(SCALE)])     SCALE = [faux $VAR(SCALE) e 10]         #  scaling factor for functions"
if {$CL_SPEC == 2} {
puts  $pin "$c1([laux $VAR(MERGE)])     MERGE                                   #  switch for merging XAS-spectra"
}
if {$CL_SPEC != 0} {
puts  $pin "$c1([laux $VAR(MLDIC)])     MLD                                     #  switch to deal with MLD"
}
puts  $pin " "
}
#-------------------------------------------------- WR_PLOT_SECTION END


if {$spectrum != 0} {
set W1 $VAR(WLOR1)
set W2 $VAR(WLOR2)
puts  $pin "$cc1($kbroaden)BROADEN$cc2($kbroaden)                         #  broadening parameters"
if {$CL_SPEC > 0 && $KEYWORD!="NRAES" && $KEYWORD!="SP-AES" } {
if {$VAR(NVAL) > -1} {
   puts  $pin "$c1([laux $VAR(NVAL)])     NVAL    = [faux $VAR(NVAL) i 10]         #  number of valence electrons (0: suppress broadening)"
}
puts  $pin "$c1([expr [laux $W1] + [laux $W2]])     WL    = { [faux $W1 f 6] [faux $W2 f 6] }  #  Lorentzian broadening "
}
if { $KEYWORD=="NRAES" || $KEYWORD=="SP-AES" } {
puts  $pin "$c1([expr [laux $W1] + [laux $W2]])     WLAES = { [faux $W1 f 6] [faux $W2 f 6] }  #  Lorentzian broadening "
}
if { $KEYWORD=="SPR-VB-XPS"||$KEYWORD=="SPR-VB-PES" || $KEYWORD=="SPR-VB-ARP" || $KEYWORD=="VB-PES" || $KEYWORD=="VB-AR-PES"} {
puts  $pin "$c1([expr [laux $W1] + [laux $W2]])     WLVBXPS={ [faux $W1 f 6] [faux $W2 f 6] }  #  Lorentzian broadening "
}
puts  $pin "$c1([laux $VAR(WGAUSS)])     WG    = [faux $VAR(WGAUSS) f 10]         #  Gaussian broadening "
puts  $pin " "
}


puts  $pin "$cc1($koutput)OUTPUT$cc2($koutput)                          #  output control"
puts  $pin "$c1([laux $VAR(NOHEADER)])     NOHEADER                   #  suppress header (xmgr-titles)"
if { $KEYWORD!="BSF" } {
puts  $pin "$c1([laux $VAR(TABLE)])     TABLE                      #  tabulate curves in file for export"
}
if { $KEYWORD=="DOS" } {
puts  $pin "$c1([laux $VAR(JDOS)])     JDOS                         #  tabulate JDOS in file for export"
puts  $pin "$c1([laux $VAR(USERYD)])     USERYD                     #  use Rydberg units"
}
if { $KEYWORD=="BSF" || $KEYWORD=="SPR-VB-ARP" || $KEYWORD=="VB-AR-PES"} {
puts  $pin "$c1([laux $VAR(MTV)])     MTV                        #  output in MTV format (default)"
puts  $pin "$c1([laux $VAR(CUTOFF)])     CUTOFF =  [faux $VAR(CUTOFF) f 10]       #  cutoff for z-value "
}
puts  $pin " "


if {$spectrum != 0} {
set key [laux $VAR(EXP_FILE_NAME)]
puts  $pin "$cc1($key)EXPERIMENT$cc2($key)                      #  info on exp. data - if available"
puts  $pin "$c1($key)     FILE  = $VAR(EXP_FILE_NAME)                   #  file name for expt. spectra"
puts  $pin "$c1($key)     ES    = [faux $VAR(EXP_ES)   f 10]         #  energy scale shift "
puts  $pin "$c1($key)     BLS   = [faux $VAR(EXP_BLS)  e 10]         #  base line shift "
puts  $pin "$c1($key)     SCL   = [faux $VAR(EXP_SCL)  e 10]         #  scaling factor "
puts  $pin "$c1($key)     NC    = [faux $VAR(EXP_NC)   i 10]         #  number of curves "
puts  $pin "$c1($key)     NPOL  = [faux $VAR(EXP_NPOL) i 10]         #  polarisation states     "
}

close $pin

    patience  $Wplot.a 1
    writescr  $Wplot.d.tt "asking for patience\n"
    eval set res [catch "exec $PLOTPROG < $pinfile " message]
    patience  $Wplot.a 0
    writescr  $Wplot.d.tt "$message \n"

    plot_sel_order $sorttype
    plot_dir_update
                      
    destroy $w                                              
}




########################################################################################
 proc plot_sel_e {f} {

     global Wplot pinfile sorttype PLOTPROG

     plot_focus $Wplot

     if {[file exists $f]} {
	 set pinfile $f
	 writescr0 $Wplot.d.tt "new pin-file selected: $pinfile\n\n"; plot_anzeige $Wplot
	 $Wplot.b.c1.e.but  configure -state normal -bg LightBlue1
	 patience $Wplot.a 1
	 writescr  $Wplot.d.tt "asking for patience\n"
	 eval set res [catch "exec $PLOTPROG < $f " message]
	 patience $Wplot.a 0
	 writescr  $Wplot.d.tt "$message \n"
	 plot_dir_update
     }
 }
 
########################################################################################
 proc plot_sel_e2 {f} {

     global Wplot pinfile
     global editor edback edxterm edoptions

     plot_focus $Wplot
     if {[file exists $f]} {
	 set pinfile $f
	 writescr0 $Wplot.d.tt "new pin-file selected: $pinfile\n\n"; plot_anzeige $Wplot
	 $Wplot.b.c1.e.but  configure -state normal -bg LightBlue1
	 set ed_file $f
	 eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
	 plot_dir_update
     }
 }

########################################################################################
proc plot_sel_s {f} {

    global Wplot xmgrfile XMGR XMGRACE

    set PRC "plot_sel_s"
    plot_focus $Wplot

    if {[file exists $f]} {
	# set ext [file extension $f] could replace following 3 lines
	set i1 [string last . $f]
	set i2 [expr [string length $f] - 1]
	set ext [string range $f $i1 $i2]  
	debug $PRC "XMGR $f $i1 $i2 $ext" 

	# remember suffixes are listet in locals/xmgrsuffix.vst 
	switch $ext {
	    ".xmgr" { set PROG $XMGR }
	    ".agr"  { set PROG $XMGRACE }
	    ".mtv"  { set PROG "plotmtv" }
	    ".ps"   { set PROG "gv  " } 
	    ".gp"  { set PROG "gnuplot" }
	    default { error "should never happen in $PRC"}  
	}
	
	set xmgrfile $f
	writescr0 $Wplot.d.tt "new xmgr-file selected: $xmgrfile\n\n"; plot_anzeige $Wplot
	$Wplot.b.c1.s.but  configure -state normal -bg LightBlue1

	eval set res [catch "exec $PROG $f &" message]
	writescr  $Wplot.d.tt "$message \n"
	plot_dir_update
    }
}

########################################################################################
 proc plot_sel_s2 {f} {

  global Wplot xmgrfile
  global editor edback edxterm edoptions

  plot_focus $Wplot

  if {[file exists $f]} {
    set xmgrfile $f
    writescr0 $Wplot.d.tt "new xmgr-file selected: $xmgrfile\n\n"; plot_anzeige $Wplot
    $Wplot.b.c1.s.but  configure -state normal -bg LightBlue1
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    plot_dir_update
  }
 }

########################################################################################
 proc plot_sel_order {s} {
  global Wplot datasuffixlist pinsuffixlist xmgrsuffixlist sorttype

  if {$s=="alphabetical"} {set sorttype alphabetical} else {set sorttype access}
  lfuellen $Wplot.c.m.f.f.li $datasuffixlist
  lfuellen $Wplot.c.e.f.f.li $pinsuffixlist 
  lfuellen $Wplot.c.s.f.f.li $xmgrsuffixlist
 }

########################################################################################
 proc plot_dummy {s} { }


########################################################################################
 proc plot_focus {w} {
  global Wplot plot_foc
  if     {$plot_foc==0} {set r 0} \
  elseif {$plot_foc==1} {set r [plot_edn]} \
  elseif {$plot_foc==2} {set r [plot_emfn]} \
  elseif {$plot_foc==3} {set r [plot_eefn]} \
  elseif {$plot_foc==4} {set r [plot_esfn]}
  if {$w!=""} {focus $w} else {focus $Wplot; return $r}
 }

########################################################################################
 proc plot_edn {} {# enter directory name
  global Wplot plot_foc plot_d chgdir_allowed

  set plot_foc 0

  if {$chgdir_allowed==0} {
       give_warning "." "WARNING \n\n change of directory not allowed in plot \n\n \
                                      go back to calling window and change  \n "
       return
  }

  set d [string trim [$Wplot.c.d.i get]]
  $Wplot.c.d.i delete 0 end
  writescr0 $Wplot.d.tt ""

  if {$d==""} {return 0}
  set plot_d [expand_dollar_home $d]

  if {![file isdirectory $plot_d] && ![file exists $plot_d]} {
      mkdcdfill
  } else {
      dirselected_comp $plot_d
      plot_anzeige $Wplot
  }
  
  return 1
 }

########################################################################################
 proc plot_emfn {} {#                                             enter datafile name
  global Wplot datasuffix datafile pinfile xmgrfile liste plot_foc

  set plot_foc 0
  writescr0 $Wplot.d.tt ""
  if {$f=="$datasuffix"} {
      set f ""
      writescr $Wplot.d.tt "filename cannot consist of file suffix only\n"
  }
  regsub "$datasuffix$" $f "" h
  if {"$h$datasuffix"=="$f"} {set f "$h"}
  testfilename $f $Wplot.d.tt $datasuffix
  if {$f==""} {return 0}

  set datafile $f
  writescr $Wplot.d.tt "input of new datafile name: $datafile\n\n"
  plot_anzeige $Wplot
  return 1
 }


########################################################################################
 proc plot_eefn {} {#                                             enter pin-file name
  global Wplot pinsuffix pinfile plot_foc

  set plot_foc 0
  writescr0 $Wplot.d.tt ""
  if {$f=="$pinsuffix"} {set f ""; writescr $Wplot.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${pinsuffix}$" $f "" h; if {"$h$pinsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wplot.d.tt $pinsuffix;  if {$f==""} {return 0}

  set pinfile $f
  writescr $Wplot.d.tt "input of new pin-file name: $pinfile\n\n" 
  plot_anzeige $Wplot
  return 1
 }


########################################################################################
 proc plot_esfn {} {#                                          enter xmgr file name
  global Wplot xmgrsuffix xmgrfile plot_foc

  set plot_foc 0
  writescr0 $Wplot.d.tt ""
  if {$f=="$xmgrsuffix"} {set f ""; writescr $Wplot.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${xmgrsuffix}$" $f "" h; if {"$h$xmgrsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wplot.d.tt $xmgrsuffix;  if {$f==""} {return 0}

  set xmgrfile $f
  writescr $Wplot.d.tt "input of new xmgr-file name $xmgrfile\n\n" 
  plot_anzeige $Wplot
  return 1
 }

########################################################################################
proc editfile {f} {#                                              enter xmgr file name
  global datafile pinfile xmgrfile
  global edxterm editor edoptions edback

  if     {$f=="m"} {set ff "$datafile"}  \
  elseif {$f=="e"} {set ff "$pinfile"} \
  elseif {$f=="s"} {set ff "$xmgrfile"} \
  else             {set ff ""}

  set ed_file $ff
  eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
  set pid "$message"
}

########################################################################################
 proc plot_anzeige {w} {#                                              update display
  global working_directory datafile pinfile xmgrfile
  $w.b.c1.d.t        configure -text "$working_directory"
  if {$datafile==""}   {set m "NO DATA-FILE SELECTED"} else {set m "$datafile"}
  if {$pinfile==""}    {set e "NO PIN-FILE SELECTED"}  else {set e "$pinfile"}
  if {$xmgrfile==""}   {set s "NO XMGR-FILE SELECTED"} else {set s "$xmgrfile"}
  $w.b.c1.m.t        configure -text "$m"
  $w.b.c1.e.t        configure -text "$e"
  $w.b.c1.s.t        configure -text "$s"
 }

########################################################################################
 proc lfuellen {w suffixlist} {#                fill filelist with suffix s into widget w
  global sub sorttype filter
  $w delete 0 end;  set subalt $sub
  if {$sorttype=="alphabetical"} {set liste [exec ls] } else {set liste [exec ls -t]}
  foreach x $suffixlist { 
     foreach i $liste {
       if {[file isfile $i]} {
         if {[string match "*$x" $i]} { 
            if {[string match "*$filter*" $i]} {$w insert end $i}
         }
       }
     } 
  }
  lock
  set sub $subalt
 }


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wplot Hplot hlp_dir working_directory plot_foc datafile pinfile xmgrfile xmgrsuffix 
global mfold sysdirlist
global editor edback edxterm edoptions
global chgdir_allowed

set Hplot plot_h.hlp

set datafile ""
set pinfile  ""
set xmgrfile ""

toplevel_init $Wplot "PLOT DATA" 0 0;   set tyh 10;  set working_directory [pwd] ;  set mfold "$datafile"

# top buttons

insert_topbuttons $Wplot $Hplot
bind $Wplot.a.e <Button-1> {
  if [plot_focus ""] {
    writescr $Wplot.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wplot; unlock_list
    if {($mfold!=$datafile)&&([file exists "${datafile}.vst"])} {.b.c1.1.vl invoke}
    Bend
  }
}

pack  $Wplot.a  -fill x  -expand y
label $Wplot.a.patience  -text " " -fg black -relief flat
pack  $Wplot.a.patience -side right -expand yes -fill both

########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wplot.b   
pack configure $Wplot.b -in $Wplot -anchor w -pady 8

frame $Wplot.b.c1 
frame $Wplot.b.c2
pack configure $Wplot.b.c1 -in $Wplot.b -anchor w -side left  -pady 8
pack configure $Wplot.b.c2 -in $Wplot.b -anchor e -side right -pady 8

frame $Wplot.b.c2.sor
frame $Wplot.b.c2.fil
pack configure $Wplot.b.c2.sor $Wplot.b.c2.fil -in $Wplot.b.c2 -anchor w

label $Wplot.b.c2.fil.l -width 4 -text "Filter" -anchor w  -padx 80
entry $Wplot.b.c2.fil.e -background {lightblue} -textvariable {filter} -width {29}
#          -font "-Adobe-Helvetica-Medium-R-Normal--*-160-*-*-*-*-*-*" 
bind $Wplot.b.c2.fil.e <Key-Return> {plot_sel_order $sorttype}
 
pack configure $Wplot.b.c2.fil.l $Wplot.b.c2.fil.e -in $Wplot.b.c2.fil -anchor w -side left


label $Wplot.b.c2.sor.l -width 4 -text "sort mode" -anchor w  -padx 80
frame $Wplot.b.c2.sor.b
pack configure $Wplot.b.c2.sor.l $Wplot.b.c2.sor.b -in $Wplot.b.c2.sor -anchor w -side left
CreateLSBoxSBx $Wplot.b.c2.sor.b " " top 11 3 0 $Hplot $Wplot.d.tt \
    "" "\{alphabetical\} \{last access\}" plot_sel_order ""


#=======================================================================================
frame $Wplot.b.c1.d; frame $Wplot.b.c1.m; frame $Wplot.b.c1.e; frame $Wplot.b.c1.s
pack configure $Wplot.b.c1.d $Wplot.b.c1.m $Wplot.b.c1.e $Wplot.b.c1.s -in $Wplot.b.c1 -anchor w

label $Wplot.b.c1.d.l -width 15 -text "current directory" -anchor w
label $Wplot.b.c1.d.t -anchor w
pack configure $Wplot.b.c1.d.l $Wplot.b.c1.d.t -in $Wplot.b.c1.d -anchor w -side left
bind  $Wplot.b.c1.d.l <Button-3> {cathfile0 plot_dirdisp $Wplot}
bind  $Wplot.b.c1.d.t <Button-3> {cathfile0 plot_dirdisp $Wplot}

label $Wplot.b.c1.m.l -width 15 -anchor w -text "datafile"
label $Wplot.b.c1.m.t -width 30 -anchor w
button $Wplot.b.c1.m.but -text "edit" -width 10 -height 1 \
    -command "editfile m" -state disabled -bg grey 
pack configure $Wplot.b.c1.m.l $Wplot.b.c1.m.t $Wplot.b.c1.m.but -in $Wplot.b.c1.m -anchor w -side left
bind  $Wplot.b.c1.m.l <Button-3> {cathfile0 plot_datadisp $Wplot}
bind  $Wplot.b.c1.m.t <Button-3> {cathfile0 plot_datadisp $Wplot}

########################################################################################

label $Wplot.b.c1.e.l -width 15 -anchor w -text "pin-file"
label $Wplot.b.c1.e.t -width 30 -anchor w
button $Wplot.b.c1.e.but -text "edit" -width 10 -height 1 \
    -command "editfile e" -state disabled -bg grey
pack configure $Wplot.b.c1.e.l $Wplot.b.c1.e.t $Wplot.b.c1.e.but -in $Wplot.b.c1.e -anchor w -side left
bind  $Wplot.b.c1.e.l <Button-3> {cathfile0 plot_dimdisp $Wplot}
bind  $Wplot.b.c1.e.t <Button-3> {cathfile0 plot_dimdisp $Wplot}

label $Wplot.b.c1.s.l -width 15 -anchor w -text "xmgr-file"
label $Wplot.b.c1.s.t -width 30 -anchor w
button $Wplot.b.c1.s.but -text "edit" -width 10 -height 1 \
    -command "editfile s" -state disabled -bg grey

pack configure $Wplot.b.c1.s.l $Wplot.b.c1.s.t $Wplot.b.c1.s.but -in $Wplot.b.c1.s -anchor w -side left
bind  $Wplot.b.c1.s.l <Button-3> {cathfile0 plot_xmgrdisp $Wplot}
bind  $Wplot.b.c1.s.t <Button-3> {cathfile0 plot_xmgrdisp $Wplot}
#=======================================================================================


########################################################################################
#   frame c for modification: d - directory, m - datafile, e - pin-file, s - xmgr-file
########################################################################################

frame $Wplot.c; pack configure $Wplot.c -in $Wplot -anchor w

frame $Wplot.c.d;  frame $Wplot.c.m;  frame $Wplot.c.e;  frame $Wplot.c.s
pack configure $Wplot.c.d -in $Wplot.c -side left -anchor se -ipadx 3
pack configure $Wplot.c.s $Wplot.c.e  $Wplot.c.m -in $Wplot.c -side right -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
#
CreateLSBoxSBx $Wplot.c.d "select directory" top 24 17 2 $Hplot $Wplot.d.tt "" "" plot_sel_d sel
$Wplot.c.d.f.f.li insert end "\$HOME" ".."
if {[llength $sysdirlist]>0} {
	  foreach x $sysdirlist { $Wplot.c.d.f.f.li insert end "$x"}
#    $Wplot.c.d.f.f.li insert end $sysdirlist
}

foreach i [lsort [glob -nocomplain -- *]] {
    if [file isdirectory $i] {
	$Wplot.c.d.f.f.li insert end $i
    }  
}

# new directory insert
label $Wplot.c.d.li -anchor w -text "directory:"
bind  $Wplot.c.d.li <Button-3> {cathfile0 plot_diredi $Wplot}
entry $Wplot.c.d.i -width 29 -relief sunken
pack configure $Wplot.c.d.li $Wplot.c.d.i -in $Wplot.c.d -side top -anchor w
bind  $Wplot.c.d.i <Button-1> {plot_focus $Wplot.c.d.i; set plot_foc 1}
bind  $Wplot.c.d.i <Button-2> \
  {writescr0 $Wplot.d.tt "plot_19  $dirdatawork\n"; \
             $Wplot.c.d.i delete 0 end;             \
             $Wplot.c.d.i insert end $working_directory}
bind  $Wplot.c.d.i <Button-3> {cathfile0 plot_diredi $Wplot}
bind  $Wplot.c.d.i <Return>   {plot_edn; focus $Wplot}


########################################################################################
#                                                                          datafile: c.m
########################################################################################
#
label $Wplot.c.m.l -text "select and run datafile" -anchor w
bind  $Wplot.c.m.l <Button-3> \
	{writescr0  $Wplot.d.tt "left mouse click:\n"; cat_file $Hplot $Wplot.d.tt}
pack configure $Wplot.c.m.l -in $Wplot.c.m -side top -anchor w

frame $Wplot.c.m.1; # suffix-box and file-ordering-buttons
pack configure $Wplot.c.m.1 -in $Wplot.c.m -side top -anchor w -pady 2

frame $Wplot.c.m.1.2
pack configure $Wplot.c.m.1.2 -in $Wplot.c.m.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wplot.c.m.1.2 "suffix" left 9 3 2 $Hplot $Wplot.d.tt \
	"" [vst2list datasuffix.vst 0 "" "" dummy] plot_dummy sel
	
# file name selection box
CreateLSBoxSBx2 $Wplot.c.m "" "" 30 15 2 $Hplot $Wplot.d.tt "" "" plot_sel_m plot_sel_m2 sel


########################################################################################
#                                                                   pin-file: .c.e
########################################################################################
#
label $Wplot.c.e.l -text "select and run pin-file" -anchor w
bind  $Wplot.c.e.l <Button-3> \
	{writescr0 $Wplot.d.tt "left mouse click:\n"; cat_file $Hplot $Wplot.d.tt}
pack configure $Wplot.c.e.l -in $Wplot.c.e -side top -anchor w

frame $Wplot.c.e.1; # suffix-box and file-ordering-buttons
pack configure $Wplot.c.e.1 -in $Wplot.c.e -side top -anchor w -pady 2

frame $Wplot.c.e.1.2
pack configure $Wplot.c.e.1.2 -in $Wplot.c.e.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wplot.c.e.1.2 "suffix  " left 9 3 2 $Hplot $Wplot.d.tt \
	"" [vst2list pinsuffix.vst 0 "" "" dummy] plot_dummy sel

# file name selection box
CreateLSBoxSBx2 $Wplot.c.e "" "" 30 15 2 $Hplot $Wplot.d.tt "" "" plot_sel_e plot_sel_e2 sel


########################################################################################
#                                                                      xmgr-file: .c.s
########################################################################################

label $Wplot.c.s.l -text "select and run graphics file" -anchor w
bind  $Wplot.c.s.l <Button-3> \
    {writescr0 $Wplot.d.tt "left mouse click:\n"; cat_file $Hplot $Wplot.d.tt}
pack configure $Wplot.c.s.l -in $Wplot.c.s -side top -anchor w

frame $Wplot.c.s.1; # suffix-box and file-ordering-buttons
pack configure $Wplot.c.s.1 -in $Wplot.c.s -side top -anchor w -pady 2

frame $Wplot.c.s.1.1; frame $Wplot.c.s.1.2
pack configure $Wplot.c.s.1.1 -in $Wplot.c.s.1 -side left  -anchor sw -ipady 3
pack configure $Wplot.c.s.1.2 -in $Wplot.c.s.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wplot.c.s.1.2 "suffix  " left 9 3 2 $Hplot $Wplot.d.tt \
    "" [vst2list  xmgrsuffix.vst 0 "" "" dummy] plot_dummy sel

# file name selection box
CreateLSBoxSBx2 $Wplot.c.s "" "" 30 15 2 $Hplot $Wplot.d.tt "" "" plot_sel_s plot_sel_s2 sel


########################################################################################


insert_textframe $Wplot $tyh

# initialisation, focus etc.

global datasuffixlist pinsuffixlist xmgrsuffixlist sorttype filter
set    datasuffixlist [vst2list datasuffix.vst 0 "" "" x]
set     pinsuffixlist [vst2list  pinsuffix.vst 0 "" "" x]
set    xmgrsuffixlist [vst2list xmgrsuffix.vst 0 "" "" x]

if {[llength $datasuffixlist]<=0} {
   set datasuffixlist [list .dos .rat]
}
if {[llength $pinsuffixlist]<=0} {
   set pinsuffixlist [list .pin _pin]
}
if {[llength $xmgrsuffixlist]<=0} {
   set xmgrsuffixlist [list .xmgr _xmgr]
}

set sorttype alphabetical
set filter   "*"

set chgdir_allowed $chgdir_allowed_in
if {$chgdir_allowed==0} {$Wplot.c.d.f.l configure -text "change not allowed"}

plot_anzeige $Wplot
$Wplot.c.d.i delete 0 end

plot_sel_order $sorttype

focus $Wplot; set plot_foc 0

}

