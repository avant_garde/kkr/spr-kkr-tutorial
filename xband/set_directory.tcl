#  set directory
#
#
# Copyright (C) 2002 H. Ebert



proc set_directory { } {

########################################################################################
 proc set_directory_sel_d {d} {
  global Wsetdir Wsetdir_L Wsetdir_R
  update; set_directory_focus $Wsetdir
  $Wsetdir_L.c.d.f.f.li delete 0 end
  update; dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default
    global Wsetdir Wsetdir_L Wsetdir_R working_directory file_selected liste
    global directory_to_handle sysdirlist
    
    set cdir [expand_dollar_home $d]

    if [winfo exists $Wsetdir] {set w $Wsetdir.d.tt} else {set w .d.tt}

    if {[catch {cd $cdir} m]!=0} {
	set working_directory [pwd]
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	writescr0 $w "new directory chosen: [pwd]\n\n"
	set_directory_refresh_top_panel
	set working_directory [pwd]
	set file_selected "NONE"
	set directory_to_handle [pwd]
	if {[winfo exists $Wsetdir]} {
	    set_directory_anzeige $Wsetdir
	    
	    $Wsetdir_L.c.d.f.f.li delete 0 end
	    $Wsetdir_L.c.d.f.f.li insert end "\$HOME" ".."
	    foreach x $sysdirlist { $Wsetdir_L.c.d.f.f.li insert end "$x"}
	    foreach k [lsort [glob -nocomplain -- *]] {
		if {[file isdirectory $k]} {$Wsetdir_L.c.d.f.f.li insert end $k}
	    }
	    fill_file_list $Wsetdir_L.c.m.f.f.li
	}
	return 1
    }
}

########################################################################################
 proc fill_file_list  {w} {#                fill filelist into widget w
  global sub sorttype filter

  $w delete 0 end;  set subalt $sub

  if {$sorttype=="alphabetical"} {
      set liste [exec ls] 
  } else {
      set liste [exec ls -t]
  }

  foreach i $liste {
    if {[file isfile $i]} {
         if {[string match "*$filter*" $i]} {$w insert end $i}
    }
  } 

  lock;  set sub $subalt
 }


########################################################################################
 proc set_directory_dir_update {} {
     global Wsetdir Wsetdir_L Wsetdir_R liste working_directory sysdirlist sysdirlistmaxindex

     #set diraux [expand_dollar_home $working_directory]
    
     $Wsetdir_L.c.d.f.f.li delete 0 end
     $Wsetdir_L.c.d.f.f.li insert end "\$HOME" ".."

     foreach x $sysdirlist { $Wsetdir_L.c.d.f.f.li insert end "$x" }

 }

########################################################################################
 proc set_directory_refresh_top_panel {} {
  global Wsetdir Wsetdir_L Wsetdir_R filter file_selected

  set filter "*"
  set file_selected [pwd]

  set_directory_anzeige $Wsetdir

 }


########################################################################################
 proc set_directory_sel_m {f} {
  global Wsetdir Wsetdir_L Wsetdir_R file_selected sorttype
  set_directory_focus $Wsetdir
  if {[file exists $f]} {
    set file_selected $f
    writescr0 $Wsetdir.d.tt "new file_selected selected $file_selected\n\n"
    set_directory_anzeige $Wsetdir

    set_directory_sel_order $sorttype
    set_directory_dir_update
  }
 }

########################################################################################
 proc set_directory_sel_m2 {f} {
  global Wsetdir Wsetdir_L Wsetdir_R file_selected sorttype
  global editor edback edxterm edoptions

  set_directory_focus $Wsetdir
  if {[file exists $f]} {
    set file_selected $f
    writescr0 $Wsetdir.d.tt "new file_selected selected $file_selected\n\n"
    set_directory_anzeige $Wsetdir

    set ed_file $file_selected
    eval set res [catch "exec $edxterm $editor $edoptions $file_selected $edback" message]

    set_directory_sel_order $sorttype
    set_directory_dir_update
  }
 }


########################################################################################
 proc set_directory_sel_order {s} {
  global Wsetdir Wsetdir_L Wsetdir_R sorttype
  if {$s=="alphabetical"} {set sorttype alphabetical} else {set sorttype access}
  fill_file_list $Wsetdir_L.c.m.f.f.li
 }


########################################################################################
 proc set_directory_focus {w} {
  global Wsetdir Wsetdir_L Wsetdir_R set_directory_foc
  if     {$set_directory_foc==0} {set r 0} \
  elseif {$set_directory_foc==1} {set r [set_directory_edn]} \
  elseif {$set_directory_foc==2} {set r [set_directory_emfn]} \
  elseif {$set_directory_foc==3} {set r [set_directory_eefn]} \
  elseif {$set_directory_foc==4} {set r [set_directory_esfn]}
  if {$w!=""} {focus $w} else {focus $Wsetdir; return $r}
 }


########################################################################################
 proc set_directory_emfn {} {#                                             enter file_selected name
  global Wsetdir Wsetdir_L Wsetdir_R file_selected liste set_directory_foc

  set set_directory_foc 0
  writescr0 $Wsetdir.d.tt ""

  set file_selected $f
  writescr $Wsetdir.d.tt "input of new file to handle name: $file_selected\n\n"
  set_directory_anzeige $Wsetdir
  return 1
 }


########################################################################################
# handled keys: create, create+cd, remove
#
proc directory_to_handle_execute {key} {                  
    
    global directory_to_handle PRINTPS
    global Wsetdir Wsetdir_L Wsetdir_R
    global edxterm editor edoptions edback
    global sorttype
    
    writescr0 $Wsetdir.d.tt \
	"directory_to_handle_execute   --  directory to handle: $directory_to_handle"
    
    if {$directory_to_handle=="NONE"} {return}

    # no fullpath provided, prepend with current working directory
    # (second test is for drive in cygwin: C:/...) 
    set c0 [string index $directory_to_handle 0]
    set c1 [string index $directory_to_handle 1]
    if { ! ($c0 == "/" || $c1 == ":") } {
	set directory_to_handle "[pwd]/$directory_to_handle"
    }     
    
    if {$key=="create"||$key=="create+cd"} {
	
	if { [file isfile $directory_to_handle] } {
	    give_warning .  "WARNING \n\n cannot create new directory $directory_to_handle   \n\n \
                          a file with this name exists" 
	    return
	}
	if { [file isdirectory $directory_to_handle] } {
	    give_warning .  "WARNING \n\n cannot create new directory $directory_to_handle   \n\n \
                          a directory with this name exists already" 
	    return
	}
	
	eval set res [catch "exec mkdir $directory_to_handle" message]
	writescr $Wsetdir.d.tt  "\n $message \n"
	
	if {$key=="create+cd"} {
	    dirselected_comp $directory_to_handle
	} else {
	    dirselected_comp [pwd]
	}
	
    } elseif {$key=="remove"} {    
	
	if { [file isdirectory $directory_to_handle]==0 } {
	    give_warning . "WARNING \n\n $directory_to_handle \n\n  is not a directory" 
	    return
	}
	
	set nfiles [llength [exec ls $directory_to_handle]]
	writescr $Wsetdir.d.tt "\n number of files in $directory_to_handle: $nfiles "
	
	set cw $Wsetdir.confirm 
	toplevel_init $cw " " 0 0
	wm geometry $cw +300+50
	wm title    $cw "confirm removal"
	wm iconname $cw "confirm removal"
	if {$nfiles==0} {
	    button $cw.confirm  -text "confirm removal of $directory_to_handle" \
		-width 50 -height 10 -bg green \
		-command "set_directory_confirm_rmdir $directory_to_handle $cw"
	} else {
	    button $cw.confirm  -text "confirm removal of $directory_to_handle \n\n \
                 THE DIRECTORY IS NOT EMPTY" \
		-width 50 -height 10 -bg tomato \
		-command "set_directory_confirm_rmdir $directory_to_handle $cw"
	}
	button $cw.close  -text "close" -width 50 -height 5 -bg tomato1 \
	    -command "destros $cw"
	pack $cw.confirm $cw.close
	
    } else {  
	give_warning . "WARNING \n\n directory_to_handle_execute called \n \n with key = $key "
    }                                                                                      
}
#                                                        directory_to_handle_execute END
########################################################################################



########################################################################################
proc set_directory_confirm_rmdir {directory_to_handle cw} {# set_directory_confirm_rmdir

    global Wsetdir env

    set PRC set_directory_confirm_rmdir
    
    if {$directory_to_handle==$env(HOME)} {
	give_warning . "WARNING \n\n cannot remove home directory \n \n $directory_to_handle"
	destros $cw 
	return
    }
 
    debug $PRC "removing directory   $directory_to_handle "
    writescr $Wsetdir.d.tt "\n\n removing directory   $directory_to_handle  \n"
     
    set curr_dir [pwd]
    
    # :TODO: in cygwin file normalisation required
    if {$directory_to_handle==$curr_dir} {
	set short_cdir [file tail $curr_dir]
	set i [string last $short_cdir $curr_dir]
	set par_dir [string range $curr_dir 0 [expr $i-1]]
	dirselected_comp $par_dir
	set curr_dir [pwd]
	writescr $Wsetdir.d.tt "\n parent directory $par_dir \n"
	debug $PRC "parent directory $par_dir "
    }
    
    eval set res [catch "exec rm -r $directory_to_handle" message]
    writescr $Wsetdir.d.tt "\n$message \n"
    
    dirselected_comp $curr_dir
    destros $cw
}
#                                                        END set_directory_confirm_rmdir
########################################################################################



########################################################################################
 proc set_directory_anzeige {w} {#                                        update display
  global working_directory file_selected
  $w.main.left.b.c1.d.t        configure -text "$working_directory"
 }


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wsetdir Wsetdir_L Wsetdir_R
global Hfiles hlp_dir working_directory set_directory_foc file_selected
global mfold sysdirlist directory_to_handle
global editor edback edxterm edoptions
global COLOR WIDTH HEIGHT FONT
global dir0_set_directory

set PRC set_directory


set Hfiles files_h.hlp

set file_selected "NONE"
set directory_to_handle [pwd]

toplevel_init $Wsetdir "handle directory" 0 0

set tyh 14;  set working_directory [pwd] ;  set mfold "$file_selected"

# top buttons

insert_topbuttons $Wsetdir $Hfiles
bind $Wsetdir.a.e <Button-1> {
  if [set_directory_focus ""] {
    writescr $Wsetdir.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wsetdir; unlock_list
    if {($mfold!=$file_selected)&&([file exists "${file_selected}.vst"])} {.b.c1.1.vl invoke}
    Bend
  }
}


########################################################################################
#                  left and right column for MAIN section
########################################################################################

frame $Wsetdir.main                       
 pack $Wsetdir.main
frame $Wsetdir.main.left ; frame $Wsetdir.main.right 
 pack $Wsetdir.main.left         $Wsetdir.main.right -side left

set Wsetdir_L $Wsetdir.main.left
set Wsetdir_R $Wsetdir.main.right

########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wsetdir_L.b   
pack configure $Wsetdir_L.b -in $Wsetdir_L -anchor w -pady 8

frame $Wsetdir_L.b.c1 
frame $Wsetdir_L.b.c12 
frame $Wsetdir_L.b.c2 
frame $Wsetdir_L.b.c3 

pack configure $Wsetdir_L.b.c1  -in $Wsetdir_L.b -anchor w -side left -pady 8
pack configure $Wsetdir_L.b.c12 -in $Wsetdir_L.b -anchor w -side left -padx 20
pack configure $Wsetdir_L.b.c2  -in $Wsetdir_L.b -anchor nw -side left -pady 8 -padx 5
pack configure $Wsetdir_L.b.c3  -in $Wsetdir_L.b -anchor nw -side left -pady 8 -padx 2

#=======================================================================================
#                                     b   COLUMN 1
#=======================================================================================
set wmb  22
set wbut 10
set wt1  15
set went 23
set wt2  30

frame $Wsetdir_L.b.c1.d; frame $Wsetdir_L.b.c1.x;  frame $Wsetdir_L.b.c1.m; frame $Wsetdir_L.b.c1.m2
frame $Wsetdir_L.b.c1.e; frame $Wsetdir_L.b.c1.s
pack configure $Wsetdir_L.b.c1.d $Wsetdir_L.b.c1.x  $Wsetdir_L.b.c1.m  $Wsetdir_L.b.c1.m2 \
               $Wsetdir_L.b.c1.e $Wsetdir_L.b.c1.s -in $Wsetdir_L.b.c1 -anchor w

label $Wsetdir_L.b.c1.d.l -width $wt1 -text "current directory" -anchor w -padx 10
label $Wsetdir_L.b.c1.d.t -anchor w
pack configure $Wsetdir_L.b.c1.d.l $Wsetdir_L.b.c1.d.t -in $Wsetdir_L.b.c1.d -anchor w -side left


#=======================================================================================
frame $Wsetdir_L.b.c1.sor
frame $Wsetdir_L.b.c1.fil
pack configure $Wsetdir_L.b.c1.fil $Wsetdir_L.b.c1.sor -in $Wsetdir_L.b.c1 -anchor w -side left

label $Wsetdir_L.b.c1.fil.l -width 4 -text "Filter" -anchor w  -padx 10
entry $Wsetdir_L.b.c1.fil.e -background {lightblue} -textvariable {filter} -width {15} 
#          -font {-Adobe-Helvetica-Medium-R-Normal--*-160-*-*-*-*-*-*}
bind $Wsetdir_L.b.c1.fil.e <Key-Return> {set_directory_sel_order $sorttype}
 
pack configure $Wsetdir_L.b.c1.fil.l $Wsetdir_L.b.c1.fil.e -in $Wsetdir_L.b.c1.fil -anchor w -side left


label $Wsetdir_L.b.c1.sor.l -width 4 -text "sort mode" -anchor w  -padx 50
frame $Wsetdir_L.b.c1.sor.b
pack configure $Wsetdir_L.b.c1.sor.l $Wsetdir_L.b.c1.sor.b -in $Wsetdir_L.b.c1.sor -anchor w -side left
CreateLSBoxSBx $Wsetdir_L.b.c1.sor.b " " top 11 3 0 $Hfiles $Wsetdir.d.tt \
    "" "\{alphabetical\} \{last access\}" set_directory_sel_order ""

$Wsetdir_L.b.c1.sor.b.f.f.1 select


########################################################################################
#   frame c for modification: d - directory, m - file_selected
########################################################################################

frame $Wsetdir_L.c; pack configure $Wsetdir_L.c -in $Wsetdir_L -anchor w

frame $Wsetdir_L.c.d;  frame $Wsetdir_L.c.m;  frame $Wsetdir_L.c.e;  frame $Wsetdir_L.c.s
pack configure $Wsetdir_L.c.d -in $Wsetdir_L.c -side left -anchor se -ipadx 3
pack configure $Wsetdir_L.c.s $Wsetdir_L.c.e  $Wsetdir_L.c.m -in $Wsetdir_L.c -side right -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 
CreateLSBoxSBx $Wsetdir_L.c.d "select directory" top 24 21 2 $Hfiles $Wsetdir.d.tt "" "" \
	set_directory_sel_d sel
$Wsetdir_L.c.d.f.f.li insert end "\$HOME" ".."
foreach x $sysdirlist { $Wsetdir_L.c.d.f.f.li insert end "$x"}
foreach i [lsort [glob -nocomplain -- *]] {
    if {[file isdirectory $i]} {$Wsetdir_L.c.d.f.f.li insert end $i}
}


########################################################################################
#                                                                    file_selected: c.m
########################################################################################
#

# file name selection box
CreateLSBoxSBx2 $Wsetdir_L.c.m "select | edit file" top 30 21 2 $Hfiles \
             $Wsetdir.d.tt "" "" set_directory_sel_m set_directory_sel_m2 sel




#=======================================================================================
#                                RIGHT COLUMN
#=======================================================================================
set c3bg green
set c3wb 10
set c3hb 2
set c3py 0

frame $Wsetdir_R.top  
label  $Wsetdir_R.top.lab1  -text "directory name" -font $FONT(GEN)
entry  $Wsetdir_R.top.ent1  -textvariable directory_to_handle  \
                            -width 45 -relief sunken -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label  $Wsetdir_R.top.spac  -text " " -height 1
pack   $Wsetdir_R.top.lab1   $Wsetdir_R.top.ent1 $Wsetdir_R.top.spac -side top -anchor nw
#----------------------------------------------------------------------------------------
button $Wsetdir_R.create -text "create  new directory" -width 30 -height $c3hb \
   -command "directory_to_handle_execute create" \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wsetdir_R.create_cd -text "create  new  +  change directory" -width 30 -height $c3hb \
   -command "directory_to_handle_execute create+cd" \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wsetdir_R.delete -text "remove  current  directory" -width 30 -height $c3hb  \
      -command "directory_to_handle_execute remove"  -bg tomato -pady $c3py -padx 7    
#----------------------------------------------------------------------------------------

label $Wsetdir_R.label6 -text " "
button $Wsetdir_R.close -text "close"      -width $c3wb -height $c3hb \
   -command "destros $Wsetdir; unlock_list" \
   -bg tomato -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wsetdir_R.close_create -text "close + create system" -width 30 -height $c3hb \
   -command "destros $Wsetdir; create_system" \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------

pack configure $Wsetdir_R.top $Wsetdir_R.create $Wsetdir_R.create_cd \
         -in  $Wsetdir_R -anchor nw -side top -expand y -fill x 

pack configure  $Wsetdir_R.close_create $Wsetdir_R.close $Wsetdir_R.label6 $Wsetdir_R.delete  \
                -in $Wsetdir_R -anchor nw -side bottom -expand y -fill x 


########################################################################################


insert_textframe $Wsetdir $tyh

# initialisation, focus etc.

set dir0_set_directory [pwd]
debug $PRC "called starting from directory  $dir0_set_directory"  

set sorttype alphabetical
set filter   "*"

set_directory_anzeige $Wsetdir

set_directory_sel_order $sorttype

focus $Wsetdir; set set_directory_foc 0


}  
#########################################################  END of proc  set_directory
########################################################################################

