# set path 
#
#
# Copyright (C) 2000 H. Ebert
#

proc set_prog_path {NOTE PROG_NAME} {

########################################################################################
 proc set_prog_path_sel_d {d} {
  global Wpath
  update 
  set_prog_path_focus $Wpath
  $Wpath.c.d.f.f.li delete 0 end
  update
  dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default
  global Wpath exedirwork liste
  global programfile exedirlist

  set cdir [expand_dollar_home $d]

  if {[file isdirectory $cdir]==0} {
      writescr0 .d.tt "\n the directory  $cdir  does not exist"
      give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
      return
  }

  if [winfo exists $Wpath] {set w $Wpath.d.tt} else {set w .d.tt}

  if {[catch {cd $cdir} m]!=0} {
      set exedirwork [pwd]
      writescr0 $w "error while changing directory!\nerror message:\n$m\n"
      return 0
  } else {
      set exedirwork [pwd]
      writescr0 $w "new directory chosen: $exedirwork\n\n"
  
      set programfile  "" 
      if {[winfo exists $Wpath]} {
	  set_prog_path_anzeige $Wpath

	  $Wpath.c.d.f.f.li delete 0 end
	  $Wpath.c.d.f.f.li insert end "\$HOME" ".."
	  foreach x $exedirlist { $Wpath.c.d.f.f.li insert end "$x"}
	  foreach k [lsort [glob -nocomplain -- *]] {
	      if [file isdirectory $k] {$Wpath.c.d.f.f.li insert end $k}
	  }
	  pfuellen $Wpath.c.e.f.f.li 
      }
      return 1
  }
}



########################################################################################
#                   deduce the program version, update and return properly
#
 proc set_prog_path_prog_selected {f} {
    global Wpath Wprog PROGS_NAME_LIST PROGS_VERS_LIST PROG_VERS 
    global PROG_PATH working_directory exedirwork

    writescr0  $Wpath.d.tt  "<set_prog_path_prog_selected>        program selected from list: $f\n"
    writescr0  $Wprog.d.tt  "<set_prog_path_prog_selected>        program selected from list: $f\n"

    set run_prog_iprog "????"
    set run_prog_PROG  "????"
    set n     0
    set noversion 0
    set i2 [expr [string length $f]-1]
    set iprg -1
    foreach prg $PROGS_NAME_LIST { 
        incr iprg
        if { $f==$prg } {
           set noversion 1
           set run_prog_iprog $iprg
           set run_prog_PROG  $prg
        } else {
           if {[string first $prg $f]==0} {
              set i1 [string length $prg]
              incr n
              set run_prog_iprog $iprg
              set run_prog_PROG  $prg
              set PROG_VERS      [string range $f $i1 $i2]
           } 
       }
    }

    if {$run_prog_PROG!="????"} {
       writescr  $Wpath.d.tt  "program: $run_prog_PROG      "
       writescr  $Wprog.d.tt  "program: $run_prog_PROG      "
    }

    set reset_version 1
    if {$noversion==1} {
       writescr  $Wpath.d.tt  "NO program version \n"
       writescr  $Wprog.d.tt  "NO program version \n"
    } else {
       if {$n==1} {
           writescr  $Wpath.d.tt  "version: $PROG_VERS \n"
           writescr  $Wprog.d.tt  "version: $PROG_VERS \n"
           set PROGS_VERS_LIST [lreplace $PROGS_VERS_LIST $run_prog_iprog $run_prog_iprog $PROG_VERS]
           $Wprog.b.c1.m2.mb$run_prog_PROG configure -text "$run_prog_PROG$PROG_VERS"
           set reset_version 0
       } else {
           writescr  $Wpath.d.tt  "NO unique program version found  -- n = $n \n"
           writescr  $Wprog.d.tt  "NO unique program version found  -- n = $n \n"
       }
    }

    if {$run_prog_iprog!="????"&&$run_prog_PROG!="????"} {
       if {$reset_version==1} {
          set PROGS_VERS_LIST [lreplace $PROGS_VERS_LIST $run_prog_iprog $run_prog_iprog ""]
          $Wprog.b.c1.m2.mb$run_prog_PROG configure -text "$run_prog_PROG"
       }
    } else {
        give_warning .  "WARNING \n\n from <set_prog_path_prog_selected> \n\n \
               program name could not be fixed  \n\n \
               selection $f\n "
    }
#
#------------------------------------------- no return properly
#    
  writescr  $Wprog.d.tt  "returning from   [pwd]  \
                          to   $working_directory \n"

  set_prog_path_ok

}

########################################################################################
 proc set_prog_path_focus {w} {
  global Wpath set_prog_path_foc
  if     {$set_prog_path_foc==0} {set r 0} \
  elseif {$set_prog_path_foc==1} {set r [set_prog_path_edn]} \
  elseif {$set_prog_path_foc==2} {set r [set_prog_path_emfn]} \
  elseif {$set_prog_path_foc==3} {set r [set_prog_path_eefn]} \
  elseif {$set_prog_path_foc==4} {set r [set_prog_path_esfn]}
  if {$w!=""} {focus $w} else {focus $Wpath; return $r}
 }

########################################################################################
 proc set_prog_path_edn {} {# enter directory name
     global Wpath set_prog_path_foc set_prog_path_d

     set set_prog_path_foc 0
     set d [string trim [$Wpath.c.d.i get]]
     $Wpath.c.d.i delete 0 end
     writescr0 $Wpath.d.tt ""

     if {$d==""} {return 0}
     set set_prog_path_d [expand_dollar_home $d]
     
     if {![file isdirectory $set_prog_path_d] && ![file exists $set_prog_path_d]} {
	 mkdcdfill
     } else {
	 dirselected_comp $set_prog_path_d
	 set_prog_path_anzeige $Wpath
     }
     return 1
 }

########################################################################################
 proc set_prog_path_anzeige {w} {#                                              update display
  global exedirwork
  $w.b.d.t configure -text "$exedirwork"
  pfuellen $w.c.e.f.f.li
 }

########################################################################################
 proc pfuellen {w} {#                                    fill program list into widget w
  global sub PROGS_NAME_LIST exedirwork

  set PRC "pfuellen"

  $w delete 0 end;  set subalt $sub

  foreach p $PROGS_NAME_LIST { 
      set liste [exec ls $exedirwork]
      debug $PRC "program   $p  exedirwork $exedirwork "
      foreach x $liste {           
         if {[string match $p* $x] == 1 } {
            if {[file executable "$exedirwork/$x"]} {$w insert end $x }
	}
      }
  }
  lock;  set sub $subalt
 }


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wpath Wprog hlp_dir exedirwork set_prog_path_foc programfile
global exedirlist PROGS_NAME_LIST PROG_PATH working_directory
global editor edback edxterm edoptions

toplevel_init $Wpath "SET_PROG_PATH PROGRAMS" 0 0;   set tyh 14

set exedirwork [pwd]

set programfile "XXX"

# top buttons

insert_topbuttons $Wpath set_prog_path_h.hlp
bind $Wpath.a.e <Button-1> {
  cd $working_directory
  writescr .d.tt  "\nINFO from <set_prog_path> \n\n working directory reset to $working_directory \n"
  if [set_prog_path_focus ""] {
    writescr $Wpath.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wpath; unlock_list
    Bend
  }
}


########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wpath.b   
pack configure $Wpath.b -in $Wpath -anchor w -pady 8
frame $Wpath.b.d; frame $Wpath.b.e
pack configure $Wpath.b.d $Wpath.b.e -in $Wpath.b -anchor w

label $Wpath.b.d.l -width 20 -text "current directory" -anchor w
label $Wpath.b.d.t -anchor w
pack configure $Wpath.b.d.l $Wpath.b.d.t -in $Wpath.b.d -anchor w -side left

button $Wpath.b.d.ok  -text " OK ?"  -command set_prog_path_ok -height 2 -width 10 -bg green1 -padx 20 
pack configure $Wpath.b.d.ok -in $Wpath.b.d -side top -anchor w

#---------------------------------------------------------------------------
#
proc set_prog_path_ok {} {# 

    global PROG_PATH Wpath Wprog working_directory 
    global exedirwork exedirlist exedirlistmaxindex 
    
    set PROG_PATH "$exedirwork/" 

    set diraux [collapse_dollar_home $exedirwork]

    if {[llength $exedirlist]>0} {
	set exedirlist [update_dirlist $exedirlist $exedirlistmaxindex $diraux]
    } else {
	lappend exedirlist $diraux
    }
    
    cd $working_directory
    writescr .d.tt  "\nINFO from <set_prog_path> \n\n working directory reset to $working_directory \n"
    destroy $Wpath
    unlock_list
}
#---------------------------------------------------------------------------

########################################################################################



label $Wpath.b.e.l -width 20 -anchor w -text "programs requested"

if {$PROG_NAME=="NONE"} {
   set pl ""; foreach p $PROGS_NAME_LIST { set pl "$pl$p   "}
} else {
   set pl $PROG_NAME
}

label $Wpath.b.e.t -width 70 -anchor w -text "$pl"

pack configure $Wpath.b.e.l $Wpath.b.e.t -in $Wpath.b.e -anchor w -side left


########################################################################################
#   frame c for modification: d - directory, e - programfile
########################################################################################

frame $Wpath.c; pack configure $Wpath.c -in $Wpath -anchor w

frame $Wpath.c.d; frame $Wpath.c.e
pack configure $Wpath.c.d -in $Wpath.c -side left -anchor se -ipadx 3
pack configure $Wpath.c.e -in $Wpath.c -side left -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 
CreateLSBox $Wpath.c.d "select directory" top 24 21 2 set_prog_path_dirsel.hlp $Wpath.d.tt "" "" set_prog_path_sel_d sel

$Wpath.c.d.f.f.li insert end "\$HOME" ".."
foreach x $exedirlist {
    $Wpath.c.d.f.f.li insert end "$x"
}
foreach i [lsort [glob -nocomplain -- *]] {
    if [file isdirectory $i] {
	$Wpath.c.d.f.f.li insert end $i
    }   
}

# new directory insert
label $Wpath.c.d.li -anchor w -text "directory:"
entry $Wpath.c.d.i -width 29 -relief sunken
pack configure $Wpath.c.d.li $Wpath.c.d.i -in $Wpath.c.d -side top -anchor w
bind  $Wpath.c.d.i <Button-1> {set_prog_path_focus $Wpath.c.d.i; set set_prog_path_foc 1}
bind  $Wpath.c.d.i <Button-2> { 
    writescr0 $Wpath.d.tt "set_prog_path_19  $direxework\n"; 
    $Wpath.c.d.i delete 0 end
    $Wpath.c.d.i insert end $exedirwork
}
bind  $Wpath.c.d.i <Return> {set_prog_path_edn; focus $Wpath}



########################################################################################
#                                                                   program-file: .c.e
########################################################################################
#
label $Wpath.c.e.l -text "programs found" -anchor w
pack configure $Wpath.c.e.l -in $Wpath.c.e -side top -anchor w

# file name selection box
CreateLSBox $Wpath.c.e "" "" 24 16 2 set_prog_path_filesel.hlp $Wpath.d.tt "" "" set_prog_path_prog_selected sel

insert_textframe $Wpath $tyh    ; # ...d.tt

#writescr0 $Wpath.d.tt "last file name:  $programfile\n" 

########################################################################################
# initialisation, focus etc.

set_prog_path_anzeige $Wpath

$Wpath.c.d.i delete 0 end

pfuellen $Wpath.c.e.f.f.li

focus $Wpath; set set_prog_path_foc 0

writescr0  $Wpath.d.tt  $NOTE

}




