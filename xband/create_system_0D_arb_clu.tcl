#
# setting up the structure for arbitrary 0D systems
#
#
# Copyright (C) 2005 H. Ebert / S. Seidler
#
########################################################################################


# run_geometry is already called

proc create_system_0D_arb_clu {} {

    global visualizer 
    global sysfile syssuffix COLOR FONT HEIGHT Wclua
    global system cluster_type 
    global W_basvec IQCNTR NQCLU clu_box_dist clu_box_show    
#
#------------------------------------------------------------------------------------ ND
#
    global structure_window_calls
    global sysfile syssuffix 
    global TABCHSYM iprint irel Wcsys
    global system NQ NT ZT RQX RQY RQZ NOQ NLQ CONC TXTT
    global ITOQ IQAT RWS
    global RBASX RBASY RBASZ
    global NM IMT IMQ RWSM NAT
    global NQCL ICLQ
#
#------------------------------------------------------------------------------------ 2D
#
    global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
    global NQ_bulk_L NQ_bulk_R     
    global RASMOL
    global NSHLRELAX RELAXRAD NQRELAX NQSEL
    global NLAYBOX actual_top_layer ILAY_SEL selection_layer
    global create_system_0D_arb_clu_canvas_X create_system_0D_arb_clu_canvas_Y
    global create_system_0D_arb_clu_canvas_Z
    global IOCCTYPE_CURRENT OCCTYPE_CURRENT OCCTYPE NOCCTYPE
    
    #set clu_box_show 1
    #set clu_box_dist 0.5
    #run_geometry  create_box 
    #eval set res [catch "exec $RASMOL -script cluster_data.ras &" message]
    #writescr .d.tt "$message \n"


#---------------------------------------------------------------------------------------
#                               save settings for RESET
    global L_BOX  L_BOX_SAV

    for {set i 1} {$i <= 3} {incr i} { set L_BOX_SAV($i) $L_BOX($i) }

#---------------------------------------------------------------------------------------

    set  visualizer "rasmol"

    set NQSEL 0
    set NQRELAX 0  
  
    set BG1 #90ff90  ; # linen 
    set BG2 SteelBlue1 
    set BG3 ivory1
    set BG4 LightSalmon1
    set BG5 lightgreen
    set BGinfo yellow

    set W_arbclu .arbclu
    if {[winfo exists $W_arbclu]} {destroy $W_arbclu} 
    toplevel_init $W_arbclu "create 0-dimensional system  STEP B (showing actual toplayer and 2 layers below)" 0 0
    

    #######   column 1_DIR     scrolled canvas widget

    foreach DIR [list X Y Z ] {

       switch -exact $DIR {
	  X { set CWIDTH 200 ; set view "-x" ; set dir_hor "y" ; set dir_ver "z" }
          Y { set CWIDTH 200 ; set view "+y" ; set dir_hor "x" ; set dir_ver "z" }
          Z { set CWIDTH 450 ; set view "+z" ; set dir_hor "y" ; set dir_ver "x" }
       }
     
       set f [frame $W_arbclu.col1_$DIR]
       pack $f -expand 1 -fill both -side left

       set f.head [canvas $W_arbclu.col1_$DIR.head -background linen \
                -width $CWIDTH -height 80]
       pack $f.head -anchor n -fill x -side top

       set xorigin [expr $CWIDTH/2]
       set yorigin 70
       set delta   50
       set x0  [expr $xorigin - $delta/2]
       set y0  $yorigin
       set x1  [expr $x0 + $delta]
       set y1  [expr $y0 - $delta]

       $f.head create text $xorigin 10  -anchor center \
              -text "display box seen along $view axis" 
       $f.head create line $x0 $y0 $x1 $y0        -fill black -width 2 -arrow last
       $f.head create text [expr $x1 + 10] $y0     -anchor center -text $dir_hor
       $f.head create line $x0 $y0 $x0 $y1        -fill black -width 2 -arrow last
       $f.head create text [expr $x0 - 10] [expr $y1 +5]    -anchor center -text $dir_ver

       set f.body [frame $W_arbclu.col1_$DIR.body]
       pack $f.body -anchor s -fill both -expand 1 -side bottom

    
       scrollbar $f.body.sbx -command [list $f.body.canv xview] -orient horizontal
       scrollbar $f.body.sby -command [list $f.body.canv yview]
       set canvas_$DIR [canvas $f.body.canv -xscrollcommand [list $f.body.sbx set] \
	    -yscrollcommand [list $f.body.sby set] -background linen -width $CWIDTH]
    
       pack $f.body.sbx -anchor s -fill x -side bottom
       pack $f.body.sby -anchor e -fill y -side right
       pack $f.body.canv -expand 1 -fill both

    }

    $W_arbclu.col1_X.body.sby configure -command \
       [list BindYview [list $W_arbclu.col1_Y.body.canv $W_arbclu.col1_X.body.canv] ]
    $W_arbclu.col1_Y.body.sby configure -command \
       [list BindYview [list $W_arbclu.col1_Y.body.canv $W_arbclu.col1_X.body.canv] ]

    proc BindYview {lists args} {
        foreach l $lists { 
           eval {$l yview} $args
        }
    }

    set create_system_0D_arb_clu_canvas_X $canvas_X
    set create_system_0D_arb_clu_canvas_Y $canvas_Y
    set create_system_0D_arb_clu_canvas_Z $canvas_Z

    #######  column 2      parameter panels
    
    set col2 [frame $W_arbclu.col2]
    pack $col2 -fill both -side left

    ### button section
    frame $col2.btnback -background $BG1
    pack  $col2.btnback -fill both -side top
    
    set f [frame $col2.btnback.btn -background $BG1]
    pack $f -fill x -ipady 10 -padx 4 -side top
    button $f.cls -text CLOSE  -command [list destroy $W_arbclu]
    button $f.rst -text RESET  -command [list reset_occupation_types $canvas_Z]
    button $f.shw -text Show3D -command [list handle_show3d]
    button $f.dne -text DONE   -command [list create_system_0D_arb_clu_done $W_arbclu]
    eval pack [winfo children $f] -expand 1 -fill x -padx 2 -side left
    

    ### information
    frame $col2.info -background $BGinfo
    pack  $col2.info -fill both
    label $col2.info.t -background $BGinfo -text \
       " (reoccupy | select | deselect) \n\n cluster sites in displayed regime "
    pack  $col2.info.t -side top -anchor c -ipady 10

    
    ### layer info section
    frame $col2.r1back -background $BG2
    pack $col2.r1back -fill both

    frame $col2.r1back.row1 -background $BG2
    pack $col2.r1back.row1 -ipadx 10 -ipady 5 -pady 5
    frame $col2.r1back.row1.spacer -background $BG2 -borderwidth 0 -height 5
    pack $col2.r1back.row1.spacer

    set f [frame $col2.r1back.row1.r1 -background $BG2]
    pack $f -fill x -padx 40 -side top
    label $f.t -background $BG2 -text "number of layers"
    label $f.v -background $BG2 -textvariable NLAYBOX 
    pack $f.t -side left
    pack $f.v -side right
    
    set f [frame $col2.r1back.row1.r2 -background $BG2]
    pack $f -fill x -padx 40 -side top
    label $f.t -background $BG2 -text "actual top layer"
    label $f.v -background $BG2 -textvariable actual_top_layer
    pack $f.t -side left
    pack $f.v -side right
    
    set f [frame $col2.r1back.row1.r3 -background $BG2]
    pack $f -fill x -padx 40 -side top
    label $f.t -background $BG2 -text "reference layer"
    label $f.v -background $BG2 -textvariable ILAY_SEL 
    pack $f.t -side left
    pack $f.v -side right

    set f [frame $col2.r1back.row1.r4 -background $BG2]
    pack $f -ipady 10 -side top
    button $f.bprv -text "  prev layer  " -command [list change_atom_layers $canvas_Z -1] 
    button $f.bnxt -text "  next layer  " -command [list change_atom_layers $canvas_Z +1]
    pack $f.bprv $f.bnxt -padx 10 -side left

    set f $col2.r1back.row1.r5
    pack [frame $f -background $BG2 -borderwidth 0]
    label $f.lab1 -background $BG2 -text "select only from"
    checkbutton $f.top -background $BG2 -highlightthickness 0 -text "top"   \
	    -variable selection_layer(top) 
    checkbutton $f.mid -background $BG2 -highlightthickness 0 -text "middle" \
	    -variable selection_layer(mid) 
    checkbutton $f.bot -background $BG2 -highlightthickness 0 -text "bottom" \
	    -variable selection_layer(bot) 
    label $f.lab2 -background $BG2 -text "layers"
    eval pack [winfo children $f] -side left

    foreach t [list top mid bot] {
	set selection_layer($t) 1
    }

    ### selected sites
    frame $col2.nqsel -background $BG3
    pack $col2.nqsel -fill both

    set f [frame $col2.nqsel.row2 -background $BG3]
    pack $f -fill both -ipadx 10 -pady 10

    frame $f.r1 -background $BG3
    pack $f.r1 -side top
    label $f.r1.t -background $BG3 -text "number of selected sites: "
    label $f.r1.v -background $BG3 -textvariable NQSEL
    pack $f.r1.t -side left
    pack $f.r1.v -side right
    
    ### occupation types section
    frame $col2.r2back -background $BG3
    pack $col2.r2back -fill both

    set f [frame $col2.r2back.row2 -background $BG3]
    pack $f -fill both -ipadx 10

    frame $f.r1 -background $BG3
    pack $f.r1 -side top
    label $f.r1.t -background $BG3 -text "present occupation type:  "
    label $f.r1.v -background $BG3 -textvariable OCCTYPE_CURRENT
    pack $f.r1.t -side left
    pack $f.r1.v -side right
    
    pack [frame $f.spacer -background $BG3 -borderwidth 0 -height 10]
    label $f.lab -background $BG3 -text "available occupation types"
    pack $f.lab -side top
    
    pack [frame $f.r2] -fill both -ipadx 10 -padx 10 -side top
    set occtype_lb [listbox $f.r2.lb -yscrollcommand [list $f.r2.sb set] -selectmode single \
	    -height 6]
    scrollbar $f.r2.sb -command [list $f.r2.lb yview]
    pack $f.r2.lb -expand 1 -fill both -side left
    pack $f.r2.sb -fill y -side right
   
    # fill occupation types listbox 
    eval $occtype_lb insert end [prepare_occupation_types] 
    bind $occtype_lb <ButtonRelease-1> { picked_occtype %W [%W nearest %y] }
    
    proc picked_occtype {lb index} {
	
	global IOCCTYPE_CURRENT OCCTYPE_CURRENT
	
	$lb selection clear 0 end
	$lb selection set $index
	$lb see $index   
	
	set  OCCTYPE_CURRENT [$lb get $index]
	set IOCCTYPE_CURRENT [expr {$index + 1}] 
	
    }
    
    picked_occtype $occtype_lb 0
   
    button $f.addocc -text "new occupation type" \
	    -command [list handle_new_occupation_type $occtype_lb]
    
    proc handle_new_occupation_type {occtype_lb} {
	
	global NOCCTYPE
	
	set label [add_occupation_type]
	
	if {$label != ""} { 
	    # insert new occupation type in listbox
	    $occtype_lb insert end $label 
	    # and select the new one
	    picked_occtype $occtype_lb [expr {$NOCCTYPE - 1}]  
	}
	
    }
    
    pack $f.addocc -pady 10 -side top
    
    #-----------------------------------------------------------------------------------
    ### relaxation zone section 
    frame $col2.r3back -background $BG4
    pack  $col2.r3back -fill both

    set f [frame $col2.r3back.row3 -background $BG4]
    pack $f -ipadx 10 -ipady 10 -pady 10
 
    frame $f.r1 -background $BG4
    pack $f.r1 -fill x -side top
    label $f.r1.l -background $BG4 -text "NSHLRELAX: "
    scale $f.r1.s -background $BG4 -highlightthickness 0 -orient horizontal \
	    -from 0 -to 20 -sliderlength 40 \
	    -variable NSHLRELAX -command "reset_RELAXRAD "
    pack $f.r1.l -side left
    pack $f.r1.s -side right
    
    proc reset_RELAXRAD {{x dummy}} {global RELAXRAD ; set RELAXRAD 0.0 }

    frame $f.r2 -background $BG4
    pack $f.r2 -fill x -side top
    label $f.r2.l -background $BG4 -text "RELAXRAD: "
    scale $f.r2.s -background $BG4 -highlightthickness 0 -orient horizontal \
	    -from 0 -to 25.0 -resolution 0.1 -sliderlength 40 \
	    -variable RELAXRAD -command "reset_NSHLRELAX "
    pack $f.r2.l -side left
    pack $f.r2.s -side right
    
    proc reset_NSHLRELAX {{x dummy}} {
	global NSHLRELAX RELAXRAD
	if {$RELAXRAD==0.0} {
	    set NSHLRELAX 2
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
global QUICK QUICK_0D
if {$QUICK_0D==1} {
  puts "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQUICK 0D"
  set NSHLRELAX 1
}
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
	} else {
	    set NSHLRELAX 0 
	}
    }
    
    button $f.add_relaxzone -text "add relaxation zone" \
        -command create_system_0D_arb_clu_relax
    pack $f.add_relaxzone -pady 10 -side top
    
    frame $f.r3 -background $BG4
    pack $f.r3 -fill x
    label $f.r3.l -background $BG4 -text "sites in relaxation zone"
    label $f.r3.v -background $BG4 -textvariable NQRELAX 
    pack $f.r3.l -side left
    pack $f.r3.v -side right

    #-----------------------------------------------------------------------------------
    ### center cluster
    frame $col2.r3center -background $BG5
    pack  $col2.r3center -fill both

    set f [frame $col2.r3center.row3 -background $BG5]
    pack $f -ipadx 10 -ipady 10 -pady 10
 
    button $f.center_cluster -text "center cluster at origin" \
        -command create_system_0D_arb_clu_center
    pack $f.center_cluster -pady 10 -side top

    #-----------------------------------------------------------------------------------

    update idletasks   ; # needed for call to center_canvas_items below

    init_iqbox_globals

    #
    set actual_top_layer 1

    occupy_cluster_atoms $canvas_Z $actual_top_layer 

    occupy_cluster_atoms_XY $canvas_X $canvas_Y

    #      (reoccupy | select | deselect)  cluster sites in displayed regime 

    # change occtype of "atom" to IOCCTYPE_CURRENT
    $canvas_Z bind atom      <Button-1> { change_iocctype %W [get_iqbox_from_tags %W] $IOCCTYPE_CURRENT }
    $canvas_Z bind atomlabel <Button-1> { change_iocctype %W [get_iqbox_from_tags %W] $IOCCTYPE_CURRENT }
    
    # select atom site leaving type unchanged
    $canvas_Z bind atom      <Button-2> { change_iocctype %W [get_iqbox_from_tags %W] -2 }
    $canvas_Z bind atomlabel <Button-2> { change_iocctype %W [get_iqbox_from_tags %W] -2 }
    
    # reset occtype
    $canvas_Z bind atom      <Button-3> { change_iocctype %W [get_iqbox_from_tags %W] -3 }
    $canvas_Z bind atomlabel <Button-3> { change_iocctype %W [get_iqbox_from_tags %W] -3 }

    # freehandselection of atoms by polygon
    bind_freehandselection $canvas_Z [list freehand_change_iocctype $canvas_Z]
    # [list handle_selected_items $canvas_Z] 
    
    #$canvas_Z configure -closeenough 3.0


#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
global QUICK QUICK_0D
if {$QUICK_0D==1} {
  puts "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQUICK 0D"
 handle_new_occupation_type $occtype_lb
}
#QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ

}

#
########################################################################################
#
proc handle_show3d {} {

    global COLOR
    global visualizer show_site_labels show_unit_cell show_cube show_axis 
    global show_empty_spheres sphere_color
    
    set win .occ_show3d
    catch {destroy $win}
    toplevel $win

    build_visualizer_parameter_controls $win narrow   ; # compactstyle
    
    label $win.patience -text " " -fg black -relief flat -height 3
    pack  $win.patience -expand y -fill x
    
    button $win.run -bg green1 -height 3 -padx 9 -pady 3 \
	    -text "run visualizer (set up of input may take a while)" \
	    -command { 
	
	patience .occ_show3d 1 
	
	run_visualizer_for_arbclu_occtype $visualizer \
		$show_site_labels $show_unit_cell $show_cube $show_axis \
		$show_empty_spheres $sphere_color
	
	patience .occ_show3d 0
	
    }
    
    button $win.cls -bg $COLOR(CLOSE) -height 3 -padx 9 -pady 3 -text "CLOSE" \
	    -command [list destroy $win] 
    
    pack $win.run $win.cls -expand 1 -fill x 
    
}

#
########################################################################################
# add atom to systeminfo for run_visualizer
#
proc add_arbclu_systeminfo {iatom} {

    global NQBOX NLAYBOX ILAY_SEL RQBOX IQBOX1_ILAY IQBOX2_ILAY PN_ILAY IQ_IQBOX N_IQBOX
    global IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE NOCCTYPE Z_OCCTYPE
    global RWS
    
    set PRC [myname]
    set dbg 1

    # prepare color tempfacs for rasmol
    set color_codes [list dummy]  ; # start index with 1
    for {set i 1} {$i <= $NOCCTYPE } {incr i} {
	lappend color_codes [expr {$i*3.0/$NOCCTYPE}]
    }
    
    if $dbg { debug $PRC "color_codes: $color_codes" }
    
    for {set ILAY 1} {$ILAY <= $NLAYBOX} {incr ILAY} {
	for {set IQBOX $IQBOX1_ILAY($ILAY)} {$IQBOX <= $IQBOX2_ILAY($ILAY)} { incr IQBOX } {
	    
	    incr iatom
	    
	    set x $RQBOX(1,$IQBOX)
	    set y $RQBOX(2,$IQBOX) 
	    set z $RQBOX(3,$IQBOX) 
	    
	    #set zlevel $ILAY  
	    
	    set iocctype [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)]
	    #set color [map_iocctype_to_color $iocctype] 
	    #set color_code [map_color_to_tempfac $color]
	    #set color_code 2.0
	    set color_code [lindex $color_codes $iocctype]
	    
	    set label $OCCTYPE($iocctype) 
	    set changed $SEL_IQBOX($IQBOX)
	    
	    set iq $IQ_IQBOX($IQBOX)
            if {$changed==0} {
               set rws [expr $RWS($iq)*0.3]
	    } else {
               set rws $RWS($iq)
            }
	    # 
            set zt $Z_OCCTYPE(1,$iocctype) 

	    push_systeminfo_atom "a" $iatom $iatom $x $y $z $color_code \
		    $label $iq $iocctype $zt $rws 

	    if $dbg {
		debug $PRC "iatom: $iatom x: $x y: $y z: $z color: $color_code lab: $label iq: $iq ($IQBOX) iocctype: $iocctype rws: $rws" 
	    }
	    
	}
    }
    
    return $iatom
    
}

#
########################################################################################
#
proc freehand_change_iocctype {canvas ids} {

    global IOCCTYPE_CURRENT
    global selection_layer

    foreach id $ids {
	
	set tags [$canvas gettags $id]
	
	if {[lcontain $tags "atom"]} { 
	    if {[regexp {zlevel([0-9]*)} $tags dummy zlevel] == 1} {
		set layertype [zlevel_to_layer_type $zlevel] 
		if {$selection_layer($layertype)} {
		    # last arg is suppress_intensify
		    change_iocctype $canvas [get_iqbox_from_tags $canvas $id] \
                                    $IOCCTYPE_CURRENT 1
		}
	    }
	}
	
    }

    # so far suppressed, now do all at once
    intensify_levels $canvas
    
}

#
########################################################################################
# layer_type:  top mid bot
#
proc zlevel_to_layer_type {zlevel {layers_to_draw 3}} {
    
    global actual_top_layer
    
    if {$zlevel == $actual_top_layer} {
	set layertype "top"
    } elseif {$zlevel == ($actual_top_layer + $layers_to_draw - 1)} {
	set layertype "bot"
    } else {
	set layertype "mid"
    }
    
    return $layertype
    
}

########################################################################################
# ::tk::Darken -- (from ./lib/tk8.4/palette.tcl)
# Given a color name, computes a new color value that darkens (or
# brightens) the given color by a given percent.
#
# Arguments:
# color -       Name of starting color.
# perecent -    Integer telling how much to brighten or darken as a
#               percent: 50 means darken by 50%, 110 means brighten
#               by 10%.
#
proc ___darken_color {color percent} {
    return [::tk::Darken $color $percent]
}

proc ___brighten_color {color percent} {
    set percent [expr {100 + $percent}]
    return [::tk::Darken $color $percent]
}


# 
########################################################################################
#
proc change_atom_layers {canvas layerdelta {layers_to_draw 3}} {
    
    global NLAYBOX actual_top_layer
    global create_system_0D_arb_clu_canvas_X create_system_0D_arb_clu_canvas_Y
    
    # limit to top layer
    set new_top_layer [expr {$actual_top_layer + $layerdelta}]
    if {$new_top_layer <= 1} {set new_top_layer 1}
    
    # limit to be able to draw $layers_to_draw from bottom layer 
    set bottom_layer [expr {$NLAYBOX - $layers_to_draw + 1}]
    if {$new_top_layer > $bottom_layer} {
	set new_top_layer $bottom_layer
    }
    
    if {$new_top_layer != $actual_top_layer} {
	
	set actual_top_layer $new_top_layer

	$canvas delete all    ;  # atom
	occupy_cluster_atoms $canvas $actual_top_layer 

        occupy_cluster_atoms_XY $create_system_0D_arb_clu_canvas_X \
                                $create_system_0D_arb_clu_canvas_Y

    }

}

#
########################################################################################
#
proc draw_atoms {canv scale xs ys iqboxs colors zlevels selections level_radii labels} {

    # offset to map real zlevel (NLAYBOX) to index for radii (starting with 0)
    set offset_level_to_index [lindex [lsort -uniq -integer $zlevels] 0]
    #debug  [myname] "zlevels: $zlevels (uniqsort: [lsort -uniq $zlevels])"
 
    foreach x $xs y $ys iqbox $iqboxs color $colors zlevel $zlevels \
	    selection $selections label $labels {
	
	set radius [lindex $level_radii [expr {$zlevel - $offset_level_to_index}]]
	
	if {$selection == 1} {
	    set color2 black     ; # selected means changed occtype
	} else {
	    set color2 $color
	}
	
	set x0 [expr {($scale*$x) - $radius}]
	set y0 [expr {($scale*$y) - $radius}]
	set x1 [expr {($scale*$x) + $radius}]
	set y1 [expr {($scale*$y) + $radius}]
	$canv create oval $x0 $y0 $x1 $y1 -fill $color -outline $color2 \
		-width 2 -tags [list "atom" "zlevel$zlevel" "atom$iqbox" "iqbox$iqbox"] 

	$canv create text [expr {$scale*$x}] [expr {$scale*$y}] -anchor center \
		-text $label -tags [list "atomlabel" "atomlabel$iqbox" "iqbox$iqbox"]
    }
    
    # bring circles in correct drawing order (smallest is top)
    # highest level is top
    foreach i [lsort -decreasing -uniq $zlevels] {
	$canv raise "zlevel$i"
    }
    $canv raise atomlabel

}

#
########################################################################################
#
proc draw_atoms_XY {canv_X canv_Y scale xs ys zs iqboxs colors xlevels ylevels \
                    selections radii_x radii_y labels z_ILAY z_ILAY_SEL z_ILAY_SHOW \
                    IQ_list xmin xmax ymin ymax} {

    global canvas_largest_circle_radius canvas_smallest_circle_radius canvas_scale
    global actual_top_layer

    foreach x $xs y $ys z $zs IQ $IQ_list iqbox $iqboxs color $colors \
	    xlevel $xlevels ylevel $ylevels \
	    selection $selections label $labels radius_x $radii_x radius_y $radii_y {
	
	set xx [format %4.1f $x]
	set yy [format %4.1f $y]
	set zz [format %4.1f [expr -$z]]

	if {$selection == 1} {
	    set color2 black     ; # selected means changed occtype
	} else {
	    set color2 $color
	}
	set x0 [expr {($scale*$x) - $radius_x}]
	set y0 [expr {($scale*$y) - $radius_x}]
	set z0 [expr {($scale*$z) - $radius_x}]
	set x1 [expr {($scale*$x) + $radius_x}]
	set y1 [expr {($scale*$y) + $radius_x}]
	set z1 [expr {($scale*$z) + $radius_x}]

	$canv_X create oval $y0 $z0 $y1 $z1 -fill $color -outline $color2 \
		-width 2 -tags [list "atom" "xlevel$xlevel" "atom$iqbox" "iqbox$iqbox"] 

	$canv_X create text [expr {$scale*$y}] [expr {$scale*$z}] -anchor center \
		-tags [list "atomlabel" "atomlabel$iqbox" "iqbox$iqbox"]  \
      		-text "$label\n $IQ" 
	      # -text "$yy \n $zz"


	set x0 [expr {($scale*$x) - $radius_y}]
	set y0 [expr {($scale*$y) - $radius_y}]
	set z0 [expr {($scale*$z) - $radius_y}]
	set x1 [expr {($scale*$x) + $radius_y}]
	set y1 [expr {($scale*$y) + $radius_y}]
	set z1 [expr {($scale*$z) + $radius_y}]

	$canv_Y create oval $x0 $z0 $x1 $z1 -fill $color -outline $color2 \
		-width 2 -tags [list "atom" "ylevel$ylevel" "atom$iqbox" "iqbox$iqbox"] 

	$canv_Y create text [expr {$scale*$x}] [expr {$scale*$z}] -anchor center \
		-tags [list "atomlabel" "atomlabel$iqbox" "iqbox$iqbox"]  \
      		-text "$label\n $IQ" 
	      # -text "$xx \n $zz"
    }

    set W [ expr 1.6 * $canvas_largest_circle_radius ]

    set x0 [expr $scale * $xmin - 2*$canvas_largest_circle_radius]
    set x1 [expr $scale * $xmax + 2*$canvas_largest_circle_radius]
    set y0 [expr $scale * $ymin - 2*$canvas_largest_circle_radius]
    set y1 [expr $scale * $ymax + 2*$canvas_largest_circle_radius]

    foreach z $z_ILAY_SHOW {
        set z0 [expr $scale * $z ]
        $canv_X create line $y0 $z0 $y1 $z0 -fill lightgrey -width $W
        $canv_Y create line $x0 $z0 $x1 $z0 -fill lightgrey -width $W
    }

    set z0 [expr $scale * $z_ILAY_SEL ]
    $canv_X create line $y0 $z0 $y1 $z0 -fill black -width [expr 0.25*$W]
    $canv_Y create line $x0 $z0 $x1 $z0 -fill black -width [expr 0.25*$W]


    set x0 [expr $scale * $xmin - 1.5*$canvas_largest_circle_radius]
    set x1 [expr $scale * $xmax + 1.5*$canvas_largest_circle_radius]
    set y0 [expr $scale * $ymin - 1.5*$canvas_largest_circle_radius]
    set y1 [expr $scale * $ymax + 1.5*$canvas_largest_circle_radius]
    set ILAY 0
    foreach z $z_ILAY {
       incr ILAY	

       $canv_X create text $y0 [expr {$scale*$z}] -anchor center \
		-text "$ILAY"
       $canv_X create text $y1 [expr {$scale*$z}] -anchor center \
		-text "$ILAY"

       $canv_Y create text $x0 [expr {$scale*$z}] -anchor center \
		-text "$ILAY"
       $canv_Y create text $x1 [expr {$scale*$z}] -anchor center \
		-text "$ILAY"

       if { $ILAY==$actual_top_layer} {
          $canv_X create text $y0 [expr {$scale*$z}] -anchor center \
		-text " top\n\nlayer"
          $canv_X create text $y1 [expr {$scale*$z}] -anchor center \
		-text " top\n\nlayer"
          $canv_Y create text $x0 [expr {$scale*$z}] -anchor center \
		-text " top\n\nlayer"
          $canv_Y create text $x1 [expr {$scale*$z}] -anchor center \
		-text " top\n\nlayer"
       }

    }

    
    # bring circles in correct drawing order (smallest is top)
    # highest level is top
    foreach i [lsort -decreasing -uniq $xlevels] {
	$canv_X raise "xlevel$i"
    }
    $canv_X raise atomlabel

    foreach i [lsort -decreasing -uniq $ylevels] {
	$canv_Y raise "ylevel$i"
    }
    $canv_Y raise atomlabel

}

#
########################################################################################
# change iocctype of atom IQBOX (changes in two globalvars) and 
# recolor circle in canvas accordingly
#
proc change_iocctype {canv IQBOX OCCTYPE_key {suppress_intensify 0}} {
    
    global IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE NQBOX NQSEL

    #-----------------------------------------------------------------------------------
    # no change in occupation if layer is deseclected from modification >> return

    global zlevels iqboxs selection_layer
    
    set zlevel_IQBOX 0
    foreach i $iqboxs zlevel $zlevels {
	if {$i == $IQBOX} { set zlevel_IQBOX $zlevel }
    }

    if {$zlevel>=1 && $zlevel<=3} {
       set layertype [zlevel_to_layer_type $zlevel_IQBOX] 
       if {$selection_layer($layertype)==0} {puts "RETURN !!!!!!!!!"; return}
    }
    #-----------------------------------------------------------------------------------

    
    # default iocctype is always in position 0
    set iocctype [lindex $IOCCTYPE_IQBOX($IQBOX) 0]
    
    # OCCTYPE_key = -3:  deselect and reset to original iocctype
    #               -2:    select and   set to original iocctype

    if {$OCCTYPE_key == -3} {    

	set sel 0 

    } elseif {($OCCTYPE_key == -2) || ($iocctype == $OCCTYPE_key) } {
        
	lappend iocctype [lindex $IOCCTYPE_IQBOX($IQBOX) 0] 
	set sel 1

    } else {

	set sel 1
	# changed iocctype is in position 1
	lappend iocctype $OCCTYPE_key 

    }
    
    # or color as argument
    set color [map_iocctype_to_color [lindex $iocctype $sel]]
    
    if {$sel == 1} {
	set color2 black
    } else {
	set color2 $color
    }
    
    set      SEL_IQBOX($IQBOX) $sel 
    set IOCCTYPE_IQBOX($IQBOX) $iocctype

    $canv itemconfigure      "atom$IQBOX" -fill $color -outline $color2     
    $canv itemconfigure "atomlabel$IQBOX" -text $OCCTYPE([lindex $iocctype $sel])

    # intensify level mode is default of arg in proc
    # brute force approach intensify all displayed atoms not only the changed one ...
    # but its fast enough if not used in bulk operations like freehand_change_iocctype 
    # or reset_occupation_types

    if {! $suppress_intensify} {
	intensify_levels $canv  
    }

    set NQSEL 0
    for {set I 1} {$I <= $NQBOX} {incr I} {
      if { $SEL_IQBOX($I)>0} {incr NQSEL}  
    } 
    
}


#
########################################################################################
# returns -1 if tag not available
#
proc get_iqbox_from_tags {canv {tagOrId current}} {
    
    set tags [$canv gettags $tagOrId]
    
    if {[regexp {iqbox([0-9]*)} $tags dummy iqbox] != 1} {
	set iqbox -1
    }
    
    return $iqbox
    
}

#
# only preliminary
#
########################################################################################
# maybe it's better to use a calculated colorscale
# available is e.g. proc map_rasmol_tempfactor_to_rgb
#
proc map_iocctype_to_color {iocctype} {

    # black is never used
#   set colors0 [list black green yellow orange red purple tomato lightgreen lightblue]
#   
#   if 0 {
#       set colors1 [list]
#       set perc 50 ; foreach color $colors0 { lappend colors1 [::tk::Darken $color $perc] }
#   }	
#   
#   set colors1 [list #007f00 #7f7f00 #00007f #7f5200 #7f0000 #501078 #7f3123 #487748 #566c73]
#   
#   set colors [concat $colors0 $colors1] 

#   set colors [list black darkseagreen PaleTurquoise palegreen magenta2 SeaGreen2  
    set colors [list black DarkOrchid yellowgreen gray20 lightsteelblue tomato cyan4  \
                LightYellow1 brown4 LightSkyBlue4 SeaGreen LightSalmon4 aliceblue orange1\
                lavenderblush midnightblue DarkKhaki sienna2 lightpink SpringGreen4 whitesmoke\
                IndianRed2 DarkGoldenrod2 coral3 orchid3 LightCyan2 OliveDrab1 OldLace \
                honeydew gray79 mediumspringgreen PaleVioletRed grey3 yellow plum2 DarkMagenta \
                BlanchedAlmond HotPink1 thistle2 navajowhite DodgerBlue1 saddlebrown mistyrose \
                gray47 HotPink4 yellow2 PeachPuff1 grey74 aquamarine gray6 gray24 plum1  \
                grey39 LightSalmon PeachPuff4 seashell grey77 SkyBlue3 snow IndianRed3  \
                grey64 grey58 PaleGreen turquoise2 gray69 pink4 grey96 DarkOrange1  \
                MediumOrchid1 DimGray gray68 tan4 chartreuse1 gray10 red4 gray95 gray85  \
                gray45 gray31 MistyRose3 wheat gray72 royalblue grey54 DarkSlateGrey  \
                RosyBrown chocolate gray70 gray58 GhostWhite DarkOliveGreen RoyalBlue3  \
                OliveDrab4 slategrey SlateBlue4 sienna3 ivory1 MediumVioletRed gray91  \
                lightseagreen gold SteelBlue4 greenyellow goldenrod3 gray22 darkgrey gray50  \
                grey46 chocolate2 grey2 DeepSkyBlue gray65 DarkOliveGreen4 moccasin  \
                LemonChiffon4 LightCyan1 gray54 honeydew2 gray92 AntiqueWhite mintcream  \
                grey88 wheat1 grey76 LightSkyBlue3 blue2 DarkRed orchid2 MintCream gray5  \
                bisque1 grey4 RosyBrown4 azure3 DodgerBlue chocolate3 darkorange MistyRose4  \
                pink2 PaleTurquoise2 azure lightcyan LavenderBlush3 SpringGreen3 brown2  \
                orange4 LightPink4 grey14 DeepPink2 dodgerblue firebrick3 LightSalmon3  \
                tomato4 LightYellow3 grey95 thistle gold3 darkorchid grey38 lightgreen  \
                seashell2 PaleGreen3 DarkOrange3 bisque4 LightSteelBlue2 grey45 magenta1  \
                gray84 DarkGoldenrod4 gray19 DarkOliveGreen2 gray30 MistyRose1 OliveDrab3  \
                maroon2 RosyBrown1 gray3 orange2 DarkSeaGreen1 DarkOrchid4 grey71 grey35  \
                grey86 tan1 gray100 LightYellow RosyBrown2 gray44 darkcyan RoyalBlue4  \
                NavajoWhite4 gray78 gray42 DarkGray DodgerBlue2 HotPink dimgrey gray41  \
                lavender gray18 grey89 OrangeRed bisque seashell1 grey93 gray34 papayawhip  \
                DarkOrchid1 yellowgreen gray20 DarkOrchid grey73 LightGoldenrod  \
                lightslategrey mediumvioletred honeydew3 DarkOrchid3 chartreuse4  \
                LavenderBlush1 red1 grey3 forestgreen yellow honeydew1 lightsteelblue  \
                mediumblue darkolivegreen cyan1 grey62 grey48 MediumAquamarine gray12  \
                gray93 springgreen chartreuse2 gray98 lawngreen NavajoWhite1 gray40  \
                SteelBlue3 LemonChiffon3 DarkSlateGray LimeGreen gray14 gray97 aquamarine3  \
                snow4 grey26 tomato cyan4 SlateGray2 grey98 tomato1 OliveDrab2 darksalmon  \
                grey34 gray64 coral2 coral3 salmon VioletRed lightblue darkgreen  \
                palegoldenrod AntiqueWhite4 grey31 grey56 AntiqueWhite1 grey59 limegreen  \
                DarkSeaGreen2 grey5 orchid3 grey92 aquamarine4 VioletRed2 SpringGreen4  \
                lightgoldenrodyellow turquoise magenta3 aliceblue IndianRed maroon4  \
                thistle4 powderblue IndianRed2 LightYellow1 LightPink2 DarkGoldenrod2  \
                coral4 grey19 gray86 gray48 MediumPurple3 SeaGreen LightSkyBlue4 brown4  \
                LightSteelBlue1 indianred whitesmoke LightSalmon4 peachpuff purple grey16  \
                PaleVioletRed gray0 gray brown1 PaleTurquoise3 SlateBlue2 gray23 violetred  \
                lightpink LightGray DarkSeaGreen4 blue3 purple1 DarkViolet  \
                mediumspringgreen grey11 red grey79 ForestGreen GreenYellow grey24 grey32  \
                AntiqueWhite2 SlateGray4 DarkGoldenrod gray17 DarkOrange4 gray59 goldenrod2  \
                sienna2 tan3 SlateBlue3 DarkKhaki grey28 grey43 gray37 cadetblue SteelBlue2  \
                green3 CadetBlue4 grey33 yellow4 ivory SlateGray firebrick plum grey8  \
                darkkhaki lightyellow SkyBlue2 gray79 DarkGrey grey70 sandybrown  \
                DeepSkyBlue3 gray63 BlanchedAlmond bisque3 gray2 honeydew4 SeaGreen3  \
                olivedrab LightSlateGray khaki4 gray16 lightsalmon MistyRose2  \
                DarkGoldenrod1 sienna4 paleturquoise gray52 burlywood grey100  \
                PaleVioletRed1 SlateBlue ivory2 antiquewhite BlueViolet SeaGreen4 gray80  \
                gray38 PowderBlue OrangeRed1 magenta navy grey7 grey83 LemonChiffon2 gray46  \
                SkyBlue4 LightBlue3 tan gray73 lavenderblush grey9 burlywood3  \
                DarkSlateGray4 orangered aquamarine2 midnightblue blueviolet darkslateblue  \
                LightSalmon2 deepskyblue HotPink1 gray11 IndianRed4 gray26 SaddleBrown  \
                DarkSeaGreen RosyBrown3 SlateGray1 oldlace darkslategray FloralWhite  \
                LawnGreen orange3 gray61 gray51 LightSkyBlue2 HotPink3 LightGoldenrod1  \
                SlateGray3 RoyalBlue palevioletred MediumSlateBlue brown lightcoral DimGrey  \
                HotPink2 green2 ivory3 SkyBlue LightBlue2 lightslateblue cyan2  \
                LightSkyBlue1 grey27 grey84 DeepPink4 beige MediumOrchid3 cornflowerblue  \
                PaleVioletRed3 mediumaquamarine grey37 gray49 gray62 lemonchiffon gray96  \
                grey57 cornsilk1 green4 cyan MediumPurple2 gray75 SlateGrey grey10 purple2  \
                RoyalBlue1 MidnightBlue aquamarine1 gray1 OrangeRed4 grey40 thistle3  \
                OrangeRed3 DarkOliveGreen1 MediumBlue WhiteSmoke pink1 ghostwhite gray32  \
                hotpink chartreuse grey69 VioletRed4 khaki3 blue1 grey22 mediumorchid  \
                LightPink3 honeydew MediumOrchid darkslategrey thistle2 tomato2 navajowhite  \
                LightBlue1 salmon1 grey41 PaleGreen1 CadetBlue2 gray15 gray28 DodgerBlue4  \
                grey12 grey65 SpringGreen firebrick1 salmon3 LightCyan4 DeepPink1 brown3  \
                firebrick4 PapayaWhip DodgerBlue1 gray60 grey23 YellowGreen DeepSkyBlue2  \
                gray66 saddlebrown mistyrose yellow1 plum2 tomato3 DarkMagenta  \
                mediumseagreen LightYellow4 gray7 orange1 goldenrod4 grey25 mediumpurple  \
                grey17 green1 maroon1 LightCyan2 gray57 salmon2 OliveDrab1 coral OldLace  \
                DarkSalmon bisque2 SteelBlue PaleVioletRed2 grey15 grey36 MediumPurple  \
                grey72 DeepSkyBlue4 chartreuse3 PaleTurquoise1 gray9 CadetBlue3  \
                PaleTurquoise4 OliveDrab CadetBlue1 DarkCyan DarkOrange2 turquoise4  \
                chocolate1 grey78 VioletRed1 grey87 grey13 gold4 DarkGoldenrod3 gray90  \
                LightGrey LightCyan gray21 linen grey20 azure4 LightSteelBlue3 VioletRed3  \
                green grey52 LightGoldenrodYellow steelblue SpringGreen2 burlywood4  \
                LightSalmon1 lightslategray grey90 grey60 gray4 goldenrod1 cornsilk4 red3  \
                DarkSlateBlue yellow3 slateblue DeepPink3 SlateBlue1 LightSeaGreen thistle1  \
                gray88 grey66 gold1 darkgray gray82 SteelBlue1 LightGreen khaki2 chocolate4  \
                LightPink gray71 floralwhite grey1 peru gold2 grey42 dimgray NavyBlue  \
                PeachPuff grey61 gray81 white plum4 burlywood1 grey75 NavajoWhite2  \
                LemonChiffon1 pink LavenderBlush4 gray27 gray89 azure1 LemonChiffon gray29  \
                gray39 blue MediumOrchid4 MediumPurple4 grey29 grey30 grey53 CadetBlue  \
                DarkOrchid2 DarkSlateGray2 gray8 grey47 darkturquoise grey85 LightBlue4  \
                NavajoWhite LightYellow2 AliceBlue khaki DarkOliveGreen3 maroon grey91  \
                gray83 snow3 blanchedalmond cornsilk2 PaleGreen4 DarkSlateGray3 deeppink  \
                gray33 grey94 snow1 LightPink1 grey LightCoral wheat4 blue4 PaleGreen2  \
                orchid4 mediumturquoise seagreen NavajoWhite3 gray53 darkgoldenrod  \
                DodgerBlue3 cyan3 DarkSeaGreen3 grey18 darkblue orchid1 grey51 sienna1  \
                LightSteelBlue turquoise1 DarkGreen darkmagenta grey63 grey44 grey55  \
                lightskyblue AntiqueWhite3 darkred grey50 LightSkyBlue LightGoldenrod2  \
                turquoise3 seashell4 grey80 LightSlateBlue wheat3 gray56 violet ivory4  \
                RoyalBlue2 grey21 lightgoldenrod SkyBlue1 magenta4 gray55 khaki1 grey81  \
                grey0 gray77 grey99 gray35 mediumslateblue DarkSlateGray1 MediumOrchid2  \
                LightGoldenrod4 grey67 sienna firebrick2 PaleVioletRed4 gray36 grey6  \
                purple3 gainsboro red2 maroon3 SeaGreen1 wheat2 cornsilk3 pink3 orange  \
                PeachPuff2 salmon4 gray76 goldenrod IndianRed1 azure2 darkviolet purple4  \
                MediumTurquoise grey68 LavenderBlush LightCyan3 gray67 DarkTurquoise gray94  \
                gray25 LightSlateGrey MediumSeaGreen PaleGoldenrod CornflowerBlue gray43  \
                lightgray LavenderBlush2 SpringGreen1 coral1 lightgrey DeepSkyBlue1 orchid  \
                LightSteelBlue4 snow2 grey49 gray74 DarkBlue gray99 tan2 skyblue OrangeRed2  \
                LightBlue cornsilk grey82 gray87 MediumPurple1 plum3 PeachPuff3 DeepPink  \
                gray13 MediumSpringGreen SandyBrown DarkOrange slategray grey97 burlywood2  \
                rosybrown seashell3 navyblue MistyRose LightGoldenrod3]  


    
    if {$iocctype >= [llength $colors]} {
        puts "[myname] *********************************************** $iocctype"
        puts "[myname] *********************************************** [llength $colors]"
	error "[myname] to less colors (to many occtypes)"
    }	
    
    return [lindex $colors $iocctype]
    
}

#
########################################################################################
# compare with proc zlevel_to_layer_type
#
proc intensify_levels {canv {mode shade}} {

    # collect all available zlevelX
    set zlevel_tags [list]
    foreach id [$canv find withtag atom] {
	set tags [$canv gettags $id]
	if {[regexp {zlevel([0-9]*)} $tags zlevel zlevelnr] == 1} {
	    #puts $zlevel
	    lappend zlevel_tags $zlevel
	}
    }  
    
    set zlevel_tags [lsort -uniq $zlevel_tags]  ; # :CAVEAT: "zlevel10" < "zlevel9"
    #puts $zlevel_tags

    set top_layer_tag  [lindex $zlevel_tags 0]
    set bot_layer_tag  [lindex $zlevel_tags end]
    set mid_layer_tags [lrange $zlevel_tags 1 end-1]
    
    #puts $top_layer_tag
    #puts $bot_layer_tag
    #puts $mid_layer_tags
    
    intensify_level $canv "$top_layer_tag" $mode top
    foreach mid_layer_tag $mid_layer_tags {
	intensify_level $canv "$mid_layer_tag" $mode mid
    }
    intensify_level $canv "$bot_layer_tag" $mode bot
    
}


#
########################################################################################
# different modes to clarify the level of the atom layers
#
# zleveltag:   tag of the corresponding atoms e.g. zlevel0
# mode : shade stipple outline
# level: top mid(dle) bot(tom)
#
proc intensify_level {canv zleveltag mode level} {

    # check arguments once and eventually abort (no defaults in switches below)
    if {![lcontain [list shade stipple outline] $mode]} {
	error "unknown mode $mode"
    }
    
    if {![lcontain [list top mid bot] $level]} {
	error "unknown level $level" 
    }
    
    foreach id [$canv find withtag "atom&&$zleveltag"] {
	
	switch -exact $mode {

	    shade {
		
		# slightly change top und bottom layer color 
		# darken brighten
		global IOCCTYPE_IQBOX SEL_IQBOX
		set IQBOX [get_iqbox_from_tags $canv $id] 
		set color [map_iocctype_to_color [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)]] 
		
		switch -exact $level {
		    top { set color [::tk::Darken $color 110] } 
		    mid { }
		    bot { set color [::tk::Darken $color 90] }
		}

		# outline handling
		if {$SEL_IQBOX($IQBOX) == 1} {
		    set color2 black
		} else {
		    set color2 $color
		}
		
		set cfgargs "-fill $color -outline $color2" 

	    }	    
	    
	    stipple {
		# stippling
		#.canvas create rect 10 10 80 80 -fill black -stipple @/path/to/my.bmp
		switch -exact $level {
		    top { set stipple gray25 }
		    mid { set stipple gray50 }
		    bot { set stipple gray75 }
		}

		set cfgargs "-stipple $stipple"
		
	    }
	    
	    outline {
		# thickness (not visible when -outline color == -fill color)  
		switch -exact $level {
		    top { set width 1 }
		    mid { set width 2 }
		    bot { set width 3 }
		}
		
		set cfgargs "-width $width"
		
	    }
	    
	}
	
	#puts "$canv itemconfigure $id $cfgargs" 
	eval $canv itemconfigure $id $cfgargs
    }
    
}


#
########################################################################################
#
proc find_mindist {vecs} {
    
    set n [llength $vecs]
    
    set mindist [euclideandistance [lindex $vecs 0] [lindex $vecs 1]]
    
    for {set i 0} {$i < $n} {incr i} {
	set avec [lindex $vecs $i]
	for {set j [expr {$i+1}]} {$j < $n} {incr j} {
	    set dist [euclideandistance $avec [lindex $vecs $j]]
	    if {$dist < $mindist} {
		set mindist $dist
	    }
	}
    }
    
    return $mindist
}

#
########################################################################################
# euclidische distanz von avec und bvec
#
proc euclideandistance {avec bvec} {
    
    set sum 0.0
    foreach a $avec b $bvec {
	set val [expr {$a-$b}]
	set sum [expr {$sum+$val*$val}]
    }	
    
    return [expr {sqrt($sum)}]
    
}

  
#
########################################################################################
# 
proc occupy_cluster_atoms {canv top_layer {layers_to_draw 3}} {
#
#  create Z               X |              
#                           |			
#                           o ---- Y       	
#      view along          +Z                   
#                            
#----- note for plotting on the screen the role of X and Y is interchanged 
#                           the sign of the vertical coordinate X is reversed
#
    global NQBOX NLAYBOX ILAY_SEL RQBOX IQBOX1_ILAY IQBOX2_ILAY PN_ILAY IQ_IQBOX N_IQBOX
    global IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE
    global canvas_largest_circle_radius  canvas_smallest_circle_radius canvas_scale
    global iqboxs zlevels
    
    set PRC [myname]

    set dbg 1
  
    set bottom_layer [expr {$top_layer + $layers_to_draw - 1}]
    if {$bottom_layer > $NLAYBOX} { set bottom_layer $NLAYBOX }

    # extract x/y/z info for atoms of layers
    set xs [list]
    set ys [list]
    set zlevels [list]
    set iqboxs [list]
    set colors [list]
    set selections [list]
    set labels [list]
    for {set ILAY $top_layer} {$ILAY <= $bottom_layer} {incr ILAY} {
	for {set IQBOX $IQBOX1_ILAY($ILAY)} {$IQBOX <= $IQBOX2_ILAY($ILAY)} { incr IQBOX } {
##	    lappend xs $RQBOX(1,$IQBOX)
##	    lappend ys $RQBOX(2,$IQBOX) 
	    lappend xs [expr +$RQBOX(2,$IQBOX)]
	    lappend ys [expr -$RQBOX(1,$IQBOX)]
	    lappend zlevels $ILAY  
	    lappend iqboxs $IQBOX
	    set iocctype [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)]
	    lappend colors [map_iocctype_to_color $iocctype] 
	    lappend labels $OCCTYPE($iocctype) 
	    lappend selections $SEL_IQBOX($IQBOX)
	}
    }
    
    if $dbg {
	#debug $PRC "xs: $xs"
	#debug $PRC "ys: $ys"
	debug $PRC "zlevels = $zlevels" 
	debug $PRC "labels = $labels"
	debug $PRC "colors = $colors"
	debug $PRC "NLAYBOX = $NLAYBOX"
    }
    
    # largest circle is used in bottom level, compute minimum distance
    # to compute scalefactor
    set vecs [list]
    foreach x $xs y $ys zlevel $zlevels {
	if {$zlevel == $bottom_layer} {
	    lappend vecs [list $x $y]
	}
    }
    
    if {[llength $vecs] >= 2} {
	set min_dist_bottom_layer [find_mindist $vecs]
    } else {
	# workaround for pathological cases with only single atom in a layer 
	set min_dist_bottom_layer 0.5
    }
    
    if $dbg { debug $PRC "min_dist_bottom_layer: $min_dist_bottom_layer" }
    
    # atom circle drawing parameters
    set outer_circle_border    5
    set inner_circle_border    10
    set smallest_circle_radius 10
    
    # prepare list of radii from smallest_circle_radius and inner_circle_border
    set level_radii [list]
    for {set i 0} {$i < $layers_to_draw} {incr i} {
	lappend level_radii [expr {$smallest_circle_radius + $i*$inner_circle_border}]
    }
    set largest_circle_radius [lindex $level_radii end]

    # compute scalefactor to draw atoms in the required distance
    set scale [expr {($outer_circle_border+2*$largest_circle_radius)/$min_dist_bottom_layer}]
        
    set canvas_largest_circle_radius   $largest_circle_radius
    set canvas_smallest_circle_radius  $smallest_circle_radius
    set canvas_scale                   $scale

    draw_atoms $canv $scale $xs $ys $iqboxs $colors $zlevels $selections $level_radii $labels
    
    intensify_levels $canv 
    center_canvas_items $canv
    adjust_scrollregion $canv

}

########################################################################################
# 

proc occupy_cluster_atoms_XY {canv_X canv_Y} {
#    
#  create X and Y plots   Z |                         Z |            
#                           |			        |            
#                           o ---- Y       	        o ---- X     
#      view along          -X                           Y                   
#                            
#----- note for plotting on the screen the sign of the vertical coordinate Z is reversed
#
    global NQBOX NLAYBOX ILAY_SEL RQBOX IQBOX1_ILAY IQBOX2_ILAY PN_ILAY IQ_IQBOX N_IQBOX
    global IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE 
    global L_BOX actual_top_layer
    global canvas_largest_circle_radius canvas_smallest_circle_radius canvas_scale
 
    set PRC [myname]

    $canv_X  delete all 
    $canv_Y  delete all 

    set dbg 1

    # extract x/y/z info for atoms of layers
    set xs [list]
    set ys [list]
    set zs [list]

    set xlevels [list]
    set ylevels [list]

    set iqboxs [list]
    set colors [list]
    set selections [list]
    set labels [list]

    set xmin +10000000000000
    set ymin +10000000000000
    set xmax -10000000000000
    set ymax -10000000000000

    set layers_to_draw 3
    set bot_layer [expr {$actual_top_layer + $layers_to_draw - 1}]
    if {$bot_layer > $NLAYBOX} { set bot_layer $NLAYBOX }
    set z_ILAY_SHOW [list]

    for {set ILAY $actual_top_layer} {$ILAY <= $bot_layer} {incr ILAY} {
        set IQBOX $IQBOX1_ILAY($ILAY)
        set i [expr $ILAY - $actual_top_layer + 1]
        if {$i >= 1  && $i <= 3 } {
	    lappend z_ILAY_SHOW [expr -$RQBOX(3,$IQBOX)]
        }
    }

    set bot_layer      1
    set top_layer      $NLAYBOX
    set layers_to_draw $NLAYBOX
    set z_ILAY      [list]
    set IQ_list     [list]

    for {set ILAY $bot_layer} {$ILAY <= $top_layer} {incr ILAY} {        
        set IQBOX $IQBOX1_ILAY($ILAY)
        lappend z_ILAY [expr -$RQBOX(3,$IQBOX)]
        if {$ILAY == $ILAY_SEL} {
	    set z_ILAY_SEL [expr -$RQBOX(3,$IQBOX)]
        }
	for {set IQBOX $IQBOX1_ILAY($ILAY)} {$IQBOX <= $IQBOX2_ILAY($ILAY)} { incr IQBOX } {
	    lappend IQ_list $IQ_IQBOX($IQBOX)

	    lappend xs        $RQBOX(1,$IQBOX)
	    lappend ys        $RQBOX(2,$IQBOX) 
	    lappend zs [expr -$RQBOX(3,$IQBOX)] 
	    lappend xlevels   $RQBOX(1,$IQBOX)  
	    lappend ylevels   $RQBOX(2,$IQBOX)  

            if { $RQBOX(1,$IQBOX) < $xmin } {set xmin $RQBOX(1,$IQBOX)}
            if { $RQBOX(2,$IQBOX) < $ymin } {set ymin $RQBOX(2,$IQBOX)}
            if { $RQBOX(1,$IQBOX) > $xmax } {set xmax $RQBOX(1,$IQBOX)}
            if { $RQBOX(2,$IQBOX) > $ymax } {set ymax $RQBOX(2,$IQBOX)}
            
	    lappend iqboxs $IQBOX
	    set iocctype [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)]
	    lappend colors [map_iocctype_to_color $iocctype] 
	    lappend labels $OCCTYPE($iocctype) 
	    lappend selections $SEL_IQBOX($IQBOX)
	}
    }
    
#    if $dbg {
#	#debug $PRC "xs: $xs"
#	#debug $PRC "ys: $ys"
#	debug $PRC "xlevels = $xlevels" 
#	debug $PRC "ylevels = $ylevels" 
#	debug $PRC "labels = $labels"
#	debug $PRC "colors = $colors"
#	debug $PRC "NLAYBOX = $NLAYBOX"
#    }


    # largest circle is used in bottom level, compute minimum distance
    # to compute scalefactor
    set radii_x [list]
    set radii_y [list]
    set radius [expr 0.5 * ($canvas_largest_circle_radius\
                          + $canvas_smallest_circle_radius) ]
    foreach x $xs y $ys z $zs {
       lappend radii_x [list $radius]
       lappend radii_y [list $radius]
    }


    draw_atoms_XY $canv_X $canv_Y $canvas_scale $xs $ys $zs $iqboxs \
                  $colors $xlevels $ylevels $selections $radii_x $radii_y \
                  $labels $z_ILAY $z_ILAY_SEL $z_ILAY_SHOW \
                  $IQ_list $xmin $xmax $ymin $ymax 
    
#    intensify_levels    $canv_X 
    center_canvas_items $canv_X
    adjust_scrollregion $canv_X

#    intensify_levels    $canv_Y
    center_canvas_items $canv_Y
    adjust_scrollregion $canv_Y

}


#
########################################################################################
#
proc center_canvas_items {canvas} {

    set dbg 1

    set bbox [$canvas bbox all]
    
    if {[llength $bbox] > 0} {
	lassign $bbox x0 y0 x1 y1 
	
	set midx [expr {0.5*($x1+$x0)}]  
	set midy [expr {0.5*($y1+$y0)}]

	if {[winfo ismapped $canvas]} {
	    set cwidth  [winfo width  $canvas]
	    set cheight [winfo height $canvas]
	} else {
	    set cwidth  [$canvas cget -width]
	    set cheight [$canvas cget -height]
	}
	
	set dx [expr {(0.5* $cwidth) - $midx}]
	set dy [expr {(0.5*$cheight) - $midy}]

	$canvas move all $dx $dy

	if $dbg { debug "[myname]" "cw: $cwidth ch: $cheight dx: $dx dy: $dy" }

    } 
    
}

#
########################################################################################
#
proc adjust_scrollregion {canv {border 10}} {
    
    set bbox [$canv bbox all]
    
    if {$bbox != ""} {
	lassign $bbox x0 y0 x1 y1
	
	set x0 [expr {$x0 - $border}]
	set y0 [expr {$y0 - $border}]
	set x1 [expr {$x1 + $border}]
	set y1 [expr {$y1 + $border}]
	
	$canv configure -scrollregion [list $x0 $y0 $x1 $y1]
    }
}

#
########################################################################################
#
proc add_occupation_type {} {

    global NOCCTYPE OCCTYPE IOCTYPE_IO
    global Wocc pass_NOCC TXTOCC ZOCC CONCOCC
    global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE
    
    # sites_occupation $IQ $NOCC $simple_occ_handling_mode
    sites_occupation "new" 1 1     
    tkwait window $Wocc
    
    # close button pressed in sites_occupation ; means break 
    if {$pass_NOCC == 0} {
	return ""
    }
    
    # build labelstring for new OCCTYPE
    set label "" 
    for {set i 1} {$i <= $pass_NOCC} {incr i} {
	set TXT $TXTOCC($i)
	set CNC $CONCOCC($i)
	
	append label $TXT 
	if {$CNC < 1.0} {append label $CNC}

	#puts "NOCC: $pass_NOCC i: $i TXT: $TXT CNC: $CNC label: $label"
	
    }
    
    # check if OCCTYPE is really new
    set found 0 
    for {set i 1} {$i <= $NOCCTYPE } {incr i} {
	if {$label == $OCCTYPE($i)} {
	    set found 1
	}
    }

    debug [myname] "OCCTYPE: $label"
    
    if {$found} {
	# here we would need a call to proc deal_with_names_of_atom_types
	# for now its not allowed
	puts "choosen occupation type is allready listed"
	set label ""
    } else {
	incr NOCCTYPE
	set OCCTYPE($NOCCTYPE) $label

        debug "[myname]" "occupation type IOCCTYPE=$NOCCTYPE new !!!!!!!!!!!!!!!!!!!"
        set NOCC_OCCTYPE($NOCCTYPE) $pass_NOCC
        for {set IO 1} {$IO <= $pass_NOCC} {incr IO} {
           set    Z_OCCTYPE($IO,$NOCCTYPE)    $ZOCC($IO)
	   set  TXT_OCCTYPE($IO,$NOCCTYPE)  $TXTOCC($IO) 
   	   set CONC_OCCTYPE($IO,$NOCCTYPE) $CONCOCC($IO)
           debug "[myname]" "occupant $IO: Z=$ZOCC($IO) TXT0=$TXTOCC($IO) CONC=$CONCOCC($IO) "
        }

    }    
    
    return $label
    
}

#
########################################################################################
#
proc reset_occupation_types {canv} {
    
#---------------------------------------------------------------------------- NEW
    global run_geometry_task
    global L_BOX  L_BOX_SAV

    for {set i 1} {$i <= 3} {incr i} { set L_BOX_SAV($i) $L_BOX($i) }

    set run_geometry_task cr_rec_regime

    run_geometry $run_geometry_task

    create_system_0D_arb_clu

#---------------------------------------------------------------------------- OLD
    global NQBOX  NQRELAX
    
    for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
	change_iocctype $canv $IQBOX -3 1   ; # last arg: suppress_intensify
    } 

    # above suppressed, now do it for all
    intensify_levels $canv
 
    set NQRELAX 0
  
    change_atom_layers $canv 0 
}

#
########################################################################################
#
proc prepare_occupation_types {} {

    set dbg 1

    global NQ NOQ ITOQ ZT TXTT0 CONC
    global NOCCTYPE OCCTYPE IOCCTYPE_IQ
    global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE

    set occupation_types [list]
    set IOCCTYPE 0
    
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	
	set label ""
	for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
	    
	    set IT  $ITOQ($IO,$IQ)
	    set TXT $TXTT0($IT)
	    set CNC $CONC($IT)
	    
	    append label $TXT 
	    if {$CNC < 1.0} {append label $CNC}
	    
	}
	
	set idx [lsearch -exact $occupation_types $label]
	if {$idx == -1} {
	    # new occupation_type detected
	    lappend occupation_types $label
	    incr IOCCTYPE
	    set OCCTYPE($IOCCTYPE) $label
	    set IOCCTYPE_IQ($IQ)  $IOCCTYPE 

            debug "[myname]" "occupation type IOCCTYPE=$IOCCTYPE from site IQ=$IQ"
            set NOCC_OCCTYPE($IOCCTYPE) $NOQ($IQ)
            for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
	       set IT     $ITOQ($IO,$IQ)
               set    Z_OCCTYPE($IO,$IOCCTYPE)    $ZT($IT)
               set  TXT_OCCTYPE($IO,$IOCCTYPE) $TXTT0($IT) 
	       set CONC_OCCTYPE($IO,$IOCCTYPE)  $CONC($IT) 
               debug "[myname]" "occupant $IO: Z=$ZT($IT) TXT0=$TXTT0($IT) CONC=$CONC($IT) "
	    }

	} else {
	    set IOCCTYPE_IQ($IQ) [expr {$idx + 1}]
	}

    }
    
    set NOCCTYPE $IOCCTYPE
    
    if $dbg {
	debug "[myname]" "occupation_types: $occupation_types"
    }
    
    return $occupation_types
    
}


#
########################################################################################
# IOCCTYPE_IQBOX($IQBOX) = $IOCCTYPE
#      SEL_IQBOX($IQBOX) = 0          (because IOCCTYPE is initially "unchanged")
#
proc init_iqbox_globals {} {

    global NQBOX IQ_IQBOX IOCCTYPE_IQ
    global IOCCTYPE_IQBOX SEL_IQBOX
    
    for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
	set IQ   $IQ_IQBOX($IQBOX)
	set IOCC $IOCCTYPE_IQ($IQ)
	set IOCCTYPE_IQBOX($IQBOX) [list $IOCC]
	set      SEL_IQBOX($IQBOX) 0
    }
    
}


#
#    canvas selection    change IOCCTYPE_IQBOX(IQBOX) accordingly and change SEL_IQBOX(IQBOX) to 1
#

#bind .listbox <ButtonRelease-1> { set my_global [%W get [%W nearest %y]] }
#bind .listbox <Double-Button-1> {+ destroy %W; break }

#
########################################################################################
#    create_system_0D_arb_clu_done     write the new system file 
#
proc create_system_0D_arb_clu_done {w} {

global NQBOX IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE
global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE
global sysfile syssuffix sysfile_new list_ALPHA 

#
#-------------------------- tabulate the cluster geometry data
#

    create_system_0D_arb_clu_tabulate

#
#-------------------------- check whether system file exists
#

    set ff $sysfile$syssuffix

    set ext "-0D_"

    set sysfile_new "${sysfile}${ext}a"
    set ff_new      ${sysfile_new}$syssuffix
    set IALPH -1

    while {[file exists $ff_new]!=0} {
       incr IALPH
       set sysfile_new "${sysfile}${ext}[lindex $list_ALPHA $IALPH]"
       set ff_new       ${sysfile_new}$syssuffix
    }

    set cw $w.confirm
    if {[winfo exists $cw] == 1} {destroy $cw}
    toplevel_init $cw " " 0 0

    wm geometry $cw +300+50
	
    wm title    $cw "confirm action"
    wm iconname $cw "confirm action"

    set files "${sysfile}${syssuffix}"

    if {[file exists "${files}"]} {

       eval set res [catch "exec ls -l ${files}" message]

       label $cw.old -text "the parent system file   $ff  exists \n\n$message" \
            -pady 10  -padx 30 -anchor w -justify left

       frame $cw.new 
       label $cw.new.lab -text "new system file" -pady 10 -anchor w -justify left
       entry $cw.new.ent -width 30 -relief sunken -textvariable sysfile_new \
             -bg lightblue 
       pack  $cw.new.lab $cw.new.ent -side left
       button $cw.store -text "store new sysfile" \
          -width 50 -height 5 -bg green \
          -command "create_system_0D_arb_clu_store $w"
       button $cw.close -text "dismiss" -width 50 -height 5 -bg red \
           -command "destros $cw"
       pack $cw.old $cw.new  $cw.store $cw.close

   } else {

      give_warning "." "WARNING \n\n parent system file \n\n ${files} \
                                    \\  DOES NOT EXIST   \n "
      return

   }

}

#
########################################################################################
#
proc create_system_0D_arb_clu_store {w} {

global sysfile syssuffix sysfile_new CLUSTER_TYPE sysfile_host

    set PRC [myname]
    set dbg 1

    set ff ${sysfile_new}${syssuffix}
    
    if $dbg { debug $PRC "sysfile_new: $ff" }

    if {[file exists "$ff"]} {
        give_warning "." "WARNING \n\n from <create_system_0D_arb_clu_store>  \n\n \
                      a file with the name \n\n\n  $ff \n\n\n already exists \
                      \n\n >>> choose a different name  \n "
        return
    }

    destros $w

#
#----------------------------------- finally store NEW sysfile

    set sysfile_host  ${sysfile}
    set sysfile       ${sysfile_new}

    set CLUSTER_TYPE "embedded"

    write_sysfile  

}
#
########################################################################################
#    create_system_0D_arb_clu_tabulate     tabulate the cluster geometry data
#
proc create_system_0D_arb_clu_tabulate { } {

global NQBOX IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE
global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE
global NQCLU IQ_IQCLU N5VEC_IQCLU RQCLU NOCC_IQCLU Z_IQCLU TXT_IQCLU CONC_IQCLU
global IQ_IQBOX N_IQBOX RQBOX

   puts "\n\n **********************************************************"
   puts     " ***************** NEW CLUSTER TABLE **********************"
   puts     " **********************************************************"

   set NQCLU 0

   for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {

      set SEL $SEL_IQBOX($IQBOX)

      if {$SEL>0} {

         incr NQCLU
         set IQCLU $NQCLU

         set IQ               $IQ_IQBOX($IQBOX)
         set IQ_IQCLU($IQCLU) $IQ
         for {set I 1} {$I <= 5} {incr I} {
            set N5VEC_IQCLU($I,$IQCLU) $N_IQBOX($I,$IQBOX)
         }
         for {set I 1} {$I <= 3} {incr I} {
            set RQCLU($I,$IQCLU) $RQBOX($I,$IQBOX)
         }

         set IOCCTYPE [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL]

         set NOCC_IQCLU($IQCLU) $NOCC_OCCTYPE($IOCCTYPE) 

         set TXT ""
         for {set IO 1} {$IO <= $NOCC_OCCTYPE($IOCCTYPE)} {incr IO} {
            set Z    $Z_OCCTYPE($IO,$IOCCTYPE)
            set T  $TXT_OCCTYPE($IO,$IOCCTYPE)
            set C $CONC_OCCTYPE($IO,$IOCCTYPE)

            set         Z_IQCLU($IO,$IQCLU) $Z
            set       TXT_IQCLU($IO,$IQCLU) $T
            set      CONC_IQCLU($IO,$IQCLU) $C

            set TXT "$TXT $Z $T $C "
         }

         puts "$IQBOX $SEL_IQBOX($IQBOX) $IOCCTYPE $TXT"

      }

   }

   puts     " **********************************************************\n"

   if {$NQCLU<1} {
       give_warning "." "WARNING \n\n from <create_system_0D_arb_clu_tabulate>  \n\n \
                     no cluster sites were selected \n\n NQCLU = 0  \n "
       return
   }


}
	
#
########################################################################################
#    create_system_0D_arb_clu_relax     add relaxation zone
#
proc create_system_0D_arb_clu_relax { } {

global NQBOX IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE
global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE
global NQCLU IQ_IQCLU N5VEC_IQCLU RQCLU NOCC_IQCLU Z_IQCLU TXT_IQCLU CONC_IQCLU
global IQ_IQBOX N_IQBOX RQBOX
global NSHLRELAX RELAXRAD NQRELAX NQSEL
global create_system_0D_arb_clu_canvas_Z actual_top_layer 
global create_system_0D_arb_clu_canvas_X create_system_0D_arb_clu_canvas_Y

    puts "add relaxation zone invoked"

    run_geometry cr_relax_zone

    #
    set actual_top_layer 1
    occupy_cluster_atoms $create_system_0D_arb_clu_canvas_Z $actual_top_layer 

    change_atom_layers $create_system_0D_arb_clu_canvas_Z +1
    change_atom_layers $create_system_0D_arb_clu_canvas_Z -1

    occupy_cluster_atoms_XY $create_system_0D_arb_clu_canvas_X \
                            $create_system_0D_arb_clu_canvas_Y
}


#
########################################################################################
#    create_system_0D_arb_clu_center       center the atom cluster 
#
proc create_system_0D_arb_clu_center { } {

global NQBOX IOCCTYPE_IQBOX SEL_IQBOX OCCTYPE
global NOCC_OCCTYPE Z_OCCTYPE TXT_OCCTYPE CONC_OCCTYPE
global NQCLU IQ_IQCLU N5VEC_IQCLU RQCLU NOCC_IQCLU Z_IQCLU TXT_IQCLU CONC_IQCLU
global IQ_IQBOX N_IQBOX RQBOX
global NSHLRELAX RELAXRAD NQRELAX NQSEL
global create_system_0D_arb_clu_canvas_Z actual_top_layer 
global create_system_0D_arb_clu_canvas_X create_system_0D_arb_clu_canvas_Y

    puts "center the atom cluster invoked"

    run_geometry center_cluster

    #
    set actual_top_layer 1
    occupy_cluster_atoms $create_system_0D_arb_clu_canvas_Z $actual_top_layer 

    change_atom_layers $create_system_0D_arb_clu_canvas_Z +1
    change_atom_layers $create_system_0D_arb_clu_canvas_Z -1

    occupy_cluster_atoms_XY $create_system_0D_arb_clu_canvas_X \
                            $create_system_0D_arb_clu_canvas_Y
}
