# run programs
#
#
# Copyright (C) 2001 H. Ebert

proc run_programs { PROGRAM_PACKAGE } {

########################################################################################
 proc prog_sel_d {d} {
     global Wprog
     update
     prog_focus $Wprog
     $Wprog.c.d.f.f.li delete 0 end
     update
     dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default
    global Wprog working_directory inpfile liste
    global jobsuffixlist jobfile inpsuffixlist
    global outsuffixlist outfile sysdirlist
    
    set cdir [expand_dollar_home $d]
    
    if {[file isdirectory $cdir]==0} {
	writescr0 .d.tt "\n the directory  $cdir  does not exist"
	give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
	return
    }
    
    if [winfo exists $Wprog] {set w $Wprog.d.tt} else {set w .d.tt}
    
    if {[catch {cd $cdir} m]!=0} {
	set working_directory [pwd]
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	writescr0 $w "new directory chosen: [pwd]\n\n"
	prog_refresh_top_panel
	set working_directory [pwd]
	set inpfile ""
	set jobfile   "" 
	set outfile  "" 
	if {[winfo exists $Wprog]} {
	    prog_anzeige $Wprog
	    
	    $Wprog.c.d.f.f.li delete 0 end
	    $Wprog.c.d.f.f.li insert end "\$HOME" ".."
	    foreach x $sysdirlist { $Wprog.c.d.f.f.li insert end "$x"}
	    foreach k [lsort [glob -nocomplain -- *]] {
		if {[file isdirectory $k]} {$Wprog.c.d.f.f.li insert end $k}
	    }
	    lfuellen $Wprog.c.m.f.f.li $inpsuffixlist
	    lfuellen $Wprog.c.e.f.f.li $jobsuffixlist
	    lfuellen $Wprog.c.s.f.f.li $outsuffixlist
	}
	return 1
    }
}


########################################################################################
 proc prog_dir_update {} {
     global Wprog liste working_directory sysdirlist sysdirlistmaxindex

     set diraux [collapse_dollar_home $working_directory]
     
     if {[llength $sysdirlist]>0} {
	 set sysdirlist [update_dirlist $sysdirlist $sysdirlistmaxindex $diraux]
     } else {
	 lappend sysdirlist $diraux
     }
     $Wprog.c.d.f.f.li delete 0 end
     $Wprog.c.d.f.f.li insert end "\$HOME" ".."
     foreach x $sysdirlist { $Wprog.c.d.f.f.li insert end "$x"}
     
 }
 


########################################################################################
 proc prog_refresh_top_panel {} {
  global Wprog filter inpfile jobfile outfile
  global buttonlist_PROGS

  set filter   "*"
  set inpfile ""
  set jobfile ""
  set outfile ""

  foreach i $buttonlist_PROGS { $i configure -state disabled -bg grey }
                  $Wprog.b.c1.m.mb configure -state disabled -bg grey
                  $Wprog.b.c1.e.mb configure -state disabled -bg grey
                  $Wprog.b.c1.s.mb configure -state disabled -bg grey
 
  prog_anzeige $Wprog

 }


########################################################################################
 proc prog_sel_m {f} {
  global Wprog inpfile sorttype PROGPROG
  global buttonlist_PROGS
  prog_focus $Wprog
  if {[file exists $f]} {
    set inpfile $f
    writescr0 $Wprog.d.tt "new inpfile selected $inpfile\n\n"; prog_anzeige $Wprog
    foreach i $buttonlist_PROGS { $i configure -state normal -bg green1 }
    $Wprog.b.c1.m.mb configure -state normal -bg LightBlue
##    set    job [open "zzzzzz_job" w] 
##    puts  $job "INPSET    $f"
##    close $job
##    eval set res [catch "exec $PROGPROG < zzzzzz_job " message]
##    writescr  $Wprog.d.tt "$message \n"
##    eval set res [catch "exec rm zzzzzz_job" message]
    prog_sel_order $sorttype
    prog_dir_update
  }
 }

########################################################################################
 proc prog_sel_m2 {f} {
  global buttonlist_PROGS
  global Wprog inpfile
  global editor edback edxterm edoptions
  prog_focus $Wprog
  if {[file exists $f]} {
    set inpfile $f
    writescr0 $Wprog.d.tt "new input-file selected: $inpfile\n\n"; prog_anzeige $Wprog
    foreach i $buttonlist_PROGS { $i configure -state normal -bg green1 }
                    $Wprog.b.c1.m.mb configure -state normal -bg LightBlue
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    prog_dir_update
  }
 }


########################################################################################
 proc prog_sel_e {f} {
  global Wprog jobfile sorttype PROGPROG
  prog_focus $Wprog
  if {[file exists $f]} {
    set jobfile $f
    writescr0 $Wprog.d.tt "new job-file selected: $jobfile\n\n"; prog_anzeige $Wprog
    $Wprog.b.c1.e.mb configure -state normal -bg LightBlue
#    eval set res [catch "exec $PROGPROG < $f " message]
#    writescr  $Wprog.d.tt "$message \n"
    prog_dir_update
  }
 }

########################################################################################
 proc prog_sel_e2 {f} {
  global Wprog jobfile
  global editor edback edxterm edoptions
  prog_focus $Wprog
  if {[file exists $f]} {
    set jobfile $f
    writescr0 $Wprog.d.tt "new job-file selected: $jobfile\n\n"; prog_anzeige $Wprog
    $Wprog.b.c1.e.mb configure -state normal -bg LightBlue
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    prog_dir_update
  }
 }

########################################################################################
 proc prog_sel_s {f} {
  global Wprog outfile
  prog_focus $Wprog
  if {[file exists $f]} {
    set outfile $f
    writescr0 $Wprog.d.tt "new out-file selected: $outfile\n\n"; prog_anzeige $Wprog
    $Wprog.b.c1.s.mb configure -state normal -bg LightBlue
    eval set res [catch "exec out $f & " message]
    writescr  $Wprog.d.tt "$message \n"
    prog_dir_update
  }
 }

########################################################################################
 proc prog_sel_s2 {f} {
  global Wprog outfile
  global editor edback edxterm edoptions
  prog_focus $Wprog
  if {[file exists $f]} {
    set outfile $f
    writescr0 $Wprog.d.tt "new out-file selected: $outfile\n\n"; prog_anzeige $Wprog
    $Wprog.b.c1.s.mb configure -state normal -bg LightBlue
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    prog_dir_update
  }
 }

########################################################################################
 proc prog_sel_order {s} {
  global Wprog inpsuffixlist jobsuffixlist outsuffixlist sorttype

  if {$s=="alphabetical"} {set sorttype alphabetical} else {set sorttype access}
  lfuellen $Wprog.c.m.f.f.li $inpsuffixlist
  lfuellen $Wprog.c.e.f.f.li $jobsuffixlist 
  lfuellen $Wprog.c.s.f.f.li $outsuffixlist
 }

########################################################################################
 proc prog_dummy {s} { }


########################################################################################
 proc prog_focus {w} {
  global Wprog prog_foc
  if     {$prog_foc==0} {set r 0} \
  elseif {$prog_foc==1} {set r [prog_edn]} \
  elseif {$prog_foc==2} {set r [prog_emfn]} \
  elseif {$prog_foc==3} {set r [prog_eefn]} \
  elseif {$prog_foc==4} {set r [prog_esfn]}
  if {$w!=""} {focus $w} else {focus $Wprog; return $r}
 }

########################################################################################
 proc prog_edn {} {# enter directory name
     global Wprog prog_foc prog_d
     set prog_foc 0
     set d [string trim [$Wprog.c.d.i get]]
     $Wprog.c.d.i delete 0 end
     writescr0 $Wprog.d.tt ""
     
     if {$d==""} {return 0}
     set prog_d [expand_dollar_home $d]
     
     if {![file isdirectory $prog_d] && ![file exists $prog_d]} {
	 mkdcdfill
     } else {
	 dirselected_comp $prog_d
	 prog_anzeige $Wprog
     }
     return 1
 }
 
########################################################################################
 proc prog_emfn {} {#                                             enter inpfile name
  global Wprog inpsuffix inpfile jobfile outfile liste prog_foc

  set prog_foc 0
  writescr0 $Wprog.d.tt ""
  if {$f=="$inpsuffix"} {set f ""; writescr $Wprog.d.tt "filename cannot consist of file suffix only\n"}
##  regsub "$inpsuffix$" $f "" h; if {"$h$inpsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wprog.d.tt $inpsuffix;  if {$f==""} {return 0}

  set inpfile $f
  writescr $Wprog.d.tt "input of new inpfile name: $inpfile\n\n"
  prog_anzeige $Wprog
  return 1
 }


########################################################################################
 proc prog_eefn {} {#                                             enter job-file name
  global Wprog jobsuffix jobfile prog_foc

  set prog_foc 0
  writescr0 $Wprog.d.tt ""
  if {$f=="$jobsuffix"} {set f ""; writescr $Wprog.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${jobsuffix}$" $f "" h; if {"$h$jobsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wprog.d.tt $jobsuffix;  if {$f==""} {return 0}

  set jobfile $f
  writescr $Wprog.d.tt "input of new job-file name: $jobfile\n\n" 
  prog_anzeige $Wprog
  return 1
 }


########################################################################################
 proc prog_esfn {} {#                                          enter out file name
  global Wprog outsuffix outfile prog_foc

  set prog_foc 0
  writescr0 $Wprog.d.tt ""
  if {$f=="$outsuffix"} {set f ""; writescr $Wprog.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${outsuffix}$" $f "" h; if {"$h$outsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wprog.d.tt $outsuffix;  if {$f==""} {return 0}

  set outfile $f
  writescr $Wprog.d.tt "input of new out-file name $outfile\n\n" 
  prog_anzeige $Wprog
  return 1
 }

########################################################################################
proc editfile {f} {#                                              enter out file name
  global inpfile jobfile outfile
  global edxterm editor edoptions edback

  if     {$f=="m"} {set ff "$inpfile"}  \
  elseif {$f=="e"} {set ff "$jobfile"} \
  elseif {$f=="s"} {set ff "$outfile"} \
  else             {set ff ""}

  set ed_file $ff
  eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
  set pid "$message"
}

########################################################################################
proc do_with_file {f k} {#                                                       copy a file
  global inpfile jobfile outfile PRINTPS
  global inpsuffixlist jobsuffixlist outsuffixlist
  global Wprog key_cpmvdl file_cpmvdl 
  global edxterm editor edoptions edback
  global GREP1 GREP2 GREP3 
  
  set key_cpmvdl  $k
  set file_cpmvdl $f

  set cw $Wprog.cpmvdl_file

  if     {$f=="m"} {set ff "$inpfile"} \
  elseif {$f=="e"} {set ff "$jobfile"} \
  elseif {$f=="s"} {set ff "$outfile"} \
  else             {set ff ""}

  if {$k=="e"} { 
     set ed_file $ff
     eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
     set pid "$message"
     return 
  }

  if {$k=="l"} { execunixcmd "cat $ff" ; return }

  if {$k=="p"} {$PRINTPS $ff;  writescr  .d.tt "\n file $ff send to the printer \n" ; return }

#  if {$k=="t"} { execunixcmd "tail -f $ff" ; return }
  if {$k=="t"} {give_warning .  "WARNING \n\n the tail-command \n\n is not yet available " ; return }

  if {$k=="grep1"} { execunixcmd "grep \"$GREP1\" $ff" ; return }
  if {$k=="grep2"} { execunixcmd "grep \"$GREP2\" $ff" ; return }
  if {$k=="grep3"} { execunixcmd "grep \"$GREP3\" $ff" ; return }
  if {$k=="grep12"} { execunixcmd "egrep \"$GREP1|$GREP2\" $ff" ; return }


 
  set source $ff
  set target $ff
  
  toplevel_init $cw " " 0 0

  wm geometry $cw +300+50
 
  if       {$k=="c"} {
  wm title    $cw "Copy File"
  wm iconname $cw "Copy File"
  label $cw.head -text "modify source and/or target file name and press <RETURN> to copy" -height 2 -padx 0
  set TEXT1 "source file"
  set TEXT2 "copy"
  } elseif {$k=="m"} {
  wm title    $cw "Move File"
  wm iconname $cw "Move File"
  label $cw.head -text "modify source and/or target file name and press <RETURN> to move" -height 2 -padx 0
  set TEXT1 "source file"
  set TEXT2 "move"
  } else {
  wm title    $cw "Delete File"
  wm iconname $cw "Delete File"
  label $cw.head -text "modify file name and press <RETURN> to delete" -height 2 -padx 0
  set TEXT1 "file name"
  set TEXT2 "delete"
  }
  pack  $cw.head -anchor n -side top -fill x
  
  frame $cw.source -borderwidth 10
  pack  $cw.source -side top -fill x
  
  label $cw.source.l   -text "$TEXT1" -padx 0
  entry $cw.source.cmd -width 45 -relief sunken -textvariable source
  bind  $cw.source.cmd <Key-Return> {execute_cpmvdl}
        $cw.source.cmd delete 0 end
        $cw.source.cmd insert 0 $source
  pack  $cw.source.l   -side left
  pack  $cw.source.cmd -side right -fill x

  
  if       {$k!="d"} {
  frame $cw.target -borderwidth 10
  pack  $cw.target -side top -fill x
  
  label $cw.target.l   -text "target file" -padx 0
  entry $cw.target.cmd -width 45 -relief sunken -textvariable target
  bind  $cw.target.cmd <Key-Return> {execute_cpmvdl}
        $cw.target.cmd delete 0 end
        $cw.target.cmd insert 0 $target
  pack  $cw.target.l   -side left
  pack  $cw.target.cmd -side right -fill x
  }

 
  button $cw.execute -text "$TEXT2" -command execute_cpmvdl -height 2 -width 50 -bg green1
 		     
  button $cw.close -text Close  -command "destroy $cw"  -height 2 -width 50 -bg tomato1
 
  pack $cw.execute $cw.close -side top -expand y -fill x -pady 2m
                                                                                                                                                                                                   
  proc execute_cpmvdl { } {
     global inpsuffixlist jobsuffixlist outsuffixlist 
     global Wprog key_cpmvdl file_cpmvdl
     global inpfile jobfile outfile

     set cw $Wprog.cpmvdl_file

     set source [string trim [$cw.source.cmd get]]
     if       {$key_cpmvdl!="d"} {
     set target [string trim [$cw.target.cmd get]]
     }  

     if {[file exists $source]} {

       if       {$key_cpmvdl=="c"} {
          eval set res [catch "exec cp $source $target" message]
       } elseif {$key_cpmvdl=="m"} {
          eval set res [catch "exec mv $source $target" message]
          if     {$file_cpmvdl=="m"} {set inpfile "$target"} \
          elseif {$file_cpmvdl=="e"} {set jobfile "$target"} \
          elseif {$file_cpmvdl=="s"} {set outfile "$target"} \
          else   {writescr0 $Wprog.d.tt "file $file_cpmvdl not allowed \n"}
       } elseif {$key_cpmvdl=="d"} {
          eval set res [catch "exec rm $source "        message]
          if     {$file_cpmvdl=="m"} {set inpfile ""} \
          elseif {$file_cpmvdl=="e"} {set jobfile ""} \
          elseif {$file_cpmvdl=="s"} {set outfile ""} \
          else   {writescr0 $Wprog.d.tt "file $file_cpmvdl not allowed \n"}
       } else {
         writescr0 $Wprog.d.tt "key $key_cpmvdl not allowed \n"
         destroy $cw
       }

       writescr0 $Wprog.d.tt "$message \n"
       lfuellen $Wprog.c.m.f.f.li $inpsuffixlist
       lfuellen $Wprog.c.e.f.f.li $jobsuffixlist
       lfuellen $Wprog.c.s.f.f.li $outsuffixlist
       prog_anzeige $Wprog
     }
     destroy $cw
  }
}

########################################################################################
 proc prog_anzeige {w} {#                                              update display
  global working_directory inpfile jobfile outfile inpsuffix jobsuffix outsuffix
  $w.b.c1.d.t        configure -text "$working_directory"
  if {$inpfile==""}   {set m "NO INP-FILE SELECTED"} else {set m "$inpfile"}
  if {$jobfile==""}   {set e "NO JOB-FILE SELECTED"} else {set e "$jobfile"}
  if {$outfile==""}   {set s "NO OUT-FILE SELECTED"} else {set s "$outfile"}
  $w.b.c1.m.t        configure -text "$m"
  $w.b.c1.e.t        configure -text "$e"
  $w.b.c1.s.t        configure -text "$s"
 }

########################################################################################
 proc lfuellen {w suffixlist} {#                fill filelist with suffix s into widget w
  global sub sorttype filter
  $w delete 0 end;  set subalt $sub
  if {$sorttype=="alphabetical"} {set liste [exec ls] } else {set liste [exec ls -t]}
  foreach x $suffixlist { 
     foreach i $liste {
       if {[file isfile $i]} {
         if {[string match "*$x" $i]} { 
            if {[string match "*$filter*" $i]} {$w insert end $i}
         }
       }
     } 
  }
  lock;  set sub $subalt
 }


########################################################################################
# 
# key is one of:
#
# i : run interactive
# b : batch
# q : submit on queue
# j : create job-file
# v : select version
#

 proc run_prog {iprog key} {#                        run program number i in  PROGS_NAME_LIST

     global Wprog inpfile jobfile outfile sorttype 
     global PROGS_NAME_LIST PROGS_VERS_LIST working_directory PROG_PATH PROG_DIRECT PROG_OPTION
     global run_prog_cw run_prog_iprog run_prog_PROG PACKAGE
     global wishcall xband_path env  
     
     set PRC "run_prog"
     
     set PROG      [lindex $PROGS_NAME_LIST $iprog]
     set PROG_VERS [lindex $PROGS_VERS_LIST $iprog]
     
     set l2 [expr {[string length $inpfile] -1}]
     
     if {[string range $inpfile [expr {$l2 - 2}] $l2] == "inp"} {set l2 [expr {$l2 - 4}]}
     
     if {$l2 < 0} {
	 set jobfile "_job"
	 set outfile "_out"
     } else {
	 set jobfile "[string range $inpfile 0 $l2].job"
	 set outfile "[string range $inpfile 0 $l2].out"
     }
     
     $Wprog.b.c1.e.mb configure -state normal -bg LightBlue
     $Wprog.b.c1.s.mb configure -state normal -bg LightBlue
     
     prog_anzeige $Wprog
     
     #---------------------------------------------------------------------------------------
     if {$key == "v"} {
	 
	 set cw $Wprog.run_prog_version
	 
	 set run_prog_cw     $cw
	 set run_prog_iprog  $iprog
	 set run_prog_PROG   $PROG
	 
	 toplevel_init $cw "set program version" 0 0
	 
	 wm geometry $cw +300+50
	 
	 wm iconname $cw "set program version"
	 label $cw.head -height 6 -padx 2 -text "specify version of program $PROG \n\n \
		 to run  $PROG{version} \n\n  and press <RETURN> or button" 
		
	 pack  $cw.head -anchor n -side top -fill x
	 
	 set version ""
	 frame $cw.version -borderwidth 10
	 pack  $cw.version -side top -fill x
	 label $cw.version.l   -text "version" -padx 0
	 entry $cw.version.cmd -width 45 -relief sunken -textvariable version
	 bind  $cw.version.cmd <Key-Return> {run_prog_set_version}
	 $cw.version.cmd delete 0 end
	 $cw.version.cmd insert 0 $version
	 pack  $cw.version.l   -side left
	 pack  $cw.version.cmd -side right -fill x
	 
	 button $cw.execute -text "set version" -height 2 -width 50 -bg green1 \
		 -command "run_prog_set_version"
	 
	 button $cw.close -text "Close" -height 2 -width 50 -bg tomato1 \
		 -command "destroy $cw"
	 
	 pack $cw.execute $cw.close -side top -expand y -fill x -pady 2m
     } ; # end key == v                                                                  
     
     proc run_prog_set_version { } {
	 
	 global run_prog_cw run_prog_iprog run_prog_PROG 
	 global PROGS_NAME_LIST PROGS_VERS_LIST Wprog PROG_VERS PACKAGE PROGS_VERS_LIST_TAB
	 
	 set PRC "run_prog_set_version" 
	 
	 set PROG_VERS [$run_prog_cw.version.cmd get]
	 
	 set PROGS_VERS_LIST [lreplace $PROGS_VERS_LIST $run_prog_iprog $run_prog_iprog $PROG_VERS]

	 set PROGS_VERS_LIST_TAB($PACKAGE) $PROGS_VERS_LIST

         puts "******************* PROGS_VERS_LIST_TAB:  $PROGS_VERS_LIST_TAB($PACKAGE)"
	 
	 $Wprog.b.c1.m2.mb$run_prog_PROG configure -text "$run_prog_PROG$PROG_VERS"
	 
	 destroy $run_prog_cw
	 
     }
     
     #---------------------------------------------------------------------------------------
     # ts 041112: DEFAULT PATH is never set in programm 
     if {$PROG_PATH =="DEFAULT PATH"} { 
	 eval set res [catch "exec which $PROG$PROG_VERS" PP] 
	 set l [string length $PP]
	 if {$l>0} {
	     set PROG_PATH [string range $PP 0 [expr [string last $PROG$PROG_VERS $PP]-1]] 
	     writescr0  $Wprog.d.tt "program path taken from default PATH:  $PROG_PATH \n"
	 }
     }
     
     set PP $PROG_PATH
     debug $PRC "XXX $PP $PROG $PROG_VERS [file exists $PP$PROG$PROG_VERS] [file executable $PP$PROG$PROG_VERS]"
     
     if {[file exists $PROG_PATH$PROG$PROG_VERS]==0} {
	 writescr0  $Wprog.d.tt "not executable: $PROG_PATH$PROG$PROG_VERS \n"
	 set_prog_path "$PROG_PATH$PROG$PROG_VERS is not executable \n \
		 set proper program path for $PROG$PROG_VERS" $PROG$PROG_VERS
	 return
     }       
     
     if {$key == "i"} {
	 set START_DATE [exec date]

	 set   ia [open xband_aux w] 
	 puts  $ia "\# interactive script created by   xband "
	 puts  $ia "\# "

	 # for security reasons LD_LIBRARY_PATH is not transmitted to subshell if 
	 # called via xterm -e, set explicitly 
	 if {[info exists env(LD_LIBRARY_PATH)]} { 
	     puts  $ia "LD_LIBRARY_PATH=$env(LD_LIBRARY_PATH)"
	     puts  $ia "export LD_LIBRARY_PATH"
	 }
	 
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      starting interactive run of $PROG_PATH$PROG$PROG_VERS \" "
	 puts  $ia "echo \"      input file:   $inpfile  \" "
	 puts  $ia "echo \"      output will be appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "cd $working_directory "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    execution via xband:    $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) \"  >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "$PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) 2>&1 | tee -a  $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    execution via xband:    $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE)  \"  >> $outfile "
	 puts  $ia "echo \"    run started at: $START_DATE  \"      >> $outfile "
	 puts  $ia "echo \"    and ended   at: `date`       \"      >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      interactive run  \" "
	 puts  $ia "echo \"      program call:\" "
	 puts  $ia "echo \"      $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) | tee -a $outfile\" "
	 puts  $ia "echo \"      input file:          $inpfile  \" "
	 puts  $ia "echo \"      output appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "$wishcall -f $xband_path/message.tcl -n message 1 \$* & "
	 puts  $ia "exec bash "
	 close $ia
	 writescr0 .d.tt  "\nINFO from <run_prog> \n\nrunning $PROG_PATH$PROG$PROG_VERS interactively \
		 \noutput will be appended to   $outfile " 
	 exec chmod 755 xband_aux
	 eval set res [catch "exec xterm -T \"run $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) \" \
		 -geometry 82x40+100+100 -bg yellow -fg black -e ./xband_aux  &" message]
	 
     } else {
	 set MPI MPI
	 set    job [open $jobfile w] 
	 puts  $job  [prepare_batchjob_header $jobfile]
	 puts  $job "cd $working_directory "
	 puts  $job "mpirun -np 8 $PROG_PATH$PROG$PROG_VERS$MPI $inpfile $PROG_OPTION($PACKAGE) > $outfile "
#	 puts  $job "mpirun -np 8 $PROG_PATH$PROG$PROG_VERS $PROG_DIRECT($PACKAGE) $inpfile $PROG_OPTION($PACKAGE) > $outfile "
	 close $job
	 if {$key=="b"} {submit_batch_job $jobfile }
	 if {$key=="s"} {submit_batch_job $jobfile }
	 if {$key=="q"} {queue_batch_job $jobfile }
	 # else key==j create jobfile, no start 
	 
	 prog_sel_order $sorttype
	 prog_dir_update
     }
     
 }
 

########################################################################################
 proc do_with_job {key} {#                            use job-file

     global jobfile outfile working_directory sorttype Wprog 
     
     if {[file exists $jobfile]==0} {
	 give_warning .  "WARNING \n\n the jobfile \n\n $jobfile \n\n is not available " 
	 return
     } else {
	 
	 set l2 [expr {[string length $jobfile] -1}]
	 if {$l2 < 0} {
	     set outfile "_out"
	 } else {
	     set outfile "[string range $jobfile 0 $l2].out"
	 }
	 
	 if {$key=="i"} {
	     exec chmod 755 $jobfile
	     
	     set   ia [open xband_aux_do_with_job w] 
	     puts  $ia "\# interactive script to run job-file created by   xband "
	     puts  $ia "\# "
	     puts  $ia "cd $working_directory "
	     puts  $ia "$jobfile | tee -a  $outfile "
	     puts  $ia "bash "
	     close $ia
	     exec chmod 755 xband_aux_do_with_job
	     exec xterm -T "run $jobfile " -e ./xband_aux_do_with_job &
	     
	 } elseif {$key=="b"} {
	     
	     submit_batch_job $jobfile
	     
	 } elseif {$key=="q"} {
	     
	     queue_batch_job $jobfile
	     
	 } else {
	     
	     submit_batch_job $jobfile
	     
	 }
	 
	 prog_anzeige   $Wprog
	 prog_sel_order $sorttype
	 prog_dir_update
     }
     
 }
 
 
########################################################################################
 

########################################################################################
 proc submit_batch_job {jobfile} {#          
     global Wprog 
     
     set w ""
     
     eval set res [catch "exec whoami " user]
     eval set res [catch "exec cat $jobfile "     message1]
     eval set res [catch "exec batch < $jobfile " message]
     
     writescr0  $w.d.tt "\nINFO from <submit_batch_job> \n\n \
                         submitting batch job file $jobfile  \
                         \n\n=======================================================  \
                         \n$message1  \
                         \n\n=======================================================  \
                         \n\n$message  \
                         \n\n=======================================================  \
                         \n"
   
     set   mf [open "xband_mail" w] 
     puts  $mf $message
     puts  $mf "======================================================="
     puts  $mf "jobfile $jobfile"
     puts  $mf "======================================================="
     close $mf
     eval set res [catch "exec cat  $jobfile >> xband_mail " message1]
     eval set res [catch "exec mail $user@[exec hostname] < xband_mail " message1]
     eval set res [catch "exec rm xband_mail " message]
     writescr   $w.d.tt "$message1 \n"
     
 }
 
########################################################################################
 proc queue_batch_job {jobfile} {#          
     global Wprog outfile
     
     set w ""
     set oldout $outfile
     append oldout _sav
     eval set res [catch "exec whoami " user]
     eval set res [catch "exec cat $jobfile "  message1]
     eval set res [catch "exec qsub $jobfile " message]
     
     writescr0  $w.d.tt "\nINFO from <queue_batch_job> \n\n \
                         submitting batch job file $jobfile to queue  \
                         \n\n=======================================================  \
                         \n$message1  \
                         \n\n=======================================================  \
                         \n\n$message  \
                         \n\n=======================================================  \
                         \n"
   
#     set   mf [open "xband_mail" w] 
#     puts  $mf $message
#     puts  $mf "======================================================="
#     puts  $mf "jobfile $jobfile"
#     puts  $mf "======================================================="
#     close $mf
#     eval set res [catch "exec cat  $jobfile >> xband_mail " message1]
#     eval set res [catch "exec mail $user@[exec hostname] < xband_mail " message1]
#     eval set res [catch "exec rm xband_mail " message]
     writescr   $w.d.tt "$message \n"
     writescr   $w.d.tt "$message1 \n"
     set   ia [open xband_aux w] 
     puts  $ia "\#!/bin/bash"
     puts  $ia "\# interactive script created by   xband "
     puts  $ia "\# "
     puts  $ia "#Your job has been submited into the queue"
     puts  $ia "#Please check bellow if it starts to run"
     puts  $ia "echo \"$message\""
     puts  $ia "qstat"
     puts  $ia "sleep 2"
     puts  $ia "qstat"
     puts  $ia "sleep 2"
     puts  $ia "qstat"
     puts  $ia "sleep 2"
     puts  $ia "echo \"-------------------------------\" "
     puts  $ia "echo \"\n tail of $outfile\" "
     puts  $ia "echo \"-------------------------------\" "
     puts  $ia "tail -f $outfile"
     puts  $ia "exec bash"
     close $ia
     eval set res [catch "exec chmod 770 xband_aux" message]

     eval set res [catch "exec xterm -T \"running JOB in QUEUE \" \
		 -geometry 82x40+100+100 -bg yellow -fg black -e ./xband_aux  &" message]

     writescr   $w.d.tt "$message \n"     
 }                                                             ;# END of  queue_batch_job
########################################################################################



########################################################################################
#                                                                                   MAIN
########################################################################################

global Wprog Hprog hlp_dir working_directory prog_foc inpfile jobfile outfile outsuffix 
global mfold sysdirlist
global editor edback edxterm edoptions
global PROGS_NAME_LIST PROGS_VERS_LIST PROG_PATH PROG_DIRECT PROG_OPTION SYMMETRY
global buttonlist_PROGS PACKAGE 
global exedirlist sysfile syssuffix W_rasmol_button W_findsym_button
global GREP1 GREP2 GREP3 
global env COLOR WIDTH HEIGHT FONT
global xband_path

set PRC run_programs

set PACKAGE $PROGRAM_PACKAGE

set buttonlist_PROGS [list ]

if {[string trim $PROG_PATH]==""} {set PROG_PATH $env(HOME)/bin/}

set PROG_PATH [expand_dollar_home $PROG_PATH]

#---------------------------------------------------------------------------------------
#      find proper program path - check only availability of first program in PROGS_NAME_LIST
#

#****************************************************** temporarily suppressed

#set found_PROG_PATH 0
#set p [lindex $PROGS_NAME_LIST 0]
#debug $PRC "PROG_PATH: $PROG_PATH    program: $p    found: $found_PROG_PATH "
#set p_version $p
#set PROG_PATH_AUX [expand_dollar_home $PROG_PATH]
#set liste [exec ls $PROG_PATH_AUX]
#foreach x $liste {           
#   if {[string match $p* $x] == 1 } {
#      if {[file executable "$PROG_PATH_AUX/$x"]} {set p_version $x}
#   }
#}
#set p $p_version
#if {[file executable $PROG_PATH$p]==0} {
#   if {[llength  $exedirlist]==0} {
#      set PROG_PATH ""
#      incr found_PROG_PATH
#   } else {
#      set p [lindex $PROGS_NAME_LIST 0]
#      foreach d $exedirlist { 
#            set PROG_PATH_AUX [expand_dollar_home $d]
#
#         if {[file executable "$PROG_PATH_AUX/$p"]!=0} {
#            incr found_PROG_PATH
#            set PROG_PATH "$d"
#         } 
#      }
#   }
#} else {
#   set found_PROG_PATH 1
#}
#debug $PRC "PROG_PATH: $PROG_PATH    program: $p    found: $found_PROG_PATH "

##if {$found_PROG_PATH!=1} {set_prog_path "no unique program path found \n" NONE} 
#if {$found_PROG_PATH!=1} {debug $PRC  "found_PROG_PATH: $found_PROG_PATH  no unique program path found " } 

debug $PRC "PROG_PATH: $PROG_PATH"


#---------------------------------------------------------------------------------------
#                                                    initialize PROGS_VERS_LIST properly
####if { [string trim $PROGS_VERS_LIST]==""} {
####    foreach x $PROGS_NAME_LIST { lappend PROGS_VERS_LIST "" }
####}

#---------------------------------------------------------------------------------------

set Hprog prog_h.hlp

set inpfile ""
set jobfile ""
set outfile ""

toplevel_init $Wprog "RUN PROGRAM PACKAGE $PROGRAM_PACKAGE" 0 0

set tyh 7;  set working_directory [pwd] ;  set mfold "$inpfile"

# top buttons

insert_topbuttons $Wprog $Hprog
bind $Wprog.a.e <Button-1> {
  if [prog_focus ""] {
    writescr $Wprog.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wprog; unlock_list
    if {($mfold!=$inpfile)&&([file exists "${inpfile}.vst"])} {.b.c1.1.vl invoke}
    Bend
  }
}


########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wprog.b   
pack configure $Wprog.b -in $Wprog -anchor w -pady 8

frame $Wprog.b.c1 
frame $Wprog.b.c12 
frame $Wprog.b.c2 
frame $Wprog.b.c3 

pack configure $Wprog.b.c1  -in $Wprog.b -anchor w -side left -pady 8
pack configure $Wprog.b.c12 -in $Wprog.b -anchor w -side left -padx 20
pack configure $Wprog.b.c2  -in $Wprog.b -anchor nw -side left -pady 8 -padx 5
pack configure $Wprog.b.c3  -in $Wprog.b -anchor nw -side left -pady 8 -padx 2

#=======================================================================================
#                                     b   COLUMN 1
#=======================================================================================
set wmb  22
set wbut 10
set wt1  15
set went 23
set wt2  30
set hbut  1

frame $Wprog.b.c1.d; frame $Wprog.b.c1.x;  frame $Wprog.b.c1.m; frame $Wprog.b.c1.m2
frame $Wprog.b.c1.e; frame $Wprog.b.c1.s
pack configure $Wprog.b.c1.d $Wprog.b.c1.x  $Wprog.b.c1.m  $Wprog.b.c1.m2 \
               $Wprog.b.c1.e $Wprog.b.c1.s -in $Wprog.b.c1 -anchor w

label $Wprog.b.c1.d.l -width $wt1 -text "current directory" -anchor w -padx 10
label $Wprog.b.c1.d.t -anchor w
pack configure $Wprog.b.c1.d.l $Wprog.b.c1.d.t -in $Wprog.b.c1.d -anchor w -side left

label $Wprog.b.c1.x.l -width $wt1 -anchor w -text "system" -padx 10
label $Wprog.b.c1.x.e -width $wt2 -anchor w -textvariable sysfile
### entry $Wprog.b.c1.x.e -textvariable sysfile -width $went -relief flat -font $FONT(GEN)
button $Wprog.b.c1.x.but -text "create input" -width $wmb -height $hbut \
    -command "create_inpfile" -bg  {lightblue}

pack configure $Wprog.b.c1.x.l $Wprog.b.c1.x.e $Wprog.b.c1.x.but  \
     -in $Wprog.b.c1.x -anchor w -side left


########################################################################################

label  $Wprog.b.c1.m.l -width $wt1 -anchor w -text "input-file" -padx 10
label  $Wprog.b.c1.m.t -width $wt2 -anchor w
menubutton $Wprog.b.c1.m.mb -text "inp-file menu" -menu $Wprog.b.c1.m.mb.menu \
           -height $hbut -width $wmb -state disabled -bg grey -relief raised
set mm [menu $Wprog.b.c1.m.mb.menu -tearoff 1]
$mm add command -label "edit input file"    -command "do_with_file m e"
$mm add command -label "copy input file"    -command "do_with_file m c"
$mm add command -label "move input file"    -command "do_with_file m m"
$mm add command -label "delete input file"  -command "do_with_file m d"
$mm add command -label "list input file"    -command "do_with_file m l"
$mm add command -label "print input file"   -command "do_with_file m p"
pack configure $Wprog.b.c1.m.l $Wprog.b.c1.m.t -in $Wprog.b.c1.m -anchor w -side left
pack configure $Wprog.b.c1.m.mb -in $Wprog.b.c1.m -anchor w -side left -padx 1 -pady 2

label  $Wprog.b.c1.m2.l -width $wt1 -anchor w -text "program path" -padx 10
label  $Wprog.b.c1.m2.t -width $wt2 -anchor w -textvariable PROG_PATH
pack configure $Wprog.b.c1.m2.l  $Wprog.b.c1.m2.t -in $Wprog.b.c1.m2  \
     -anchor w -side left

set wmbPROG 11
set n [llength $PROGS_NAME_LIST]
for {set i 0} {$i < $n} {incr i} {
    set PROG       [lindex $PROGS_NAME_LIST $i]
    set PROG_VERS  [lindex $PROGS_VERS_LIST $i]

    menubutton $Wprog.b.c1.m2.mb$PROG -text "$PROG$PROG_VERS" -menu $Wprog.b.c1.m2.mb$PROG.menu \
              -height $hbut -width $wmbPROG -state disabled -bg grey -relief raised
    set mp($PROG) [menu $Wprog.b.c1.m2.mb$PROG.menu -tearoff 0]
    $mp($PROG) add command -label "run   (interactive)        " -command "run_prog $i  i"
    $mp($PROG) add command -label "submit to queue            " -command "run_prog $i  q"
    $mp($PROG) add command -label "create job-file            " -command "run_prog $i  j"
    $mp($PROG) add command -label "batch (on site)            " -command "run_prog $i  b"
    $mp($PROG) add command -label "select version             " -command "run_prog $i  v"
    $mp($PROG) add command -label "select program path        " -command "set_prog_path \"set path for \"  $PROG"

    pack configure $Wprog.b.c1.m2.mb$PROG -in $Wprog.b.c1.m2 -anchor w -side left -padx 1 -pady 2
    set buttonlist_PROGS [lappend buttonlist_PROGS $Wprog.b.c1.m2.mb$PROG]
} 

label  $Wprog.b.c1.e.l -width $wt1 -anchor w -text "job-file" -padx 10
label  $Wprog.b.c1.e.t -width $wt2 -anchor w
menubutton $Wprog.b.c1.e.mb -text "job-file menu" -menu $Wprog.b.c1.e.mb.menu \
           -height $hbut -width $wmb -state disabled -bg grey -relief raised
set me [menu $Wprog.b.c1.e.mb.menu -tearoff 1]
$me add command -label "run   (interactive)        " -command "do_with_job i"
$me add command -label "batch (on site)            " -command "do_with_job b"
$me add command -label "submit to queue            " -command "do_with_job q"
$me add command -label "edit job file"    -command "do_with_file e e"
$me add command -label "copy job file"    -command "do_with_file e c"
$me add command -label "move job file"    -command "do_with_file e m"
$me add command -label "delete job file"  -command "do_with_file e d"
$me add command -label "list job file"    -command "do_with_file e l"
$me add command -label "print job file"   -command "do_with_file e p"
pack configure $Wprog.b.c1.e.l $Wprog.b.c1.e.t -in $Wprog.b.c1.e -anchor w -side left
pack configure $Wprog.b.c1.e.mb -in $Wprog.b.c1.e -anchor w -side left -padx 1 -pady 2

label $Wprog.b.c1.s.l -width $wt1 -anchor w -text "out-file" -padx 10
label $Wprog.b.c1.s.t -width $wt2 -anchor w
menubutton $Wprog.b.c1.s.mb -text "out-file menu" -menu $Wprog.b.c1.s.mb.menu \
           -height $hbut -width $wmb -state disabled -bg grey -relief raised
set ms [menu $Wprog.b.c1.s.mb.menu -tearoff 1]
$ms add command -label "edit output file"    -command "do_with_file s e"
$ms add command -label "copy output file"    -command "do_with_file s c"
$ms add command -label "move output file"    -command "do_with_file s m"
$ms add command -label "delete output file"  -command "do_with_file s d"
$ms add command -label "list output file"    -command "do_with_file s l"
$ms add command -label "print output file"   -command "do_with_file s p"
$ms add command -label "tail -f output file" -command "do_with_file s t"
$ms add command -label "grep $GREP1 output file" -command "do_with_file s grep1"
$ms add command -label "grep $GREP2 output file" -command "do_with_file s grep2"
$ms add command -label "grep $GREP3 output file" -command "do_with_file s grep3"
$ms add command -label "egrep $GREP1|$GREP2 output file" -command "do_with_file s grep12"
pack configure $Wprog.b.c1.s.l $Wprog.b.c1.s.t -in $Wprog.b.c1.s -anchor w -side left
pack configure $Wprog.b.c1.s.mb -in $Wprog.b.c1.s -anchor w -side left -padx 1 -pady 2

#=======================================================================================
frame $Wprog.b.c1.sor
frame $Wprog.b.c1.fil
pack configure $Wprog.b.c1.fil $Wprog.b.c1.sor -in $Wprog.b.c1 -anchor w -side left

label $Wprog.b.c1.fil.l -width 4 -text "Filter" -anchor w  -padx 10
entry $Wprog.b.c1.fil.e -textvariable {filter} -width {29} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
bind $Wprog.b.c1.fil.e <Key-Return> {prog_sel_order $sorttype}
 
pack configure $Wprog.b.c1.fil.l $Wprog.b.c1.fil.e -in $Wprog.b.c1.fil -anchor w -side left


label $Wprog.b.c1.sor.l -width 4 -text "sort mode" -anchor w  -padx 80
frame $Wprog.b.c1.sor.b
pack configure $Wprog.b.c1.sor.l $Wprog.b.c1.sor.b -in $Wprog.b.c1.sor -anchor w -side left
CreateLSBoxSBx $Wprog.b.c1.sor.b " " top 11 3 0 $Hprog $Wprog.d.tt \
    "" "\{alphabetical\} \{last access\}" prog_sel_order ""

$Wprog.b.c1.sor.b.f.f.1 select

#=======================================================================================
#                                     b   COLUMN 2
#=======================================================================================
set c2bg tomato
set c2wb 10
set c2hb 1
set c2py 0
set Wc2  $Wprog.b.c2

button $Wc2.b1 -text "prog-path" -width $c2wb -height $c2hb \
      -command "set_prog_path \" \" NONE"  -bg $c2bg -pady $c2py -padx 7
button $Wc2.b2 -text "plot"      -width $c2wb -height $c2hb -command "plot_data 0" \
   -bg $c2bg -pady $c2py -padx 7
button $Wc2.b3 -text "rasmol"  -width $c2wb -height $c2hb -command "sites_graphik"  \
   -bg $c2bg -pady $c2py -padx 7 -state disabled
set W_rasmol_button $Wc2.b3
button $Wc2.b4 -text "findsym"  -width $c2wb -height $c2hb -command "exec_findsym" \
   -bg $c2bg -pady $c2py -padx 7 -state disabled
set W_findsym_button $Wc2.b4
button $Wc2.b5 -text "TIDY UP" -width $c2wb -height $c2hb \
      -command "clear .d.tt"  -bg $c2bg -pady $c2py -padx 7

if {[file exists $xband_path/tutorial_${PROGRAM_PACKAGE}.tcl]==0} {

  pack configure $Wc2.b1 $Wc2.b2 $Wc2.b3 $Wc2.b4 $Wc2.b5 \
            -in $Wc2 -anchor nw -side top

} else {

  button $Wc2.b6 -text "TUTORIAL" -width $c2wb -height $c2hb \
      -command "tutorial"  -bg $c2bg -pady $c2py -padx 7

  pack configure $Wc2.b1 $Wc2.b2 $Wc2.b3 $Wc2.b4 $Wc2.b5 $Wc2.b6 \
            -in  $Wc2 -anchor nw -side top
} 



#----------------------------------------------------------------------------------
proc exec_findsym { } {# local function connected with button    W_findsym_button
    set PRC "exec_findsym"

    global SYMMETRY
    if [file exists struc.inp] {
	execunixcmd "$SYMMETRY"
	# $W_findsym_button configure -state normal
    } else {
	give_warning . "WARNING \n\n the standard input file \n\n struc.inp \n\n \
		for the symmetry programm  findsym  \n\n does not exist"
    }             
}
#----------------------------------------------------------------------------------

#=======================================================================================
#                                     b   COLUMN 3
#=======================================================================================
set c3bg tomato
set c3wb 10
set c3hb 1
set c3py 0
set Wc3  $Wprog.b.c3

button $Wc3.b1 -text "unix-cmd"  -width $c3wb -height $c3hb -command "execunixcmd \"pwd\" "     \
   -bg $c3bg -pady $c3py -padx 7
button $Wc3.b2 -text "xterm"     -width $c3wb -height $c3hb -command "exec xterm &"             \
   -bg $c3bg -pady $c3py -padx 7
button $Wc3.b3 -text "ls -l"     -width $c3wb -height $c3hb -command "execunixcmd \"ls -l \" "  \
   -bg $c3bg -pady $c3py -padx 7
button $Wc3.bf -text "files"     -width $c3wb -height $c3hb -command "handle_files"  \
   -bg $c3bg -pady $c3py -padx 7
button $Wc3.b4 -text "top"       -width $c3wb -height $c3hb -command "exec xterm -e top  &"     \
   -bg $c3bg -pady $c3py -padx 7
button $Wc3.b5 -text "pine"      -width $c3wb -height $c3hb -command "exec xterm -e pine &"     \
   -bg $c3bg -pady $c3py -padx 7
label $Wc3.b6 -text " "
button $Wc3.b7 -text "close"      -width $c3wb -height $c3hb -command "destros $Wprog; unlock_list" \
   -bg red -pady $c3py -padx 7

pack configure $Wc3.b1 $Wc3.b2 $Wc3.b3 $Wc3.bf $Wc3.b4\
               $Wc3.b5 $Wc3.b6 $Wc3.b7  -in  $Wc3 -anchor nw -side top



########################################################################################
#   frame c for modification: d - directory, m - inpfile, e - job-file, s - out-file
########################################################################################

frame $Wprog.c; pack configure $Wprog.c -in $Wprog -anchor w

frame $Wprog.c.d;  frame $Wprog.c.m;  frame $Wprog.c.e;  frame $Wprog.c.s
pack configure $Wprog.c.d -in $Wprog.c -side left -anchor se -ipadx 3
pack configure $Wprog.c.s $Wprog.c.e  $Wprog.c.m -in $Wprog.c -side right -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 
CreateLSBoxSBx $Wprog.c.d "select directory" top 24 15 2 $Hprog $Wprog.d.tt "" "" prog_sel_d sel
$Wprog.c.d.f.f.li insert end "\$HOME" ".."
foreach x $sysdirlist { $Wprog.c.d.f.f.li insert end "$x"}
foreach i [lsort [glob -nocomplain -- *]] {
    if {[file isdirectory $i]} {$Wprog.c.d.f.f.li insert end $i}
}

# new directory insert
label $Wprog.c.d.li -anchor w -text "directory:"
entry $Wprog.c.d.i -width 29 -relief sunken
####pack configure $Wprog.c.d.li $Wprog.c.d.i -in $Wprog.c.d -side top -anchor w
bind  $Wprog.c.d.i <Button-1> {prog_focus $Wprog.c.d.i; set prog_foc 1}
bind  $Wprog.c.d.i <Button-2> \
  {writescr0 $Wprog.d.tt "prog_19  $dirinpwork\n"; \
             $Wprog.c.d.i delete 0 end;             \
             $Wprog.c.d.i insert end $working_directory}
bind  $Wprog.c.d.i <Return>   {prog_edn; focus $Wprog}


########################################################################################
#                                                                          inpfile: c.m
########################################################################################
#
label $Wprog.c.m.l -text "select | edit input-file" -anchor w
pack configure $Wprog.c.m.l -in $Wprog.c.m -side top -anchor w

frame $Wprog.c.m.1; # suffix-box and file-ordering-buttons
pack configure $Wprog.c.m.1 -in $Wprog.c.m -side top -anchor w -pady 2

frame $Wprog.c.m.1.2
pack configure $Wprog.c.m.1.2 -in $Wprog.c.m.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wprog.c.m.1.2 "allowed \n suffix" left 9 3 2 $Hprog $Wprog.d.tt \
	"" [vst2list inpsuffix.vst 0 "" "" dummy] prog_dummy sel
	
# file name selection box
CreateLSBoxSBx2 $Wprog.c.m "" "" 30 12 2 $Hprog $Wprog.d.tt "" "" prog_sel_m prog_sel_m2 sel


########################################################################################
#                                                                   job-file: .c.e
########################################################################################
#
label $Wprog.c.e.l -text "select | edit job-file" -anchor w
pack configure $Wprog.c.e.l -in $Wprog.c.e -side top -anchor w

frame $Wprog.c.e.1; # suffix-box and file-ordering-buttons
pack configure $Wprog.c.e.1 -in $Wprog.c.e -side top -anchor w -pady 2

frame $Wprog.c.e.1.2
pack configure $Wprog.c.e.1.2 -in $Wprog.c.e.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wprog.c.e.1.2 "allowed \n suffix  " left 9 3 2 $Hprog $Wprog.d.tt \
	"" [vst2list jobsuffix.vst 0 "" "" dummy] prog_dummy sel

# file name selection box
CreateLSBoxSBx2 $Wprog.c.e "" "" 30 12 2 $Hprog $Wprog.d.tt "" "" prog_sel_e prog_sel_e2 sel


########################################################################################
#                                                                      out-file: .c.s
########################################################################################

label $Wprog.c.s.l -text "select | edit output-file" -anchor w
pack configure $Wprog.c.s.l -in $Wprog.c.s -side top -anchor w

frame $Wprog.c.s.1; # suffix-box and file-ordering-buttons
pack configure $Wprog.c.s.1 -in $Wprog.c.s -side top -anchor w -pady 2

frame $Wprog.c.s.1.1; frame $Wprog.c.s.1.2
pack configure $Wprog.c.s.1.1 -in $Wprog.c.s.1 -side left  -anchor sw -ipady 3
pack configure $Wprog.c.s.1.2 -in $Wprog.c.s.1 -side right -anchor ne -ipady 3 -padx 5

# suffix selection box
CreateLSBox $Wprog.c.s.1.2 "allowed \n suffix  " left 9 3 2 $Hprog $Wprog.d.tt \
    "" [vst2list  outsuffix.vst 0 "" "" dummy] prog_dummy sel

# file name selection box
CreateLSBoxSBx2 $Wprog.c.s "" "" 30 12 2 $Hprog $Wprog.d.tt "" "" prog_sel_s prog_sel_s2 sel


########################################################################################


insert_textframe $Wprog $tyh

# initialisation, focus etc.

global PROGS_NAME_LIST PROGS_VERS_LIST

global inpsuffixlist jobsuffixlist outsuffixlist sorttype filter
set    inpsuffixlist [vst2list inpsuffix.vst 0 "" "" x]
set    jobsuffixlist [vst2list jobsuffix.vst 0 "" "" x]
set    outsuffixlist [vst2list outsuffix.vst 0 "" "" x]

if {[llength $inpsuffixlist]<=0} {
   set inpsuffixlist [list .inp _inp]
}
if {[llength $jobsuffixlist]<=0} {
   set jobsuffixlist [list .job _job]
}
if {[llength $outsuffixlist]<=0} {
   set outsuffixlist [list .out _out]
}

set sorttype alphabetical
set filter   "*"

prog_anzeige $Wprog

$Wprog.c.d.i     delete 0 end

prog_sel_order $sorttype

focus $Wprog; set prog_foc 0

if {$sysfile=="system"} {set sysfile ""} ;# remove dummy setting

debug $PRC "exedirlist:   $exedirlist"
debug $PRC "programs:   $PROGS_NAME_LIST"
debug $PRC "versions:   $PROGS_VERS_LIST"

global QUICK

if {$QUICK==1} {
   create_inpfile
}
#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
global QUICK_tut
if {$QUICK_tut==1} {
   tutorial
}
#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT

}  
#########################################################  END of proc  run_programs
########################################################################################





