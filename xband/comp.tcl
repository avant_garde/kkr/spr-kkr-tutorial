# compilation of programs 
#
#
# Copyright (C) 1998 H. Ebert

proc compile_progs {} {

########################################################################################
 proc compile_sel_d {d} {
  global Wcomp
  update; compile_focus $Wcomp; $Wcomp.c.d.f.f.li delete 0 end; update; dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default
    global Wcomp working_directory makefile makesuffix liste mdatsort
    global dimsuffix dimfile edatsort
    global srcsuffix srcfile sdatsort prgdirlist
    
    set cdir [expand_dollar_home $d]
    
    if {[file isdirectory $cdir]==0} {
	writescr0 .d.tt "\n the directory  $cdir  does not exist"
	give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
	return
    }

    if [winfo exists $Wcomp] {set w $Wcomp.d.tt} else {set w .d.tt}
    if {[catch {cd $cdir} m]!=0} {
	set working_directory [pwd]
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	writescr0 $w "new directory chosen: [pwd]\n\n"
	set working_directory [pwd]
	set makefile ""
	set dimfile  "" ; set dimsuffix ".dim"
	set srcfile  "" ; set srcsuffix ".f"
	if {[winfo exists $Wcomp]} {
	    compile_anzeige $Wcomp
	    
	    $Wcomp.c.d.f.f.li delete 0 end
	    $Wcomp.c.d.f.f.li insert end "\$HOME" ".."
	    foreach x $prgdirlist { $Wcomp.c.d.f.f.li insert end "$x"}
	    foreach k [lsort [glob -nocomplain -- *]] {
		if [file isdirectory $k] {$Wcomp.c.d.f.f.li insert end $k}
	    }
	    ffuellen $Wcomp.c.m.f.f.li $makesuffix $mdatsort
	    ffuellen $Wcomp.c.e.f.f.li $dimsuffix  $edatsort
	    ffuellen $Wcomp.c.s.f.f.li $srcsuffix  $sdatsort
	}
	return 1
    }
}


########################################################################################
 proc compile_sel_m {f} {
  global Wcomp makesuffix makefile liste working_directory prgdirlist prgdirlistmaxindex env
  compile_focus $Wcomp
  if {[file exists $f]} {
    $Wcomp.c.m.i delete 0 end
    regsub "$makesuffix\$" $f "" makefile

    writescr0 $Wcomp.d.tt "new makefile selected $makefile\n\n"; compile_anzeige $Wcomp
    mfuellen $Wcomp.b.mm.sel.f.f.li $makefile

    set diraux [collapse_dollar_home $working_directory]

    if {[llength $prgdirlist]>0} {
       set prgdirlist [update_dirlist $prgdirlist $prgdirlistmaxindex $diraux]
    } else {
       lappend prgdirlist $diraux
    }
    $Wcomp.c.d.f.f.li delete 0 end
    $Wcomp.c.d.f.f.li insert end "\$HOME" ".."
    foreach x $prgdirlist { $Wcomp.c.d.f.f.li insert end "$x"}
  }
 }
########################################################################################
 proc compile_sel_mm {f} {
  global Wcomp makesuffix makefile liste 

    execunixcmd "make -f $f"
 }

########################################################################################
 proc compile_sel_e {f} {
  global Wcomp dimsuffix dimfile
  global editor edback edxterm edoptions 
  compile_focus $Wcomp
  if {[file exists $f]} {
    $Wcomp.c.e.i delete 0 end
    if {$dimsuffix==""} {set dimfile $f} else {regsub "$dimsuffix\$" $f "" dimfile}
    writescr0 $Wcomp.d.tt "new dim-file selected: $dimfile$dimsuffix\n\n"; compile_anzeige $Wcomp
  }
 }

########################################################################################
 proc compile_sel_s {f} {
  global Wcomp srcsuffix srcfile
  global editor edback edxterm edoptions
  compile_focus $Wcomp
  if {[file exists $f]} {
    $Wcomp.c.e.i delete 0 end
    if {$srcsuffix==""} {set dfile $f} else {regsub "$srcsuffix\$" $f "" srcfile}
    writescr0 $Wcomp.d.tt "new source-file selected: $srcfile$srcsuffix\n\n"; compile_anzeige $Wcomp
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    set pid "$message"
  }
 }

########################################################################################
 proc compile_sel_m_order {s} {
  global Wcomp mdatsort makesuffix
  if {$s=="alphabetical"} {set mdatsort 0} else {set mdatsort 1}
  ffuellen $Wcomp.c.m.f.f.li $makesuffix $mdatsort
 }

########################################################################################
 proc compile_sel_e_order {s} {
  global Wcomp edatsort dimsuffix
  if {$s=="alphabetical"} {set edatsort 0} else {set edatsort 1}
  ffuellen $Wcomp.c.e.f.f.li $dimsuffix $edatsort
 }

########################################################################################
 proc compile_sel_s_order {s} {
  global Wcomp sdatsort srcsuffix
  if {$s=="alphabetical"} {set sdatsort 0} else {set sdatsort 1}
  ffuellen $Wcomp.c.s.f.f.li $srcsuffix $sdatsort
 }

########################################################################################
 proc compile_sel_m_suffix {s} {
  global Wcomp makesuffix makefile mdatsort
  $Wcomp.c.m.i delete 0 end
  if {$makesuffix!=$s} {set makesuffix $s; set makefile ""}
  ffuellen $Wcomp.c.m.f.f.li $makesuffix $mdatsort
  writescr0 $Wcomp.d.tt "new value for makefile suffix: $makesuffix\n\n"; compile_anzeige $Wcomp
  $Wcomp.b.mm.sel.f.f.li delete 0 end
 }

########################################################################################
 proc compile_sel_e_suffix {s} {
  global Wcomp dimsuffix dimfile edatsort
  $Wcomp.c.e.i delete 0 end
  if {$s=="*"} then {set ns ""} else {set ns $s}
  if {$dimsuffix!=$ns} {set dimfile ""; set dimsuffix $ns}
  ffuellen $Wcomp.c.e.f.f.li $dimsuffix $edatsort
  writescr0 $Wcomp.d.tt "new value for dim-file suffix: *$dimsuffix\n\n"; compile_anzeige $Wcomp
 }

########################################################################################
 proc compile_sel_s_suffix {s} {
  global Wcomp srcsuffix srcfile sdatsort
  $Wcomp.c.s.i delete 0 end
  if {$s=="*"} then {set ns ""} else {set ns $s}
  if {$srcsuffix!=$ns} {set srcfile ""; set srcsuffix $ns}
  ffuellen $Wcomp.c.s.f.f.li $srcsuffix $sdatsort
  writescr0 $Wcomp.d.tt "new value for file suffix: *$srcsuffix\n\n"; compile_anzeige $Wcomp
 }

########################################################################################
 proc compile_run_m {f} {
  global Wcomp srcsuffix srcfile
  global editor edback edxterm edoptions
  compile_focus $Wcomp
  if {[file exists $f]} {
    $Wcomp.c.e.i delete 0 end
    if {$srcsuffix==""} {set dfile $f} else {regsub "$srcsuffix\$" $f "" srcfile}
    writescr0 $Wcomp.d.tt "new source-file selected: $srcfile$srcsuffix\n\n"; compile_anzeige $Wcomp
    set ed_file $f
    eval set res [catch "exec $edxterm $editor $edoptions $f $edback" message]
    set pid "$message"
  }
 }

########################################################################################
 proc compile_focus {w} {
  global Wcomp compile_foc
  if     {$compile_foc==0} {set r 0} \
  elseif {$compile_foc==1} {set r [compile_edn]} \
  elseif {$compile_foc==2} {set r [compile_emfn]} \
  elseif {$compile_foc==3} {set r [compile_eefn]} \
  elseif {$compile_foc==4} {set r [compile_esfn]}
  if {$w!=""} {focus $w} else {focus $Wcomp; return $r}
 }

########################################################################################
 proc compile_edn {} {# enter directory name
     global Wcomp compile_foc compile_d
     set compile_foc 0
     set d [string trim [$Wcomp.c.d.i get]]
     $Wcomp.c.d.i delete 0 end
     writescr0 $Wcomp.d.tt ""
     
     if {$d==""} {return 0}
     set compile_d [expand_dollar_home $d]
     
     if {![file isdirectory $compile_d] && ![file exists $compile_d]} {
	 mkdcdfill
     } else {
	 dirselected_comp $compile_d
	 compile_anzeige $Wcomp
     }
     return 1
 }
 
########################################################################################
 proc compile_emfn {} {#                                             enter makefile name
  global Wcomp makesuffix makefile dimfile srcfile dimsuffix liste compile_foc

  set compile_foc 0
  set f [string trim [$Wcomp.c.m.i get]]; $Wcomp.c.m.i delete 0 end; writescr0 $Wcomp.d.tt ""
  if {$f=="$makesuffix"} {set f ""; writescr $Wcomp.d.tt "filename cannot consist of file suffix only\n"}
  regsub "$makesuffix$" $f "" h; if {"$h$makesuffix"=="$f"} {set f "$h"}
  testfilename $f $Wcomp.d.tt $makesuffix;  if {$f==""} {return 0}

  set makefile $f
  writescr $Wcomp.d.tt "input of new makefile name: $makefile\n\n"
  compile_anzeige $Wcomp
  return 1
 }



########################################################################################
 proc compile_eefn {} {#                                             enter dim-file name
  global Wcomp dimsuffix dimfile compile_foc

  set compile_foc 0
  set f [string trim [$Wcomp.c.e.i get]]; $Wcomp.c.e.i delete 0 end; writescr0 $Wcomp.d.tt ""
  if {$f=="$dimsuffix"} {set f ""; writescr $Wcomp.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${dimsuffix}$" $f "" h; if {"$h$dimsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wcomp.d.tt $dimsuffix;  if {$f==""} {return 0}

  set dimfile $f
  writescr $Wcomp.d.tt "input of new dim-file name: $dimfile$dimsuffix\n\n" 
  compile_anzeige $Wcomp
  return 1
 }


########################################################################################
 proc compile_esfn {} {#                                          enter source file name
  global Wcomp srcsuffix srcfile compile_foc

  set compile_foc 0
  set f [string trim [$Wcomp.c.s.i get]]; $Wcomp.c.s.i delete 0 end; writescr0 $Wcomp.d.tt ""
  if {$f=="$srcsuffix"} {set f ""; writescr $Wcomp.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${srcsuffix}$" $f "" h; if {"$h$srcsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wcomp.d.tt $srcsuffix;  if {$f==""} {return 0}

  set srcfile $f
  writescr $Wcomp.d.tt "input of new source file name $srcfile$srcsuffix\n\n" 
  compile_anzeige $Wcomp
  return 1
 }

########################################################################################
proc editfile {f} {#                                              enter source file name
  global makefile dimfile dimsuffix srcfile srcsuffix
  global edxterm editor edoptions edback

  if     {$f=="m"} {set ff "$makefile"}  \
  elseif {$f=="e"} {set ff "$dimfile$dimsuffix"} \
  elseif {$f=="s"} {set ff "$srcfile$srcsuffix"} \
  else             {set ff ""}

  set ed_file $ff
  eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
  set pid "$message"
}

########################################################################################
 proc compile_anzeige {w} {#                                              update display
  global working_directory makefile makesuffix dimfile dimsuffix srcfile srcsuffix dimsuffixl
  $w.b.d.t   configure -text "$working_directory"
####  .c.1.wdir.dir  configure -text "$working_directory"
  $w.c.m.li  configure -text "makefile: ($makesuffix)"
  $w.c.e.li  configure -text "dim-file: ([alternative $dimsuffix * $dimsuffix]):"
  $w.c.s.li  configure -text "source-file: ($srcsuffix):"
  if {$makefile==""}  {set m "NONE"}                     else {set m "$makefile"}
  if {$dimfile==""}   {set e "NO DIM-FILE SELECTED"}     else {set e "$dimfile$dimsuffix"}
  if {$srcfile==""}   {set s "NO SOURCE FILE SELECTED"}  else {set s "$srcfile$srcsuffix"}
  if {$dimsuffix==""} {set dimsuffixl "*"} else {set dimsuffixl $dimsuffix}
  $w.b.m.t        configure -text "$m"
  $w.b.e.t        configure -text "$e"
  $w.b.s.t        configure -text "$s"
  $w.c.m.1.1.f.l  configure -text "file list ($makesuffix):" -anchor w
  $w.c.e.1.1.f.l  configure -text "file list ($dimsuffixl):" -anchor w
  $w.c.s.1.1.f.l  configure -text "file list ($srcsuffix): " -anchor w
 }

########################################################################################
 proc ffuellen {w s datsort} {#                fill filelist with suffix s into widget w
  global sub
  $w delete 0 end;  set subalt $sub
  if {$datsort==0} {set liste [exec ls]} else {set liste [exec ls -t]}
  foreach i $liste {if {[file isfile $i]} {if {[string match "*$s" $i]} {$w insert end $i}}}
  lock;  set sub $subalt
 }

########################################################################################
 proc mfuellen {w f} {#                             fill makefile commands into widget w
  $w delete 0 end; 
  set fileId [open $f r]
  set i 0
  set pat {[a-z]*:*}
  $w insert end "$f "

  while {[gets $fileId line] >=0} {
     if {[string match $pat $line]==1} {
        incr i
        string first : $line
        set j [ expr [string first : $line] -1 ]
        set option "[string range $line 0 $j ]"
        $w insert end "$f $option"
      }
  }
  close $fileId

  lock; 
 }


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wcomp hlp_dir working_directory compile_foc makefile makesuffix dimfile dimsuffix srcfile srcsuffix 
global mdatsort edatsort sdatsort mfold prgdirlist
global compile_sel_m_order_v compile_sel_e_order_v compile_sel_s_order_v
global editor edback edxterm edoptions

set makefile ""
if {[file exists makefile]} {set makefile "makefile"}

set  dimfile ""
set  srcfile ""

toplevel_init $Wcomp "COMPILE PROGRAMS" 0 0;   set tyh 10;   set working_directory [pwd];   set mfold "$makefile"


# top buttons

insert_topbuttons $Wcomp compile_h.hlp
bind $Wcomp.a.e <Button-1> {
  if [compile_focus ""] {
    writescr $Wcomp.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wcomp; unlock_list
    if {($mfold!=$makefile)&&([file exists "${makefile}.vst"])} {.b.1.vl invoke}
    Bend
  }
}


########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wcomp.b   
pack configure $Wcomp.b -in $Wcomp -anchor w -pady 8
frame $Wcomp.b.d; frame $Wcomp.b.m; frame $Wcomp.b.mm;  frame $Wcomp.b.e; frame $Wcomp.b.s
pack configure $Wcomp.b.d $Wcomp.b.m $Wcomp.b.mm $Wcomp.b.e $Wcomp.b.s -in $Wcomp.b -anchor w

label $Wcomp.b.d.l -width 15 -text "current directory" -anchor w
label $Wcomp.b.d.t -anchor w
pack configure $Wcomp.b.d.l $Wcomp.b.d.t -in $Wcomp.b.d -anchor w -side left
bind  $Wcomp.b.d.l <Button-3> {cathfile0 compile_dirdisp $Wcomp}
bind  $Wcomp.b.d.t <Button-3> {cathfile0 compile_dirdisp $Wcomp}

label $Wcomp.b.m.l -width 15 -anchor w -text "makefile"
label $Wcomp.b.m.t -width 25 -anchor w
button $Wcomp.b.m.but -text "edit" -width 10 -height 1  -bg LightBlue1 \
    -command "editfile m" 
pack configure $Wcomp.b.m.l $Wcomp.b.m.t $Wcomp.b.m.but -in $Wcomp.b.m -anchor w -side left
bind  $Wcomp.b.m.l <Button-3> {cathfile0 compile_makedisp $Wcomp}
bind  $Wcomp.b.m.t <Button-3> {cathfile0 compile_makedisp $Wcomp}


########################################################################################
label $Wcomp.b.mm.l -width 15 -anchor w -text "select and run"

# file name selection box
frame $Wcomp.b.mm.sel
CreateLSBoxSBx $Wcomp.b.mm.sel "" "" 24  4 2 compile_filesel.hlp $Wcomp.d.tt "" "" compile_sel_mm sel
pack configure $Wcomp.b.mm.l  $Wcomp.b.mm.sel -in $Wcomp.b.mm -anchor w -side left

########################################################################################



label $Wcomp.b.e.l -width 15 -anchor w -text "dimension file"
label $Wcomp.b.e.t -width 25 -anchor w
button $Wcomp.b.e.but -text "edit" -width 10 -height 1  -bg LightBlue1 \
    -command "editfile e"
pack configure $Wcomp.b.e.l $Wcomp.b.e.t $Wcomp.b.e.but -in $Wcomp.b.e -anchor w -side left
bind  $Wcomp.b.e.l <Button-3> {cathfile0 compile_dimdisp $Wcomp}
bind  $Wcomp.b.e.t <Button-3> {cathfile0 compile_dimdisp $Wcomp}

label $Wcomp.b.s.l -width 15 -anchor w -text "source file"
label $Wcomp.b.s.t -width 25 -anchor w
button $Wcomp.b.s.but -text "edit" -width 10 -height 1  -bg LightBlue1 \
    -command "editfile s"
pack configure $Wcomp.b.s.l $Wcomp.b.s.t $Wcomp.b.s.but -in $Wcomp.b.s -anchor w -side left
bind  $Wcomp.b.s.l <Button-3> {cathfile0 compile_sourcedisp $Wcomp}
bind  $Wcomp.b.s.t <Button-3> {cathfile0 compile_sourcedisp $Wcomp}


########################################################################################
#   frame c for modification: d - directory, m - makefile, e - dim-file, s - source-file
########################################################################################

frame $Wcomp.c; pack configure $Wcomp.c -in $Wcomp -anchor w

frame $Wcomp.c.d;  frame $Wcomp.c.m;  frame $Wcomp.c.e;  frame $Wcomp.c.s
pack configure $Wcomp.c.d -in $Wcomp.c -side left -anchor se -ipadx 3
pack configure $Wcomp.c.m $Wcomp.c.e  $Wcomp.c.s -in $Wcomp.c -side left -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 
CreateLSBoxSBx $Wcomp.c.d "select directory" top 24 17 2 compile_dirsel.hlp $Wcomp.d.tt "" "" compile_sel_d sel
$Wcomp.c.d.f.f.li insert end "\$HOME" ".."
foreach x $prgdirlist { $Wcomp.c.d.f.f.li insert end "$x"}
foreach i [lsort [glob -nocomplain -- *]] {
    if [file isdirectory $i] {$Wcomp.c.d.f.f.li insert end $i}
}

# new directory insert
label $Wcomp.c.d.li -anchor w -text "directory:"
bind  $Wcomp.c.d.li <Button-3> {cathfile0 compile_diredi $Wcomp}
entry $Wcomp.c.d.i -width 29 -relief sunken
pack configure $Wcomp.c.d.li $Wcomp.c.d.i -in $Wcomp.c.d -side top -anchor w
bind  $Wcomp.c.d.i <Button-1> {compile_focus $Wcomp.c.d.i; set compile_foc 1}
bind  $Wcomp.c.d.i <Button-2> \
  {writescr0 $Wcomp.d.tt "compile_19  $dirprgwork\n"; $Wcomp.c.d.i delete 0 end; $Wcomp.c.d.i insert end $working_directory}
bind  $Wcomp.c.d.i <Button-3> {cathfile0 compile_diredi $Wcomp}
bind  $Wcomp.c.d.i <Return> {compile_edn; focus $Wcomp}


########################################################################################
#                                                                          makefile: c.m
########################################################################################
#
label $Wcomp.c.m.l -text "select makefile" -anchor w
bind  $Wcomp.c.m.l <Button-3> \
	{writescr0  $Wcomp.d.tt "left mouse click:\n"; cat_file ${hlp_dir}compile_filesel.hlp $Wcomp.d.tt}
pack configure $Wcomp.c.m.l -in $Wcomp.c.m -side top -anchor w

frame $Wcomp.c.m.1; # suffix-box and file-ordering-buttons
pack configure $Wcomp.c.m.1 -in $Wcomp.c.m -side top -anchor w -pady 2

frame $Wcomp.c.m.1.1; frame $Wcomp.c.m.1.2
pack configure $Wcomp.c.m.1.1 -in $Wcomp.c.m.1 -side left  -anchor sw -ipady 3
pack configure $Wcomp.c.m.1.2 -in $Wcomp.c.m.1 -side right -anchor ne -ipady 3 -padx 5

if ![info exists compile_sel_m_order_v] {set compile_sel_m_order_v "alphabetical"}
CreateLSBoxSBx $Wcomp.c.m.1.1 " " top 11 3 0 compile_filesel.hlp $Wcomp.d.tt \
	"" "\{alphabetical\} \{last access\}" compile_sel_m_order ""

# suffix selection box
CreateLSBox $Wcomp.c.m.1.2 "makefile suffix" top 9 3 2 compile_filesuff.hlp $Wcomp.d.tt \
	"" [vst2list makesuffix.vst 0 "" "" compile_sel_m_suffix_v] compile_sel_m_suffix sel

# file name selection box
CreateLSBoxSBx $Wcomp.c.m "" "" 24 12 2 compile_filesel.hlp $Wcomp.d.tt "" "" compile_sel_m sel

# insert new file name
label $Wcomp.c.m.li
entry $Wcomp.c.m.i -width 29 -relief sunken
pack configure $Wcomp.c.m.li $Wcomp.c.m.i -in $Wcomp.c.m -side top -anchor w
bind  $Wcomp.c.m.li <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.m.i <Button-1> {+ compile_focus $Wcomp.c.m.i; set compile_foc 2}
bind  $Wcomp.c.m.i <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.m.i <Return> {compile_emfn; focus $Wcomp}
bind  $Wcomp.c.m.i <Button-2> {
  writescr0 $Wcomp.d.tt "last file name:  $makefile\n"
  $Wcomp.c.m.i delete 0 end; $Wcomp.c.m.i insert end $makefile
}

########################################################################################
#                                                                   dimension-file: .c.e
########################################################################################
#
label $Wcomp.c.e.l -text "select dim-file" -anchor w
bind  $Wcomp.c.e.l <Button-3> \
	{writescr0 $Wcomp.d.tt "left mouse click:\n"; cat_file ${hlp_dir}compile_filesel.hlp $Wcomp.d.tt}
pack configure $Wcomp.c.e.l -in $Wcomp.c.e -side top -anchor w

frame $Wcomp.c.e.1; # suffix-box and file-ordering-buttons
pack configure $Wcomp.c.e.1 -in $Wcomp.c.e -side top -anchor w -pady 2

frame $Wcomp.c.e.1.1; frame $Wcomp.c.e.1.2
pack configure $Wcomp.c.e.1.1 -in $Wcomp.c.e.1 -side left  -anchor sw -ipady 3
pack configure $Wcomp.c.e.1.2 -in $Wcomp.c.e.1 -side right -anchor ne -ipady 3 -padx 5

if ![info exists compile_sel_e_order_v] {set compile_sel_e_order_v "alphabetical"}
CreateLSBoxSBx $Wcomp.c.e.1.1 " " top 11 3 0 compile_filesel.hlp $Wcomp.d.tt \
	"" "\{alphabetical\} \{last access\}" compile_sel_e_order ""

# suffix selection box
CreateLSBox $Wcomp.c.e.1.2 "dim-file suffix" top 9 3 2 compile_filesuff.hlp $Wcomp.d.tt \
	"" [vst2list dimensionsuffix.vst 0 "" "" compile_sel_e_suffix_v] compile_sel_e_suffix sel

# file name selection box
CreateLSBoxSBx $Wcomp.c.e "" "" 24 12 2 compile_filesel.hlp $Wcomp.d.tt "" "" compile_sel_e sel

# insert new file name
label $Wcomp.c.e.li
entry $Wcomp.c.e.i -width 29 -relief sunken
pack configure $Wcomp.c.e.li $Wcomp.c.e.i -in $Wcomp.c.e -side top -anchor w
bind  $Wcomp.c.e.li <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.e.i <Button-1> {+ compile_focus $Wcomp.c.e.i; set compile_foc 3}
bind  $Wcomp.c.e.i <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.e.i <Return> {compile_eefn; focus $Wcomp}
bind  $Wcomp.c.e.i <Button-2> {
  writescr0 $Wcomp.d.tt "last file name:  $dimfile$dimsuffix\n"
  $Wcomp.c.e.i delete 0 end
  if {$dimfile==""} {writescr0 $Wcomp.d.tt "last file name:  $dimfile\n"; $Wcomp.c.e.i insert end $dimfile} \
     else {writescr0 $Wcomp.d.tt "last file name:  $dimfile$dimsuffix\n"; $Wcomp.c.e.i insert end $dimfile$dimsuffix}
}


########################################################################################
#                                                                      source-file: .c.s
########################################################################################

label $Wcomp.c.s.l -text "select and edit source file" -anchor w
bind  $Wcomp.c.s.l <Button-3> \
    {writescr0 $Wcomp.d.tt "left mouse click:\n"; cat_file ${hlp_dir}compile_filesel.hlp $Wcomp.d.tt}
pack configure $Wcomp.c.s.l -in $Wcomp.c.s -side top -anchor w

frame $Wcomp.c.s.1; # suffix-box and file-ordering-buttons
pack configure $Wcomp.c.s.1 -in $Wcomp.c.s -side top -anchor w -pady 2

frame $Wcomp.c.s.1.1; frame $Wcomp.c.s.1.2
pack configure $Wcomp.c.s.1.1 -in $Wcomp.c.s.1 -side left  -anchor sw -ipady 3
pack configure $Wcomp.c.s.1.2 -in $Wcomp.c.s.1 -side right -anchor ne -ipady 3 -padx 5

if ![info exists compile_sel_s_order_v] {set compile_sel_s_order_v "alphabetical"}
CreateLSBoxSBx $Wcomp.c.s.1.1 " " top 11 3 0 compile_filesel.hlp $Wcomp.d.tt \
    "" "\{alphabetical\} \{last access\}" compile_sel_s_order ""

# suffix selection box
CreateLSBox $Wcomp.c.s.1.2 "source file suffix" top 9 3 2 compile_filesuff.hlp $Wcomp.d.tt \
    "" [vst2list  sourcesuffix.vst 0 "" "" compile_sel_s_suffix_v] compile_sel_s_suffix sel

# file name selection box
CreateLSBoxSBx $Wcomp.c.s "" "" 24 12 2 compile_filesel.hlp $Wcomp.d.tt "" "" compile_sel_s sel

# insert new file name
label $Wcomp.c.s.li
entry $Wcomp.c.s.i -width 29 -relief sunken
pack configure $Wcomp.c.s.li $Wcomp.c.s.i -in $Wcomp.c.s -side top -anchor w
bind  $Wcomp.c.s.li <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.s.i  <Button-1> {+ compile_focus $Wcomp.c.s.i; set compile_foc 4}
bind  $Wcomp.c.s.i  <Button-3> {cathfile0 compile_fileedi $Wcomp}
bind  $Wcomp.c.s.i  <Return>   {compile_eefn; focus $Wcomp}
bind  $Wcomp.c.s.i  <Button-2> {
  writescr0 $Wcomp.d.tt "last file name:  $srcfile$srcsuffix\n"
  $Wcomp.c.s.i delete 0 end
  if {$srcfile==""} {writescr0 $Wcomp.d.tt "last file name:  $srcfile\n"; $Wcomp.c.s.i insert end $srcfile} \
     else {writescr0 $Wcomp.d.tt "last file name:  $srcfile$srcsuffix\n"; $Wcomp.c.s.i insert end $srcfile$srcsuffix}
}

########################################################################################




insert_textframe $Wcomp $tyh

# initialisation, focus etc.

compile_anzeige $Wcomp
$Wcomp.c.d.i delete 0 end; $Wcomp.c.m.i delete 0 end; $Wcomp.c.e.i delete 0 end; $Wcomp.c.s.i delete 0 end

ffuellen $Wcomp.c.m.f.f.li $makesuffix $mdatsort
ffuellen $Wcomp.c.e.f.f.li $dimsuffix  $edatsort
ffuellen $Wcomp.c.s.f.f.li $srcsuffix  $sdatsort
if {[file exists $makefile]} {mfuellen $Wcomp.b.mm.sel.f.f.li $makefile}

focus $Wcomp; set compile_foc 0
}
