      subroutine def_bz(rbas,iun)
c      subroutine def_bz(w,rbas,eps,nvert_m,nrib_m,nbrnk_m,inbrm
c     $     ,nvert,nrib,nbrnk,vert,iverib,ibrib
c     $     ,nvbr,lstbr,vort,qbas,volbz)

      parameter(NVERT_M=100,NBRNK_M=100,NRIB_M=100,INBRM=30)

c
c rbas - real space basis vectors
c nvert <= nvert_m - number of vertices
c nrib  <= nrib_m  - number of edges
c nbrnk <= nbrnk_m - number of faces
c inbrm - max. number of vertices per face
c volbz - volume of BZ
c
c      implicit double precision(a-h,p-z),integer(o)
c
c      integer w(*)
c

      dimension indvrt(nvert_m),indrib(nrib_m),indbrn(nbrnk_m)
      dimension ind1br(inbrm*nbrnk_m),cgb (3*nbrnk_m),ang(inbrm)
      dimension plg(3*inbrm)


      dimension rbas(3,3)               ! input
      dimension vert(3,nvert_m),iverib(2,nrib_m),ibrib(2,nrib_m) ! output
     $     ,nvbr(nbrnk_m),lstbr(inbrm,nbrnk_m),vort(3,NBRNK_M)
     $     ,qbas(3,3),vertn(3,nvert_m)
      dimension q(6),u_r(3,3),u_q(3) ! local
c

c      subroutine def_bz(w,rbas,eps,nvert_m,nrib_m,nbrnk_m,inbrm
c     $     ,nvert,nrib,nbrnk,vert,iverib,ibrib
c     $     ,nvbr,lstbr,vort,qbas,volbz)

      eps=1d-5
c      read*,((rbas(i,j),i=1,3),j=1,3)
      det=trnt(rbas(1,1),rbas(1,2),rbas(1,3))
      vol=abs(det)
      call cross(qbas(1,1),rbas(1,2),rbas(1,3))
      call cross(qbas(1,2),rbas(1,3),rbas(1,1))      
      call cross(qbas(1,3),rbas(1,1),rbas(1,2))
      do j=1,3
        do i=1,3
          qbas(i,j)=qbas(i,j)/det
        enddo
      enddo
      volbz=1.d0/vol
*
*=> 3) init polyhedron (parallelipipped)
*
      nvert=8
      nbrnk=6
      nrib=12
      call zbinit(qbas,rbas,nvert,nbrnk,inbrm,nrib
     $     ,vert,iverib,ibrib,nvbr,lstbr,q,qmax)
*
*=> 4) max. m1,m2,m3 for generation of recipr. lattice vectors
*
* orts along direct basis vectors
      do j=1,3
        rr=rbas(1,j)*rbas(1,j)+rbas(2,j)*rbas(2,j)+rbas(3,j)*rbas(3,j)
        rr=sqrt(rr)
        do k=1,3
          u_r(k,j)=rbas(k,j)/rr
        enddo
      enddo
      do j=1,3
        u_q(j)=qbas(1,j)*u_r(1,j)+qbas(2,j)*u_r(2,j)+qbas(3,j)*u_r(3,j)
      enddo
      m1=qmax/u_q(1)+1
      m2=qmax/u_q(2)+1
      m3=qmax/u_q(3)+1
      m1m2m3=m1+m2+m3
*
* => 5) Successive "cutting" of polyhedron
*
c
      rmax=qmax/2.d0
      vol=0.d0
      do n1=2,m1m2m3
        do i1=-n1,n1
          i1m=abs(i1)
          n2=n1-i1m
          do i2=-n2,n2
            i2m=abs(i2)
            n3=n2-i2m
            do i3=-n3,n3
              if(i1m+i2m+abs(i3).ne.n1)cycle
              ql=0.d0
              do k=1,3
                q(k)=qbas(k,1)*i1+qbas(k,2)*i2+qbas(k,3)*i3
                ql=ql+q(k)*q(k)
              enddo
              ql=sqrt(ql)
              d=0.5d0*ql
c d is greater then the most distant vertex
              if(d.gt.rmax)cycle
              do k=1,3
                q(k)=q(k)/ql
              enddo
c$$$              write(*,"(i3,' i',3i4,' q',4f10.5)")i1m+i2m+abs(i3)
c$$$     $             ,i1,i2,i3,(q(k),k=1,3),ql
              call polys(q,d,nvert_m,nvert,vert,indvrt,
     $             nrib_m,nrib,iverib,ibrib,indrib,
     $             nbrnk_m,nbrnk,nvbr,indbrn,
     $             inbrm,lstbr,ind1br,
     $             eps,n_new,rmax)
c vopoly should be called at least ones
              if(vol.gt.eps.and.n_new.lt.3)cycle
c              
              call vopoly(nvert,vert,nbrnk,nvbr,inbrm
     $             ,lstbr,vort,cgb,ang,plg,vol)
c$$$              write(*,*)'vol,volbz',vol,volbz
              if(abs(vol-volbz).lt.eps)goto 10
            enddo                       ! i3
          enddo                         ! i2
        enddo                           ! i1
      enddo                             ! n1
 10   continue
      call zbprint(nvert,nbrnk,inbrm,vert,
     $     nvbr,lstbr,lstbr,vort,vertn,iun)      
      write(iun,'(3f20.6)')((qbas(j,i),j=1,3),i=1,3)
      end
c
      subroutine zbinit(QBAS,rbas, NVERT,NBRNK,INBRM,NRIB,
     >    vert,Iverib,Ibrib, NVBR,LSTBR, QQ,QMAX)
c      implicit double precision(a-h,o-z)
* 19.03- 20.03.99 Yavorsky
* Last 13.04.99
      dimension QBAS(3,3),rbas(3,3),vert(3,NVERT), QQ(6),
     ,Iverib(2,NRIB),Ibrib(2,NRIB),NVBR(NBRNK),LSTBR(INBRM,NBRNK)
*
* a) squared moduls of recipr.basis vectors (metric tensor)
      Do j=1,3
         QQ(j)=QBAS(1,j)*QBAS(1,j)+
     +         QBAS(2,j)*QBAS(2,j)+
     +         QBAS(3,j)*QBAS(3,j)
      EndDo
      Do i=1,3
         j_=i+1
         if(j_.gt.3)j_=1
         J=i+3
         QQ(J)=QBAS(1,i)*QBAS(1,j_)+
     +         QBAS(2,i)*QBAS(2,j_)+
     +         QBAS(3,i)*QBAS(3,j_)
      EndDo
c      print*,' QQ:',QQ
* b) verteces
      iv=0
      DO I3=-1,1,2
      DO I2=-1,1,2
      DO I1=-1,1,2
         iv=iv+1
         do k=1,3
            vert(k,iv)=0.5*(I1*QQ(1)*rbas(k,1)+
     +                      I2*QQ(2)*rbas(k,2)+
     +                      I3*QQ(3)*rbas(k,3))
         enddo
      ENDDO
      ENDDO
      ENDDO
* c) ribs
      N_r=0
      Do iv=1,7,2
         N_r=N_r+1
         Iverib(1,N_r)=iv
         Iverib(2,N_r)=iv+1
      EndDo
      Do iv=1,4
         N_r=N_r+1
         Iverib(1,N_r)=iv
         Iverib(2,N_r)=iv+4
      EndDo
      Do iv=1,2
         N_r=N_r+1
         Iverib(1,N_r)=iv
         Iverib(2,N_r)=iv+2         
      EndDo
      Do iv=5,6
         N_r=N_r+1
         Iverib(1,N_r)=iv
         Iverib(2,N_r)=iv+2         
      EndDo
c      print*,((Iverib(k,i),k=1,2),':',i=1,12)
      N_r=0
      Do i1=1,2
         Do i2=5,6
            N_r=N_r+1
            Ibrib(1,N_r)=i1
            Ibrib(2,N_r)=i2
         EndDo
      EndDo
      Do i2=5,6
         Do i1=3,4
            N_r=N_r+1
            Ibrib(1,N_r)=i1
            Ibrib(2,N_r)=i2
         EndDo
      EndDo
      Do i1=1,2
         Do i2=3,4
            N_r=N_r+1
            Ibrib(1,N_r)=i1
            Ibrib(2,N_r)=i2
         EndDo
      EndDo
c      print*,((Ibrib(k,i),k=1,2),':',i=1,12)
* d) brinks
      do ibr=1,NBRNK
         NVBR(ibr)=4
      enddo
      Do iv=1,4
         LSTBR(iv,1)=iv
         LSTBR(iv,2)=iv+4
      EndDo
      i_=0
      Do iv=1,7,2
         i_=i_+1
         LSTBR(i_,3)=iv
         LSTBR(i_,4)=iv+1
      EndDo
      iv=0
      Do ir=5,6
         do i_=1,2
            iv=iv+1
            LSTBR(i_  ,ir)=iv
            LSTBR(i_+2,ir)=iv+4            
         enddo
      EndDo
c      print*,((LSTBR(k,i),k=1,4),':',i=1,6)
* e) QMAX (max diagonal)
      DD1=(vert(1,8)-vert(1,1))*(vert(1,8)-vert(1,1))+
     +    (vert(2,8)-vert(2,1))*(vert(2,8)-vert(2,1))+
     +    (vert(3,8)-vert(3,1))*(vert(3,8)-vert(3,1))
      DD2=(vert(1,5)-vert(1,4))*(vert(1,5)-vert(1,4))+
     +    (vert(2,5)-vert(2,4))*(vert(2,5)-vert(2,4))+
     +    (vert(3,5)-vert(3,4))*(vert(3,5)-vert(3,4))
      DD3=(vert(1,6)-vert(1,3))*(vert(1,6)-vert(1,3))+
     +    (vert(2,6)-vert(2,3))*(vert(2,6)-vert(2,3))+
     +    (vert(3,6)-vert(3,3))*(vert(3,6)-vert(3,3))
      DD4=(vert(1,7)-vert(1,2))*(vert(1,7)-vert(1,2))+
     +    (vert(2,7)-vert(2,2))*(vert(2,7)-vert(2,2))+
     +    (vert(3,7)-vert(3,2))*(vert(3,7)-vert(3,2))
      QMAX=max(DD1,DD2,DD3,DD4)
      QMAX=SQRT(QMAX)
      end

      subroutine zbprint(nvert,nbrnk,inbrm,vert,
     $     nvbr,lstbr,ifaces,vort,vertn,iun)
c      implicit double precision(a-h,o-z)
c
      dimension vert(3,nvert),nvbr(nbrnk),lstbr(inbrm,nbrnk)
     $     ,ifaces(inbrm,nbrnk),vort(3,*),vertn(3,nvert)
*
      write(iun,"(i4)")nvert
      do j=1,nvert
        vertn(1,j)=0
        vertn(2,j)=0
        vertn(3,j)=0
      enddo
      do j=1,nbrnk
        do i=1,nvbr(j)
          k=ifaces(i,j)
          vertn(1,k)=vertn(1,k)+vort(1,j)
          vertn(2,k)=vertn(2,k)+vort(2,j)
          vertn(3,k)=vertn(3,k)+vort(3,j)
        enddo
      enddo
      do j=1,nvert
        vabs=sqrt(vertn(1,j)**2+vertn(2,j)**2+vertn(3,j)**2)
        write(iun,"(6f12.7)")(vert(i,j),i=1,3),(vertn(i,j)/vabs,i=1,3)
      enddo
      write(iun,"(i4)")nbrnk
      do j=1,nbrnk
        write(iun,"(i3,2x,12i3)")nvbr(j),(ifaces(i,j),i=1,nvbr(j))
c        write(*,"(3f12.7)")(vort(i,j),i=1,3)
      enddo
      end
      subroutine polys(v,d,nvert_m,nvert,vert,indvrt,
     ,     nrib_m,nrib,iverib,ibrib,indrib,
     ,     nbrnk_m,nbrnk,nvbr,indbrn,inbrm,lstbr,ind1br,
     ,     eps,n_new,rmax)
c      implicit double precision(a-h,o-z)
** section of polyhedron by plane (will be)
c defined by its normal vector v and distance d.
** 13.04.99- 14.04.99 yavorsky
      dimension v(3)                    ! input
      dimension vert(3,nvert_m),iverib(2,nrib_m) ! input-output
     $     ,ibrib(2,nrib_m),nvbr(nbrnk_m),lstbr(inbrm,nbrnk_m)
      dimension indvrt(nvert_m),indrib(nrib_m),indbrn(nbrnk_m) ! local
     $     ,ind1br(inbrm,nbrnk_m),w1(3),w2(3)
*
*=>  0) inits
      ipr=0
c check vertex position relative to the plane
c n_new here is the number of vertices inside the half-space
      n_new=0
      do iv=1,nvert
        d1=v(1)*vert(1,iv)+v(2)*vert(2,iv)+v(3)*vert(3,iv)
        if(abs(d1-d).lt.eps)then        ! vertex is in the plane
          indvrt(iv)=0
        elseif(d1.gt.d)then             ! vertex is outside
          indvrt(iv)=-1
        else
          indvrt(iv)=2                  ! vertex is inside
          n_new=n_new+1
        endif
      enddo                             ! iv
c all the vertices are at or outside the cutting plane
      if(n_new.eq.0)return
c set indrib to sum of indvrt for its ends
c indrib<0 - edge is outside
c indrib=0 - edge lies in the plane
c indrib=1 - edge is crossed by the plane
c indrib>1 - edge is inside
      do ir=1,nrib
        indrib(ir)=indvrt(iverib(1,ir))+indvrt(iverib(2,ir))
      enddo
c find new vertex as crossings of edges with the plane
      n_vert=nvert
      do ir=1,nrib
        if(indrib(ir).eq.1)then
          do k=1,3
            w1(k)=vert(k,iverib(1,ir))
            w2(k)=vert(k,iverib(2,ir))
          enddo
          d1=w1(1)*v(1)+w1(2)*v(2)+w1(3)*v(3)
          d2=w2(1)*v(1)+w2(2)*v(2)+w2(3)*v(3)
          n_vert=n_vert+1
          if(n_vert.gt.nvert_m)then
            print *,'POLYS: Too many vertices. Increase nvert_m !'
            stop
          endif
          indvrt(n_vert)=0
          a=(d-d2)/(d1-d2)
          do k=1,3
            vert(k,n_vert)=w1(k)*a+w2(k)*(1.d0-a)
          enddo
          if(indvrt(iverib(1,ir)).lt.0)then
            iverib(1,ir)=n_vert
          else
            iverib(2,ir)=n_vert
          endif
          indrib(ir)=indvrt(iverib(1,ir))+indvrt(iverib(2,ir))
        endif
      enddo                             ! ir
      nvert=n_vert
c n_new is the number of verices lying in the cutting plane
      n_new=0
      do iv=1,n_vert
        if(indvrt(iv).eq.0)n_new=n_new+1
      enddo                             ! iv
      if(n_new.lt.3)return
c An edge is removed if one end is outside the cutting plane and
c another one at or outside.
      n_rib=0
      do ir=1,nrib
        if(indrib(ir).ge.0)then
          n_rib=n_rib+1
c correct indrib is needed for faces
          indrib(n_rib)=indrib(ir)
          do k=1,2
            iverib(k,n_rib)=iverib(k,ir)
            ibrib(k,n_rib)=ibrib(k,ir)
          enddo
        endif
      enddo                             ! ir
      nrib=n_rib
c remove faces which are outside the cutting plane. A face is removed
c also if one edge is in the plane and the others are outside
      do ib=1,nbrnk_m
        indbrn(ib)=0
      enddo
      do ir=1,nrib
        if(indrib(ir).gt.0)then
          indbrn(ibrib(1,ir))=indbrn(ibrib(1,ir))+1
          indbrn(ibrib(2,ir))=indbrn(ibrib(2,ir))+1
        endif
      enddo                             ! ir
      n_brnk=0
      do ib=1,nbrnk
        if(indbrn(ib).gt.0)then
          n_brnk=n_brnk+1
          indbrn(ib)=n_brnk
c shift elements of nvbr and lstbr
          if(n_brnk.lt.ib)then
            nvbr(n_brnk)=nvbr(ib)
            do ivb=1,nvbr(n_brnk)
              lstbr(ivb,n_brnk)=lstbr(ivb,ib)
            enddo
          endif
        endif
      enddo                             ! ib
c change ibrib accordingly
      do ir=1,nrib
        ibrib(1,ir)=indbrn(ibrib(1,ir))
        ibrib(2,ir)=indbrn(ibrib(2,ir))
      enddo                             ! ir
      nbrnk=n_brnk
c reassign vertices to faces
      do ib=1,nbrnk_m
        nvbr(ib)=0
        do ivb=1,inbrm
          ind1br(ivb,ib)=0
        enddo
      enddo                             ! ib
      do ir=1,nrib
        do ivr=1,2
          iv=iverib(ivr,ir)
          do ibr=1,2
            ib=ibrib(ibr,ir)            ! a face containing the edge
            if(ib.eq.0)cycle            ! removed face
            do ivb=1,nvbr(ib)
              if(lstbr(ivb,ib).eq.iv)then
c the vertex already belongs to the face
                inb_r=ivb
                goto 10
              endif
            enddo
c new vertex added to the face ib
            nvbr(ib)=nvbr(ib)+1
            lstbr(nvbr(ib),ib)=iv
            inb_r=nvbr(ib)
 10         continue
c ind1br(ivb,ib) counts to how many faces belongs this end of the edge?
            ind1br(inb_r,ib)=ind1br(inb_r,ib)+1
          enddo                         ! ibr
        enddo                           ! ivr
      enddo                             ! ir
      n_rib=nrib
      do ib=1,nbrnk
        ind=0
        do ivb=1,nvbr(ib)
          if(ind1br(ivb,ib).eq.1)then
            ind=ind+1
            if(ind.gt.2)then
              stop 'POLYS: error ind>2'
            elseif(ind.eq.1)then
              n_rib=n_rib+1
              if(n_rib.gt.nrib_m)then
                print*,' Too many ribs, do increase NRIB_M!'
                stop
              endif
              iverib(1,n_rib)=lstbr(ivb,ib)
              ibrib(1,n_rib)=ib
              ibrib(2,n_rib)=nbrnk+1    ! second end is at the new face?
            else
              iverib(2,n_rib)=lstbr(ivb,ib)
            endif
          elseif(ind1br(ivb,ib).ne.2)then
            stop 'POLYS: error ind1br!'
          endif
        enddo
      enddo                             ! ib
      nrib=n_rib
c new face
      n_brnk=n_brnk+1
      if(n_brnk.gt.nbrnk_m)then
        print *,'Too many brinks. Increase NBRNK_M !'
        stop
      endif
c empty ends belong to the new face
      do ir=1,nrib
        if(ibrib(1,ir).eq.0)ibrib(1,ir)=n_brnk
        if(ibrib(2,ir).eq.0)ibrib(2,ir)=n_brnk
      enddo                             ! ir
c add verices which lie in the cutting plane to the list for the new
c face
      nvb=0
      do iv=1,nvert
        if(indvrt(iv).eq.0)then
          nvb=nvb+1
          lstbr(nvb,n_brnk)=iv
        endif
      enddo                             ! iv
      nvbr(n_brnk)=nvb
      nbrnk=n_brnk
c=>  5) avoid excessive vertices and change arrays of pointers to vertex
c coordinates for edges and faces
      n_vert=0
      do iv=1,nvert
        if(indvrt(iv).ge.0)then
          n_vert=n_vert+1
          indvrt(iv)=n_vert
          do k=1,3
            vert(k,n_vert)=vert(k,iv)
          enddo
        endif
      enddo                             ! iv
      do ir=1,nrib
        iverib(1,ir)=indvrt(iverib(1,ir))
        iverib(2,ir)=indvrt(iverib(2,ir))
      enddo
      do ib=1,nbrnk
        do ivb=1,nvbr(ib)
          lstbr(ivb,ib)=indvrt(lstbr(ivb,ib))
        enddo
      enddo
      nvert=n_vert
c the most distant vertex
      rmax=0.d0
      do iv=1,nvert
        rmax=max(rmax,vert(1,iv)**2+vert(2,iv)**2+vert(3,iv)**2)
      enddo
      rmax=sqrt(rmax)
      end
      Subroutine Vopoly(NVERT,Vert,NBRNK,NVBR,INBRM,LSTBR,
     ,     Vort,CGB, Ang,PLG, VOLY)    
c      implicit double precision(a-h,o-z)
      parameter (eps=1.d-10)
** Volume of convex polyherdon.
*  Input:
*      Vert(3,NVERT) - NVERT coords. of vertices
*      NBRNK - numb. of brinks
*      NVBR(NBRNK) - numb. of verteces in each brink
*      INBRM - max. numb. of verteces in each brink
*      LSTBR(INBRM,NBR) - pointer to verteces in NBR brink
*      Ang(INBRM),PLG(3,NBRM) - work arrays for ORDANG (see below)
* Output:
*      Vort(3,NBRNK) - outer ort normal to each brink
*       CGB(3,NBRNK) - center gravity of each brink
*      VOLY - volume of polyhedron.
**
* 29.03.99 Yavorsky
*
      Dimension Vert(3,NVERT),NVBR(NBRNK),LSTBR(INBRM,NBRNK),
     ,     Vort(3,NBRNK),CGB(3,NBRNK),Ang(INBRM),PLG(3,INBRM),
     ,     O(3),A(3),B(3),C(3)
*
c center of gravity of the polyhedron
      do k=1,3
        o(k)=0.d0
      enddo
      do iv=1,nvert
        do k=1,3
          o(k)=o(k)+vert(k,iv)
        enddo
      enddo
      do k=1,3
        o(k)=o(k)/nvert
      enddo
** ??????
      voly=0.d0
      do ib=1,nbrnk
c$$$        print *,'Before ordering'
c$$$        print "(' face',i3,' containes',i3,' vertices:')",ib,nvbr(ib)
c$$$        print "(24i3)",(lstbr(ivb,ib),ivb=1,nvbr(ib))
        do ivb=1,nvbr(ib)
          do k=1,3
            plg(k,ivb)=vert(k,lstbr(ivb,ib))
          enddo
        enddo
        call ordang(nvbr(ib),plg,o,vort(1,ib),cgb(1,ib),ang,1)
c reoder list of verices for the face
        do ivb=1,nvbr(ib)
          do iv=1,nvert
            if(dst2(vert(1,iv),plg(1,ivb)).lt.eps)then
              lstbr(ivb,ib)=iv
              exit
            endif
          enddo
          if(iv.gt.nvert)then
            write(*,*)'VOPOLY: can not find vertex. ib=',ib,ivb
            stop
          endif
        enddo                           ! i
c$$$        print *,'After ordering'
c$$$        print "(24i3)",(lstbr(ivb,ib),ivb=1,nvbr(ib))
c volume of the polygon
        do k=1,3
          c(k)=cgb(k,ib)-o(k)
        enddo
        do i=1,nvbr(ib)
          i1=i
          i2=i+1
          if(i2.gt.nvbr(ib))i2=1
          do k=1,3
            b(k)=plg(k,i1)-o(k)
            a(k)=plg(k,i2)-o(k)
          enddo
c            print*,'i,i1,i2:',i,i1,i2
c            print*,' A:',A
c            print*,' B:',B
c            print*,' C:',C
          v1=trnt(a,b,c)/6.d0
c            print*,' v1=',v1
          voly=voly+abs(v1)
        enddo                           ! i
c         print*,IB,' voly=',voly
      enddo                             ! ib
      end
c
c
      function dst2(a,b)
c      implicit real*8 (a-h,o-z)
      dimension a(3),b(3)
c
      dst2=(a(1)-b(1))**2+(a(2)-b(2))**2+(a(3)-b(3))**2
      end
      subroutine ordang(nsp,s_p,o,v,cg,ang,key)
c      implicit double precision(a-h,o-z)
** Sort set of points, lying in one plane, on angle
** i.e. construct convex (!) polygon.
*  input:
*      S_P(3,NPS)- coords. of NSP point to order
*      O(3) - "global" center of gravity of the polyhedron (see below)
*      ! if key=0 v(3)- ort norm. to plane on input
* output:
*      S_P - ordered by angle ang(NSP)
*      cg(3)- cent.grav. of the polygon
*      if key=1 v(3)- outer with respect to O(3) normal to plane
**
* 29.03.99 Yavorsky
*
      parameter(pi2=6.283185482025146d0)
      dimension S_P(3,NSP),O(3),v(3),cg(3),ang(NSP),A(3),B(3),C(3)
*
      if(nsp.lt.3)then
        print*,' !! Warning from ORDANG, too few points:',NSP
        return
      endif
* 1) cent.grav. of polygon
      do k=1,3
        cg(k)=0.d0
      enddo
      do isp=1,nsp
        do k=1,3
          cg(k)=cg(k)+s_p(k,isp)
        enddo
      enddo
      do k=1,3
        cg(k)=cg(k)/nsp
      enddo
      if(key.ne.0)then
* 2) determ. for outer normal to plane
        do k=1,3
          b(k)=s_p(k,2)-s_p(k,1)
          c(k)=s_p(k,3)-s_p(k,1)
          a(k)=cg(k)-o(k)
        enddo
        call cross(v,b,c)
        vvv=sqrt(v(1)*v(1)+v(2)*v(2)+v(3)*v(3))
        if(abs(vvv).lt.1.d-8)then
          print *,'vvv',vvv
          do j=1,3
            print "(i3,3f10.5)",j,(s_p(k,j),k=1,3)
          enddo
          print "(a,3f10.5)",'B',(b(k),k=1,3)
          print "(a,3f10.5)",'C',(c(k),k=1,3)
          stop
        endif

        do k=1,3
          v(k)=v(k)/vvv
        enddo
        av=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
        if(av.lt.0.d0)then
          do k=1,3
            v(k)=-v(k)
          enddo
        endif
      endif
c      print*,' Normal:',v
* 3) first point as 'reper' direction
      ang(1)=0.d0
      do k=1,3
        b(k)=s_p(k,1)-cg(k)
      enddo
      bb=b(1)*b(1)+b(2)*b(2)+b(3)*b(3)
      bb=sqrt(bb)
      if(abs(bb).lt.1.d-8)print *,'bb',bb
c b is a unit vector pointing from the CG to the first point
      do k=1,3
        b(k)=b(k)/bb
      enddo
* 3) angles with respect to 'reper'
      do ip=2,nsp
        do k=1,3
          c(k)=s_p(k,ip)-cg(k)
        enddo
        cc=c(1)*c(1)+c(2)*c(2)+c(3)*c(3)
        cc=sqrt(cc)
        if(abs(cc).lt.1.d-8)print *,'ip,cc',ip,cc
        do k=1,3
          c(k)=c(k)/cc
        enddo
        call cross(a,b,c)
        sn=a(1)*v(1)+a(2)*v(2)+a(3)*v(3)
        cs=b(1)*c(1)+b(2)*c(2)+b(3)*c(3)
***   ???
        one=sn*sn+cs*cs
        one=sqrt(one)
        sn=sn/one
        cs=cs/one
***   ???
c     print*,ip,' point, sn, cs:',sn,cs
        if(sn.ge.0.)then
          ang(ip)=acos(cs)
        else
          ang(ip)=pi2-acos(cs)
        endif
      enddo                             ! ip
c      print*,' angles before sorting'
c      do isp=1,nsp
c         print*,isp,' angle=',ang(isp),' point:',(s_p(k,isp),k=1,3)
c      enddo
* 4) ordering
      do isp=nsp-1,2,-1
        do j=2,isp
          if(ang(j).gt.ang(j+1))then
            ang_=ang(j)
            ang(j)=ang(j+1)
            ang(j+1)=ang_
            do k=1,3
              b(k)=s_p(k,j)
              s_p(k,j)=s_p(k,j+1)
              s_p(k,j+1)=b(k)
            enddo
          endif
        enddo
      enddo                             ! isp
c      print*,' angles AFTER sorting'
c      do isp=1,nsp
c         print*,isp,' angle=',ang(isp),' point:',(s_p(k,isp),k=1,3)
c      enddo
      end
      subroutine cross2(a,b,c)
c cross product (ax,ay,az)=(bx,by,bz)*(cx,cy,cz)
c      implicit double precision (a-h,o-z)
      dimension a(3),b(3),c(3)
c      
      a(1)=b(2)*c(3)-b(3)*c(2)
      a(2)=b(3)*c(1)-b(1)*c(3)
      a(3)=b(1)*c(2)-b(2)*c(1)
      end
      double precision function trnt(a,b,c)
c      implicit double precision (a-h,o-z)
      dimension a(3),b(3),c(3)
      trnt=a(1)*b(2)*c(3)+a(2)*b(3)*c(1)+a(3)*b(1)*c(2)
     $     -a(3)*b(2)*c(1)-a(2)*b(1)*c(3)-a(1)*b(3)*c(2)
      end
