# Aufr�umen
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

proc clear {f} {

#-------------------------------------------------------------------------------------------
 proc akanzeigen {} {
  global Wa aufmax aufsuff auftoggle
  writescr0 $Wa.d.tt "reset following items:!!\n\n length of the file list: $aufmax\n suffix list            : $aufsuff\n defaults               : $auftoggle\n\n"
  $Wa.d.tt yview 0
 }

global Wa hlp_dir auftoggle aufsuff sub geom
global ak_h delsuff nmax deldat dd dmax dmaxmax daus aufmax df df1 df2

set nmax [llength $aufsuff]

toplevel $Wa;  wm title $Wa "clean up";  wm minsize $Wa 0 0
if [info exists geom($Wa)] {wm geometry $Wa $geom($Wa)}

set tyh 18
set dir [pwd]

writescr0 $f ""
writescr0 $f ""


#-------------------------------------------------------------------------------------------
# Bereich a fuer erste Buttons

insert_topbuttons $Wa rm_h.hlp
bind $Wa.a.e <Button-1> {
  set auftoggle "";  set n 0;  while {$n<$nmax} {lappend auftoggle $ak_v($n); incr n}
  destros $Wa; unlock_list; Bend
}
bind   $Wa.a.e <Button-3> {cathfile0 rm_quit $Wa}
button $Wa.a.g -text "reset to defaults" -command {
  vstr_cleanup;  destros $Wa;  clear .d.tt;  akanzeigen 
}
bind   $Wa.a.g <Button-3> {cathfile0 rm_grund $Wa}
pack configure $Wa.a.e $Wa.a.h $Wa.a.l $Wa.a.g -in $Wa.a -side left -padx 3 -pady 3


#-------------------------------------------------------------------------------------------
# Bereich b fuer Anzeigen

frame $Wa.b; pack configure $Wa.b -in $Wa -pady 13 -anchor w
bind  $Wa.b    <Button-3> {cathfile0 rm_anzeig $Wa}
label $Wa.b.dv -text "current directory:  $dir" -width 90 -anchor w; pack configure $Wa.b.dv -in $Wa.b
bind  $Wa.b.dv <Button-3> {cathfile0 rm_anzeig $Wa}


# Bereich c fuer Suffix- und Datei-Listen

frame $Wa.c; pack configure $Wa.c -in $Wa -anchor w
frame $Wa.c.1; frame $Wa.c.2
pack configure $Wa.c.1 $Wa.c.2 -in $Wa.c -side left -fill x -ipadx 40


#-------------------------------------------------------------------------------------------
# Bereich c1 fuer Suffix-Liste

label $Wa.c.1.g -text "groups to be removed:"; pack configure $Wa.c.1.g -in $Wa.c.1 -anchor n
bind  $Wa.c.1.g <Button-3> {cathfile0 rm_suffix $Wa}
frame $Wa.c.1.fr; pack configure $Wa.c.1.fr -in $Wa.c.1 -anchor n

set nmax [llength $aufsuff]
set n 0

while {$n<$nmax} {
  checkbutton $Wa.c.1.fr.cb$n -relief raised -borderwidth 1 -variable ak_v($n) \
	  -text "  [lindex $aufsuff $n] " -width 10 -anchor w
  pack configure  $Wa.c.1.fr.cb$n -in $Wa.c.1.fr
  bind $Wa.c.1.fr.cb$n <Button-3> {cathfile0 rm_suffix $Wa}
  set delsuff($Wa.c.1.fr.cb$n) [lindex $aufsuff $n]
  set ak_h($Wa.c.1.fr.cb$n) [lindex $auftoggle $n]
  if {$ak_h($Wa.c.1.fr.cb$n)==1} {$Wa.c.1.fr.cb$n select} else {$Wa.c.1.fr.cb$n deselect}
  incr n
}

set daus 0
set n 0
while {$n<$nmax} {
  bind $Wa.c.1.fr.cb$n <Button-1> {
    #tkButtonDown %W;# (tk/tclversion dependent?)
    if {$ak_h(%W)==1} {set ak_h(%W) 0} else {set ak_h(%W) 1}
    set dmax 0
    while {$dmax<$dmaxmax} {
      if [string match "$delsuff(%W)" $deldat($dmax)] {
	if {($dd($dmax)==0)||($dd($dmax)==1)} {set dd($dmax) $ak_h(%W)}
      }
      incr dmax
    }
    $Wa.c.2.fr.f.f.li delete 0 end 
    set daus 0; set dmax 0; set df1 -1; set df2 -1
    while {$dmax<$dmaxmax} {
      if {$dd($dmax)==1} {
        if {$df1==-1} {set df1 $dmax}
        if {$daus<$aufmax} \
		{$Wa.c.2.fr.f.f.li insert end $deldat($dmax); incr daus; set df2 $dmax}
      }
      incr dmax
    }
  }
  incr n
}


#-------------------------------------------------------------------------------------------
# Bereich c2 fuer Datei-Anzeige

button $Wa.c.2.ad -width 39 -text "remove all the files shown here"
bind   $Wa.c.2.ad <Button-3> \
	"writescr0 $Wa.d.tt \"left mouse click:\n\"; cat_file ${hlp_dir}rm_files.hlp $Wa.d.tt"
frame  $Wa.c.2.fr -borderwidth 2
pack configure $Wa.c.2.ad $Wa.c.2.fr -in $Wa.c.2
CreateLSBoxSBx $Wa.c.2.fr "" "" 37 $aufmax 2 rm_files.hlp $Wa.d.tt "" "" "" sel0


insert_textframe $Wa $tyh; # Bereich d fuer Hilfe-Ausgaben


#-------------------------------------------------------------------------------------------
# Erstellen Dateiliste f�r L�schen (Dateien mit allen gesetzten Suffixen der Liste und core)

set dmaxmax 0;  set dirliste [lsort [glob -nocomplain -- *]]
foreach i $dirliste {
  if [file isfile $i] {
    if {($i=="core")} {set deldat($dmaxmax) $i; set dd($dmaxmax) 1; incr dmaxmax}
    for {set n 0} {$n<$nmax} {incr n} {
      if [string match "$delsuff($Wa.c.1.fr.cb$n)" $i] {
        set deldat($dmaxmax) $i
        if {$ak_h($Wa.c.1.fr.cb$n)==1} {set dd($dmaxmax) 1} else {set dd($dmaxmax) 0}
        incr dmaxmax
      }
    }
  }
}


#-------------------------------------------------------------------------------------------
# Ausgabe der zum Loeschen vorgesehenen Dateien

$Wa.c.2.fr.f.f.li delete 0 end
set daus 0; set dmax 0; set df1 -1; set df2 -1
while {$dmax<$dmaxmax} {
  if {$dd($dmax)==1} {
    if {$df1==-1} {set df1 $dmax}
    if {$daus<$aufmax} {$Wa.c.2.fr.f.f.li insert end $deldat($dmax);incr daus;set df2 $dmax}
  }
  incr dmax
}


#-------------------------------------------------------------------------------------------
#Bind fuer Datei-Selektion

bind  $Wa.c.2.fr.f.f.li <Button-1> {
  set d [Selektion %W %y]; selection clear %W
  if {$d!=""} {
    set df $df1
    while {$df<=$df2} {if {[string match "$deldat($df)" $d]} {set dd($df) -1}; incr df}
    $Wa.c.2.fr.f.f.li delete 0 end
    set df $df1
    while {$df<=$df2} {if {$dd($df)==1} {$Wa.c.2.fr.f.f.li insert end $deldat($df)}; incr df}
  }
  Bend
}


#-------------------------------------------------------------------------------------------
# bind fuer Datei-L�schen

bind $Wa.c.2.ad <Button-1> {
  if {$df1<0} {
    writescr $Wa.d.tt "file list is empty!\n"
  } else {
    set df $df1
    writescr0 $Wa.d.tt "the following files have been removed:"
    while {$df<=$df2} {
      if {$dd($df)==1} {
	writescr $Wa.d.tt "\n $deldat($df)"
	if {[file writable $deldat($df)]} {
           exec rm $deldat($df) 
        } else {
           writescr $Wa.d.tt "  not removable (write protected)"
        }
	set dd($df) -2
      }
      incr df
    }
    writescr $Wa.d.tt "\n"
    $Wa.c.2.fr.f.f.li delete 0 end
    set dmax $df2; set daus 0; set df1 -1; set df2 -1
    while {$dmax<$dmaxmax} {
      if {$dd($dmax)==1} {
	if {$df1==-1} {set df1 $dmax}
	if {$daus<$aufmax} \
		{$Wa.c.2.fr.f.f.li insert end $deldat($dmax); incr daus; set df2 $dmax}
      }
      incr dmax
    }
  }
}


focus $Wa
}
