
########################################################################################
proc patience {w key} {#               give a patience message on the screen

    set Wpat $w.patience  
    
    if {[winfo exists $Wpat]} {
	
	if {$key==0} {
	    $Wpat configure  -text "" -bg lightgrey -relief flat 
	} else { 
	    $Wpat configure  -bg yellow -fg black -text "PATIENCE PLEASE"  -relief raised
	} 
    } else {
	
	# label widget not available, skip silently
	
    }	
    
}

#
#
# use this proc if there is no space left in windows for dedicated empty label which is only changed 
# to "patience please" if required
#
# Wframe contains two subwidgets (children) 
# (should both be frames packable without parameters, possible when subsubwidgets requests the size) 
# one of these is named $subwidget which is used in patience 1/on case, 
# the other is used in case of patience 0/off
# these two subwidgets are dynamically repacked as required
#
#
# there might be a problem caused by the loss of packing parameters for child0 ?!
# for the present use its not a problem
#
proc patience_repack {Wframe onoff {subwidget patience}} {
    
    set children [winfo children $Wframe]
    if {[llength $children] == 2} {
	
	lassign $children child0 child1
	if {! [string match "*.$subwidget" $child1]} {
	    # swap children
	    set child0 $child1       ; # patience off
	    set child1 $subwidget    ; # patience on
	}
	
	if {$onoff == 1} { 
	    pack forget $child0
	    pack $child1 -fill x
	} else {
	    pack forget $child1
	    pack $child0
	}
	
	patience $child1 $onoff   
	update    ; # ensure text is visible

    } else {
	error "repack not possible, more than two subwidgets available" 
    }
    
}

