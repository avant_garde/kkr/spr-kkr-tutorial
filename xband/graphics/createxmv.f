C*==createxmv.f    processed by SPAG 6.05Rc at 14:23 on 18 Mar 2004
      SUBROUTINE CREATEXMV(DATSET,LDATSET,IDENT1,LIDENT1,IDENT2,LIDENT2,
     &                     FILNAM,LFNMAX,LFN,XMIN,KXMIN,XMAX,KXMAX,YMIN,
     &                     KYMIN,YMAX,KYMAX,ZMIN,KZMIN,ZMAX,KZMAX,XTXT,
     &                     LXTXT,YTXT,LYTXT,TITLE,LTITLE,SUBTITLE,
     &                     LSUBTITLE,KNOHEADER)
C **********************************************************************
C *                                                                    *
C *  DATSET    LDATSET     dataset name and length of string           *
C *  IDENT1    LIDENT1     add. identifier and length of string > 0 !! *
C *  IDENT2    LIDENT2     add. identifier and length of string        *
C *  FILNAM    LFNMAX LFN  file name to be created, its max. and       *
C *                        actual length on exit                       *
C *  XMIN      KXMIN       x-start and key to fix (0) or float (1)     *
C *  XMAX      KXMAX       x-end   and key to fix (0) or float (1)     *
C *  YMIN      KYMIN       y-start and key to fix (0) or float (1)     *
C *  YMAX      KYMAX       y-end   and key ...                         *
C *  ZMIN      KZMIN       z-start and key ...                         *
C *  ZMAX      KZMAX       z-end   and key ...                         *
C *  XTXT      LXTXT       text for x-axis label                       *
C *  YTXT      LYTXT       text for y-axis label                       *
C *  TITLE     LTITLE      title and length of string                  *
C *  SUBTITLE  LSUBTITLE   subtitle and length of string               *
C *  KNOHEADER             key to get (false) or suppress (true) title *
C *                                                                    *
C *  NOTE:  only  FILNAM and LFN will be changed on exit               *
C *         apart of IDENT1 any text may have length 0                 *
C *                                                                    *
C *         use '!' instead of '%' for the zmatrix escape character    *
C *         in the text variables --- see <XMVWRITE>, where also       *
C *         the macro list is stored  !UP, !DN,                        *
C *                                                                    *
C **********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LSTRMAX
      PARAMETER (LSTRMAX=80)
C
C Dummy arguments
C
      CHARACTER*(LSTRMAX) DATSET,FILNAM,IDENT1,IDENT2,SUBTITLE,TITLE,
     &                    XTXT,YTXT
      LOGICAL KNOHEADER
      INTEGER KXMAX,KXMIN,KYMAX,KYMIN,KZMAX,KZMIN,LDATSET,LFN,LFNMAX,
     &        LIDENT1,LIDENT2,LSUBTITLE,LTITLE,LXTXT,LYTXT
      REAL*8 XMAX,XMIN,YMAX,YMIN,ZMAX,ZMIN
C
C Local variables
C
      REAL*8 DX,DY,XA,XB,YA,YB
      INTEGER IFIL
C
C*** End of declarations rewritten by SPAG
C
C
C ----------------------------------------------------------------------
C         create filename   FILNAM(1:LFN)//'.xmv'   and open
C ----------------------------------------------------------------------
C
C
      CALL XMGRFNAM(DATSET,LDATSET,IDENT1,LIDENT1,IDENT2,LIDENT2,'.xmv',
     &              4,FILNAM,LFN,LFNMAX)
C
      IFIL = 80
      OPEN (IFIL,FILE=FILNAM(1:LFN))
C
C ----------------------------------------------------------------------
C                       set range  parameters
C ----------------------------------------------------------------------
C
      CALL XMGRTICKS(XMIN,XA,KXMIN,XMAX,XB,KXMAX,0.0D0,DX,1)
      CALL XMGRTICKS(YMIN,YA,KYMIN,YMAX,YB,KYMAX,0.0D0,DY,1)
C
C ----------------------------------------------------------------------
C                       write head of mathpack xmv - file
C ----------------------------------------------------------------------
C
C
      WRITE (IFIL,99003) '[S1.dataset,'
      WRITE (IFIL,99001) FILNAM(1:LFN-4)
      WRITE (IFIL,99003) ' (scene_coloring, 2),'
      WRITE (IFIL,99003) ' (theDataType, 1)'
      WRITE (IFIL,99003) ']'
      WRITE (IFIL,99003) '[G1,'
      WRITE (IFIL,99003) ' (iset, 1),'
      WRITE (IFIL,99003) ' (plotdim, 2),'
      WRITE (IFIL,99002) ' (plotype2d, ',206,'),'
      WRITE (IFIL,99003) ' (show, true)'
      WRITE (IFIL,99003) ']'
C
      WRITE (IFIL,99003) 
     &            '[G1.viewport,(viewport, MpRectangle(0.05,0,1,0.95))]'
C
C
C ---------------------------------------------------------------- title
      IF ( .NOT.KNOHEADER ) THEN
         WRITE (IFIL,99003) '[G1.annotations,'
         WRITE (IFIL,99003) ' (LabelList, [MpViewLabelData,'
         WRITE (IFIL,99003) ' (font, 3),'
         WRITE (IFIL,99003) ' (line_spacing, 1.2),'
         WRITE (IFIL,99003) ' (size, 1.4),'
C
C        ***WRITE (IFIL,99001) ' (text, "SPRKKR calculations for FeCo"),'
         CALL MPKWRITE(IFIL,' (text, ','),',TITLE,LTITLE)
C
         WRITE (IFIL,99003) ' (width, 5),'
         WRITE (IFIL,99003) ' (xpos, 0.5),'
         WRITE (IFIL,99003) ' (ypos, 0.97)'
         WRITE (IFIL,99003) ']'
C
C ------------------------------------------ subtitle
         WRITE (IFIL,99003) ','
         WRITE (IFIL,99003) '[MpViewLabelData,'
         WRITE (IFIL,99003) ' (font, 3),'
         WRITE (IFIL,99003) ' (line_spacing, 1.2),'
         WRITE (IFIL,99003) ' (size, 1.4),'
C
C        ***WRITE (IFIL,99001) ' (text, "total"),'
         CALL MPKWRITE(IFIL,' (text, ','),',SUBTITLE,LSUBTITLE)
C
         WRITE (IFIL,99003) ' (width, 5),'
         WRITE (IFIL,99003) ' (xpos, 0.5),'
         WRITE (IFIL,99003) ' (ypos, 0.93)'
         WRITE (IFIL,99003) ']'
         WRITE (IFIL,99003) ')'
         WRITE (IFIL,99003) ']'
      END IF
C
C not required
C ------------------------------------------------------------ tick step
C
C        WRITE (IFIL,99004) 'x',DX
C        WRITE (IFIL,99005) 'x',DX
C        WRITE (IFIL,99004) 'y',DY1,'y',DY1
C        WRITE (IFIL,99005) 'y',DY1,'y',DY1
C
C
      WRITE (IFIL,99003) '[G1.axes,'
      WRITE (IFIL,99003) ' (Xaxis, [MpAxis,'
      WRITE (IFIL,99003) ' (AxisBarArrowLabel, ""),'
      WRITE (IFIL,99003) ' (AxisBarWidth, 3),'
      WRITE (IFIL,99003) ' (AxisShow, true),'
      WRITE (IFIL,99003) ' (LabelDirection, 2),'
      WRITE (IFIL,99003) ' (LabelFont, 6),'
      WRITE (IFIL,99003) ' (LabelFormat, "%6G"),'
C     WRITE (IFIL,99001) ' (LabelMax, 1),'
C     WRITE (IFIL,99001) ' (LabelMin, 0),'
      WRITE (IFIL,99004) ' (LabelMax, ',XB,'),'
      WRITE (IFIL,99004) ' (LabelMin, ',XA,'),'
      WRITE (IFIL,99003) ' (LabelOpposite, false),'
      WRITE (IFIL,99003) ' (LabelShow, true),'
      WRITE (IFIL,99003) ' (LabelSize, 1),'
      WRITE (IFIL,99003) ' (LabelWidth, 5),'
      WRITE (IFIL,99003) ' (LabelZero, 1e-07),'
      WRITE (IFIL,99003) ' (LogBase, 0),'
C     WRITE (IFIL,99001) ' (Title, "wave vector (k) "),'
      CALL MPKWRITE(IFIL,' (Title, ','),',XTXT,LXTXT)
      WRITE (IFIL,99003) ' (TitleFont, 3),'
      WRITE (IFIL,99003) ' (TitleOffset, 1.3),'
      WRITE (IFIL,99003) ' (TitleShow, true),'
      WRITE (IFIL,99003) ' (TitleSize, 1.2),'
      WRITE (IFIL,99003) ' (TitleWidth, 5)'
      WRITE (IFIL,99003) ']'
      WRITE (IFIL,99003) '),'
      WRITE (IFIL,99003) ' (Yaxis, [MpAxis,'
      WRITE (IFIL,99003) ' (AxisBarArrowLabel, ""),'
      WRITE (IFIL,99003) ' (AxisBarWidth, 3),'
      WRITE (IFIL,99003) ' (AxisShow, true),'
      WRITE (IFIL,99003) ' (LabelDirection, 1),'
      WRITE (IFIL,99003) ' (LabelFont, 6),'
      WRITE (IFIL,99003) ' (LabelFormat, "%6G"),'
      WRITE (IFIL,99004) ' (LabelMax, ',YB,'),'
      WRITE (IFIL,99004) ' (LabelMin, ',YA,'),'
      WRITE (IFIL,99003) ' (LabelOpposite, false),'
      WRITE (IFIL,99003) ' (LabelShow, true),'
      WRITE (IFIL,99003) ' (LabelSize, 1),'
      WRITE (IFIL,99003) ' (LabelWidth, 5),'
      WRITE (IFIL,99003) ' (LabelZero, 1e-07),'
      WRITE (IFIL,99003) ' (LogBase, 0),'
C     WRITE (IFIL,99001) ' (Title, "E(k) (eV)"),'
      CALL MPKWRITE(IFIL,' (Title, ','),',YTXT,LYTXT)
      WRITE (IFIL,99003) ' (TitleFont, 3),'
      WRITE (IFIL,99003) ' (TitleOffset, 1.8),'
      WRITE (IFIL,99003) ' (TitleShow, true),'
      WRITE (IFIL,99003) ' (TitleSize, 1.2),'
      WRITE (IFIL,99003) ' (TitleWidth, 5)'
      WRITE (IFIL,99003) ']'
      WRITE (IFIL,99003) '),'
C      WRITE (IFIL,99001) ' (Zaxis, [MpAxis,'
C      WRITE (IFIL,99002) ' (LabelMax, ', ZMAX, '),'
C      WRITE (IFIL,99002) ' (LabelMin, ', ZMIN, '),'
C      WRITE (IFIL,99001) ']'
C      WRITE (IFIL,99001) '),'
      WRITE (IFIL,99003) ' (log_z, false)'
      WRITE (IFIL,99003) ']'
C     WRITE (IFIL,99001) ''
C
      CLOSE (IFIL)
C
99001 FORMAT (' (Import_Filename, "',A,'.mpk"),')
99002 FORMAT (A,I4,A)
99003 FORMAT (A)
99004 FORMAT (A,F12.6,A)
      END
C*==mpkwrite.f    processed by SPAG 6.05Rc at 14:23 on 18 Mar 2004
C
      SUBROUTINE MPKWRITE(IFIL,T1,T2,TXT,LTXT)
C **********************************************************************
C *                                                                    *
C *  write the text strings  T1, TXT and T2 - modify TXT if necessary  *
C *                                                                    *
C *  use ! as escape character will be replaced by % in the xmv-file   *
C *                                                                    *
C **********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LSTRMAX
      PARAMETER (LSTRMAX=80)
C
C Dummy arguments
C
      INTEGER IFIL,LTXT
      CHARACTER*(*) T1,T2,TXT
C
C Local variables
C
      INTEGER L
      CHARACTER*(LSTRMAX) T
C
C*** End of declarations rewritten by SPAG
C
      IF ( LTXT.LE.0 ) RETURN
C
      T = TXT
      L = LTXT
C
C ------------------ escape % sign (! is used as input escape character)
C
      CALL XMGRSUBS(T,L,'%',1,'%%',2)
C
C ----------------------------------------------- reset to original font
C
C  %n normal %R RomanBold (has to correspond to "(font, 3)" in xmv file
C
      CALL XMGRSUBS(T,L,'!F',2,'!n!R',4)
C
C ------------------------------------ replace macros for spin UP and DN
C
C %y Symbol font %82 character code 82 (up triangle) 84 (down triangle)
C look at xmatrix / ? / Information / Font Preview
C
C
      CALL XMGRSUBS(T,L,'!UP',3,'!y!82!n!R',9)
C
      CALL XMGRSUBS(T,L,'!DN',3,'!y!84!n!R',9)
C
C
C --------------------------- replace '!' by the proper escape character
      CALL XMGRSUBS(T,L,'!',1,'%',1)
C
      WRITE (IFIL,99001) T1,T(1:L),T2
99001 FORMAT (A,'"',A,'"',A)
      END
C*==mpktext.f    processed by SPAG 6.05Rc at 14:23 on 18 Mar 2004
C
      SUBROUTINE MPKTEXT(TXT,LTXT,LTXTMAX)
C **********************************************************************
C *                                                                    *
C *    change subscripts in  TXT  according to  XMGR syntax            *
C *    works only for concentrations like "A_23B_77" or "A_ .23B_ .77" *
C *    '!' is used as the xmatrix escape parameter -                   *
C *    it will be replaced later by   <MPKWRITE>                       *
C *                                                                    *
C **********************************************************************
C  %^  superscript
C  %_  subscript
C  %n  normal
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LTMAX
      PARAMETER (LTMAX=120)
C
C Dummy arguments
C
      INTEGER LTXT,LTXTMAX
      CHARACTER*(*) TXT
C
C Local variables
C
      CHARACTER C
      INTEGER I,I0,I9,IC,ID,K,L
      INTEGER ICHAR
      CHARACTER*(LTMAX) T
C
C*** End of declarations rewritten by SPAG
C
      IF ( LTXTMAX.GT.LTMAX ) STOP '1 in <MPKTEXT>'
C
      I0 = ICHAR('0')
      I9 = ICHAR('9')
      ID = ICHAR('.')
C
      K = 0
      L = 0
      DO I = 1,LTXT
         C = TXT(I:I)
         IC = ICHAR(C)
         IF ( C.EQ.'_' ) THEN
            K = 1
            T((L+1):(L+2)) = '!_'
            L = L + 2
         ELSE IF ( K.EQ.1 ) THEN
            IF ( C.NE.' ' ) THEN
               IF ( (IC.NE.ID) .AND. ((IC.LT.I0) .OR. (IC.GE.I9)) ) THEN
                  T((L+1):(L+2)) = '!n'
                  L = L + 2
                  K = 0
               END IF
               L = L + 1
               T(L:L) = C
            END IF
         ELSE
            L = L + 1
            T(L:L) = C
         END IF
C
      END DO
C
      IF ( L.GT.LTXTMAX ) STOP '2 in <MPKTEXT>'
      LTXT = L
      TXT(1:L) = T(1:L)
C
      END

