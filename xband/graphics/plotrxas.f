      SUBROUTINE PLOTRXAS( IFIL,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    KW,MLD,DATASET,LDATASET,
     &                    NLQ, NLT, NMIN,NMAX, 
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,RYOVEV,EUNIT,EREFER,
     &                    EMIN,EMAX,DE,MERGE,WGAUSS,WLOR,
     &                    NEEXP,NCEXP,NPOLEXP,EXPDATAVL,
     &                    IXRS,BXRS,NVMAX,WGTGAUTAB,WLORTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE, 
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,  
     &                    AD,AT,RD,RT, TD,TT,TE, KTABLE, SCLFAC, 
     &                    NOCORESPLIT, NOTABEXT,KNOHEADER )
C **********************************************************************
C *                                                                    *
C *    the programm reads in XAS - spectra from the  LMTO - or  KKR -  *
C *    package and broadens the spectra according the directives       *
C *                                                                    *
C *    relativistic version                                            *
C *                                                                    *
C *              NPOL = 2:        NPOL = 3:                            *
C *    IPOL =  1  <==>  (-)     1  <==>  (-)                           *
C *                             2  <==>  (+)                           *
C *            2  <==>  (+)     3  <==>  (z)                           *
C *                                                                    *
C *    MLD = FALSE    circular dichroism:   (+) - (-)                  *
C *          TRUE     linear   dichroism:  [(+) + (-)]/2 - (z)         *
C *                                                                    *
C **********************************************************************
c ----------------------------------------------------------------------
      PARAMETER ( LCMDMAX = 500 )
      PARAMETER ( LSTRMAX = 100 )
      PARAMETER ( IFTABLE = 98, IFCONVT = 99 )
      PARAMETER ( IFC=IFCONVT, LSM=LSTRMAX )
      PARAMETER ( EQUTOL= 1E-6 )
c ----------------------------------------------------------------------


      REAL         EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
      REAL         E(NEMAX)
      REAL         AD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             AT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             RD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX), 
     &             RT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             SD(:,:,:,:),
     &             SSR(:),OSR(:),
     &             E0(:)

      CHARACTER*80 YTXT0,YTXT1
      CHARACTER*80 HEADER,SYSTEM, INFO, FILNAM, STR80
      CHARACTER    SHELL(5)*1, SPEC*3, EXT*7, EUNIT*2,IDENT*4
      CHARACTER*10 TXTT(NTMAX), STR10
      CHARACTER*80 HEADT(:,:), STR80T(:,:)
      INTEGER LHEADT(:,:), LSTR80T(:,:)

      
      INTEGER LHEADER, LSYSTEM, TABNVAL
      INTEGER NLQ(NQMAX), NLT(NTMAX)
      INTEGER IQOFT(NQMAX,NTMAX), NQT(NTMAX), LTXTT(NTMAX)
      INTEGER NCXRAY(NTMAX), LCXRAY(NTMAX)
      INTEGER ITXRSGRP(NTMAX)
      INTEGER ICST1(2), ICST2(2), KTYP(2), KTYPCOR(NCSTMAX)
      INTEGER KAPCOR(NCSTMAX), MM05COR(NCSTMAX),
     &        NKPCOR(NCSTMAX),  IKMCOR(NCSTMAX,2)
      INTEGER IN(NEMAX), NEEXP(2)
      INTEGER ATOMNUMB
      
      REAL    IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX)
      REAL    WGTGAUTAB(0:NEMAX),VUC
      REAL    CONC(NTMAX),WLOR(2) 
      REAL    NMIN(0:NTMAX), NMAX(0:NTMAX)
      REAL    ECORE(NTMAX,NCSTMAX)
      REAL    TD(NEMAX),TT(NEMAX),TE(NEMAX), WLORTAB(NEMAX)
      REAL    ED(:),ET(:)
      REAL    SUMK(2), ECOREAV(2), RAV(2),RDF(2)
      CHARACTER*3 SUBSH(0:4), DICHROISM
      CHARACTER SUBSH2(0:4)*2, KW*10
      LOGICAL EXPDATAVL,CHIXAS, INTPOLEF

c ----------------------------------------------------------------------
      CHARACTER*(LCMDMAX) CMDUC, CMD00
      CHARACTER*(LSTRMAX) DATASET, STR
      LOGICAL TABLE, CMDFOUND, UDT, KTABLE, MLD, XRS, XMO, XAS, XES
      LOGICAL NOCORESPLIT, NOTABEXT, KNOHEADER
c ----------------------------------------------------------------------

      DATA    SHELL / 'K', 'L', 'M', 'N', 'O' /                  
      DATA    SUBSH / '1  ','2,3','4,5','6,7','8,9'/
      DATA    SUBSH2/ '1 ', '23', '45', '67', '89' /
      DATA IEX /0/

      ALLOCATABLE HEADT,E0,STR80T,ED,SD,ET,LHEADT,LSTR80T,OSR,SSR
      ALLOCATE (HEADT(0:NTMAX,2),E0(NEMAX),STR80T(0:NTMAX,2))
      ALLOCATE (ED(NEMAX),SD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX))
      ALLOCATE (ET(NEMAX),LHEADT(0:NTMAX,2),LSTR80T(0:NTMAX,2))
      ALLOCATE (OSR(NEMAX),SSR(NEMAX))

      IF( NOCORESPLIT ) THEN
         NOTABEXT    = .TRUE.
         KTABLE      = .TRUE.
      END IF
c      
      XAS  = .FALSE.         
      XES  = .FALSE.         
      XMO  = .FALSE. 
      XRS  = .FALSE. 
C        
      IF( KW(1:4) .EQ. 'RXAS' ) THEN 
         SPEC = 'XAS'
         XAS  = .TRUE.         
      ELSE IF( KW(1:4) .EQ. 'RXES' ) THEN 
         SPEC = 'XES'
         XES  = .TRUE.         
      ELSE IF( KW(1:3) .EQ. 'XMO' ) THEN 
         SPEC = 'XMO'
         XMO  = .TRUE.         
      ELSE IF( KW(1:3) .EQ. 'XRS' ) THEN 
         SPEC = 'XRS'
         XRS  = .TRUE.         
      ELSE IF( KW(1:6) .EQ. 'CHIXAS' ) THEN 
         SPEC = 'CHI'
         CHIXAS  = .TRUE.         
      ELSE 
         STOP 'in <PLOTRXAS>   wrong keyword KW'
      END IF

      ITCHECK = 1
      ITCHECK = 0
      IW      = 80

c ----------------------------------------------------------------------
      
      CALL       RDRXAS( SPEC,IFIL, NE, NPOL,E,RD,RT, MLD,RYOVEV,EUNIT,
     &                   ECORE,NCST,KAPCOR,MM05COR,NKPCOR,IKMCOR,
     &                   NCXRAY, LCXRAY, ITXRSGRP, NTXRSGRP, CHIXAS,VUC,
     &                   KUNIT,NEMAX,NCSTMAX,NSPMAX,NTMAX)

      IF( MLD ) THEN 
         DICHROISM = 'MLD'
      ELSE
         DICHROISM = 'MCD'
      END IF

      WRITE(6,9140) ' original  E-range: ',E(1),E(NE),EUNIT

c ------------------------------------------- adjust tables if necessary
      DO ITXG = 2,NTXRSGRP
         IF( ITXRSGRP(ITXG) .LE. ITXRSGRP(ITXG-1) ) THEN
           WRITE(*,*) 'ITXRSGRP(I) <= ITXRSGRP(I-1) for I=',ITXG
            STOP
         END IF
      END DO

      IF( CHIXAS ) THEN 
         NTXRSGRP = 5
         IT = ITXRSGRP(1)
         DO ITXG=2,NTXRSGRP
            ITXRSGRP(ITXG) = ITXRSGRP(ITXG-1)+1
            NCXRAY(ITXG)   = NCXRAY(1) 
            LCXRAY(ITXG)   = LCXRAY(1)
            DO ICST=1,NCST 
               ECORE(ITXG,ICST) = ECORE(1,ICST)
            END DO
            LTXTT(ITXG) = LTXTT(IT)
            TXTT(ITXG)  = TXTT(IT)
            NLT(ITXG)   = NLT(IT)
            NQT(ITXG)   = NQT(IT)
            CONC(ITXG)  = CONC(IT)
         END DO
     
      ELSE

         SUM = 0.0
         DO ITXG = 1,NTXRSGRP
            IT = ITXRSGRP(ITXG)
            IF( IT .LT. ITXG ) STOP 'IT < ITXG'
            WRITE(*,*) 'adjusting table for ITXG IT:',ITXG,IT
            LTXTT(ITXG) = LTXTT(IT)
            TXTT(ITXG)  = TXTT(IT)
            NLT(ITXG)   = NLT(IT)
            NQT(ITXG)   = NQT(IT)
            CONC(ITXG)  = CONC(IT)
            SUM = CONC(IT)
            DO II=1,NQT(ITXG)        
               IQOFT(II,ITXG) = IQOFT(II,IT)
            END DO
         END DO

      END IF 

      IF( ABS(SUM) .LT. EQUTOL ) THEN 
         DO ITXG = 1,NTXRSGRP
            CONC(ITXG)  = 1.0
         END DO
      END IF

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                            tabulate unmodified spectra
      IF( ITCHECK .NE. 0 ) THEN
         DO ICST=1,NCST
            DO IPOL=1,NPOL
               FILNAM = 'XA_raw_'
               LL     = 7
               CALL ADDNTOSTR( FILNAM, ITCHECK, LL )
               CALL ADDNTOSTR( FILNAM, ICST  , LL )
               CALL ADDNTOSTR( FILNAM, IPOL   , LL )
               OPEN( IW, FILE=FILNAM(1:LL)//".chk", STATUS='UNKNOWN' )
               WRITE(IW,9100) '#  unmodified spectrum IT ICST IPOL',
     &                         ITCHECK,ICST,IPOL
               DO IE=1,NE
                  WRITE(IW,'(3E15.7)') E(IE), RD(IE,ITCHECK,ICST,IPOL)
     &                                      , RT(IE,ITCHECK,ICST,IPOL) 
               END DO
            END DO
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

C -------------------------------------------------- sort  input E-table
      IN(1)=1
      DO 100 I=2,NE
         EI = E(I)
         IF( EI .LT. E(IN(I-1)) ) THEN
            N1 = 1
              DO 110 N=N1,(I-1)
              IF( EI .GE. E(IN(N)) ) GOTO 110
                 JJ= I
                 DO 120 J=N,(I-1)
                    IN(JJ)=IN(JJ-1)
                    JJ    = JJ - 1
  120            CONTINUE
                 IN(N) = I
                 GOTO 100
  110       CONTINUE
         END IF
 
         IN(I)= I
  100 CONTINUE
C
      DO I=2,NE
         JL= IN(I-1)
         J = IN(I)
         IF( (E(J)-E(JL)) .LT. 0.0 )
     &   WRITE(6,*) ' sorting NOT successful:',I,J,E(J), (E(J)-E(JL))
      END DO  
C -------------------------------------------------------- arrays sorted
C
      WRITE(6,9140) '   current E-range: ',E(IN(1)),E(IN(NE)),EUNIT
 
c XAS/CHIXAS   EREFER = E( 1) = E_FERMI(eV) read in READHEAD
c XES          EREFER = E(NE) = E_FERMI(eV) read in READHEAD
c XMO          EREFER =         E_FERMI(eV) read in READHEAD
c XRS          EREFER =         E_FERMI(eV) read in READHEAD

      DO ITXG=1,NTXRSGRP
         DO ICST=1,NCST 
            ECORE(ITXG,ICST) = ECORE(ITXG,ICST) - EREFER
         END DO
      END DO

      DIST = 1000.0

      DO I=1,NE
         J       = IN(I)
         TE(I) = E(J) - EREFER 
         IF( ABS(TE(I)-EF).LT.DIST ) THEN 
            DIST = ABS(TE(I)-EF)
            NEF = I
         END IF
      END DO

      IF( (DIST .LT. 1E-4) .OR. XMO .OR. XRS ) THEN
         INTPOLEF = .FALSE.
      ELSE
         INTPOLEF = .TRUE.
      END IF

      EF     = 0.0
      CALL CPR4VEC( E, TE, NE )
 
      DO IPOL = 1,NPOL
         DO ICST = 1,NCST
            DO ITXG = 1,NTXRSGRP
               DO IE=1,NE
                  TD(IE) = RD(IN(IE),ITXG,ICST,IPOL)
                  TT(IE) = RT(IN(IE),ITXG,ICST,IPOL)
               END DO    
               CALL CPR4VEC( RD(1,ITXG,ICST,IPOL), TD, NE )
               CALL CPR4VEC( RT(1,ITXG,ICST,IPOL), TT, NE )
            END DO  
         END DO  
      END DO  

C --------------------------------------------------- interpolate to E_F
      IF( INTPOLEF ) THEN
         I1 = NEF
         IF( ((NEF .EQ. 1) .OR. (E(NEF).LE.EF)) .AND. (NEF.LT.NE) ) THEN
            I2 = NEF + 1
         ELSE
            I2 = NEF - 1
         END IF
         W1 = (E(I2) - EF) / (E(I2) - E(I1))
         W2 = (EF - E(I1)) / (E(I2) - E(I1))
         DO IPOL = 1,NPOL
            DO ICST = 1,NCST
               DO ITXG = 1,NTXRSGRP
                  RD(I1,ITXG,ICST,IPOL) =  W1*RD(I1,ITXG,ICST,IPOL)
     &                                    +W2*RD(I2,ITXG,ICST,IPOL) 
                  RT(I1,ITXG,ICST,IPOL) =  W1*RT(I1,ITXG,ICST,IPOL)
     &                                    +W2*RT(I2,ITXG,ICST,IPOL) 
               END DO
            END DO
         END DO
         E(I1) = EF
      END IF          

      IF( XAS .OR. CHIXAS ) THEN 
C=================================================================== XAS
C ---------------------------------------------- XAS: set = 0 below  E_F
      IE1=1
      IE2=NEF-1
      DO IPOL = 1,NPOL
         DO ICST = 1,NCST
            DO ITXG = 1,NTXRSGRP
               DO IE  = IE1,IE2
                  RD(IE,ITXG,ICST,IPOL) = 0.0
                  RT(IE,ITXG,ICST,IPOL) = 0.0
               END DO
            END DO
         END DO
      END DO
      
C ---------------------------------------XAS: extend E-range  below  E_F
      IF( NOTABEXT ) THEN
         NADD =  0
      ELSE
         NADD = 50
      END IF
      NN   = NADD - NEF + 1
      IF( EUNIT .EQ. 'eV' ) THEN
         DELE = 25.0
      ELSE
         DELE = 25.0/RYOVEV
      END IF
          
      DO J=NE,1,-1
         I    = J + NN
         E(I) = E(J)
         DO IPOL = 1,NPOL
            DO ICST = 1,NCST
               DO ITXG = 1,NTXRSGRP
                  RD(I,ITXG,ICST,IPOL) = RD(J,ITXG,ICST,IPOL)
                  RT(I,ITXG,ICST,IPOL) = RT(J,ITXG,ICST,IPOL)
               END DO
            END DO
         END DO
      END DO
      NE  = NE  + NN
      NEF = NEF + NN 
      
      DO IE=1,NEF-1
         E(IE) = E(NEF) - DELE*(FLOAT(NEF-IE)/FLOAT(NADD))**3
         DO ICST = 1,NCST
            DO IPOL = 1,NPOL
               DO ITXG = 1,NTXRSGRP
                  RD(IE,ITXG,ICST,IPOL) = 0.0
                  RT(IE,ITXG,ICST,IPOL) = 0.0
               END DO
            END DO
         END DO
      END DO

      END IF
C=================================================================== XAS

      DO I=2,NE
         IF( ABS(E(I)-E(I-1)) .LT. EQUTOL ) E(I) = E(I)*1.00001
      END DO
 
      PRINT *,' '
      WRITE(6,9140) ' adjusted  E-range: ',E(1),E(NE),EUNIT
      WRITE(6,9142) EREFER,EF,E(NEF), NEF, DIST, INTPOLEF 

 9140 FORMAT(A,F10.4,:,' ... ',F10.4,' ',A)
 9142 FORMAT(/,'  EREFER    ',F10.4,' eV = E_Fermi (rel. VMTZ)',/,
     &       '  EF        ',F10.4,/,
     &       '  E(NEF)    ',F10.4,/,
     &       '  NEF       ',I10,/,
     &       '  DIST      ',F10.4,/,
     &       '  INTPOLEF  ',L10,/)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                  tabulate spectra after E-range adjust

      IF( ITCHECK .NE. 0 ) THEN
         DO ICST=1,NCST
            DO IPOL=1,NPOL
               FILNAM = 'XB_E_adjusted_'
               LL     = 14
               CALL ADDNTOSTR( FILNAM, ITCHECK, LL )
               CALL ADDNTOSTR( FILNAM, ICST   , LL )
               CALL ADDNTOSTR( FILNAM, IPOL   , LL )
               OPEN( IW, FILE=FILNAM(1:LL)//".chk", STATUS='UNKNOWN' )
               WRITE(IW,9100) 
     &         '#  spectrum after E-range adjust IT ICST IPOL',
     &                         ITCHECK,ICST,IPOL
               DO IE=1,NE
                  WRITE(IW,'(3E15.7)') E(IE), RD(IE,ITCHECK,ICST,IPOL)
     &                                      , RT(IE,ITCHECK,ICST,IPOL) 
               END DO
            END DO
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
                    
C ---------------------------------------- find KTYP for each core state
      NKTYP   = 0
      KTYP(1) = 0
      DO 210 ICST=1,NCST  
         DO K=1,NKTYP
            IF( KAPCOR(ICST) .EQ. KTYP(K) ) THEN
               KTYPCOR(ICST) = K
               GOTO 210
            END IF
         END DO
         NKTYP         = NKTYP + 1
         IF( NKTYP .GT. 2 ) THEN
            STOP ' NKTYP > 2  >>>>>> check core states'
         END IF
         KTYP(NKTYP)   = KAPCOR(ICST)
         KTYPCOR(ICST) = NKTYP
  210 CONTINUE

      IZ   = ATOMNUMB( TXTT(ITXRSGRP(1))(1:2) )
      IF( E(NE) .GT. 50.0 ) THEN
         NVAL = 0
      ELSE
         NVAL = TABNVAL( IZ )
      END IF
      CALL FINDCMD(5,'BROADEN',7,CMDUC,CMD00,LCMD,
     &                CMDFOUND,TABLE, LCMDMAX,IFTABLE)
      IF( CMDFOUND ) THEN
         CALL UPDATEINT(NVAL,'NVAL',4,CMDUC,LCMD,STR,LSM,IFC,UDT)
      END IF
      
      WRITE(6,9143) TXTT(ITXRSGRP(1))(1:2)
     &               //'  atomic number ',IZ,
     &              '    number of valence electrons ',NVAL
 9143 FORMAT(5X,A,I3,A,I3)

      IF( WLOR(1) .GT. 9000.0 )  THEN
         WRITE(6,'('' Lorentzian width taken from table '',/)')
         DO K=1,NKTYP
            WLOR(K) = WCOREHOLE( IZ,NCXRAY(1),LCXRAY(1),K )
         END DO  
      END IF
               
      DO K=1,NKTYP
         SUMK(K)   = 0
         ECOREAV(K)= 0.0
      END DO  

      IF( NOCORESPLIT ) THEN
         WRITE(*,*) ' ************ CORE SPLITTING WILL BE SUPPRESSED'
         DO ITXG=1,NTXRSGRP
            DO ICST=1,NCST
               ECORE(ITXG,ICST) = ECORE(1,1)
            END DO
         END DO
      END IF
               
 9020 FORMAT(' ICST KAP TYPE      ECORE    ',      
     &       ' W_LOR     W_GAU   (',A,')')

      DO ITXG=1,NTXRSGRP
         WGT= NQT(ITXRSGRP(ITXG))*CONC(ITXRSGRP(ITXG))
         wgt= 1.0
         write(*,*) ITXG,ITXRSGRP(ITXG),NQT(ITXRSGRP(ITXG)),
     *              CONC(ITXRSGRP(ITXG))
         DO ICST=1,NCST
            K          = KTYPCOR(ICST) 
            ECOREAV(K) = ECOREAV(K) + WGT * ECORE(ITXG,ICST) 
            SUMK(K)    = SUMK(K)    + WGT
            WRITE(6,599) ICST,KAPCOR(ICST),K,ECORE(ITXG,ICST),
     &                   WLOR(K), WGAUSS
         END DO  
      END DO  

  599 FORMAT(3I4,4X,F10.3,1X,F8.2,2X,F8.2)

      DO K=1,NKTYP
         ECOREAV(K)= ECOREAV(K) / SUMK(K) 
         WRITE(6,'(''  average:'',I2,4X,F10.3)') K, ECOREAV(K) 
      END DO  

      NCSUM = 0
      DO K=1,NKTYP
         NC = 0 
         ICST1(K) = 1000
         ICST2(K) =    0
         DO ICST=1,NCST
            IF( K .EQ. KTYPCOR(ICST) ) THEN
               NC = NC + 1
               ICST1(K) = MIN( ICST1(K), ICST )
               ICST2(K) = MAX( ICST2(K), ICST )
            END IF
         END DO  
         IF( NC .NE. (ICST2(K)-ICST1(K)+1) ) THEN
            WRITE(*,*) ' K       = ', K
            WRITE(*,*) ' ICST1/2 = ', ICST1(K),ICST2(K)
            WRITE(*,*) ' NC      = ',NC
            STOP '  ????????????????????????'
         END IF
         
         NCSUM = NCSUM + NC
      END DO  

      IF( NCSUM .NE. NCST ) THEN
          WRITE(*,*) ' NCST    = ',NCST 
          WRITE(*,*) ' NCSUM   = ',NCSUM
          STOP '  ????????????????????????'
      END IF

C ----------------------------------------------------------- broadening

      DO K=1,NKTYP   
         DO ITXG = 1,NTXRSGRP 
            NLT(ITXG) = ICST2(K)
         END DO  
         
         CALL    BROADEN( RD,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB,K,NVAL, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, 1,NTXRSGRP, ICST1(K),NLT, 1,NPOL )

         CALL    BROADEN( RT,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB,K,NVAL, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, 1,NTXRSGRP, ICST1(K),NLT, 1,NPOL )

      END DO
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                      tabulate spectra after broadening
      IF( ITCHECK .NE. 0 ) THEN
         DO ICST=1,NCST
            DO IPOL=1,NPOL
               FILNAM = 'XC_broadened_'
               LL     = 13
               CALL ADDNTOSTR( FILNAM, ITCHECK, LL )
               CALL ADDNTOSTR( FILNAM, ICST   , LL )
               CALL ADDNTOSTR( FILNAM, IPOL   , LL )
               OPEN( IW, FILE=FILNAM(1:LL)//".chk", STATUS='UNKNOWN' )
               WRITE(IW,9100) 
     &         '#  spectrum after broadening  IT ICST IPOL',
     &                         ITCHECK,ICST,IPOL
               DO IE=1,NE
                  WRITE(IW,'(3E15.7)') E(IE), RD(IE,ITCHECK,ICST,IPOL)
     &                                      , RT(IE,ITCHECK,ICST,IPOL) 
               END DO
            END DO
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


      DCORMIN = +100000.0
      DCORMAX = -100000.0
      DO ITXG=1,NTXRSGRP
         DO ICST=1,NCST
            DO JCST=1,NCST
               IF( KTYPCOR(ICST) .NE. KTYPCOR(JCST) ) THEN
                  D = ABS( ECORE(ITXG,ICST) - ECORE(ITXG,JCST) )
                  DCORMIN = MIN( D, DCORMIN )
                  DCORMAX = MAX( D, DCORMAX )
               END IF
            END DO  
         END DO  
      END DO  
      
      IF( NKTYP .GT. 1 )  PRINT *,' '
      KOVLAP = 0
      DO K1=        1,NKTYP
         DO K2=(K1+1),NKTYP 
            DCOR = ABS(ECOREAV(K1)-ECOREAV(K2))
            IF( DCOR .LT. (E(NE)-E(1)) ) THEN
               WRITE(*,9000) K1,K2, DCOR,EUNIT, 
     &                       DCORMIN,EUNIT, DCORMAX,EUNIT
 9000          FORMAT(  '  spectra of type ',I2,' and ',I2,
     &                  ' overlap:', 
     &                  '  DE_av  = ',F6.3,' ',A,/,
     &              36X,'  DE_min = ',F6.3,' ',A,/,
     &              36X,'  DE_max = ',F6.3,' ',A,/)
               KOVLAP  = 1
            END IF
         END DO  
      END DO  
      IF( KOVLAP .EQ. 1 .AND. MERGE .EQ. 9999 ) MERGE = 1
      IF( NKTYP  .LE. 1 .OR.  MERGE .EQ. 9999 ) MERGE = 0   
      IF( NOCORESPLIT )                         MERGE = 0
      NE0=NE
c ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
      IF( (NKTYP .GT. 1) .AND. (.NOT.XMO) .AND. (.NOT.XRS))  THEN
         PRINT *,' '
         PRINT *,' copying spectra to deal with SUM RULES'
         PRINT *,' '

         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               K           = KTYPCOR(ICST) 
               DE          = ECOREAV(K) - ECORE(ITXG,ICST)
               DO IPOL = 1,NPOL
                  DO I    = 1,NE    
                     EI = E(I) - DE
                     SD(I,ITXG,ICST,IPOL) = 
     &                       YLAG(EI,E,RD(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                  END DO  
               END DO  
            END DO  
         END DO

         DO I    = 1,NE    
            E0(I) = E(I)
         END DO  

         NE0 = NE

      END IF
c ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss


c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
      IF( MERGE .EQ. 0 ) THEN 
         PRINT *,' NO merging of spectra  '
         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               K           = KTYPCOR(ICST) 
               DE          = ECOREAV(K) - ECORE(ITXG,ICST)
               DO IPOL = 1,NPOL
                  DO I    = 1,NE    
                     EI = E(I) - DE
                     AD(I,ITXG,ICST,IPOL) = 
     &                       YLAG(EI,E,RD(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                     AT(I,ITXG,ICST,IPOL) = 
     &                       YLAG(EI,E,RT(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                  END DO  
               END DO  
            END DO  
         END DO  
      ELSE
c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
         KREF = 1
         KBOT = 1
         DO K=2,NKTYP
            IF( ECOREAV(K) .GT. ECOREAV(KREF) )   KREF = K
            IF( ECOREAV(K) .LT. ECOREAV(KBOT) )   KBOT = K
         END DO  
         WRITE(6,'(''  E_ref:  '',I2,4X,F10.3)') KREF, ECOREAV(KREF) 
         WRITE(6,'(''  E_bot:  '',I2,4X,F10.3)') KBOT, ECOREAV(KBOT) 

         CALL CPR4VEC( TE, E, NE )

         JE = 0
         DO IE=1,NE
            IF( E(IE) .GT. DCORMIN ) THEN 
               JE = IE - 1
               GOTO 565
            END IF
         END DO  
  565    CONTINUE 
         IF( JE .EQ. 0 ) STOP 'JE=0: no E(IE) > DCORMIN found'
         
         IF( JE + (NE-NEF+1) .GT. NEMAX ) THEN
            WRITE(*,*) ' NE gets too large by merging '
            WRITE(*,*) ' NE_new = ',JE + (NE-NEF+1) 
            WRITE(*,*) ' NEMAX  = ',NEMAX
            STOP
         END IF

         DO IE=NEF,NE
            JE = JE + 1
            TE(JE) = (E(IE)-E(NEF)) + DCORMIN
         END DO  
         NEREF = JE

         EBOT = -9999.0
         ETOP = +9999.0
         DO ICST = 1,NCST 
            DO ITXG = 1,NTXRSGRP
               K  = KTYPCOR(ICST) 
               DE = ECOREAV(KREF) - ECORE(ITXG,ICST)
               EBOT = MAX(EBOT,E(1)+DE)
               ETOP = MIN(ETOP,E(NE)+DE)
               DO I    = 1,NEREF
                  EI = TE(I) - DE
                  IF( EI .LT. E(1) ) THEN 
                     DO IPOL = 1,NPOL
                        AD(I,ITXG,ICST,IPOL) = 0.0
                        AT(I,ITXG,ICST,IPOL) = 0.0
                     END DO  
                  ELSE
                     IF( EI .GT. E(NE) ) THEN 
                        DO IPOL = 1,NPOL  
                           AD(I,ITXG,ICST,IPOL) = RD(NE,ITXG,ICST,IPOL)
                           AT(I,ITXG,ICST,IPOL) = RT(NE,ITXG,ICST,IPOL)
                        END DO  
                     ELSE
                        DO IPOL = 1,NPOL
                           AD(I,ITXG,ICST,IPOL) =
     &                        YLAG(EI,E,RD(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                           AT(I,ITXG,ICST,IPOL) =
     &                        YLAG(EI,E,RT(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                        END DO  
                     END IF
                  END IF
               END DO  
            END DO  
         END DO  
  
         NE = NEREF
         DO I=1,NE
            E(I) = TE(I)     
         END DO  

         WRITE(*,9001) NE
 9001    FORMAT(/,'  spectra were merged together on common E-scale ',/,
     &            '  new number of E-points    NE =', I5)

         DO I=1,NE
            IF( E(I) .LT. EBOT ) IEBOT = I + 1
            IF( E(I) .LT. ETOP ) IETOP = I 
         END DO

         J = 0
         DO I = IEBOT,IETOP
            J=J+1
            E(J)=E(I)
         END DO  
         NE = J
         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               DO IPOL = 1,NPOL
                  J = 0
                  DO I = IEBOT,IETOP
                     J=J+1
                     AD(J,ITXG,ICST,IPOL) = AD(I,ITXG,ICST,IPOL)
                     AT(J,ITXG,ICST,IPOL) = AT(I,ITXG,ICST,IPOL)
                  END DO  
               END DO  
            END DO  
         END DO  
                   

         WRITE(*,9002) EBOT,IEBOT, ETOP,IETOP, NE

 9002    FORMAT(/,'  spectra were cut according to common limits',/,
     &            '  common energy limit     EBOT =', F10.4,I5,/,
     &            '  common energy limit     ETOP =', F10.4,I5/,
     &            '  new number of E-points    NE =', I5,/)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                tabulate spectra after merging datasets

      IF( ITCHECK .NE. 0 ) THEN
         DO ICST=1,NCST
            DO IPOL=1,NPOL
               FILNAM = 'XD_merged_'
               LL     = 10
               CALL ADDNTOSTR( FILNAM, ITCHECK, LL )
               CALL ADDNTOSTR( FILNAM, ICST   , LL )
               CALL ADDNTOSTR( FILNAM, IPOL   , LL )
               OPEN( IW, FILE=FILNAM(1:LL)//".chk", STATUS='UNKNOWN' )
               WRITE(IW,9100) 
     &         '#  spectrum after merging datasets IT ICST IPOL',
     &                     ITCHECK,ICST,IPOL
               DO IE=1,NE
                  WRITE(IW,'(3E15.7)') E(IE), AD(IE,ITCHECK,ICST,IPOL)
     &                                      , AT(IE,ITCHECK,ICST,IPOL) 
               END DO
            END DO
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      END IF       
c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm


c --------------------- combine spectra with common KAPPA for core state
      DO K    = 1,NKTYP
         DO ITXG = 1,NTXRSGRP
            DO IPOL = 1,NPOL
               DO IE   = 1,NE    
                  RD(IE,ITXG,K,IPOL) = 0.0
                  RT(IE,ITXG,K,IPOL) = 0.0 
               END DO  
            END DO  
         END DO  
      END DO  

      DO ITXG = 1,NTXRSGRP
         DO ICST = 1,NCST
            K           = KTYPCOR(ICST) 
            DO IPOL = 1,NPOL
               DO I    = 1,NE    
                  RD(I,ITXG,K,IPOL) = 
     &            RD(I,ITXG,K,IPOL) + AD(I,ITXG,ICST,IPOL)
                  RT(I,ITXG,K,IPOL) = 
     &            RT(I,ITXG,K,IPOL) + AT(I,ITXG,ICST,IPOL)
               END DO  
            END DO  
         END DO  
      END DO  


c ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
      IF( NKTYP .GT. 1 )  THEN

         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               K           = KTYPCOR(ICST) 
               DE          = ECOREAV(K) - ECORE(ITXG,ICST)
               DO IPOL = 1,NPOL
                  DO I    = 1,NE0    
                     AD(I,ITXG,ICST,IPOL) = SD(I,ITXG,ICST,IPOL)
                  END DO  
               END DO  
            END DO  
         END DO

         DO K    = 1,NKTYP
            DO ITXG = 1,NTXRSGRP
               DO IPOL = 1,NPOL
                  DO IE   = 1,NE0    
                     SD(IE,ITXG,K,IPOL) = 0.0
                  END DO  
               END DO  
            END DO  
         END DO  
         
         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               K           = KTYPCOR(ICST) 
               DO IPOL = 1,NPOL
                  DO I    = 1,NE0    
                     SD(I,ITXG,K,IPOL) = 
     &               SD(I,ITXG,K,IPOL) + AD(I,ITXG,ICST,IPOL)
                  END DO  
               END DO  
            END DO  
         END DO  

         CALL FILLUP( NLT,SD,AD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &                NE,NTXRSGRP,NPOL,CONC,NQT )


      END IF
c ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss




cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                      tabulate KAPPA - combined spectra
      IF( ITCHECK .NE. 0 ) THEN
         DO ICST=1,NCST
            DO IPOL=1,NKTYP
               FILNAM = 'XE_kappa_'
               LL     = 9
               CALL ADDNTOSTR( FILNAM, ITCHECK, LL )
               CALL ADDNTOSTR( FILNAM, ICST   , LL )
               CALL ADDNTOSTR( FILNAM, IPOL   , LL )
               OPEN( IW, FILE=FILNAM(1:LL)//".chk", STATUS='UNKNOWN' )
               WRITE(IW,9100) 
     &         '#  KAPPA - combined spectrum IT ICST IPOL',
     &                     ITCHECK,ICST,IPOL
               DO IE=1,NE
                  WRITE(IW,'(3E15.7)') E(IE), RD(IE,ITCHECK,ICST,IPOL)
     &                                      , RT(IE,ITCHECK,ICST,IPOL) 
               END DO
            END DO
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      IF( (NKTYP .GT. 1) .AND. (.NOT.XMO) .AND. (.NOT.XRS))  THEN
         DO K=1,NKTYP
            SUMK(K) = 0.0
         END DO

         DO ITXG = 1,NTXRSGRP
            DO K    = 1,NKTYP
               DO I    = 2,NE    
                  SUMK(K) = SUMK(K) + 0.5*(E(I)-E(I-1))*
     &                        (RD(I,ITXG,K,NPOL) - RD(I,ITXG,K,1))
               END DO  
            END DO  
         END DO  
         WRITE(*,'(/,A,3F10.4,/)') '  L2/L3-RATIO   ',SUMK(1),
     &                       SUMK(2),SUMK(2)/SUMK(1)
      END IF
C ======================================================================
      
      NKTYPEFF = NKTYP
      IF( MERGE .EQ. 1 ) THEN 
         NKTYPEFF = 1
         DO ITXG = 1,NTXRSGRP
            DO IPOL = 1,NPOL
               DO I = 1,NE    
                  RD(I,ITXG,1,IPOL) = RD(I,ITXG,1,IPOL) 
     &                              + RD(I,ITXG,2,IPOL)
                  RT(I,ITXG,1,IPOL) = RT(I,ITXG,1,IPOL)
     &                              + RT(I,ITXG,2,IPOL)
               END DO  
            END DO  
         END DO  
      END IF




C ======================================================================
      NC = NCXRAY(1)
      LC = LCXRAY(1)

      DO K=1,NKTYPEFF
         DO ITXG = 0,NTXRSGRP
         IT = MAX(1,ITXG)
         HEADER = SHELL(NC)
         LHEADER= 1
         IDENT  = SHELL(NC)
         LIDENT = 1
         IF( NC .GT. 1 ) THEN
            HEADER = SHELL(NC)//'!s'//
     &               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'!N'
            LHEADER= 6
            IDENT  = SHELL(NC)//CHAR(ICHAR('0')+LC+ABS(KTYP(K)))
            LIDENT = 2
            IF( MERGE .EQ. 1 ) THEN 
               HEADER = HEADER(1:(LHEADER-2))//
     &                  ','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)//'!N'
               LHEADER= 8
               IDENT  = IDENT(1:LIDENT)//
     &                  CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
               LIDENT = 3
            END IF
         END IF

         LH = LHEADER + 1 + 3 + 12 + LTXTT(IT) + 4
         LL = MIN( (80-LH),LSYSTEM )

         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:LTXTT(IT))//' in '//SYSTEM(1:LL)
         LHEADER = LH + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            I1 = LL - LIDENT+1
            IF ( IDENT(1:LIDENT) .EQ. DATASET(I1:LL) ) THEN
               STR80 = DATASET(1:LL)
               LSTR80 = LL
            ELSE
               STR80 = DATASET(1:LL)//'_'//IDENT(1:LIDENT)
               LSTR80 = LL + 1 + LIDENT
            END IF
         ELSE 
            STR80  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//IDENT(1:LIDENT)
            LSTR80 = 4 + LTXTT(IT) + 1 + LIDENT
         END IF

         HEADT(ITXG,K) = HEADER
         LHEADT(ITXG,K) = LHEADER
         STR80T(ITXG,K) = STR80
         LSTR80T(ITXG,K) = LSTR80

      END DO
      END DO
C ======================================================================



      IF( XMO ) THEN 
         WRITE(*,99911) 
99911    FORMAT(/,5X,'BROADENING and MERGING completed',
     &               ' for X-ray optics  ',/,
     &            5X,'starting to deal with X-ray MOKE',/)
         CALL XRAYMOKE( NTXRSGRP, ECOREAV, 
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    SPEC,DATASET,LDATASET,
     &                    NLQ, NLT, NMIN,NMAX, 
     &                    NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADT,STR80T,LHEADT,LSTR80T,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,RYOVEV,EUNIT,EREFER,
     &                    EMIN,EMAX,DE,MERGE,IZ,VUC,
     &                    NEEXP,NCEXP,NPOLEXP,EXPDATAVL,
     &                    DX,KTYP,NKTYP,NKTYPEFF, 
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE, 
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,  
     &                    AD,AT,RD,RT, TD,TT,TE, KTABLE, SCLFAC,NPOL, 
     &                    NOTABEXT,KNOHEADER )
   
         STOP 'X-ray magneto optics done - FEIERABEND'
      END IF

      IF( XRS ) THEN 
         WRITE(*,99912) 
99912    FORMAT(/,5X,'BROADENING and MERGING completed',
     &               ' for X-ray resonant scattering ',/,
     &            5X,'starting to deal with XRS',/)
         CALL XRAYMOKE( NTXRSGRP, ECOREAV, 
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    SPEC,DATASET,LDATASET,
     &                    NLQ, NLT, NMIN,NMAX, 
     &                    NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADT,STR80T,LHEADT,LSTR80T,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,RYOVEV,EUNIT,EREFER,
     &                    EMIN,EMAX,DE,MERGE,IZ,VUC,
     &                    NEEXP,NCEXP,NPOLEXP,EXPDATAVL,
     &                    DX,KTYP,NKTYP,NKTYPEFF, 
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE, 
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,  
     &                    AD,AT,RD,RT, TD,TT,TE, KTABLE, SCLFAC,NPOL, 
     &                    NOTABEXT,KNOHEADER )
   
         STOP 'X-ray resonant scattering done - FEIERABEND'
      END IF

      IF( EXPDATAVL ) THEN
         IF( NCEXP .GT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  > NKTYPEFF=',NKTYPEFF
         END IF
         IF( NCEXP .LT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  < NKTYPEFF=',NKTYPEFF
         END IF
      END IF
      DO ITXG = 1,NTXRSGRP 
         NLT(ITXG) = NKTYPEFF  
      END DO  
      write(*,*)  ' NTXRSGRP ',NTXRSGRP
      write(*,*)  ' NKTYPEFF ',NKTYPEFF   
      write(*,*)  ' NPOL     ',NPOL   
      write(*,*)  ' NE       ',NE     , emin,emax   

      CALL FILLUP( NLT,RD,AD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )
      CALL FILLUP( NLT,RT,AT,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )
      
      CALL FINDEXTR( NLT,RD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &               NE,NTXRSGRP,0,NPOL, NMIN,NMAX )

C ----------------------------------------------------------------------
C                         scale the rate if its given in arbitrary units
C
      IF( KUNIT .EQ. 0 ) THEN 
C
         X = 100.0/NMAX(0)
         WRITE(*,'(A,E12.4)') '  scaling factor to have MAX=100: ', X
         IF( ABS(SCLFAC) .LT. EQUTOL ) THEN
            SCLFAC = X
         ELSE
            WRITE(*,'(A,E12.4)') '  scaling factor read in:   ',SCLFAC
         END IF
         
         CALL SCALEN( NLT,RD,AD,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &               NE,NTXRSGRP,NPOL, SCLFAC )
     
c undo second scaling for extrema  NMIN and NMAX
         DO ITXG=0,NTXRSGRP
            NMIN(ITXG) = NMIN(ITXG) / SCLFAC
            NMAX(ITXG) = NMAX(ITXG) / SCLFAC
         END DO  
         
         CALL SCALEN( NLT,RT,AT,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &               NE,NTXRSGRP,NPOL, SCLFAC )
C
      END IF
         
C ======================================================================
C                 PLOT SPECTRA AND COMPARE WITH EXPERIMENT 
C ======================================================================
      IF( ABS(EMIN-9999.) .LT. 0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF( ABS(EMAX-9999.) .LT. 0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------

      DO K=1,NKTYPEFF

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         HEADER = SHELL(NC)
         LHEADER= 1
         IDENT  = SHELL(NC)
         LIDENT = 1
         IF( NC .GT. 1 ) THEN
            HEADER = SHELL(NC)//'!s'//
     &               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'!N'
            LHEADER= 6
            IDENT  = SHELL(NC)//CHAR(ICHAR('0')+LC+ABS(KTYP(K)))
            LIDENT = 2
            IF( MERGE .EQ. 1 ) THEN 
               HEADER = HEADER(1:(LHEADER-2))//
     &                  ','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)//'!N'
               LHEADER= 8
               IDENT  = IDENT(1:LIDENT)//
     &                  CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
               LIDENT = 3
            END IF
         END IF

         LH = LHEADER + 1 + 3 + 12 + LTXTT(IT) + 4
         LL = MIN( (80-LH),LSYSTEM )

         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:LTXTT(IT))//' in '//SYSTEM(1:LL)
         LHEADER = LH + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            I1 = LL - LIDENT+1
            IF ( IDENT(1:LIDENT) .EQ. DATASET(I1:LL) ) THEN
               STR80 = DATASET(1:LL)
               LSTR80 = LL
            ELSE
               STR80 = DATASET(1:LL)//'_'//IDENT(1:LIDENT)
               LSTR80 = LL + 1 + LIDENT
            END IF
         ELSE 
            STR80  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//IDENT(1:LIDENT)
            LSTR80 = 4 + LTXTT(IT) + 1 + LIDENT
         END IF

         DO IE=1,NE
            TT(IE) =  0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL)
            TD(IE) = -0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL)
         END DO

         YMAX = -1E+10
         DMIN = +1E+10
         DMAX = -1E+10
         DO IE = 1,NE
            YMAX = MAX( YMAX, TT(IE) ) 
            DMIN = MIN( DMIN, TD(IE) )
            DMAX = MAX( DMAX, TD(IE) )
         END DO  

         IFIL = 10 

         YTXT1 = '!xm!F!m{1}!S'//TXTT(IT)(1:LTXTT(IT))//
     &           '!M{1}!N!s'//SHELL(NC)
         LYTXT1 = 12 + LTXTT(IT) + 10
         IF( SHELL(NC) .NE. 'K' ) THEN 
            YTXT1  = YTXT1(1:LYTXT1)//'!s'//SUBSH(LCXRAY(IT))
            LYTXT1 = LYTXT1 + 5
         ELSE
            YTXT1  = YTXT1(1:LYTXT1)//'   '
            LYTXT1 = LYTXT1 + 3
         END IF         

         IF( KUNIT .EQ. 0 ) THEN  
            YTXT1  = YTXT1(1:LYTXT1)//'!N  (a.u.)'
            LYTXT1 = LYTXT1 + 10
         ELSE IF( KUNIT .EQ. 1 ) THEN  
            YTXT1  = YTXT1(1:LYTXT1)//'!N  (Mb)  '
            LYTXT1 = LYTXT1 + 10
         ELSE  IF( KUNIT .EQ. 2 ) THEN  
            YTXT1  = YTXT1(1:2)//'s'// YTXT1(4:LYTXT1)
            YTXT1  = YTXT1(1:LYTXT1)//'!N  (10!S15!N s!S-1!N)'
            LYTXT1 = LYTXT1 + 22
         ELSE 
            STOP 'in <PLOTRXAS>  KUNIT not known'
         END IF

         CALL XMGR4HEAD( ' ',0,STR80,LSTR80,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN, 1,XMAX,1,
     &                  DMIN, 1,DMAX,1,
     &                  0.0,  0,YMAX,1,
     &                  'energy (eV)',11,
     &                  '!xD'//YTXT1,LYTXT1+3,YTXT1,LYTXT1,
     &                  HEADER,LHEADER,' ',0,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )
         
c ---------------------------------------- polarisation averaged spectra

         CALL XMGR4TABLE(1,0,E,TT,1.0,NE,IFIL)

         IF( EXPDATAVL ) THEN
            DO IE=1,NEEXP(K)
               ET(IE) = 0.5*IEXP(IE,K,1) + 0.5*IEXP(IE,K,NPOLEXP)
            END DO  
  
            CALL XMGR4TABLE(1,1,EEXP(1,K),ET,1.0,NEEXP(K),IFIL)

         END IF

c --------------------------------------------------- difference spectra

         CALL XMGR4TABLE(0,0,E,TD,1.0,NE,IFIL)

         IF( EXPDATAVL ) THEN

            DO IE=1,NEEXP(K)
               ED(IE) = +0.5*IEXP(1,K,1)-0.5*IEXP(1,K,NPOLEXP)
            END DO
  
            CALL XMGR4TABLE(0,1,EEXP(1,K),ED,1.0,NEEXP(K),IFIL)

         END IF

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' ',DICHROISM,'-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM),'  IT = ',IT,' ',TXTT(IT)
         WRITE(*,*) '  '

c ----------------------------------------------------------------------
c                      tabulate spectra if required
c ----------------------------------------------------------------------
         IF( KTABLE ) THEN

            IF( NKTYPEFF .GT. 1 ) THEN
               STR10 = 'xas_'//TXTT(IT)(1:2)//'_'//CHAR(ICHAR('1')+K-1)
            ELSE
               STR10 = 'xas_'//TXTT(IT)(1:2)
            END IF
            OPEN (90, FILE=STR10, STATUS='UNKNOWN' )
            IF( MERGE .EQ. 1 ) THEN
               DO IE=1,NE
                  DO KK=1,NKTYP
                     RAV(KK) = +0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                     RDF(KK) = -0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                  END DO
                  WRITE(90,'(20F15.8)') E(IE),
     &                RAV(1),RDF(1)
c    &                RAV(1),RDF(1), RAV(1)-RAV(2), RDF(1)-RDF(2),
c    &                RAV(2),RDF(2)
               END DO
            ELSE
               DO IE=1,NE 
                  WRITE(90,'(20F15.8)') E(IE),
     &                          +0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL),
     &                          -0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL)
               END DO
            END IF
            CLOSE (90)
            WRITE(*,*) '  '
            WRITE(*,*) ' ',DICHROISM,'-SPECTRUM tabulated in ', 
     &               STR10,'  IT = ',IT,' ',TXTT(IT)
            WRITE(*,*) '  '

         END IF
c ----------------------------------------------------------------------


      END DO  
C ======================================================================




C ======================================================================
C                             SUM RULE
C ======================================================================
      IF ( (NKTYP .GT. 1) .AND. (DICHROISM.EQ.'MCD') ) THEN

      IF( ABS(EMIN-9999.) .LT. 0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF( ABS(EMAX-9999.) .LT. 0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         HEADER = SHELL(NC)//'!s'//SUBSH(LC)//'!N' 
         LHEADER= 8
         IDENT  = SHELL(NC)//SUBSH(LC)
         LIDENT = 3
 
         LH = LHEADER + 1 + 3 + 14 + LTXTT(IT) + 4
         LL = MIN( (80-LH),LSYSTEM )

         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' sum rule for '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LL)
         LHEADER = LH + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            STR80 = DATASET(1:LL)//'_sumrule'
            LSTR80 = LL + 8
         ELSE 
            STR80  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//SHELL(NC)//SUBSH2(LC)//'_sumrule'
            LSTR80 = 4 + LTXTT(IT) + 1 + LIDENT + 8
         END IF

         IFIL = 10 

c ------------------------------------------------------- apply sum rule
         SMIN = 0.0
         SMAX = 0.0
         OMIN = 0.0
         OMAX = 0.0
         DO IE=1,NE0
c           YA1 =  0.5*SD(IE,0,1,1)+0.5*SD(IE,0,1,NPOL)
c           YA2 =  0.5*SD(IE,0,2,1)+0.5*SD(IE,0,2,NPOL)
            YD1 = -0.5*SD(IE,0,1,1)+0.5*SD(IE,0,1,NPOL)
            YD2 = -0.5*SD(IE,0,2,1)+0.5*SD(IE,0,2,NPOL)
            SSR(IE) = 3 * ( YD2 - 2*YD1 )
            OSR(IE) = 2 * ( YD2 +   YD1 ) 
            SMIN = MIN( SMIN, SSR(IE) )
            SMAX = MAX( SMAX, SSR(IE) )
            OMIN = MIN( OMIN, OSR(IE) )
            OMAX = MAX( OMAX, OSR(IE) )
         END DO

         TOP = MAX(ABS(SMIN),ABS(SMAX),ABS(OMIN),ABS(OMAX))

         DO IE=1,NE0
            SSR(IE) = SSR(IE) * 100 / TOP
            OSR(IE) = OSR(IE) * 100 / TOP
         END DO

         SMIN = SMIN * 100 / TOP
         SMAX = SMAX * 100 / TOP
         OMIN = OMIN * 100 / TOP
         OMAX = OMAX * 100 / TOP

         IFIL = 10
 
         YTXT1 = 'd<!xs!F!sz!N>!s'//TXTT(IT)(1:LTXTT(IT))//
     &           '!N/dE  (a.u.)'
         LYTXT1 = 15 + LTXTT(IT) + 13
         YTXT0 = 'd<l!sz!N>!s'//TXTT(IT)(1:LTXTT(IT))//
     &           '!N/dE  (a.u.)'
         LYTXT0 = 11 + LTXTT(IT) + 13

         CALL XMGR4HEAD( ' ',0,STR80,LSTR80,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2,
     &                  XMIN, 1,XMAX,1,
     &                  OMIN, 1,OMAX,1,
     &                  SMIN, 1,SMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT0,YTXT1,LYTXT1,
     &                  HEADER,LHEADER,' ',0,KNOHEADER)
 
         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

c ----------------------------------------------------------------- spin

         CALL XMGR4TABLE(1,0,E0,SSR,1.0,NE0,IFIL)
 
c -------------------------------------------------------------- orbital

         CALL XMGR4TABLE(0,0,E0,OSR,1.0,NE0,IFIL)

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' ',DICHROISM,'-sum rule written to ', 
     &               FILNAM(1:LFILNAM),'  IT = ',IT,' ',TXTT(IT)
         WRITE(*,*) '  '

      END IF
C ======================================================================


      IF( NTXRSGRP .EQ. 1 ) RETURN

      W0  = 0.0
      DO ITXG=1,NTXRSGRP
         W0 = W0 + NQT(ITXG)*CONC(ITXG)
      END DO
      W0 = MAX( 1.0, W0 )
C ======================================================================
C              COMPARE SPECTRA OF THE VARIOUS ATOM TYPES IT 
C ======================================================================

      DO K=1,NKTYPEFF

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         IF( NC .EQ. 1 ) THEN
            EXT='   '
            LHEADER= 1
         ELSE
            EXT=               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'  '
            LHEADER= 2
         END IF
         IF( MERGE .EQ. 1 ) THEN 
            EXT=EXT(1:1)//','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
            LHEADER= 4
         END IF
         HEADER = SHELL(NC)//EXT
         LHEADER0 = LHEADER
         IF( (80 - (LHEADER + 1 + 3 + 12 + 2 + 4) ) .LT. LSYSTEM ) THEN
             LL = 80 - (LHEADER + 1 + 3 + 12 + 2 + 4) 
         ELSE 
             LL = LSYSTEM
         END IF
         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LL)
         LHEADER = LHEADER + 1 + 3 + 12 + 2 + 4 + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            I1 = LL - LHEADER0+1
            IF ( HEADER(1:LHEADER0) .EQ. DATASET(I1:LL) ) THEN
               FILNAM = DATASET(1:LL)//'_split.xmgr'
               LFILNAM = LL + 11
            ELSE
               FILNAM = DATASET(1:LL)//'_'//HEADER(1:LHEADER0)//
     &                  '_split.xmgr'
               LFILNAM = LL + 1 + LHEADER0 + 11
            END IF
         ELSE 
            FILNAM  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//HEADER(1:LHEADER0)//'_split.xmgr'
            LFILNAM = 4 + LTXTT(IT) + 1 + LHEADER0 + 11
         END IF

         IFIL = 10 
         OPEN (IFIL, FILE=FILNAM(1:LFILNAM), STATUS='UNKNOWN' )

         STR80 = '\\8m\\0\\S'//TXTT(IT)(1:2)//'\\b\\b\\N\\s'//SHELL(NC)
         L80 = LNGSTRING( STR80, 80 )
         IF( SHELL(NC) .NE. 'K' ) THEN 
            STR80 = STR80(1:L80)//'\\s'//SUBSH(LCXRAY(IT))
            L80   = L80 + 5
         END IF         
         STR80 = STR80(1:L80)//'\\N (a.u.)'
         L80 = L80 + 9

c ---------------------------------------- polarisation averaged spectra
         YMIN = 0.0
         YMAX = NMAX(0)

         DY = 20.0
c        YMIN = FLOAT( INT(YMIN/DY) - 1 ) * DY
         YMAX = FLOAT( INT(YMAX/DY) + 1 ) * DY

         WRITE(IFIL,80000) 
         WRITE(IFIL,80100) 0,0
         WRITE(IFIL,80200) 0.5, 0.85
         WRITE(IFIL,80300) XMIN,XMAX, YMIN,YMAX, 
     &        'energy (eV)', DX,DX/2, 
     &         STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'
         WRITE(IFIL,80400) 'xaxis  ticklabel off'

         IF( .NOT.KNOHEADER ) THEN
            WRITE(IFIL,80400) 'title "'//
     &      HEADER(1:LHEADER)//'" '
            WRITE(IFIL,80400) 'title font 0'
c           WRITE(IFIL,80400) 'subtitle "'//
c    &      'XAS-spectrum of '//TXTT(IT)(1:LTXTT(IT))//
c    &       ' in '//SYSTEM(1:LSYSTEM)//'" '
c           WRITE(IFIL,80400) 'subtitle font 0'
         END IF

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 0,0
            WRITE(IFIL,80800) (E(IE), 
     &        (0.5*RD(IE,0,K,1)+0.5*RD(IE,ITXG,K,NPOL) ), IE=1,NE)
            WRITE(IFIL,80900)
         END DO

c --------------------------------------------------- difference spectra
         DMIN= +1E+10
         DMAX= -1E+10
         DO IE = 1,NE
            DEL =          RD(IE,0,K,NPOL)-RD(IE,0,K,1)
            DMIN= MIN( DMIN, DEL )
            DMAX= MAX( DMAX, DEL )
         END DO  
         DMIN= 1.03 * DMIN / 2.0                             
         DMAX= 1.03 * DMAX / 2.0 

         IF( LCXRAY(1) .EQ. 0 ) THEN
            DY = 0.5
         ELSE
            DY = 4.0
         END IF
         DMIN = FLOAT( INT(DMIN/DY) - 1 ) * DY
         DMAX = FLOAT( INT(DMAX/DY) + 1 ) * DY - 0.001

         WRITE(IFIL,80100) 1,1
         WRITE(IFIL,80200) 0.15, 0.5
         WRITE(IFIL,80300) XMIN,XMAX, DMIN,DMAX,
     &        'energy (eV)',     DX,DX/2,
     &        '\\8D'//STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 1,1
            WRITE(IFIL,80800) (E(IE),
     &        (-0.5*RD(IE,0,K,1)+0.5*RD(IE,ITXG,K,NPOL) ), IE=1,NE)
            WRITE(IFIL,80900)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' ',DICHROISM,'-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM),'  IT = ',IT,' ',TXTT(IT)
         WRITE(*,*) '  '

      END DO  
C ======================================================================




C ======================================================================
C                             SUM RULE
C ======================================================================
      IF ( (NKTYP .GT. 1) .AND. (DICHROISM.EQ.'MCD') ) THEN

      IF( ABS(EMIN-9999.) .LT. 0.01 ) THEN
         XMIN = FLOAT( INT(E(1)/DX) - 1 ) * DX
      ELSE
         XMIN = EMIN
      END IF
      IF( ABS(EMAX-9999.) .LT. 0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         HEADER = SHELL(NC)//SUBSH(LC)
         LHEADER= 4
         LHEADER0 = LHEADER
         IF( (80 - (LHEADER + 1 + 3 + 12 + 2 + 4) ) .LT. LSYSTEM ) THEN
             LL = 80 - (LHEADER + 1 + 3 + 12 + 2 + 4) 
         ELSE 
             LL = LSYSTEM
         END IF
         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' sum rule for '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LL)
         LHEADER = LHEADER + 1 + 3 + 12 + 2 + 4 + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            FILNAM = DATASET(1:LL)//'_sumrule_split.xmgr'
            LFILNAM = LL + 19
         ELSE 
            FILNAM  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//SHELL(NC)//SUBSH2(LC)
     &                //'_sumrule_split.xmgr'
            LFILNAM = 4 + LTXTT(IT) + 1 + LHEADER0 + 19
         END IF

         IFIL = 10 
         OPEN (IFIL, FILE=FILNAM(1:LFILNAM), STATUS='UNKNOWN' )

         STR80 = '\\8m\\0\\S'//TXTT(IT)(1:2)//'\\b\\b\\N\\s'//SHELL(NC)
         L80 = LNGSTRING( STR80, 80 )
         IF( SHELL(NC) .NE. 'K' ) THEN 
            STR80 = STR80(1:L80)//'\\s'//SUBSH(LCXRAY(IT))
            L80   = L80 + 5
         END IF         
         STR80 = STR80(1:L80)//'\\N (a.u.)'
         L80 = L80 + 9

c ----------------------------------------------------------------- spin
         DY = 0.02
c        SMIN = FLOAT( INT(SMIN/DY) - 1 ) * DY
         SMAX = FLOAT( INT(SMAX/DY) + 1 ) * DY

         WRITE(IFIL,80000) 
         WRITE(IFIL,80100) 0,0
         WRITE(IFIL,80200) 0.5, 0.85
         WRITE(IFIL,80300) XMIN,XMAX, SMIN,SMAX, 
     &        'energy (eV)', DX,DX/2, 
     &         STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'
         WRITE(IFIL,80400) 'xaxis  ticklabel off'

         IF( .NOT.KNOHEADER ) THEN
            WRITE(IFIL,80400) 'title "'//
     &      HEADER(1:LHEADER)//'" '
            WRITE(IFIL,80400) 'title font 0'
c           WRITE(IFIL,80400) 'subtitle "'//
c    &      'XAS-spectrum of '//TXTT(IT)(1:LTXTT(IT))//
c    &       ' in '//SYSTEM(1:LSYSTEM)//'" '
c           WRITE(IFIL,80400) 'subtitle font 0'
         END IF

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 0,0             
            DO IE=1,NE0
               YD1 = -0.5*SD(IE,ITXG,1,1)+0.5*SD(IE,ITXG,1,NPOL)
               YD2 = -0.5*SD(IE,ITXG,2,1)+0.5*SD(IE,ITXG,2,NPOL)
               SSR(IE) = 3 * ( YD2 - 2*YD1 )
               OSR(IE) = 2 * ( YD2 +   YD1 ) 
               WRITE(IFIL,80800) E0(IE),SSR(IE)
            END DO
            WRITE(IFIL,80900)
         END DO

c -------------------------------------------------------------- orbital
         DMIN = FLOAT( INT(DMIN/DY) - 1 ) * DY
         DMAX = FLOAT( INT(DMAX/DY) + 1 ) * DY - 0.001

         WRITE(IFIL,80100) 1,1
         WRITE(IFIL,80200) 0.15, 0.5
         WRITE(IFIL,80300) XMIN,XMAX, OMIN,OMAX,
     &        'energy (eV)',     DX,DX/2,
     &        '\\8D'//STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 1,1
            DO IE=1,NE0
               YD1 = -0.5*SD(IE,ITXG,1,1)+0.5*SD(IE,ITXG,1,NPOL)
               YD2 = -0.5*SD(IE,ITXG,2,1)+0.5*SD(IE,ITXG,2,NPOL)
               SSR(IE) = 3 * ( YD2 - 2*YD1 )
               OSR(IE) = 2 * ( YD2 +   YD1 ) 
               WRITE(IFIL,80800) E0(IE),OSR(IE)
            END DO
            WRITE(IFIL,80900)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' ',DICHROISM,'-sum rule written to ', 
     &               FILNAM(1:LFILNAM),'  IT = ',IT,' ',TXTT(IT)
         WRITE(*,*) '  '


      END IF
C ======================================================================

 9100 FORMAT(A,5I4)
80000 FORMAT('# ACE/gr parameter file',/,'#',/,'#',/,'#',/,
     &'@default font 0',/,'@default font source 0')
80100 FORMAT('@with g',I1,/,'@g',I1,' on')
80200 FORMAT(
     &'@    view xmin 0.150',/,
     &'@    view xmax 0.850',/,
     &'@    view ymin ',F7.3,/,
     &'@    view ymax ',F7.3,/,
     &'@    xaxis  label font 0',/,
     &'@    xaxis  ticklabel font 0',/,
     &'@    yaxis  label font 0',/,
     &'@    yaxis  ticklabel font 0' )

80300 FORMAT(
     &'@    world xmin ',F9.5,/,'@    world xmax ',F9.5,/,
     &'@    world ymin ',F9.5,/,'@    world ymax ',F9.5,/,
     &'@    zeroxaxis  bar on',/,
     &'@    xaxis  label "',A,'"',/,
     &'@    xaxis  tick on',/,
     &'@    xaxis  tick major ',F7.3,/,
     &'@    xaxis  tick minor ',F7.3,/,
     &'@    yaxis  label "',A,'"',/,
     &'@    yaxis  tick on',/,
     &'@    yaxis  tick major ',F7.3,/,
     &'@    yaxis  tick minor ',F7.3)
80400 FORMAT('@    ',A) 
80500 FORMAT('@    s',I1,' color ',I2)
80600 FORMAT('@    s',I2,' color ',I2)
80700 FORMAT('@WITH G',I1,/,'@G',I1,' ON',/,'@TYPE xy')
80800 FORMAT(2E15.7)
80900 FORMAT('&')
81000 FORMAT('@    s',I1,' linestyle ',I1)
81100 FORMAT('@    s',I2,' linestyle ',I1)
    
      RETURN
      END   
