      SUBROUTINE CREATEGNUPLOTBSF(ISCRIPT,FILENAME,NKDIR,KP,NK2,LBLKDIR
     &     ,INDKDIR,EMIN,EMAX,TITLE,ZMIN,ZMAX)
        IMPLICIT NONE

        CHARACTER*80 FILENAME,TITLE
        CHARACTER*8 LBLKDIR(NKDIR)
        INTEGER      NKDIR,NK2,INDKDIR(NKDIR),I,ISCRIPT,J
        REAL    emin,emax,kstart,kend,zmin,zmax,KP(NK2),kk,zmaxp1,km,kl
        CHARACTER*80 KLBL(:,:)
        INTEGER LNGKLBL(:,:)
        ALLOCATABLE KLBL,LNGKLBL
        ALLOCATE (KLBL(NKDIR,3),LNGKLBL(NKDIR,3))
        
        kstart = KP(1)
        kend = KP(NK2)

C        zmin = 1.0D-6
        IF (ZMIN .LT. 1.0D-3) ZMIN=1.D-3
        zmaxp1 = zmax+1.0D0

C ######################################################################
        write(ISCRIPT,99001)"setting output"
        write(ISCRIPT,99005)'set term postscript enhanced color'
        write(ISCRIPT,99006) FILENAME(1:LEN_TRIM(FILENAME)-4)
        write(ISCRIPT,99004)
C        #
C        
        
C ######################################################################
        write(ISCRIPT,99001)"setting title"
        write(ISCRIPT,99007) TITLE(1:LEN_TRIM(TITLE))
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting variables"
        write(ISCRIPT,99003)"emin",emin
        write(ISCRIPT,99003)"emax",emax
        write(ISCRIPT,99003)"kstart",kstart
        write(ISCRIPT,99003)"kend",kend
        write(ISCRIPT,99003)"zmin",zmin
        write(ISCRIPT,99003)"zmax",zmax
C ######################################################################
        write(ISCRIPT,99001)"setting BZ path"
        IF (NKDIR .GT. 1) THEN
           do I=1,NKDIR-1
           kk = kp(INDKDIR(I))
           write(ISCRIPT,99008)kk,emin,zmaxp1,kk,emax,zmaxp1
           end do
        END IF
        IF(LBLKDIR(1)(1:4).NE. 'NONE') THEN
            CALL PREPKDIRLAB(NKDIR,INDKDIR,LBLKDIR,KLBL,LNGKLBL,'\\')
            write(ISCRIPT,99009)
            do I=1,NKDIR
                IF (I.EQ.1) THEN
                    kk=0.0D0
                ELSE
                    kk=kp(INDKDIR(I-1))
                END IF
                kl = kp(INDKDIR(I))
                km=kk+(kl-kk)/2.0D0

              if (KLBL(I,1)(1:LEN_TRIM(KLBL(I,1))).NE.'SAME') THEN
                write(ISCRIPT,99011) KLBL(I,1)(1:LEN_TRIM(KLBL(I,1))),kk
                write(ISCRIPT,99010)
             end if
              write(ISCRIPT,99011) KLBL(I,2)(1:LEN_TRIM(KLBL(I,2))),km
              write(ISCRIPT,99010)
              write(ISCRIPT,99011) KLBL(I,3)(1:LEN_TRIM(KLBL(I,3))),kl
              IF ( I .NE. NKDIR) write(ISCRIPT,99010)
            end do
            write(ISCRIPT,99012)
        END IF
C        set xtics ("{/Symbol G}" kg,"K" kk,"M" km,"L" kl)
C        
C ######################################################################
        IF (EMIN .LT. 0.0D0 .AND. EMAX .GT. 0.0D0) THEN
        write(ISCRIPT,99001)"drawing line at EF"

        write(ISCRIPT,99008) 0.0,0.0,zmaxp1,kp(INDKDIR(NKDIR)),0.0
     &       ,zmaxp1
        END IF
        
C ######################################################################
        write(ISCRIPT,99001)"setting style"
        write(ISCRIPT,99005)'set lmargin 0'
        write(ISCRIPT,99005)'unset key'
        write(ISCRIPT,99005)'unset ztics'
        write(ISCRIPT,99005)'set view map'
        write(ISCRIPT,99005)'set format y "%1.2f"'
        write(ISCRIPT,99005)'set pm3d at b'
C        write(ISCRIPT,99005)'set multiplot layout 1,1'
        write(ISCRIPT,99005)'set colorbox'
        write(ISCRIPT,99005)'set pointsize 0.6'
        write(ISCRIPT,99005)'set pm3d explicit'
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting labels"
        write(ISCRIPT,99005)'set ylabel "E (eV)"'
C        write(ISCRIPT,99005)'set xlabel "k"'
        write(ISCRIPT,99005)'set cblabel "arb. units"'
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting colours"
        write(ISCRIPT,99005)'#set palette rgbformulae 21,22,23'
        write(ISCRIPT,99005)'#set palette rgbformulae 34,35,36'
        write(ISCRIPT,99005)'#set palette rgbformulae 7,5,15'
        write(ISCRIPT,99005)'set palette defined (0 "white", 5 "grey", 6 
     &  "blue", 7 "red", 9 "green", 10 "black")'
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting ranges"
        write(ISCRIPT,99005)"set xrange [kstart:kend]"
        write(ISCRIPT,99005)"set yrange [emin:emax]"
        write(ISCRIPT,99005)"set cbrange [zmin:zmax]"
        write(ISCRIPT,99005)"set log zcb"
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99002) FILENAME(1:LEN_TRIM(FILENAME))

C ######################################################################
        write(ISCRIPT,99005)'set term x11 enhanced'
        write(ISCRIPT,99005)'replot'
        write(ISCRIPT,99005)'pause -1'
        write(ISCRIPT,99004)
C ###################################################################### 

99001 FORMAT (80('#'),/,'# ',A,/,80('#'))
99002 FORMAT ("splot '",A,"' u ($1):($2):($3) with pm3d at b")
99003 FORMAT (A,'=',99E15.8)
99004 FORMAT (80('#'),/)
99005 FORMAT (A)
99006 FORMAT ('set output "',A,'.ps"')
99007 FORMAT ('set title "',A,'"')
99008 FORMAT ('set arrow from',E15.8,',',E15.8,',',E15.8,1X,'to'
     &                     ,E15.8,',',E15.8,',',E15.8,1X,'nohead front')

99009 FORMAT ('set xtics ( \')
99010 FORMAT (', \')
99011 FORMAT ('"{',A,'}"',F7.4,'\')
99012 FORMAT (')')
         END
C*==prepkdirlab.f    processed by SPAG 6.05Rc at 14:25 on 18 Mar 2004
      SUBROUTINE PREPKDIRLAB(NKDIR,INDKDIR,LBLKDIR,KLBL,LNGKLBL,BSLASH)
C
C                 prepare kdir labels
C
C *  when calling PREPKDIRLAB BSLASH should be set to '\\'.      
C *  for any compiler / machine this should be converted to      
C *  have the escape character  '\'  in the labels at the end    
C * (e.g. required for ifort vs. g77)
C
C  call to ENSUREBSLASH(BSLASH,'\\') could be used instead of BSLASH arg 
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER NKDIR
      INTEGER INDKDIR(NKDIR),LNGKLBL(NKDIR,3)
      CHARACTER*1 BSLASH
      CHARACTER*80 KLBL(NKDIR,3)
      CHARACTER*8 LBLKDIR(NKDIR)
C
C Local variables
C
      CHARACTER C1
      INTEGER I,ID,LB
C
C*** End of declarations rewritten by SPAG
C
      DO ID = 1,NKDIR
         DO LB = 1,3
            I = LB*3 - 2
            C1 = LBLKDIR(ID)(I:I)
            IF ( (C1.EQ.'G' .OR. C1.EQ.'g') ) THEN
C               KLBL(ID,LB) = '!x'//LBLKDIR(ID)((I+1):(I+1))//'!f{}'
C               LNGKLBL(ID,LB) = 2 + 1 + 4
C
               C1 = LBLKDIR(ID)((I+1):(I+1))
               IF ( C1.EQ.'G' .OR. C1.EQ.'g' ) THEN
                  KLBL(ID,LB) = '/'//'Symbol '//'G'
               ELSE IF ( C1.EQ.'L' .OR. C1.EQ.'l' ) THEN
                  KLBL(ID,LB) = '/'//'Symbol '//'L'
               ELSE IF ( C1.EQ.'S' .OR. C1.EQ.'s' ) THEN
                  KLBL(ID,LB) = '/'//'Symbol '//'S'
               ELSE IF ( C1.EQ.'D' .OR. C1.EQ.'d' ) THEN
                  KLBL(ID,LB) = '/'//'Symbol '//'D'
               ELSE
C     what should i do here
                  KLBL(ID,LB) = '???'
                  LNGKLBL(ID,LB) = 7
               END IF
C
            ELSE
               KLBL(ID,LB) = C1
               LNGKLBL(ID,LB) = 1
            END IF
            LNGKLBL(ID,LB) = LEN_TRIM(KLBL(ID,LB))
         END DO
         IF ( ID.GT.1 ) THEN
            IF ( KLBL(ID,1).EQ.KLBL(ID-1,3) ) THEN
               KLBL(ID,1) = 'SAME'
               LNGKLBL(ID,1) = 4
            ELSE
C
C               KLBL(ID-1,3) = KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))//'   '
C               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 3
C               KLBL(ID,1) = '   '//KLBL(ID,1)(1:LNGKLBL(ID,1))
C               LNGKLBL(ID,1) = LNGKLBL(ID,1) + 3
C
C------------- combine 2 k-labels on seg ID-1 and delete that for seg ID
C
               KLBL(ID-1,3) = KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))
     &                        //' '//KLBL(ID,1)(1:LNGKLBL(ID,1))
               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 1 + LNGKLBL(ID,1)
               KLBL(ID,1) = ' '
               LNGKLBL(ID,1) = 1
            END IF
         END IF
      END DO
C
      END

