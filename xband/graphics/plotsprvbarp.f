C*==plotsprvbarp.f    processed by SPAG 6.05Rc at 09:53 on 25 Jun 2004
      SUBROUTINE PLOTSPRVBARP(IFIL,DATASET,LDATASET,NQMAX,NTMAX,NEMAX,
     &     NCMAX,NPOLMAX,NLMAX,SPEC,NLQ,NLT,NMIN,
     &     NMAX,WMIN,WMAX,IQOFT,NQT,CONC,TXTT,LTXTT,
     &     INFO,LINFO,HEADER,LHEADER,SYSTEM,LSYSTEM,
     &     E,Y,NQ,NT,NS,NE,IREL,EF,
     &     MTV,
     &     KTABLE,NVMAX,NEXRSMAX,WGTGAUTAB,
     &     WLORTAB,WLORVBXPS,NCEXPMAX,
     &     NPOLEXPMAX,ICOLOR)
C **********************************************************************
C *                                                                    *
C *  plot SPR-VB-XPS - spectra                                         *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX,NKMAX
      PARAMETER (NLEGMAX=80,NKMAX=1000)
      INTEGER NKDIRMAX
      PARAMETER (NKDIRMAX=1)

C
C COMMON variables
C
      REAL BLSEXP(2),EMAX,EMIN,EREFER,ESEXP(2),RYOVEV,SCLEXP(2),WGAUSS,
     &     WLOR(2)
      CHARACTER*2 EUNIT
      LOGICAL EXPDATAVL
      CHARACTER*80 FMTEXP
      CHARACTER*80 FILENAME,IDENT1,IDENT2,SUBTITLE,TITLE
      INTEGER MERGE,NCEXP,NEEXP(2),NPOLEXP
      COMMON /BRDPAR/ WGAUSS,WLOR
      COMMON /ENERGY/ RYOVEV,EREFER,EUNIT
      COMMON /EXPPAR/ ESEXP,BLSEXP,SCLEXP,NEEXP,NCEXP,NPOLEXP,FMTEXP,
     &                EXPDATAVL
      COMMON /PLOPAR/ EMIN,EMAX,MERGE
C
C Dummy arguments
C
      CHARACTER*80 DATASET,HEADER,INFO,SYSTEM
      REAL EF
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSYSTEM,NCEXPMAX,NCMAX,
     &        NE,NEMAX,NEXRSMAX,NLMAX,NPOLEXPMAX,NPOLMAX,NQ,NQMAX,NS,NT,
     &        NTMAX,NVMAX
      LOGICAL KTABLE
      CHARACTER*3 SPEC
      REAL CONC(NTMAX),E(NEMAX),
     &     NMAX(0:NTMAX),NMIN(0:NTMAX),WGTGAUTAB(0:NEMAX),
     &     WLORTAB(1:NEMAX),WLORVBXPS(2),WMAX(0:NTMAX),WMIN(0:NTMAX),
     &     Y(NEMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      CHARACTER*1 CHPOL(5)
      CHARACTER*80 EPHOTCHAR,NTEXT
      REAL EPHOTEV,KP(NKMAX)

          REAL POL(:,:,:,:),TOT(:,:,:,:)





      INTEGER I,IE,IEP,IK,IPOL,IT,LFN,LINEPHOT,LNTEXT,LOUTEPHOT,N0,NK2,
     &        NPOL
      INTEGER LNGSTRING,LNGZEROSTRING
      CHARACTER*3 MEFORM

      REAL IXRS(:,:),BXRS(:,:)
      REAL*8 ZMAT(:)





       LOGICAL MTV
       CHARACTER*30 XLABEL,YLABEL
      CHARACTER*4 CDSP
      CHARACTER*8 LBLKDIR(NKDIRMAX)
      INTEGER INDKDIR(NKDIRMAX)
      CHARACTER*1 BSLASH


      ALLOCATABLE TOT,POL,IXRS,BXRS,ZMAT

C
C
C     set BSLASH to single backslash character
      CALL ENSUREBSLASH(BSLASH,'\\')


      READ (IFIL,99008) FILENAME
      LFN = LNGSTRING(FILENAME,80)
      READ (IFIL,99008) NTEXT
      NTEXT = 'I(XPS) (arb. units)'
      LNTEXT = 19
      READ (IFIL,99008) MEFORM
      READ (IFIL,*)
      READ (IFIL,99006) EPHOTEV
      READ (IFIL,99005) NPOL,(CHPOL(I),I=1,NPOL)
      READ (IFIL,99004) NE
C
      READ (IFIL,*)
      READ (IFIL,99004) NK2
      IF ( NK2.GT.NKMAX ) STOP 'in <PLOTSPRVBARP>:   NK2 > NKMAX'
      DO IK = 1,NK2
         READ (IFIL,99009) KP(IK)
      END DO
      READ (IFIL,*)
C
C
      WRITE (EPHOTCHAR,'(f12.2)') EPHOTEV
      LINEPHOT = LNGZEROSTRING(EPHOTCHAR,80)
      LOUTEPHOT = LNGSTRING(EPHOTCHAR,80)
C
      IF ( LDATASET.EQ.0 ) THEN
         LDATASET = LFN
         DATASET = FILENAME
      END IF
      IF ( DATASET((LDATASET-3):LDATASET).EQ.'.xps' )
     &     LDATASET = LDATASET - 4
C
      WRITE (6,99013) '  '
      WRITE (6,99013) 'DATASET   ',DATASET(1:LDATASET)
      WRITE (6,99013) 'FILENAME  ',FILENAME(1:LFN)
      WRITE (6,99013) 'MEFORM    ',MEFORM
      WRITE (6,99012) 'E_ph (eV) ',EPHOTEV
      WRITE (6,99011) 'NPOL      ',NPOL,(CHPOL(I),I=1,NPOL)
      WRITE (6,99010) 'NE        ',NE
      WRITE (6,99010) 'NK        ',NK2
      

      ALLOCATE (TOT(NEMAX,0:NT,0:NK2,0:NPOLMAX))
      ALLOCATE (POL(NEMAX,0:NT,0:NK2,0:NPOLMAX))

C
C ------------------------------------------------------------------
C    SPIN DENSITY MATRIX VERSION
C
      DO IE = 1,NE
          READ (IFIL,'(I5,10X,2F10.5)') IEP,E(IE)
          DO IK = 1,NK2
              READ (IFIL,99014) (TOT(IE,0,IK,IPOL),TOT(IE,0,IK,IPOL),
     &             IPOL=1,NPOL),
     &             (POL(IE,0,IK,IPOL),POL(IE,0,IK,IPOL),
     &             IPOL=1,NPOL)
              DO IT = 1,NT
                  READ (IFIL,99014) (TOT(IE,IT,IK,IPOL),TOT(IE,IT,IK
     &                 ,IPOL),IPOL=1,NPOL),(POL(IE,IT,IK,IPOL),POL(IE,IT
     &                 ,IK,IPOL),IPOL=1,NPOL)
              END DO
C     
          END DO
C     
      END DO
C     ------------------------------------------------------------------
C                  READING IN FINISHED
C     ------------------------------------------------------------------
C
C ------------------------------------------------ sum over polarization
      DO IT = 0,NT
          DO IK = 0,NK2
            DO IP = 1,NPOL
               DO IE = 1,NE
                  TOT(IE,IT,IK,0) = TOT(IE,IT,IK,0) + TOT(IE,IT,IK,IP)
     &                             /REAL(NPOL)
                  POL(IE,IT,IK,0) = POL(IE,IT,IK,0) + POL(IE,IT,IK,IP)
     &                             /REAL(NPOL)
               END DO
            END DO
         END DO
      END DO
C
      EUNIT = 'eV'

      IF ( EUNIT.EQ.'eV' ) THEN
         DO IE = 1,NE
            E(IE) = E(IE)*RYOVEV
         END DO
      END IF
      DO IE = 1,NE
         E(IE) = E(IE) - EREFER
      END DO
C ----------------------------------------------------------------------
C

C     ------------------------------------------------------------------
C     extend E - mesh
C     ------------------------------------------------------------------
      WRITE (6,99015)
      WRITE (6,99012) 'E(min/max)',E(1),E(NE)
      WRITE (6,99010) 'NE  (old) ',NE
C
      DE = E(2) - E(1)
      NEADD = NINT(5.0/DE)
      IF( ((WLORVBXPS(1)+WLORVBXPS(2)) .LT. 0.001) .AND.
     &     (WGAUSS .LT. 0.001) ) NEADD=0
      IF (NEADD.GT.0) THEN
      DO IE = NE,1, - 1
         DO IT = 0,NT
            DO IK = 1,NK2
               DO IP = 0,NPOLMAX
                  TOT(IE+NEADD,IT,IK,IP) = TOT(IE,IT,IK,IP)
                  POL(IE+NEADD,IT,IK,IP) = POL(IE,IT,IK,IP)
                  E(IE+NEADD) = E(IE)
               END DO
            END DO
         END DO
      END DO
C
      DO IE = 1,NEADD
         DO IT = 0,NT
            DO IK = 1,NK2
               DO IP = 0,NPOLMAX
                  TOT(IE,IT,IK,IP) = 0.0
                  POL(IE,IT,IK,IP) = 0.0
                  E(IE) = E(NEADD+1) - (NEADD-IE+1)*DE
               END DO
            END DO
         END DO
      END DO
      NE = NE + NEADD
C
      DO IE = 1,NEADD
         E(IE+NE) = E(NE) + (IE-1)*DE + 0.001
      END DO
      NE = NE + NEADD
C
      WRITE (6,99012) 'DE   (eV) ',DE
      WRITE (6,99010) 'NEADD     ',NEADD
      WRITE (6,99012) 'E(min/max)',E(1),E(NE)
      WRITE (6,99010) 'NE  (new) ',NE
      WRITE (6,99015)
      END IF
C ----------------------------------------------------------------------
C                       BROADEN AND SCALE CURVES
C ----------------------------------------------------------------------
      WLOR(1) = WLORVBXPS(1)
      WLOR(2) = WLORVBXPS(2)
C
      IF ( (ABS(9999-WLOR(1)).LT.1E-5) ) WLOR(1) = 0.0
C
      NVAL = 0
      ILOR = 1
      DO I = 1,NE
          WLORTAB(I) = WLOR(2)*E(I)**2
      END DO

      IF( ((WLORVBXPS(1)+WLORVBXPS(2)) .GT. 0.001) .OR.
     &     (WGAUSS .GT. 0.001) ) THEN

          ALLOCATE(IXRS(1,NEMAX),BXRS(1,NEMAX))

          I = 0
          ICALL = 0
          DO IPOL=0,NPOL
              DO IK=1,NK2
                  DO IT=0,NT
                      DO IE=1,NE
                          IXRS(1,IE) = TOT(IE,IT,IK,IPOL)
                      END DO
                      IF( ((WLOR(1)+WLOR(2)) .GT. 0.001) .OR. (NVAL .NE.
     &                     0)) THEN
                          
                          IF(ICALL.EQ.0) CALL INILORBRD(NVAL,E,WLORTAB
     &                         ,NE)
                          CALL VECLORBRD(E, IXRS, BXRS, 1, 1, NE,NEMAX
     &                         ,WLOR(1), WLORTAB )
                      END IF
                      IF( WGAUSS .GT. 0.001 ) THEN
                          IF(ICALL.EQ.0) CALL INIGAUBRD(WGAUSS,WGTGAUTAB
     &                         ,NEMAX,DXG,IGMAX)
                          CALL VECGAUBRD( E,IXRS,BXRS,1,1,NE,NEMAX,
     &                         WGTGAUTAB, DXG, IGMAX )
                      END IF
                      ICALL = ICALL + 1
                      DO IE=1,NE
                          TOT(IE,IT,IK,IPOL)=IXRS(1,IE) 
                      END DO
                  END DO
              END DO
          END DO

          DEALLOCATE(IXRS,BXRS)

      END IF
      TITLE = 'AR-VB-XPS'
          LTITLE = 9

cC Print total 
          DO IPOL=0,NPOL
              IF ( IPOL.EQ.1 ) THEN
                  IDENT1 = CHPOL(IPOL)
                  LIDENT1 = 1
              ELSE IF ( IPOL.EQ.2 ) THEN
                  IDENT1 = CHPOL(IPOL)
                  LIDENT1 = 1
              ELSE IF ( IPOL.EQ.3 ) THEN
                  IDENT1 = CHPOL(IPOL)
                  LIDENT1 = 1
              ELSE IF ( IPOL.EQ.0 ) THEN
                  IDENT1 = 'tot'
                  LIDENT1 = 3
              END IF
              IDENT2 = ''
              LIDENT2 = 0

              IF(IPOL.EQ.0) THEN
                  SUBTITLE = 'for '//SYSTEM(1:LSYSTEM)//' '//'total'
                  LSUBTITLE = 8 + LSYSTEM + 15
              ELSE
                  SUBTITLE = 'for '//SYSTEM(1:LSYSTEM)//' '//CHPOL(IPOL)
     &                 //' polarisation'
                  LSUBTITLE = 8 + LSYSTEM + 15
              END IF
              LFNMAX=80
C
C ryd -> eV
            EMIN = E(1)
            EMAX = E(NE)
C
            XMIN = KP(1)
            XMAX = KP(NK2)
            YMIN = EMIN
            YMAX = EMAX
            KYMIN = 0
            KXMIN = 0
            KYMAX = 0
            KXMAX = 0
            KZMIN = 0
            KZMAX = 0
            ZMIN = 0 
            ZMAX = 0
            XLABEL = 'k (2 !gp!F/a)'
            LXLABEL = 13
C
            YLABEL = 'E (eV)'
            LYLABEL = 6



            CALL CREATEXMV(DATASET,LDATASET,IDENT1,LIDENT1,IDENT2,
     &           LIDENT2,FILENAME,LFNMAX,LFN,DBLE(XMIN),KXMIN,
     &           DBLE(XMAX),KXMAX,DBLE(YMIN),KYMIN,DBLE(YMAX),
     &           KYMAX,DBLE(ZMIN),KZMIN,DBLE(ZMAX),KZMAX,
     &           XLABEL(1:LXLABEL),LXLABEL,YLABEL(1:LYLABEL),
     &           LYLABEL,TITLE,LTITLE,SUBTITLE,LSUBTITLE,
     &           .TRUE. )
          END DO
cC
         WRITE (*,*) ' '
         WRITE (*,*) 
     &              ' Angular resolved VB-XPS'
         WRITE (*,*) ' '
         WRITE (*,*) ' '
         WRITE (*,*) ' '
cC

      STOP
C
C ----------------------------------------------------------------------
99001 FORMAT (1000(E12.5,2x))
99002 FORMAT ('#  ',A,A,A)
99003 FORMAT (10X,A80)
99004 FORMAT (10X,I10)
99005 FORMAT (10X,I10,5(:,3X,A,1X))
99006 FORMAT (10X,10F10.5)
99007 FORMAT (I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99008 FORMAT (10X,A)
99009 FORMAT (8F10.4)
99010 FORMAT (10X,A10,I10)
99011 FORMAT (10X,A10,I10,5(:,'  (',A,')'))
99012 FORMAT (10X,A10,10F10.5)
99013 FORMAT (10X,A10,A)
99014 FORMAT (11X,100E12.5)
99015 FORMAT (80('#'))
99016 FORMAT (/,'# column array2d (1,',i5,',1,',i5,') of real scalar',/)
      END
