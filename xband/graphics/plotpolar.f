C*==plotpolar.f    processed by SPAG 6.05Rc at 09:46 on 25 Sep 2021
      SUBROUTINE PLOTPOLAR(IFIL,ORBPOL,SPNPOL,DATASET,LDATASET,NTMAX,
     &                     NEMAX,NCMAX,NSPMAX,NLT,ORBPOLMAX,TXTT,LTXTT,
     &                     HEADER,LHEADER,SYSTEM,LSYSTEM,E,RYOVEV,
     &                     EREFER,EUNIT,EMIN,EMAX,NT,NE,EF,KNOHEADER)
C **********************************************************************
C *                                                                    *
C *  plot spin and orbital polarisation                                *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX
      PARAMETER (NLEGMAX=50)
C
C Dummy arguments
C
      CHARACTER*(*) DATASET
      REAL EMAX,EMIN,EREFER,RYOVEV
      CHARACTER*2 EUNIT
      CHARACTER*80 HEADER,SYSTEM
      INTEGER IFIL,LDATASET,LHEADER,LSYSTEM,NCMAX,NE,NEMAX,NSPMAX,NT,
     &        NTMAX
      LOGICAL KNOHEADER
      REAL E(NEMAX),ORBPOL(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),
     &     ORBPOLMAX(0:NTMAX),SPNPOL(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      INTEGER LTXTT(NTMAX),NLT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      CHARACTER CHAR
      CHARACTER*80 FILNAM,STR80,YTXT1,YTXT2
      INTEGER I0,IE,IL,IT,LFILNAM,LS,LYTXT1,LYTXT2,NCL,NG
      INTEGER ICHAR
      CHARACTER*20 LEG(NLEGMAX)
      REAL ORBPOLMIN(:),SPNPOLMAX(:),SPNPOLMIN(:),WGT,
     &     XMAX,XMIN,YMAX1,YMAX2,YMIN1,YMIN2

      ALLOCATABLE ORBPOLMIN,SPNPOLMIN,SPNPOLMAX
      ALLOCATE (ORBPOLMIN(0:NTMAX),SPNPOLMIN(0:NTMAX))
      ALLOCATE (SPNPOLMAX(0:NTMAX))
C
C*** End of declarations rewritten by SPAG
C
C ----------------------------------------------------------------------
C
      DO IE = 1,NE
         READ (IFIL,99001,END=100) E(IE),
     &                             ((SPNPOL(IE,IT,IL,1),IL=1,NLT(IT)),
     &                             (ORBPOL(IE,IT,IL,1),IL=1,NLT(IT)),
     &                             (DUMMY,IL=1,NLT(IT)),
     &                             IT=1,NT)
         NE = IE
      END DO
 100  CONTINUE
      WRITE (*,'(A,I10)') ' number of energies read:  ',NE
      WRITE (*,'(A,2F10.4)') ' boundaries             :  ',E(1),E(NE)
C
C ----------------------------------------------------------------------
C                      READING IN FINISHED
C ----------------------------------------------------------------------
C
      IF ( EUNIT.EQ.'eV' ) THEN
         DO IT = 1,NT
            DO IL = 1,NLT(IT)
               DO IE = 1,NE
                  ORBPOL(IE,IT,IL,1) = ORBPOL(IE,IT,IL,1)/RYOVEV
                  SPNPOL(IE,IT,IL,1) = SPNPOL(IE,IT,IL,1)/RYOVEV
               END DO
            END DO
         END DO
         DO IE = 1,NE
            E(IE) = E(IE)*RYOVEV
         END DO
      END IF
      DO IE = 1,NE
         E(IE) = E(IE) - EREFER
      END DO
C ----------------------------------------------------------------------
C
      DO IT = 1,NT
         ORBPOLMIN(IT) = 0.0
         SPNPOLMIN(IT) = 0.0
         ORBPOLMAX(IT) = 0.0
         SPNPOLMAX(IT) = 0.0
         DO IL = 1,NLT(IT)
            DO IE = 1,NE
               ORBPOLMIN(IT) = MIN(ORBPOLMIN(IT),ORBPOL(IE,IT,IL,1))
               SPNPOLMIN(IT) = MIN(SPNPOLMIN(IT),SPNPOL(IE,IT,IL,1))
               ORBPOLMAX(IT) = MAX(ORBPOLMAX(IT),ORBPOL(IE,IT,IL,1))
               SPNPOLMAX(IT) = MAX(SPNPOLMAX(IT),SPNPOL(IE,IT,IL,1))
            END DO
         END DO
      END DO
C
C ======================================================================
C                              XMGRACE - output
C ======================================================================
      IF ( ABS(EMIN-9999.).LT.0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF ( ABS(EMAX-9999.).LT.0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------
      IFIL = 90
      NG = 2
C ----------------------------------------------------------------------
      DO IT = 1,NT
C
         YMIN1 = ORBPOLMIN(IT)
         YMAX1 = ORBPOLMAX(IT)
         YMIN2 = SPNPOLMIN(IT)
         YMAX2 = SPNPOLMAX(IT)
C
         STR80 = '!m{1}!Sl!M{1}!N!s'//TXTT(IT)(1:LTXTT(IT))
     &           //'!N/dE  (!xm!0!sB!N/eV)'
         LS = 17 + LTXTT(IT) + 22
         YTXT1 = 'd<l!sz!N>'//STR80(1:LS)
         LYTXT1 = 9 + LS
         YTXT2 = 'd<!xs!0!sz!N>'//STR80(1:LS)
         LYTXT2 = 13 + LS
C
         CALL XMGR4HEAD(DATASET,LDATASET,'polar',5,TXTT(IT),LTXTT(IT),
     &                  FILNAM,80,LFILNAM,IFIL,NG,XMIN,1,XMAX,1,YMIN1,0,
     &                  YMAX1,1,YMIN2,0,YMAX2,1,'energy (eV)',11,YTXT1,
     &                  LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &                  'spin and orbital polarisation of '//TXTT(IT)
     &                  (1:LTXTT(IT))//' in '//SYSTEM(1:LSYSTEM),
     &                  (33+LTXTT(IT)+4+LSYSTEM),KNOHEADER)
C
         NCL = MIN(NLEGMAX,NLT(IT))
C
         LEG(1) = 's'
         LEG(2) = 'p'
         LEG(3) = 'd'
         I0 = ICHAR('1') - 1
         DO IL = 4,NLT(IT)
            LEG(IL) = CHAR(ICHAR('f')+IL-4)
         END DO
C
         CALL XMGRLEG1(IFIL,0,NCL,LEG,0.18D0,0.49D0)
         CALL XMGRLEG1(IFIL,1,NCL,LEG,0.18D0,0.84D0)
C
         WGT = 1.0
C
         DO IL = 1,NLT(IT)
            CALL XMGR4TABLE(0,(IL-1),E,ORBPOL(1,IT,IL,1),WGT,NE,IFIL)
         END DO
C
         DO IL = 1,NLT(IT)
            CALL XMGR4TABLE(1,(IL-1),E,SPNPOL(1,IT,IL,1),WGT,NE,IFIL)
         END DO
C
         WRITE (6,*) '  '
         WRITE (6,*) '   DOS written to file  ',FILNAM(1:LFILNAM)
         CLOSE (IFIL)
C
      END DO
C ----------------------------------------------------------------------
      RETURN
99001 FORMAT (10E12.5)
      END
