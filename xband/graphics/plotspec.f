      SUBROUTINE PLOTSPEC(IFIL,DATASET,LDATASET,WGTGAUTAB,
     &     WLORTAB,WLORVBXPS,NE,KW,TEMP,WGAUSS)


      IMPLICIT NONE

      INTEGER NLEGMAX,NSMAX
      PARAMETER (NLEGMAX=80,NSMAX=1)


      CHARACTER*80 DATASET,HEADER,YTXT1,YTXT2,FNOUT,TITLE,CHTMP
      CHARACTER*20 LEG(NLEGMAX),LEG2(NLEGMAX)
      CHARACTER*10 KW,TXTSPN(0:2)
      INTEGER IFIL,LDATASET,NE,NT,NP,LTITLE,NCURVES
      INTEGER LHEADER
      REAL WGTGAUTAB(0:NE),XMIN,XMAX,
     &     WLORTAB(1:NE),WLORVBXPS(2)
      LOGICAL E_KPARA,I_E,FSPLOT,RD,KNOHEADER
      REAL E(:),THETA(:,:),PHI(:,:),RDATA(:,:,:,:),KP(:,:,:),RTMP
      REAL KPX(:,:),KPY(:,:),YMAX,YMIN,Y1(:),Y2(:)
      REAL TEMP,WGAUSS,ZMIN,ZMAX
      INTEGER JE,JP,JT,LYTXT1,LYTXT2,I,J,LFNOUT
      CHARACTER*80 FILENAME,SCRIPTNAME
      ALLOCATABLE E,THETA,PHI,RDATA,KP,KPX,KPY,Y1,Y2
      KNOHEADER=.TRUE.
      E_KPARA=.FALSE.
      I_E = .FALSE.
      FSPLOT =.FALSE.
      
      READ (IFIL,99004) NT
      READ (IFIL,99004) NP
      ALLOCATE (E(NE),THETA(NE,NT),PHI(NE,NP),KP(NE,NT,NP))
      ALLOCATE (RDATA(NE,NT,NP,0:2),KPX(NT,NP),KPY(NT,NP))
      E=0.0
      THETA=0.0
      PHI=0.0
      KP=0.0
      RDATA=0.0
      
      IF (NE.GT.1 .AND. NT.EQ.1 .AND. NP.EQ.1) I_E=.TRUE.
      IF (NE.GT.1 .AND. NT.GT.1 .AND. NP.EQ.1) E_KPARA=.TRUE.
      IF (NE.EQ.1 .AND. NT.GT.1 .AND. NP.GT.1) FSPLOT=.TRUE.
  
      RD = .TRUE.
      DO WHILE (RD) 
      READ(IFIL,*) CHTMP
      IF (CHTMP(1:4) .EQ. '####') RD=.FALSE.
      END DO
  
      DO JE=1,NE
         DO JP=1,NP
            DO JT=1,NT
               IF (NE.GT.1) THEN
                  READ(IFIL,99006) THETA(JE,JT),E(JE),RDATA(JE,JT,JP,0),
     &                 RDATA(JE,JT,JP,1),RDATA(JE,JT,JP,2),RTMP,KP(JE,
     &                 JT,JP),RTMP
               ELSE
                  READ(IFIL,99006) KPX(JT,JP),KPY(JT,JP),RDATA(JE,JT,JP,
     &                 0),RDATA(JE,JT,JP,1),RDATA(JE,JT,JP,2),RTMP
               END IF
            END DO
         END DO
      END DO
      CLOSE(IFIL)
      WRITE(6,*) "PLOTTING KKRSPEC DATA"


      IF (I_E) THEN
         ALLOCATE (Y1(NE),Y2(NE))
         IF (TEMP.GT.0.0 .OR. WGAUSS.GT.0.0) THEN
            WRITE(6,*) "DATA WILL BE BROADEN WITH FWHM: GAUSSIAN=",
     &           WGAUSS
            WRITE(6,*) "DATA WILL BE FOLDED BY FERMI FUNC.: TEMP=",
     &           TEMP
         DO I=0,2
               Y1(1:NE)=RDATA(1:NE,1,1,I)
               CALL BROAD(E,Y1,Y2,TEMP,WGAUSS,NE)
               RDATA(1:NE,1,1,I)=Y2(1:NE)
         END DO
      END IF

C     ------------------------------------------------------------------
         IFIL = 90
C     
C ----------------------------------------------------------------------
C                              XMGRACE - Total VBXPS
C ----------------------------------------------------------------------
         YTXT1 = 'Intensity (arb. units)'
         LYTXT1 = 22
         YTXT2 = ' '
         LYTXT2 = 1
C     
         YMAX = -1E10
         YMIN = 1E10
         DO I = 0,2
            DO J = 1,NE
               IF ( YMAX.LT.RDATA(J,1,1,I) ) YMAX = RDATA(J,1,1,I)
               IF ( YMIN.GT.RDATA(J,1,1,I) ) YMIN = RDATA(J,1,1,I)
            END DO
         END DO

         XMAX = -1E10
         XMIN = 1E10

         DO J = 1,NE
            IF ( XMAX.LT.E(J) ) XMAX = E(J)
            IF ( XMIN.GT.E(J) ) XMIN = E(J)
         END DO
C     
         TITLE='ARPES  spectrum'
         LTITLE=15
         CALL XMGR4HEAD(DATASET,LDATASET,KW(1:5),5,'',0,
     &        FNOUT,80,LFNOUT,IFIL,1,XMIN,1,XMAX,1,YMIN,1,
     &        YMAX,1,YMIN,1,YMAX,1,'energy (eV)',11,YTXT1,
     &        LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &        TITLE,LTITLE,KNOHEADER)
C
         NCURVES = 3
C
         CALL XMGRCURVES(IFIL,1,NCURVES,NCURVES,2,1,0)
C
         LEG(1) = 'total'
         LEG(2) = 'spin up'      
         LEG(3) = 'spin down'     

         CALL XMGRLEGEND(IFIL,1,NCURVES,NCURVES,LEG,LEG)
C
         DO I = 0,2
            CALL XMGR4TABLE(0,I,E,RDATA(1,1,1,I),1.0,NE,IFIL)
         END DO
C
C
         WRITE (6,*) '  '
         WRITE (6,*) 'Spectrum written to ',FNOUT(1:LFNOUT)
         CLOSE (IFIL)
C
      ELSE IF (E_KPARA) THEN
C ----------------------------------------------------------------------
C             gnuplot E of k_para
C ----------------------------------------------------------------------
         FILENAME = DATASET(1:LDATASET)//'_dn'//'_spc.gnu'
         OPEN (13,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_up'//'_spc.gnu'
         OPEN (12,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_tot'//'_spc.gnu'
         OPEN (11,FILE=FILENAME)

         DO I = 0,2
            IFIL = 11 + I
            do JT = 1,NT
               DO JE=1,NE
                  write (IFIL,'(3f25.10)') THETA(JE,JT),E(JE),RDATA(JE,
     &                 JT,1,I) 
               END DO
               write(IFIL,*)
            END DO
            CLOSE(IFIL)
         END DO
C     
         TXTSPN(1) = 'spin up   '
         TXTSPN(2) = 'spin down '
         TXTSPN(0) = 'total     '
C
         DO I = 0,2
            TITLE = 'ARPES for '//TXTSPN(I)
            
            IF (I.EQ.2) THEN
               FILENAME = DATASET(1:LDATASET)//'_dn'//'_spc.gnu'
               SCRIPTNAME = DATASET(1:LDATASET)//'_dn'//'_spc.gp'
            ELSE IF (I.EQ.1) THEN
               FILENAME = DATASET(1:LDATASET)//'_up'//'_spc.gnu'
               SCRIPTNAME = DATASET(1:LDATASET)//'_up'//'_spc.gp'
            ELSE
               FILENAME = DATASET(1:LDATASET)//'_tot'//'_spc.gnu'
               SCRIPTNAME = DATASET(1:LDATASET)//'_tot'//'_spc.gp'
            END IF
            IFIL=102
            OPEN (UNIT=IFIL,FILE=SCRIPTNAME(1:LEN_TRIM(SCRIPTNAME)
     &           ))
            ZMAX = -1E10
            ZMIN = 1E10
            DO J = 1,NE
               DO JT=1,NT
                  IF ( ZMAX.LT.RDATA(J,JT,1,I) ) ZMAX = RDATA(J,
     &                 JT,1,I)
                  IF ( ZMIN.GT.RDATA(J,JT,1,I) ) ZMIN = RDATA(J,
     &                 JT,1,I)
               END DO
            END DO
            XMAX = -1E10
            XMIN = 1E10
            DO J = 1,NE
               DO JT=1,NT
                  IF ( XMAX.LT.THETA(J,JT)) XMAX = THETA(J,JT)
                  IF ( XMIN.GT.RDATA(J,JT,1,I) ) XMIN = THETA(J,JT)
               END DO
            END DO
            YMAX = -1E10
            YMIN = 1E10
            
            DO J = 1,NE
               IF ( YMAX.LT.E(J) ) YMAX = E(J)
               IF ( YMIN.GT.E(J) ) YMIN = E(J)
            END DO
C

            CALL CREATEGNUPLOTARPES(IFIL,FILENAME,XMIN,
     &           XMAX,YMIN,YMAX,TITLE,ZMIN,ZMAX)
               
            CLOSE (IFIL)
         END DO

      END IF
      

99004 FORMAT (10X,I10)
99006 FORMAT (2x,e14.5,2x,e14.5,2x,e14.5,2x,e14.5,2x,e14.5,2x,e14.5,2x,
     &        e14.5,2x,e14.5,2x,e14.5)

      END

      SUBROUTINE CREATEGNUPLOTARPES(ISCRIPT,FILENAME,XMIN,XMAX,YMIN,
     &     YMAX,TITLE,ZMIN,ZMAX)
        IMPLICIT NONE

        CHARACTER*80 FILENAME,TITLE
        INTEGER      I,ISCRIPT,J
        REAL    ymin,ymax,xmin,xmax,zmin,zmax

        IF (ZMIN .LT. 1.0D-10) ZMIN=1.D-10

C ######################################################################
        write(ISCRIPT,99001)"setting output"
        write(ISCRIPT,99005)'set term postscript enhanced color'
        write(ISCRIPT,99006) FILENAME(1:LEN_TRIM(FILENAME)-4)
        write(ISCRIPT,99004)
C
C        
        
C ######################################################################
        write(ISCRIPT,99001)"setting title"
        write(ISCRIPT,99007) TITLE(1:LEN_TRIM(TITLE))
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting variables"
        write(ISCRIPT,99003)"emin",ymin
        write(ISCRIPT,99003)"emax",ymax
        write(ISCRIPT,99003)"kstart",xmin
        write(ISCRIPT,99003)"kend",xmax
        write(ISCRIPT,99003)"zmin",zmin
        write(ISCRIPT,99003)"zmax",zmax
C ######################################################################
        write(ISCRIPT,99001)"setting style"
        write(ISCRIPT,99005)'set lmargin 0'
        write(ISCRIPT,99005)'unset key'
        write(ISCRIPT,99005)'unset ztics'
        write(ISCRIPT,99005)'set view map'
        write(ISCRIPT,99005)'set format y "%1.2f"'
        write(ISCRIPT,99005)'set pm3d at b'
C        write(ISCRIPT,99005)'set multiplot layout 1,1'
        write(ISCRIPT,99005)'set colorbox'
        write(ISCRIPT,99005)'set pointsize 0.6'
        write(ISCRIPT,99005)'set pm3d explicit'
        write(ISCRIPT,99004)
C ######################################################################
        write(ISCRIPT,99001)"setting labels"
        write(ISCRIPT,99005)'set ylabel "E (eV)"'
        write(ISCRIPT,99005)'set cblabel "arb. units"'
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting colours"
        write(ISCRIPT,99005)'#set palette rgbformulae 21,22,23'
        write(ISCRIPT,99005)'#set palette rgbformulae 34,35,36'
        write(ISCRIPT,99005)'#set palette rgbformulae 7,5,15'
        write(ISCRIPT,99005)'#set palette defined (0 "white", 5 "grey",
     &       6"blue", 7 "red", 9 "green", 10 "black")'
        write(ISCRIPT,99005)'set palette grey'
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99001)"setting ranges"
        write(ISCRIPT,99005)"set xrange [kstart:kend]"
        write(ISCRIPT,99005)"set yrange [emin:emax]"
        write(ISCRIPT,99005)"set cbrange [zmin:zmax]"
C        write(ISCRIPT,99005)"set log zcb"
        write(ISCRIPT,99004)

C ######################################################################
        write(ISCRIPT,99002) FILENAME(1:LEN_TRIM(FILENAME))

C ######################################################################
        write(ISCRIPT,99005)'set term x11 enhanced'
        write(ISCRIPT,99005)'replot'
        write(ISCRIPT,99005)'pause -1'
        write(ISCRIPT,99004)
C ###################################################################### 

99001 FORMAT (80('#'),/,'# ',A,/,80('#'))
99002 FORMAT ("splot '",A,"' u ($1):($2):($3) with pm3d at b")
99003 FORMAT (A,'=',99E15.8)
99004 FORMAT (80('#'),/)
99005 FORMAT (A)
99006 FORMAT ('set output "',A,'.ps"')
99007 FORMAT ('set title "',A,'"')
99008 FORMAT ('set arrow from',E15.8,',',E15.8,',',E15.8,1X,'to'
     &                     ,E15.8,',',E15.8,',',E15.8,1X,'nohead front')

99009 FORMAT ('set xtics ( \')
99010 FORMAT (', \')
99011 FORMAT ('"{',A,'}"',F7.4,'\')
99012 FORMAT (')')
         END

C*==broad.f    processed by SPAG 6.55Rc at 14:26 on 24 Nov 2008
      SUBROUTINE BROAD(E,FE,G,T,FWHM,LL)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER LL
      REAL E(LL),FE(LL),G(LL)
C
C Local variables
C
      REAL A,AM,ARGG,EM,F(:,:),FERMI(:),FWHM,GAUSS(:,:),H,
     &     K,T,WPI,XF,EI(:)
      INTEGER I,J

      ALLOCATABLE F,FERMI,GAUSS,EI
C
C*** End of declarations rewritten by SPAG
C
      ALLOCATE(F(LL,LL),FERMI(LL),GAUSS(LL,LL),EI(LL))

      K = 0.862E-4
      WPI = SQRT(3.1415926535)
      A = 2.0*SQRT(LOG(2.0))/FWHM
C     Berechnung der Fermifunktion
      H=E(2)-E(1)
      DO I = 1,LL
         EI(I)=E(I)
         XF = E(I)/(K*T)
         IF ( XF.LT.-16.0 ) THEN
            FERMI(I) = 1.0
         ELSE IF ( XF.GT.16.0 ) THEN
            FERMI(I) = 0.0
         ELSE
            FERMI(I) = 1.0/(1.0+EXP(XF))
         END IF
      END DO
      
C     Gaussfaltung des spektrums***************************************
      DO I = 1,LL
         DO J = 1,LL
            AM = A*A
            EM = (E(J)-EI(I))*(E(J)-EI(I))
            ARGG = AM*EM
            IF ( ARGG.GT.50 ) THEN
               GAUSS(J,I) = 0.0
            ELSE
               GAUSS(J,I) = EXP(-ARGG)
            END IF
            IF ( GAUSS(J,I).LT.1.0E-16 ) THEN
               F(I,J) = (A/WPI)*FE(J)*1.0E-16*FERMI(J)
            ELSE
               F(I,J) = (A/WPI)*FE(J)*GAUSS(J,I)*FERMI(J)
            END IF
         END DO
      END DO

      DO I = 1,LL
         CALL SIMPS(H,F,G,I,LL)
      END DO
      DO I=1,LL
         G(I)=-G(I)
      END DO
      END
C*==simps.f    processed by SPAG 6.55Rc at 14:26 on 24 Nov 2008
C
      SUBROUTINE SIMPS(H,F,QMT,I,LL)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL H
      INTEGER I,LL
      REAL F(LL,LL),QMT(LL)
C
C Local variables
C
      INTEGER J
C
C*** End of declarations rewritten by SPAG
C
      QMT(I) = F(I,1)
      DO J = 1,LL - 2,2
         QMT(I) = QMT(I) + 4.0*F(I,J+1) + 2.0*F(I,J+2)
      END DO
      QMT(I) = (H/3.0)*(QMT(I)-F(I,LL))
      END
C
