C*==plotsprcvvaes.f    processed by SPAG 6.05Rc at 09:49 on 16 Dec 2001
      SUBROUTINE PLOTSPRCVVAES(IFIL,DATASET,LDATASET,N1,N2,NQMAX,NTMAX,
     &                         NEMAX,NCMAX,NPOLMAX,NLMAX,SPEC,NLQ,NLT,
     &                         NMIN,NMAX,WMIN,WMAX,IQOFT,NQT,CONC,TXTT,
     &                         LTXTT,INFO,LINFO,HEADER,LHEADER,SYSTEM,
     &                         LSYSTEM,E,NQ,NT,NS,NE,IREL,EF,EXRS,IXRS,
     &                         BXRS,WXRS,NVMAX,NEXRSMAX,WGTGAUTAB,
     &                         WLORTAB,WLORAES,EEXP,IEXP,NCEXPMAX,
     &                         NPOLEXPMAX,KW)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX,NCSTMAX,IS,NADD
      PARAMETER (NLEGMAX=80,NCSTMAX=20,IS=3,NADD=30)
C
C COMMON variables
C
      REAL BLSEXP(2),EMAX,EMIN,EREFER,ESEXP(2),RYOVEV,SCLEXP(2),WGAUSS,
     &     WLOR(2)
      CHARACTER*2 EUNIT
      LOGICAL EXPDATAVL
      CHARACTER*80 FMTEXP
      INTEGER MERGE,NCEXP,NEEXP(2),NPOLEXP
      COMMON /BRDPAR/ WGAUSS,WLOR
      COMMON /ENERGY/ RYOVEV,EREFER,EUNIT
      COMMON /EXPPAR/ ESEXP,BLSEXP,SCLEXP,NEEXP,NCEXP,NPOLEXP,FMTEXP,
     &                EXPDATAVL
      COMMON /PLOPAR/ EMIN,EMAX,MERGE
C
C Dummy arguments
C
      REAL EF,EXRS,WXRS
      CHARACTER*80 DATASET,HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSYSTEM,NCEXPMAX,NCMAX,
     &        NE,NEMAX,NEXRSMAX,NLMAX,NPOLEXPMAX,NPOLMAX,NQ,NQMAX,NS,NT,
     &        NTMAX,NVMAX
      CHARACTER*10 KW
      CHARACTER*3 SPEC
      REAL BXRS(NVMAX,NEMAX),CONC(NTMAX),E(NEMAX),EEXP(NEMAX,NCEXPMAX),
     &     IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX),IXRS(NVMAX,NEMAX),
     &     N1(NEMAX,0:NTMAX,0:NCMAX,0:NPOLMAX),
     &     N2(NEMAX,0:NTMAX,0:NCMAX,0:NPOLMAX),NMAX(0:NTMAX),
     &     NMIN(0:NTMAX),WGTGAUTAB(0:NEMAX),WLORAES(2),WLORTAB(1:NEMAX),
     &     WMAX(0:NTMAX),WMIN(0:NTMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      REAL DE,DELCST(:,:),DXG,EALL(:),ECOR(NCSTMAX),
     &     ECST(:,:),EFERMI,ENE,ETABFIN(:),EZERO,
     &     FB(:,:),IINTER(:,:,:),ISUM(:,:),ITMP(3),
     &     RTMP,SPC(:,:),SPCTMP(:,:),WDT,WDT1,WDT2,WG(:)
     &     ,WORK2(:),WTMP(:),X(:),XINP,XMAX,XMIN
     &     ,Y(:),YDUMG(:,:),YMAX,YMAX2,YMIN,YMIN2,YOUT
      CHARACTER*80 FNOUT,STRG,YTXT1,YTXT2
      INTEGER I,ICST,IE,IEE,IENEW,IEX,IGMAX,II,IIS,IITMP,INE,IREGION,
     &        ISP,J,KAPCOR(NCSTMAX),KNOHEADER,LFNOUT,LYTXT1,LYTXT2,NCST,
     &        NEALL,NEFIN,NESEPAR,NESPC,NKPCOR(NCSTMAX),NNEW,NNMAX,
     &        NNMIN,NP,NPOINTS,NSEPAR,REG(:)
      COMPLEX*16 IAES(:,:,:),ISPC(:,:,:)
      CHARACTER*20 LEG(NLEGMAX),LEG2(NLEGMAX)
      CHARACTER*10 STR10
      REAL YLAG

      ALLOCATABLE EALL,IAES,ECST,ISPC,ISUM,WTMP,WORK2,X,Y,YDUMG,FB,WG
      ALLOCATABLE DELCST,IINTER,SPCTMP,REG,SPC,ETABFIN
      ALLOCATE (EALL(NEMAX),IAES(NCSTMAX,NEMAX,IS))
      ALLOCATE (ECST(NCSTMAX,NEMAX),ISPC(NCSTMAX,-NADD+1:NEMAX,IS))
      ALLOCATE (ISUM(NEMAX,IS),WTMP(-NADD+1:NEMAX),WORK2(NEMAX))
      ALLOCATE (X(NEMAX),Y(NEMAX),YDUMG(IS,NEMAX),FB(NEMAX,3))
      ALLOCATE (WG(0:NEMAX),DELCST(NCSTMAX,NEMAX))
      ALLOCATE (IINTER(NCSTMAX,NEMAX,IS),SPCTMP(IS,NEMAX))
      ALLOCATE (REG(NEMAX),SPC(NEMAX,IS),ETABFIN(-NADD+1:NEMAX))
C
C*** End of declarations rewritten by SPAG
C
C
C
C
C **********************************************************************
C *                                                                    *
C     *  plot SPR-CVV-AES - spectra
C     *
C *                                                                    *
C **********************************************************************
C
C
C
C ----------------------------------------------------------------------
C ----------------------------------------------------------------------
      IF ( KW(1:6).EQ.'SP-AES' ) THEN
         WRITE (*,*) 
     &            'Spin-resolved CVV-AES from relativistic calculations'
         REWIND (IFIL)
 50      CONTINUE
         READ (IFIL,'(a)') STRG
         IF ( STRG(1:10).NE.'IREL      ' ) GOTO 50
         READ (IFIL,99002) STR10,EFERMI
         WRITE (*,99001) EFERMI*RYOVEV
         REWIND (IFIL)
 100     CONTINUE
         READ (IFIL,'(a)') STRG
         IF ( STRG(1:5).NE.' CORE' ) GOTO 100
         READ (IFIL,99003) NCST
         IF ( NCST.EQ.2 ) THEN
            NSEPAR = 1
         ELSE IF ( NCST.EQ.6 ) THEN
            NSEPAR = 2
         ELSE IF ( NCST.EQ.10 ) THEN
            NSEPAR = 4
         ELSE IF ( NCST.EQ.14 ) THEN
            NSEPAR = 6
         END IF
         REWIND (IFIL)
 150     CONTINUE
         READ (IFIL,'(a)') STRG
         IF ( STRG(1:7).NE.' output' ) GOTO 150
         READ (IFIL,'(a)') STRG
         WRITE (*,*) STRG
C----------
C     reading spectrum
C----------
         DO ICST = 1,NCST
            IE = 0
            DO I = 1,NEMAX
               READ (IFIL,'(1x,e12.5,2x,e12.5,a)') ECST(ICST,I),
     &               DELCST(ICST,I)
               ECST(ICST,I) = ECST(ICST,I)*RYOVEV
               READ (IFIL,'(20x,6(1x,e18.10))') IAES(ICST,I,1),
     &               IAES(ICST,I,2),IAES(ICST,I,3)
               IE = IE + 1
               IF ( DELCST(ICST,I).EQ.0D0 ) GOTO 200
            END DO
 200     END DO
         NE = IE
C------------------------------------------------------------
C     reading CORE STATES
C------------------------------------------------------------
         REWIND (IFIL)
 250     CONTINUE
         READ (IFIL,'(a)') STRG
         IF ( STRG(1:6).NE.'LCXRAY' ) GOTO 250
C
         READ (IFIL,99004) NCST,(NKPCOR(ICST),ICST=1,NCST)
         READ (IFIL,'(a)') STRG
         READ (IFIL,'(a)') STRG
         DO ICST = 1,NCST
            READ (IFIL,99005) IITMP,IITMP,IITMP,KAPCOR(ICST),IITMP,
     &                        IITMP,RTMP,RTMP,ECOR(ICST),RTMP,IITMP
            IF ( NKPCOR(ICST).EQ.2 ) READ (IFIL,99006) IITMP,RTMP
         END DO
         CLOSE (IFIL)
      ELSE
         STOP 'STOP '
C
      END IF
C
      CALL EREF(EZERO,EFERMI,KW,ECOR,NCST,NCSTMAX)
C     ------------------------------------------------------------------
C
C
C=========================================================
C     loop over the core states
C=========================================================
C
      DO ICST = 1,NCST
         WDT2 = WLORAES(1)
         WDT = WLORAES(2)
C---------------------------------------------------------
C     extend e-range above calculated limit
C---------------------------------------------------------
         DE = (ECST(ICST,NE)-ECST(ICST,1))/(NE-1)
         DO INE = 1,NE
            ETABFIN(INE) = ECST(ICST,1) + DE*(INE-1)
            DO IIS = 1,3
               ISPC(ICST,INE,IIS) = IAES(ICST,INE,IIS)
            END DO
         END DO
         DO IE = 1,NE
            WTMP(IE) = 0.0D0
            WTMP(IE) = WDT*(ECST(ICST,NE)-ECST(ICST,IE))**2
         END DO
         DO IE = NE + 1,NE + NADD
            ETABFIN(IE) = 0.0D0
            ETABFIN(IE) = ECST(ICST,1) + DE*(IE-1)
            WTMP(IE) = 0.0000000001D0
            DO IIS = 1,3
               ISPC(ICST,IE,IIS) = (0.0D0,0.0D0)
            END DO
         END DO
         DO IE = 0,1 - NADD, - 1
            ETABFIN(IE) = ECST(ICST,1) - DE*ABS(IE-1)
            WTMP(IE) = 0.0000000001D0
            DO IIS = 1,3
               ISPC(ICST,IE,IIS) = (0.0D0,0.0D0)
            END DO
         END DO
         NPOINTS = 0
         NNMAX = NE + NADD
         NNMIN = NADD - 1
         DO IE = -NNMIN,NNMAX
            NPOINTS = NPOINTS + 1
            E(NPOINTS) = ETABFIN(IE)
            WLORTAB(NPOINTS) = WTMP(IE)
            DO IIS = 1,3
               SPC(NPOINTS,IIS) = DREAL(ISPC(ICST,IE,IIS))
               SPCTMP(IIS,NPOINTS)=DREAL(ISPC(ICST,IE,IIS))
            END DO
         END DO
         NEFIN = NPOINTS
C---------------------------------------------------------
C     Lorentzian broadening
C---------------------------------------------------------
         IF ( WDT2.GT.0.D0 ) THEN
            CALL LORBRDAES(E,SPC,FB,3,3,NPOINTS,NEMAX,WDT2)
         ELSE
            PRINT *,' Lorentzian broadening skipped!'
         END IF
         DO IE = 1,NEFIN
            ECST(ICST,IE) = E(IE)
            DO IIS = 1,3
                IAES(ICST,IE,IIS) = DCMPLX(SPC(IE,IIS))
                SPCTMP(IIS,IE)=SPC(IE,IIS)
            END DO
         END DO
         DO IE = 1,NEFIN
            ENE = 0.0D0
            ENE = ECST(ICST,IE) + EZERO - 2*EFERMI
         END DO
C---------------------------------------------------------
C     Lorentzian broadening - energy dependent (quadratic)
C---------------------------------------------------------
         IF ( WDT.GT.0.D0 ) THEN
            CALL LORBRDAESENE(E,SPC,FB,3,3,NPOINTS,NEMAX,WLORTAB)
         ELSE
            PRINT *,' Energy dependent Lorenzian broadening skipped'
         END IF
         DO IE = 1,NEFIN
            ECST(ICST,IE) = E(IE)
            DO IIS = 1,3
               IAES(ICST,IE,IIS) = DCMPLX(SPC(IE,IIS))
               SPCTMP(IIS,IE)=SPC(IE,IIS)
            END DO
         END DO
         WRITE (*,*) 'Lorenzian, E^2 energy dep. for core level',ICST
C------------------------------------------------------gausian broad
         WDT1 = WGAUSS
C  
             WRITE(*,*) "WGAUSS SET TO ZERO !!!!!!!!!"
             WDT1=0.0d0
         IF ( WDT1.GT.0.D0 ) THEN
            CALL INIGAUBRD(WDT1,WG,NEMAX,DXG,IGMAX)
            CALL VECGAUBRD(E,SPCTMP,YDUMG,3,IS,NPOINTS,NEMAX,WG,DXG
     &           ,IGMAX)

            DO IE = 1,NEFIN
               ECST(ICST,IE) = E(IE)
               DO IIS = 1,3
                   IAES(ICST,IE,IIS) = DCMPLX(YDUMG(IIS,IE))
               END DO
               write(67,*) e(ie),DREAL(IAES(ICST,IE,1)),DREAL(IAES(ICST
     &              ,IE,2)),DREAL(IAES(ICST,IE,3))
            END DO
         ELSE
            DO IE = 1,NEFIN
               ECST(ICST,IE) = E(IE)
               DO IIS = 1,3
                  IAES(ICST,IE,IIS) = SPC(IE,IIS)
               END DO
            END DO
            PRINT *,' Gausian broadening skipped!'
         END IF
C
      END DO                    !ncst
C---------
C setting of the energy region
C-------
      EMIN = 1.E10
      EMAX = 0
      DO ICST = 1,NCST
         DO IE = 1,NEFIN
            IF ( ECST(ICST,IE).LE.EMIN ) EMIN = ECST(ICST,IE)
            IF ( ECST(ICST,IE).GE.EMAX ) EMAX = ECST(ICST,IE)
         END DO
      END DO
C---------------
C     setup of new e-mesh
C--------------
      NEALL = NEFIN*NCST
      DE = (EMAX-EMIN)/(NEALL-1)
      DO IE = 1,NEALL
         EALL(IE) = EMIN + DE*(IE-1)
      END DO
C-----------------
C  interpolation
C----------------
      DO ICST = 1,NCST
         DO NP = 1,NEALL
            DO I = 1,IS
               IINTER(NCST,NP,I) = 0.0D0
            END DO
         END DO
      END DO
      DO I = 1,NEALL
         Y(I) = 0
      END DO
C------------
      DO ICST = 1,NCST
         IREGION = 0
         DO IE = 1,NEALL
            IF ( (EALL(IE).GE.ECST(ICST,1)) .AND. 
     &           (EALL(IE).LE.ECST(ICST,NEFIN)) ) THEN
               IREGION = IREGION + 1
               REG(IREGION) = IE
            END IF
         END DO
         DO IENEW = 1,IREGION
            NNEW = REG(IENEW)
            XINP = EALL(NNEW)
            DO I = 1,IS
               DO IEE = 1,NEFIN
                  Y(IEE) = 0
                  X(IEE) = 0
                  Y(IEE) = DREAL(IAES(ICST,IEE,I))
                  X(IEE) = ECST(ICST,IEE)
               END DO
               IEX = 1
               YOUT = 0.0D0
               YOUT = YLAG(XINP,X,Y,0,2,NEMAX,IEX)
               IINTER(ICST,NNEW,I) = YOUT
            END DO
         END DO
      END DO
C-----------------
C     sumation of spectra
C----------------
      DO IE = 1,NEALL
         DO I = 1,IS
            DO ICST = 1,NCST
               ISUM(IE,I) = ISUM(IE,I) + IINTER(ICST,IE,I)
            END DO
         END DO
      END DO

C
C
C------------------------------------------------------------------
C ----------------------------------------------------------------------
CC     XMGRACE - Spin resolved
      DO IE = 1,NEALL
         EALL(IE) = EALL(IE) + EZERO - 2*EFERMI
      END DO
      XMAX = EALL(NEALL)
      XMIN = EALL(1)
      YTXT1 = 'Intensity (arb. units)'
      LYTXT1 = 23
      YTXT2 = 'Intensity (arb. units)'
      LYTXT2 = 23
C
      YMAX = -1D-10
      YMIN = 1D10
      DO I = 1,2
         DO IE = 1,NEALL
            IF ( YMAX.LT.ISUM(IE,I) ) YMAX = ISUM(IE,I)
            IF ( YMIN.GT.ISUM(IE,I) ) YMIN = ISUM(IE,I)
         END DO
      END DO
      YMAX2 = -1D10
      YMIN2 = 1D10
      DO J = 1,NEALL
         IF ( YMAX2.LT.(ISUM(J,2)-ISUM(J,1))/(ISUM(J,1)+ISUM(J,2)+1D-20)
     &        ) YMAX2 = (ISUM(J,2)-ISUM(J,1))
     &                  /(ISUM(J,1)+ISUM(J,2)+1D-20)
         IF ( YMIN2.GT.(ISUM(J,2)-ISUM(J,1))/(ISUM(J,1)+ISUM(J,2)+1D-20))
     &        ) YMIN2 = (ISUM(J,2) - ISUM(J,1))
     &                  /(ISUM(J,1)+ISUM(J,2)+1D-20)
      END DO
      CALL XMGR4HEAD(DATASET,LDATASET,'aes',3,' ',0,FNOUT,80,LFNOUT,
     &               IFIL,2,XMIN,1,XMAX,1,YMIN2,1,YMAX2,1,YMIN,1,YMAX,1,
     &               'energy (eV)',11,YTXT1,LYTXT1,YTXT2,LYTXT2,HEADER,
     &               LHEADER,'Spin resolved CVV-AES of '//
     &               SYSTEM(1:LSYSTEM),(25+LSYSTEM),KNOHEADER)
C
C
      CALL XMGRCURVES(IFIL,2,1,2,2,1,0)
C
C
      LEG(2) = 'I!S!UP'
      LEG(1) = 'I!S!DN'
      LEG2(1) = 'I!S!UP!N-I!S!DN'
C
C
      CALL XMGRLEGEND(IFIL,2,1,2,LEG2,LEG)
C
C
C
      DO I = 1,NEMAX
         WORK2(I) = (ISUM(I,2)-ISUM(I,1))  !/(ISUM(I,1)+ISUM(I,2)+1D-20)
      END DO
      CALL XMGR4TABLE(0,0,EALL,WORK2(1),1.0,NEALL,IFIL)
C
      CALL XMGR4TABLE(1,0,EALL,ISUM(1,1),1.0,NEALL,IFIL)
      CALL XMGR4TABLE(1,1,EALL,ISUM(1,2),1.0,NEALL,IFIL)
C
C
      WRITE (6,*) '  '
      WRITE (6,*) 'Spin resolved CVV-AES writen in ',FNOUT(1:LFNOUT)
C
      RETURN
99001 FORMAT ('EF=',F12.5,'eV')
99002 FORMAT (A10,F10.5)
99003 FORMAT (/,' ncst:  ',i4)
99004 FORMAT (//,' CORE STATES :',//,' NCST:  ',I4,/,' NKPCOR:',20I4)
99005 FORMAT (5I4,'/2',I4,F12.6,F12.4,F12.3,F12.4,I5)
99006 FORMAT (22X,I4,F12.6)
C
99007 FORMAT (200E12.5)
99008 FORMAT ('#  ',A,A,A)
C
99009 FORMAT (10X,A80)
99010 FORMAT (10X,I10)
99011 FORMAT (10X,I10,5(:,3X,A,1X))
99012 FORMAT (10X,10F10.5)
99013 FORMAT (I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99014 FORMAT (10X,A)
99015 FORMAT (8F10.4)
C
99016 FORMAT (A10,I10)
99017 FORMAT (A10,I10,5(:,'  (',A,')'))
99018 FORMAT (A10,10F10.5)
99019 FORMAT (A10,A)
99020 FORMAT (11X,10E12.5)
C
C
      END
C*==eref.f    processed by SPAG 6.05Rc at 09:49 on 16 Dec 2001
C
C
      SUBROUTINE EREF(EZERO,EFERMI,KW,ECOR,NCST,NCSTMAX)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL RYOVEV
      PARAMETER (RYOVEV=13.605826)
C
C Dummy arguments
C
      REAL EFERMI,EZERO
      CHARACTER*10 KW
      INTEGER NCST,NCSTMAX
      REAL ECOR(NCSTMAX)
C
C*** End of declarations rewritten by SPAG
C
C========================================================
C Setting EZERO
C========================================================
      EZERO = 0.0D0
      EFERMI = EFERMI*RYOVEV
      IF ( NCST.EQ.2 ) THEN
         EZERO = (ECOR(1)+ECOR(2))/2D0
         WRITE (*,99001) ABS(EZERO)
      ELSE IF ( NCST.EQ.6 ) THEN
         EZERO = (ECOR(1)+ECOR(2))/2D0
         WRITE (*,99001) ABS(EZERO)
      ELSE IF ( NCST.EQ.10 ) THEN
         EZERO = (ECOR(1)+ECOR(2)+ECOR(3)+ECOR(4))/4D0
         WRITE (*,99001) ABS(EZERO)
      ELSE IF ( NCST.EQ.14 ) THEN
         EZERO = (ECOR(1)+ECOR(2)+ECOR(3)+ECOR(4)+ECOR(5)+ECOR(6))/6
         WRITE (*,99001) ABS(EZERO)
      END IF
C
99001 FORMAT ('EZERO',F12.5,'eV')
      END
C*==lorbrdaes.f    processed by SPAG 6.05Rc at 09:49 on 16 Dec 2001
C
      SUBROUTINE LORBRDAES(E,F0,FB,NV,NVMAX,NPOINTS,NEMAX,G0)
C
C **********************************************************************
C *                                                                    *
C *  lorentzian-broadening of the function   f0   width: g0            *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL G0
      INTEGER NEMAX,NPOINTS,NV,NVMAX
      REAL E(NEMAX),F0(NEMAX,NVMAX),FB(NEMAX,NVMAX)
C
C Local variables
C
      DOUBLE PRECISION DBLE
      REAL G,W,X0,X1,X2
      INTEGER I,IE,IIS,IV,J
C
C*** End of declarations rewritten by SPAG
C
      DO IE = 1,NPOINTS
         DO IV = 1,NV
            FB(IE,IV) = 0.0D0
         END DO
      END DO
C
      DO I = 1,NPOINTS
         X0 = E(I)
         DO J = 1,NPOINTS - 1
C
            X1 = E(J) + 0.000001
            X2 = E(J+1) - 0.000001
C
            G = G0
            W = ATAN(DBLE((X2-X1)/(G+(X2-X0)*(X1-X0)/G)))/2.0
C
            DO IV = 1,NV
               FB(I,IV) = FB(I,IV) + W*(F0(J,IV)+F0(J+1,IV))
            END DO
         END DO
      END DO
      DO IE = 1,NPOINTS
         DO IIS = 1,NV
            F0(IE,IIS) = FB(IE,IIS)
         END DO
      END DO
      END
C*==lorbrdaesene.f    processed by SPAG 6.05Rc at 09:49 on 16 Dec 2001
C
      SUBROUTINE LORBRDAESENE(E,F0,FB,NV,NVMAX,NE,NEMAX,WLORTAB)
C **********************************************************************
C *                                                                    *
C *  lorentzian-broadening of the function   F0   width: G0            *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL PI
      PARAMETER (PI=3.141592653589793238462643D00)
C
C Dummy arguments
C
      INTEGER NE,NEMAX,NV,NVMAX
      REAL E(NEMAX),F0(NEMAX,NVMAX),FB(NEMAX,NVMAX),WLORTAB(NEMAX)
C
C Local variables
C
      REAL G,W,X0,X1,X2
      INTEGER I,IE,IIS,J
C
C*** End of declarations rewritten by SPAG
C
C
C
      DO IE = 1,NE
         DO IIS = 1,NV
            FB(IE,IIS) = 0.0D0
         END DO
      END DO
      DO I = 1,NE
         DO IIS = 1,NV
            X0 = E(I)
C
            DO J = 1,NE - 1
C
               X1 = E(J) + 0.000001D0
               X2 = E(J+1) - 0.000001D0
C
               G = (WLORTAB(J)+WLORTAB(J+1))/2.D0
               W = ATAN((X2-X1)/(G+(X2-X0)*(X1-X0)/G))/2.D0
               FB(I,IIS) = FB(I,IIS) + W*(F0(J,IIS)+F0(J+1,IIS))/PI
            END DO
         END DO
      END DO
      DO IE = 1,NE
         DO IIS = 1,NV
            F0(IE,IIS) = FB(IE,IIS)
         END DO
      END DO
      END
