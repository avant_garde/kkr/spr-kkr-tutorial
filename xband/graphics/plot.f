      PROGRAM PLOT
c **********************************************************************
c *                                                                    *
c *  program to plot bandstructure data  DOS, SPECTRA ....             *
c *  using the plotting programm   XMGRACE                             *
c *  the program as well as these routines work with REAL*4            *
c *  however some standard input routines are called that expect       *
c *  REAL*8 -- exchange is done via the variables R8 and R8A(NR8AMAX)  *
c *--------------------------------------------------------------------*
c *  PLOT can deal with the various datafiles directly (supplied via   *
c *  standard input) without any additional input for the layout       *
c *  if the layout should be modified or experimental data is          *
c *  available to be plotted this can be done be reading a control     *
c *  file from standard input which contains the necessary information *
c *  this control file may contain various sections beginning with     *
c *  a keyword in the first column of a line. the keyword may be       *
c *  followed by various lines (with first column empty) containing    *
c *  the setting of parameters. all input following the character #    *
c *  is assumed to be comment i.e. is ignored                          *
c *  for all parameters the specification is optionally                *
c *--------------------------------------------------------------------*
c *  KEYWORD    PARAMETER                                              *
c *--------------------------------------------------------------------*
c *  DATASET    string          filename of the dataset created by     *
c *                             the bandstructure program              *
c *                                                                    *
c *  PLOT       EMIN                                                   *
c *             EMAX                                                   *
c *             DE                                                     *
c *             DY                                                     *
c *             MERGE                                                  *
c *                                                                    *
c *  BROADEN    WL                                                     *
c *             WG                                                     *
c *                                                                    *
c *  EXPERIMENT FILE = string   filename of the experimental dataset   *
c *             ESEXP           shift of the energy scale for plotting *
c *             BLSEXP          shift of the base line for plotting    *
c *             SCLEXP          scaling of the data for plotting       *
c *             NCEXP                                                  *
c *             NPOLEXP                                                *
c *             FMT                                                    *
c *                                                                    *
c *  OUTPUT     JDOS            print "JDOS"                           *
c *             TABLE           KW=DOS:  tabulate   DOS  on file       *
c *                             KW=RXAS: tabulate   abs.coeff. on file *
c *                                                                    *
c **********************************************************************
c common limit for  NPOL  and   NS                                 >= 3
c NSPMAX = 9  to deal with full SIGMA - tensor 
      PARAMETER ( NSPMAX    =     9 )
c don't set lower than number of core states in <PLOTRXAS>      L2,3: 6
      PARAMETER ( NCMAX     =    12 )
      PARAMETER ( NEXRSMAX  =    60 )
c max. number of experimental curves NCEXPMAX
c if > 2 then increase dimension of ESEXP, BLSEXP ...
      PARAMETER ( NCEXPMAX  =     2 )
      PARAMETER ( NPOLEXPMAX=     3 )
      PARAMETER ( NR8AMAX   =   200 )

c ----------------------------------------------------------------------
c   this table is also present in <PLOTRXAS>
c
      PARAMETER ( LCMDMAX = 500 )
      PARAMETER ( LSTRMAX = 100 )
      PARAMETER ( IFTABLE = 98, IFCONVT = 99 )
      PARAMETER ( IFC=IFCONVT, LSM=LSTRMAX )
c ----------------------------------------------------------------------
      CHARACTER*10 KW
      REAL         N(:,:,:,:), E(:), Y(:)
      REAL         NW(:,:,:,:)
      REAL         WXRS(:,:,:,:), EXRS(:)
      REAL         IXRS(:,:), BXRS(:,:)
      REAL         WGTGAUTAB(:), WLORTAB(:)
      REAL         WLORVBXPS(2), WLORAES(2)
      REAL         EEXP(:,:),IEXP(:,:,:)

      INTEGER      NVAL
      INTEGER      NCXRAY(:), LCXRAY(:)
      INTEGER      ITXRSGRP(:)
      INTEGER      NLQ(:)
      INTEGER      NLT(:)
      INTEGER      IQOFT(:,:), NQT(:), LTXTT(:) 
      REAL         CONC(:),TEMP 
      REAL         NMIN(:), NMAX(:)
      REAL         WMIN(:), WMAX(:)
      
      REAL*8 R8, R8A(NR8AMAX)

      CHARACTER*10 TXTT(:)
      CHARACTER*80 HEADER,SYSTEM,INFO

      REAL    AD(:,:,:,:),AT(:,:,:,:)
      REAL    ECORE(:,:)
      REAL    TD(:),TT(:),TE(:)

      INTEGER KAPCOR(:), MM05COR(:),
     &        NKPCOR(:),  IKMCOR(:,:)
      INTEGER IN(:), KTYPCOR(:),ICOLOR

      REAL*8  TETQPH(:),PHIQPH(:)
      REAL*8  TETEPH(:),PHIEPH(:)
      REAL*8  TETKEL(:),PHIKEL(:)

c ----------------------------------------------------------------------

c ----------------------------------------------------------------------
      CHARACTER*(LCMDMAX) CMDUC, CMD00
      CHARACTER*(LSTRMAX) DATASET, STR, EXPDATSET
      LOGICAL FOUND, TABLE, CMDFOUND, UDT, KJDOS,KTABLE, MLD,MTV 
      LOGICAL NOCORESPLIT, NOTABEXT, TABEXT, KNOHEADER,IQRES
c ----------------------------------------------------------------------
      COMMON /PLOPAR/ EMIN,EMAX, MERGE
      CHARACTER FMTEXP*80
      LOGICAL EXPDATAVL
      COMMON /EXPPAR/ ESEXP(2),BLSEXP(2),SCLEXP(2),NEEXP(2),
     &                NCEXP,NPOLEXP,FMTEXP,EXPDATAVL
      COMMON /BRDPAR/ WGAUSS,WLOR(2)
      CHARACTER EUNIT*2
      COMMON /ENERGY/ RYOVEV, EREFER,EUNIT

c ----------------------------------------------------------------------
      DATA ESEXP /0.0,0.0/, BLSEXP /0.0,0.0/,SCLEXP /0.0,0.0/,
     &     NCEXP /1/, NPOLEXP /1/
      DATA FMTEXP(1:4) /'FREE'/, EXPDATAVL /.FALSE./
      DATA WGAUSS /0.0/, WLOR /9999.0, 9999.0/, WLORAES /-1.0,-1.0/
      DATA EUNIT / 'eV'/,  RYOVEV / 13.605826 /
      DATA EMIN / 9999./, EMAX / 9999./, EREFER / 9999. /, DE / 9999. /
      DATA MERGE / 9999 /, DY / 9999. /
      DATA NS / 2 /, NVAL / 9999 /
      DATA WLORVBXPS /0.0,0.0/
      ALLOCATABLE  N,E,Y
      ALLOCATABLE  NW
      ALLOCATABLE  WXRS, EXRS
      ALLOCATABLE  IXRS, BXRS
      ALLOCATABLE  WGTGAUTAB, WLORTAB
      ALLOCATABLE  EEXP,IEXP

      ALLOCATABLE  NCXRAY,LCXRAY
      ALLOCATABLE  ITXRSGRP
      ALLOCATABLE  NLQ
      ALLOCATABLE  NLT
      ALLOCATABLE  IQOFT, NQT, LTXTT 
      ALLOCATABLE  CONC
      ALLOCATABLE  NMIN,NMAX
      ALLOCATABLE  WMIN,WMAX

      ALLOCATABLE  AD
      ALLOCATABLE  AT
      ALLOCATABLE  ECORE
      ALLOCATABLE  TD,TT,TE

      ALLOCATABLE  KAPCOR, MM05COR
      ALLOCATABLE  NKPCOR,  IKMCOR
      ALLOCATABLE  IN, KTYPCOR
      ALLOCATABLE  TETQPH,PHIQPH
      ALLOCATABLE  TETEPH,PHIEPH
      ALLOCATABLE  TETKEL,PHIKEL

      ALLOCATABLE TXTT
             
      KJDOS       = .FALSE.
      KTABLE      = .FALSE.
      KNOHEADER   = .FALSE.
      NOCORESPLIT = .FALSE.
      NOTABEXT    = .FALSE.
      MTV = .FALSE.
      MLD = .FALSE.
      CUTOFF = 300.0
      IQRES=.FALSE.
c ----------------------------------------------------------------------
      IFILEXP = 2

      OPEN( IFTABLE, STATUS = 'SCRATCH' )
      OPEN( IFCONVT, STATUS = 'SCRATCH' ) 

      LDATASET = 0

C ======================================================================
C READIN INPUT FILE
C ======================================================================
      CALL FINDCMD(5,'DATASET',7,CMDUC,CMD00,LCMD,FOUND,TABLE,
     &                LCMDMAX,IFTABLE)
      IF( .NOT. FOUND  ) THEN
         IFIL = 5
         REWIND 5
      ELSE
         IPOS = 1
         CALL GTVALSTR(DATASET,LDATASET,LSM,CMD00,IPOS,LCMD)
         IFIL = 1
         OPEN( 1, FILE=DATASET )
         WRITE(6,*) ' DATASET:    ',DATASET(1:LDATASET)
c ----------------------------------------------------------------------
         CALL FINDCMD(5,'PLOT',4,CMDUC,CMD00,LCMD,
     &                   CMDFOUND,TABLE, LCMDMAX,IFTABLE)
         IF( CMDFOUND ) THEN
            CALL UPDATREAL(R8, 'EMIN', 4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) EMIN = REAL( R8 )
            CALL UPDATREAL(R8, 'EMAX', 4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) EMAX = REAL( R8 )
            CALL UPDATREAL(R8, 'DE',   2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) DE   = REAL( R8 )
            CALL UPDATREAL(R8, 'DY',   2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) DY   = REAL( R8 )
            CALL UPDATEINT(MERGE,'MERGE',5,CMDUC,LCMD,STR,LSM,IFC,UDT)
            CALL FINDKW('MLD',      3,IPOS,CMDUC,LCMD,MLD  )
            CALL UPDATREAL(R8,   'SCALE',5,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN
               SCLFAC = REAL( R8 )
            ELSE
               SCLFAC = 0.0    
            END IF
         END IF
c ----------------------------------------------------------------------
         CALL FINDCMD(5,'BROADEN',7,CMDUC,CMD00,LCMD,
     &                   CMDFOUND,TABLE, LCMDMAX,IFTABLE)
         IF( CMDFOUND ) THEN
            CALL UPDATRARR(R8A,LWLOR,2, 
     &                              'WL',  2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN
                DO I=1,LWLOR
                    IF( I .GT. NR8AMAX ) STOP ' LWLOR > NR8AMAX !!!!!! '
                    WLOR(I) = REAL( R8A(I) )
                END DO
            END IF

            CALL UPDATRARR(R8A,LWLOR,2, 
     &           'WLVBXPS',7,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
                DO I=1,LWLOR
                    IF( I .GT. NR8AMAX ) STOP ' LWLOR > NR8AMAX !!!!!! '
                    WLORVBXPS(I) = REAL( R8A(I) )
                END DO
            END IF
            CALL UPDATRARR(R8A,LWLOR,2, 
     &           'WLAES',5,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
                DO I=1,LWLOR
                    IF( I .GT. NR8AMAX ) STOP ' LWLOR > NR8AMAX !!!!!! '
                    WLORAES(I) = REAL( R8A(I) )
                END DO
            END IF
            
            IF( .NOT. UDT ) 
     &      CALL UPDATRARR(R8A,LWLOR,2, 
     &                              'WLOR',4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
               DO 10 I=1,LWLOR
                  IF( I .GT. NR8AMAX ) STOP ' LWLOR > NR8AMAX !!!!!! '
                  WLOR(I) = REAL( R8A(I) )
   10          CONTINUE
            END IF
            CALL UPDATREAL(R8, 'WG',  2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( .NOT. UDT ) 
     &      CALL UPDATREAL(R8, 'WGAU',4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) WGAUSS = REAL( R8 )
            CALL UPDATEINT(NVAL,'NVAL',4,CMDUC,LCMD,STR,LSM,IFC,UDT)

            CALL UPDATREAL(R8, 'TEMP',4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            TEMP=0.0
            IF (UDT) TEMP=REAL(R8)
         END IF
c ----------------------------------------------------------------------
         CALL FINDCMD(5,'EXPERIMENT',10,CMDUC,CMD00,LCMD,
     &                EXPDATAVL,TABLE, LCMDMAX,IFTABLE)
         IF( EXPDATAVL ) THEN

            CALL UPDATESTR('FILE',4,CMDUC,CMD00,LCMD,
     &                     EXPDATSET,LEXPDATSET,LSM,UDT)    
     
            IF( LEXPDATSET .GT. 0 ) THEN
c              CALL CNVTLC( EXPDATSET(1:LEXPDATSET),1,LEXPDATSET )
               OPEN( IFILEXP, FILE=EXPDATSET(1:LEXPDATSET) )
               WRITE(6,*) ' EXPERIMENT: ',EXPDATSET(1:LEXPDATSET)

            ELSE
               WRITE(*,*) ' CMDUC: ',CMDUC(1:LCMD)
               STOP ' in <<MAIN>>:  FILE = >EXPDATSET<  expected '
            END IF
            CALL UPDATRARR
     &                (R8A ,LES ,2,'ES', 2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
               DO 20 I=1,LES
                  IF( I .GT. NR8AMAX ) STOP ' LES > NR8AMAX !!!!!! '
                  ESEXP(I) = REAL( R8A(I) )
   20          CONTINUE
            END IF

            CALL UPDATRARR
     &                (R8A,LBLS,2,'BLS',3,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
               DO 30 I=1,LBLS
                  IF( I .GT. NR8AMAX ) STOP ' LBLS > NR8AMAX !!!!!! '
                  BLSEXP(I) = REAL( R8A(I) )
   30          CONTINUE
            END IF
            CALL UPDATRARR
     &                (R8A,LSCL,2,'SCL',3,CMDUC,LCMD,STR,LSM,IFC,UDT)
            IF( UDT ) THEN 
               DO 40 I=1,LSCL
                  IF( I .GT. NR8AMAX ) STOP ' LSCL > NR8AMAX !!!!!! '
                  SCLEXP(I) = REAL( R8A(I) )
   40          CONTINUE
            END IF
            CALL UPDATEINT(NCEXP,    'NC', 2,CMDUC,LCMD,STR,LSM,IFC,UDT)
            CALL UPDATEINT(NPOLEXP,'NPOL', 4,CMDUC,LCMD,STR,LSM,IFC,UDT)
            CALL UPDATESTR('FMT',3,CMDUC,CMD00,LCMD,
     &                                           FMTEXP,LFMTEXP,LSM,UDT)
            
            CALL RDEXPDATA( EEXP, IFILEXP, IEXP,NEMAX, 
     &                      NCEXPMAX,NPOLEXPMAX )
         END IF
c ----------------------------------------------------------------------
         CALL FINDCMD(5,'OUTPUT',6,CMDUC,CMD00,LCMD,
     &                CMDFOUND,TABLE, LCMDMAX,IFTABLE)
         IF( CMDFOUND ) THEN
            CALL FINDKW('JDOS',        4,IPOS,CMDUC,LCMD,KJDOS)
            CALL FINDKW('TABLE',       5,IPOS,CMDUC,LCMD,KTABLE)
            CALL FINDKW('NOHEADER',    8,IPOS,CMDUC,LCMD,KNOHEADER)
            CALL FINDKW('NOCORESPLIT',11,IPOS,CMDUC,LCMD,NOCORESPLIT)
            CALL FINDKW('NOTABEXT'   , 8,IPOS,CMDUC,LCMD,TABEXT)
            CALL FINDKW('MTV'        , 3,IPOS,CMDUC,LCMD,MTV)
            CALL FINDKW('IQRES'      , 5,IPOS,CMDUC,LCMD,IQRES)
            CALL UPDATREAL(R8, 'CUTOFF',6,CMDUC,LCMD,STR,LSM,IFC,UDT)
            CUTOFF = 10000.0
            IF( UDT ) CUTOFF = REAL( R8 )
        END IF
c ----------------------------------------------------------------------
      END IF
C ======================================================================

C ======================================================================
C Allocation of arrays
C ======================================================================

      REWIND(IFIL)
      READ(IFIL,'(10X,A)') KW
      CALL RDHEAD1(IFIL,NQ,NT,NE,NL)

      NTMAX=NT
      NQMAX=NQ
      NGEOMAX   = NTMAX
      NVMAX= (NTMAX+1)*(NCMAX+1)*3
      NLMAX=NL
C     NEMAX SET TO 1000+NE to allow enoght points for broadening
      NEMAX=1500+NE
      NCSTMAX   = NCMAX 

      ALLOCATE(N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),E(NEMAX),Y(NEMAX))
      ALLOCATE(NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX))
      ALLOCATE(WXRS(NEXRSMAX,0:NTMAX,0:NCMAX,2), EXRS(NEXRSMAX))
      ALLOCATE(IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX))
      ALLOCATE(WGTGAUTAB(0:NEMAX), WLORTAB(1:NEMAX))
      ALLOCATE(EEXP(NEMAX,NCEXPMAX),IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX))

      ALLOCATE(NCXRAY(NTMAX),LCXRAY(NTMAX))
      ALLOCATE(ITXRSGRP(NTMAX))
      ALLOCATE(NLQ(NQMAX))
      ALLOCATE(NLT(NTMAX))
      ALLOCATE(IQOFT(NQMAX,NTMAX), NQT(NTMAX), LTXTT(NTMAX) )
      ALLOCATE(CONC(NTMAX))
      ALLOCATE(NMIN(0:NTMAX), NMAX(0:NTMAX))
      ALLOCATE(WMIN(0:NTMAX), WMAX(0:NTMAX))

      ALLOCATE(AD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX))
      ALLOCATE(AT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX))
      ALLOCATE(ECORE(NTMAX,NCSTMAX))
      ALLOCATE(TD(NEMAX),TT(NEMAX),TE(NEMAX))

      ALLOCATE(KAPCOR(NCSTMAX), MM05COR(NCSTMAX))
      ALLOCATE(NKPCOR(NCSTMAX),  IKMCOR(NCSTMAX,2))
      ALLOCATE(IN(NEMAX), KTYPCOR(NCSTMAX))
      ALLOCATE(TETQPH(NGEOMAX),PHIQPH(NGEOMAX))
      ALLOCATE(TETEPH(NGEOMAX),PHIEPH(NGEOMAX))
      ALLOCATE(TETKEL(NGEOMAX),PHIKEL(NGEOMAX))
      
      ALLOCATE (TXTT(NTMAX))

      REWIND(IFIL)

      READ(IFIL,'(10X,A)') KW

      CALL RDHEAD( IFIL, N,NW, NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,
     &             NLQ, NLT, NMIN, NMAX,
     &             IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &             HEADER,LHEADER, SYSTEM,LSYSTEM, 
     &             NQ,NT,NE,IREL,EF )

      WRITE(*,*) 'keyword read:          >>',KW,'<<'
      WRITE(*,*) ' '
     
C ======================================================================
      IF( KW(1:10) .EQ. 'DOS       ' ) THEN
         CALL PLOTDOS(    IFIL, N,NW, DATASET,LDATASET,LSTRMAX,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    NLQ, NLT, NMIN, NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    RYOVEV,EREFER,EUNIT,EMIN,EMAX,DE,DY,
     &                    NQ,NT,NE,IREL,EF,
     &                    KJDOS,KTABLE,KNOHEADER, AD )
         STOP 'plotdos'
      END IF

C ======================================================================
      IF( KW(1:10) .EQ. 'DOS-KAPPA ' ) THEN
         CALL PLOTKAPDOS( IFIL,N,NW,DATASET,LDATASET,NTMAX,NEMAX,
     &                    NCMAX,NSPMAX,NLT,NMAX,TXTT,LTXTT,HEADER,
     &                    LHEADER, SYSTEM,LSYSTEM,E,RYOVEV,EREFER,
     &                    EUNIT,EMIN,EMAX,NT,NE,EF,KNOHEADER )
      STOP 'plotkapdos'
      END IF

C ======================================================================
      IF( KW(1:10) .EQ. 'POLAR     ' ) THEN
         CALL PLOTPOLAR( IFIL,N,NW,DATASET,LDATASET,NTMAX,NEMAX,
     &                    NCMAX,NSPMAX,NLT,NMAX,TXTT,LTXTT,HEADER,
     &                    LHEADER, SYSTEM,LSYSTEM,E,RYOVEV,EREFER,
     &                    EUNIT,EMIN,EMAX,NT,NE,EF,KNOHEADER )
         STOP 'plotpolar'
      END IF

C ======================================================================
      IF( KW(1:10) .EQ. 'DISPERSION' ) THEN
         CALL PLOTBAND( IFIL, HEADER,LHEADER, SYSTEM,LSYSTEM,
     &                    INFO,LINFO,DATASET,LDATASET,
     &                    IREL,EF,KNOHEADER,
     &                    EMIN,EMAX, RYOVEV, EREFER,EUNIT)
         STOP 'plotband'
      END IF
C ======================================================================
      IF( KW(1:3) .EQ. 'XPS' .OR. KW(1:3) .EQ. 'BIS' ) THEN
        CALL PLOTXPS(     IFIL,N,NW,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    KW(1:3),
     &                    NLQ, NLT, NMIN,NMAX, WMIN,WMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    NQ,NT,NE,IREL,EF,
     &                    EXRS,IXRS,BXRS,WXRS,NVMAX,NEXRSMAX,WGTGAUTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX )
         STOP 'plotxps'
      END IF
C ======================================================================
      IF( (KW(1:3) .EQ. 'XAS') .OR. (KW(1:3) .EQ. 'XES') ) THEN
        CALL PLOTNRXAS(   IFIL,N,NW,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    KW(1:3),
     &                    NLQ, NLT, NMIN,NMAX, WMIN,WMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    NT,NS,NE,
     &                    EXRS,IXRS,BXRS,WXRS,NVMAX,NEXRSMAX,WGTGAUTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX,
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE,NCSTMAX )
         STOP 'plotnrxas'
      END IF
C ======================================================================
      IF( (KW(1:4) .EQ. 'RXAS')  .OR. (KW(1:3) .EQ. 'XMO')  .OR.
     &    (KW(1:3) .EQ. 'XRS') ) THEN
         CALL PLOTRXAS(   IFIL,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    KW,MLD,DATASET,LDATASET,
     &                    NLQ, NLT, NMIN,NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,RYOVEV,EUNIT,EREFER,
     &                    EMIN,EMAX,DE,MERGE,WGAUSS,WLOR,
     &                    NEEXP,NCEXP,NPOLEXP,EXPDATAVL,
     &                    IXRS,BXRS,NVMAX,WGTGAUTAB,WLORTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX,
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE,
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,
     &                    AD,AT,N,NW, TD,TT,TE, KTABLE, SCLFAC,
     &                    NOCORESPLIT,NOTABEXT,KNOHEADER )
         STOP 'PLOTRXAS'
      END IF                                                           
C ======================================================================
      IF( KW(1:4) .EQ. 'RXES' ) THEN
         CALL PLOTRXES(   IFIL,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,NLMAX,
     &                    KW(2:4),
     &                    NLQ, NLT, NMIN,NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,
     &                    IXRS,BXRS,NVMAX,WGTGAUTAB,WLORTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX,
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE,
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,
     &                    AD,AT,N,NW, TD,TT,TE, KTABLE, SCLFAC,
     &                    NOCORESPLIT, NOTABEXT )
         STOP 'PLOTRXES'
      END IF
C ======================================================================
      IF( KW(1:5) .EQ. 'CLXPS' ) THEN
         CALL PLOTCLXPS( IFIL,DATASET,LDATASET,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    KW(1:5),
     &                    NLQ, NLT, NMIN,NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,
     &                    IXRS,BXRS,NVMAX,WGTGAUTAB,WLORTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX,
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE,
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,
     &                    AD,AT,N,NW, TD,TT,TE,MLD,
     &                    NGEOMAX,TETQPH,PHIQPH,TETEPH,PHIEPH,
     &                    TETKEL,PHIKEL  )
         STOP 'PLOT-CL-XPS'
      END IF
C ======================================================================
      IF( KW(1:10) .EQ. 'SPR-VB-XPS' .OR.KW(1:10) .EQ. 'SPR-VB-PES' .OR.
     &     KW(1:6) .EQ. 'VB-PES')THEN
          CALL PLOTSPRVBXPS(IFIL,DATASET,LDATASET,
     &         NQMAX,NTMAX,NEMAX,NCMAX,3,NLMAX,
     &         KW(1:3),
     &         NLQ,NLT,NMIN,NMAX,WMIN,WMAX,
     &         IQOFT,NQT,CONC,TXTT,LTXTT,INFO,LINFO,
     &         HEADER,LHEADER,SYSTEM,LSYSTEM,E,Y,
     &         NQ,NT,NS,NE,IREL,EF,
     &         KTABLE,NVMAX,NEXRSMAX,WGTGAUTAB,
     &         WLORTAB,WLORVBXPS,NCEXPMAX,NPOLEXPMAX)
         STOP 'SPR-VB-XPS'
      END IF
C ======================================================================
      IF( KW(1:10) .EQ. 'SPR-VB-ARP'.OR.  KW(1:10) .EQ. 'VB-AR-PES')
     &     THEN
          CALL PLOTSPRVBARP(IFIL,DATASET,LDATASET,
     &         NQMAX,NTMAX,NEMAX,NCMAX,3,NLMAX,
     &         KW(1:3),
     &         NLQ,NLT,NMIN,NMAX,WMIN,WMAX,
     &         IQOFT,NQT,CONC,TXTT,LTXTT,INFO,LINFO,
     &         HEADER,LHEADER,SYSTEM,LSYSTEM,E,Y,
     &         NQ,NT,NS,NE,IREL,EF,
     &         MTV,
     &         KTABLE,NVMAX,NEXRSMAX,WGTGAUTAB,
     &         WLORTAB,WLORVBXPS,NCEXPMAX,NPOLEXPMAX,ICOLOR)
         STOP 'SPR-VB-ARP'
      END IF
C ======================================================================
      IF( KW(1:6) .EQ. 'SP-AES' ) THEN
         CALL PLOTSPRCVVAES(IFIL,DATASET,LDATASET,N,NW,
     &         NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &         KW(1:3),
     &         NLQ, NLT, NMIN,NMAX, WMIN,WMAX,
     &         IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &         HEADER,LHEADER, SYSTEM,LSYSTEM, E,
     &         NQ,NT,NS,NE,IREL,EF,
     &         EXRS,IXRS,BXRS,WXRS,
     &         NVMAX,NEXRSMAX,
     &         WGTGAUTAB, WLORTAB,WLORAES,
     &         EEXP, IEXP, NCEXPMAX,NPOLEXPMAX,KW)
         STOP 'SPR-CVV-AES'
      END IF
C ======================================================================
      IF( KW(1:5) .EQ. 'NRAES' ) THEN
         CALL PLOTNRAESAPS(1,IFIL,DATASET,LDATASET,
     &         HEADER,LHEADER, SYSTEM,LSYSTEM,
     &         WLORAES,KW,NEMAX,NLMAX)
         STOP 'NR-CVV-AES'
      END IF
C ======================================================================
      IF( KW(1:5) .EQ. 'NRAPS' ) THEN
         CALL PLOTNRAESAPS(2,IFIL,DATASET,LDATASET,
     &         HEADER,LHEADER, SYSTEM,LSYSTEM,
     &         WLORAES,KW,NEMAX,NLMAX)
         STOP 'NR-APS'
      END IF
C ======================================================================
      IF( KW(1:7) .EQ. 'COMPTON' ) THEN
          CALL PLOTCOMPTON(IFIL,
     &         NQMAX,NTMAX,
     &         NLQ, NLT, NMIN,NMAX,
     &         IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &         HEADER,LHEADER, SYSTEM,LSYSTEM, 
     &         NQ,NT,NE,IREL,EF,dataset,ldataset)
          STOP 'COMPTON   '
      END IF
C ======================================================================
      IF( KW(1:4) .EQ. 'BSF ' ) THEN
                          
         READ(IFIL,'(/,10X,A,/,10X,A,2(/,10X,I10) )') DATASET,KW,NY,NX
         LDATASET = LNGSTRING(DATASET,80)
         IF( KW .EQ. 'CONST-E   ' ) THEN
            MODE = 2
         ELSE
            MODE = 1
         END IF

         EFRYD = EREFER / RYOVEV
         CALL PLOTBSF(    MODE,CUTOFF,MTV,EFRYD,
     &                    IFIL,KW,NX,NY,ERYD, DATASET,LDATASET,LSTRMAX,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    NLQ, NLT, NMIN, NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    RYOVEV,EREFER,EUNIT,EMIN,EMAX,DE,DY,
     &                    NQ,NT,NE,IREL,EF,IQRES )
         STOP 'BSF'
      END IF
C ======================================================================
      IF( KW(1:6) .EQ. 'BSF-SP' ) THEN
                          
         READ(IFIL,'(/,10X,A,/,10X,A,2(/,10X,I10) )') DATASET,KW,NY,NX
         LDATASET = LNGSTRING(DATASET,80)
         IF( KW .EQ. 'CONST-E   ' ) THEN
            MODE = 2
         ELSE
            MODE = 1
         END IF

         EFRYD = EREFER / RYOVEV
         CALL PLOTBSF_SPOL(    MODE,CUTOFF,MTV,EFRYD,
     &                    IFIL,KW,NX,NY,ERYD, DATASET,LDATASET,LSTRMAX,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    NLQ, NLT, NMIN, NMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    RYOVEV,EREFER,EUNIT,EMIN,EMAX,DE,DY,
     &                    NQ,NT,NE,IREL,EF,IQRES)
         STOP 'BSF'
      END IF
C ======================================================================
      IF( KW(1:10) .EQ. 'ARPES     '.OR.  KW(1:10) .EQ. 'AIPES    '
     &     .OR. KW(1:10) .EQ. 'SPLEED    '.OR. KW(1:10) .EQ.
     &     'BAND      ')THEN
         CALL PLOTSPEC(IFIL,DATASET,LDATASET,WGTGAUTAB,
     &        WLORTAB,WLORVBXPS,NE,KW,TEMP,WGAUSS)
         STOP 'PLOT SPEC'
      END IF
C ======================================================================
      WRITE(*,*) '>>>>>>>>>>>>>>>> KEYWORD  >>',KW,'<<   not found'
      STOP
      END


C                     
      SUBROUTINE ENSUREBSLASH(BSLASH,BSLASHSTRING)
C
C *  when calling ENSUREBSLASH BSLASHSTRING should be set to '\\'.      
C *  for any compiler / machine this should be converted to      
C *  BSLASH have the escape character  '\' at the end
C * (e.g. required for ifort vs. g77)
C
C     
      CHARACTER*1 BSLASH,BSLASHSTRING
C     
      BSLASH = BSLASHSTRING(1:1)
C     
      END
      
      




