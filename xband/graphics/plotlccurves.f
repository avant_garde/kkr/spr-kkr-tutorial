      SUBROUTINE PLOTLCCURVES( EMIN,EMAX,NMIN,NMAX,
     &                         NEMAX, W1,C1, W2,C2, C,E, NE )
c **********************************************************************
c *                                                                    *
c *   plot a linear combination of curves                              *
c *                                                                    *
c **********************************************************************
      REAL NMIN,NMAX 
      REAL C1(NEMAX), C2(NEMAX), C(NEMAX), E(NEMAX) 

      DO 10 IE = 1, NE
         C(IE) = W1 * C1(IE) + W2 * C2(IE)
   10 CONTINUE        

C# CURVE( 1, NE, E, C, 'LINEAR' )
      END
