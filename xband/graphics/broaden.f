      SUBROUTINE BROADEN( N,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB,ILOR,NVAL, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, IT1,IT2, IC1,NCT, ISP1,ISP2 )
c **********************************************************************
c *                                                                    *
c *    broaden the intensities  N                                      *
c *                                                                    *
c **********************************************************************
      INTEGER NCT(NTMAX)
      REAL    N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL    IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX)
      REAL    WGTGAUTAB(0:NEMAX), E(NEMAX), WLORTAB(NEMAX) 
c ----------------------------------------------------------------------
      COMMON /BRDPAR/ WGAUSS,WLOR(2)
c ----------------------------------------------------------------------

      SAVE ICALL 

      DATA ICALL / 0 /
      NCT0 = 0
      DO IT=MAX(1,IT1),IT2
         NCT0 = MAX( NCT(IT), NCT0 )
      END DO
                                  
      IF( ((WLOR(1)+WLOR(2)) .LT. 0.001) .AND.
     &               (WGAUSS .LT. 0.001) .AND. NVAL.EQ.0 )    RETURN 
 
      I = 0
      DO 55 IS=ISP1,ISP2
         DO 55 IT=IT1,IT2
            IF( IT .EQ. 0 ) THEN
               IC2=NCT0
            ELSE
               IC2=NCT(IT)
            END IF
            DO 55 IC=IC1,IC2
               I = I + 1
               DO 55 IE=1,NE
                  IXRS(I,IE) = N(IE,IT,IC,IS)
   55 CONTINUE
      NV = I
  
      IF( ((WLOR(1)+WLOR(2)) .GT. 0.001) .OR. (NVAL .NE. 0) ) THEN
         
         IF( ICALL .EQ. 0 ) CALL INILORBRD(NVAL,E,WLORTAB,NE)
         
         CALL VECLORBRD(E, IXRS, BXRS, NV, NVMAX, NE,NEMAX, 
     &                 WLOR(ILOR), WLORTAB )

      END IF

      IF( WGAUSS .GT. 0.001 ) THEN
         IF( ICALL .EQ. 0 ) 
     &   CALL INIGAUBRD(WGAUSS,WGTGAUTAB,NEMAX,DXG,IGMAX)

         CALL VECGAUBRD( E, IXRS,BXRS,NV,NVMAX,NE,NEMAX,
     &                   WGTGAUTAB, DXG, IGMAX )

      END IF

      I = 0
      DO 57 IS=ISP1,ISP2
         DO 57 IT=IT1,IT2
            IF( IT .EQ. 0 ) THEN
               IC2=NCT0
            ELSE
               IC2=NCT(IT)
            END IF
            DO 57 IC=IC1,IC2
               I = I + 1
               DO 57 IE=1,NE
                  N(IE,IT,IC,IS) = IXRS(I,IE) 
   57 CONTINUE
      ICALL = 1

      END
