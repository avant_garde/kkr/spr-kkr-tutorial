      SUBROUTINE RDRXAS( SPEC,IFIL, NE, NPOL,E,RD,RT, MLD,RYOVEV,EUNIT,
     &                   ECORE,NCST,KAPCOR,MM05COR,NKPCOR,IKMCOR,
     &                   NCXRAY, LCXRAY, ITXRSGRP, NTXRSGRP,CHIXAS,VUC,
     &                   KUNIT,NEMAX,NCSTMAX,NSPMAX,NTMAX)
C **********************************************************************
C *                                                                    *
C *      read unbroadened theoretical relat. XAS - spectra             *
C *                                                                    *
C *                                                           14/05/94 *
C ********************************************************************** 
 
      IMPLICIT REAL   (A-H,O-Z)  

      COMPLEX    ERYD, ABSRTA, ABSRTB,ABSRATE
      REAL       XNORM(2),DUM,VUC
      CHARACTER  CDUM*1, EUNIT*2, FMT710*80, FMT740*80, SPEC*3
      LOGICAL    MLD,CHIXAS      
      DIMENSION E(NEMAX),RD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &                   RT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX)

      DIMENSION ECORE(NTMAX,NCSTMAX),
     &          KAPCOR(NCSTMAX),MM05COR(NCSTMAX),
     &          NKPCOR(NCSTMAX),IKMCOR(NCSTMAX,2),
     &          NCXRAY(NTMAX), LCXRAY(NTMAX), ITXRSGRP(NTMAX)
c ----------------------------------------------------------------------
 
      READ(IFIL,9020) NTXRSGRP
      READ(IFIL,9020) IDIPOL
      READ(IFIL,9020) NPOL

      IF( MLD .AND. NPOL.NE.3 ) THEN 
         WRITE(*,*) 'WARNING ***********************'
         WRITE(*,*) '  MLD requested but   NPOL=',NPOL
         WRITE(*,*) '  MLD set to .FALSE. '
         WRITE(*,*) '  MCD treated instead '
         WRITE(*,*) ' '
         MLD = .FALSE.
      END IF

      IF( NTXRSGRP .EQ. -1 ) THEN 
         NTXRSGRP = 1
         CHIXAS = .TRUE.
         WRITE(*,*) ' '
         WRITE(*,*) ' ============================'
         WRITE(*,*) '    NTXRSGRP was set to -1 '
         WRITE(*,*) '    CHIXAS set to .TRUE. '
         WRITE(*,*) ' ============================'
         WRITE(*,*) ' '
      ELSE
         CHIXAS = .FALSE.
      END IF

C ======================================================================
      DO ITXG=1,NTXRSGRP
         READ(IFIL,9050) KW 
         READ(IFIL,9020) ITXRSGRP(ITXG)
         READ(IFIL,9020) NCXRAY(ITXG)
         READ(IFIL,9020) LCXRAY(ITXG)

         IF( ITXG .EQ. 1 ) THEN
            WRITE(6,102) IDIPOL, NCXRAY(ITXG), LCXRAY(ITXG), NPOL
         ELSE
            ITXGP = ITXG-1
            IF( NCXRAY(ITXG) .NE. NCXRAY(ITXGP) .OR.
     &          LCXRAY(ITXG) .NE. LCXRAY(ITXGP) ) THEN
                WRITE(6,*) ' NC:  ',NCXRAY(ITXG),NCXRAY(ITXGP)
                WRITE(6,*) ' LC:  ',LCXRAY(ITXG),LCXRAY(ITXGP)
                WRITE(6,*) ' ITXG:',       ITXG ,       ITXGP 
                STOP ' data inconsistent in <RDRXAS> '
            END IF 
         END IF

         READ(IFIL,182,ERR=999) NCST, (NKPCOR(ICST),ICST=1,NCST)
         READ(IFIL,'(A1)')     (CDUM,I=1,2)
         WRITE(6,981) NCST, (NKPCOR(ICST),ICST=1,NCST)
         WRITE(6,982)

c ----------------------------------------------------------------------
         DO ICST=1,NCST 
                         
            READ(IFIL,184) JCST,NCXRAY(ITXG),LCXRAY(ITXG),KAPCOR(ICST),
     &                  MM05COR(ICST), IKMCOR(ICST,1),XNORM(1), 
     &                  ECORE(ITXG,ICST)
            MM05COR(ICST) =  (MM05COR(ICST)-1)/2
            WRITE(6,984) ICST,NCXRAY(ITXG),LCXRAY(ITXG),KAPCOR(ICST),
     &            (2*MM05COR(ICST)+1), IKMCOR(ICST,1),XNORM(1), 
     &             ECORE(ITXG,ICST),
     &             ECORE(ITXG,ICST)*RYOVEV
            IF( NKPCOR(ICST) .EQ. 2 ) THEN 
               READ(IFIL,986)          IKMCOR(ICST,2),XNORM(2)
               WRITE(6,  986)          IKMCOR(ICST,2),XNORM(2)
            END IF   
          
            IF( EUNIT .EQ. 'eV' ) THEN
               ECORE(ITXG,ICST) = ECORE(ITXG,ICST) * RYOVEV
            END IF
         END DO
c ----------------------------------------------------------------------

         READ(IFIL,'(/,20X,I4,/,19X,2I5)') NE0,IFMT,KUNIT

            FMT710='(      2X,2F 7.4,4X,F8.4, 12X,2E14.6 )'
            FMT740='(I2,I2,1X,2E11.4,1X,2E11.4,2X,2E11.4 )'
         IF( IFMT .GE. 1 ) THEN
            FMT710='(      2X,2F11.8,4X,F8.4, 12X,2E14.6 )'
            FMT740='(I2,I2,1X,2E15.8,1X,2E15.8,2X,2E15.8 )'
         END IF
         IF( IFMT .GE. 2 ) THEN
            READ (IFIL,'(10X,F15.8)') VUC
         ELSE
            VUC=0.0
         END IF        
         IF( IFMT .NE. 0 )  READ(IFIL,*)
c ----------------------------------------------------------------------
         DO IE = 1,NE0
            READ(IFIL,FMT=FMT710,ERR=999,END=997) ERYD

            IF( SPEC .NE. 'XMO' ) THEN 
               DO ICST = 1,NCST
                  DO IPOL = 1,NPOL
                     READ(IFIL,FMT=FMT740,ERR=999) 
     &                    I1,I2, ABSRTA,ABSRTB,ABSRATE
           
                     RD(IE,ITXG,ICST,IPOL) = REAL( ABSRTA  )
                     RT(IE,ITXG,ICST,IPOL) = REAL( ABSRATE ) 
                  END DO
               END DO
            ELSE
C ----------------------- store REAL and IMAG part of SIGMA in RD and RT
               DO ICST = 1,NCST
                  DO IPOL = 1,NPOL
                     READ(IFIL,FMT=FMT740,ERR=999) 
     &                    I1,I2,RD(IE,ITXG,ICST,IPOL),
     &                    RT(IE,ITXG,ICST,IPOL)
                  END DO
               END DO
            END IF

            IF( CHIXAS ) THEN
               DO ICST = 1,NCST
                  DO IPOL = 1,NPOL
                     READ(IFIL,FMT=99014,ERR=999) 
     &                    I1,I2, (RD(IE,ICHI,ICST,IPOL),DUM,ICHI=2,5)
                     DO ICHI=2,5
                        RT(IE,ICHI,ICST,IPOL) = 0D0
                     END DO
                  END DO
               END DO
            END IF
99014       FORMAT (2I2,2(1X,2E15.8),/,4X,2(1X,2E15.8))

            IF( MLD ) THEN 
                DO ICST = 1,NCST
C                  DO IPOL = 1,NPOL

                     RD(IE,ITXG,ICST,1) = 0.5*(  RD(IE,ITXG,ICST,1)
     &                                         + RD(IE,ITXG,ICST,2) )
                     RD(IE,ITXG,ICST,2) =        RD(IE,ITXG,ICST,3)
            
              
                     RT(IE,ITXG,ICST,1) = 0.5*(  RD(IE,ITXG,ICST,1)
     &                                         + RD(IE,ITXG,ICST,2) )
                     RT(IE,ITXG,ICST,2) =        RD(IE,ITXG,ICST,3)
            
C                  END DO
               END DO
            END IF

            E(IE) = REAL( ERYD )
            READ(IFIL,750,END=997) CDUM
            NE = IE
         END DO
c ----------------------------------------------------------------------
  997    CONTINUE
         IF( NE .NE. NE0 ) THEN
            WRITE(6,*) ' only ', NE,' E-values tabulated instead of',NE0
            IF( NTXRSGRP .NE. 1 ) STOP 'NTXRSGRP > 1 may cause troble !'
         END IF

      END DO
c     DO ITXG=1,NTXRSGRP
C ======================================================================
 
      IF( EUNIT .EQ. 'eV' ) THEN
         DO IE=1,NE
            E(IE) = E(IE) * RYOVEV            
         END DO
      END IF

      WRITE(6,970) NE

      IF( SPEC .NE. 'XMO' ) NPOL = 2

  970 FORMAT(//,' X-RAY transition-rate read in for ',
     &          I4,' energy-points',/
     &          ' <RDRXAS> passed ',//)
  750 FORMAT(A1)
  710 FORMAT(    2X, 2F7.4,4X,F8.4,12X, 2E14.6 )
  740 FORMAT(I2,I2,1X,2E11.4,1X,2E11.4,2X,2E11.4 )
  102 FORMAT(1H1,//,
     & 4X,' IDIPOL:     ',I5,'  0: rate=A+B',/,
     & 4X, '                    1: rate=A',//,
     & 4X,' CORE quantum-numbers  N=',I2,'  L=',I2,/,
     & 4X,' NPOL:       ',I5,/)
  182 FORMAT(////,8X,I4,/,8X,20I4,///)
  184 FORMAT(5I4,2X,I4,F12.6,F12.4,F12.3)
  981 FORMAT(//,' CORE STATES :',//,
     &          ' NCST:  ',I4,/,' NKPCOR:',20I4)
  982 FORMAT( /,' ICST  N   L  KAP  MUE  IKM     NORM   ',
     &          '     E(Ry)       E(eV)')
  984 FORMAT(5I4,'/2',I4,F12.6,F12.4,F12.3)
  986 FORMAT(22X,     I4,F12.6      )
 9020 FORMAT(10X,I10)
 9050 FORMAT(10X,A)
      RETURN
  999 STOP 'ERROR reading datafile in <RDRXAS>'
      END
