C*==tabmuqin.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      SUBROUTINE TABMUQIN(Z,A,MUNUC,QNUC,INUC,IPRINT)
C   *******************************************************************
C   *                                                                 *
C   *   get nuclear magnetic moment in units of the nuclear magneton  *
C   *                  MUNUC = mu/mu_N                                *
C   *   get nuclear quadrupole moment QNUC in barn                    *
C   *   get nuclear spin INUC                                         *
C   *   taken from Lechner, Physikalisch-chemische Daten D'Ans/Lax-   *
C   *   Taschenbuch fuer Chemiker und Physiker Bd.1, 4.Aufl.),        *
C   *   Springer, Berlin, 1992.                                       *
C   *   For input atomic number Z, the isotope listed has mass        *
C   *   number A and is the one with the highest natural abundance    *
C   *   (with the longest lifetime, resp.)                            *
C   *   that has a non-vanishing magnetic moment.                     *
C   *                                                  HF 05/10/95    *
C   *******************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER A,IPRINT,Z
      REAL*8 INUC,MUNUC,QNUC
C
C Local variables
C
      INTEGER ATAB(0:110),I
      REAL*8 ITAB(0:110),MUTAB(0:110),QTAB(0:110)
      CHARACTER*2 TABCHSYM
C
C*** End of declarations rewritten by SPAG
C
      DATA ATAB/0,1,3,7,9,11,13,14,17,19,21,23,25,27,29,31,33,35,39,39,
     &     43,45,47,51,53,55,57,59,61,63,67,69,73,75,77,81,83,85,87,89,
     &     91,93,97,99,101,103,105,109,111,115,119,121,125,127,129,133,
     &     137,139,0,141,143,147,147,153,157,159,163,165,167,169,173,
     &     175,179,181,183,187,189,193,195,197,199,205,207,209,0,0,0,0,
     &     0,227,229,231,235,0,239,241,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA ITAB/0.0,0.5,0.5,1.5,1.5,1.5,0.5,1.0,2.5,0.5,1.5,1.5,2.5,2.5,
     &     0.5,0.5,1.5,1.5,3.5,1.5,3.5,3.5,2.5,3.5,1.5,2.5,0.5,3.5,1.5,
     &     1.5,2.5,1.5,4.5,1.5,0.5,1.5,4.5,2.5,0.5,0.5,2.5,4.5,2.5,4.5,
     &     2.5,0.5,2.5,0.5,0.5,4.5,0.5,2.5,0.5,2.5,0.5,3.5,1.5,3.5,0.0,
     &     2.5,3.5,3.5,3.5,2.5,1.5,1.5,2.5,3.5,3.5,0.5,2.5,3.5,4.5,3.5,
     &     0.5,2.5,1.5,1.5,0.5,1.5,0.5,0.5,0.5,4.5,0.0,0.0,0.0,0.0,0.0,
     &     1.5,2.5,1.5,3.5,0.0,0.5,2.5,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0/
      DATA (MUTAB(I),I=1,40)/2.792846, - 2.127624,3.256424, - 1.177800,
     &      2.688637,0.702411,0.403761, - 1.893790,2.628866, - 0.661796,
     &      - 2.217520, - 0.855450,3.641504, - 0.555290,1.131600,
     &      0.643821,0.821874, - 1.300000,0.391466, - 1.317260,4.756483,
     &      - 0.788480,5.151400, - 0.474540,3.453200,0.090623,4.627000,
     &      - 0.750020,2.223300,0.875478,2.016590, - 0.879467,1.439470,
     &      0.535060,2.270560, - 0.970669,1.353030, - 1.092820,
     &      - 0.137415, - 1.303620/
      DATA (MUTAB(I),I=41,80)/6.170500, - 0.933500,5.684700, - 0.718800,
     &      - 0.088400, - 0.642000, - 0.130691, - 0.594886,5.540800,
     &      - 1.047280,3.363400, - 0.888280,2.813270, - 0.777976,
     &      2.582023,0.937365,2.783200,0.000000,4.136000, - 1.065000,
     &      2.580000, - 0.814800,1.533000, - 0.339800,2.014000,0.672600,
     &      4.173000, - 0.566500, - 0.231600, - 0.679890,2.232700,
     &      - 0.640900,2.370000,0.117785,3.219700,0.659933,0.159100,
     &      0.609490,0.148158,0.505885/
      DATA (MUTAB(I),I=81,110)/1.638213,0.582190,4.110600,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,1.100000,0.460000,
     &      2.310000, - 0.350000,0.000000,0.203000,1.610000,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,
     &      0.000000,0.000000/
      DATA (QTAB(I),I=1,40)/0.000000,0.000000, - 0.040000,0.053000,
     &      0.040000,0.000000,0.019300, - 0.026000,0.000000,0.102900,
     &      0.108000,0.220000,0.150000,0.000000,0.000000, - 0.064000,
     &      - 0.082490,0.000000,0.054000,0.230000, - 0.220000,0.290000,
     &      - 0.051500, - 0.028500,0.330000,0.000000,0.420000,0.162000,
     &      - 0.222000,0.150000,0.168000, - 0.190000,0.290000,0.000000,
     &      0.270000,0.260000,0.273000,0.150000,0.000000,0.000000/
      DATA (QTAB(I),I=41,80)/ - 0.280000,0.200000,0.340000,0.440000,
     &      0.000000,0.660000,0.000000,0.000000,0.861000,0.000000,
     &      - 0.330000,0.000000, - 0.789000,0.000000, - 0.003000,
     &      0.340000,0.200000,0.000000, - 0.041000, - 0.560000,0.660000,
     &      - 0.180000,3.920000,1.340000,1.340000,2.510000,2.730000,
     &      2.827000,0.000000,2.800000,5.680000,5.100000,3.440000,
     &      0.000000,2.220000,0.800000,0.700000,0.000000,0.594000,
     &      0.000000/
      DATA (QTAB(I),I=81,110)/0.000000,0.000000, - 0.460000,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,1.700000,4.400000,
     &      0.000000,4.300000,0.000000,0.000000,4.900000,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,
     &      0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,
     &      0.000000,0.000000/
C
      IF ( Z.GE.0 .AND. Z.LE.110 ) THEN
         A = ATAB(Z)
         MUNUC = MUTAB(Z)
         INUC = ITAB(Z)
         QNUC = QTAB(Z)
         IF ( IPRINT.GT.0 ) THEN
            WRITE (6,99001) MUNUC,A,TABCHSYM(Z)
            WRITE (6,99002) INUC,QNUC
         END IF
         IF ( A.NE.0 ) RETURN
         IF ( IPRINT.GT.0 ) WRITE (6,99003)
      END IF
      WRITE (6,*) ' TABMUQIN: warning for atomic number = ',Z
99001 FORMAT (' nuclear magnetic moment mu/mu_N =',F8.5,'  for ',I3,A2)
99002 FORMAT (' nuclear spin I =',F5.1,
     &        ' nuclear quadrupole moment Q = ',F9.6,' [barn]')
99003 FORMAT (' no stable isotope with non-vanishing mu')
      END
C*==tabgamman.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      SUBROUTINE TABGAMMAN(Z,GAMN)
C   ********************************************************************
C   *                                                                  *
C   *     get nuclear gyro-magnetic ratio  GAMN=gamma_n in  [1/s*G]    *
C   *                                                                  *
C   *     GN = gamma_n/2*pi [kHz/G] taken from Carter et al.           *
C   *          Prog. Mater. Sci. Sci. I, 1977, Tab. 9.2a               *
C   *          the isotope listed has mass number  MN  and is          *
C   *          normally that with the highest natural abundance        *
C   *                                                                  *
C   *                                                         03/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 PI
      PARAMETER (PI=3.141592653589793238462643D0)
C
C Dummy arguments
C
      REAL*8 GAMN
      INTEGER Z
C
C Local variables
C
      REAL*8 GN(0:110)
      INTEGER I,MN(0:110)
      CHARACTER*2 TABCHSYM
C
C*** End of declarations rewritten by SPAG
C
      DATA MN/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     &     63,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,105,109,0,0,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,195,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
C
      DATA (GN(I),I=1,40)/0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,1.12850,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000/
      DATA (GN(I),I=41,80)/0.00000,0.00000,0.00000,0.00000,0.13400,
     &      0.19500,0.19808,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.90940,0.00000,0.00000/
      DATA (GN(I),I=81,110)/0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,
     &      0.00000,0.00000,0.00000,0.00000/
C
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         GAMN = GN(Z)*2*PI*1000
         WRITE (6,99001) GN(Z),MN(Z),TABCHSYM(Z)
         IF ( ABS(GAMN).GT.0.00001 ) RETURN
         WRITE (*,*) ' table uncomplete '
      END IF
      WRITE (*,*) ' atomic number = ',Z
      STOP ' in <TABGAMMAN>'
99001 FORMAT (' nuclear gyro-magnetic ratio  g_n/2*pi=',F8.5,
     &        ' [kHz/G]    for ',I3,A2)
      END
C*==tabrwshls.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABRWSHLS(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return the Wigner-Seitz-radiues RWS for atomic number  Z     *
C   *     experimental values taken from H.L.Skrivers's book           *
C   *                                                                  *
C   *                                                         03/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      REAL*8 TABRWSHLS
C
C Local variables
C
      INTEGER I
      REAL*8 TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
C
      DATA (TABLE(I),I=0,40)/0.000,0.000,0.000,0.000,0.000,0.000,0.000,
     &      0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,0.000,
     &      0.000,0.000,4.862,4.122,3.427,3.052,2.818,2.684,2.699,2.662,
     &      2.621,2.602,2.669,0.000,0.000,0.000,0.000,0.000,0.000,0.000,
     &      5.197,4.494,3.761,3.347/
      DATA (TABLE(I),I=41,80)/3.071,2.922,2.840,2.791,2.809,2.873,3.005,
     &      0.000,0.000,0.000,0.000,0.000,0.000,0.000,5.656,4.652,3.920,
     &      3.800,3.818,3.804,3.783,3.768,4.263,3.764,3.720,3.704,3.687,
     &      3.668,3.649,4.052,3.624,3.301,3.069,2.945,2.872,2.825,2.835,
     &      2.897,3.002,0.000/
      DATA (TABLE(I),I=81,110)/0.000,0.000,0.000,0.000,0.000,0.000,
     &      5.900,4.790,3.900,3.756,3.430,3.221,3.140,3.181,3.614,3.641,
     &      3.550,0.000,0.000,0.000,0.000,0.000,3.500,0.000,0.000,0.000,
     &      0.000,0.000,0.000,0.000/
C
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABRWSHLS = TABLE(Z)
         IF ( ABS(TABRWSHLS).GT.0.00001 ) RETURN
         WRITE (*,*) ' table uncomplete '
      END IF
      WRITE (*,*) ' atomic number = ',Z
      STOP ' in <TABRWSHLS>'
      END
C*==tabestmom.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABESTMOM(Z)
C   ********************************************************************
C   *                                                                  *
C   *  return the estimated spin magnetic moment for atomic number  Z  *
C   *                                                                  *
C   *                                                         26/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      REAL*8 TABESTMOM
C
C Local variables
C
      REAL*8 TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
C
      DATA TABLE/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.6,4.0,2.1,1.6,
     &     0.6,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     &     0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0/
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABESTMOM = TABLE(Z)
         RETURN
      END IF
      WRITE (*,*) ' atomic number = ',Z
      STOP ' in <TABESTMOM>'
      END
C*==tabncore.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABNCORE(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return number of core electrons for atomic number  Z         *
C   *                                                                  *
C   *                                                         03/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      INTEGER TABNCORE
C
C Local variables
C
      INTEGER TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
      DATA TABLE/0,0,0,2,2,2,2,2,2,2,2,10,10,10,10,10,10,10,10,18,18,18,
     &     18,18,18,18,18,18,18,18,28,28,28,28,28,28,28,36,36,36,36,36,
     &     36,36,36,36,36,36,46,46,46,46,46,46,46,54,54,54,54,54,54,54,
     &     54,54,54,54,54,54,54,54,54,54,68,68,68,68,68,68,68,68,78,78,
     &     78,78,78,78,78,86,86,86,86,86,86,86,86,86,86,86,86,86,86,86,
     &     86,86,86,86,86,86,86,86,86/
C
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABNCORE = TABLE(Z)
      ELSE
         WRITE (*,*) ' atomic number = ',Z
         STOP ' in <TABNCORE>'
      END IF
      END
C*==tabnval.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABNVAL(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return number of valence electrons for atomic number  Z      *
C   *                                                                  *
C   *                                                         03/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      INTEGER TABNVAL
C
C Local variables
C
      INTEGER TABNCORE
C
C*** End of declarations rewritten by SPAG
C
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABNVAL = Z - TABNCORE(Z)
      ELSE
         WRITE (*,*) ' atomic number = ',Z
         STOP ' in <TABNVAL>'
      END IF
      END
C*==tabnlval.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABNLVAL(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return the minimum number of (l_max+1) needed to represent   *
C   *     the valence states for atomic number  Z                      *
C   *                                                                  *
C   *     f-electrons are assumed to be core electrons                 *
C   *                                                         26/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      INTEGER TABNLVAL
C
C Local variables
C
      INTEGER TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
      DATA TABLE/1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,
     &     3,3,3,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,
     &     3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,
     &     2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3/
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABNLVAL = TABLE(Z)
      ELSE
         WRITE (*,*) ' atomic number = ',Z
         STOP ' in <TABNLVAL>'
      END IF
      END
C*==tabnsemcor.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABNSEMCOR(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return number of semi-core electrons for atomic number  Z    *
C   *                                                                  *
C   *                                                         03/08/93 *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      REAL*8 TABNSEMCOR
C
C Local variables
C
      INTEGER TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
      DATA TABLE/0,0,0,0,0,0,0,0,0,0,0,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     &     6,6,6,10,10,10,10,10,10,10,6,6,6,6,6,6,6,6,6,6,6,10,10,10,10,
     &     10,10,10,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     &     10,10,10,10,10,10,10,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
     &     6,6,6,6/
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABNSEMCOR = TABLE(Z)
      ELSE
         WRITE (*,*) ' atomic number = ',Z
         STOP ' in <TABNSEMCOR>'
      END IF
      END
C*==tabchsym.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION TABCHSYM(Z)
C   ********************************************************************
C   *                                                                  *
C   *     return chemical symbol for atomic number Z                   *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
      CHARACTER*2 TABCHSYM
C
C Local variables
C
      CHARACTER*2 TABLE(0:110)
C
C*** End of declarations rewritten by SPAG
C
      DATA TABLE/'Vc','H ','He','Li','Be','B ','C ','N ','O ','F ','Ne',
     &     'Na','Mg','Al','Si','P ','S ','Cl','Ar','K ','Ca','Sc','Ti',
     &     'V ','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se',
     &     'Br','Kr','Rb','Sr','Y ','Zr','Nb','Mo','Tc','Ru','Rh','Pd',
     &     'Ag','Cd','In','Sn','Sb','Te','I ','Xe','Cs','Ba','La','Ce',
     &     'Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb',
     &     'Lu','Hf','Ta','W ','Re','Os','Ir','Pt','Au','Hg','Tl','Pb',
     &     'Bi','Po','At','Rn','Fr','Ra','Ac','Th','Pa','U ','Np','Pu',
     &     'Am','Cm','Bk','Cf','Es','Fm','Md','No','Lw','Kt','??','??',
     &     '??','??','??','??'/
C
      IF ( Z.GE.0 .AND. Z.LE.104 ) THEN
         TABCHSYM = TABLE(Z)
      ELSE
         WRITE (*,*) ' atomic number = ',Z
         STOP ' in <TABCHSYM>'
      END IF
      END
C*==atomnumb.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
      FUNCTION ATOMNUMB(CHSYM)
C   ********************************************************************
C   *                                                                  *
C   *     return atomic number Z for chemical symbol CHSYM             *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      CHARACTER*2 CHSYM
      INTEGER ATOMNUMB
C
C Local variables
C
      CHARACTER*2 CHSYMP
      CHARACTER*2 TABCHSYM
      INTEGER Z
C
C*** End of declarations rewritten by SPAG
C
C
      CHSYMP = CHSYM
C
      CALL CNVTLC(CHSYMP,2,2)
C
      IF(CHSYM(2:2).EQ.' '.OR.CHSYM(2:2).EQ.'_'
     &                    .OR.CHSYM(2:2).EQ.'.') THEN
         CHSYMP = CHSYM(1:1)//' '
      END IF
C
      ATOMNUMB = 0
      DO Z = 0,104
         IF ( CHSYMP.EQ.TABCHSYM(Z) ) THEN
            ATOMNUMB = Z
            RETURN
         END IF
      END DO
      WRITE (*,*) ' atomic number not found for  CHSYM=',CHSYMP
      STOP
      END
C*==pot_nuc.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
C
      REAL*8 FUNCTION POT_NUC(XR,ANCLR)
C   ********************************************************************
C   *                                                                  *
C   *  this function returns the potential inside the nucleus          *
C   *  assuming a homogenously charged sphere                          *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 ANCLR,XR
C
C Local variables
C
      INTEGER NINT
      REAL*8 RNUC
      REAL*8 RNUCTAB
C
C*** End of declarations rewritten by SPAG
C
C
      RNUC = RNUCTAB(NINT(ANCLR))
      POT_NUC = (ANCLR/RNUC**3)*(XR**2-3*RNUC**2)
      END
C*==rnuctab.f    processed by SPAG 6.05Rc at 14:54 on  4 Jan 2002
C
      REAL*8 FUNCTION RNUCTAB(Z)
C   ********************************************************************
C   *                                                                  *
C   *  this function contains the nuclear radii for many    Z          *
C   *  calculated  from  1.128*a**(1/3)fm   (here saved in a.u.)       *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER Z
C
C Local variables
C
      REAL*8 RNUC(0:109)
C
C*** End of declarations rewritten by SPAG
C
      DATA RNUC/0.0D0,2.688280D-05,3.394993D-05,3.893984D-05,
     &     4.293464D-05,4.632524D-05,4.930297D-05,5.197748D-05,
     &     5.441818D-05,5.667210D-05,5.877274D-05,6.074494D-05,
     &     6.260766D-05,6.437575D-05,6.606108D-05,6.767330D-05,
     &     6.922041D-05,7.070905D-05,7.214488D-05,7.353271D-05,
     &     7.487670D-05,7.618046D-05,7.744715D-05,7.867954D-05,
     &     7.988009D-05,8.105101D-05,8.219423D-05,8.331154D-05,
     &     8.440452D-05,8.547461D-05,8.652311D-05,8.755123D-05,
     &     8.856005D-05,8.955057D-05,9.052372D-05,9.148035D-05,
     &     9.242124D-05,9.334712D-05,9.425867D-05,9.515651D-05,
     &     9.604123D-05,9.691338D-05,9.777347D-05,9.862198D-05,
     &     9.945937D-05,1.002860D-04,1.011024D-04,1.019088D-04,
     &     1.027057D-04,1.034933D-04,1.042720D-04,1.050420D-04,
     &     1.058038D-04,1.065574D-04,1.073032D-04,1.080414D-04,
     &     1.087723D-04,1.094961D-04,1.102130D-04,1.109231D-04,
     &     1.116267D-04,1.123240D-04,1.130152D-04,1.137003D-04,
     &     1.143796D-04,1.150532D-04,1.157213D-04,1.163840D-04,
     &     1.170414D-04,1.176938D-04,1.183411D-04,1.189835D-04,
     &     1.196212D-04,1.202542D-04,1.208827D-04,1.215068D-04,
     &     1.221265D-04,1.227420D-04,1.233534D-04,1.239607D-04,
     &     1.245640D-04,1.251634D-04,1.257591D-04,1.263510D-04,
     &     1.269393D-04,1.275240D-04,1.281052D-04,1.286830D-04,
     &     1.292574D-04,1.298285D-04,1.303964D-04,1.309611D-04,
     &     1.315227D-04,1.320813D-04,1.326369D-04,1.331895D-04,
     &     1.337392D-04,1.342861D-04,1.348302D-04,1.353715D-04,
     &     1.359102D-04,1.364462D-04,1.369796D-04,1.375105D-04,
     &     1.380389D-04,1.385648D-04,1.390882D-04,1.396093D-04,
     &     1.401280D-04,1.406444D-04/
      RNUCTAB = RNUC(Z)
      END
