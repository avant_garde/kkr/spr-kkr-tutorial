      SUBROUTINE SCALEN( NCT,N,NW,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &           NE,NT,NS, S )
c **********************************************************************
c *                                                                    *
c *    scale the functions  N   and  NW                                *
c *                                                                    *
c **********************************************************************
      REAL S                
      REAL  N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL NMIN(0:NTMAX),NMAX(0:NTMAX)
      INTEGER NCT(NTMAX)

      NC = 0 
      DO 10 IT=1,NT
         NC = MAX( NC, NCT(IT) )
   10 CONTINUE

      DO 52 IT=0,NT
         IF( IT .EQ. 0 ) THEN 
            IC2 = NC
         ELSE
            IC2 = NCT(IT)
         END IF
         NMIN(IT) = NMIN(IT) * S
         NMAX(IT) = NMAX(IT) * S
         DO 52 IS=0,NS
            DO 52 IC=0,IC2     
               DO 52 IE=1,NE
                   N(IE,IT,IC,IS) =  N(IE,IT,IC,IS)  * S            
                  NW(IE,IT,IC,IS) = NW(IE,IT,IC,IS)  * S
   52 CONTINUE
      END
