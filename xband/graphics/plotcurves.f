      SUBROUTINE PLOTCURVES( EMIN,EMAX,NMIN,NMAX,
     &                       NEMAX,NTMAX,NCMAX,NSPMAX,
     &                       N,E,Y, NE, IT1,IT2, IC1,IC2, IS1,IS2 )
c **********************************************************************
c *                                                                    *
c *    plot a set of curves                                            *
c *                                                                    *
c **********************************************************************
      REAL NMIN,NMAX 
      REAL N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX), E(NEMAX), Y(NEMAX)
      REAL S(0:2)

      S(0) = 1.0
      IF( IS2 .EQ. 2 ) THEN
         S(1) = -1.0
         S(2) = +1.0
      ELSE
         S(1) = +1.0
      END IF

      DO 60 IT=IT1,IT2
      DO 60 IC=IC1,IC2  

         DO 65 IS=IS1,IS2
            DO 70 IE = 1,NE
               Y(IE) =  S(IS) * N(IE,IT,IC,IS)
   70       CONTINUE
C# CURVE( 1, NE, E, Y, 'LINEAR' )
   65    CONTINUE
   60 CONTINUE
      END
