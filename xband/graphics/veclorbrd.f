C*==veclorbrd.f    processed by SPAG 6.05Rc at 17:30 on 26 Dec 2000
      SUBROUTINE VECLORBRD(E,F0,FB,NV,NVMAX,NE,NEMAX,G0,WLORTAB)
C **********************************************************************
C *                                                                    *
C *  lorentzian-broadening of the function   F0   width: G0            *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NELAG
      PARAMETER (NELAG=5)
      REAL*8 PI
      PARAMETER (PI=3.141592653589793238462643D0)
C
C Dummy arguments
C
      REAL G0
      INTEGER NE,NEMAX,NV,NVMAX
      REAL E(NEMAX),F0(NVMAX,NEMAX),FB(NVMAX,NEMAX),WLORTAB(NE)
C
C Local variables
C
      DOUBLE PRECISION DBLE
      REAL DE,F0LAG(NELAG,NVMAX),F1,F2,G,W,X0,X1,X2
      INTEGER I,IE,IEX,IV,J
      REAL REAL
      REAL*8 W8
      REAL YLAG
C
C*** End of declarations rewritten by SPAG
C
      DO IE = 1,NE
         DO IV = 1,NV
            FB(IV,IE) = 0.0
         END DO
      END DO
C
      DO IE = 1,NELAG
         DO IV = 1,NV
            F0LAG(IE,IV) = F0(IV,IE)
         END DO
      END DO
C
C
      DE = MAX(E(2)-E(1),0.5)
C
      DO I = 1,NE
C
         X0 = E(I)
C
C -------------------------------------------- deal with low energy tail
C                                        by extrapolation of function F0
         DO J = 1,4
            X1 = E(1) - J*DE
            X2 = X1 + DE
C
            G = G0 + WLORTAB(1)
            W8 = ATAN(DBLE((X2-X1)/(G+(X2-X0)*(X1-X0)/G)))/2D0
            W = REAL(W8/PI)
C
            DO IV = 1,NV
               F1 = YLAG(X1,E,F0LAG(1,IV),0,2,NELAG,IEX)
               F2 = YLAG(X2,E,F0LAG(1,IV),0,2,NELAG,IEX)
               FB(IV,I) = FB(IV,I) + W*(F1+F2)
            END DO
C
         END DO
C
C ----------------------------------------- deal with major contribution
         DO J = 1,NE - 1
C
            X1 = E(J) + 0.000001
            X2 = E(J+1) - 0.000001
C
            G = G0 + (WLORTAB(J)+WLORTAB(J+1))/2.0
            W8 = ATAN(DBLE((X2-X1)/(G+(X2-X0)*(X1-X0)/G)))/2D0
            W = REAL(W8/PI)
C
            DO IV = 1,NV
               FB(IV,I) = FB(IV,I) + W*(F0(IV,J)+F0(IV,J+1))
            END DO
C
         END DO
C
C ------------------------------------------- deal with high energy tail
C                                     by assuming a constant value of F0
         DO J = 0,3
            X1 = E(NE) + J*DE
            X2 = X1 + DE
C
            G = G0 + WLORTAB(NE)
            W8 = ATAN(DBLE((X2-X1)/(G+(X2-X0)*(X1-X0)/G)))/2D0
            W = REAL(W8/PI)
C
            DO IV = 1,NV
               F1 = F0(IV,NE)
               F2 = F0(IV,NE)
               FB(IV,I) = FB(IV,I) + W*(F1+F2)
            END DO
C
         END DO
C
C
      END DO
C
      DO IE = 1,NE
         DO IV = 1,NV
            F0(IV,IE) = FB(IV,IE)
         END DO
      END DO
C
      END
