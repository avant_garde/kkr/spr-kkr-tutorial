      SUBROUTINE FINDEXTR( NCT,N,NEMAX,NTMAX,NCMAX,NSPMAX,
     &                     NE,NT,ICFLAG,NS, NMIN,NMAX )
c **********************************************************************
c *                                                                    *
c *  find extrema in function   N  for IT = IT1,NT                     *
c *  for ICFLAG = 0 :   search within the summed up function IC=0      *
c *            <> 0 :   search within the various curves IC=1,...      *
c *                                                                    *
c **********************************************************************
      REAL N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX), 
     &     NMIN(0:NTMAX),NMAX(0:NTMAX)
      INTEGER NCT(NTMAX)
      IF( ICFLAG .EQ. 0 ) THEN
         IT1 = 0
         IC1 = 0
         IC2 = 0
      ELSE
         IT1 = 1
         IC1 = 1
      END IF
      DO 58 IT=IT1,NT
         NMAX(IT) = 0.0
         IF( ICFLAG .NE. 0 ) IC2 = NCT(IT)
         DO 56 IC=IC1,IC2
         DO 56 IS=1,NS
            DO 56 IE=1,NE
               NMAX(IT) = MAX( NMAX(IT), N(IE,IT,IC,IS) )
   56    CONTINUE
         NMAX(IT) = 1.03 * NMAX(IT)
         IF( NS .EQ. 1 ) THEN
            NMIN(IT) = 0.0
         ELSE
            NMIN(IT) = - NMAX(IT)
         END IF
   58 CONTINUE
      END
