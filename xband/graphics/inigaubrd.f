      SUBROUTINE INIGAUBRD(WGAUSS,WG,NEMAX,DXG,IGMAX)
C **********************************************************************
C *                                                                    *
C *  initialize GAUSSIAN broadening                                    *
C *                                                                    *
C **********************************************************************
 
      IMPLICIT REAL   (A-H,O-Z)

      DIMENSION WG(0:NEMAX)
             
C    cutoff-paramater for range of gaussian

      CUT = 0.001

      DXG = 0.002  

      Y0  = 1.0 / ( SQRT(2*3.1415926) * WGAUSS )

      IGMAX = 0
   10 X     = - DXG
      DO 20 I=0,NEMAX  
         X  = X + DXG
         WG(I) = Y0*EXP( - 0.5*(X/WGAUSS)**2 )
         IF( IGMAX .EQ. 0 ) THEN 
            IF( (WG(I) / Y0) .LT. CUT ) THEN
               IGMAX = I              
            END IF
         END IF
   20 CONTINUE
   21 CONTINUE
      IF( IGMAX .EQ. 0 ) THEN
         DXG = 1.05 * DXG
         GOTO 10
      END IF

      WRITE(*,'('' gaussian broadening: DXG   ='',F8.3,'' eV'',/,
     &          ''                      X_CUT ='',F8.3,'' eV'',/,
     &          ''                      CUT   ='',F8.3,/,
     &          ''                      IGMAX ='',I8,/)') 
     &      DXG,DXG*IGMAX,CUT,IGMAX
      RETURN
      END    
