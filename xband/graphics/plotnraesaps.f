C*==plotnraesaps.f    processed by SPAG 6.05Rc at 15:07 on 15 Jan 2002
      SUBROUTINE PLOTNRAESAPS(ISPEC,IFIL,DATASET,LDATASET,HEADER,
     &                        LHEADER,SYSTEM,LSYSTEM,WLORAS,KW,NEMAX,
     &                        NLMAX)
C **********************************************************************
C *                                                                    *
C *  plot NR-CVV-AES and NR-APS - spectra                              *
C *                                                                    *
C *  VP 01/2002 (NR-AES originally from JM )                           *
C **********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NADD,NLEGMAX,NLMAXLOC
      PARAMETER (NADD=30,NLEGMAX=80)
C
C COMMON variables
C
      REAL WGAUSS,WLOR(2)
      COMMON /BRDPAR/ WGAUSS,WLOR
C
C Dummy arguments
C
      CHARACTER*80 DATASET,HEADER,SYSTEM
      INTEGER IFIL,ISPEC,LDATASET,LHEADER,LSYSTEM,NEMAX,NLMAX
      CHARACTER*10 KW
      REAL WLORAS(2)
C
C Local variables
C
      CHARACTER*3 BIGHEAD(2),SMALLHEAD(2)
      REAL DE,DXG,E(:),ECST(:),ETABFIN(:),
     &     FB(:,:),FBGAU(:,:),WDT,WDT1,WDT2,
     &     WG(:),WLORTAB(:),WTMP(:),XMAX,XMIN,
     &     YDIFF(:),YMAX1,YMAX2,YMIN1,YMIN2,YSPC(:,:),
     &     YSPCGAU(:,:),YSUM(:,:),
     &     ZYSPC(:,:)
      LOGICAL DECSPEC
      CHARACTER*2 DECSTR
      CHARACTER*80 FNOUT,STRG,SUBTITLE,YTXT1,YTXT2
      INTEGER IDUM,IE,IGMAX,IL,IS,KNOHEADER,LFNOUT,LSUBTIT,LYTXT1,
     &        LYTXT2,NCOL,NE,NL,NNMAX,NNMIN,NPOINTS
      CHARACTER*40 LEG1(NLEGMAX),LEG2(NLEGMAX)
      INTEGER LNGSTRING
      CHARACTER*1 LTXT(5),MULT
      REAL REAL
      EXTERNAL LNGSTRING

      ALLOCATABLE ECST,YSPC,WTMP,YSUM,E,FBGAU,YDIFF,ZYSPC,FB,WG
      ALLOCATABLE ETABFIN,WLORTAB,YSPCGAU
      ALLOCATE (ECST(NEMAX),YSPC(NEMAX,3*(NLMAX+1)))
      ALLOCATE (WTMP(-NADD+1:NEMAX),YSUM(NEMAX,2),E(NEMAX))
      ALLOCATE (FBGAU(3*(NLMAX+1),NEMAX),YDIFF(NEMAX))
      ALLOCATE (ZYSPC(-NADD+1:NEMAX,3*(NLMAX+1)))
      ALLOCATE (FB(NEMAX,3*(NLMAX+1)),WG(0:NEMAX))
      ALLOCATE (ETABFIN(-NADD+1:NEMAX),WLORTAB(NEMAX))
      ALLOCATE (YSPCGAU(3*(NLMAX+1),NEMAX))
C
C*** End of declarations rewritten by SPAG
C
      DATA SMALLHEAD/'aes','aps'/
      DATA BIGHEAD/'AES','APS'/
      DATA LTXT/'s','p','d','f','g'/
C
C
      REWIND (IFIL)
 100  CONTINUE
      READ (IFIL,'(a)') STRG
      IF ( STRG(1:5).NE.'   IQ' ) GOTO 100
      READ (IFIL,*) IDUM,NL
C
C----------------------------------------------------------
      IF ( NL.GT.NLMAX ) THEN
         WRITE (6,*) ' NL read in ',NL,' > NLMAX = ',NLMAX
         RETURN
      END IF
C----------------------------------------------------------
      DECSPEC = .FALSE.
      NCOL = (NL+1)*3
C
      IF ( KW(6:8).EQ.'DEC' ) THEN
         NCOL = 4
         DECSPEC = .TRUE.
         REWIND (IFIL)
 150     CONTINUE
         READ (IFIL,'(a)') STRG
         IF ( STRG(1:5).NE.'NTXRS' ) GOTO 150
         READ (IFIL,'(10x,a)') STRG
         IDUM = LNGSTRING(STRG,80)
         DECSTR = STRG(IDUM-1:IDUM)
      END IF
C
      WRITE (6,*) 'Nonrelativistic CVV-'//BIGHEAD(ISPEC)//' spectrum '
      IF ( DECSPEC ) WRITE (6,'(11x,3a)') '       --  ',DECSTR,
     &                      '-resolved'
C
C-----------------------------------------------------------
C     skip header
C
      REWIND (IFIL)
 200  CONTINUE
      READ (IFIL,'(a)') STRG
      IF ( STRG(1:5).NE.' ICST' ) GOTO 200
      READ (IFIL,'(A)') STRG
C-----------------------------------------------------------
C     read data
      NE = 1
      DO IDUM = 1,NEMAX
         READ (IFIL,'(30e12.5)',END=300) ECST(NE),
     &         (YSPC(NE,IL),IL=1,NCOL)
         NE = NE + 1
      END DO
 300  CONTINUE
      NE = NE - 1
      CLOSE (IFIL)
      WRITE (6,'(A,i4)') 'Number of energies read in :',NE
      WRITE (6,'(A,f10.5,A,f10.5,A)') '      from E = ',ECST(1),' to ',
     &                                ECST(NE),' eV.'
C
C=====================================================================
C  BROADENING THE SPECTRA
C
      IF ( ISPEC.EQ.1 ) THEN
         WDT2 = WLORAS(1)
         WDT = WLORAS(2)
      ELSE
         WDT2 = WLOR(1)
         WDT = 0.00
         IF ( WDT2.GT.10.0 ) THEN
            WRITE (6,*) ' Dubious value for W_L, set to ZERO '
            WDT2 = 0.00
         END IF
      END IF
C
C---------------------------------------------------------
C     extend e-range above calculated limit
C---------------------------------------------------------
      DE = (ECST(NE)-ECST(1))/REAL((NE-1))
      DO IDUM = 1,NE
         ETABFIN(IDUM) = ECST(1) + DE*(IDUM-1)
         DO IL = 1,NCOL
            ZYSPC(IDUM,IL) = YSPC(IDUM,IL)
         END DO
      END DO
      DO IE = 1,NE
         WTMP(IE) = 0.00
         WTMP(IE) = WDT*(ECST(NE)-ECST(IE))**2
      END DO
      DO IE = NE + 1,NE + NADD
         ETABFIN(IE) = 0.00
         ETABFIN(IE) = ECST(1) + DE*(IE-1)
         WTMP(IE) = 0.0000000001
         DO IL = 1,NCOL
            ZYSPC(IE,IL) = 0.00
         END DO
      END DO
C---------------------------------------------------------
C     for AES extend e-range below calculated limit
C---------------------------------------------------------
      IF ( ISPEC.EQ.1 ) THEN
         DO IE = 0,1 - NADD, - 1
            ETABFIN(IE) = ECST(1) - DE*ABS(IE-1)
            WTMP(IE) = 0.0000000001
            DO IL = 1,NCOL
               ZYSPC(IE,IL) = 0.00
            END DO
         END DO
      END IF
C
      NPOINTS = 0
      NNMAX = NE + NADD
      NNMIN = -(ISPEC-2)*NADD - 1
C
      DO IE = -NNMIN,NNMAX
         NPOINTS = NPOINTS + 1
         E(NPOINTS) = ETABFIN(IE)
         WLORTAB(NPOINTS) = WTMP(IE)
         DO IL = 1,NCOL
            YSPC(NPOINTS,IL) = ZYSPC(IE,IL)
         END DO
      END DO
C---------------------------------------------------------
C     Lorentzian broadening
C---------------------------------------------------------
      IF ( WDT2.GT.0.00 ) THEN
         CALL LORBRDAES(E,YSPC,FB,NCOL,3*(NLMAX+1),NPOINTS,NEMAX,WDT2)
      ELSE
         WRITE (6,*) ' Lorentzian broadening skipped!'
      END IF
C
C---------------------------------------------------------
C     Lorentzian broadening - energy dependent (quadratic)
C---------------------------------------------------------
      IF ( ISPEC.EQ.1 ) THEN
         IF ( WDT.GT.0.D0 ) THEN
            CALL LORBRDAESENE(E,YSPC,FB,NCOL,3*(NLMAX+1),NPOINTS,NEMAX,
     &                        WLORTAB)
         ELSE
            WRITE (6,*) 
     &                ' Energy-dependent Lorentzian broadening skipped!'
         END IF
      END IF
C
C------------------------------------------------------
C    Gaussian broadening
C------------------------------------------------------
      WDT1 = WGAUSS
      IF ( WDT1.GT.0.D0 ) THEN
         DO IL = 1,NCOL
            DO IE = 1,NPOINTS
               YSPCGAU(IL,IE) = YSPC(IE,IL)
            END DO
         END DO
         CALL INIGAUBRD(WDT1,WG,NEMAX,DXG,IGMAX)
         CALL VECGAUBRD(E,YSPCGAU,FBGAU,NCOL,3*(NLMAX+1),NPOINTS,NEMAX,
     &                  WG,DXG,IGMAX)
         DO IL = 1,NCOL
            DO IE = 1,NPOINTS
               YSPC(IE,IL) = YSPCGAU(IL,IE)
            END DO
         END DO
      ELSE
         WRITE (6,*) ' Gausian broadening skipped!'
      END IF
C
C  BROADENING OF THE SPECTRA END
C=====================================================================
C
C
C=====================================================================
C     XMGRACE - Spin resolved
C
      XMIN = E(1)
      XMAX = E(NPOINTS)
C
      YTXT1 = 'Intensity (arb. units)'
      LYTXT1 = 23
      YTXT2 = 'Intensity (arb. units)'
      LYTXT2 = 23
C
      YMAX1 = -1E-10
      YMIN1 = 1E10
      YMAX2 = -1E-10
      YMIN2 = 1E10
      IF ( .NOT.DECSPEC ) THEN
         DO IE = 1,NPOINTS
            YSUM(IE,1) = YSPC(IE,1)
            YSUM(IE,2) = YSUM(IE,1)
         END DO
      ELSE
         DO IE = 1,NPOINTS
            YSUM(IE,1) = YSPC(IE,1) + YSPC(IE,2)
            YSUM(IE,2) = YSPC(IE,3) + YSPC(IE,4)
         END DO
      END IF
C
      DO IE = 1,NPOINTS
         IF ( YMAX1.LT.YSUM(IE,1) ) YMAX1 = YSUM(IE,1)
         IF ( YMIN1.GT.YSUM(IE,1) ) YMIN1 = YSUM(IE,1)
C
         IF ( YMAX2.LT.YSUM(IE,2) ) YMAX2 = YSUM(IE,2)
         IF ( YMIN2.GT.YSUM(IE,2) ) YMIN2 = YSUM(IE,2)
      END DO
C
      IF ( .NOT.DECSPEC ) THEN
         IF ( ISPEC.EQ.1 ) THEN
            DO IE = 1,NPOINTS
               YDIFF(IE) = (YSPC(IE,2)-YSPC(IE,3))*10.00
               IF ( YMIN1.GT.YDIFF(IE) ) YMIN1 = YDIFF(IE)
            END DO
            SUBTITLE = 'Spin-resolved NR-CVV-AES of '//SYSTEM(1:LSYSTEM)
            LSUBTIT = 28 + LSYSTEM
         ELSE
            SUBTITLE = 'Spin-resolved NR-APS of '//SYSTEM(1:LSYSTEM)
            LSUBTIT = 24 + LSYSTEM
         END IF
C
C
         CALL XMGR4HEAD(DATASET,LDATASET,SMALLHEAD(ISPEC),3,' ',0,FNOUT,
     &                  80,LFNOUT,IFIL,2,XMIN,1,XMAX,1,YMIN1,1,YMAX1,1,
     &                  YMIN2,1,YMAX2,1,'energy (eV)',11,YTXT1,LYTXT1,
     &                  YTXT2,LYTXT2,HEADER,LHEADER,SUBTITLE,LSUBTIT,
     &                  KNOHEADER)
C
         CALL XMGRCURVES(IFIL,2,5-ISPEC,2*NL,2,1,0)
C
         LEG1(1) = 'I!S!UP!N+I!S!DN'
         LEG1(2) = 'I!S!UP!N-I!S!DN!N!4x!010'
         LEG1(4-ISPEC) = 'I!S!UP'
         LEG1(5-ISPEC) = 'I!S!DN'
C
         MULT = '2'
         IF ( ISPEC.EQ.1 ) MULT = '3'
C
         DO IS = 1,2
            STRG = 'UP'
            IF ( IS.EQ.2 ) STRG = 'DN'
            DO IL = 1,NL
               IDUM = (IS-1)*NL + IL
               IF ( IL.LE.2 ) THEN
                  LEG2(IDUM) = 'I!S'//LTXT(IL)//','//STRG(1:2)
     &                         //'!N!4x!010!S'//MULT(1:1)
               ELSE
                  LEG2(IDUM) = 'I!S'//LTXT(IL)//','//STRG(1:2)
               END IF
            END DO
         END DO
C
         CALL XMGRLEGEND(IFIL,2,5-ISPEC,2*NL,LEG1,LEG2)
C
         CALL XMGR4TABLE(0,0,E,YSUM(1,1),1.0,NPOINTS,IFIL)
         IF ( ISPEC.EQ.1 ) CALL XMGR4TABLE(0,1,E,YDIFF,1.0,NPOINTS,IFIL)
         DO IL = 1,2
            IDUM = IL + 2 - ISPEC
            CALL XMGR4TABLE(0,IDUM,E,YSPC(1,IL+1),1.0,NPOINTS,IFIL)
         END DO
C
         DO IDUM = 1,2
            DO IE = 1,NPOINTS
               YSPC(IE,3+NL+IDUM) = YSPC(IE,3+NL+IDUM)*10.00**(4-ISPEC)
               YSPC(IE,3+2*NL+IDUM) = YSPC(IE,3+2*NL+IDUM)
     &                                *10.00**(4-ISPEC)
            END DO
         END DO
C
         IDUM = -1
         DO IS = 1,2
            DO IL = 1,NL
               IDUM = IDUM + 1
               CALL XMGR4TABLE(1,IDUM,E,YSPC(1,3+NL+IDUM+1),1.0,NPOINTS,
     &                         IFIL)
            END DO
         END DO
C
      ELSE
C
         IF ( ISPEC.EQ.1 ) THEN
            SUBTITLE = 'Spin-resolved NR-CVV-AES of '//SYSTEM(1:LSYSTEM)
     &                 //', '//DECSTR(1:2)//'-contributions'
            LSUBTIT = 28 + LSYSTEM + 18
         ELSE
            SUBTITLE = 'Spin-resolved NR-APS of '//SYSTEM(1:LSYSTEM)
     &                 //', '//DECSTR(1:2)//'-contributions'
            LSUBTIT = 24 + LSYSTEM + 18
         END IF
C
         CALL XMGR4HEAD(DATASET,LDATASET,SMALLHEAD(ISPEC),3,' ',0,FNOUT,
     &                  80,LFNOUT,IFIL,2,XMIN,1,XMAX,1,YMIN1,1,YMAX1,1,
     &                  YMIN2,1,YMAX2,1,'energy (eV)',11,YTXT1,LYTXT1,
     &                  YTXT2,LYTXT2,HEADER,LHEADER,SUBTITLE,LSUBTIT,
     &                  KNOHEADER)
C
         CALL XMGRCURVES(IFIL,2,3,3,2,1,0)
C
         LEG1(1) = 'I!S!UP'
         LEG1(2) = 'I!S!UP UP'
         LEG1(3) = 'I!S!UP DN'
C
         LEG2(1) = 'I!S!DN'
         LEG2(2) = 'I!S!DN UP'
         LEG2(3) = 'I!S!DN DN'
C
         CALL XMGRLEGEND(IFIL,2,3,3,LEG1,LEG2)
C
         CALL XMGR4TABLE(0,0,E,YSUM(1,1),1.0,NPOINTS,IFIL)
         CALL XMGR4TABLE(0,1,E,YSPC(1,1),1.0,NPOINTS,IFIL)
         CALL XMGR4TABLE(0,2,E,YSPC(1,2),1.0,NPOINTS,IFIL)
C
         CALL XMGR4TABLE(1,0,E,YSUM(1,2),1.0,NPOINTS,IFIL)
         CALL XMGR4TABLE(1,1,E,YSPC(1,3),1.0,NPOINTS,IFIL)
         CALL XMGR4TABLE(1,2,E,YSPC(1,4),1.0,NPOINTS,IFIL)
      END IF
C
C     XMGRACE - Spin resolved
C=====================================================================
C
      WRITE (6,*) '  '
      WRITE (6,*) 'Spin resolved NR-CVV-'//BIGHEAD(ISPEC)
     &            //' writen in ',FNOUT(1:LFNOUT)
C
      END
