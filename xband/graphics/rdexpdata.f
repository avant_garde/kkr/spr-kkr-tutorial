      SUBROUTINE RDEXPDATA( EEXP, IFILEXP, IEXP,NEMAX, 
     &                      NCEXPMAX,NPOLEXPMAX )
      REAL EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
C **********************************************************************
C *                                                                    *
C *      read experimental spectra                                     *
C *                                                                    *
C ********************************************************************** 
      CHARACTER FMTEXP*80,LINE*80
      LOGICAL EXPDATAVL
      COMMON /EXPPAR/ ESEXP(2),BLSEXP(2),SCLEXP(2),NEEXP(2),
     &                NCEXP,NPOLEXP,FMTEXP,EXPDATAVL
c ----------------------------------------------------------------------
      IF( NCEXP .GT. NCEXPMAX ) STOP '  NCEXP > NCEXPMAX '

      CALL KWPOSFIL( IFILEXP, '##########', 10, 1, 0, IPOS )

      IF( FMTEXP(1:4) .EQ. 'FREE' ) THEN
         DO 10 IE=1,NEMAX
            READ(IFILEXP,*,END=30) 
     &      EEXP(IE,1), (IEXP(IE,1,IPOL),IPOL=1,NPOLEXP)
            NEEXP(1) = IE
   10    CONTINUE
      ELSE
         DO 20 IE=1,NEMAX
            READ(IFILEXP,FMT=FMTEXP,END=30) 
     &      EEXP(IE,1), (IEXP(IE,1,IPOL),IPOL=1,NPOLEXP)
            NEEXP(1) = IE
   20    CONTINUE
      END IF
   30 CONTINUE

      WRITE(6,'(/,''  experimental data:'',/,
     &            ''  =================='',/)')
      IF( IPOS .NE. 0 ) THEN
         REWIND IFILEXP
   35    READ( IFILEXP, '(A)' ) LINE
         IF( LINE(1:5) .NE. '#####' ) THEN
            WRITE(6,'(2X,A)' ) LINE 
            GOTO 35
         ELSE
            WRITE(6,*) ' '
         END IF
      END IF
         
      WRITE(6,9010) '  number of polarisations ',NPOLEXP
      WRITE(6,9010) '  number of curves        ',NCEXP
      WRITE(6,9010) '  number of energies      ',( NEEXP(IC),IC=1,NCEXP) 

      IF( (ESEXP(1)+BLSEXP(1)+SCLEXP(1)) .LT. 0.0001 ) RETURN
      
      WRITE(6,9020) '  energy shift            ',( ESEXP(IC),IC=1,NCEXP) 
      WRITE(6,9020) '  base line shift         ',(BLSEXP(IC),IC=1,NCEXP) 
      WRITE(6,9020) '  scaling factor          ',(SCLEXP(IC),IC=1,NCEXP) 
      
      DO 40 IC=1,NCEXP
      DO 40 IE=1,NEEXP(IC)
         EEXP(IE,IC) = EEXP(IE,IC) - ESEXP(IC)
         DO 50 IPOL=1,NPOLEXP
            IEXP(IE,IC,IPOL) = 
     &     (IEXP(IE,IC,IPOL) - BLSEXP(IC)) * SCLEXP(IC) 
   50    CONTINUE    
   40 CONTINUE 

 9010 FORMAT(A,5I12)
 9020 FORMAT(A,5E12.4)
      END
