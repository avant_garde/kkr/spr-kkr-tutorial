      SUBROUTINE FILLUP( NCT,N,NW,NEMAX,NTMAX,NCMAX,NSPMAX,
     &                   NE,NT,NS,CONC,NQT )
c **********************************************************************
c *                                                                    *
c *      fill up the arrays for the dos or intensity                   *
c *      the element for a given index = 0 contains the sum            *
c *      over all indices <> 0                                         *
c *      e.g.:  N(IE,0,0,0)  contains the sum over  IT, IL and IS      *
c *      nw contains the concentration*stoichiometry weighted  n       *
c *                                                                    *
c **********************************************************************
      REAL WT,CONC(NTMAX)
      REAL  N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      INTEGER NCT(NTMAX), NQT(NTMAX)

      NC = 0 
      DO 10 IT=1,NT
         NC = MAX( NC, NCT(IT) )
   10 CONTINUE

      DO 52 IT=1,NT
         DO 52 IC=1,NCT(IT)
            DO 52 IS=1,NS
               DO 52 IE=1,NE
                  N(IE,IT,0,IS) = N(IE,IT,0,IS) + N(IE,IT,IC,IS)
                  N(IE,IT,IC,0) = N(IE,IT,IC,0) + N(IE,IT,IC,IS)
   52 CONTINUE
      
      DO 54 IT=1,NT
         DO 54 IS=1,NS
            DO 54 IE=1,NE
               N(IE,IT,0,0 ) = N(IE,IT,0,0 ) + N(IE,IT,0,IS)
   54 CONTINUE

      DO 56 IT=1,NT
         WT = NQT(IT) * CONC(IT)
         DO 56 IC=0,NCT(IT)
            DO 56 IS=0,NS
               DO 56 IE=1,NE
                  NW(IE,IT,IC,IS) = N(IE,IT,IC,IS) * WT
   56 CONTINUE

      DO 58 IT=1,NT
         DO 58 IC=0,NC
            DO 58 IS=0,NS
               DO 58 IE=1,NE
                  N(IE,0,IC,IS) = N(IE,0,IC,IS) + NW(IE,IT,IC,IS)
   58 CONTINUE

      DO 60 IC=0,NC
         DO 60 IS=0,NS
            DO 60 IE=1,NE
               NW(IE,0,IC,IS) = N(IE,0,IC,IS)
   60 CONTINUE
      END
