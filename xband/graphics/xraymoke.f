      SUBROUTINE XRAYMOKE( NTXRSGRP, ECOREAV,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,
     &                    SPEC,DATASET,LDATASET,
     &                    NLQ, NLT, NMIN,NMAX, 
     &                    NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADT,STR80T,LHEADT,LSTR80T,
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,RYOVEV,EUNIT,EREFER,
     &                    EMIN,EMAX,DE,MERGE,IZ,VUC,
     &                    NEEXP,NCEXP,NPOLEXP,EXPDATAVL,
     &                    DX,KTYP,NKTYP,NKTYPEFF, 
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE, 
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,  
     &                    AD,AT,RD,RT, TD,TT,TE, KTABLE, SCLFAC,NPOL, 
     &                    NOTABEXT,KNOHEADER )
C **********************************************************************
C *                                                                    *
C **********************************************************************
c ----------------------------------------------------------------------
      PARAMETER ( LCMDMAX = 500 )
      PARAMETER ( LSTRMAX = 100 )
      PARAMETER ( IFTABLE = 98, IFCONVT = 99 )
      PARAMETER ( IFC=IFCONVT, LSM=LSTRMAX )
      PARAMETER ( EQUTOL= 1E-6 )
      PARAMETER ( NLEGMAX= 50 ) 
      PARAMETER ( NTETMAX = 10 )
      COMPLEX*16 CI
      PARAMETER (CI=(0.0D0,1.0D0)  )
      REAL*8     PI
      PARAMETER ( PI = 3.141592653589793238462643D0 ) 
c CNVTE2O:     E [eV] --> omega [10^15 1/s]
      REAL*8 CNVTE2O, CCGS, A0, E0, EMASS
      PARAMETER( CNVTE2O=1.519249D0 )                                          
      PARAMETER ( CCGS = 2.997930D+10 )
      PARAMETER ( A0 = 0.52917725D-08 )          
      PARAMETER ( E0=1.6021892D-19*2.997930D+09 )
      PARAMETER ( EMASS=0.91095344D-27 )
c ----------------------------------------------------------------------

      COMPLEX*16 SIGXX,SIGXY,CKANG,CEPS(:,:),CSIG
      COMPLEX*16 CWORK1,CWORK2,CWORK3,CWORK4,CBETA,CEPSXX,CEPSXY
      COMPLEX*16 SIGPLS,SIGMIN,CNPLS,CNMIN,CFANG
      REAL*8     OMEGA,DD,RE,N0,EPS0
      REAL EPSR(:,:), EPSI(:,:)
      CHARACTER*20 LEG(NLEGMAX)

      REAL         EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
      REAL         E(NEMAX),RWORK1,RWORK2
      REAL         AD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             AT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             RD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX), 
     &             RT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             SD(:,:,:,:)

      CHARACTER*80 YTXT0,YTXT1
      CHARACTER*80 HEADER,SYSTEM, INFO, FILNAM, STR80
      CHARACTER    SHELL(5)*1, SPEC*3, EXT*7, EUNIT*2,IDENT*4
      CHARACTER*10 TXTT(NTMAX), STR10
      CHARACTER*10 SUP,SDN,STET(NTETMAX)
      CHARACTER*80 HEADT(0:NTMAX,2), STR80T(0:NTMAX,2)
      INTEGER LHEADT(0:NTMAX,2), LSTR80T(0:NTMAX,2)
      
      INTEGER LHEADER, LSYSTEM,IZ,LSTET(NTETMAX)
      INTEGER NLQ(NQMAX), NLT(NTMAX)
      INTEGER IQOFT(:,:), NQT(NTMAX), LTXTT(NTMAX)
      INTEGER NCXRAY(NTMAX), LCXRAY(NTMAX)
      INTEGER ITXRSGRP(NTMAX)
      INTEGER ICST1(2), ICST2(2), KTYP(2), KTYPCOR(NCSTMAX)
      INTEGER KAPCOR(NCSTMAX), MM05COR(NCSTMAX),
     &        NKPCOR(NCSTMAX),  IKMCOR(NCSTMAX,2)
      INTEGER IN(NEMAX), NEEXP(2)
      INTEGER ATOMNUMB
      
      REAL    REFL(:,:), ASYM(:,:)
      REAL    TETK(:), EPSK(:), DDEL(:)
      REAL    TETF(:), EPSF(:), DBET(:)
      REAL    EPS1(:,:), EPS2(:,:)
      REAL    CONC(NTMAX),VUC, TET(NTETMAX)
      REAL    NMIN(0:NTMAX), NMAX(0:NTMAX)
      REAL    ECORE(NTMAX,NCSTMAX)
      REAL    TD(NEMAX),TT(NEMAX),TE(NEMAX)
      REAL    ED(:),ET(:)
      REAL    SUMK(2), ECOREAV(2), RAV(2),RDF(2)
      CHARACTER*3 SUBSH(0:4)
      CHARACTER*2 SUBSH2(0:4)
      LOGICAL EXPDATAVL,EPSCORR

c ----------------------------------------------------------------------
      CHARACTER*(LCMDMAX) CMDUC, CMD00
      CHARACTER*(LSTRMAX) DATASET, STR
      LOGICAL TABLE, CMDFOUND, UDT, KTABLE
      LOGICAL NOTABEXT, KNOHEADER
c ----------------------------------------------------------------------

      DATA    SHELL / 'K', 'L', 'M', 'N', 'O' /                  
      DATA    SUBSH / '1  ','2,3','4,5','6,7','8,9'/
      DATA    SUBSH2/ '1 ', '23', '45', '67', '89' /

      ALLOCATABLE DDEL,DBET,REFL,CEPS,EPSF,EPSI,EPSK,TETF,TETK,EPSR
      ALLOCATABLE ASYM,IQOFT,ED,SD,ET,EPS1,EPS2
      ALLOCATE (DDEL(NEMAX),DBET(NEMAX),REFL(NEMAX,NTETMAX))
      ALLOCATE (CEPS(NEMAX,9),EPSF(NEMAX),EPSI(NEMAX,9),EPSK(NEMAX))
      ALLOCATE (TETF(NEMAX),TETK(NEMAX),EPSR(NEMAX,9))
      ALLOCATE (ASYM(NEMAX,NTETMAX),IQOFT(NQMAX,NTMAX),ED(NEMAX))
      ALLOCATE (SD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),ET(NEMAX))
      ALLOCATE (EPS1(NEMAX,9),EPS2(NEMAX,9))

c      it=1
c      k=1
c         do isig=1,9
c         do ie = 1,ne
c            rd(ie,it,k,isig)=-rd(ie,it,k,isig)  
c            rt(ie,it,k,isig)=-rt(ie,it,k,isig) 
c         end do
c         end do  

      NTET = 5 
      DO ITET=1,NTET
         TET(ITET) = 2.0*ITET
      END DO

      OPEN(99,STATUS='SCRATCH')
      DO ITET=1,NTET
         REWIND 99
         IF( ABS(TET(ITET)-INT(TET(ITET))).LT.1E-4) THEN 
            WRITE(99,'(I2)') INT(TET(ITET))
         ELSE
            WRITE(99,'(F6.3)') TET(ITET)
         END IF
         REWIND 99
         READ(99,'(A)') STR10
         LL = LNGSTRING( STR10, 10 )
         STET(ITET) =  STR10(1:LL)        
         LSTET(ITET) = LL        
      END DO
      CLOSE( 99 )

      IF( EXPDATAVL ) THEN
         IF( NCEXP .GT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  > NKTYPEFF=',NKTYPEFF
         END IF
         IF( NCEXP .LT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  < NKTYPEFF=',NKTYPEFF
         END IF
      END IF
      DO ITXG = 1,NTXRSGRP 
         NLT(ITXG) = NKTYPEFF  
      END DO  

      CALL FILLUP( NLT,RD,AD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )
      CALL FILLUP( NLT,RT,AT,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )

C ======================================================================
C                 PLOT SPECTRA AND COMPARE WITH EXPERIMENT 
C ======================================================================
      IF( ABS(EMIN-9999.) .LT. 0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF( ABS(EMAX-9999.) .LT. 0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
      XMIN = -30.0
C ----------------------------------------------------------------------
      IFIL = 10 

      DO ITXG = 1,NTXRSGRP 
         IT = ITXG


         IF( ABS(VUC) .LT. 1E-6 ) THEN
            WRITE(6,91001) 
            EPSCORR = .FALSE.
         ELSE
            N0 = DBLE(NQT(IT)*CONC(IT))/(DBLE(VUC)*A0**3)
            RE = E0**2 / ( EMASS * CCGS**2 )
            WRITE(6,91002) VUC,IZ,NQT(IT),CONC(IT),RE
            EPSCORR = .TRUE.
         END IF

      DO K=1,NKTYPEFF
C
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
C
         SUP = TXTT(IT)(1:LTXTT(IT))
         LSUP = LTXTT(IT)
C
         IF( SHELL(NC) .NE. 'K' ) THEN 
            SDN  = SHELL(NC)//'!s'//SUBSH(LC)
            LSDN = 1 + 5
         ELSE
            SDN  = SHELL(NC)
            LSDN = 1
         END IF
         SUP=' '
         SDN=' '         
         LSUP=1
         LSDN=1
C
         IF( NKTYPEFF .EQ. NKTYP ) THEN
            EOFFSET = ABS( ECOREAV(K) )
         ELSE
            EOFFSET = -9999.9
            DO I=1,NKTYP
                EOFFSET = MAX(EOFFSET,ABS(ECOREAV(K)))
            END DO
         END IF
         
         WRITE(*,90012) EOFFSET
90012    FORMAT('  energy offset',F12.3,' eV to convert E to omega')         

C=======================================================================
C                      diagonal elements of SIGMA
C=======================================================================
         YRMIN = +1E+10
         YRMAX = -1E+10
         YIMIN = +1E+10
         YIMAX = -1E+10
         DO ISIG=1,9,4
         DO IE = 1,NE
            YRMIN= MIN( YRMIN,RD(IE,IT,K,ISIG) ) 
            YRMAX= MAX( YRMAX,RD(IE,IT,K,ISIG) ) 
            YIMIN= MIN( YIMIN,RT(IE,IT,K,ISIG) ) 
            YIMAX= MAX( YIMAX,RT(IE,IT,K,ISIG) )
         END DO
         END DO  

         YTXT1 = '!xs!F!m{1}!S2 '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'!xaa!F '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 7 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N  (10!S15!N s!S-1!N)'
         LYTXT1 = LYTXT1 + 22
         YTXT0 = YTXT1(1:12)//'1'//YTXT1(14:LYTXT1)

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_sig_dia',
     &                  LL+8,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YRMIN, 1,YRMAX,1,
     &                  YIMIN, 1,YIMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT1,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'diagonal elements of !xs',24,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         LEG(1)= 'xx'
         LEG(2)= 'yy'
         LEG(3)= 'zz'
 
         CALL XMGRLEGEND( IFIL,NS,3,0,LEG,LEG )

c --------------------------------------------------- real part of SIGMA
         IS=-1
         DO ISIG=1,9,4
            IS=IS+1
            CALL XMGR4TABLE(0,IS,E,RD(1,IT,K,ISIG),1.0,NE,IFIL)
         END DO

c ---------------------------------------------- imaginary part of SIGMA
         IS=-1
         DO ISIG=1,9,4
            IS=IS+1
            CALL XMGR4TABLE(1,IS,E,RT(1,IT,K,ISIG),1.0,NE,IFIL)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' SIGMA-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C                      off-diagonal elements of SIGMA
C=======================================================================
         YRMIN = +1E+10
         YRMAX = -1E+10
         YIMIN = +1E+10
         YIMAX = -1E+10
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
         DO IE = 1,NE
            YRMIN= MIN( YRMIN,RD(IE,IT,K,ISIG) ) 
            YRMAX= MAX( YRMAX,RD(IE,IT,K,ISIG) ) 
            YIMIN= MIN( YIMIN,RT(IE,IT,K,ISIG) ) 
            YIMAX= MAX( YIMAX,RT(IE,IT,K,ISIG) )
         END DO
         END IF
         END DO  

         YTXT1 = '!xs!F!m{1}!S2 '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'!xab!F '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 7 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N  (10!S15!N s!S-1!N)'
         LYTXT1 = LYTXT1 + 22
         YTXT0 = YTXT1(1:12)//'1'//YTXT1(14:LYTXT1)

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_sig_off',
     &                  LL+8,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YRMIN, 1,YRMAX,1,
     &                  YIMIN, 1,YIMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT1,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'off-diagonal elements of !xs',28,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         LEG(1)= 'xy'
         LEG(2)= 'xz'
         LEG(3)= 'yx'
         LEG(4)= 'yz'
         LEG(5)= 'zx'
         LEG(6)= 'zy'
 
         CALL XMGRLEGEND( IFIL,NS,6,0,LEG,LEG )
 
c --------------------------------------------------- real part of SIGMA
         IS=-1
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
            IS=IS+1
            CALL XMGR4TABLE(0,IS,E,RD(1,IT,K,ISIG),1.0,NE,IFIL)
         END IF
         END DO

c ---------------------------------------------- imaginary part of SIGMA
         IS=-1
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
            IS=IS+1
            CALL XMGR4TABLE(1,IS,E,RT(1,IT,K,ISIG),1.0,NE,IFIL)
         END IF
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' SIGMA-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C                   POLAR FARADAY AND KERR-ROTATION
C=======================================================================
 
         DO I = 1,NE
            OMEGA = DBLE( E(I) + EOFFSET ) * CNVTE2O
 
            DO ISIG=1,9
               CSIG = DCMPLX( RD(I,IT,K,ISIG),RT(I,IT,K,ISIG) )
               CEPS(I,ISIG) = + CI*4.D0*PI*CSIG/OMEGA
               EPSR(I,ISIG) = REAL( DREAL(CEPS(I,ISIG)) ) 
               EPSI(I,ISIG) = REAL( DIMAG(CEPS(I,ISIG)) )
            END DO
 
            DO ISIG=1,9,4
               EPSR(I,ISIG) = 1.0 + EPSR(I,ISIG) 
            END DO
            
            IF( EPSCORR ) THEN 
               EPS0 = - 4.D0*PI*N0*RE*(CCGS**2)*IZ/(OMEGA*1D+15)**2
               DO ISIG=1,9,4
                  EPSR(I,ISIG) = REAL(EPS0) + EPSR(I,ISIG) 
               END DO
            END IF

            ISIG = 1
            SIGXX = DCMPLX( RD(I,IT,K,ISIG),RT(I,IT,K,ISIG) )
            ISIG = 4
            SIGXY = DCMPLX( RD(I,IT,K,ISIG),RT(I,IT,K,ISIG) )
C
            CKANG = - SIGXY/
     &            ( SIGXX * SQRT(1.D0+CI*4.D0*PI*SIGXX/OMEGA) )
            CKANG = CKANG*180.D0/PI
C
            TETK(I) = REAL( DREAL(CKANG) ) 
            EPSK(I) = REAL( DIMAG(CKANG) )
C
            SIGPLS = SIGXX + CI*SIGXY
            SIGMIN = SIGXX - CI*SIGXY
C
            CNPLS =  SQRT(1.D0+CI*4.D0*PI*SIGPLS/OMEGA)
            CNMIN =  SQRT(1.D0+CI*4.D0*PI*SIGMIN/OMEGA)

C add scaling factor 10^+15 for omega  and  set d = 1 nm
            DD  = 1D0 * 1D-7
            CFANG = (OMEGA*1D+15*DD/(2D0*CCGS)) * (CNPLS-CNMIN)
            CFANG = - CFANG*180.D0/PI

            TETF(I) = REAL( DREAL(CFANG) )
            EPSF(I) = REAL( DIMAG(CFANG) )

            DDEL(I) = - REAL( DREAL(CNPLS-CNMIN) / 2D0 )
            DBET(I) =   REAL( DIMAG(CNPLS-CNMIN) / 2D0 )

         END DO

         YKMIN = +1E+10
         YKMAX = -1E+10
         YFMIN = +1E+10
         YFMAX = -1E+10
C
         DO IE = 1,NE
            YKMIN= MIN( YKMIN,TETK(IE),EPSK(IE) ) 
            YKMAX= MAX( YKMAX,TETK(IE),EPSK(IE) ) 
            YFMIN= MIN( YFMIN,TETF(IE),EPSF(IE) ) 
            YFMAX= MAX( YFMAX,TETF(IE),EPSF(IE) )
         END DO

         YTXT1 = '!xq!F!m{1}!S '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'K '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 2 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N  (!So!N)'
         LYTXT1 = LYTXT1 + 11
         I = 13 + LSUP + 9
         YTXT0 = YTXT1(1:I)//'F'//YTXT1((I+2):(LYTXT1-1))//'/nm)'
         LYTXT0 = LYTXT1 + 3

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_kerr',
     &                  LL+5,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YFMIN, 1,YFMAX,1,
     &                  YKMIN, 1,YKMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT0,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'polar Faraday- and Kerr-spectra',31,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         LEG(1)= '!xq!F!sF'
         LEG(2)= '!xe!F!sF'
         CALL XMGRLEGEND( IFIL,NS,2,0,LEG,LEG )
         LEG(1)= '!xq!F!sK'
         LEG(2)= '!xe!F!sK'
         CALL XMGRLEGEND( IFIL,NS,0,2,LEG,LEG )
 
c --------------------------------------------------- Faraday - rotation
         CALL XMGR4TABLE(0,0,E,TETF,1.0,NE,IFIL)
         CALL XMGR4TABLE(0,1,E,EPSF,1.0,NE,IFIL)

c --------------------------------------------------- Kerr    - rotation
         CALL XMGR4TABLE(1,0,E,TETK,1.0,NE,IFIL)
         CALL XMGR4TABLE(1,1,E,EPSK,1.0,NE,IFIL)

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' MOKE-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C             dichroic parts of complex refractive index
C=======================================================================
         YDMIN = +1E+10
         YDMAX = -1E+10
         YBMIN = +1E+10
         YBMAX = -1E+10
C
         DO IE = 1,NE
            YDMIN= MIN( YDMIN,DDEL(IE) ) 
            YDMAX= MAX( YDMAX,DDEL(IE) ) 
            YBMIN= MIN( YBMIN,DBET(IE) ) 
            YBMAX= MAX( YBMAX,DBET(IE) )
         END DO

         YTXT1 = '!xDb!F!m{1}!S '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//SDN(1:LSDN)
         LYTXT1 = 15 + LSUP + 9 + LSDN
         YTXT0 = '!xDd'//YTXT1(5:LYTXT1)

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_n_dic',
     &                  LL+6,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YDMIN, 1,YDMAX,1,
     &                  YBMIN, 1,YBMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT1,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'dichroic part of refractive index n',35,
     &                  KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

c         LEG(1)= '!xDd!F  '
c         CALL XMGRLEGEND( IFIL,NS,1,0,LEG,LEG )
c         LEG(1)= '!xDb!F  '
c         CALL XMGRLEGEND( IFIL,NS,0,1,LEG,LEG )
 
c -------------------------------------------------- Real part     delta
         CALL XMGR4TABLE(0,0,E,DDEL,1.0,NE,IFIL)

c -------------------------------------------------- Imaginary part beta
         CALL XMGR4TABLE(1,0,E,DBET,1.0,NE,IFIL)

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' dich. part of n  written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C                      diagonal elements of EPSILON
C=======================================================================
         YRMIN = +1E+10
         YRMAX = -1E+10
         YIMIN = +1E+10
         YIMAX = -1E+10
         DO ISIG=1,9,4
         DO IE = 1,NE
            YRMIN= MIN( YRMIN,EPSR(IE,ISIG) ) 
            YRMAX= MAX( YRMAX,EPSR(IE,ISIG) ) 
            YIMIN= MIN( YIMIN,EPSI(IE,ISIG) ) 
            YIMAX= MAX( YIMAX,EPSI(IE,ISIG) )
         END DO
         END DO  

         YTXT1 = '!xe!F!m{1}!S2 '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'!xaa!F '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 7 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N '
         LYTXT1 = LYTXT1 + 3
         YTXT0 = YTXT1(1:12)//'1'//YTXT1(14:LYTXT1)

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_eps_dia',
     &                  LL+8,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YRMIN, 1,YRMAX,1,
     &                  YIMIN, 1,YIMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT1,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'diagonal elements of !xe',24,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         LEG(1)= 'xx'
         LEG(2)= 'yy'
         LEG(3)= 'zz'
 
         CALL XMGRLEGEND( IFIL,NS,3,0,LEG,LEG )

c ------------------------------------------------- real part of EPSILON
         IS=-1
         DO ISIG=1,9,4
            IS=IS+1
            CALL XMGR4TABLE(0,IS,E,EPSR(1,ISIG),1.0,NE,IFIL)
         END DO

c -------------------------------------------- imaginary part of EPSILON
         IS=-1
         DO ISIG=1,9,4
            IS=IS+1
            CALL XMGR4TABLE(1,IS,E,EPSI(1,ISIG),1.0,NE,IFIL)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' EPS-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C                      off-diagonal elements of  EPSILON
C=======================================================================
         YRMIN = +1E+10
         YRMAX = -1E+10
         YIMIN = +1E+10
         YIMAX = -1E+10
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
         DO IE = 1,NE
            YRMIN= MIN( YRMIN,EPSR(IE,ISIG) ) 
            YRMAX= MAX( YRMAX,EPSR(IE,ISIG) ) 
            YIMIN= MIN( YIMIN,EPSI(IE,ISIG) ) 
            YIMAX= MAX( YIMAX,EPSI(IE,ISIG) )
         END DO
         END IF
         END DO  

         YTXT1 = '!xe!F!m{1}!S2 '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'!xab!F '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 7 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N '
         LYTXT1 = LYTXT1 + 3
         YTXT0 = YTXT1(1:12)//'1'//YTXT1(14:LYTXT1)

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_eps_off',
     &                  LL+8,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YRMIN, 1,YRMAX,1,
     &                  YIMIN, 1,YIMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT1,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'off-diagonal elements of !xe',28,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         LEG(1)= 'xy'
         LEG(2)= 'xz'
         LEG(3)= 'yx'
         LEG(4)= 'yz'
         LEG(5)= 'zx'
         LEG(6)= 'zy'
 
         CALL XMGRLEGEND( IFIL,NS,6,0,LEG,LEG )
 
c ------------------------------------------------- real part of EPSILON
         IS=-1
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
            IS=IS+1
            CALL XMGR4TABLE(0,IS,E,EPSR(1,ISIG),1.0,NE,IFIL)
         END IF
         END DO

c -------------------------------------------- imaginary part of EPSILON
         IS=-1
         DO ISIG=2,8
         IF( ISIG .NE. 5 ) THEN 
            IS=IS+1
            CALL XMGR4TABLE(1,IS,E,EPSI(1,ISIG),1.0,NE,IFIL)
         END IF
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' EPS-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C=======================================================================
C                   reflectivity and asymmetry
C=======================================================================
 
C=======================================================================
         DO I = 1,NE
             DO ISIG=1,9
                 CEPS(I,ISIG) = DCMPLX(EPSR(I,ISIG),EPSI(I,ISIG))  
             END DO
         END DO

         DO ITET=1,NTET 
             XTET = TET(ITET)*PI/180D0
             DO I = 1,NE
                 CEPSXX=CEPS(I,1)
                 CEPSXY=CEPS(I,4)
                 CBETA=SQRT(1.0D0-(COS(XTET))**2/CEPSXX)
                 CWORK1=( SQRT(CEPSXX)*SIN(XTET)-CBETA) /
     &                  ( SQRT(CEPSXX)*SIN(XTET)+CBETA)
                 CWORK2=(CEPSXY*SIN(2*XTET)/CEPSXX) /
     &                  (CEPSXX*(SIN(XTET))**2-CBETA**2)

                 CWORK3=CWORK1*(1.0D0+CWORK2)
                 CWORK4=CWORK1*(1.0D0-CWORK2)

                 RWORK1=REAL(ABS(CWORK3)**2)
                 RWORK2=REAL(ABS(CWORK4)**2)

                 REFL(I,ITET) = (RWORK1+RWORK2)/2
                 ASYM(I,ITET) = (RWORK1-RWORK2)/(RWORK1+RWORK2)
             END DO
         END DO

         YRMIN = +1E+10
         YRMAX = -1E+10
         YAMIN = +1E+10
         YAMAX = -1E+10
C
         DO ITET=1,NTET 
            DO IE = 1,NE
               YRMIN= MIN( YRMIN,REFL(IE,ITET) ) 
               YRMAX= MAX( YRMAX,REFL(IE,ITET) ) 
               YAMIN= MIN( YAMIN,ASYM(IE,ITET) ) 
               YAMAX= MAX( YAMAX,ASYM(IE,ITET) )
            END DO
         END DO

         YTXT1 = '!xq!F!m{1}!S '//SUP(1:LSUP)//
     &           '!M{1}!N!s'//'K '//SDN(1:LSDN)
         LYTXT1 = 14 + LSUP + 9 + 2 + LSDN
         YTXT1  = YTXT1(1:LYTXT1)//'!N  (!So!N)'
         LYTXT1 = LYTXT1 + 11
         I = 13 + LSUP + 9
         YTXT0 = YTXT1(1:I)//'F'//YTXT1((I+2):(LYTXT1-1))//'/nm)'
         LYTXT0 = LYTXT1 + 3

         LL=LSTR80T(IT,K)
         CALL XMGR4HEAD( ' ',0,STR80T(IT,K)(1:LL)//'_refl',
     &                  LL+5,' ',0,
     &                  FILNAM,80,LFILNAM,IFIL,2, 
     &                  XMIN,  0,XMAX,1,
     &                  YRMIN, 1,YRMAX,1,
     &                  YAMIN, 1,YAMAX,1,
     &                  'energy (eV)',11,
     &                  YTXT0,LYTXT0,YTXT1,LYTXT1,
     &                  HEADT(IT,K),LHEADT(IT,K),
     &                  'reflectivity and asymmetry',26,KNOHEADER)

         CALL XMGRCURVES( IFIL,2,1,1,2,0,0 )

         DO ITET=1,NTET
            LEG(ITET) = '!xq!F='//STET(ITET)(1:LSTET(ITET))//'!So!N'
         END DO
         CALL XMGRLEGEND( IFIL,NS,NTET,NTET,LEG,LEG )
 
c --------------------------------------------------------- reflectivity
         DO ITET=1,NTET 
            CALL XMGR4TABLE(0,ITET-1,E,REFL(1,ITET),1.0,NE,IFIL)
         END DO

c ------------------------------------------------------------ asymmetry
         DO ITET=1,NTET 
            CALL XMGR4TABLE(1,ITET-1,E,ASYM(1,ITET),1.0,NE,IFIL)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' reflectivity and asymmetry written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

C ======================================================================




c ----------------------------------------------------------------------
c                      tabulate spectra if required
c ----------------------------------------------------------------------
         IF( KTABLE ) THEN

            IF( NKTYPEFF .GT. 1 ) THEN
               STR10 = 'xas_'//TXTT(IT)(1:2)//'_'//CHAR(ICHAR('1')+K-1)
            ELSE
               STR10 = 'xas_'//TXTT(IT)(1:2)
            END IF
            OPEN (90, FILE=STR10, STATUS='UNKNOWN' )
            IF( MERGE .EQ. 1 ) THEN
               DO IE=1,NE
                  DO KK=1,NKTYP
                     RAV(KK) = +0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                     RDF(KK) = -0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                  END DO
                  WRITE(90,'(20F15.8)') E(IE),
     &                RAV(1),RDF(1)
c    &                RAV(1),RDF(1), RAV(1)-RAV(2), RDF(1)-RDF(2),
c    &                RAV(2),RDF(2)
               END DO
            ELSE
               DO IE=1,NE 
                  WRITE(90,'(20F15.8)') E(IE),
     &                          +0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL),
     &                          -0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL)
               END DO
            END IF
            CLOSE (90)
            WRITE(*,*) '  '
            WRITE(*,*) ' X-MOKE-SPECTRUM tabulated in ', 
     &               STR10,'  IT = ',IT,' ',TXTT(IT)
            WRITE(*,*) '  '

         END IF
c ----------------------------------------------------------------------


      END DO  
      END DO  
C ======================================================================



      IF( NTXRSGRP .EQ. 1 ) RETURN

      W0  = 0.0
      DO ITXG=1,NTXRSGRP
         W0 = W0 + NQT(ITXG)*CONC(ITXG)
      END DO
      W0 = MAX( 1.0, W0 )
C ======================================================================
C              COMPARE SPECTRA OF THE VARIOUS ATOM TYPES IT 
C ======================================================================

      DO K=1,NKTYPEFF

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         IF( NC .EQ. 1 ) THEN
            EXT='   '
            LHEADER= 1
         ELSE
            EXT=               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'  '
            LHEADER= 2
         END IF
         IF( MERGE .EQ. 1 ) THEN 
            EXT=EXT(1:1)//','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
            LHEADER= 4
         END IF
         HEADER = SHELL(NC)//EXT
         LHEADER0 = LHEADER
         IF( (80 - (LHEADER + 1 + 3 + 12 + 2 + 4) ) .LT. LSYSTEM ) THEN
             LL = 80 - (LHEADER + 1 + 3 + 12 + 2 + 4) 
         ELSE 
             LL = LSYSTEM
         END IF
         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LL)
         LHEADER = LHEADER + 1 + 3 + 12 + 2 + 4 + LL
        
         IF( LDATASET .GT. 0 ) THEN
             LL = INDEX( DATASET, '.rate' ) - 1
             IF( LL .LE. 0 ) LL = INDEX( DATASET, '_rate' ) - 1
         ELSE
             LL = 0
         END IF

         IF( LL .GT. 0 ) THEN 
            I1 = LL - LHEADER0+1
            IF ( HEADER(1:LHEADER0) .EQ. DATASET(I1:LL) ) THEN
               FILNAM = DATASET(1:LL)//'_split.xmgr'
               LFILNAM = LL + 11
            ELSE
               FILNAM = DATASET(1:LL)//'_'//HEADER(1:LHEADER0)//
     &                  '_split.xmgr'
               LFILNAM = LL + 1 + LHEADER0 + 11
            END IF
         ELSE 
            FILNAM  = 'xas_'//TXTT(IT)(1:LTXTT(IT))
     &                //'_'//HEADER(1:LHEADER0)//'_split.xmgr'
            LFILNAM = 4 + LTXTT(IT) + 1 + LHEADER0 + 11
         END IF

         IFIL = 10 
         OPEN (IFIL, FILE=FILNAM(1:LFILNAM), STATUS='UNKNOWN' )

         STR80 = '\\8m\\0\\S'//TXTT(IT)(1:2)//'\\b\\b\\N\\s'//SHELL(NC)
         L80 = LNGSTRING( STR80, 80 )
         IF( SHELL(NC) .NE. 'K' ) THEN 
            STR80 = STR80(1:L80)//'\\s'//SUBSH(LCXRAY(IT))
            L80   = L80 + 5
         END IF         
         STR80 = STR80(1:L80)//'\\N (a.u.)'
         L80 = L80 + 9

c ---------------------------------------- polarisation averaged spectra
         YMIN = 0.0
         YMAX = NMAX(0)

         DY = 20.0
c        YMIN = FLOAT( INT(YMIN/DY) - 1 ) * DY
         YMAX = FLOAT( INT(YMAX/DY) + 1 ) * DY

         WRITE(IFIL,80000) 
         WRITE(IFIL,80100) 0,0
         WRITE(IFIL,80200) 0.5, 0.85
         WRITE(IFIL,80300) XMIN,XMAX, YMIN,YMAX, 
     &        'energy (eV)', DX,DX/2, 
     &         STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'
         WRITE(IFIL,80400) 'xaxis  ticklabel off'

         IF( .NOT.KNOHEADER ) THEN
            WRITE(IFIL,80400) 'title "'//
     &      HEADER(1:LHEADER)//'" '
            WRITE(IFIL,80400) 'title font 0'
c           WRITE(IFIL,80400) 'subtitle "'//
c    &      'XAS-spectrum of '//TXTT(IT)(1:LTXTT(IT))//
c    &       ' in '//SYSTEM(1:LSYSTEM)//'" '
c           WRITE(IFIL,80400) 'subtitle font 0'
         END IF

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 0,0
            WRITE(IFIL,80800) (E(IE), 
     &        (0.5*RD(IE,0,K,1)+0.5*RD(IE,ITXG,K,NPOL) ), IE=1,NE)
            WRITE(IFIL,80900)
         END DO

c --------------------------------------------------- difference spectra
         DMIN= +1E+10
         DMAX= -1E+10
         DO IE = 1,NE
            DEL =          RD(IE,0,K,NPOL)-RD(IE,0,K,1)
            DMIN= MIN( DMIN, DEL )
            DMAX= MAX( DMAX, DEL )
         END DO  
         DMIN= 1.03 * DMIN / 2.0                             
         DMAX= 1.03 * DMAX / 2.0 

         IF( LCXRAY(1) .EQ. 0 ) THEN
            DY = 0.5
         ELSE
            DY = 4.0
         END IF
         DMIN = FLOAT( INT(DMIN/DY) - 1 ) * DY
         DMAX = FLOAT( INT(DMAX/DY) + 1 ) * DY - 0.001

         WRITE(IFIL,80100) 1,1
         WRITE(IFIL,80200) 0.15, 0.5
         WRITE(IFIL,80300) XMIN,XMAX, DMIN,DMAX,
     &        'energy (eV)',     DX,DX/2,
     &        '\\8D'//STR80(1:L80), DY,DY
         WRITE(IFIL,80400) 's0 linewidth 2'

         DO ITXG=1,NTXRSGRP
            IF( ITXG .LT. 10 ) THEN
               WRITE(IFIL,80500) ITXG, ITXG
            ELSE
               WRITE(IFIL,80600) ITXG, ITXG
            END IF
         END DO

         DO ITXG=1,NTXRSGRP
            WRITE(IFIL,80700) 1,1
            WRITE(IFIL,80800) (E(IE),
     &        (-0.5*RD(IE,0,K,1)+0.5*RD(IE,ITXG,K,NPOL) ), IE=1,NE)
            WRITE(IFIL,80900)
         END DO

         CLOSE( IFIL )

         WRITE(*,*) '  '
         WRITE(*,*) ' X-MOKE-SPECTRUM written to ', 
     &               FILNAM(1:LFILNAM)
         WRITE(*,*) '  '

      END DO  
C ======================================================================




 9100 FORMAT(A,5I4)
80000 FORMAT('# ACE/gr parameter file',/,'#',/,'#',/,'#',/,
     &'@default font 0',/,'@default font source 0')
80100 FORMAT('@with g',I1,/,'@g',I1,' on')
80200 FORMAT(
     &'@    view xmin 0.150',/,
     &'@    view xmax 0.850',/,
     &'@    view ymin ',F7.3,/,
     &'@    view ymax ',F7.3,/,
     &'@    xaxis  label font 0',/,
     &'@    xaxis  ticklabel font 0',/,
     &'@    yaxis  label font 0',/,
     &'@    yaxis  ticklabel font 0' )

80300 FORMAT(
     &'@    world xmin ',F9.5,/,'@    world xmax ',F9.5,/,
     &'@    world ymin ',F9.5,/,'@    world ymax ',F9.5,/,
     &'@    zeroxaxis  bar on',/,
     &'@    xaxis  label "',A,'"',/,
     &'@    xaxis  tick on',/,
     &'@    xaxis  tick major ',F7.3,/,
     &'@    xaxis  tick minor ',F7.3,/,
     &'@    yaxis  label "',A,'"',/,
     &'@    yaxis  tick on',/,
     &'@    yaxis  tick major ',F7.3,/,
     &'@    yaxis  tick minor ',F7.3)
80400 FORMAT('@    ',A) 
80500 FORMAT('@    s',I1,' color ',I2)
80600 FORMAT('@    s',I2,' color ',I2)
80700 FORMAT('@WITH G',I1,/,'@G',I1,' ON',/,'@TYPE xy')
80800 FORMAT(2E15.7)
80900 FORMAT('&')
81000 FORMAT('@    s',I1,' linestyle ',I1)
81100 FORMAT('@    s',I2,' linestyle ',I1)
91001 FORMAT(/,5X,'WARNING: ',/,5X,'VUC not supplied',/,
     &         5X,'-- no correction for EPSILON',/,
     &         5X,'-- no Kerr spectra ',/)
91002    FORMAT(/,5X,'correction for EPSILON based on:'/,
     &         5X,'VUC     = ',F10.3,/,
     &         5X,'Z       = ',I10,/,
     &         5X,'NAT     = ',I10,/,
     &         5X,'CONC    = ',F10.3,/, 
     &         5X,'r_e     = ',E12.5,/)
    
      RETURN
      END   
