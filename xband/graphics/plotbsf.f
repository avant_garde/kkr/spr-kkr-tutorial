C*==plotbsf.f    processed by SPAG 6.05Rc at 14:14 on 18 Mar 2004
      SUBROUTINE PLOTBSF(MODE,CUTOFF,MTV,EFRYD,
     &                   IFIL,KW,NX,NY,ERYD,DATASET,LDATASET,LSTRMAX,
     &                   NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,NLQ,NLT,
     &                   NMIN,NMAX,IQOFT,NQT,CONC,TXTT,LTXTT,INFO,LINFO,
     &                   HEADER,LHEADER,SYSTEM,LSYSTEM,E,Y,RYOVEV,
     &                   EREFER,EUNIT,EMIN,EMAX,DE,DY,NQ,NT,NE,IREL,EF)
C **********************************************************************
C *                                                                    *
C *  plot   Bloch spectral function  A(k,E)                            *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NKDIRMAX
      PARAMETER (NKDIRMAX=20)
C
C Dummy arguments
C
      REAL CUTOFF,DE,DY,EF,EFRYD,EMAX,EMIN,EREFER,ERYD,RYOVEV
      CHARACTER*(*) DATASET
      LOGICAL MTV,GNUPLOT
      CHARACTER*1 BSLASH
      CHARACTER*2 EUNIT
      CHARACTER*80 HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSTRMAX,LSYSTEM,MODE,
     &        NCMAX,NE,NEMAX,NLMAX,NQ,NQMAX,NSPMAX,NT,NTMAX,NX,NY
      CHARACTER*10 KW
      REAL CONC(NTMAX),E(NEMAX),NMAX(0:NTMAX),NMIN(0:NTMAX),Y(NEMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      REAL AKE(:,:,:,:),AKQS(:,:,:),ERYDR,KP(:),RYDI,VECK1(3),ENE(:),
     &     VECK2(3),XMAX,XMIN,YMAX,YMIN,ZMAX,ZMIN,KPY(:)
      CHARACTER*4 CDSP
      DOUBLE PRECISION DBLE
      CHARACTER*80 FILENAME,IDENT1,IDENT2,SUBTITLE,TITLE,SCRIPTNAME
      INTEGER I,IK,INDKDIR(:),IOUT,IQ,IS,IX,IY,KXMAX,KXMIN,KYMAX,KYMIN,
     &        KZMAX,KZMIN,LFN,LFNMAX,LIDENT1,LIDENT2,LSRDLAB(8),
     &        LSUBTITLE,LTITLE,LXLABEL,LYLABEL,NK1,NK2,NKDIR,NKMAX,
     &        NTROUBLE,ISCRIPT
      CHARACTER*8 LBLKDIR(NKDIRMAX)
      CHARACTER*25 SRDLAB(8)
      CHARACTER*10 STR10,TXTSPN(3)
      CHARACTER*30 XLABEL,YLABEL
      REAL*8 ZMAT(:,:)

      ALLOCATABLE AKQS,ZMAT,KP,AKE,INDKDIR,ENE,KPY
      ALLOCATE (AKQS(NX,NQ,3),ZMAT(NX,NY),KP(NX),AKE(NX,NY,3,0:NQ))
      ALLOCATE (KPY(NY))
      ALLOCATE (INDKDIR(NY))
      ALLOCATE (ENE(NE))
C
C*** End of declarations rewritten by SPAG
C
      GNUPLOT=.TRUE.
C     set BSLASH to single backslash character
      CALL ENSUREBSLASH(BSLASH,'\\')

C
C init key to fix (0) or float (1) for x/y/z axis label
C
      KXMIN = 0
C
      KXMAX = 0
      KYMIN = 0
      KYMAX = 0
      KZMIN = 0
      KZMAX = 0
C
      TXTSPN(1) = 'spin up   '
      TXTSPN(2) = 'spin down '
      TXTSPN(3) = 'total     '
C
      IDENT2 = ''
      LIDENT2 = 0
C
      LFNMAX = 80
      ICOLOR = 7
C
      DO IS = 1,2
         DO IY = 1,NY
            DO IX = 1,NX
               AKE(IX,IY,IS,0:NQ) = 0.0
            END DO
         END DO
      END DO
C
C -------------------------------------------------------------- read in
C
C-----------------------------------------------------------------------
C                                                               E-k-plot
      IF ( MODE.EQ.1 ) THEN
         READ (IFIL,99005)
         READ (IFIL,99008) EMIN,EMAX
         READ (IFIL,99005)
         READ (IFIL,99005) STR10,NKDIR
         DO I = 1,NKDIR
            READ (IFIL,99006) LBLKDIR(I)
         END DO
         READ (IFIL,99005)
         DO I = 1,NKDIR
            READ (IFIL,99005) STR10,INDKDIR(I)
         END DO
         READ (IFIL,99005)
         READ (IFIL,99005) STR10,NK2
         IF ( NK2.GT.NX ) STOP 'in <PLOTBSF>:   NK2 > NX'
         DO IK = 1,NK2
            READ (IFIL,99007) KP(IK)
         END DO
         READ (IFIL,99005)
C
         WRITE (6,99003) EFRYD,EMIN,EMAX,NX,NY
C
C ryd -> eV
         EMIN = (EMIN-EFRYD)*13.656
         EMAX = (EMAX-EFRYD)*13.656
C
         XMIN = KP(1)
         XMAX = KP(NK2)
         YMIN = EMIN
         YMAX = EMAX
C         XLABEL = 'k (2 pi/a)'
C         LXLABEL = 10
         XLABEL = 'k (2 !gp!F/a)'
         LXLABEL = 13
C
         YLABEL = 'E (eV)'
         LYLABEL = 6
C
C-----------------------------------------------------------------------
C                                                               k-k-plot
      ELSE
C
         READ (IFIL,'(10X,2F10.5)') ERYDR,RYDI
         READ (IFIL,99005)
         READ (IFIL,99005) STR10,NK1
         READ (IFIL,99008) VECK1
         READ (IFIL,99005) STR10,NK2
         READ (IFIL,99008) VECK2
         READ (IFIL,99005) STR10
C
         WRITE (6,99004) EF,ERYDR,RYDI,NK1,VECK1,NK2,VECK2
C
         XMIN = 0.0
         XMAX = SQRT(VECK1(1)**2+VECK1(2)**2+VECK1(3)**2)
         YMIN = 0.0
         YMAX = SQRT(VECK2(1)**2+VECK2(2)**2+VECK2(3)**2)
         XLABEL = 'k1 (2pi/a)'
         LXLABEL = 10
         YLABEL = 'k2 (2pi/a)'
         LYLABEL = 10
C
      END IF
C
C ----------------------------------------------------------------------
C                    read BSF
C-----------------------------------------------------------------------
      NTROUBLE = 0
      DO IY = 1,NY
C     
          DO IS = 1,2
              DO IQ = 1,NQ
                  DO IX = 1,NX
                      READ (IFIL,99010,ERR=5) AKQS(IX,IQ,IS)
                      GOTO 10
 5                    CONTINUE
                      NTROUBLE = NTROUBLE + 1
                      AKQS(IX,IQ,IS) = 0.0
 10               END DO
              END DO
          END DO
C
          DO IS = 1,2
              DO IQ = 1,NQ
                  DO IX = 1,NX
                      AKE(IX,IY,IS,IQ) = AKQS(IX,IQ,IS)
                      AKE(IX,IY,IS,0) = AKE(IX,IY,IS,0) + AKQS(IX,IQ,IS)
                  END DO
              END DO
          END DO
C
          DO IX = 1,NX
              AKE(IX,IY,3,0) = AKE(IX,IY,1,0) + AKE(IX,IY,2,0)
              AKE(IX,IY,1,0) = MAX(0.0,AKE(IX,IY,1,0))
              AKE(IX,IY,2,0) = MAX(0.0,AKE(IX,IY,2,0))
          END DO
          DO IQ=1,NQ
             DO IX=1,NX
           
              AKE(IX,IY,3,IQ) = AKE(IX,IY,1,IQ) + AKE(IX,IY,2,IQ)
              END DO
          END DO
C     
      END DO
C
      WRITE (*,*) 'corrected data points',NTROUBLE
       IF ( CUTOFF .LE. 0.0 ) THEN
           WRITE (*,*) 'Maximum of z used '
      ELSE
          WRITE (*,*) 'cutoff for z-value',CUTOFF
      END IF
C ----------------------------------------------------------------------
      ZMIN = 0.0
      IF ( CUTOFF .GT. 0.0 ) THEN
          DO IS = 1,3
            DO IQ = 1,NQ
              DO IY = 1,NY
                  DO IX = 1,NX
                    AKE(IX,IY,IS,IQ) = MIN(AKE(IX,IY,IS,IQ)-ZMIN,CUTOFF)
                  END DO
              END DO
            END DO
          END DO
          ZMAX=CUTOFF
      ELSE
          ZMAX=-9999.0D0
          DO IS = 1,3
            DO IQ = 1,NQ
              DO IY = 1,NY
                  DO IX = 1,NX
                      ZMAX = MAX(AKE(IX,IY,IS,IQ),ZMAX)
                  END DO
              END DO
            END DO
          END DO
      END IF
      ZMIN= 1D10
      DO IS = 1,3
        DO IQ = 1,NQ
          DO IY = 1,NY
              DO IX = 1,NX
                  ZMIN = MIN(AKE(IX,IY,IS,IQ),ZMIN)
              END DO
          END DO
        END DO
      END DO

C
C ----------------------------------------------------------------------
C
C
      IF (MODE.EQ.1) THEN
      DO IQ=0,NQ

       IF ( IQ.LT.10 ) THEN
             WRITE (STR10,'(I1)') IQ
             IX=1
       ELSE
             WRITE (STR10,'(I2)') IQ
             IX=2
       END IF
       IF (IQ.EQ.0) THEN

          FILENAME = DATASET(1:LDATASET)//'_dn'//'_bsf1.gnu'
          OPEN (11,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_up'//'_bsf2.gnu'
          OPEN (12,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_tot'//'_bsf.gnu'
          OPEN (13,FILE=FILENAME)

      ELSE  IF (IQ.GT.0 ) THEN

         FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &        '_bsf1.gnu'
         OPEN (11,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &        '_bsf2.gnu'
         OPEN (12,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &        '_bsf.gnu'
         OPEN (13,FILE=FILENAME)

      END IF

      DO IS = 1,3
         IOUT = 10 + IS
         DENE=(EMAX-EMIN)/(NE-1)
         DO IE=1,NY
            ENE(IE)=EMIN+ (IE-1)*DENE
         END DO
         
         do IX = 1, NX
            DO IY=1,NY
               write (IOUT,'(3f25.10)') KP(IX),ENE(IY),AKE(IX,IY,IS,IQ)
            END DO
            write(IOUT,*)
         END DO
         CLOSE(IOUT)
       END DO
       END DO
       ELSE
       DO IQ=0,NQ
       IF ( IQ.LT.10 ) THEN
             WRITE (STR10,'(I1)') IQ
             IX=1
       ELSE
             WRITE (STR10,'(I2)') IQ
             IX=2
       END IF
       IF (IQ.EQ.0) THEN

          FILENAME = DATASET(1:LDATASET)//'_dn'//
     &         '_bsf1.gnu'
          OPEN (11,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_up'//'_bsf2.gnu'
          OPEN (12,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_tot'//'_bsf.gnu'
          OPEN (13,FILE=FILENAME)

       ELSE  IF (IQ.GT.0 ) THEN

          FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &         '_bsf1.gnu'
          OPEN (11,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &         '_bsf2.gnu'
          OPEN (12,FILE=FILENAME)
          FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//STR10(1:IX)//
     &         '_bsf.gnu'
          OPEN (13,FILE=FILENAME)
          
       END IF
      DO IS = 1,3
             IOUT = 10 + IS
             DY=(YMAX-YMIN)/(NY-1)
             DO IY=1,NY
                KPY(IY)=YMIN+(IY-1)*DY
             END DO
             DX=(XMAX-XMIN)/(NX-1)
             DO IX=1,NX
                KP(IX)=XMIN+(IX-1)*DX
             END DO
             do IX = 1, NX
                DO IY=1,NY
                   write (IOUT,'(3f25.10)') KP(IX),KPY(IY),AKE(IX,IY,
     &                  IS,IQ)
                END DO
                write(IOUT,*)
             END DO
             CLOSE(IOUT)
          END DO
         END DO
       END IF
       
C
       IF (GNUPLOT) THEN
           DO IS = 1,3
             DO IQ=0,NQ
C     
               
               TITLE = 'BSF for '//SYSTEM(1:LSYSTEM)//'    '//
     &              TXTSPN(IS)

               IF (IQ.EQ.0) THEN
                  IF ( IS.EQ.1 ) THEN
                     IDENT1 = '_dn_bsf1'
                     LIDENT1 = 8
                  ELSE IF ( IS.EQ.2 ) THEN
                     IDENT1 = '_up_bsf2'
                     LIDENT1 = 8
                  ELSE
                     IDENT1 = '_tot_bsf'
                     LIDENT1 = 8
                  END IF
                  FILENAME = DATASET(1:LDATASET)//IDENT1(1:LIDENT1)//
     &                 '.gnu'
                  
                  SCRIPTNAME = DATASET(1:LDATASET)//
     &                 IDENT1(1:LIDENT1)//'.gp'

               ELSE 
                  IF ( IS.EQ.1 ) THEN
                     IDENT1 = '_bsf1'
                     LIDENT1 = 5
                  ELSE IF ( IS.EQ.2 ) THEN
                     IDENT1 = '_bsf2'
                     LIDENT1 = 5
                  ELSE
                     IDENT1 = '_bsf'
                     LIDENT1 = 4
                  END IF
                  IF ( IQ.LT.10 ) THEN
                     WRITE (STR10,'(I1)') IQ
                     IX=1
                  ELSE
                     WRITE (STR10,'(I2)') IQ
                     IX=2
                  END IF
                  FILENAME = DATASET(1:LDATASET)//'_IQEFF_'//
     &                    STR10(1:IX)//IDENT1(1:LIDENT1)//'.gnu'
               
                  SCRIPTNAME = DATASET(1:LDATASET)//'_IQEFF_'//
     &                 STR10(1:IX)//IDENT1(1:LIDENT1)//'.gp'
               
               END IF

               ISCRIPT=102
               OPEN (UNIT=ISCRIPT,FILE=SCRIPTNAME(1:LEN_TRIM(SCRIPTNAME)
     &              ))
               
               CALL CREATEGNUPLOTBSF(ISCRIPT,FILENAME,NKDIR,KP,NK2
     &              ,LBLKDIR,INDKDIR,EMIN,EMAX,TITLE,ZMIN,ZMAX)
               
               CLOSE (ISCRIPT)
             END DO
           END DO
         END IF
         IF ( MTV ) THEN
         FILENAME = DATASET(1:LDATASET)//'_bsf1.mtv'
         OPEN (11,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_bsf2.mtv'
         OPEN (12,FILE=FILENAME)
         FILENAME = DATASET(1:LDATASET)//'_bsf.mtv'
         OPEN (13,FILE=FILENAME)
         DO IS = 1,3
            IOUT = 10 + IS
            WRITE (IOUT,99001) XMIN,XMAX,YMIN,YMAX,NX,NY,
     &                         XLABEL(1:LXLABEL),YLABEL(1:LYLABEL),
     &                         SYSTEM(1:LSYSTEM),TXTSPN(IS)
C
            IF ( IS.LT.3 ) THEN
             WRITE (IOUT,'(7e13.5)') ((AKE(IX,IY,IS,0),IX=1,NX),IY=1,NY)
            ELSE
             WRITE (IOUT,'(7e13.5)') ((AKE(IX,IY,3,0),IX=1,NX),IY=1,NY)
            END IF
            WRITE (IOUT,99002)
         END DO
C
         WRITE (*,*) ' '
         WRITE (*,*) 
     &              ' Bloch spectral function written to graphics files'
         WRITE (*,*) ' use   plotmtv   for viewing:'
         WRITE (*,*) ' '
         WRITE (*,99011) ' plotmtv ',DATASET(1:LDATASET),'_bsf1.mtv'
         WRITE (*,99011) ' plotmtv ',DATASET(1:LDATASET),'_bsf2.mtv'
         WRITE (*,99011) ' plotmtv ',DATASET(1:LDATASET),'_bsf.mtv'
         WRITE (*,*) ' '
         WRITE (*,*) ' '
         END IF
C
C
      RETURN
C
99001 FORMAT ('$ DATA=CONTOUR',/,' ',/,'#',/,
     &        '#  file created by plotbsf',/,'#',/,' ',/,'% xmin=',
     &        F10.4,' xmax=',F10.4,/,'% ymin=',F10.4,' ymax=',F10.4,/,
     &        '% nx=',I4,' ny=',I4,/,'% xlabel = "',A,'" ',/,
     &        '% ylabel = "',A,'" ',/,
     &        '% toplabel = "Bloch spectral function  A_B(k,E)"',/,
     &        '% subtitle = "for ',A,4X,A,'" ',/,
     &        '% interp     = 3      # Rectangular interpolation',/,
     &        '% contfill   = on     # Contour filling',/,
     &        '% bitmap     = on')
99002 FORMAT ('$ END  ')
99003 FORMAT (/,1X,79('*'),/,24X,'Bloch spectral function  A(k,E)',/,1X,
     &        79('*'),//,10X,'E(k)-relation mode for: ',//,10X,
     &        'Fermi energy       ',F10.6,' Ry',/,10X,
     &        'energy range       ',F10.6,'   --- ',F10.6,' Ry',/,10X,
     &        'with  NE =',I5,'  NK =',I5,/)
C
99004 FORMAT (/,1X,79('*'),/,24X,'Bloch spectral function  A(k,E)',/,1X,
     &        79('*'),//,10X,'constant E mode for: ',//,10X,
     &        'Fermi energy       ',F10.6,' Ry',/,10X,
     &        'fixed energy       ',2F10.6,//,10X,'k-space parameters:',
     &        /,10X,'NK1 =',I5,'    K1 =',2X,'(',3F8.4,' )',/,10X,
     &        'NK2 =',I5,'    K2 =',2X,'(',3F8.4,' )',/)
99005 FORMAT (A10,2I10)
99006 FORMAT (10X,A)
99007 FORMAT (8F10.4)
99008 FORMAT (10X,8F10.4)
99009 FORMAT (/,'# column array2d (1,',i5,',1,',i5,') of real scalar',/)
99010 FORMAT (e12.5)
99011 FORMAT (1X,A,A,A)
      END
