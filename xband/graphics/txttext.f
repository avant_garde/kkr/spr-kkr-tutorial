      SUBROUTINE TXTTEXT( TXT_T,LTXT_T, NT, NTMAX )
c **********************************************************************
c *                                                                    *
c *     add an extension to  TXTT  if there are more types with same Z *
c *                                                                    *
c **********************************************************************
      INTEGER      LTXT_T(NTMAX), KDONE(NTMAX)
      CHARACTER*10 TXT_T(NTMAX)
C
C*** End of declarations rewritten by SPAG
C
      ICAL = ICHAR('a')
      ICAU = ICHAR('A')
      ICZL = ICHAR('z')
      ICZU = ICHAR('Z')
C
C----------------------------------- reset type names to chemical symbol
      DO IT = 1,NT
         TXT_T(IT) = TXT_T(IT)(1:2)//'  '
         IC2 = ICHAR(TXT_T(IT)(2:2))
C
         IF ( ((IC2.LT.ICAL) .OR. (IC2.GT.ICZL)) .AND. 
     &        ((IC2.LT.ICAU) .OR. (IC2.GT.ICZU)) ) THEN
            TXT_T(IT)(2:2) = ' '
            LTXT_T(IT) = 1
         ELSE
            LTXT_T(IT) = 2
         END IF
         KDONE(IT) = 0
      END DO
C
      DO IT = 1,NT
         I1 = LTXT_T(IT) + 1
         I2 = I1 + 1
         IEXT = 1
         IF ( KDONE(IT).NE.1 ) THEN
            DO JT = 1,NT
               IF ( (IT.NE.JT) .AND.
     &              (TXT_T(IT)(1:LTXT_T(IT))
     &              .EQ.TXT_T(JT)(1:LTXT_T(JT)))) THEN
                  TXT_T(IT)(I1:I2) = '_1'
                  IEXT = IEXT + 1
                  TXT_T(JT)(1:I1) = TXT_T(IT)(1:I1)
                  LTXT_T(JT) = I1
                  CALL ADDNTOSTR(TXT_T(JT),IEXT,LTXT_T(JT))
                  KDONE(JT) = 1
               END IF
            END DO
         END IF
C
         IF ( IEXT.NE.1 ) LTXT_T(IT) = LTXT_T(IT) + 2
         KDONE(IT) = 1
C
      END DO
      
C
      END
