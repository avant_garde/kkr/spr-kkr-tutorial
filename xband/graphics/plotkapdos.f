C*==plotkapdos.f    processed by SPAG 6.05Rc at 08:35 on 25 Sep 2021
      SUBROUTINE PLOTKAPDOS(IFIL,DOSL,DOSK,DATASET,LDATASET,NTMAX,NEMAX,
     &                      NCMAX,NSPMAX,NLT,DOSLMAX,TXTT,LTXTT,HEADER,
     &                      LHEADER,SYSTEM,LSYSTEM,E,RYOVEV,EREFER,
     &                      EUNIT,EMIN,EMAX,NT,NE,EF,KNOHEADER)
C **********************************************************************
C *                                                                    *
C *  plot kappa-resolved density of states                             *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL (A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX
      PARAMETER (NLEGMAX=50)
C
C Dummy arguments
C
      CHARACTER*(*) DATASET
      REAL EMAX,EMIN,EREFER,RYOVEV
      CHARACTER*2 EUNIT
      CHARACTER*80 HEADER,SYSTEM
      INTEGER IFIL,LDATASET,LHEADER,LSYSTEM,NCMAX,NE,NEMAX,NSPMAX,NT,
     &        NTMAX
      LOGICAL KNOHEADER
      REAL DOSK(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),
     &     DOSL(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),DOSLMAX(0:NTMAX),
     &     E(NEMAX)
      INTEGER LTXTT(NTMAX),NLT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      CHARACTER CHAR
      REAL DOSKMAX(:),WGT,XMAX,XMIN,YMAX
      CHARACTER*80 FILNAM,YTXT1,YTXT2
      INTEGER I0,IE,IK,IL,IT,LFILNAM,LS,LYTXT1,LYTXT2,NCK,NCL,NG,
     &        NKT(:)
      INTEGER ICHAR
      CHARACTER*20 LEGK(NLEGMAX),LEGL(NLEGMAX),STR20

      ALLOCATABLE NKT,DOSKMAX
      ALLOCATE (NKT(NTMAX),DOSKMAX(0:NTMAX))
C
C*** End of declarations rewritten by SPAG
C
C ----------------------------------------------------------------------
C
      DO IT = 1,NT
         NKT(IT) = 2*NLT(IT) - 1
         IF ( NKT(IT).GT.NCMAX ) STOP 'in <PLOTKAPDOS>:   NKT > NCMAX'
      END DO
C
      DO IE = 1,NE
         READ (IFIL,99001,END=100) E(IE),
     &                             ((DOSL(IE,IT,IL,1),IL=1,NLT(IT)),
     &                             (DOSK(IE,IT,IK,1),IK=1,NKT(IT)),IT=1,
     &                             NT)
         NE = IE
      END DO
 100  CONTINUE
      WRITE (*,'(A,I10)') ' number of energies read:  ',NE
      WRITE (*,'(A,2F10.4)') ' boundaries             :  ',E(1),E(NE)
C
C ----------------------------------------------------------------------
C                      READING IN FINISHED
C ----------------------------------------------------------------------
C
      IF ( EUNIT.EQ.'eV' ) THEN
         DO IT = 1,NT
            DO IL = 1,NLT(IT)
               DO IE = 1,NE
                  DOSL(IE,IT,IL,1) = DOSL(IE,IT,IL,1)/RYOVEV
               END DO
            END DO
            DO IK = 1,NKT(IT)
               DO IE = 1,NE
                  DOSK(IE,IT,IK,1) = DOSK(IE,IT,IK,1)/RYOVEV
               END DO
            END DO
         END DO
         DO IE = 1,NE
            E(IE) = E(IE)*RYOVEV
         END DO
      END IF
      DO IE = 1,NE
         E(IE) = E(IE) - EREFER
      END DO
C ----------------------------------------------------------------------
C
      DO IT = 1,NT
         DOSLMAX(IT) = 0.0
         DO IL = 1,NLT(IT)
            DO IE = 1,NE
               DOSLMAX(IT) = MAX(DOSLMAX(IT),DOSL(IE,IT,IL,1))
            END DO
         END DO
C
         DOSKMAX(IT) = 0.0
         DO IK = 1,NKT(IT)
            DO IE = 1,NE
               DOSKMAX(IT) = MAX(DOSKMAX(IT),DOSK(IE,IT,IK,1))
            END DO
         END DO
      END DO
C
C ======================================================================
C                              XMGRACE - output
C ======================================================================
      IF ( ABS(EMIN-9999.).LT.0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF ( ABS(EMAX-9999.).LT.0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------
      IFIL = 90
      NG = 2
C ----------------------------------------------------------------------
      DO IT = 1,NT
C
         YMAX = DOSLMAX(IT)
C
         STR20 = TXTT(IT)(1:LTXTT(IT))//'!N(E)  (sts./eV)'
         LS = LTXTT(IT) + 16
         YTXT1 = 'n!m{1}!Sl!M{1}!N!s'//STR20(1:LS)
         LYTXT1 = 18 + LS
         YTXT2 = 'n!m{1}!S!xk!0!M{1}!N!s'//STR20(1:LS)
         LYTXT2 = 22 + LS
C
         CALL XMGR4HEAD(DATASET,LDATASET,'kapdos',6,TXTT(IT),LTXTT(IT),
     &                  FILNAM,80,LFILNAM,IFIL,NG,XMIN,1,XMAX,1,0.0,0,
     &                  YMAX,1,0.0,0,YMAX,1,'energy (eV)',11,YTXT1,
     &                  LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &                  '!xk!0- and l-resolved DOS of '//
     &                  TXTT(IT)(1:LTXTT(IT))//
     &                  ' in '//SYSTEM(1:LSYSTEM),
     &                  (29+LTXTT(IT)+4+LSYSTEM),KNOHEADER)
C
         NCL = MIN(NLEGMAX,NLT(IT))
         NCK = MIN(NLEGMAX,NKT(IT))
C
         LEGL(1) = 's'
         LEGL(2) = 'p'
         LEGL(3) = 'd'
         I0 = ICHAR('1') - 1
         DO IL = 4,NLT(IT)
            LEGL(IL) = CHAR(ICHAR('f')+IL-4)
         END DO
         LEGK(1) = 's!s1/2'
         IK = 1
         DO IL = 2,NLT(IT)
            IK = IK + 1
            LEGK(IK) = LEGL(IL)(1:1)//'!s'//CHAR(I0+2*IL-3)//'/2'
            IK = IK + 1
            LEGK(IK) = LEGL(IL)(1:1)//'!s'//CHAR(I0+2*IL-1)//'/2'
         END DO
C
         CALL XMGRLEG1(IFIL,0,NCL,LEGL,0.18D0,0.49D0)
         CALL XMGRLEG1(IFIL,1,NCK,LEGK,0.18D0,0.84D0)
C
         CALL XMGRCURVES(IFIL,NG,NCL,NCK,2,1,0)
C
         WGT = 1.0
C
         DO IL = 1,NLT(IT)
            CALL XMGR4TABLE(0,(IL-1),E,DOSL(1,IT,IL,1),WGT,NE,IFIL)
         END DO
C
         DO IK = 1,NKT(IT)
            CALL XMGR4TABLE(1,(IK-1),E,DOSK(1,IT,IK,1),WGT,NE,IFIL)
         END DO
C
         WRITE (6,*) '  '
         WRITE (6,*) '   DOS written to file  ',FILNAM(1:LFILNAM)
         CLOSE (IFIL)
C
      END DO
C ----------------------------------------------------------------------
      RETURN
99001 FORMAT (10E12.5)
      END
