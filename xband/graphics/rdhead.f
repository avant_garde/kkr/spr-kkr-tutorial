C*==rdhead.f    processed by SPAG 6.05Rc at 21:11 on 30 Oct 2001
      SUBROUTINE RDHEAD(IFIL,N,NW,NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLQ,
     &                  NLT,NMIN,NMAX,IQOFT,NQT,CONC,TXTT,LTXTT,INFO,
     &                  LINFO,HEADER,LHEADER,SYSTEM,LSYSTEM,NQ,NT,NE,
     &                  IREL,EF)
C **********************************************************************
C *                                                                    *
C *       read the standard head of the input data file                *
C *                                                                    *
C **********************************************************************
C
C*** Start of declarations rewritten by SPAG
C
C COMMON variables
C
      REAL EREFER,RYOVEV
      CHARACTER*2 EUNIT
      COMMON /ENERGY/ RYOVEV,EREFER,EUNIT
C
C Dummy arguments
C
      REAL EF
      CHARACTER*80 HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LHEADER,LINFO,LSYSTEM,NCMAX,NE,NEMAX,NQ,NQMAX,
     &        NSPMAX,NT,NTMAX
      REAL CONC(NTMAX),N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),NMAX(0:NTMAX),
     &     NMIN(0:NTMAX),NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX),str10
C
C Local variables
C
      INTEGER IC,IE,IQ,IQP,IQT,IS,IT,ITP
      CHARACTER*80 LINE
      INTEGER LNGSTRING
C
C*** End of declarations rewritten by SPAG
C
      DO IS = 0,NSPMAX
         DO IC = 0,NCMAX
            DO IT = 0,NTMAX
               NMIN(IT) = +10E6
               NMAX(IT) = -10E6
               DO IE = 1,NEMAX
                  N(IE,IT,IC,IS) = 0.0
                  NW(IE,IT,IC,IS) = 0.0
               END DO
            END DO
         END DO
      END DO
C
      READ (IFIL,99001) HEADER
      READ (IFIL,99001) SYSTEM
      READ (IFIL,*) str10, NQ
      READ (IFIL,*) str10,NT
      READ (IFIL,*) 
      READ (IFIL,*) str10, NE
      READ (IFIL,*) str10, IREL
      READ (IFIL,*) str10, EF
      READ (IFIL,99001) INFO
      READ (IFIL,99005)
C
C ----------------------------------------------------------------------
      READ (IFIL,99001)
C
      OPEN (88,STATUS='SCRATCH')
C
      DO IQP = 1,NQ
         READ (IFIL,'(A)') LINE
         REWIND 88
         WRITE (88,'(A)') LINE(2:80)
         REWIND 88
         READ (88,*) IQ,NLQ(IQP)
C
      END DO
C ----------------------------------------------------------------------
      READ (IFIL,99001)
      DO ITP = 1,NT
         READ (IFIL,*) IT,TXTT(ITP),CONC(ITP),NQT(ITP),
     &                     (IQOFT(IQT,ITP),IQT=1,NQT(ITP))
         NLT(ITP) = NLQ(IQOFT(1,ITP))
      END DO
C
      CALL TXTTEXT(TXTT,LTXTT,NT,NTMAX)
C
      LHEADER = LNGSTRING(HEADER,80)
      LINFO = LNGSTRING(INFO,80)
      LSYSTEM = LNGSTRING(SYSTEM,80)
C
      CALL XMGRTEXT(HEADER,LHEADER,80)
      CALL XMGRTEXT(SYSTEM,LSYSTEM,80)
C
      IF ( EUNIT.EQ.'eV' ) THEN
         EF = EF*RYOVEV
      ELSE
         EUNIT = 'Ry'
      END IF
      IF ( ABS(EREFER-9999.).LT.1.0 ) THEN
         EREFER = EF
         EF = EF - EREFER
      END IF
C
      WRITE (6,*) ' HEADER:     ',HEADER(1:LHEADER)
      WRITE (6,*) ' SYSTEM:     ',SYSTEM(1:LSYSTEM)
      WRITE (*,*) '<RDHEAD>  passed '
C
99001 FORMAT (10X,A80)
99002 FORMAT (10X,I10)
99003 FORMAT (10X,F10.5)
99004 FORMAT (1X,I4,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99005 FORMAT (10X,A10)
      END
