# 1 "plotsprvbxps.f"
C*==plotsprvbarp.f    processed by SPAG 6.05Rc at 09:53 on 25 Jun 2004
      SUBROUTINE PLOTSPRVBXPS(IFIL,DATASET,LDATASET,NQMAX,NTMAX,NEMAX,
     &     NCMAX,NPOLMAX,NLMAX,SPEC,NLQ,NLT,NMIN,
     &     NMAX,WMIN,WMAX,IQOFT,NQT,CONC,TXTT,LTXTT,
     &     INFO,LINFO,HEADER,LHEADER,SYSTEM,LSYSTEM,
     &     E,Y,NQ,NT,NS,NE,IREL,EF,
     &     KTABLE,NVMAX,NEXRSMAX,WGTGAUTAB,
     &     WLORTAB,WLORVBXPS,NCEXPMAX,
     &     NPOLEXPMAX)
C **********************************************************************
C *                                                                    *
C *  plot SPR-VB-XPS - spectra                                         *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL(A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX,NSMAX
      PARAMETER (NLEGMAX=80,NSMAX=1)

C
C COMMON variables
C
      REAL BLSEXP(2),EMAX,EMIN,EREFER,ESEXP(2),RYOVEV,SCLEXP(2),WGAUSS,
     &     WLOR(2)
      CHARACTER*2 EUNIT
      LOGICAL EXPDATAVL
      CHARACTER*80 FMTEXP
      CHARACTER*80 FILENAME,IDENT1,IDENT2,SUBTITLE,TITLE
      INTEGER MERGE,NCEXP,NEEXP(2),NPOLEXP
      COMMON /BRDPAR/ WGAUSS,WLOR
      COMMON /ENERGY/ RYOVEV,EREFER,EUNIT
      COMMON /EXPPAR/ ESEXP,BLSEXP,SCLEXP,NEEXP,NCEXP,NPOLEXP,FMTEXP,
     &                EXPDATAVL
      COMMON /PLOPAR/ EMIN,EMAX,MERGE
C
C Dummy arguments
C
      CHARACTER*80 DATASET,HEADER,INFO,SYSTEM,YTXT1,YTXT2
      REAL EF
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSYSTEM,NCEXPMAX,NCMAX,
     &        NE,NEMAX,NEXRSMAX,NLMAX,NPOLEXPMAX,NPOLMAX,NQ,NQMAX,NS,NT,
     &        NTMAX,NVMAX
      LOGICAL KTABLE
      CHARACTER*3 SPEC
      REAL CONC(NTMAX),E(NEMAX),
     &     NMAX(0:NTMAX),NMIN(0:NTMAX),WGTGAUTAB(0:NEMAX),
     &     WLORTAB(1:NEMAX),WLORVBXPS(2),WMAX(0:NTMAX),WMIN(0:NTMAX),
     &     Y(NEMAX),RDUMMY
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      CHARACTER*1 CHPOL(5)
      CHARACTER*80 EPHOTCHAR,NTEXT,FNOUT
      CHARACTER*20 LEG(NLEGMAX),LEG2(NLEGMAX)
      REAL EPHOTEV


          REAL POL(:,:,:,:),TOT(:,:,:,:),WORK(:)






      INTEGER I,IE,IEP,IK,IPOL,IT,LFN,LINEPHOT,LNTEXT,LOUTEPHOT,N0,NK2,
     &        NPOL
      INTEGER LNGSTRING,LNGZEROSTRING
      CHARACTER*3 MEFORM

      REAL IXRS(:,:),BXRS(:,:)

      CHARACTER*30 XLABEL,YLABEL
      CHARACTER*4 CDSP


      ALLOCATABLE TOT,POL,IXRS,BXRS,WORK

C
C
      READ (IFIL,99008) FILENAME
      LFN = LNGSTRING(FILENAME,80)
      READ (IFIL,99008) NTEXT
      NTEXT = 'I(XPS) (arb. units)'
      LNTEXT = 19
      READ (IFIL,99008) MEFORM
      READ (IFIL,*)
      READ (IFIL,99006) EPHOTEV
      READ (IFIL,99005) NPOL,(CHPOL(I),I=1,NPOL)
      READ (IFIL,99004) NE
C
C
      WRITE (EPHOTCHAR,'(f12.2)') EPHOTEV
      LINEPHOT = LNGZEROSTRING(EPHOTCHAR,80)
      LOUTEPHOT = LNGSTRING(EPHOTCHAR,80)
C
      IF ( LDATASET.EQ.0 ) THEN
         LDATASET = LFN
         DATASET = FILENAME
      END IF

      IF ( DATASET((LDATASET-3):LDATASET).EQ.'.xps' )
     &     LDATASET = LDATASET - 4
C
      WRITE (6,99013) '  '
      WRITE (6,99013) 'DATASET   ',DATASET(1:LDATASET)
      WRITE (6,99013) 'FILENAME  ',FILENAME(1:LFN)
      WRITE (6,99013) 'MEFORM    ',MEFORM
      WRITE (6,99012) 'E_ph (eV) ',EPHOTEV
      WRITE (6,99011) 'NPOL      ',NPOL,(CHPOL(I),I=1,NPOL)
      WRITE (6,99010) 'NE        ',NE
      

      ALLOCATE (TOT(NEMAX,0:NT,0:NSMAX,0:NPOLMAX))
      ALLOCATE (POL(NEMAX,0:NT,0:NSMAX,0:NPOLMAX))
      ALLOCATE (WORK(NEMAX))

C
C ------------------------------------------------------------------
C    SPIN DENSITY MATRIX VERSION
C    TOT-- total intensity
C    POL -- two different spin channels are written in index NSMAX (0,1)
C     it is done in this funny way, just to make use of broaden
C     subroutine

      DO IPOL=0,NPOLMAX
          DO IS=0,NSMAX
              DO IT=0,NT
                  DO IE=1,NE
                    TOT(IE,IT,IS,IPOL)=0.0  
                    POL(IE,IT,IS,IPOL)=0.0  
                  END DO
              END DO
          END DO
      END DO
      
      DO IE = 1,NE
          READ (IFIL,'(I5,10X,2F10.5)') IEP,E(IE)
              READ (IFIL,99014) (TOT(IE,0,0,IPOL),RDUMMY,
     &             IPOL=1,NPOL),
     &             (POL(IE,0,0,IPOL),POL(IE,0,1,IPOL),
     &             IPOL=1,NPOL)
              DO IT = 1,NT
                  READ (IFIL,99014) (TOT(IE,IT,0,IPOL),TOT(IE,IT,0
     &                 ,IPOL),IPOL=1,NPOL),(POL(IE,IT,0,IPOL),POL(IE,IT
     &                 ,1,IPOL),IPOL=1,NPOL)
              END DO
C    
      END DO
C     ------------------------------------------------------------------
C                  READING IN FINISHED
C     ------------------------------------------------------------------
C
C ------------------------------------------------ sum over polarization
      DO IT = 0,NT
          DO IP = 1,NPOL
              DO IE = 1,NE
                  TOT(IE,IT,0,0) = TOT(IE,IT,0,0) + TOT(IE,IT,0,IP)
     &                 /REAL(NPOL)
                  DO IK=0,1
                      POL(IE,IT,IK,0) = POL(IE,IT,IK,0) + POL(IE,IT,IK
     &                     ,IP)/REAL(NPOL)
                  END DO
              END DO
          END DO
      END DO
C
      EUNIT = 'eV'

      IF ( EUNIT.EQ.'eV' ) THEN
         DO IE = 1,NE
            E(IE) = E(IE)*RYOVEV
         END DO
      END IF
      DO IE = 1,NE
         E(IE) = E(IE) - EREFER
      END DO
C ----------------------------------------------------------------------
C

C     ------------------------------------------------------------------
C     extend E - mesh
C     ------------------------------------------------------------------
      WRITE (6,99015)
      WRITE (6,99012) 'E(min/max)',E(1),E(NE)
      WRITE (6,99010) 'NE  (old) ',NE
C
      DE = E(2) - E(1)
      NEADD = NINT(5.0/DE)
      IF( ((WLORVBXPS(1)+WLORVBXPS(2)) .LT. 0.001) .AND.
     &     (WGAUSS .LT. 0.001) ) NEADD=0
      IF (NEADD.GT.0) THEN
      DO IE = NE,1, - 1
         DO IT = 0,NT
               DO IP = 0,NPOLMAX
                  TOT(IE+NEADD,IT,0,IP) = TOT(IE,IT,0,IP)
                  POL(IE+NEADD,IT,0,IP) = POL(IE,IT,0,IP)
                  POL(IE+NEADD,IT,1,IP) = POL(IE,IT,1,IP)
                  E(IE+NEADD) = E(IE)
               END DO
         END DO
      END DO
C
      DO IE = 1,NEADD
          DO IT = 0,NT
              DO IP = 0,NPOLMAX
                  TOT(IE,IT,0,IP) = 0.0
                  POL(IE,IT,0,IP) = 0.0
                  POL(IE,IT,1,IP) = 0.0
                  E(IE) = E(NEADD+1) - (NEADD-IE+1)*DE
              END DO
          END DO
      END DO
      NE = NE + NEADD
C
      DO IE = 1,NEADD
         E(IE+NE) = E(NE) + (IE-1)*DE + 0.001
      END DO
      NE = NE + NEADD
C
      WRITE (6,99012) 'DE   (eV) ',DE
      WRITE (6,99010) 'NEADD     ',NEADD
      WRITE (6,99012) 'E(min/max)',E(1),E(NE)
      WRITE (6,99010) 'NE  (new) ',NE
      WRITE (6,99015)
      END IF
C
C ----------------------------------------------------------------------
C                       BROADEN AND SCALE CURVES
C ----------------------------------------------------------------------
      WLOR(1) = WLORVBXPS(1)
      WLOR(2) = WLORVBXPS(2)
C
      IF ( (ABS(9999-WLOR(1)).LT.1E-5) ) WLOR(1) = 0.0
C
      NVAL = 0
      ILOR = 1
      DO I = 1,NE
          WLORTAB(I) = WLOR(2)*E(I)**2
      END DO

      ALLOCATE(IXRS(NVMAX,NEMAX),BXRS(NVMAX,NEMAX))

      DO IT=1,NTMAX
          NLT(IT)=1
      END DO
      CALL BROADEN(TOT,IXRS,BXRS,E,WGTGAUTAB,WLORTAB,ILOR,NVAL,NEMAX,
     &             NT,NSMAX,NPOLMAX,NVMAX,NE,0,NT,0,NLT,0,NPOL)
C
      CALL BROADEN(POL,IXRS,BXRS,E,WGTGAUTAB,WLORTAB,ILOR,NVAL,NEMAX,
     &             NT,NSMAX,NPOLMAX,NVMAX,NE,0,NT,0,NLT,0,NPOL)


      DEALLOCATE(IXRS,BXRS)

C ----------------------------------------------------------------------
C ======================================================================
C                              XMGRACE - output
C ======================================================================
      IF ( ABS(EMIN-9999.).LT.0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF ( ABS(EMAX-9999.).LT.0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C     ------------------------------------------------------------------
      IFIL = 90
C
C ----------------------------------------------------------------------
C                              XMGRACE - Total VBXPS
C ----------------------------------------------------------------------
      IF ( NT.GE.1 ) THEN
         YTXT1 = 'Intensity (arb. units)'
         LYTXT1 = 23
         YTXT2 = ' '
         LYTXT2 = 1
C
         YMAX = -1D-10
         YMIN = 1D10
         DO I = 0,NT
            DO J = 1,NE
               IF ( YMAX.LT.TOT(J,I,0,0) ) YMAX = TOT(J,I,0,0)
               IF ( YMIN.GT.TOT(J,I,0,0) ) YMIN = TOT(J,I,0,0)
            END DO
         END DO
C
         TITLE='VB-XPS of '//SYSTEM(1:LSYSTEM)//' at E!sphot!N='
     &        //EPHOTCHAR(LINEPHOT:LOUTEPHOT)
         LTITLE=(25+LINEPHOT+LOUTEPHOT+LSYSTEM)
         CALL XMGR4HEAD(DATASET,LDATASET,'vb-xps',6,' ',0,
     &        FNOUT,80,LFNOUT,IFIL,1,XMIN,1,XMAX,1,YMIN,1,
     &        YMAX,1,YMIN,1,YMAX,1,'energy (eV)',11,YTXT1,
     &        LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &        TITLE,LTITLE,KNOHEADER)
C
         NCURVES = MIN(NLEGMAX,(NT+1))
C
         CALL XMGRCURVES(IFIL,1,NCURVES,NCURVES,2,1,0)
C
         LEG(1) = 'total'
C
         DO IT = 1,NT
            LEG(IT+1) = TXTT(IT)(1:LTXTT(IT))
         END DO
C     

         CALL XMGRLEGEND(IFIL,1,NCURVES,NCURVES,LEG,LEG)
C
         DO IT = 0,NT
            IF ( IT.EQ.0 ) THEN
               WGT = 1.0
            ELSE
               WGT = CONC(IT)*NQT(IT)
            END IF
C
            CALL XMGR4TABLE(0,IT,E,TOT(1,IT,0,0),WGT,NE,IFIL)
C
         END DO
C
C
         WRITE (6,*) '  '
         WRITE (6,*) 'Total VB-XPS written to ',FNOUT(1:LFNOUT)
         CLOSE (IFIL)
C
      END IF
C ----------------------------------------------------------------------
C                              XMGRACE - Total VBXPS
C ----------------------------------------------------------------------
      IF ( NT.GE.1 ) THEN
          YTXT1 = 'Intensity (arb. units)'
          LYTXT1 = 23
          YTXT2 = ' '
          LYTXT2 = 1
C
         YMAX = -1D-10
         YMIN = 1D10
         DO I = 0,NT
            DO J = 1,NE
               IF ( YMAX.LT.TOT(J,I,0,0) ) YMAX = TOT(J,I,0,0)
               IF ( YMIN.GT.TOT(J,I,0,0) ) YMIN = TOT(J,I,0,0)
            END DO
         END DO
C
         TITLE='VB-XPS of '//SYSTEM(1:LSYSTEM)//' at E!sphot!N='
     &        //EPHOTCHAR(LINEPHOT:LOUTEPHOT)
         LTITLE=(25+LINEPHOT+LOUTEPHOT+LSYSTEM)
         CALL XMGR4HEAD(DATASET,LDATASET,'vb-xps_tot',10,' ',0,
     &                  FNOUT,80,LFNOUT,IFIL,1,XMIN,1,XMAX,1,YMIN,1,
     &                  YMAX,1,YMIN,1,YMAX,1,'energy (eV)',11,YTXT1,
     &                  LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &                  TITLE,LTITLE,KNOHEADER)
C
         NCURVES = MIN(NLEGMAX,(NT+1))
C
         CALL XMGRCURVES(IFIL,1,NCURVES,NCURVES,2,1,0)
C
         LEG(1) = 'total'
C
         DO IT = 1,NT
            LEG(IT+1) = TXTT(IT)(1:LTXTT(IT))
         END DO
C     

         CALL XMGRLEGEND(IFIL,1,NCURVES,NCURVES,LEG,LEG)
C
         DO IT = 0,0
            IF ( IT.EQ.0 ) THEN
               WGT = 1.0
            ELSE
               WGT = CONC(IT)*NQT(IT)
            END IF
        WGT=1.0
C
            CALL XMGR4TABLE(0,IT,E,TOT(1,IT,0,0),WGT,NE,IFIL)
C
         END DO
C
C
         WRITE (6,*) '  '
         WRITE (6,*) 'Total VB-XPS written to ',FNOUT(1:LFNOUT)
         CLOSE (IFIL)
C
      END IF
C ----------------------------------------------------------------------
C     XMGRACE - Spin and Polarisation resolved
C ----------------------------------------------------------------------
      YTXT1 = 'Intensity (arb. units)'
      LYTXT1 = 23
      YTXT2 = 'Intensity (arb. units)'
      LYTXT2 = 23
C
      YMAX = -1D-10
      YMIN = 1D10
      DO I = 1,3
         DO J = 1,NE
            IF ( YMAX.LT.TOT(J,0,0,I) ) YMAX = TOT(J,0,0,I)
            IF ( YMIN.GT.TOT(J,0,0,I) ) YMIN = TOT(J,0,0,I)
         END DO
      END DO
      YMAX2 = -1D10
      YMIN2 = 1D10
      DO I = 1,3
         DO J = 1,NE
            IF ( YMAX2.LT.(POL(J,0,0,I)-POL(J,0,1,I)) )
     &           YMAX2 = (POL(J,0,0,I)-POL(J,0,1,I))
            IF ( YMIN2.GT.(POL(J,0,0,I)-POL(J,0,1,I)) )
     &           YMIN2 = (POL(J,0,0,I)-POL(J,0,1,I))
         END DO
      END DO
      TITLE='Spin and polarisation resolved VB-XPS of '
     &     //SYSTEM(1:LSYSTEM)
      LTITLE=(41+LSYSTEM)
      CALL XMGR4HEAD(DATASET,LDATASET,'vb-xps_spin',11,' ',0,
     &               FNOUT,80,LFNOUT,IFIL,2,XMIN,1,XMAX,1,YMIN,1,
     &               YMAX,1,YMIN2,1,YMAX2,1,'energy (eV)',11,YTXT1,
     &               LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &               TITLE,LTITLE,KNOHEADER)
C
      CALL XMGRCURVES(IFIL,2,4,3,2,1,0)
C
      LEG(1) = 'POL(+)'
      LEG(2) = 'POL(-)'
      LEG(3) = 'POL(z)'
      LEG2(1) ='TOT(+)'
      LEG2(2) ='TOT(-)'
      LEG2(3) ='TOT(z)'
C
      CALL XMGRLEGEND(IFIL,2,3,3,LEG2,LEG)
C
C
      CALL XMGR4TABLE(0,0,E,TOT(1,0,0,1),1.0,NE,IFIL)
      CALL XMGR4TABLE(0,1,E,TOT(1,0,0,2),1.0,NE,IFIL)
      CALL XMGR4TABLE(0,2,E,TOT(1,0,0,3),1.0,NE,IFIL)
C
      DO I = 1,NEMAX
         WORK(I) = POL(I,0,0,1) - POL(I,0,1,1)
      END DO
      CALL XMGR4TABLE(1,0,E,WORK(1),1.0,NE,IFIL)
      DO I = 1,NEMAX
         WORK(I) = POL(I,0,0,2) - POL(I,0,1,2)
      END DO
      CALL XMGR4TABLE(1,1,E,WORK(1),1.0,NE,IFIL)
      DO I = 1,NEMAX
          WORK(I) = POL(I,0,0,3) - POL(I,0,1,3)
      END DO
      CALL XMGR4TABLE(1,2,E,WORK(1),1.0,NE,IFIL)
C
      WRITE (6,*) '  '
      WRITE (6,*) 'Spin resolved VB-XPS written to ',FNOUT(1:LFNOUT)
      WRITE (6,*) '  '
C

      STOP
C
C ----------------------------------------------------------------------
99001 FORMAT (1000(E12.5,2x))
99002 FORMAT ('#  ',A,A,A)
99003 FORMAT (10X,A80)
99004 FORMAT (10X,I10)
99005 FORMAT (10X,I10,5(:,3X,A,1X))
99006 FORMAT (10X,10F10.5)
99007 FORMAT (I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99008 FORMAT (10X,A)
99009 FORMAT (8F10.4)
99010 FORMAT (10X,A10,I10)
99011 FORMAT (10X,A10,I10,5(:,'  (',A,')'))
99012 FORMAT (10X,A10,10F10.5)
99013 FORMAT (10X,A10,A)
99014 FORMAT (11X,100E12.5)
99015 FORMAT (80('#'))
99016 FORMAT (/,'# column array2d (1,',i5,',1,',i5,') of real scalar',/)
      END
