      SUBROUTINE FINDCMD(IFIL,KW,LKW,CMDUC,CMD00,LCMD,FOUND,TABLE,
     &                   LCMDMAX,IFTABLE)
C   ********************************************************************
C   *                                                                  *
C   *  find keyword KW(1:LKW) starting at column 1 in file  IFIL       *
C   *  combine this and all follwing lines to a string CMD00(1:LCMD)   *
C   *  until a non-blank character in the 1 column or EOF is found     *
C   *  CMDUC contains an  upper case - copy of CMD00                   *
C   *                                                                  *
C   *  the KW is expected to be upper case                             *
C   *                                                                  *
C   *  if the string 'TABLE' is found in CMDUC, the follwing lines     *
C   *  are interpreted as a table as long there is no character in the *
C   *  1st column - these lines are copied to an aux. file  IFTABLE    *
C   *  this table has to be the last part of the command               *
C   *                                                                  *
C   *  only  LLMAX  characters per input line are accepted             *
C   *                                                                  *
C   *  lines starting with # are comment - lines                       *
C   *  input after # is interpreted as  comment                        *
C   *  the character '=' is replaced by a blank                        *
C   *                                                                  *
C   *  a blank is added to  CMDUC/CMD00 at the end  >>>  <FINDKW>   *
C   *                                                                  *
C   ********************************************************************
      PARAMETER ( LLMAX = 256 )
      CHARACTER*(*) KW, CMDUC, CMD00
      CHARACTER*(LLMAX) LINEUC, LINE00
      LOGICAL FOUND, TABLE            
 
      FOUND = .FALSE.
      TABLE = .FALSE. 

      REWIND IFIL

  10  READ(IFIL,'(A)',END=900) LINE00
                  
      LINEUC = LINE00

      CALL CNVTUC(LINEUC,1,MIN(LKW,LLMAX))
      IF( LINEUC(1:LKW) .EQ. KW(1:LKW) ) THEN
         FOUND = .TRUE. 
         LL  = LNGSTRING(LINEUC,LLMAX) 
         LCMD  =  LL - LKW + 1
         CMD00 = LINE00(LKW+1:LL)//' '
         CMDUC = CMD00
         CALL CNVTUC(CMDUC,1,LCMD)
         CALL FINDKW('TABLE',5,IPOS,CMDUC,LCMD,TABLE)
         IF( TABLE ) GOTO 30

   20    READ(IFIL,'(A)',END=900) LINE00
         IF( LINE00(1:1) .EQ. '#' ) GOTO 20
         IF( LINE00(1:1) .NE. ' ' ) GOTO 900
         LL      = LNGSTRING(LINE00,LLMAX) 
         IF( LL .EQ. 0 ) GOTO 900
         CALL SKPLBLK(LINE00,LL)    
         I = INDEX(LINE00(1:LCMD),'#')
         IF( I .EQ. 1 ) GOTO 20
         IF( I .GT. 1 ) LL = I - 1
         IF( LCMD + LL .GT. LCMDMAX ) THEN
             WRITE(*,'(A,A)') 'CMD :',CMD00(1:LCMD)
             WRITE(*,'(A,A)') '+  LINE :',LINE00(1:LL)
             WRITE(*,'(A,I3)')'TOO LONG !  LCMDMAX=',LCMDMAX
         END IF
         LINEUC = LINE00
         CALL CNVTUC(LINEUC,1,LL)
         LCMD = LCMD + 1
         CMD00(LCMD:LCMD) = ' '
         CMDUC(LCMD:LCMD) = ' '
         DO 25 I=1,LL
            LCMD = LCMD + 1
            CMD00(LCMD:LCMD) = LINE00(I:I)
            CMDUC(LCMD:LCMD) = LINEUC(I:I)
   25    CONTINUE
         LCMD = LCMD + 1
         CMD00(LCMD:LCMD) = ' '
         CMDUC(LCMD:LCMD) = ' '

         CALL FINDKW('TABLE',5,IPOS,CMDUC,LCMD,TABLE)
   30    CONTINUE

         IF( TABLE ) THEN
            OPEN( IFTABLE, STATUS='SCRATCH' )
            ITAB = 0
   40       READ(IFIL,'(A)',END=900) LINE00
            LL = LNGSTRING(LINE00,LLMAX) 
            IF( LINE00(1:1) .EQ. '#' ) GOTO 40
            IF( LINE00(1:1) .NE. ' ' ) GOTO 900 
            IF( LL .EQ. 0 ) GOTO 900
            I  = INDEX(LINE00(1:LL),'#')
            IF( I .EQ. 1 ) GOTO 40
            IF( I .GT. 1 ) LL = I - 1
            WRITE(IFTABLE,'(A)') LINE00(1:LL)//' '
            ITAB = ITAB + 1
            GOTO 40
         END IF

         GOTO 20       
      ELSE 
         GOTO 10
      END IF   
  900 CONTINUE
      REWIND IFTABLE 
      IF( FOUND ) THEN
         LCMD    = LCMD + 1
         CMD00(LCMD:LCMD) = ' '
         CMDUC(LCMD:LCMD) = ' '
         CALL EXCHCHAR('=',' ',CMD00,LCMD)
         CALL EXCHCHAR('=',' ',CMDUC,LCMD)
         CALL EXCHCHAR('(','{',CMDUC,LCMD)
         CALL EXCHCHAR(')','}',CMDUC,LCMD)
         CALL EXCHCHAR('[','{',CMDUC,LCMD)
         CALL EXCHCHAR(']','}',CMDUC,LCMD)
         CALL EXCHCHAR(';',',',CMDUC,LCMD)
      END IF
      END
      SUBROUTINE EXCHCHAR(C1,C2,STRING,LSTR)
C   ********************************************************************
C   *                                                                  *
C   *  exchange all characters  C1  by C2 in  STRING(1:LSTR)           *
C   *                                                                  *
C   ********************************************************************
      CHARACTER C1,C2,STRING*(*)
   10 CONTINUE
      I = INDEX(STRING(1:LSTR),C1)
      IF( I .NE. 0 ) THEN
         STRING(I:I) = C2
         GOTO 10
      END IF  
      END
      SUBROUTINE FINDKW(KW,LKW,IPOS,LINE,LL,FOUND)
C   ********************************************************************
C   *                                                                  *
C   *  find the keyword   KW(1:LKW)  in  LINE(1:LL)                    *
C   *  a blank is assumed to be before and after the KW in LINE        *
C   *  LINE is assumed to be upper case                                *
C   *  IPOS = 0    KW is not found                                     *
C   *  IPOS > 0    position of first character after KW//' '           *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) LINE, KW
      CHARACTER*42  KWP
      LOGICAL FOUND                            
      IF( LKW .GT. 40 ) STOP '  LKW > 40 in <FINDKW>'
      KWP  = ' '//KW(1:LKW)//' '  
      LKWP = LKW + 2
      CALL CNVTUC(KWP,2,LKWP-1)

      I = INDEX( LINE(1:LL), KWP(1:LKWP) )
      IF( I .EQ. 0 ) THEN
         IPOS  = 0
         FOUND = .FALSE.
      ELSE
         IPOS  = MIN( I + LKWP, LL )
         FOUND = .TRUE.
      END IF
      END
      SUBROUTINE UPDATEINT(X,STRX,LSTRX,CMDLINE,LCMD,
     &                     STR,LSTRMAX,IFCONVT,UPDATE)  
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDLINE(1:LCMD) and         *
C   *   update the integer number  X  if STRX  was found               *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDLINE, STR
      INTEGER X 
      LOGICAL UPDATE, FOUND
      CALL FINDKW(STRX,LSTRX,IPOS,CMDLINE,LCMD,UPDATE)
                      
      IF( UPDATE ) THEN                                                     
         CALL GTVALINT(X,STR,LSTRMAX,CMDLINE,IPOS,LCMD,IFCONVT,FOUND)         
         IF( .NOT. FOUND ) STOP 'no integer supplied for update !! '
      END IF
      END
      SUBROUTINE UPDATREAL(X,STRX,LSTRX,CMDLINE,LCMD,
     &                     STR,LSTRMAX,IFCONVT,UPDATE)         
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDLINE(1:LCMD) and         *
C   *   update the real number  X  if STRX  was found                  *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDLINE, STR
      REAL*8  X 
      LOGICAL UPDATE, FOUND
      CALL FINDKW(STRX,LSTRX,IPOS,CMDLINE,LCMD,UPDATE)
                      
      IF( UPDATE ) THEN 
         CALL GTVALREAL(X,STR,LSTRMAX,CMDLINE,IPOS,LCMD,IFCONVT,FOUND)         
         IF( .NOT. FOUND ) STOP 'no  real supplied for update !! '
      END IF
      END
      SUBROUTINE UPDATESTR(STRX,LSTRX,CMDUC,CMD00,LCMD,
     &                     STR,LSTR,LSTRMAX,UPDATE)         
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDUC(1:LCMD) and           *
C   *   update the string  STR(1:LSTR)  using CMD00(1:LCMD)            *
C   *   if STRX  was found                                             *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDUC,CMD00, STR
      LOGICAL UPDATE
      CALL FINDKW(STRX,LSTRX,IPOS,CMDUC,LCMD,UPDATE)
                      
      IF( UPDATE ) THEN 
         CALL GTVALSTR(STR,LSTR,LSTRMAX,CMD00,IPOS,LCMD)         
         IF( LSTR .EQ. 0 ) STOP 'no  string supplied for update !! '
      END IF
      END
      SUBROUTINE UPDATIARR(X,NX,DX,STRX,LSTRX,CMDLINE,LCMD,
     &                     STR,LSTRMAX,IFCONVT,UPDATE)
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDLINE(1:LCMD) and         *
C   *   update the integer array   X(1,DX)  if STRX  was found         *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDLINE, STR
      CHARACTER*1   CSEP, CDEL,C
      INTEGER DX
      INTEGER X(DX)
      LOGICAL FOUND,UPDATE

      CALL FINDKW(STRX,LSTRX,IPOS,CMDLINE,LCMD,UPDATE)

      NX = 0

      IF( UPDATE ) THEN
         CDEL = ' '
   10    CONTINUE
         C = CMDLINE(IPOS:IPOS)
         IF( C .EQ. ' ' ) THEN
            IF( IPOS .LT. LCMD ) THEN
               IPOS = IPOS + 1
               GOTO 10
            ELSE
               WRITE(*,*) ' <UPDATIARR>:  IPOS > LCMD '
               WRITE(*,*) ' CMDLINE ',CMDLINE(1:LCMD)
               WRITE(*,*) '         ',(' ',I=1,IPOS-1),'^'
               WRITE(*,*) ' STR     ',STRX
            END IF
         ELSE
            IF( C .EQ. '{' ) CDEL = '}'
            IF( C .EQ. '[' ) CDEL = ']'
            IF( C .EQ. '(' ) CDEL = ')'
         END IF

         DO 20 I=1,DX
            CALL GTVALINT(X(I),STR,LSTRMAX,
     &                    CMDLINE,IPOS,LCMD,IFCONVT,FOUND)         
            IF( CMDLINE(IPOS:IPOS) .EQ. CDEL ) THEN
               NX = I
               DO 30 J=NX+1,DX
                  X(J) = X(NX)
   30          CONTINUE
               RETURN
            END IF
   20    CONTINUE
 
      END IF
      END
      SUBROUTINE UPDATRARR(X,NX,DX,STRX,LSTRX,CMDLINE,LCMD,
     &                     STR,LSTRMAX,IFCONVT,UPDATE)         
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDLINE(1:LCMD) and         *
C   *   update the real array   X(1,DX)  if STRX  was found            *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDLINE, STR
      CHARACTER*1   CSEP, CDEL,C
      INTEGER DX
      REAL*8  X(DX) 
      LOGICAL FOUND,UPDATE
      
      CALL FINDKW(STRX,LSTRX,IPOS,CMDLINE,LCMD,UPDATE)
                      
      NX = 0

      IF( UPDATE ) THEN 
         CDEL = ' '
   10    CONTINUE
         C = CMDLINE(IPOS:IPOS)
         IF( C .EQ. ' ' ) THEN
            IF( IPOS .LT. LCMD ) THEN
               IPOS = IPOS + 1
               GOTO 10
            ELSE
               WRITE(*,*) ' <UPDATRARR>:  IPOS > LCMD '
               WRITE(*,*) ' CMDLINE ',CMDLINE(1:LCMD)
               WRITE(*,*) '         ',(' ',I=1,IPOS-1),'^'
               WRITE(*,*) ' STR     ',STRX
            END IF
         ELSE
            IF( C .EQ. '{' ) CDEL = '}'
            IF( C .EQ. '[' ) CDEL = ']'
            IF( C .EQ. '(' ) CDEL = ')'
         END IF

         DO 20 I=1,DX
            CALL GTVALREAL(X(I),STR,LSTRMAX,
     &                     CMDLINE,IPOS,LCMD,IFCONVT,FOUND)         

            IF( CMDLINE(IPOS:IPOS) .EQ. CDEL .OR. CDEL .EQ. ' ' ) THEN
               NX = I
               DO 30 J=NX+1,DX
                  X(J) = X(NX)
   30          CONTINUE
               RETURN
            END IF
   20    CONTINUE
         
      END IF
      END
      SUBROUTINE UPDATSARR(X,NX,LX,DX,STRX,LSTRX,CMDLINE,LCMD,
     &                     STR,LSTRMAX,IFCONVT,UPDATE)
C   ********************************************************************
C   *                                                                  *
C   *   find the string  STRX(1:LSTRX) in  CMDLINE(1:LCMD) and         *
C   *   update the string array   X(1,DX)(1:LX)   if STRX  was found   *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRX, CMDLINE, STR
      CHARACTER*1   CSEP, CDEL,C
      INTEGER DX
      CHARACTER*(*) X(DX)
      LOGICAL FOUND,UPDATE

      CALL FINDKW(STRX,LSTRX,IPOS,CMDLINE,LCMD,UPDATE)

      NX = 0

      IF( UPDATE ) THEN
         CDEL = ' '
   10    CONTINUE
         C = CMDLINE(IPOS:IPOS)
         IF( C .EQ. ' ' ) THEN
            IF( IPOS .LT. LCMD ) THEN
               IPOS = IPOS + 1
               GOTO 10
            ELSE
               WRITE(*,*) ' <UPDATSARR>:  IPOS > LCMD '
               WRITE(*,*) ' CMDLINE ',CMDLINE(1:LCMD)
               WRITE(*,*) '         ',(' ',I=1,IPOS-1),'^'
               WRITE(*,*) ' STR     ',STRX
            END IF
         ELSE
            IF( C .EQ. '{' ) CDEL = '}'
            IF( C .EQ. '[' ) CDEL = ']'
            IF( C .EQ. '(' ) CDEL = ')'
         END IF

         DO 20 I=1,DX     
            CALL GTVALSTR(X(I),L,LX,CMDLINE,IPOS,LCMD)
            IF( CMDLINE(IPOS:IPOS) .EQ. CDEL ) THEN
               NX = I
               DO 30 J=NX+1,DX
                  X(J) = X(NX)
   30          CONTINUE
               RETURN
            END IF
   20    CONTINUE
 
      END IF
      END
      SUBROUTINE GTVALINT(I,STR,LSTRMAX,
     &                    STRING,IPOS,LSTRING,IFCONVT,FOUND)
C   ********************************************************************
C   *                                                                  *
C   *   get the next integer      I      in  STRING(IPOS:LSTRING)      *
C   *   delimiters:  blank , ; { } [ ] ( )  ignored                    *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR, STRING
      LOGICAL FOUND
      INTEGER TMP

      CALL GTVALSTR(STR,LSTR,LSTRMAX,STRING,IPOS,LSTRING)

      IF( LSTR .EQ. 0 ) THEN
         FOUND = .FALSE.
      ELSE
         REWIND IFCONVT
         WRITE( IFCONVT, '(A)' ) STR(1:LSTR)
         REWIND IFCONVT
          READ( IFCONVT,*) TMP
         I = TMP
         FOUND = .TRUE.
      END IF
      END
      SUBROUTINE GTVALREAL(R,STR,LSTRMAX,
     &                    STRING,IPOS,LSTRING,IFCONVT,FOUND)
C   ********************************************************************
C   *                                                                  *
C   *   get the next real number       R      in  STRING(IPOS:LSTRING) *
C   *   delimiters:  blank , ; { } [ ] ( )  ignored                    *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR, STRING
      LOGICAL FOUND
      REAL*8  TMP, R
      CALL GTVALSTR(STR,LSTR,LSTRMAX,STRING,IPOS,LSTRING)

      IF( LSTR .EQ. 0 ) THEN
         FOUND = .FALSE.
      ELSE
         CALL CNVSTOR( STR, LSTR, R, IFCONVT )
         FOUND = .TRUE.
      END IF
      END
      SUBROUTINE GTVALSTR(STR,LSTR,LSTRMAX,STRING,IPOS,LSTRING)
C   ********************************************************************
C   *                                                                  *
C   *   get the next string  STR  in STRING(IPOS:LSTRING)              *
C   *   delimiters:  blank , ; { } [ ] ( )  ignored                    *
C   *   push IPOS to position of next non-blank character in STRING    *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR, STRING

      DO I=1,LSTRMAX
         STR(I:I) = ' '
      END DO

      I0   = IPOS
      LSTR = 0          
      DO 10 I=I0,LSTRING
         IF( STRING(I:I) .NE. ' ' .AND. STRING(I:I) .NE. '=' 
     &                            .AND. STRING(I:I) .NE. ','       
     &                            .AND. STRING(I:I) .NE. ';'       
     &                            .AND. STRING(I:I) .NE. '{'       
     &                            .AND. STRING(I:I) .NE. '['       
     &                            .AND. STRING(I:I) .NE. '(' ) THEN
            DO 20 J=I,LSTRING
               IF( STRING(J:J) .EQ. ' ' .OR.
     &             STRING(J:J) .EQ. ',' .OR.
     &             STRING(J:J) .EQ. ';' .OR.
     &             STRING(J:J) .EQ. '}' .OR.
     &             STRING(J:J) .EQ. ']' .OR.
     &             STRING(J:J) .EQ. ')' ) GOTO 30
               LSTR = LSTR + 1     
               IF( LSTR .GT. LSTRMAX ) THEN
                  WRITE(*,'(A,/,A,A,/,A,I3)') ' ERROR in <GTVALSTR> ',
     &            ' STRING:             ', STRING(1:LSTRING),
     &            ' STR too long LSTRMAX:',LSTRMAX
                  STOP
               END IF
               STR(LSTR:LSTR) = STRING(J:J)
               IPOS = J + 1
   20       CONTINUE  
            GOTO 30
         END IF
   10 CONTINUE  
      RETURN
   30 CONTINUE
      IF( STRING(IPOS:IPOS) .EQ. ' ' .AND. IPOS .LE. LSTRING ) THEN
         IPOS = IPOS + 1
         GOTO 30
      END IF
      END
      FUNCTION LNGZEROSTRING(STRING,LSTRMAX) 
C   ********************************************************************
C   *                                                                  *
C   *  find position of the first non-blank character in               *
C   *                    STRING(1:LSTRMAX)                             *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING 
      CHARACTER C
      INTEGER LNGZEROSTRING, LSTRMAX 
      LNGZEROSTRING=0
      DO I=1,LSTRMAX
         C = STRING(I:I)
         LNGZEROSTRING = LNGZEROSTRING+1
         IF( C .NE. ' ' .AND. ICHAR(C) .NE. 0 ) THEN
             RETURN
         END IF
      END DO
      END
      FUNCTION LNGSTRING(STRING,LSTRMAX) 
C   ********************************************************************
C   *                                                                  *
C   *  find position of last non-blank character in STRING(1:LSTRMAX)  *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING 
      CHARACTER C
      INTEGER LNGSTRING, LSTRMAX 
      LNGSTRING=0
      DO 10 I=LSTRMAX,1,-1
         C = STRING(I:I)
         IF( C .NE. ' ' .AND. ICHAR(C) .GT. 0 ) THEN
            LNGSTRING = I
            RETURN
         END IF
   10 CONTINUE
      END
      SUBROUTINE CNVTUC(STRING,I1,I2)
C   ********************************************************************
C   *                                                                  *
C   *  convert characters to upper case in  STRING(I1:I2)              *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING

      IA = ICHAR( 'a' )
      IZ = ICHAR( 'z' )
      INC= ICHAR( 'A' ) - IA

      DO 10 I=I1,I2  
         J = ICHAR( STRING(I:I) )
         IF( J .GE. IA .AND. J .LE. IZ ) THEN
            STRING(I:I) = CHAR( J + INC )
         END IF   
   10 CONTINUE
      END
      SUBROUTINE CNVTLC(STRING,I1,I2)
C   ********************************************************************
C   *                                                                  *
C   *  convert characters to lower case in  STRING(I1:I2)              *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING

      IA = ICHAR( 'A' )
      IZ = ICHAR( 'Z' )
      INC= ICHAR( 'a' ) - IA

      DO 10 I=I1,I2  
         J = ICHAR( STRING(I:I) )
         IF( J .GE. IA .AND. J .LE. IZ ) THEN
            STRING(I:I) = CHAR( J + INC )
         END IF   
   10 CONTINUE
      END
      SUBROUTINE CNVSTOR( STR, LSTR, R, IFILCNV )
C   ********************************************************************
C   *                                                                  *
C   *   convert string  STR(1:LSTR)  to real number  R                 *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR
      REAL*8 R
      REWIND IFILCNV
      WRITE( IFILCNV, '(A)' ) STR(1:LSTR)
      REWIND IFILCNV
       READ( IFILCNV,*) R
      END
      SUBROUTINE CNVRTOS( STR, R, FORMAT, IFILCNV )
C   ********************************************************************
C   *                                                                  *
C   *   convert real number  R  to  string  STR(1:LSTR)                *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR, FORMAT
      REAL*8 R
      REWIND IFILCNV
      WRITE( IFILCNV, FMT=FORMAT) R
      REWIND IFILCNV
       READ( IFILCNV,'(A)') STR
      END
      SUBROUTINE SKPLBLK(STRING,LL)
C   ********************************************************************
C   *                                                                  *
C   *  skip leading blanks in   STRING                                 *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING
      DO 10 I=1,LL  
         IF( STRING(I:I) .NE. ' ' ) THEN
            IF( I .EQ. 1 ) RETURN
            K = 0
            DO 20 J=I,LL
               K = K + 1
               STRING(K:K) = STRING(J:J)
   20       CONTINUE
            LL = LL - I + 1
            RETURN
         END IF   
   10 CONTINUE
      END
      SUBROUTINE ADDNTOSTR(STRING,N,LL)
C   ********************************************************************
C   *                                                                  *
C   *  add the integer N to     STRING                                 *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STRING
      REAL*8 X
      INTEGER N, M, NDIG, J
      M = N
      DO I=1,10000
         IF( M .LT. 10 ) THEN 
            NDIG = I
            GOTO 100
         END IF
         M = M / 10
      END DO
  100 CONTINUE
      X  = DBLE( N )
      I0 = ICHAR( '0' )
      DO I=NDIG,1,-1
         LL = LL + 1
         J  = INT(X/10.0D0**(I-1)) - INT(X/10.0D0**I)*10 
         STRING(LL:LL) = CHAR( I0 + J )
      END DO
      END
