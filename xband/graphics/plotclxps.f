C*==plotclxps.f    processed by SPAG 6.05Rc at 15:18 on 19 Dec 2001
      SUBROUTINE PLOTCLXPS(IFIL,DATASET,LDATASET,NQMAX,NTMAX,NEMAX,
     &                     NCMAX,NCSTMAX,NSPMAX,SPEC,NLQ,NLT,NMIN,NMAX,
     &                     IQOFT,NQT,CONC,TXTT,LTXTT,INFO,LINFO,HEADER,
     &                     LHEADER,SYSTEM,LSYSTEM,E,EF,NT,NS,NE,IXRS,
     &                     BXRS,NVMAX,WGTGAUTAB,WLORTAB,EEXP,IEXP,
     &                     NCEXPMAX,NPOLEXPMAX,NCXRAY,LCXRAY,ITXRSGRP,
     &                     ECORE,KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR,
     &                     IN,AD,AT,N,NW,TD,TT,TE,MLD,NGEOMAX,TETQPH,
     &                     PHIQPH,TETEPH,PHIEPH,TETKEL,PHIKEL)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX
      PARAMETER (NLEGMAX=80)
C
C COMMON variables
C
      REAL EMAX,EMIN,EREFER,RYOVEV,WGAUSS,WLOR(2)
      CHARACTER*2 EUNIT
      INTEGER MERGE
      COMMON /BRDPAR/ WGAUSS,WLOR
      COMMON /ENERGY/ RYOVEV,EREFER,EUNIT
      COMMON /PLOPAR/ EMIN,EMAX,MERGE
C
C Dummy arguments
C
      REAL AD,EF
      CHARACTER*80 HEADER,INFO,SYSTEM,DATASET
      INTEGER IFIL,LDATASET,LHEADER,LINFO,LSYSTEM,N,NCEXPMAX,NCMAX,
     &        NCSTMAX,NE,NEMAX,NGEOMAX,NLT,NPOLEXPMAX,NQMAX,NS,NSPMAX,
     &        NT,NTMAX,NVMAX,NW
      LOGICAL MLD
      CHARACTER*3 SPEC
      REAL AT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),BXRS(NVMAX,NEMAX),
     &     CONC(NTMAX),E(NEMAX),ECORE(NTMAX,NCSTMAX),
     &     EEXP(NEMAX,NCEXPMAX),IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX),
     &     IXRS(NVMAX,NEMAX),NMAX(0:NTMAX),NMIN(0:NTMAX),TD(NEMAX),
     &     TE(NEMAX),TT(NEMAX),WGTGAUTAB(0:NEMAX),WLORTAB(NEMAX)
      INTEGER IKMCOR(NCSTMAX,2),IN(NEMAX),IQOFT(NQMAX,NTMAX),
     &        ITXRSGRP(NTMAX),KAPCOR(NCSTMAX),KTYPCOR(NCSTMAX),
     &        LCXRAY(NTMAX),LTXTT(NTMAX),MM05COR(NCSTMAX),NCXRAY(NTMAX),
     &        NKPCOR(NCSTMAX),NLQ(NQMAX),NQT(NTMAX)
      REAL*8 PHIEPH(NGEOMAX),PHIKEL(NGEOMAX),PHIQPH(NGEOMAX),
     &       TETEPH(NGEOMAX),TETKEL(NGEOMAX),TETQPH(NGEOMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      INTEGER ATOMNUMB,LNGSTRING,LNGZEROSTRING,TABNVAL
      CHARACTER*1 CDUM,SHELL(5)
      REAL*8 DE,DXG,EPHEV,X
      REAL EC,ECOREAV(2),EXTEND,RW,SUMK(2),
     &     W0X(:,:,:,:,:),WGT,WL,WMAX,
     &     WORK(:),XMAX,XMIN,XNORM(2),YMAX,YMAX2,YMIN,YMIN2
      CHARACTER*80 EPHOTCHAR,FNOUT,NTEXT,STRQ,STRQ1,YTXT1,YTXT2
      INTEGER I,ICST,IE,IGEO,IGMAX,II,III,IPOL,IS,ITXG,ITXGP,IX,IZ,J,
     &        JCST,K,KTYP(2),LFNOUT,LINEPHOT,LL,LOUTEPHOT,LSTRQ1,LYTXT1,
     &        LYTXT2,NCST,NGEO,NKTYP,NPOL,NTXRSGRP,NVAL
      LOGICAL KNOHEADER
      CHARACTER*10 KW
      CHARACTER*20 LEG(NLEGMAX),LEG2(NLEGMAX)
      REAL REAL
      CHARACTER*3 SUBSH(0:4)
      REAL WCOREHOLE
C
C*** End of declarations rewritten by SPAG
C
C
C COMMON variables
C
C
C
C Dummy arguments
C
C
C Local variables
C
C
C
C*** End of declarations rewritten by SPAG
C
C **********************************************************************
C *                                                                    *
C *                                                                    *
C *                                                                    *
C *jm                                                         12/12/01 *
C **********************************************************************
C
C
C ----------------------------------------------------------------------
C ----------------------------------------------------------------------
C
      DATA SHELL/'K','L','M','N','O'/
      DATA SUBSH/'1  ','2,3','4,5','6,7','8,9'/

      ALLOCATABLE WORK,W0X
      ALLOCATE (WORK(NEMAX))
      ALLOCATE (W0X(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX,0:NSPMAX))
C
      NTEXT = 'I_'//SPEC//'(E) (arb.u.)'
C
      KNOHEADER = .FALSE.
      NS = 2
      IF ( MLD ) THEN
         WRITE (*,*) '<CLXPS> Linear dichroism case'
         NPOL = 1
      ELSE
         NPOL = 3
      END IF
C ----------------------------------------------------------------------
      READ (IFIL,99009) NTXRSGRP
C
      DO ITXG = 1,NTXRSGRP
C ----------------------------------------------------------------------
         READ (IFIL,99011) KW
         READ (IFIL,99009) ITXRSGRP(ITXG)
         READ (IFIL,99009) NCXRAY(ITXG)
         READ (IFIL,99009) LCXRAY(ITXG)
         IF ( ITXG.EQ.1 ) THEN
            WRITE (6,99002) NCXRAY(ITXG),LCXRAY(ITXG)
         ELSE
            ITXGP = ITXG - 1
            IF ( NCXRAY(ITXG).NE.NCXRAY(ITXGP) .OR. LCXRAY(ITXG)
     &           .NE.LCXRAY(ITXGP) ) THEN
               WRITE (6,*) ' NC:  ',NCXRAY(ITXG),NCXRAY(ITXGP)
               WRITE (6,*) ' LC:  ',LCXRAY(ITXG),LCXRAY(ITXGP)
               WRITE (6,*) ' ITXG:',ITXG,ITXGP
               STOP ' data inconsistent in <RDCLXPS>'
            END IF
         END IF
C
         READ (IFIL,99003,ERR=200) NCST,(NKPCOR(ICST),ICST=1,NCST)
         READ (IFIL,'(A1)') (CDUM,I=1,2)
         WRITE (6,99005) NCST,(NKPCOR(ICST),ICST=1,NCST)
         WRITE (6,99006)
C
         DO ICST = 1,NCST
C
            READ (IFIL,99004) JCST,NCXRAY(ITXG),LCXRAY(ITXG),
     &                        KAPCOR(ICST),MM05COR(ICST),IKMCOR(ICST,1),
     &                        XNORM(1),ECORE(ITXG,ICST)
            MM05COR(ICST) = (MM05COR(ICST)-1)/2
            WRITE (6,99007) ICST,NCXRAY(ITXG),LCXRAY(ITXG),KAPCOR(ICST),
     &                      (2*MM05COR(ICST)+1),IKMCOR(ICST,1),XNORM(1),
     &                      ECORE(ITXG,ICST),ECORE(ITXG,ICST)*RYOVEV
            IF ( NKPCOR(ICST).EQ.2 ) THEN
               READ (IFIL,99008) IKMCOR(ICST,2),XNORM(2)
               WRITE (6,99008) IKMCOR(ICST,2),XNORM(2)
            END IF
C
            ECORE(ITXG,ICST) = ECORE(ITXG,ICST)*RYOVEV
         END DO
C
         READ (IFIL,99010) EPHEV
         READ (IFIL,99009) NGEO
         WRITE (EPHOTCHAR,'(f12.2)') EPHEV
         LINEPHOT = LNGZEROSTRING(EPHOTCHAR,80)
         LOUTEPHOT = LNGSTRING(EPHOTCHAR,80)
         DO IGEO = 1,NGEO
            READ (IFIL,'(A1)') CDUM
            READ (IFIL,99010) TETQPH(IGEO),PHIQPH(IGEO)
            READ (IFIL,99010) TETEPH(IGEO),PHIEPH(IGEO)
            READ (IFIL,99010) TETKEL(IGEO),PHIKEL(IGEO)
         END DO
         DO ICST = 1,NCST
            DO IGEO = 1,NGEO
               DO IS = 1,2
                  READ (IFIL,99001) IX,X,IX,X,X,
     &                              (W0X(NEMAX,IGEO,ICST,IS,IPOL),
     &                              IPOL=1,NPOL)
               END DO
            END DO
         END DO
C
      END DO
C
C     ---------------------------------------- find KTYP for each core
C     state
      NKTYP = 0
      DO ICST = 1,NCST
         DO K = 1,NKTYP
            IF ( KAPCOR(ICST).EQ.KTYP(K) ) THEN
               KTYPCOR(ICST) = K
               GOTO 100
            END IF
         END DO
C
         NKTYP = NKTYP + 1
         IF ( NKTYP.GT.2 ) STOP ' NKTYP > 2  >>>>>> check core states'
         KTYP(NKTYP) = KAPCOR(ICST)
         KTYPCOR(ICST) = NKTYP
C
 100  END DO
C
      IZ = ATOMNUMB(TXTT(ITXRSGRP(1))(1:2))
      NVAL = TABNVAL(IZ)
      WRITE (6,99015) ' '//TXTT(ITXRSGRP(1))(1:2)
     &                //'            atomic number ',IZ,
     &                ' number of valence electrons ',NVAL
C
      IF ( WLOR(1).GT.9000.0 ) THEN
         WRITE (6,'('' Lorentzian width taken from table '',/)')
         ITXG = 1
         DO K = 1,NKTYP
            WLOR(K) = WCOREHOLE(IZ,NCXRAY(ITXG),LCXRAY(ITXG),K)
         END DO
      END IF
C
      DO K = 1,NKTYP
         SUMK(K) = 0
         ECOREAV(K) = 0.0
      END DO
C
      WRITE (6,
     &'('' ICST KAP TYPE      ECORE    '',                              
     &                                                                  
     &                                                           '' W_  
     &                                                              LOR 
     &    W_GAU   ('',A,'')'')') EUNIT
      DO ITXG = 1,NTXRSGRP
         WGT = NQT(ITXRSGRP(ITXG))*CONC(ITXRSGRP(ITXG))
         DO ICST = 1,NCST
            K = KTYPCOR(ICST)
            ECOREAV(K) = ECOREAV(K) + WGT*ECORE(ITXG,ICST)
            SUMK(K) = SUMK(K) + WGT
            WRITE (6,99012) ICST,KAPCOR(ICST),K,ECORE(ITXG,ICST),WLOR(K)
     &                      ,WGAUSS
         END DO
      END DO
C
      DO K = 1,NKTYP
         ECOREAV(K) = ECOREAV(K)/SUMK(K)
         WRITE (6,'(''  average:'',I2,4X,F10.3)') K,ECOREAV(K)
      END DO
C
      EXTEND = 10.0
      EMIN = +99999
      DO ICST = 1,NCST
         EMIN = MIN(EMIN,ECORE(1,ICST))
      END DO
      EMIN = EMIN - EXTEND
      EMAX = -99999
      DO ICST = 1,NCST
         EMAX = MAX(EMAX,ECORE(1,ICST))
      END DO
      EMAX = EMAX + EXTEND
C
      WRITE (*,'(A,2F10.4,A)') '  plotted energy range:',EMIN + EPHEV,
     &                         EMAX + EPHEV,' eV'
C     ==================================================================
C     broadening
C
      NE = 1000
      IF ( NE.GT.(NEMAX-1) ) STOP ' NE too large in <PLOTCLCPS> '
      DE = (EMAX-EMIN)/REAL(NE)
      DO IE = 1,NE
         E(IE) = EMIN + (IE-1)*DE
      END DO
C
      DO ICST = 1,NCST
         WL = WLOR(KTYPCOR(ICST))
         EC = ECORE(1,ICST)
         DO IPOL = 1,NPOL
            DO IS = 1,NS
               DO IGEO = 1,NGEO
                  DO IE = 1,NE
                     W0X(IE,IGEO,ICST,IS,IPOL)
     &                  = W0X(NEMAX,IGEO,ICST,IS,IPOL)
     &                  *WL/(WL**2+(EC-E(IE))**2)
                  END DO
               END DO
            END DO
         END DO
      END DO
C
C     ==================================================================
C     ====
C     sumation over ms
      DO ICST = 1,NCST
         DO IPOL = 1,NPOL
            DO IGEO = 1,NGEO
               DO IE = 1,NE
                  W0X(IE,IGEO,ICST,0,IPOL) = W0X(IE,IGEO,ICST,1,IPOL)
     &               + W0X(IE,IGEO,ICST,2,IPOL)
               END DO
            END DO
         END DO
      END DO
C     sumation over core states
      WMAX = 0.0
C
      DO IGEO = 1,NGEO
         DO IPOL = 1,NPOL
            DO ICST = 1,NCST
               DO IS = 0,NS
                  DO IE = 1,NE
                     W0X(IE,IGEO,0,IS,IPOL) = W0X(IE,IGEO,0,IS,IPOL)
     &                  + W0X(IE,IGEO,ICST,IS,IPOL)
                  END DO
               END DO
            END DO
         END DO
      END DO
C     ==================================================================
C     ====
C     - GAUSIAN BROADENING
      DO IGEO = 1,NGEO
         DO IPOL = 1,NPOL
            DO ICST = 0,NCST
               DO IE = 1,NE
                  DO IS = 0,NS
                     IXRS(IS+1,IE) = 0.0D0
                     IXRS(IS+1,IE) = W0X(IE,IGEO,ICST,IS,IPOL)
                  END DO
               END DO
               IF ( WGAUSS.GT.0.001 ) THEN
                  CALL INIGAUBRD(WGAUSS,WGTGAUTAB,NEMAX,DXG,IGMAX)
                  CALL VECGAUBRD(E,IXRS,BXRS,NS+1,NVMAX,NE,NEMAX,
     &                           WGTGAUTAB,DXG,IGMAX)
               END IF
               DO IE = 1,NE
                  DO IS = 0,NS
                     W0X(IE,IGEO,ICST,IS,IPOL) = IXRS(IS+1,IE)
                  END DO
               END DO
            END DO
         END DO
      END DO
      DO IGEO = 1,NGEO
         DO IE = 1,NE
            IF ( MLD ) THEN
               WMAX = MAX(WMAX,W0X(IE,IGEO,0,0,1))
            ELSE
               WMAX = MAX(WMAX,W0X(IE,IGEO,0,0,1)+W0X(IE,IGEO,0,0,2))
            END IF
         END DO
      END DO
      RW = 100.0/WMAX
      DO IPOL = 1,NPOL
         DO IS = 0,NS
            DO ICST = 0,NCST
               DO IGEO = 1,NGEO
                  DO IE = 1,NE
                     W0X(IE,IGEO,ICST,IS,IPOL)
     &                  = W0X(IE,IGEO,ICST,IS,IPOL)*RW
                  END DO
               END DO
            END DO
         END DO
      END DO
C
C
C     ==================================================================
C     PLOT SPECTRA AND COMPARE WITH EXPERIMENT
C     ==================================================================
C
      XMIN = EMIN + EPHEV
      XMAX = EMAX + EPHEV
      DO I = 1,NE
         E(I) = E(I) + EPHEV
      END DO
      DO IGEO = 1,NGEO
         IF ( MLD ) THEN
            STRQ = SYSTEM(1:LSYSTEM)//'.mld.igeo_'
            LL = LSYSTEM + 10
            CALL ADDNTOSTR(STRQ,IGEO,LL)
            STRQ = STRQ(1:LL)//'.dat'
            OPEN (UNIT=23,FILE=STRQ,STATUS='UNKNOWN')
            STRQ = SYSTEM(1:LSYSTEM)//'.mld.igeo_'
            LL = LSYSTEM + 10
            DO IE = 1,NE
               WRITE (23,99013) E(IE),(W0X(IE,IGEO,0,0,I),I=1,NPOL),
     &                          (W0X(IE,IGEO,0,1,II),II=1,NPOL),
     &                          (W0X(IE,IGEO,0,2,III),III=1,NPOL)
            END DO
         ELSE
            STRQ = SYSTEM(1:LSYSTEM)//'.igeo_'
C
            LL = LSYSTEM + 6
            CALL ADDNTOSTR(STRQ,IGEO,LL)
            STRQ = STRQ(1:LL)//'.dat'
            OPEN (UNIT=23,FILE=STRQ,STATUS='UNKNOWN')
            STRQ = SYSTEM(1:LSYSTEM)//'.igeo_'
            LL = LSYSTEM + 6
            CALL ADDNTOSTR(STRQ,IGEO,LL)
C
            WRITE (23,*) 
     &'#E(ie);ipol(1,2,3);MCD;ipol(1,2,3)-ms=1/2                        
     &                                                                  
     &                                                       ;ipol(1,2  
     &                                                              ,3)-
     &ms=-1/2'
            DO IE = 1,NE
               WRITE (23,99014) E(IE),(W0X(IE,IGEO,0,0,I),I=1,NPOL),
     &                          W0X(IE,IGEO,0,0,2) - W0X(IE,IGEO,0,0,1),
     &                          (W0X(IE,IGEO,0,1,II),II=1,NPOL),
     &                          (W0X(IE,IGEO,0,2,III),III=1,NPOL)
            END DO
         END IF
C ----------------------------------------------------------------------
C     XMGRACE - Total and dichroism
C ----------------------------------------------------------------------
         IF ( MLD .EQV. .FALSE. ) THEN
            IFIL = 88
            YTXT1 = 'Intensity (arb. units)'
            LYTXT1 = 23
            YTXT2 = 'Dichroism (arb. units)'
            LYTXT2 = 23
C
            YMAX = -1D-10
            YMIN = 1D10
            DO J = 1,NE
               IF ( YMAX.LT.(W0X(J,IGEO,0,0,1)+W0X(J,IGEO,0,0,2)) )
     &              YMAX = (W0X(J,IGEO,0,0,1)+W0X(J,IGEO,0,0,2))
               IF ( YMIN.GT.(W0X(J,IGEO,0,0,1)+W0X(J,IGEO,0,0,2)) )
     &              YMIN = (W0X(J,IGEO,0,0,1)+W0X(J,IGEO,0,0,2))
            END DO
            YMAX2 = -1D10
            YMIN2 = 1D10
            DO IE = 1,NE
               IF ( YMAX2.LT.(-W0X(IE,IGEO,0,0,2)+W0X(IE,IGEO,0,0,1)) )
     &              YMAX2 = (-W0X(IE,IGEO,0,0,2)+W0X(IE,IGEO,0,0,1))
               IF ( YMIN2.GT.(-W0X(IE,IGEO,0,0,2)+W0X(IE,IGEO,0,0,1)) )
     &              YMIN2 = (-W0X(IE,IGEO,0,0,2)+W0X(IE,IGEO,0,0,1))
            END DO
C
            STRQ = SYSTEM(1:LSYSTEM)//'.igeo_'
            LL = LSYSTEM + 6
            CALL ADDNTOSTR(STRQ,IGEO,LL)
            LL = LNGSTRING(STRQ,80)
            STRQ1 = 'Circular dichroism in CL-XPS of '//
     &              SYSTEM(1:LSYSTEM)//' for E_phot='//
     &              EPHOTCHAR(LINEPHOT:LOUTEPHOT)//'eV'
C
            LSTRQ1 = LNGSTRING(STRQ1,80)
C
            CALL XMGR4HEAD(DATASET,LDATASET,STRQ(1:LL),LL,' ',0,FNOUT,
     &                     80,LFNOUT,IFIL,2,XMIN,1,XMAX,1,YMIN2,1,YMAX2,
     &                     1,YMIN,1,YMAX,1,'energy (eV)',11,YTXT2,
     &                     LYTXT2,YTXT1,LYTXT1,HEADER,LHEADER,
     &                     STRQ1(1:LSTRQ1),LSTRQ1,KNOHEADER)
C
C
            CALL XMGRCURVES(IFIL,2,1,1,2,1,0)
C
C
            LEG(1) = ' '
            LEG2(1) = ' '
            CALL XMGRLEGEND(IFIL,2,1,1,LEG2,LEG)
C
            DO J = 1,NEMAX
               WORK(J) = (W0X(J,IGEO,0,0,1)-W0X(J,IGEO,0,0,2))
            END DO
            CALL XMGR4TABLE(0,0,E,WORK(1),1.0,NE,IFIL)
C
            DO J = 1,NEMAX
               WORK(J) = (W0X(J,IGEO,0,0,1)+W0X(J,IGEO,0,0,2))
            END DO
            CALL XMGR4TABLE(1,0,E,WORK(1),1.0,NE,IFIL)
            WRITE (6,*) 'CL-XPS written in ',FNOUT(1:LFNOUT)
            CLOSE (IFIL)
         ELSE IF ( MLD ) THEN
            IFIL = 88
            YTXT1 = 'Intensity (arb. units)'
            LYTXT1 = 23
            YTXT2 = 'Dichroism (arb. units)'
            LYTXT2 = 23
C
            YMAX = -1D-10
            YMIN = 1D10
            DO J = 1,NE
               IF ( YMAX.LT.(W0X(J,IGEO,0,0,1)) )
     &              YMAX = (W0X(J,IGEO,0,0,1))
               IF ( YMIN.GT.(W0X(J,IGEO,0,0,1)) )
     &              YMIN = (W0X(J,IGEO,0,0,1))
            END DO
            YMAX2 = -1D10
            YMIN2 = 1D10
            DO IE = 1,NE
               IF ( YMAX2.LT.(-W0X(IE,IGEO,0,1,1)+W0X(IE,IGEO,0,2,1)) )
     &              YMAX2 = (-W0X(IE,IGEO,0,0,2)+W0X(IE,IGEO,0,0,1))
               IF ( YMIN2.GT.(-W0X(IE,IGEO,0,2,1)+W0X(IE,IGEO,0,1,1)) )
     &              YMIN2 = (-W0X(IE,IGEO,0,2,1)+W0X(IE,IGEO,0,1,1))
            END DO
C
            STRQ = SYSTEM(1:LSYSTEM)//'.igeo_'
            LL = LSYSTEM + 6
            CALL ADDNTOSTR(STRQ,IGEO,LL)
            LL = LNGSTRING(STRQ,80)
            STRQ1 = 'Linear dichroism in CL-XPS of '//SYSTEM(1:LSYSTEM)
     &              //' for E_phot='//EPHOTCHAR(LINEPHOT:LOUTEPHOT)
     &              //'eV'
            LSTRQ1 = LNGSTRING(STRQ,80)
C
            CALL XMGR4HEAD(DATASET,LDATASET,STRQ(1:LL),LL,' ',0,FNOUT,
     &                     80,LFNOUT,IFIL,2,XMIN,1,XMAX,1,YMIN2,1,YMAX2,
     &                     1,YMIN,1,YMAX,1,'energy (eV)',11,YTXT2,
     &                     LYTXT2,YTXT1,LYTXT1,HEADER,LHEADER,
     &                     STRQ1(1:LSTRQ1),LSTRQ1,KNOHEADER)
C
C
            CALL XMGRCURVES(IFIL,2,1,1,2,1,0)
C
C
            LEG(1) = ' '
            LEG2(1) = ' '
            CALL XMGRLEGEND(IFIL,2,1,1,LEG2,LEG)
C
            DO J = 1,NEMAX
               WORK(J) = (W0X(J,IGEO,0,1,1)-W0X(J,IGEO,0,2,1))
            END DO
            CALL XMGR4TABLE(0,0,E,WORK(1),1.0,NE,IFIL)
C
            DO J = 1,NEMAX
               WORK(J) = (W0X(J,IGEO,0,0,1))
            END DO
            CALL XMGR4TABLE(1,0,E,WORK(1),1.0,NE,IFIL)
            WRITE (6,*) 'MLD CL-XPS written in ',FNOUT(1:LFNOUT)
            CLOSE (IFIL)
         END IF
C
      END DO
C
C=======================================================================
      STOP
 200  CONTINUE
      STOP ' input file empty '
C
C     ==================================================================
99001 FORMAT (' ICST=',I2,'  MS=',F4.1,'  IGEO=',I2,'  E(FIN)=',2F10.5,
     &        ' RY     W(+,-,z)=',3(E14.7))
99002 FORMAT ('1',//,4X,' CORE quantum-numbers  N=',I2,'  L=',I2,/)
99003 FORMAT (////,8X,I4,/,8X,20I4,///)
99004 FORMAT (5I4,2X,I4,F12.6,F12.4,F12.3)
99005 FORMAT (//,' CORE STATES :',//,' NCST:  ',I4,/,' NKPCOR:',20I4)
99006 FORMAT (/,' ICST  N   L  KAP  MUE  IKM     NORM   ',
     &        '     E(Ry)       E(eV)')
99007 FORMAT (5I4,'/2',I4,F12.6,F12.4,F12.3)
99008 FORMAT (22X,I4,F12.6)
99009 FORMAT (10X,I10)
99010 FORMAT (10X,6F10.5)
99011 FORMAT (10X,A)
C
99012 FORMAT (3I4,4X,F10.3,1X,F8.2,2X,F8.2)
99013 FORMAT (4(E14.7,1x))
99014 FORMAT (11(E14.7,1x))
C
99015 FORMAT (A,I4,:,/,A,I4,/)
      END
