C*==rdhead.f    processed by SPAG 6.05Rc at 21:11 on 30 Oct 2001
      SUBROUTINE RDHEAD1(IFIL,NQ,NT,NE,NL)

C **********************************************************************
C *                                                                    *
C *       read the standard head of the input data file                *
C *       setup array dimentions
C *                                                                    *
C **********************************************************************
C
C
C
      CHARACTER*80 HEADER,INFO,SYSTEM 
      CHARACTER*80 LINE
      INTEGER NLQ(:)
      ALLOCATABLE NLQ

      READ (IFIL,99001) HEADER
      READ (IFIL,99001) SYSTEM
      READ (IFIL,99002) NQ
      READ (IFIL,99002) NT
      READ (IFIL,99002) 
      READ (IFIL,99002) NE
      READ (IFIL,99002) IREL
      READ (IFIL,99003) EF
      READ (IFIL,99001) INFO
      READ (IFIL,99005)

      ALLOCATE(NLQ(NQ))
C
C ----------------------------------------------------------------------
      READ (IFIL,99001)
C
      OPEN (88,STATUS='SCRATCH')
C
      DO IQP = 1,NQ
         READ (IFIL,'(A)') LINE
         REWIND 88
         WRITE (88,'(A)') LINE(2:80)
         REWIND 88
         READ (88,*) IQ,NLQ(IQP)
         NL=MAX(NL,NLQ(IQP))
      END DO
C ----------------------------------------------------------------------
C
99001 FORMAT (10X,A80)
99002 FORMAT (10X,I10)
99003 FORMAT (10X,F10.5)
99004 FORMAT (1X,I4,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99005 FORMAT (10X,A10)
      END
