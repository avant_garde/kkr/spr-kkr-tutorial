C*==plotband.f    processed by SPAG 6.05Rc at 23:06 on 30 Dec 2001
      SUBROUTINE PLOTBAND(IFIL,HEADER,LHEADER,SYSTEM,LSYSTEM,INFO,LINFO,
     &                    DATASET,LDATASET,IREL,EFERMI,KNOHEADER,EMIN,
     &                    EMAX,RYOVEV,EREFER,EUNIT)
C   ********************************************************************
C   *                                                                  *
C   *  plot the dispersion relations  E(k)                             *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT REAL (A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      CHARACTER*(*) DATASET
      REAL EFERMI,EMAX,EMIN,EREFER,RYOVEV
      CHARACTER*2 EUNIT
      CHARACTER*80 HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSYSTEM,NBANDMAX,
     &        NKDIRMAX,NKMAX
      LOGICAL KNOHEADER
C
C Local variables
C
      CHARACTER C1
      REAL DEL,DY,EJK,EK(:,:,:),EKMAX,EKMIN,ERM(:),
     &     ETAB(:),KP(:),SUMDK,TOL,XLBL(:,:),
     &     XMAX,XMIN
      CHARACTER*10 DUM
      CHARACTER*80 FILNAM,KLBL(:,:)
      INTEGER I,IC,ID,II,IK,ILB,INDKDIR(:),IOK(:),IP,
     &        ISUM1,ISUM2,J,LB,LFN,LNGKLBL(:,:),N,NBAND,
     &        NBANDK(:,:),NHIT,NKDIR,NKPT,NN,NRM(:),NRMTOT,
     &        NTAB(:),NSPIN,ISPIN,LTXTBND
      CHARACTER*8 LBLKDIR(:),TXTBND
      INTEGER NINT
      REAL REAL

      ALLOCATABLE ETAB,KLBL,NTAB,XLBL,EK,KP,NBANDK,IOK,ERM,NRM
      ALLOCATABLE LBLKDIR,INDKDIR,LNGKLBL
C
C*** End of declarations rewritten by SPAG
C
      NSPIN=1
      IF (IREL.EQ.2) NSPIN=2

      READ (IFIL,'(A)') DUM
      READ (IFIL,99017) NKDIR
      NKDIRMAX=NKDIR
      ALLOCATE (LBLKDIR(NKDIRMAX),INDKDIR(NKDIRMAX))
      ALLOCATE (LNGKLBL(NKDIRMAX,3),KLBL(NKDIRMAX,3))
      DO I = 1,NKDIR
         READ (IFIL,99001) LBLKDIR(I)
      END DO
      READ (IFIL,'(A)') DUM
      DO I = 1,NKDIR
         READ (IFIL,99017) INDKDIR(I)
      END DO
      READ (IFIL,'(A)') DUM
      READ (IFIL,99017) NKPT
      NKMAX=NKPT
      ALLOCATE (KP(NKMAX),NBANDK(NKMAX,2))
      
      IF ( NKPT.GT.NKMAX ) STOP '<plotband> NKPT > NKMAX'
      DO IK = 1,NKPT
         READ (IFIL,99002) KP(IK)
      END DO
      READ (IFIL,'(A)') DUM
      READ (IFIL,99017) NBAND
      NBANDMAX=NBAND
      ALLOCATE (ETAB(NBANDMAX*NKMAX))
      ALLOCATE (NTAB(NBANDMAX*NKMAX))
      ALLOCATE (EK(NBANDMAX,NKMAX,2))
      ALLOCATE (XLBL(NKDIRMAX,3))
      ALLOCATE (IOK(NBANDMAX),ERM(NBANDMAX),NRM(NBANDMAX))
C
      IF ( NBAND.GT.NBANDMAX ) STOP '<plotband> nband  nbandmax'
C
      DO ISPIN=1,NSPIN
          DO IK = 1,NKPT
              READ (IFIL,'(2I5)') J,NBANDK(IK,ISPIN)
              READ (IFIL,'(10F8.4)') (EK(J,IK,ISPIN),J=1,NBANDK(IK,ISPIN
     &             ))
          END DO
C
C-----------------------------------------------------------------------
C                      find spurious eigen values
C-----------------------------------------------------------------------
C
      N = 0
      ETAB(1) = 0.0
      NTAB(1) = 0
      TOL = 2E-4
      ISUM1 = 0
      DO IK = 1,NKPT
         ISUM1 = ISUM1 + NBANDK(IK,ISPIN)
         DO J = 1,NBANDK(IK,ISPIN)
            EJK = EK(J,IK,ISPIN)
            DO I = 1,N
               DEL = EJK - ETAB(I)
               IF ( DEL.LT.TOL ) THEN
                  IF ( ABS(DEL).LT.TOL ) THEN
                     NTAB(I) = NTAB(I) + 1
                  ELSE
                     IP = N + 1
                     DO II = I,N
                        ETAB(IP) = ETAB(IP-1)
                        NTAB(IP) = NTAB(IP-1)
                        IP = IP - 1
                     END DO
                     ETAB(I) = EJK
                     NTAB(I) = 1
                  END IF
                  GOTO 50
               END IF
            END DO
            N = N + 1
            ETAB(N) = EJK
            NTAB(N) = 1
 50      END DO
      END DO
C
      ISUM2 = 0
      NN = 0
      DO I = 1,N
         ISUM2 = ISUM2 + NTAB(I)
         IF ( REAL(NTAB(I))/REAL(NKPT).GT.0.8 ) THEN
            NN = NN + 1
            ERM(NN) = ETAB(I)
            NRM(NN) = NINT(REAL(NTAB(I))/REAL(NKPT))
         END IF
      END DO
      NRMTOT = 0
      DO I = 1,NN
         NRMTOT = NRMTOT + NRM(I)
         WRITE (*,*) ERM(I),NRM(I)
      END DO
      WRITE (*,*) NN,' spurious eigen values -- in total ',NRMTOT
C
      DO IK = 1,NKPT
         DO J = 1,NBANDK(IK,ISPIN)
            IOK(J) = 1
         END DO
C
         I = 1
         NHIT = 0
         DO J = 1,NBANDK(IK,ISPIN)
            EJK = EK(J,IK,ISPIN)
C
 60         CONTINUE
            DEL = EJK - ERM(I)
C
            IF ( DEL.GT.-TOL ) THEN
               IF ( ABS(DEL).LT.TOL ) THEN
                  IOK(J) = 0
                  NHIT = NHIT + 1
                  IF ( NHIT.EQ.NRM(I) ) THEN
                     I = I + 1
                     NHIT = 0
                     GOTO 60
                  END IF
               ELSE IF ( I.LT.NN ) THEN
                  I = I + 1
                  GOTO 60
               END IF
            END IF
         END DO
C
         I = 0
         DO J = 1,NBANDK(IK,ISPIN)
            IF ( IOK(J).EQ.1 ) THEN
               I = I + 1
               EK(I,IK,ISPIN) = EK(J,IK,ISPIN)
            END IF
         END DO
C
         IF ( (NBANDK(IK,ISPIN)-I).GT.NRMTOT ) WRITE (*,*)
     &        ' inconsistent removal of E-value',IK,NBANDK(IK,ISPIN),I,
     &        (NBANDK(IK,ISPIN)-I),NRMTOT
         NBANDK(IK,ISPIN) = I
      END DO
C
C   --------------------------------------------------------------------
C
      IF ( EUNIT.EQ.'eV' ) THEN
         DO IK = 1,NKPT
            DO J = 1,NBANDK(IK,ISPIN)
               EK(J,IK,ISPIN) = EK(J,IK,ISPIN)*RYOVEV
            END DO
         END DO
      END IF
C
      EKMAX = -9999
      EKMIN = +9999
      DO IK = 1,NKPT
         DO J = 1,NBANDK(IK,ISPIN)
            EK(J,IK,ISPIN) = EK(J,IK,ISPIN) - EREFER
            EKMAX = MAX(EKMAX,EK(J,IK,ISPIN))
            EKMIN = MIN(EKMIN,EK(J,IK,ISPIN))
         END DO
      END DO
C
      IF ( ABS(EMIN-9999.).LT.1.0 ) EMIN = EKMIN
      IF ( ABS(EMAX-9999.).LT.1.0 ) EMAX = EKMAX
C ......................................................................
C
      WRITE (6,99003) EREFER,EUNIT,EKMIN,EKMAX,EUNIT,EMIN,EMAX,EUNIT
C   --------------------------------------------------------------------
      SUMDK = 0.0
C
      DO ID = 1,NKDIR
         IF ( ID.EQ.1 ) THEN
            XLBL(ID,1) = 0.0
         ELSE
            XLBL(ID,1) = KP(INDKDIR(ID-1)+1)
         END IF
         XLBL(ID,3) = KP(INDKDIR(ID))
         XLBL(ID,2) = (XLBL(ID,3)+XLBL(ID,1))/2.0
         SUMDK = SUMDK + (XLBL(ID,3)-XLBL(ID,1))
         DO LB = 1,3
            I = LB*3 - 2
            C1 = LBLKDIR(ID)(I:I)
            IF ( (C1.EQ.'G' .OR. C1.EQ.'g') ) THEN
               KLBL(ID,LB) = '!x'//LBLKDIR(ID)((I+1):(I+1))//'!f{}'
               LNGKLBL(ID,LB) = 2 + 1 + 4
            ELSE
               KLBL(ID,LB) = C1
               LNGKLBL(ID,LB) = 1
            END IF
         END DO
         IF ( ID.GT.1 ) THEN
            IF ( KLBL(ID,1).EQ.KLBL(ID-1,3) ) THEN
               KLBL(ID,1) = ' '
               LNGKLBL(ID,1) = 1
            ELSE
C               KLBL(ID-1,3) = KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))//'   '
C               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 3
C               KLBL(ID,1) = '   '//KLBL(ID,1)(1:LNGKLBL(ID,1))
C               LNGKLBL(ID,1) = LNGKLBL(ID,1) + 3
C
C------------- combine 2 k-labels on seg ID-1 and delete that for seg ID
C
               KLBL(ID-1,3) = 
     &         KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))//' '//
     &         KLBL(ID,1)(1:LNGKLBL(ID,1))
               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 1 + LNGKLBL(ID,1)
               KLBL(ID,1) = ' '
               LNGKLBL(ID,1) = 1
            END IF
         END IF
      END DO
      END DO
C
C ======================================================================
C                              XMGR - output
C ======================================================================
      IF ( EUNIT.EQ.'eV' ) THEN
         DY = 2.5
      ELSE
         DY = 0.2
      END IF
C
C ----------------------------------------------------------------------
      IFIL = 90
C
      XMIN = KP(1)
      XMAX = KP(NKPT)
C
      DO ISPIN=1,NSPIN
          
          IF (NSPIN.EQ.1) THEN
              TXTBND(1:3)='bnd'
              LTXTBND=3
          ELSE
              IF (ISPIN.EQ.1) TXTBND(1:6)= 'bnd_up'
              IF (ISPIN.EQ.2) TXTBND(1:6)= 'bnd_dn'
              LTXTBND=6
          END IF
            
          CALL XMGR4HEAD(DATASET,LDATASET,TXTBND,LTXTBND,' ',0,FILNAM,80
     &         ,LFN,IFIL,1,XMIN,0,XMAX,0,EMIN,1,EMAX,0,0.0,0,0.0,0
     &         ,'wave vector k',13,'E(k) (eV)',9,' ',0,HEADER,LHEADER
     &         ,'dispersion relation of '//SYSTEM(1:LSYSTEM),(23+LSYSTEM
     &         ),KNOHEADER)
C
      WRITE (IFIL,99014) 'xaxis  ticklabel on'
      WRITE (IFIL,99014) 'xaxis  ticklabel type spec'
      WRITE (IFIL,99014) 'xaxis  tick minor off'
      WRITE (IFIL,99014) 'xaxis  tick type spec'
      WRITE (IFIL,99015) 'xaxis  tick spec ',3*NKDIR
      WRITE (IFIL,99015) 'xaxis  ticklabel place on ticks'
C
      ILB = -1
      DO ID = 1,NKDIR
         DO LB = 1,3
            ILB = ILB + 1
            WRITE (IFIL,99004) ILB,XLBL(ID,LB)
            CALL XMGRWRITXT(IFIL,'xaxis  ticklabel ',ILB,KLBL(ID,LB),
     &                      LNGKLBL(ID,LB),'\\')
         END DO
      END DO
C
      WRITE (IFIL,99016) 0,' symbol 2'
      WRITE (IFIL,99016) 0,' symbol size 0.200000'
      WRITE (IFIL,99016) 0,' symbol fill 1'
C        WRITE (IFIL,99018) 0,' symbol color 8'
      WRITE (IFIL,99016) 0,' symbol linewidth 1'
      WRITE (IFIL,99016) 0,' symbol linestyle 1'
      WRITE (IFIL,99016) 0,' symbol center true'
      WRITE (IFIL,99016) 0,' symbol char 0'
      WRITE (IFIL,99016) 0,' linestyle 0'
C
      WRITE (IFIL,99011) 0,0
C
      DO IK = 1,NKPT
         DO J = 1,NBANDK(IK,ISPIN)
            WRITE (IFIL,99012) KP(IK),EK(J,IK,ISPIN)
         END DO
      END DO
C
C ----------------------------------------------------------------------
C                                 draw vertical lines at symmetry points
      WRITE (IFIL,99013)
C
      IC = 0
      DO ID = 2,NKDIR
         IC = IC + 1
         WRITE (IFIL,99016) IC,' linestyle 1'
         WRITE (IFIL,99016) IC,' color 1'
         WRITE (IFIL,99011) 0,0
         WRITE (IFIL,99012) XLBL(ID,1),EMIN - 0.15*(EMAX-EMIN)
         WRITE (IFIL,99012) XLBL(ID,1),EMAX + 0.15*(EMAX-EMIN)
         WRITE (IFIL,99013)
      END DO
C
C ----------------------------------------------------------------------
C                                   draw horizontal lines at Fermi level
      WRITE (IFIL,99013)
C
      IC = IC + 1
      WRITE (IFIL,99016) IC,' linestyle 1'
      WRITE (IFIL,99016) IC,' color 1'
      WRITE (IFIL,99012) XMIN,0.0
      WRITE (IFIL,99012) XMAX,0.0
      WRITE (IFIL,99013)
C
      WRITE (6,*) '  '
      WRITE (6,*) '   E(k)-rel written to file  ',FILNAM(1:LFN)
C
      CLOSE (IFIL)
      END DO
      STOP
99001 FORMAT (10X,A)
99002 FORMAT (8F10.4)
99003 FORMAT (/,'   reference energy:      ',8X,F8.3,1X,A,/,
     &        '   energy range of bands: ',2F8.3,1X,A,/,
     &        '   plot range           : ',2F8.3,1X,A)
99004 FORMAT ('@    xaxis  tick ',I2,', ',f6.3)
99011 FORMAT ('@WITH G',I1,/,'@G',I1,' ON',/,'@TYPE xy')
99012 FORMAT (2E15.7)
99013 FORMAT ('&')
99014 FORMAT ('@    ',A)
99015 FORMAT ('@    ',A,I3)
99016 FORMAT ('@    s',I1,A)
99017 FORMAT (10X,I10)
      END
C*==xmgrwritxt.f    processed by SPAG 6.05Rc at 23:06 on 30 Dec 2001
      SUBROUTINE XMGRWRITXT(IFIL,T1,I,TXT,LTXT,ESC)
C **********************************************************************
C *                                                                    *
C *  write integer  I  and text string TXT  - modify TXT if necessary  *
C *                                                                    *
C *  when calling XMGRWRITXT  ESC  should be set to '\\'               *
C *  for any compiler / machine this should be converted to            *
C *  have the escape character  '\'  in the xmgr-file at the end       *
C *                                                                    *
C **********************************************************************
      IMPLICIT REAL (A-H,O-Z)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LSTRMAX
      PARAMETER (LSTRMAX=80)
C
C Dummy arguments
C
      CHARACTER*1 ESC
      INTEGER I,IFIL,LTXT
      CHARACTER*(*) T1,TXT
C
C Local variables
C
      INTEGER L
      CHARACTER*(LSTRMAX) T
C
C*** End of declarations rewritten by SPAG
C
      IF ( LTXT.LE.0 ) RETURN
C
      T = TXT
      L = LTXT
C
C ----------------------------------------------- reset to original font
      CALL XMGRSUBS(T,L,'!F',2,ESC//'f{}',4)
C
C ------------------------------------ replace macros for spin UP and DN
      CALL XMGRSUBS(T,L,'!UP',3,ESC//'x'//ESC//'c-'//ESC//'C'//ESC//
     &              'f{0}',12)
C
      CALL XMGRSUBS(T,L,'!DN',3,ESC//'x'//ESC//'c/'//ESC//'C'//ESC//
     &              'f{0}',12)
C
C --------------------------- replace '!' by the proper escape character
      CALL XMGRSUBS(T,L,'!',1,ESC,1)
C
      WRITE (IFIL,99001) T1,I,T(1:L)
99001 FORMAT ('@   ',A,2X,I3,',   "',A,'"')
      END
