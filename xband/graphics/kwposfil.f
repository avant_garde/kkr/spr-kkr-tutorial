      SUBROUTINE KWPOSFIL( IFIL, KW, LKW, ISTOP, ICASE, IPOS )
C   ********************************************************************
C   *                                                                  *
C   *   position file  IFIL  to line starting with string  KW10        *
C   *   IN:   IFIL                                                     *
C   *         KW        keyword                                        *
C   *         LKW       length of keyword   L <= 10                    *
C   *         ISTOP     0=    CONTINUE if string is not found          *
C   *                   else= STOP                                     *
C   *         ICASE     0=    ACCEPT lower and upper case characters   *
C   *                   else= DON'T ACCEPT                             *
C   *   OUT:  IPOS      0= string NOT found    1=found                 *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT REAL*8   (A-H,O-Z)

      CHARACTER*10 KW, KWP, KWIN
                      
      REWIND ( UNIT=IFIL, ERR=9998 )

      IF( LKW .GT. 10 ) THEN
         WRITE(*,*) ' WARNING from <KWPOSFIL> '
         WRITE(*,*) ' 10 < LKW=',LKW
         LKWP = 10              
      ELSE
         LKWP = LKW
      END IF
               
      KWP = KW  
      IF( ICASE .EQ. 0 ) CALL CNVTOUC(KWP,1,LKWP)

   10 READ( IFIL, FMT='(A)', END=9999 ) KWIN

      IF( ICASE .EQ. 0 ) CALL CNVTOUC(KWIN,1,LKWP)

      IF( KWP(1:LKWP) .EQ. KWIN(1:LKWP) ) THEN
         IPOS = 1
         RETURN
      ELSE
         GOTO 10
      END IF

 9999 IPOS = 0          
      
      IF( ISTOP .NE. 0 ) THEN
         WRITE(*,*)' STOP in <KWPOSFIL>'
         WRITE(*,*)' KW ',KW,' not found in file ', IFIL
         STOP
      ELSE
         REWIND IFIL
         RETURN
      END IF

 9998 WRITE(*,*) ' WARNING from <KWPOSFIL> ********************'
      WRITE(*,*) ' unit ',IFIL,' not connected -- REWIND failed'
      IPOS = 0
      RETURN 
      END  

