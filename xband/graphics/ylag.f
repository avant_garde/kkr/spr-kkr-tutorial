
      REAL FUNCTION YLAG(XI,X,Y,IND1,N1,IMAX,IEX)
C = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
C     lagrangian interpolation
C     XI     intepolated entry into x-array
C     N      order of lagrangran interpolation
C     Y      array from which YLAG is obtained by interpolation
C     IND    the min-I for X(I) > XI
C     IND=0  X-array will be searched
C     IMAX   max index of X-and Y-arrays
C     extrapolation can occur,  IEX=-1 or +1

      IMPLICIT REAL (A-H,O-Z)
 
      DIMENSION X(IMAX),Y(IMAX)  
      SAVE
            
c     if( ind1 .ne. 0 .and. ind1 .ne. imax-n1+1) then                 
c     ii = max( 2, ind1 )
c     if( xi .gt. x(ii) .or. xi .lt. x(ii-1) )
c    _ write(*,*)' YLAG : ', ind1, x(ii-1),xi,x(ii)
c     end if

      IND=IND1
      N=N1
      IEX=0
      IF (N.LE.IMAX) GO TO 10
      N=IMAX
      IEX=N
   10 IF (IND.GT.0) GO TO 40
      DO 20 J = 1,IMAX
      IF( ABS(XI-X(J)).LT.1.0E-6) GO TO 130
         IF (XI-X(J)) 30,130,20
   20    CONTINUE
      IEX=1
      GO TO 70
   30 IND=J
   40 IF (IND.GT.1) GO TO 50
      IEX=-1
   50 INL=IND-(N+1)/2
      IF (INL.GT.0) GO TO 60
      INL=1
   60 INU=INL+N-1
      IF (INU.LE.IMAX) GO TO 80
   70 INL=IMAX-N+1
      INU=IMAX
   80 S=0.0E0
      P=1.0E0
      DO 110 J=INL,INU
         P=P*(XI-X(J))
         D=1.0E0
         DO 100 I=INL,INU
            IF (I.NE.J) GO TO 90
            XD=XI
            GO TO 100
   90       XD=X(J)
  100       D=D*(XD-X(I))
  110    S=S+Y(J)/D
      YLAG=S*P
  120 RETURN
  130 YLAG=Y(J)
      GO TO 120
      END
