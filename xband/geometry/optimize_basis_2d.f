C*==optimize_basis_2d.f    processed by SPAG 6.70Rc at 19:39 on  3 Mar 2013
      SUBROUTINE OPTIMIZE_BASIS_2D(ABAS,QBAS,NQ,NQMAX,IFLAG)
C   ********************************************************************
C   *                                                                  *
C   *  optimize the list of basis vectors                              *
C   *  to minimize the interatomic distances                           *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 TOL
      PARAMETER (TOL=1D-8)
C
C Dummy arguments
C
      INTEGER IFLAG,NQ,NQMAX
      REAL*8 ABAS(3,3),QBAS(3,NQMAX)
C
C Local variables
C
      LOGICAL CHANGES_MADE
      REAL*8 DNRM2
      REAL*8 DQIJ,DQIJMAX_NEW,DQIJMAX_OLD,DQIPJMAX_TMP,QBAS_OLD(:,:),
     &       QIJVEC(3),QIPVEC(1:3)
      INTEGER I,I1,I123TOP,I1MIN,I2,I2MIN,I3,I3MIN,IQ,JQ
C
C*** End of declarations rewritten by SPAG
C
      ALLOCATABLE QBAS_OLD
C
      IF ( NQ.EQ.1 ) RETURN
C
      ALLOCATE (QBAS_OLD(3,NQ))
C
      QBAS_OLD(1:3,1:NQ) = QBAS(1:3,1:NQ)
C
C-----------------------------------------------------------------------
C modify the list of basis vectors to minimize the interatomic distance
C-----------------------------------------------------------------------
C
      DQIJMAX_OLD = 0D0
      DO IQ = 1,NQ
         DO JQ = IQ + 1,NQ
            QIJVEC(1:3) = QBAS(1:3,IQ) - QBAS(1:3,JQ)
            DQIJ = DNRM2(3,QIJVEC,1)
            DQIJMAX_OLD = MAX(DQIJ,DQIJMAX_OLD)
         END DO
      END DO
C
      CHANGES_MADE = .FALSE.
      DQIJMAX_NEW = DQIJMAX_OLD
      I123TOP = 2
C
      DO IQ = 2,NQ
C
         IFLAG = 0
C
         DO I1 = I123TOP, - I123TOP, - 1
            DO I2 = I123TOP, - I123TOP, - 1
               DO I3 = I123TOP, - I123TOP, - 1
C
                  QIPVEC(1:3) = QBAS(1:3,IQ) + I1*ABAS(1:3,1)
     &                          + I2*ABAS(1:3,2) + I3*ABAS(1:3,3)
C
                  DQIPJMAX_TMP = 0D0
                  DO JQ = 1,IQ - 1
                     QIJVEC(1:3) = QIPVEC(1:3) - QBAS(1:3,JQ)
                     DQIJ = DNRM2(3,QIJVEC,1)
                     DQIPJMAX_TMP = MAX(DQIJ,DQIPJMAX_TMP)
                  END DO
C
                  IF ( DQIPJMAX_TMP.LT.(DQIJMAX_NEW-TOL) ) THEN
                     DQIJMAX_NEW = DQIPJMAX_TMP
                     I1MIN = I1
                     I2MIN = I2
                     I3MIN = I3
                     IFLAG = 1
                     CHANGES_MADE = .TRUE.
                  END IF
C
               END DO
            END DO
         END DO
C
         IF ( IFLAG.EQ.1 ) QBAS(1:3,IQ) = QBAS(1:3,IQ)
     &        + I1MIN*ABAS(1:3,1) + I2MIN*ABAS(1:3,2)
     &        + I3MIN*ABAS(1:3,3)
C
      END DO
C
      IF ( .NOT.CHANGES_MADE ) THEN
C
         WRITE (6,99004) DQIJMAX_NEW
C
         IFLAG = 0
C
      ELSE
C
         IFLAG = 1
C
         WRITE (6,99001)
         DO IQ = 1,NQ
            WRITE (6,99002) (QBAS_OLD(I,IQ),I=1,3),(QBAS(I,IQ),I=1,3)
         END DO
         WRITE (6,99003) DQIJMAX_OLD,DQIJMAX_NEW
C
      END IF
C
      IF ( DQIJMAX_NEW.GT.(DQIJMAX_OLD+TOL) ) THEN
         WRITE (6,99005)
         IFLAG = -1
      END IF
C
99001 FORMAT (//,2(1X,79('*'),/),30X,'<OPTIMIZE_BASIS_2D>',/,
     &        2(1X,79('*'),/),//,15X,'OLD basis vectors',21X,
     &        'NEW basis vectors '/)
99002 FORMAT (2X,2(3X,'(',F10.5,',',F10.5,',',F10.5,' )'))
99003 FORMAT (/,10X,'largest distance between basis sites',/,10X,
     &        'originally:      ',F10.5,' a.u.',/,10X,
     &        'optimized:       ',F10.5,' a.u.',/)
99004 FORMAT (//,2(1X,79('*'),/),30X,'<OPTIMIZE_BASIS_2D>',/,
     &        2(1X,79('*'),/),//,10X,'no changes done on basis vectors',
     &        /,10X,'largest distance between basis sites:',F10.5,
     &        ' a.u.',/)
99005 FORMAT (//,2(1X,79('#'),/),10X,'TROUBLE finding optimzed basis',/,
     &        2(1X,79('#'),/),//)
      END
