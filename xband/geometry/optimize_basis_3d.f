C*==optimize_basis_3d.f    processed by SPAG 6.70Rc at 16:18 on  4 Mar 2013
      SUBROUTINE OPTIMIZE_BASIS_3D(ABAS,QBAS,NQ,NQMAX,IFLAG)
C   ********************************************************************
C   *                                                                  *
C   *  optimize the list of basis vectors   ->q_i                      *
C   *  to have the largest interatomic distance |->q_i - ->q_j|        *
C   *  as small as possible                                            *
C   *                                                                  *
C   *  IFLAG = 0:   no changes done                                    *
C   *          1:   changes done                                       *
C   *         -1:   changes done - result questionable                 *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 TOL
      PARAMETER (TOL=1D-10)
C
C Dummy arguments
C
      INTEGER IFLAG,NQ,NQMAX
      REAL*8 ABAS(3,3),QBAS(3,NQMAX)
C
C Local variables
C
      LOGICAL CHANGES_MADE
      REAL*8 DNRM2
      REAL*8 DQIJ,DQIJMAX_NEW,DQIJMAX_OLD,DQIJMAX_Q,DQIPJMAX,
     &       QBAS_OLD(:,:),QIJVEC(3),QIPVEC(1:3)
      INTEGER I,I1,I123TOP,I1MIN,I2,I2MIN,I3,I3MIN,IQ,JQ
C
C*** End of declarations rewritten by SPAG
C
      ALLOCATABLE QBAS_OLD
C
      IFLAG = 0
C
      IF ( NQ.EQ.1 ) RETURN
C
      ALLOCATE (QBAS_OLD(3,NQ))
C
      QBAS_OLD(1:3,1:NQ) = QBAS(1:3,1:NQ)
C
C-----------------------------------------------------------------------
C          find max. distance for old set of basis vectors
C-----------------------------------------------------------------------
C
      DQIJMAX_OLD = 0D0
      DO IQ = 1,NQ
         DO JQ = IQ + 1,NQ
            QIJVEC(1:3) = QBAS(1:3,IQ) - QBAS(1:3,JQ)
            DQIJ = DNRM2(3,QIJVEC,1)
            DQIJMAX_OLD = MAX(DQIJ,DQIJMAX_OLD)
         END DO
      END DO
C
C-----------------------------------------------------------------------
C          find MINIMUM for max. distance for set of basis vectors
C-----------------------------------------------------------------------
C
      DQIJMAX_NEW = 0D0
      CHANGES_MADE = .FALSE.
      I123TOP = 2
C----------------------------------------------------- scan all sites IQ
      DO IQ = 2,NQ
C
         DQIJMAX_Q = 1D+20
C
C------------------------------------------- scan neighboring unit cells
         DO I1 = I123TOP, - I123TOP, - 1
            DO I2 = I123TOP, - I123TOP, - 1
               DO I3 = I123TOP, - I123TOP, - 1
C
                  QIPVEC(1:3) = QBAS(1:3,IQ) + I1*ABAS(1:3,1)
     &                          + I2*ABAS(1:3,2) + I3*ABAS(1:3,3)
C
C---------------------------------------- scan all sites JQ done (JQ<IQ)
                  DQIPJMAX = 0D0
                  DO JQ = 1,IQ - 1
                     QIJVEC(1:3) = QIPVEC(1:3) - QBAS(1:3,JQ)
                     DQIJ = DNRM2(3,QIJVEC,1)
                     DQIPJMAX = MAX(DQIJ,DQIPJMAX)
                  END DO
C
                  IF ( DQIPJMAX.LT.(DQIJMAX_Q-TOL) ) THEN
                     DQIJMAX_Q = DQIPJMAX
                     I1MIN = I1
                     I2MIN = I2
                     I3MIN = I3
                  END IF
C
               END DO
            END DO
         END DO
C------------------------------------------- scan neighboring unit cells
C
         IF ( I1MIN.NE.0 .OR. I2MIN.NE.0 .OR. I3MIN.NE.0 ) THEN
C
            QBAS(1:3,IQ) = QBAS(1:3,IQ) + I1MIN*ABAS(1:3,1)
     &                     + I2MIN*ABAS(1:3,2) + I3MIN*ABAS(1:3,3)
C
            CHANGES_MADE = .TRUE.
         END IF
C
         DQIJMAX_NEW = MAX(DQIJMAX_Q,DQIJMAX_NEW)
C
      END DO
C----------------------------------------------------- scan all sites IQ
C
      IF ( CHANGES_MADE ) THEN
C
         WRITE (6,99001)
         DO IQ = 1,NQ
            WRITE (6,99002) (QBAS_OLD(I,IQ),I=1,3),(QBAS(I,IQ),I=1,3)
         END DO
         WRITE (6,99003) DQIJMAX_OLD,DQIJMAX_NEW
         IFLAG = 1
C
         IF ( DQIJMAX_NEW.GT.(DQIJMAX_OLD+TOL) ) THEN
            WRITE (6,99005)
            IFLAG = -1
         END IF
C
      ELSE
C
         WRITE (6,99004) DQIJMAX_NEW
C
      END IF
C
99001 FORMAT (//,2(1X,79('*'),/),30X,'<OPTIMIZE_BASIS_3D>',/,
     &        2(1X,79('*'),/),//,15X,'OLD basis vectors',21X,
     &        'NEW basis vectors '/)
99002 FORMAT (2X,2(3X,'(',F10.5,',',F10.5,',',F10.5,' )'))
99003 FORMAT (/,10X,'largest distance between basis sites',/,10X,
     &        'originally:      ',F10.5,' a.u.',/,10X,
     &        'optimized:       ',F10.5,' a.u.',/)
99004 FORMAT (//,2(1X,79('*'),/),30X,'<OPTIMIZE_BASIS_3D>',/,
     &        2(1X,79('*'),/),//,10X,'no changes done on basis vectors',
     &        /,10X,'largest distance between basis sites:',F10.5,
     &        ' a.u.',/)
99005 FORMAT (//,2(1X,79('#'),/),10X,'TROUBLE finding optimzed basis',/,
     &        2(1X,79('#'),/),//)
      END
