# select working directory and system
#
#
# Copyright (C) 1998 H. Ebert

proc select_system {} {

########################################################################################
 proc dirlist_sel_d {d} {
  global Wssys
  update; dirlist_focus $Wssys; $Wssys.c.d.f.f.li delete 0 end; update; dirselected_system $d
 }

########################################################################################
proc dirselected_system {d} {# fills the directory selection box; sets files to default

    global env Wssys working_directory sysfile syssuffix liste mdatsort
    global potsuffix potfile pdatsort
    global sysdirlist

    set cdir [expand_dollar_home $d]

    if {[file isdirectory $cdir]==0} {
	writescr0 .d.tt "\n the directory  $cdir  does not exist"
	give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
	return
    }
    
    if [winfo exists $Wssys] {set w $Wssys.d.tt} else {set w .d.tt}

    if {[catch {cd $cdir} m]!=0} {
	set working_directory [pwd]
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	set working_directory [pwd]
	writescr0 $w "new directory chosen: $working_directory\n\n"
	set sysfile ""
	set potfile "" 
	set potsuffix ".pot"
	if {[winfo exists $Wssys]} {
	    dirlist_anzeige $Wssys
	    
	    $Wssys.c.d.f.f.li delete 0 end
	    $Wssys.c.d.f.f.li insert end "\$HOME" ".."
	    foreach x $sysdirlist { $Wssys.c.d.f.f.li insert end "$x"}
	    foreach k [lsort [glob -nocomplain -- *]] {
		if {[file isdirectory $k]} {$Wssys.c.d.f.f.li insert end $k}
	    }
	    ffuellen $Wssys.c.m.f.f.li $syssuffix $mdatsort
	    ffuellen $Wssys.c.e.f.f.li $potsuffix $pdatsort
	}
	return 1
    }
}


########################################################################################
proc dirlist_sel_m {f} {
    global Wssys syssuffix sysfile liste working_directory 
    global sysdirlist sysdirlistmaxindex DIMENSION

    dirlist_focus $Wssys

    if {[file exists $f]} {
	$Wssys.c.m.i delete 0 end
	regsub "$syssuffix\$" $f "" sysfile
	
	writescr0 $Wssys.d.tt "\nnew sysfile selected and read in:    $sysfile\n\n"
	
	dirlist_anzeige $Wssys
	
	read_sysfile
	
	writescr $Wssys.d.tt "dimension of the system:  $DIMENSION \n\n"
	
	set diraux [collapse_dollar_home $working_directory]
	if {[llength $sysdirlist]>0} {
	    set sysdirlist [update_dirlist $sysdirlist $sysdirlistmaxindex $diraux]
	} else {
	    lappend sysdirlist $diraux
	}
	$Wssys.c.d.f.f.li delete 0 end
	$Wssys.c.d.f.f.li insert end "\$HOME" ".."
	foreach x $sysdirlist { $Wssys.c.d.f.f.li insert end "$x"}
    }

}

########################################################################################
 proc dirlist_sel_m2   {f} {
  global Wssys syssuffix sysfile
  global editor edback edxterm edoptions 
  dirlist_focus $Wssys
  if {[file exists $f]} {
    regsub "$syssuffix\$" $f "" sysfile
    destros $Wssys; unlock_list
  } else {
     give_warning "." "WARNING \n\n the selected system file \n\n $f does not exists \n "
  }
 }

########################################################################################
 proc dirlist_sel_e {f} {
  global Wssys potsuffix potfile
  global editor edback edxterm edoptions 
  dirlist_focus $Wssys
  if {[file exists $f]} {
    $Wssys.c.e.i delete 0 end
    if {$potsuffix==""} {set potfile $f} else {regsub "$potsuffix\$" $f "" potfile}
    writescr0 $Wssys.d.tt "new pot-file selected: $potfile$potsuffix\n\n"; dirlist_anzeige $Wssys
  }
 }

########################################################################################
 proc dirlist_sel_e2   {f} {
  global Wssys potsuffix potfile
  global editor edback edxterm edoptions 
  dirlist_focus $Wssys
  if {[file exists $f]} {
    regsub "$potsuffix\$" $f "" potfile
    destros $Wssys; unlock_list
  } else {
     give_warning "." "WARNING \n\n the selected pot-file file \n\n $f does not exists \n "
  }
 }

########################################################################################
 proc dirlist_sel_m_order {s} {
  global Wssys mdatsort syssuffix
  if {$s=="alphabetical"} {set mdatsort 0} else {set mdatsort 1}
  ffuellen $Wssys.c.m.f.f.li $syssuffix $mdatsort
 }

########################################################################################
 proc dirlist_sel_e_order {s} {
  global Wssys pdatsort potsuffix
  if {$s=="alphabetical"} {set pdatsort 0} else {set pdatsort 1}
  ffuellen $Wssys.c.e.f.f.li $potsuffix $pdatsort
 }

########################################################################################
 proc dirlist_sel_m_suffix {s} {
  global Wssys syssuffix sysfile mdatsort
  $Wssys.c.m.i delete 0 end
  if {$syssuffix!=$s} {set syssuffix $s; set sysfile ""}
  ffuellen $Wssys.c.m.f.f.li $syssuffix $mdatsort
  writescr0 $Wssys.d.tt "new value for sysfile suffix: $syssuffix\n\n"; dirlist_anzeige $Wssys
 }

########################################################################################
 proc dirlist_sel_e_suffix {s} {
  global Wssys potsuffix potfile pdatsort
  $Wssys.c.e.i delete 0 end
  if {$s=="*"} then {set ns ""} else {set ns $s}
  if {$potsuffix!=$ns} {set potfile ""; set potsuffix $ns}
  ffuellen $Wssys.c.e.f.f.li $potsuffix $pdatsort
  writescr0 $Wssys.d.tt "new value for pot-file suffix: *$potsuffix\n\n"; dirlist_anzeige $Wssys
 }


########################################################################################
 proc dirlist_focus {w} {
  global Wssys dirlist_foc
  if     {$dirlist_foc==0} {set r 0} \
  elseif {$dirlist_foc==1} {set r [dirlist_edn]} \
  elseif {$dirlist_foc==2} {set r [dirlist_emfn]} \
  elseif {$dirlist_foc==3} {set r [dirlist_eefn]}
  if {$w!=""} {focus $w} else {focus $Wssys; return $r}
 }

########################################################################################
 proc dirlist_edn {} {# enter directory name
  global Wssys dirlist_foc dirlist_d

  set dirlist_foc 0
  set d [string trim [$Wssys.c.d.i get]]; $Wssys.c.d.i delete 0 end; writescr0 $Wssys.d.tt ""

  if {$d==""} {return 0}
  set dirlist_d [expand_dollar_home $d]

  if {![file isdirectory $dirlist_d] && ![file exists $dirlist_d]} {
      mkdcdfill
  } else {
      dirselected_system $dirlist_d
      dirlist_anzeige $Wssys
  }
  return 1
 }

########################################################################################
 proc dirlist_emfn {} {#                                             enter sysfile name
  global Wssys syssuffix sysfile potfile potsuffix liste dirlist_foc

  set dirlist_foc 0
  set f [string trim [$Wssys.c.m.i get]]; $Wssys.c.m.i delete 0 end; writescr0 $Wssys.d.tt ""
  if {$f=="$syssuffix"} {set f ""; writescr $Wssys.d.tt "filename cannot consist of file suffix only\n"}
  regsub "$syssuffix$" $f "" h; if {"$h$syssuffix"=="$f"} {set f "$h"}
  testfilename $f $Wssys.d.tt $syssuffix;  if {$f==""} {return 0}

  set sysfile $f
  writescr $Wssys.d.tt "input of new sysfile name: $sysfile\n\n"
  dirlist_anzeige $Wssys
  return 1
 }




########################################################################################
 proc dirlist_eefn {} {#                                             enter pot-file name
  global Wssys potsuffix potfile dirlist_foc

  set dirlist_foc 0
  set f [string trim [$Wssys.c.e.i get]]; $Wssys.c.e.i delete 0 end; writescr0 $Wssys.d.tt ""
  if {$f=="$potsuffix"} {set f ""; writescr $Wssys.d.tt "filename cannot consist of file suffix only\n"}
  regsub "${potsuffix}$" $f "" h; if {"$h$potsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wssys.d.tt $potsuffix;  if {$f==""} {return 0}

  set potfile $f
  writescr $Wssys.d.tt "input of new pot-file name: $potfile$potsuffix\n\n" 
  dirlist_anzeige $Wssys
  return 1
 }

########################################################################################
proc handle_system {mode} {#                                            handle_system 
  global sysfile syssuffix Wssys

  set f "$sysfile$syssuffix"

  if {$f==".sys"} {
     writescr0 $Wssys.d.tt "\n no system file selected  \n\n  select first then proceed "
     give_warning .  "WARNING \n\n  no system file selected  \n\n  select first then proceed "
     return
  }
  if {[file exists $f]=="0"} {
     writescr0 $Wssys.d.tt "\n the system file  $f  does not exist"
     give_warning .  "WARNING \n\n  the system file  $f  \n\n does not exist " 
     return
  }

  if {$mode == "modify"} {
      
      modify_system_exec 
      destros $Wssys
      unlock_list
      
  } elseif {$mode == "show"} {
      destros $Wssys
      unlock_list
      sites_graphik
      
  } else {
      puts "unknown mode in handle_system"
  }

}

########################################################################################
proc editfile {f} {#                                                           edit file
  global sysfile potfile potsuffix syssuffix
  global edxterm editor edoptions edback

  if     {$f=="m"} {set ff "$sysfile$syssuffix"}  \
  elseif {$f=="e"} {set ff "$potfile$potsuffix"} \
  else             {set ff ""}

  set ed_file $ff
  eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
  set pid "$message"
}

########################################################################################
 proc dirlist_anzeige {w} {#                                              update display
  global working_directory sysfile syssuffix potfile potsuffix potsuffixl
  $w.b.d.t   configure -text "$working_directory"
  $w.c.m.li  configure -text "sysfile: ($syssuffix)"
  $w.c.e.li  configure -text "pot-file: ([alternative $potsuffix * $potsuffix]):"
  if {$sysfile==""}  {set m "NONE"}                     else {set m "$sysfile$syssuffix"}
  if {$potfile==""}  {set e "NO POT-FILE SELECTED"}     else {set e "$potfile$potsuffix"}
  if {$potsuffix==""} {set potsuffixl "*"} else {set potsuffixl $potsuffix}
  $w.b.m.t        configure -text "$m"
  $w.b.e.t        configure -text "$e"
  $w.c.m.1.1.f.l  configure -text "file list ($syssuffix):" -anchor w
  $w.c.e.1.1.f.l  configure -text "file list ($potsuffixl):" -anchor w
  .c.1.wdir.dir  configure -text "$working_directory"
  .c.1.pfil.dir  configure -text $e
 }

########################################################################################
 proc ffuellen {w s datsort} {#                fill filelist with suffix s into widget w
  global sub
  $w delete 0 end;  set subalt $sub

  if {$datsort==0} {
      set liste [exec ls]
  } else {
      set liste [exec ls -t]
  }
  foreach i $liste {
      if {[file isfile $i]} {if {[string match "*$s" $i]} {$w insert end $i}}
  }
  lock;  set sub $subalt
 }

########################################################################################
 proc mfuellen {w f} {#                             fill sysfile commands into widget w

  $w delete 0 end; 
  set fileId [open $f r]
  set i 0
  set pat {[a-z]*:*}
  $w insert end "$f "

  while {[gets $fileId line] >=0} {
     if {[string match $pat $line]==1} {
        incr i
        string first : $line
        set j [ expr [string first : $line] -1 ]
        set option "[string range $line 0 $j ]"
        $w insert end "$f $option"
      }
  }
  close $fileId

  lock 
 }


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wssys hlp_dir working_directory dirlist_foc sysfile syssuffix potfile potsuffix 
global mdatsort pdatsort sdatsort mfold sysdirlist
global dirlist_sel_m_order_v dirlist_sel_e_order_v dirlist_sel_s_order_v
global editor edback edxterm edoptions
global COLOR WIDTH HEIGHT

set sysfile ""
if {[file exists sysfile]} {set sysfile "sysfile"}

set  potfile ""

toplevel_init $Wssys "SELECT SYSTEM" 0 0;   set working_directory [pwd];   set mfold "$sysfile"


# top buttons

insert_topbuttons $Wssys dirlist_h.hlp
bind $Wssys.a.e <Button-1> {
  if [dirlist_focus ""] {
    writescr $Wssys.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wssys; unlock_list
    if {($mfold!=$sysfile)&&([file exists "${sysfile}.vst"])} {.b.1.vl invoke}
    Bend
  }
}


########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wssys.b   
pack configure $Wssys.b -in $Wssys -anchor w -pady 8
frame $Wssys.b.d; frame $Wssys.b.m; frame $Wssys.b.e
pack configure $Wssys.b.d $Wssys.b.m $Wssys.b.e  -in $Wssys.b -anchor w

label $Wssys.b.d.l -width 15 -text "current directory" -anchor w
label $Wssys.b.d.t -anchor w
pack configure $Wssys.b.d.l $Wssys.b.d.t -in $Wssys.b.d -anchor w -side left
bind  $Wssys.b.d.l <Button-3> {cathfile0 dirlist_dirdisp $Wssys}
bind  $Wssys.b.d.t <Button-3> {cathfile0 dirlist_dirdisp $Wssys}

label $Wssys.b.m.l -width 15 -anchor w -text "sysfile"
label $Wssys.b.m.t -width 25 -anchor w
button $Wssys.b.m.but -text "edit" -width 10 -height 1  -bg LightBlue1 \
	-command "editfile m" 
button $Wssys.b.m.shw -text "show structure" -width 15 -height 1 -bg green \
	-command [list handle_system show] 
button $Wssys.b.m.mod -text "modify" -width 10 -height 1  -bg green \
	-command [list handle_system modify] 
pack configure $Wssys.b.m.l $Wssys.b.m.t $Wssys.b.m.but $Wssys.b.m.shw $Wssys.b.m.mod \
	-in $Wssys.b.m -anchor w -side left

bind  $Wssys.b.m.l <Button-3> {cathfile0 dirlist_makedisp $Wssys}
bind  $Wssys.b.m.t <Button-3> {cathfile0 dirlist_makedisp $Wssys}


########################################################################################



label $Wssys.b.e.l -width 15 -anchor w -text "potential file"
label $Wssys.b.e.t -width 25 -anchor w
button $Wssys.b.e.but -text "edit" -width 10 -height 1  -bg LightBlue1 \
    -command "editfile e"
pack configure $Wssys.b.e.l $Wssys.b.e.t $Wssys.b.e.but -in $Wssys.b.e -anchor w -side left
bind  $Wssys.b.e.l <Button-3> {cathfile0 dirlist_potdisp $Wssys}
bind  $Wssys.b.e.t <Button-3> {cathfile0 dirlist_potdisp $Wssys}


########################################################################################
#   frame c for modification: d - directory, m - sysfile, p - pot-file
########################################################################################

frame $Wssys.c; pack configure $Wssys.c -in $Wssys -anchor w

frame $Wssys.c.d;  frame $Wssys.c.m;  frame $Wssys.c.e
pack configure $Wssys.c.d -in $Wssys.c -side left -anchor se -ipadx 3
pack configure $Wssys.c.m $Wssys.c.e -in $Wssys.c -side left -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 

CreateLSBoxSBx $Wssys.c.d "select directory" top $WIDTH(ssysLS) [expr $HEIGHT(ssysLS) +5] 2 \
            dirlist_dirsel.hlp $Wssys.d.tt "" "" dirlist_sel_d sel
$Wssys.c.d.f.f.li insert end "\$HOME" ".."
foreach x $sysdirlist { $Wssys.c.d.f.f.li insert end "$x"}
foreach i [lsort [glob -nocomplain -- *]] { 
    if [file isdirectory $i] {$Wssys.c.d.f.f.li insert end $i}
}

# new directory insert
label $Wssys.c.d.li -anchor w -text "directory:"
bind  $Wssys.c.d.li <Button-3> {cathfile0 dirlist_diredi $Wssys}
entry $Wssys.c.d.i -width [expr $WIDTH(ssysLS) +5] -relief sunken
pack configure $Wssys.c.d.li $Wssys.c.d.i -in $Wssys.c.d -side top -anchor w
bind  $Wssys.c.d.i <Button-1> {dirlist_focus $Wssys.c.d.i; set dirlist_foc 1}
bind  $Wssys.c.d.i <Button-2> { writescr0 $Wssys.d.tt "dirlist_19  $dirprgwork\n"; \
	$Wssys.c.d.i delete 0 end; $Wssys.c.d.i insert end $working_directory}
bind  $Wssys.c.d.i <Button-3> {cathfile0 dirlist_diredi $Wssys}
bind  $Wssys.c.d.i <Return> {dirlist_edn; focus $Wssys}


########################################################################################
#                                                                          sysfile: c.m
########################################################################################
#
label $Wssys.c.m.l -text "sysfile:   select | select + quit" -anchor w
bind  $Wssys.c.m.l <Button-3> \
	{writescr0  $Wssys.d.tt "left mouse click:\n"; cat_file ${hlp_dir}dirlist_filesel.hlp $Wssys.d.tt}
pack configure $Wssys.c.m.l -in $Wssys.c.m -side top -anchor w

frame $Wssys.c.m.1; # suffix-box and file-ordering-buttons
pack configure $Wssys.c.m.1 -in $Wssys.c.m -side top -anchor w -pady 2

frame $Wssys.c.m.1.1; frame $Wssys.c.m.1.2
pack configure $Wssys.c.m.1.1 -in $Wssys.c.m.1 -side left  -anchor sw -ipady 3
pack configure $Wssys.c.m.1.2 -in $Wssys.c.m.1 -side right -anchor ne -ipady 3 -padx 5

if ![info exists dirlist_sel_m_order_v] {set dirlist_sel_m_order_v "alphabetical"}
CreateLSBoxSBx $Wssys.c.m.1.1 " " top 11 3 0 dirlist_filesel.hlp $Wssys.d.tt \
	"" "\{alphabetical\} \{last access\}" dirlist_sel_m_order ""

# suffix selection box
CreateLSBox $Wssys.c.m.1.2 "sysfile suffix" top 9 3 2 dirlist_filesuff.hlp $Wssys.d.tt \
	"" [vst2list syssuffix.vst 0 "" "" dirlist_sel_m_suffix_v] dirlist_sel_m_suffix sel

# file name selection box
CreateLSBoxSBx2 $Wssys.c.m "" "" $WIDTH(ssysLS) $HEIGHT(ssysLS) 2 \
             dirlist_filesel.hlp $Wssys.d.tt "" "" dirlist_sel_m dirlist_sel_m2 sel

# insert new file name
label $Wssys.c.m.li
entry $Wssys.c.m.i -width [expr $WIDTH(ssysLS) +5] -relief sunken
pack configure $Wssys.c.m.li $Wssys.c.m.i -in $Wssys.c.m -side top -anchor w
bind  $Wssys.c.m.li <Button-3> {cathfile0 dirlist_fileedi $Wssys}
bind  $Wssys.c.m.i <Button-1> {+ dirlist_focus $Wssys.c.m.i; set dirlist_foc 2}
bind  $Wssys.c.m.i <Button-3> {cathfile0 dirlist_fileedi $Wssys}
bind  $Wssys.c.m.i <Return> {dirlist_emfn; focus $Wssys}
bind  $Wssys.c.m.i <Button-2> {
  writescr0 $Wssys.d.tt "last file name:  $sysfile\n"
  $Wssys.c.m.i delete 0 end; $Wssys.c.m.i insert end $sysfile
}

########################################################################################
#                                                                   potential-file: .c.e
########################################################################################
#
label $Wssys.c.e.l -text "pot-file:   select | select + quit" -anchor w
bind  $Wssys.c.e.l <Button-3> \
	{writescr0 $Wssys.d.tt "left mouse click:\n"; cat_file ${hlp_dir}dirlist_filesel.hlp $Wssys.d.tt}
pack configure $Wssys.c.e.l -in $Wssys.c.e -side top -anchor w

frame $Wssys.c.e.1; # suffix-box and file-ordering-buttons
pack configure $Wssys.c.e.1 -in $Wssys.c.e -side top -anchor w -pady 2

frame $Wssys.c.e.1.1; frame $Wssys.c.e.1.2
pack configure $Wssys.c.e.1.1 -in $Wssys.c.e.1 -side left  -anchor sw -ipady 3
pack configure $Wssys.c.e.1.2 -in $Wssys.c.e.1 -side right -anchor ne -ipady 3 -padx 5

if ![info exists dirlist_sel_e_order_v] {set dirlist_sel_e_order_v "alphabetical"}
CreateLSBoxSBx $Wssys.c.e.1.1 " " top 11 3 0 dirlist_filesel.hlp $Wssys.d.tt \
	"" "\{alphabetical\} \{last access\}" dirlist_sel_e_order ""

# suffix selection box
CreateLSBox $Wssys.c.e.1.2 "pot-file suffix" top 9 3 2 dirlist_filesuff.hlp $Wssys.d.tt \
	"" [vst2list potentialsuffix.vst 0 "" "" dirlist_sel_e_suffix_v] dirlist_sel_e_suffix sel

# file name selection box
CreateLSBoxSBx2 $Wssys.c.e "" "" $WIDTH(ssysLS) $HEIGHT(ssysLS) 2 \
             dirlist_filesel.hlp $Wssys.d.tt "" "" dirlist_sel_e dirlist_sel_e2 sel

# insert new file name
label $Wssys.c.e.li
entry $Wssys.c.e.i -width [expr $WIDTH(ssysLS) +5] -relief sunken
pack configure $Wssys.c.e.li $Wssys.c.e.i -in $Wssys.c.e -side top -anchor w
bind  $Wssys.c.e.li <Button-3> {cathfile0 dirlist_fileedi $Wssys}
bind  $Wssys.c.e.i <Button-1> {+ dirlist_focus $Wssys.c.e.i; set dirlist_foc 3}
bind  $Wssys.c.e.i <Button-3> {cathfile0 dirlist_fileedi $Wssys}
bind  $Wssys.c.e.i <Return> {dirlist_eefn; focus $Wssys}
bind  $Wssys.c.e.i <Button-2> {
  writescr0 $Wssys.d.tt "last file name:  $potfile$potsuffix\n"
  $Wssys.c.e.i delete 0 end
  if {$potfile==""} {writescr0 $Wssys.d.tt "last file name:  $potfile\n"; $Wssys.c.e.i insert end $potfile} \
     else {writescr0 $Wssys.d.tt "last file name:  $potfile$potsuffix\n"; $Wssys.c.e.i insert end $potfile$potsuffix}
}


########################################################################################

insert_textframe $Wssys $HEIGHT(ssys.tt)

# initialisation, focus etc.

dirlist_anzeige $Wssys
$Wssys.c.d.i delete 0 end; $Wssys.c.m.i delete 0 end; $Wssys.c.e.i delete 0 end

ffuellen $Wssys.c.m.f.f.li $syssuffix $mdatsort
ffuellen $Wssys.c.e.f.f.li $potsuffix $pdatsort


focus $Wssys; set dirlist_foc 0


###########################################
#set sysfile SmCo2
#set sysfile Co10Ir11
#read_sysfile
#modify_system_exec
#destros $Wssys
#unlock_list
###########################################


}
