#######################################################################################
#
#              interface to the fortran program   xband_geometry
#
#
#
#  available task: 
#  
#   IQ_NLAY_order
#   cr_sph_regime
#   cr_rec_regime
#   interaction_zone
#
#
#
########################################################################################
proc run_geometry {task} {#       

global IQ_ordered NVEC_LAY clu_box_dist clu_box_show
global IQCNTR NSHLCLU CLURAD
global L_BOX anchor_mode 
global NSHLRELAX RELAXRAD NQRELAX SEL_IQBOX NQSEL
global IOCCTYPE_IQ IOCCTYPE_IQBOX

global NSHLINT INTRAD NQINT
global optimize_basis_result

# make all in this proc initialized vars global 
global NQBOX NLAYBOX ILAY_SEL RQBOX IQBOX1_ILAY IQBOX2_ILAY PN_ILAY IQ_IQBOX N_IQBOX

global GEOMETRYPRG

global DEBUG 

#
#------------------------------------------------------------------------------------ ND
#
global sysfile syssuffix 
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
global system NQ NT ZT RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global NM IMT IMQ RWSM NAT QMTET QMPHI 
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION
#
#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE  ZRANGE_TYPE  STRUCTURE_SEQUENCE
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R 

global IQ_3DSURF h_3DSURF k_3DSURF l_3DSURF o_3DSURF d_3DSURF
 
set PRC run_geometry

debug $PRC "running geometry with task   $task"
  
set geo_inp [open "xband_geometry.inp" w]
puts $geo_inp "name of system file"
puts $geo_inp "$sysfile$syssuffix"
puts $geo_inp "task"
puts $geo_inp "$task"

#------------------------------------------------------- optimisation_of_basis_vectors_3D
if {$task=="optimisation_of_basis_vectors_3D"  } {

  puts $geo_inp "primitive vectors     (cart. coord.) \[A\]"
  for {set I 1} {$I <= 3} {incr I} {
     puts $geo_inp [format "%20.15f%20.15f%20.15f" $RBASX($I) $RBASY($I) $RBASZ($I)] 
  }
  puts $geo_inp "number of sites NQ"
  puts $geo_inp [format %3i $NQ]
  for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

     puts $geo_inp [format "%5i  %20.15f%20.15f%20.15f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
  }
     
  close $geo_inp

  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]

  gets $geo_out line ; # header

  gets $geo_out line ; set IERROR    [lindex $line 2]

  if {$IERROR!="0"} {
      give_warning "." "WARNING \n\n run of geometry failed for    \
	                        \n\n TASK = optimisation_of_basis_vectors_3D \
	                        \n\n check xband_geometry.inp "
      return
  }

  gets $geo_out line 

  if {[string first "NO CHANGES" $line]>=0} {
      set optimize_basis_result "no changes needed"
      debug $PRC  "optimize_basis_result: no changes needed (return)"      
      return
  } else {
      set optimize_basis_result "atoms shifted by lattice vector R"
      debug $PRC  "optimize_basis_result: atoms shifted by lattice vector R"      
  }

#------------------------------ read shifted atom positions for 3D basis

  for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set RQX($IQ)   [lindex $line 0]   ; # QBAS X
    set RQY($IQ)   [lindex $line 1]   ; # QBAS Y
    set RQZ($IQ)   [lindex $line 2]   ; # QBAS Z
  }

  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_opt_bas_vec_3D " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_opt_bas_vec_3D " message]
  }

#------------------------------------------------------------------------ IQ_NLAY_order
} elseif {$task=="IQ_NLAY_order"  } {

  puts $geo_inp "normal vector for ordering of atomic layers"
  puts $geo_inp  [format "%20.15f%20.15f%20.15f" $NVEC_LAY(1) $NVEC_LAY(2) $NVEC_LAY(3)]

  close $geo_inp 

  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out text_line
  gets $geo_out text_line
  gets $geo_out text_line
  gets $geo_out text_line
  debug $PRC  "$text_line"

  for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    gets $geo_out line
    set IQ_ordered($IQ) [lindex $line 1]
    debug $PRC  "$line >>> $IQ_ordered($IQ)  "
  }

  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_IQ_NLAY_order " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_IQ_NLAY_order " message]
  }  

#------------------------------------------------------------------------
} elseif {$task=="cr_sph_regime" } {

  puts $geo_inp "IQCNTR"
  puts $geo_inp $IQCNTR
  if {$NSHLCLU!=0  } {
     puts $geo_inp "NSHLCLU"
     puts $geo_inp $NSHLCLU
  } else {
     puts $geo_inp "CLURAD"
     puts $geo_inp $CLURAD
  }

  puts $geo_inp "normal vector of atomic layers"
  puts $geo_inp  [format "%20.15f%20.15f%20.15f" $NVEC_LAY(1) $NVEC_LAY(2) $NVEC_LAY(3)]

  puts $geo_inp "CLU_BOX_SHOW"
  puts $geo_inp $clu_box_show
  puts $geo_inp "CLU_BOX_DIST"
  puts $geo_inp $clu_box_dist

  close $geo_inp 

  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line" 
  gets $geo_out line ; debug $PRC "$line" ; set NQBOX    [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set NLAYBOX  [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set ILAY_SEL [lindex $line 2]

  gets $geo_out line ; debug $PRC "$line" 
  for {set ILAY 1} {$ILAY <= $NLAYBOX} {incr ILAY} {
    gets $geo_out line
    set IQBOX1_ILAY($ILAY) [lindex $line 1]   ; # IQ1
    set IQBOX2_ILAY($ILAY) [lindex $line 2]   ; # IQ2 
    set     PN_ILAY($ILAY) [lindex $line 3]   ; # PROJECT
    debug $PRC  "$line >>> $IQBOX1_ILAY($ILAY) $IQBOX2_ILAY($ILAY) $PN_ILAY($ILAY)"
  }

  gets $geo_out line ; debug $PRC "$line"    ; # header line
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    gets $geo_out line
    set RQBOX(1,$IQBOX)   [lindex $line 1]   ; # RQBOX X
    set RQBOX(2,$IQBOX)   [lindex $line 2]   ; # RQBOX Y
    set RQBOX(3,$IQBOX)   [lindex $line 3]   ; # RQBOX Z
    set N_IQBOX(1,$IQBOX) [lindex $line 4]   ; # NA
    set N_IQBOX(2,$IQBOX) [lindex $line 5]   ; # NB
    set N_IQBOX(3,$IQBOX) [lindex $line 6]   ; # NC
    set N_IQBOX(4,$IQBOX) [lindex $line 7]   ; # NCL
    set N_IQBOX(5,$IQBOX) [lindex $line 8]   ; # NCR
    set IQ_IQBOX($IQBOX)  [lindex $line 9]   ; # IQ
    debug $PRC  "$line >>> $N_IQBOX(1,$IQBOX) $N_IQBOX(2,$IQBOX) $N_IQBOX(3,$IQBOX)\
                 $N_IQBOX(4,$IQBOX) $N_IQBOX(5,$IQBOX) $IQ_IQBOX($IQBOX)  "
  }

  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_cr_sph_regime " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_cr_sph_regime " message]
  }

#------------------------------------------------------------------------
} elseif {$task=="cr_rec_regime" } {

# anchor_mode: xy-centered or xyz-centered

  puts $geo_inp "IQ_SEL"
  puts $geo_inp $IQCNTR        ;#$ IQ_SEL  ; # reuse available var

  puts $geo_inp "anchor_mode"
  puts $geo_inp $anchor_mode

  puts $geo_inp "normal vector of atomic layers"
  puts $geo_inp  [format "%20.15f%20.15f%20.15f" $NVEC_LAY(1) $NVEC_LAY(2) $NVEC_LAY(3)]

  puts $geo_inp "BOX dimensions  LX LY LZ"
  puts $geo_inp $L_BOX(1)
  puts $geo_inp $L_BOX(2)
  puts $geo_inp $L_BOX(3)

  close $geo_inp 

  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line" 
  gets $geo_out line ; debug $PRC "$line" ; set NQBOX    [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set NLAYBOX  [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set ILAY_SEL [lindex $line 2]

  gets $geo_out line ; debug $PRC "$line" 
  for {set ILAY 1} {$ILAY <= $NLAYBOX} {incr ILAY} {
    gets $geo_out line
    set IQBOX1_ILAY($ILAY) [lindex $line 1]   ; # IQ1
    set IQBOX2_ILAY($ILAY) [lindex $line 2]   ; # IQ2 
    set     PN_ILAY($ILAY) [lindex $line 3]   ; # PROJECT
    debug $PRC  "$line >>> $IQBOX1_ILAY($ILAY) $IQBOX2_ILAY($ILAY) $PN_ILAY($ILAY)"
  }

  gets $geo_out line ; debug $PRC "$line"    ; # header line
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    gets $geo_out line
    set RQBOX(1,$IQBOX)   [lindex $line 1]   ; # RQBOX X
    set RQBOX(2,$IQBOX)   [lindex $line 2]   ; # RQBOX Y
    set RQBOX(3,$IQBOX)   [lindex $line 3]   ; # RQBOX Z
    set N_IQBOX(1,$IQBOX) [lindex $line 4]   ; # NA
    set N_IQBOX(2,$IQBOX) [lindex $line 5]   ; # NB
    set N_IQBOX(3,$IQBOX) [lindex $line 6]   ; # NC
    set N_IQBOX(4,$IQBOX) [lindex $line 7]   ; # NCL
    set N_IQBOX(5,$IQBOX) [lindex $line 8]   ; # NCR
    set IQ_IQBOX($IQBOX)  [lindex $line 9]   ; # IQ
    debug $PRC  "$line >>> $N_IQBOX(1,$IQBOX) $N_IQBOX(2,$IQBOX) $N_IQBOX(3,$IQBOX)\
                 $N_IQBOX(4,$IQBOX) $N_IQBOX(5,$IQBOX) $IQ_IQBOX($IQBOX)  "
  }

  gets $geo_out line ; debug $PRC "$line" 


  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_cr_rec_regime " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_cr_rec_regime " message]
  }

#------------------------------------------------------------------------
} elseif {$task=="cr_relax_zone" } {
# 

  puts $geo_inp "IQ_SEL"
  puts $geo_inp $IQCNTR        ;#$ IQ_SEL  ; # reuse available var

  puts $geo_inp "normal vector of atomic layers"
  puts $geo_inp  [format "%20.15f%20.15f%20.15f" $NVEC_LAY(1) $NVEC_LAY(2) $NVEC_LAY(3)]

  if {$NSHLCLU!=0  } {
     puts $geo_inp "NSHLRELAX"
     puts $geo_inp $NSHLRELAX
  } else {
     puts $geo_inp "RELAXRAD"
     puts $geo_inp $RELAXRAD
  }

  puts $geo_inp "NQ"
  puts $geo_inp $NQ

  puts $geo_inp \
  "  IQ  IOCCTYPE_IQ"
  for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set TXT "[format %4i $IQ][format %6i $IOCCTYPE_IQ($IQ)]"
      
    puts $geo_inp $TXT
  }

  puts $geo_inp "NQBOX"
  puts $geo_inp $NQBOX

  puts $geo_inp "IQBOX     RQBOX X           RQBOX Y           RQBOX \
                 Z       NA  NB  NC NCL NCR  IQ SEL OCCTYPE"
  set NQSEL_ORG 0
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    set TXT [format %4i $IQBOX]
    set TXT "$TXT[format "%20.15f%20.15f%20.15f" \
             $RQBOX(1,$IQBOX) $RQBOX(2,$IQBOX) $RQBOX(3,$IQBOX) ]"
    for {set I 1} {$I <= 5} {incr I} {
	set TXT "$TXT[format %4i $N_IQBOX($I,$IQBOX)]"
    }
    set TXT "$TXT[format %4i  $IQ_IQBOX($IQBOX)]"
    set TXT "$TXT[format %4i $SEL_IQBOX($IQBOX)]"
    set TXT "$TXT[format %4i [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)] ]"

    if { $SEL_IQBOX($IQBOX) != 0 } { incr NQSEL_ORG }

    puts $geo_inp $TXT
  }

  close $geo_inp 

  set NQBOX_ORG $NQBOX


  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line" 
  gets $geo_out line ; debug $PRC "$line" ; set NQBOX    [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set NLAYBOX  [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set ILAY_SEL [lindex $line 2]

  gets $geo_out line ; debug $PRC "$line" 
  for {set ILAY 1} {$ILAY <= $NLAYBOX} {incr ILAY} {
    gets $geo_out line
    set IQBOX1_ILAY($ILAY) [lindex $line 1]   ; # IQ1
    set IQBOX2_ILAY($ILAY) [lindex $line 2]   ; # IQ2 
    set     PN_ILAY($ILAY) [lindex $line 3]   ; # PROJECT
    debug $PRC  "$line >>> $IQBOX1_ILAY($ILAY) $IQBOX2_ILAY($ILAY) $PN_ILAY($ILAY)"
  }

  set NQSEL 0
  gets $geo_out line ; debug $PRC "$line"    ; # header line
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    gets $geo_out line
    set RQBOX(1,$IQBOX)   [lindex $line  1]   ; # RQBOX X
    set RQBOX(2,$IQBOX)   [lindex $line  2]   ; # RQBOX Y
    set RQBOX(3,$IQBOX)   [lindex $line  3]   ; # RQBOX Z
    set N_IQBOX(1,$IQBOX) [lindex $line  4]   ; # NA
    set N_IQBOX(2,$IQBOX) [lindex $line  5]   ; # NB
    set N_IQBOX(3,$IQBOX) [lindex $line  6]   ; # NC
    set N_IQBOX(4,$IQBOX) [lindex $line  7]   ; # NCL
    set N_IQBOX(5,$IQBOX) [lindex $line  8]   ; # NCR
    set IQ_IQBOX($IQBOX)  [lindex $line  9]   ; # IQ
    set SEL_IQBOX($IQBOX) [lindex $line 10]   ; # SEL
    set iocctype          [lindex $line 11]   ; # IOCCTYPE  
    set IQ $IQ_IQBOX($IQBOX)
    set IOCCTYPE_IQBOX($IQBOX) [list $IOCCTYPE_IQ($IQ)]  
    if { $SEL_IQBOX($IQBOX) != 0 } {
       incr NQSEL 
       lappend IOCCTYPE_IQBOX($IQBOX) $iocctype 
    }

      debug $PRC  "$line >>> \
                 $N_IQBOX(1,$IQBOX) $N_IQBOX(2,$IQBOX) $N_IQBOX(3,$IQBOX)\
                 $N_IQBOX(4,$IQBOX) $N_IQBOX(5,$IQBOX) $IQ_IQBOX($IQBOX) \
                 $SEL_IQBOX($IQBOX) $IOCCTYPE_IQBOX($IQBOX) "
  }

  gets $geo_out line ; debug $PRC "$line" 

  close $geo_out

  set NQRELAX [ expr  $NQSEL - $NQSEL_ORG ]

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_cr_relax_zone " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_cr_relax_zone " message]
  }

#------------------------------------------------------------------------
} elseif {$task=="center_cluster" } {
# 

  puts $geo_inp "IQ_SEL"
  puts $geo_inp $IQCNTR        ;#$ IQ_SEL  ; # reuse available var

  puts $geo_inp "normal vector of atomic layers"
  puts $geo_inp  [format "%20.15f%20.15f%20.15f" $NVEC_LAY(1) $NVEC_LAY(2) $NVEC_LAY(3)]

  puts $geo_inp "NSHLRELAX"
  puts $geo_inp 0

  puts $geo_inp "NQ"
  puts $geo_inp $NQ

  puts $geo_inp \
  "  IQ  IOCCTYPE_IQ"
  for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set TXT "[format %4i $IQ][format %6i $IOCCTYPE_IQ($IQ)]"
      
    puts $geo_inp $TXT
  }

  puts $geo_inp "NQBOX"
  puts $geo_inp $NQBOX

  puts $geo_inp "IQBOX     RQBOX X           RQBOX Y           RQBOX \
                 Z       NA  NB  NC NCL NCR  IQ SEL OCCTYPE"
  set NQSEL_ORG 0
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    set TXT [format %4i $IQBOX]
    set TXT "$TXT[format "%20.15f%20.15f%20.15f" \
             $RQBOX(1,$IQBOX) $RQBOX(2,$IQBOX) $RQBOX(3,$IQBOX) ]"
    for {set I 1} {$I <= 5} {incr I} {
	set TXT "$TXT[format %4i $N_IQBOX($I,$IQBOX)]"
    }
    set TXT "$TXT[format %4i  $IQ_IQBOX($IQBOX)]"
    set TXT "$TXT[format %4i $SEL_IQBOX($IQBOX)]"
    set TXT "$TXT[format %4i [lindex $IOCCTYPE_IQBOX($IQBOX) $SEL_IQBOX($IQBOX)] ]"

    if { $SEL_IQBOX($IQBOX) != 0 } { incr NQSEL_ORG }

    puts $geo_inp $TXT
  }

  close $geo_inp 

  set NQBOX_ORG $NQBOX


  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line" 
  gets $geo_out line ; debug $PRC "$line" ; set NQBOX    [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set NLAYBOX  [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line" ; set ILAY_SEL [lindex $line 2]

  gets $geo_out line ; debug $PRC "$line" 
  for {set ILAY 1} {$ILAY <= $NLAYBOX} {incr ILAY} {
    gets $geo_out line
    set IQBOX1_ILAY($ILAY) [lindex $line 1]   ; # IQ1
    set IQBOX2_ILAY($ILAY) [lindex $line 2]   ; # IQ2 
    set     PN_ILAY($ILAY) [lindex $line 3]   ; # PROJECT
    debug $PRC  "$line >>> $IQBOX1_ILAY($ILAY) $IQBOX2_ILAY($ILAY) $PN_ILAY($ILAY)"
  }

  set NQSEL 0
  gets $geo_out line ; debug $PRC "$line"    ; # header line
  for {set IQBOX 1} {$IQBOX <= $NQBOX} {incr IQBOX} {
    gets $geo_out line
    set RQBOX(1,$IQBOX)   [lindex $line  1]   ; # RQBOX X
    set RQBOX(2,$IQBOX)   [lindex $line  2]   ; # RQBOX Y
    set RQBOX(3,$IQBOX)   [lindex $line  3]   ; # RQBOX Z
    set N_IQBOX(1,$IQBOX) [lindex $line  4]   ; # NA
    set N_IQBOX(2,$IQBOX) [lindex $line  5]   ; # NB
    set N_IQBOX(3,$IQBOX) [lindex $line  6]   ; # NC
    set N_IQBOX(4,$IQBOX) [lindex $line  7]   ; # NCL
    set N_IQBOX(5,$IQBOX) [lindex $line  8]   ; # NCR
    set IQ_IQBOX($IQBOX)  [lindex $line  9]   ; # IQ
    set SEL_IQBOX($IQBOX) [lindex $line 10]   ; # SEL
    set iocctype          [lindex $line 11]   ; # IOCCTYPE  
    set IQ $IQ_IQBOX($IQBOX)
    set IOCCTYPE_IQBOX($IQBOX) [list $IOCCTYPE_IQ($IQ)]  
    if { $SEL_IQBOX($IQBOX) != 0 } {
       incr NQSEL 
       lappend IOCCTYPE_IQBOX($IQBOX) $iocctype 
    }

      debug $PRC  "$line >>> \
                 $N_IQBOX(1,$IQBOX) $N_IQBOX(2,$IQBOX) $N_IQBOX(3,$IQBOX)\
                 $N_IQBOX(4,$IQBOX) $N_IQBOX(5,$IQBOX) $IQ_IQBOX($IQBOX) \
                 $SEL_IQBOX($IQBOX) $IOCCTYPE_IQBOX($IQBOX) "
  }

  gets $geo_out line ; debug $PRC "$line" 

  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_center_cluster " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_center_cluster " message]
  }

#------------------------------------------------------------------------
} elseif {$task=="cr_3D_surface" } {

  puts $geo_inp "IQ_3DSURF"
  puts $geo_inp "  $IQ_3DSURF"

  puts $geo_inp "Miller indices"
  puts $geo_inp "  $h_3DSURF  $k_3DSURF  $l_3DSURF"

  puts $geo_inp "definition of (hkl) d_3DSURF   (cryst/prim)"
  puts $geo_inp " $d_3DSURF"

  puts $geo_inp "orientation of surface o_3DSURF (down/up)"
  puts $geo_inp " $o_3DSURF"

  close $geo_inp 

  eval set res [catch "exec $GEOMETRYPRG " message]
  writescr0 .d.tt "$message \n"

  set geo_out [open "xband_geometry.out" r]
  gets $geo_out line ; debug $PRC "$line"
  gets $geo_out line ; debug $PRC "$line" ; set IERROR    [lindex $line 2]
  if {$IERROR!="0"} {
      give_warning "." "WARNING \n\n run of geometry failed for  \n\n $SYMMETRY  \
		    \n\n TASK = cr_3D_surface \n\n check xband_geometry.inp "
      return
  }

  global RBASX_3DSURF RBASY_3DSURF RBASZ_3DSURF NQ_3DSURF NOQ_3DSURF ITOQ_3DSURF
  global   RQX_3DSURF   RQY_3DSURF   RQZ_3DSURF IQ_3DBULK_IQ_3DSURF

  gets $geo_out line ; debug $PRC "$line"
  for {set I 1} {$I <= 3} {incr I} {
     gets $geo_out line ; debug $PRC "$line"
     set RBASX_3DSURF($I) [lindex $line 1]
     set RBASY_3DSURF($I) [lindex $line 2]
     set RBASZ_3DSURF($I) [lindex $line 3]
     puts "RBAS $I  $RBASX_3DSURF($I) $RBASY_3DSURF($I) $RBASZ_3DSURF($I)  "
  }

  gets $geo_out line ; debug $PRC "$line" ; set NQ_3DSURF  [lindex $line 2]
  gets $geo_out line ; debug $PRC "$line"
  puts "NQ_3DSURF $NQ_3DSURF  "

  for {set IQ 1} {$IQ <= $NQ_3DSURF} {incr IQ} {
     gets $geo_out line ; debug $PRC "$line"
     set RQX_3DSURF($IQ) [lindex $line 1]
     set RQY_3DSURF($IQ) [lindex $line 2]
     set RQZ_3DSURF($IQ) [lindex $line 3]
     puts "$IQ RQ  $RQX_3DSURF($IQ) $RQY_3DSURF($IQ) $RQZ_3DSURF($IQ)  "
  }

  gets $geo_out line ; debug $PRC "$line"
  for {set IQ 1} {$IQ <= $NQ_3DSURF} {incr IQ} {
     gets $geo_out line ; debug $PRC "$line" ; set NOQ_3DSURF($IQ)  [lindex $line 1]
     gets $geo_out line ; debug $PRC "$line"
     for {set IO 1} {$IO <= $NOQ_3DSURF($IQ)} {incr IO} {
	 set ITOQ_3DSURF($IO,$IQ)  [lindex $line [expr $IO-1] ]
     }
     puts "NOQ_3DSURF  $NOQ_3DSURF($IQ)  $ITOQ_3DSURF(1,$IQ)     "
  }

  gets $geo_out line ; debug $PRC "$line"
  for {set IQ 1} {$IQ <= $NQ_3DSURF} {incr IQ} {
     gets $geo_out line ; debug $PRC "$line" 
     set IQ_3DBULK_IQ_3DSURF($IQ)  [lindex $line 1]
     puts "IQ_3DBULK_IQ_3DSURF $IQ_3DBULK_IQ_3DSURF($IQ)     "
  }

  close $geo_out

  if {$DEBUG == 1} {
     eval set res [catch "exec mv xband_geometry.inp xband_geometry.inp_cr_3D_surface " message]
     eval set res [catch "exec mv xband_geometry.out xband_geometry.out_cr_3D_surface " message]
  }

#------------------------------------------------------------------------
} else {

   debug $PRC "running geometry with unknown task   $task"

}

debug $PRC "regular exit ********************************************************* \n\n"

}
########################################################################################














