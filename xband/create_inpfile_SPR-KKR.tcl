#######################################################################################
#                                    SPR-KKR
########################################################################################
#                                                                         create_inpfile
#
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and
#     adds the corresponding entries to the input mask create by   create_inpfile
#
#     HE 21/05/07 write SPRKKR input and pot files for new format version 6
#                 including 2D slab and extended (LIR) structures dealt in TB mode
#

proc create_inpfile_SPR-KKR {} {

    set PRC "create_inpfile_SPR-KKR"

    global structure_window_calls
    global sysfile syssuffix  PACKAGE sysfile_host potfile
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NL
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS DIMENSION
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE

    global CLUSTER_TYPE
    global NQCLU NTCLU IQ_IQCLU N5VEC_IQCLU RQCLU
    global NOCC_IQCLU ZTCLU TXTTCLU CONCCLU ITCLU_OQCLU

    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VAR0 VARLST NCPA
    global SPRKKR_EXPERT NVALT_DEFAULT
    global create_inpfile_SPRKKR_wc1
    global create_inpfile_SPRKKR_wc2
    global create_inpfile_SPRKKR_wc3
    global inpfile inpsuffix sysfile syssuffix
    global COLOR WIDTH HEIGHT FONT
    global TABCQNTOP_N
    global QUICK TABNCORE 
    global VARARRAYINITIALIZED

    set create_inpfile_SPRKKR_wc1 ".DUMMY"
    set create_inpfile_SPRKKR_wc2 ".DUMMY"
    set create_inpfile_SPRKKR_wc3 ".DUMMY"

    if {![info exists CLUSTER_TYPE] } {set CLUSTER_TYPE "NONE"}

    set AKHE 1

    set TABNCORE [list 0 0 0 2 2 2 2 2 2 2 2 10 10 10 10 10 10 10 10 18 18 18  \
                       18 18 18 18 18 18 18 18 28 28 28 28 28 28 28 36 36 36 36 36   \
                       36 36 36 36 36 36 46 46 46 46 46 46 46 54 54 54 54 54 54 54   \
                       54 54 54 54 54 54 54 54 54 54 68 68 68 68 68 68 68 68 78 78   \
                       78 78 78 78 78 86 86 86 86 86 86 86 86 86 86 86 86 86 86 86   \
                       86 86 86 86 86 86 86 86 86 ]

# highest n quantum number of core state

    set TABCQNTOP_N [list 0 0 0 1 1 1 1 1 1 1 1  2 2 2 2 2 2 2 2 3 3 \
                            3 3 3 3 3 3 3 3 3 3  3 3 3 3 3 3 4 4 4 4 \
                            4 4 4 4 4 4 4 4 4 4  4 4 4 4 5 5 5 5 5 5 \
                            5 5 5 5 5 5 5 5 5 5  5 5 5 5 5 5 5 5 5 5 \
                            5 5 5 5 5 5 6 6 6 6  6 6 6 6 6 6 6 6 6 6 \
                            6 6 6 6 6 6 6 6 6 6  6 6 6 6 6 6 6 6 6 6 \
                           ]

    if {[info exists SPRKKR_EXPERT]==0} {
       set SPRKKR_EXPERT 0
    }

    if {$QUICK==1} {set SPRKKR_EXPERT 1}
#
#----------------------------------------------- deactivate unused top buttons
#
pack forget $Wcinp.a.aa.edit
pack forget $Wcinp.a.bb.write
pack forget $Wcinp.a.bb.append
#
#----------------------------------------------- reset command for extit buttons
#
$Wcinp.a.cc.ok    configure -command "create_inpfile_SPR-KKR_check ok"
$Wcinp.a.cc.edit  configure -command "create_inpfile_SPR-KKR_check edit"
#
#--------------------------------------------------------------------------------------
#
global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGTSK1 ; set BGTSK1  Salmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1
global BGWR  ; set BGWR   lavender
global BGKKR ; set BGKKR  LightSkyBlue1 
global BGK   ; set BGK    SteelBlue1
global BGCLU ; set BGCLU  LightSkyBlue
global BGE   ; set BGE    moccasin
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1
global BGMOD ; set BGMOD  cornsilk1
global BGMAG ; set BGMAG  lavender
global BGLEX ; set BGLEX  LightPink1

set buttonbgcolor green1
set buttonheight  2

set w  $Wcinp
set wl 12
set wl1 7
set wl2 5
set we 30
set ws 160
set ws1 100
set fe $FONT(GEN)

set wc1 $w.cols.col1 
set wc2 $w.cols.col2
set wc3 $w.cols.col3
set wc4 $w.cols.col4

########################################################################################

#--------------------------------------------------------------------------------
#              set up list of variable names and initialize
#--------------------------------------------------------------------------------

set VARLST [list IPRINT ITEST WRTAU RDTAU WRTAUMQ RDTAUMQ NOHFF NOCORE FSOHFF EFG \
                 NOWRDOS WRCPA WRKAPDOS WRPOLAR ]
lappend VARLST   DATASET ADSI POTFIL
lappend VARLST   TAUMAT TAUEXP SURFACE IMPURITY SLAB IQCNTR ITCNTR NSHLCLU \
                 CLURAD NLOUT STRUC NQSLAB NSURF PATHLEN NLEGXX ISYM
lappend VARLST   SREL NREL OP BEXT BREITINT ORBPOL MALF MBET MGAM
lappend VARLST   GRID NE EMIN EMAX ImE SEARCHEF EF
lappend VARLST   CPANITER CPATOL
lappend VARLST   STRETA STRRMAX STRGMAX
lappend VARLST   SCFVXC SCFALG SCFNITER SCFMIX SCFTOL SCFISTBRY SCFITDEPT \
                 SCFMIXOP SCFCOREHOLE SCFITHOLE SCFNQNHOLE SCFLQNHOLE SCFSIM\
                 FULLPOT SPHERCELL POTFMT SEMICORE LLOYD VTMZ QIONSCL 

#lappend VARLST   SURFACE IMPURITY SLAB   \
#                   STRUC NQSLAB NSURF PATHLEN NLEGXX ISYM


# only init VAR(...) when called first, otherwise remember values
if {! [info exists VARARRAYINITIALIZED]} {
    
    set VARARRAYINITIALIZED 1
    foreach V $VARLST { set VAR($V) "" }
    
    set VAR(IPRINT)   0
    set VAR(ITEST)    0
    set VAR(WRTAU)    0
    set VAR(RDTAU)    0
    set VAR(WRTAUMQ)  0
    set VAR(RDTAUMQ)  0
    set VAR(NONMAG)     0
    set VAR(NOHFF)    0
    set VAR(NOCORE)   0
    set VAR(FSOHFF)   0
    set VAR(EFG)      0
    set VAR(NOWRDOS)  0
    set VAR(WRLMLDOS) 0
    set VAR(WRKAPDOS) 0
    set VAR(WRPOLAR)  0

    set VAR(HAMILTONIAN) "REL"

    set VAR(CPAMODE)  "SINGLE SITE"
    set VAR(WRCPA)    0
    set VAR(CPANITER) 20
    set VAR(CPATOL)   0.00001
    set VAR(NLCPA-LVL)    1
    set VAR(NLCPA-WRCFG)  0
    set VAR0(CPAMODE)     $VAR(CPAMODE)
    set VAR0(CPATOL)      $VAR(CPATOL)
    set VAR0(CPANITER)    $VAR(CPANITER)
    set VAR0(NLCPA-LVL)   $VAR(NLCPA-LVL)
    set VAR0(NLCPA-WRCFG) $VAR(NLCPA-WRCFG)
    
    set VAR(TAUIO) "AUTO"

    set VAR(SCFVXC)    VWN
    set VAR(SCFALG)    BROYDEN2
    set VAR(SCFNITER)  200
    set VAR(SCFTOL)    0.00001
    set VAR(SCFMIX)    0.20
    set VAR(SCFMIXOP)  0.20
    set VAR(SCFISTBRY) 1
    set VAR(SCFITDEPT) 40
    set VAR(SCFSIM)    0.0
    set VAR(BREITINT)  F
    set VAR(ORBPOL)    NONE
    set VAR(BEXT)      0.0
    set VAR(EXTFIELD)  F
    set VAR(BLCOUPL)   F
    set VAR(KMROT)     0
    set VAR(MDIR-x)    0
    set VAR(MDIR-y)    0
    set VAR(MDIR-z)    1
    set VAR(QMVEC-x)   0.0
    set VAR(QMVEC-y)   0.0
    set VAR(QMVEC-z)   0.0
    set VAR(IBAS)      0
    
    set VAR(USENVALT)  0

    #----------------------------------------------- DOS
    set VAR(GRID)      3
    set VAR(NE)       50
    set VAR(EMIN)    -0.2
    set VAR(EMAX)     1.0
    set VAR(ImE)      0.01
    set VAR(EF)       ""
    set VAR(SEARCHEF)   0
    #----------------------------------------------- SCF
    set VAR(GRID)      5
    set VAR(NE)       32
    set VAR(EMIN)    -0.2
    set VAR(EMAX)     ""
    set VAR(ImE)      0.0
    set VAR(EF)       ""
    set VAR(SEARCHEF)   0
    set VAR(VMTZ)     0.0
    #-----------------------------------------------

    set VAR(NTMP)      0
    set VAR(TMPMIN)    10.0
    set VAR(TMPMAX)   400.0
    set VAR(TDEBYE)   250.0

    set VAR(IBZINT)  POINTS
    set VAR(NKMIN)     400
    set VAR(NKMAX)     900
    set VAR(NKTAB)     250
    set VAR(NF)          6
    set VAR(STRETA)    0.0
    set VAR(STRRMAX)   2.9
    set VAR(STRGMAX)   3.3
    
    set VAR(CLUALG)      TAUMAT
    set VAR(IQCNTR)      1
    set VAR(ITCNTR)      0
    set VAR(CLU-SPEC)    NSH
    set VAR(CLURAD)      1.5
    set VAR(CLURAD_JXC)      1.5
    set VAR(NLOUT)       3
    
    set VAR(NEPHOT)      1
    set VAR(EPHOTMIN)    100
    set VAR(EPHOTMAX)    100
    set VAR(transition)  core-band
    set VAR(MEPLOT_l)    all
    set VAR(MEPLOT_NONMAG) YES
    set VAR(ReERYDI)     0.5
    set VAR(ImERYDI)     0.01
    set VAR(ReERYDF)     1.8
    set VAR(ImERYDF)     0.01
    
    set VAR(CORE_VB)     VB
    set VAR(VB_l)        d
    
    set VAR(ITXRAY)      1  ; # ITXRAY used throughout for any selected atom type  IT
    set VAR(NCXRAY)      2  ; # NCXRAY used throughout for quantum number n of selected atom type  IT
    set VAR(LCXRAY)      p  ; # LCXRAY used throughout for quantum number l of selected atom type  IT
    
    set VAR(USETAUNN)    0
    set VAR(USETSS)    1
    set VAR(AESNEME)    10
    set VAR(AESNEME-DEF) $VAR(AESNEME)
    
    set VAR(XMONE3)       40
    set VAR(XMONE3-DEF)   $VAR(XMONE3)
    set VAR(XMOEMAX3)     8.0
    set VAR(XMOEMAX3-DEF) $VAR(XMOEMAX3)
    
    set VAR(COMPTON-NPP)     50
    set VAR(COMPTON-PPMAX)  10.0
    set VAR(COMPTON-NPN)     50
    set VAR(COMPTON-PNMAX)   5.0
    set VAR(COMPTON-PNVECx)  0.0
    set VAR(COMPTON-PNVECy)  0.0
    set VAR(COMPTON-PNVECz)  1.0
    
    set VAR(TAUJMIN)     0.0
    set VAR(TAUJPLS)     0.0
    set VAR(RHOME)       NAB
    set VAR(XRAYME)      ADA
    set VAR(TSELECT)     0
    set VAR(IREL)        3
    
    set VAR(FMAG)        0
    
    set VAR(OWRPOT)      0
    set VAR(WROPTS)      0

    set VAR(MATTHEISS)  DEFAULT
    set VAR(USEMSPIN)    0
    set VAR(NOSSITER)    1
    if  {$DIMENSION == "2D" } {
	set VAR(QIONSCL)    0.2 
    } else {
	set VAR(QIONSCL)    1.0
    }

    set VAR(CHIPRINT)    "0"

    set VAR(EMINDISP) "-0.1"
    set VAR(EMAXDISP) "1.0"
    set VAR(NEDISP)   "100"
    set VAR(NKDISP)   "51"
    set VAR(KA1x)     "0.0"
    set VAR(KA1y)     "0.0"
    set VAR(KA1z)     "0.0"
    set VAR(KE1x)     "1.0"
    set VAR(KE1y)     "0.0"
    set VAR(KE1z)     "0.0"


    set VAR(MODEBSF) "1"
    set VAR(NKBSF)   "51"
    set VAR(KA1x)     "0.0"
    set VAR(KA1y)     "0.0"
    set VAR(KA1z)     "0.0"
    set VAR(KE1x)     "1.0"
    set VAR(KE1y)     "0.0"
    set VAR(KE1z)     "0.0"
    set VAR(NK1BSF)   "60"
    set VAR(NK2BSF)   "60"
    set VAR(KBSF1x)     "1.0"
    set VAR(KBSF1y)     "0.0"
    set VAR(KBSF1z)     "0.0"
    set VAR(KBSF2x)     "0.0"
    set VAR(KBSF2y)     "1.0"
    set VAR(KBSF2z)     "0.0"

    set VAR(TASK)        SCF

    set VAR(SCL_ALAT)          0
    set VAR(SCL_ALAT-SCL1)  0.95
    set VAR(SCL_ALAT-SCL2)  1.05
    set VAR(SCL_ALAT-NSCL)     9

    set VAR(plot1D-POT)   0
    set VAR(plot1D-RHO)   0
    set VAR(plot1D-SFN)   0

    set VAR(plot2D-POT) 0
    set VAR(plot2D-RHO) 0
    set VAR(plot2D-NA)  50
    set VAR(plot2D-NB)  50
    set VAR(plot2D-AVECx)   1
    set VAR(plot2D-AVECy)   0
    set VAR(plot2D-AVECz)   0
    set VAR(plot2D-BVECx)   0
    set VAR(plot2D-BVECy)   1
    set VAR(plot2D-BVECz)   0
    set VAR(plot2D-CVECx)   0
    set VAR(plot2D-CVECy)   0
    set VAR(plot2D-CVECz)   0

    set VAR(FULLPOT)    0
    set VAR(SPHERCELL)  0
    set VAR(SEMICORE)   0
    set VAR(LLOYD)      0

    set VAR(POTFMT)     "6"

#------------------------------------SPR-KKR standard TASK params
#    set VAR(SPR-KKR-TASK_INPVER)        new
    set VAR(SPR-KKR-TASK_STRVER)        SPKKR_FMT;
#------------------------------------SPR-KKR standard ENERGY params
    set VAR(SPR-KKR-ENERG_IMV_FIN_EV)   1.0
    set VAR(SPR-KKR-ENERG_IMV_INI_EV)   0.1
    set VAR(SPR-KKR-ENERG_EWORK_EV)     5.0
    set VAR(SPR-KKR-ENERG_EMIN_EV)      9999.0
    set VAR(SPR-KKR-ENERG_EMAX_EV)      9999.0

#------------------------------------SPR-KKR standard SPEC_PH params
    set VAR(SPR-KKR-SPEC-PH_THETA)      45.0
    set VAR(SPR-KKR-SPEC-PH_PHI)        90.0
    set VAR(SPR-KKR-SPEC-PH_EPHOT)      21.0
    set VAR(SPR-KKR-SPEC-PH_POL_P)      P; # P, S, C+, C-
#------------------------------------SPR-KKR standard SPEC_STR params
    #set VAR(SPR-KKR-SPEC-STR_N_LAYDBL)      {10(fin);10(ini)}
    set VAR(SPR-KKR-SPEC-STR_N_LAYDBL_FIN)     10
    set VAR(SPR-KKR-SPEC-STR_N_LAYDBL_INI)     10
    set VAR(SPR-KKR-SPEC-STR_NLAT_G_VEC)       30
    set VAR(SPR-KKR-SPEC-STR_N_LAYER)          30
    set VAR(SPR-KKR-SPEC-STR_SURF_BAR_MIN)      0.25
    set VAR(SPR-KKR-SPEC-STR_SURF_BAR_MAX)      0.25
#------------------------------------SPR-KKR standard SPEC params
#    set VAR(SPR-KKR-SPEC_SPOL)              4
#------------------------------------SPR-KKR standard SPEC_EL params
#    set VAR(SPR-KKR-SPEC-EL_THETA)      {0.0; 0.0}; # 0,0 to 0,0
    set VAR(SPR-KKR-SPEC-EL_THETA_MIN)      0.0
    set VAR(SPR-KKR-SPEC-EL_THETA_MAX)      0.0
#    set VAR(SPR-KKR-SPEC-EL_PHI)        {0.0, 0.0}
    set VAR(SPR-KKR-SPEC-EL_PHI_MIN)        0.0
    set VAR(SPR-KKR-SPEC-EL_PHI_MAX)        0.0
    set VAR(SPR-KKR-SPEC-EL_NT)         1
    set VAR(SPR-KKR-SPEC-EL_NP)         1
    set VAR(SPR-KKR-SPEC-EL_POL_E)      PZ; # PX, PY, PZ

#------------------------------------SPR-KKR expert TASK params
    set VAR(SPR-KKR-TASK_INPFIL)        ""; # filename
    set VAR(SPR-KKR-TASK_STRFIL)        ""; # filename
    set VAR(SPR-KKR-TASK_BARFIL)        ""; # filename
#------------------------------------SPR-KKR 3d Surface params
    set VAR(SPR-KKR-SPEC3D_IQ_AT_SURF)  1
    set VAR(SPR-KKR-SPEC3D_H)           0
    set VAR(SPR-KKR-SPEC3D_K)           0
    set VAR(SPR-KKR-SPEC3D_L)           1

# -----------------------------------SPR-KKR falg for increment NL (NL_USER)
    set VAR(APTRES_NL_INC)   0

}
# following VAR() depend on other global vars, so init/update always

set NL 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    if {$NL< $NLQ($IQ)} { set NL $NLQ($IQ) }
}
set VAR(NLUSER)  $NL


for {set IT 1} {$IT <= $NT} {incr IT} {
    set NVALT_DEFAULT($IT) [expr $ZT($IT) - [lindex $TABNCORE $ZT($IT)] ]
    debug $PRC "IT $IT   Z $ZT($IT)  NVAL(def) $NVALT_DEFAULT($IT)"
    
    for {set IL 1} {$IL <= $NL} {incr IL} {
	set INDNTL "nef$IT{_}$IL"
	set VAR($INDNTL) 0.0
    }
    
    set VAR(Brooks-OP-$IT) -
    set VAR(UHub-$IT) "0.0"
    set VAR(JHub-$IT) "0.0"
    set VAR(EHub-$IT) "0.0"
    set VAR(LDA+U-$IT) "n"
    set LOP 3
    for {set ml -$LOP} {$ml <= $LOP} {incr ml} {
	set VAR(LDA+U-$IT-$ml-DN) 0.0
	set VAR(LDA+U-$IT-$ml-UP) 0.0
    }
}

set VAR(KPATH) 1
switch $BRAVAIS {
    {0}  {# unknown
    }
    {1}  {# triclinic   primitive      -1     C_i
    }
    {2}  {# monoclinic  primitive      2/m    C_2h
    }							
    {3}  {# monoclinic  base-centered  2/m    C_2h		
    }							
    {4}  {# orthorombic primitive      mmm    D_2h 		
	set VAR(KPATH) 4					
    }							
    {5}  {# orthorombic base-centered  mmm    D_2h		
    }							
    {6}  {# orthorombic body-centered  mmm    D_2h		
    }							
    {7}  {# orthorombic face-centered  mmm    D_2h		
    }							
    {8}  {# tetragonal  primitive      4/mmm  D_4h		
    }							
    {9}  {# tetragonal  body-centered  4/mmm  D_4h		
    }							
    {10} {# trigonal (rhombohedral)   primitive      -3m    D_3d
    }
    {11} {# hexagonal   primitive      6/mmm  D_6h
	set VAR(KPATH) 4
    }
    {12} {# cubic       primitive      m3m    O_h
	set VAR(KPATH) 4
    }
    {13} {# cubic       face-centered  m3m    O_h
	set VAR(KPATH) 4
    }
    {14} {# cubic       body-centered  m3m    O_h
	set VAR(KPATH) 5
    }
}

regsub "$inpsuffix\$" $inpfile "" dataset
set VAR(DATASET) $dataset
#   $CLUSTER_TYPE == "NONE" 
if {$DIMENSION != "0D" } {
   set VAR(POTFIL)  "$dataset.pot"
} else {
   set VAR(POTFIL)      "${sysfile_host}.pot"
   set VAR(POTFIL_CLU)  "$dataset.pot"

   if {![file exists $VAR(POTFIL)]} {
      writescrd .d.tt $PRC "\n\nINFO from <create_inpfile_SPR-KKR>: \
                           \n\n default host potential file \
                           \n\n $VAR(POTFIL) \
                           \n\n does not exist  \
                           \n\n copy or rename file \n"
      give_warning "." "WARNING \n\n INFO from <create_inpfile_SPR-KKR>: \
                           \n\n default host potential file \
                           \n\n $VAR(POTFIL) \
                           \n\n does not exist  \
                           \n\n copy or rename file \n "
   }
}

set VAR(ADSI)    $VAR(TASK)

regsub "$syssuffix\$" $sysfile "" inpfile
set inpfile "$inpfile\_$VAR(TASK)$inpsuffix"


#=======================================================================================
#                             MAIN INPUT WINDOW
#
#                        supply 3 colums to be filled in
#=======================================================================================
pack configure $w.cols.col1 $w.cols.col2  $w.cols.col3 \
               -in $w.cols -side left -fill both -expand y

#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 1
#=======================================================================================
#
set create_inpfile_SPRKKR_wc1 $wc1
CreateENTRY       $wc1 $BGFIL $wl "   DATASET "   $we VAR DATASET  $fe 1 " "
CreateENTRY       $wc1 $BGFIL $wl "   ADSI "      $we VAR ADSI     $fe 1 " "
CreateENTRY       $wc1 $BGFIL $wl "   POTFIL"     $we VAR POTFIL   $fe 1 " "
if {$DIMENSION == "0D" } {
CreateENTRY       $wc1 $BGFIL $wl "   POTFIL-CLU" $we VAR POTFIL_CLU   $fe 1 " "
}

frame $wc1.label1
label $wc1.label1.lab -text "   TASKs   for the programs:   kkrscf, embscf" \
                      -bg $BGTSK1 -justify left
label $wc1.label1.blk -text " " -bg $BGTSK1 -justify left
pack  $wc1.label1.lab -side left ; pack  $wc1.label1.blk -side right -expand y -fill x
pack  $wc1.label1 -in $wc1 -anchor w -expand y -fill x

CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK aexec $fe s 1  SCF

frame $wc1.label2
label $wc1.label2.lab -text "   TASKs   for the programs:   kkrgen, embgen" \
                      -bg $BGTSK1 -justify left
label $wc1.label2.blk -text " " -bg $BGTSK1 -justify left
pack  $wc1.label2.lab -side left ; pack  $wc1.label2.blk -side right -expand y -fill x
pack  $wc1.label2 -in $wc1 -anchor w -expand y -fill x

if { $SPRKKR_EXPERT != 1 } {
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK bexec $fe s 1  DOS
} else {
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK bexec $fe s 1  DOS EKREL BLOCHSF
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK cexec $fe s 1  WFPLOT PLOT1D PLOT2D
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK dexec $fe s 1  PSHIFT SOCPAR
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK eexec $fe s 1  JXC FMAG
if { $AKHE == 1 } {
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK gexec $fe s 1  MEPLOT MECHECK
} else {
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK hexec $fe s 1  MEPLOT              
}
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK iexec $fe s 1  VBXPS CLXPS AES NRAES APS
CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK jexec $fe s 1  XAS XMO COMPTON POSANI 

frame $wc1.label3
label $wc1.label3.lab -text "   TASKs  for the programs:   kkrchi        " \
                      -bg $BGTSK1 -justify left
label $wc1.label3.blk -text " " -bg $BGTSK1 -justify left
pack  $wc1.label3.lab -side left ; pack  $wc1.label3.blk -side right -expand y -fill x
pack  $wc1.label3 -in $wc1 -anchor w -expand y -fill x

CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK mexec $fe s 1  CHI CHIXAS SIGMA GILBERT

frame $wc1.label4
label $wc1.label4.lab -text "   TASKs  for the programs:   kkrspec        " \
                      -bg $BGTSK1 -justify left
label $wc1.label4.blk -text " " -bg $BGTSK1 -justify left
pack  $wc1.label4.lab -side left ; pack  $wc1.label4.blk -side right -expand y -fill x
pack  $wc1.label4 -in $wc1 -anchor w -expand y -fill x

CreateRADIOBUTTON $wc1 $BGTSK $wl "        " VAR TASK nexec $fe s 1  ARPES ;# AIPES  LEED  BAND


}

set wee 17
if { $SPRKKR_EXPERT == 1 } {
CreateRADIOBUTTON $wc1 $BGE   $wl "   E-GRID "   VAR GRID     a $fe n 1 REAL RECTANG STRAIGHT
CreateRADIOBUTTON $wc1 $BGE   $wl "        "     VAR GRID     b $fe n 4 RECT-LOG ARC XAS XMO
}
CreateSCALE       $wc1 $BGE   $wl "   NE  "      $ws 0        600   0 3 1        VAR NE  $fe
CreateENTRY       $wc1 $BGE   $wl "   EMIN "     $wee VAR EMIN   $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   EMAX "     $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   EF   "     $wee VAR EF     $fe 3 "Ry"
if { $SPRKKR_EXPERT == 1 } {
    CreateCHECKBUTTON $wc1 $BGE   $wl " " VAR SEARCHEF    "search EF"
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
}

#---------------------------------------------------------- dummy label to fill space
if {$NCPA ==0} {
  label $wc1.col1_bot    -text "" -bg $BGE
  pack configure $wc1.col1_bot -in $wc1 -fill both -expand y
}
if { $SPRKKR_EXPERT == 1 } {
   if {$NCPA !=0} {
   CreateSCALE       $wc1 $BGCPA $wl "   CPANITER " $ws 0        30   0 2  1      VAR CPANITER $fe
   CreateSCALE       $wc1 $BGCPA $wl "   CPATOL "   $ws 0.00001 0.001 0 3 0.00001 VAR CPATOL   $fe
   CreateRADIOBUTTON $wc1 $BGCPA $wl "   CPA MODE"   VAR CPAMODE  a $fe s 1  "SINGLE SITE" "NON LOCAL"
   CreateSCALE       $wc1 $BGCPA $wl "   NL-LEVEL" $ws 1         2   0 1  1      VAR NLCPA-LVL $fe
   CreateCHECKBUTTON $wc1 $BGCPA $wl "   NL-WRITE"    VAR NLCPA-WRCFG    "CONFIG "
   }
} ; #SPRKKR_EXPERT

#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 2
#=======================================================================================
#
set create_inpfile_SPRKKR_wc2 $wc2

if { $SPRKKR_EXPERT == 1 } {
    CreateRADIOBUTTON $wc2 $BGKKR $wl "   KKRMODE "    VAR KKRMODE    a     $fe s 2  STANDARD TB CLUSTER  
    label $wc2.bz-parameter -text "   parameters for BZ-integration" -bg $BGK -anchor w
    pack  $wc2.bz-parameter -anchor w -fill x

  if { $AKHE == 1 } {
    CreateRADIOBUTTON $wc2 $BGK   $wl "   TAU CALC"    VAR IBZINT     aexec $fe s 1  WEYL POINTS TET ;##ZOOM
  } else {
    CreateRADIOBUTTON $wc2 $BGK   $wl "   TAU CALC"    VAR IBZINT     aexec $fe s 1  WEYL POINTS
  }
} else {
    CreateRADIOBUTTON $wc2 $BGKKR $wl "   KKRMODE "    VAR KKRMODE    a     $fe s 1  STANDARD CLUSTER  
    label $wc2.bz-parameter -text "   parameters for BZ-integration" -bg $BGK -anchor w
    pack  $wc2.bz-parameter -anchor w -fill x
    CreateRADIOBUTTON $wc2 $BGK   $wl "   TAU CALC"    VAR IBZINT     aexec $fe s 2       POINTS
}

if { $SPRKKR_EXPERT == 1 } {
CreateSCALE       $wc2 $BGK   $wl "   W NKMIN" $ws 0       4000   0 4  50  VAR NKMIN $fe
CreateSCALE       $wc2 $BGK   $wl "   W NKMAX" $ws 0       4000   0 4  50  VAR NKMAX $fe
}
CreateSCALE       $wc2 $BGK   $wl "   P NKTAB" $ws 0       4000   0 4  50  VAR NKTAB $fe
if { $SPRKKR_EXPERT == 1 } {
if { $AKHE == 1 } {
CreateSCALE       $wc2 $BGK   $wl "   T/Z  NF" $ws 1         20   0 2 1    VAR NF      $fe
}
label $wc2.lab_KKRSTRCONST -text "   KKR structure constants calculation" -anchor w -bg $BGK
pack  $wc2.lab_KKRSTRCONST -fill x
CreateSCALE       $wc2 $BGK   $wl "   ETA   "  $ws 0.0      1.0   0 3 0.05 VAR STRETA  $fe
CreateSCALE       $wc2 $BGK   $wl "   RMAX  "  $ws 0.0      9.9   0 3 0.05 VAR STRRMAX $fe
CreateSCALE       $wc2 $BGK   $wl "   GMAX  "  $ws 0.0      9.9   0 3 0.05 VAR STRGMAX $fe
}


label $wc2.cluster-set-up -text "   parameters for cluster set-up" -bg $BGCLU -anchor w
pack  $wc2.cluster-set-up -anchor w -fill x

###CreateRADIOBUTTON $wc2 $BGCLU $wl "   TAU CALC"    VAR IBZINT     bexec $fe s 0  CLUSTER
#if { $SPRKKR_EXPERT == 1 } {
#CreateRADIOBUTTON $wc2 $BGCLU $wl "   CLUALG"      VAR CLUALG    a $fe s 1  TAUMAT TAUEXP
#}
CreateSCALE       $wc2 $BGCLU $wl "   IQCNTR"  $ws 0        $NQ   0 [PREC_INT $NQ] 1    VAR    IQCNTR  $fe
#### SUPPRESSED CreateSCALE       $wc2 $BGCLU $wl "   ITCNTR"  $ws 0        $NT   0 [PREC_INT $NT] 1    VAR    ITCNTR  $fe
frame $wc2.clu1   -bg $BGCLU ; pack $wc2.clu1 -fill both -expand y
frame $wc2.clu1.a -bg $BGCLU ; frame $wc2.clu1.b -bg $BGCLU
 pack $wc2.clu1.a $wc2.clu1.b -fill both -expand y -side left
CreateSCALE       $wc2.clu1.a $BGCLU $wl "   NSHLCLU" $ws1 0         30   0 2 1    VAR    NSHLCLU $fe
CreateRADIOBUTTON $wc2.clu1.b $BGCLU 1 " "    VAR CLU-SPEC  a $fe s 1 NSH
frame $wc2.clu2   -bg $BGCLU ; pack $wc2.clu2 -fill both -expand y
frame $wc2.clu2.a -bg $BGCLU; frame $wc2.clu2.b  -bg $BGCLU
 pack $wc2.clu2.a $wc2.clu2.b -fill both -expand y -side left
CreateSCALE       $wc2.clu2.a $BGCLU $wl "   CLURAD"  $ws1 0         20   0 3 0.1  VAR    CLURAD  $fe
CreateRADIOBUTTON $wc2.clu2.b $BGCLU 1 " "    VAR CLU-SPEC  b $fe s 2 RAD

###CreateSCALE       $wc2 $BGCLU $wl "   NSHLCLU" $ws 0         30   0 2 1    VAR    NSHLCLU $fe
if { $SPRKKR_EXPERT == 1 } {
CreateSCALE       $wc2 $BGCLU $wl "   NLOUT"   $ws 0          6   0 1 1    VAR    NLOUT   $fe
}

widget_disable  $wc2.wVARNKMIN.2
widget_disable  $wc2.wVARNKMAX.2
#DEFAULT! widget_disable  $wc2.wVARNKTAB.2
widget_disable  $wc2.wVARNF.2
widget_disable  $wc2.wVARIQCNTR.2
widget_disable  $wc2.wVARITCNTR.2
widget_disable  $wc2.wVARNSHLCLU.2
widget_disable  $wc2.wVARCLURAD.2
widget_disable  $wc2.wVARNLOUT.2

# STRUC
# NQSLAB
# NSURF
# PATHLEN
# NLEGXX
# ISYM
if { $SPRKKR_EXPERT == 1 } {
   label $wc2.lex -text "   angular momentum expansion NL = l_max + 1"  -bg $BGLEX -anchor w
   pack $wc2.lex -anchor w -fill x
   CreateSCALE       $wc2 $BGLEX $wl "   NL         " $ws 1  12   0 2  1      VAR NLUSER $fe

   label $wc2.col2_bot    -text "" -bg $BGLEX
   pack configure $wc2.col2_bot -in $wc2 -fill both -expand y
} else {
   label $wc2.col2_bot    -text "" -bg $BGK
   pack configure $wc2.col2_bot -in $wc2 -fill both -expand y
}


#=======================================================================================
#                       MAIN INPUT WINDOW -- COLUMN 3
#=======================================================================================
#
set create_inpfile_SPRKKR_wc3 $wc3

#
if { $SPRKKR_EXPERT == 1 } {

   set wlsmall 1

   if { $SPRKKR_EXPERT == 1 } {
   frame $wc3.crl1    -bg $BGCRL ; pack $wc3.crl1 -fill x
   frame $wc3.crl1.a  -bg $BGCRL
   frame $wc3.crl1.b  -bg $BGCRL
   pack  $wc3.crl1.a  $wc3.crl1.b -side left -anchor n -expand y -fill x
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      "   CONTROL" VAR SEMICORE  "SEMICORE"
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      " "          VAR FULLPOT   "FULLPOT "
   CreateCHECKBUTTON $wc3.crl1.a $BGCRL  $wl      " "          VAR NONMAG    "NONMAG    "
   CreateCHECKBUTTON $wc3.crl1.b $BGCRL  $wlsmall " "          VAR LLOYD     "LLOYD"
   CreateCHECKBUTTON $wc3.crl1.b $BGCRL  $wlsmall " "          VAR SPHERCELL "SPHERCELL"
   }
 
   if { $SPRKKR_EXPERT == 1 } {
   CreateRADIOBUTTON $wc3 $BGMOD $wl "   Hamiltonian" VAR HAMILTONIAN a1     $fe s 0  NREL SREL SP-SREL
   CreateRADIOBUTTON $wc3 $BGMOD $wl "             "  VAR HAMILTONIAN a2exec $fe s 0  REL  Brooks-OP LDA+U
   CreateRADIOBUTTON $wc3 $BGMOD $wl "   manipulate"  VAR HAMILTONIAN b1exec $fe s 4  scale-c scale-SOC
   CreateRADIOBUTTON $wc3 $BGMOD $wl "       SOC   "  VAR HAMILTONIAN b2exec $fe s 4  SOC-xy SOC-zz
   }

   set VAR(KMROT) "0"
   if { $SPRKKR_EXPERT == 1 } {
   CreateRADIOBUTTON $wc3 $BGMAG $wl "  orientation"  VAR KMROT aexec $fe n 0 z-axis "common orientation"
   CreateRADIOBUTTON $wc3 $BGMAG $wl "  of magnet. "  VAR KMROT bexec $fe n 2 "non-collinear"
   if { $AKHE == 1 } {
   CreateRADIOBUTTON $wc3 $BGMAG $wl "             "  VAR KMROT cexec $fe n 3 "orthogonal spin-spiral" "spin-spiral"
   }
   }

   if { $SPRKKR_EXPERT == 1 } {
      CreateRADIOBUTTON $wc3 $BGP $wl "   PRINT   "     VAR IPRINT   a $fe n 0  0 1 2 3 4 5
      CreateRADIOBUTTON $wc3 $BGP $wl "   TEST "        VAR ITEST    a $fe n 0  0 1 2 3 4
   }

   frame $wc3.out1    -bg $BGWR ; pack $wc3.out1 -fill x
   frame $wc3.out1.a  -bg $BGWR
   frame $wc3.out1.b  -bg $BGWR
   pack  $wc3.out1.a  $wc3.out1.b -side left -expand y -fill x
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      "   OUTPUT" VAR NOWRDOS  "NOWRDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRLMLDOS "WRLMLDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRKAPDOS "WRKAPDOS"
   CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl      " "         VAR WRPOLAR  "WRPOLAR"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR NOHFF    "NOHFF"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR NOCORE   "NOCORE"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR FSOHFF   "FSOHFF"
   CreateCHECKBUTTON $wc3.out1.b $BGWR  $wlsmall " "         VAR EFG      "EFG"
   if {$NCPA !=0} {
      CreateCHECKBUTTON $wc3.out1.a $BGWR  $wl   "  "        VAR WRCPA    "WRCPA"
   }

   set VAR(TAUIO) "AUTO"
   frame $wc3.tauio    -bg $BGTAU ; pack $wc3.tauio -fill x
   CreateRADIOBUTTON $wc3.tauio   $BGTAU $wl "   TAU-MAT"    VAR TAUIO a $fe s 0 "AUTO"

   frame $wc3.tauio.a  -bg $BGTAU
   frame $wc3.tauio.b  -bg $BGTAU
   pack  $wc3.tauio.a  $wc3.tauio.b -side left -expand y -fill x

   CreateRADIOBUTTON $wc3.tauio.a $BGTAU $wl      "   WRITE"    VAR TAUIO d $fe s 0 "WRTAUMQ"
   CreateRADIOBUTTON $wc3.tauio.a $BGTAU $wl      "   READ "    VAR TAUIO e $fe s 0 "RDTAUMQ"
   CreateRADIOBUTTON $wc3.tauio.b $BGTAU $wlsmall " "           VAR TAUIO b $fe s 0 "WRTAU"
   CreateRADIOBUTTON $wc3.tauio.b $BGTAU $wlsmall " "           VAR TAUIO c $fe s 0 "RDTAU"

   CreateCHECKBUTTON $wc3 tomato $wl "   POTFILE" VAR OWRPOT   "OVERWRITE"
   widget_disable     $wc3.wVAROWRPOT.2
   CreateRADIOBUTTON $wc3 tomato $wl "   POTFMT   "     VAR POTFMT   a $fe n 5  5 6

   frame  $wc3.col3_bot
   button $wc3.col3_bot.experts -text "STANDARD MODE" -width 20 -height $buttonheight \
          -command "destroy $Wcinp ; set SPRKKR_EXPERT 0 ; create_inpfile " \
          -bg lightblue
} else {
   frame  $wc3.col3_bot
   button $wc3.col3_bot.experts -text "EXPERT MODE" -width 20 -height $buttonheight \
          -command "destroy $Wcinp ; set SPRKKR_EXPERT 1 ; create_inpfile " \
          -bg lightblue
}

pack  $wc3.col3_bot -expand y -fill both -anchor c
pack  $wc3.col3_bot.experts -expand y -fill both


#--------------------------------------------------------------------------------



########################################################################################
#                                                 exec_RADIOBUTTON_cmd  for SPR-KKR
proc exec_RADIOBUTTON_cmd {KEY} {

global VAR NT TXTT CONC ZT NL BRAVAIS NCPA NQ
global BGCPA BGSCF BGTSK BGTAU BGWR BGCLU BGP BGK BGE BGX BGMAG
global BGCRL SPRKKR_EXPERT BGMOD NVALT_DEFAULT
global create_inpfile_SPRKKR_wc1
global create_inpfile_SPRKKR_wc2
global create_inpfile_SPRKKR_wc3
global inpfile inpsuffix sysfile syssuffix
global COLOR WIDTH HEIGHT FONT
global TABCQNTOP_N

set ws1 100


#--------------------------------------------------------------------------------
#                             fixed photon energy
#
if {$KEY == "EPHOT-fixed" } {
    if {$VAR(EPHOT-fixed) == "He I" } {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 20.1; set VAR(EPHOTMAX) 20.1}
    if {$VAR(EPHOT-fixed) == "He II"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 48.1; set VAR(EPHOTMAX) 48.1}
    if {$VAR(EPHOT-fixed) == "Al Ka"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 1111; set VAR(EPHOTMAX) 1111}
    if {$VAR(EPHOT-fixed) == "Mg Ka"} {set VAR(NEPHOT) 1; set VAR(EPHOTMIN) 1311; set VAR(EPHOTMAX) 1311}
    return
}
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
#                              check NCXRAY and LCXRAY 
#          restrict quantum number n to be compatible with atomic number Z
#          restrict quantum number l to hold  l < n
#
if {$KEY == "NCXRAY" || $KEY == "LCXRAY" } {
    set CQNTOP_N [lindex $TABCQNTOP_N $ZT($VAR(ITXRAY))]
    if {$VAR(NCXRAY) > $CQNTOP_N} {
       set VAR(NCXRAY) $CQNTOP_N
    }

    set I 0
    foreach X [list s p d f] { incr I ; set IL($X) $I }
    if {$IL($VAR(LCXRAY)) > $VAR(NCXRAY)} {
       foreach X [list s p d f] { if {$IL($X)<=$VAR(NCXRAY)} {set VAR(LCXRAY) $X}  }
    }
    return
}
#--------------------------------------------------------------------------------



#--------------------------------------------------------------------------------
#                              check the TAU-calculation mode scales
if {$KEY == "IBZINT"} {

widget_disable  $create_inpfile_SPRKKR_wc2.wVARNKMIN.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNKMAX.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNKTAB.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNKTAB.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNF.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARIQCNTR.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARITCNTR.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNSHLCLU.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARCLURAD.2
widget_disable  $create_inpfile_SPRKKR_wc2.wVARNLOUT.2

widget_enable  $create_inpfile_SPRKKR_wc2.wVARSTRETA.2
widget_enable  $create_inpfile_SPRKKR_wc2.wVARSTRRMAX.2
widget_enable  $create_inpfile_SPRKKR_wc2.wVARSTRGMAX.2

set CASE $VAR(IBZINT)

if {$CASE == "WEYL"} {
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNKMIN.2
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNKMAX.2
    return			
} elseif {$CASE == "POINTS"} {	
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNKTAB.2
    return			
} elseif {$CASE == "TET"} {	
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNF.2
    return			
} elseif {$CASE == "ZOOM"} {	
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNF.2
    return			
} elseif {$CASE == "CLUSTER"} {	
    widget_disable  $create_inpfile_SPRKKR_wc2.wVARSTRETA.2
    widget_disable  $create_inpfile_SPRKKR_wc2.wVARSTRRMAX.2
    widget_disable  $create_inpfile_SPRKKR_wc2.wVARSTRGMAX.2

    widget_enable $create_inpfile_SPRKKR_wc2.wVARIQCNTR.2
    widget_enable $create_inpfile_SPRKKR_wc2.wVARITCNTR.2
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNSHLCLU.2
    widget_enable $create_inpfile_SPRKKR_wc2.wVARCLURAD.2
    widget_enable $create_inpfile_SPRKKR_wc2.wVARNLOUT.2
    return
}

}
#--------------------------------------------------------------------------------


    set Wrbcmd .c_rad_but_cmd
    if {[winfo exists $Wrbcmd] == 1} {destroy $Wrbcmd}
    toplevel_init $Wrbcmd "supply information for   $KEY" 0 0
    wm geometry $Wrbcmd +300+200
    wm positionfrom $Wrbcmd user
    wm sizefrom $Wrbcmd ""
    wm minsize $Wrbcmd

    set buttonbgcolor green1
    set buttonheight  2

    set w  $Wrbcmd
    set wl 12
    set we 30
    set ws 160
    set fe $FONT(GEN)
    set wee 17

    label $Wrbcmd.header
    pack configure $Wrbcmd.header -side top -anchor n -pady 14

    frame $Wrbcmd.a
#------------------------------------------------------------------------------------
    frame $Wrbcmd.a.but
#
     button $Wrbcmd.a.but.ok -text "RETURN" -width 20 -height $buttonheight \
	-command "exec_RADIOBUTTON_cmd_RETURN $Wrbcmd $KEY" -bg $buttonbgcolor
#
     pack configure $Wrbcmd.a.but.ok -anchor s -side left -pady 1 -padx 30 -pady 15

##     button $Wrbcmd.a.but.help -text "HELP" -width 20 -height $buttonheight \
##           -command "exec_RADIOBUTTON_cmd_HELP $Wrbcmd $KEY" -bg $buttonbgcolor
#
##     pack configure $Wrbcmd.a.but.help -anchor s -side right -pady 1 -padx 30 -pady 15


#------------------------------------------------------------------------------------
     pack configure $Wrbcmd.a.but -side left -fill x
     pack configure $Wrbcmd.a -side bottom -anchor s
#------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------
#                       make final consistency checks before returning
#------------------------------------------------------------------------------------
proc exec_RADIOBUTTON_cmd_RETURN {w KEY} {
global VAR create_inpfile_SPRKKR_wc1
   if {$KEY == "TASK" } {
      set TASK $VAR(TASK)
   } else {
      set TASK "UNKNOWN"
   }
#-------------------------------------------
   if {$TASK == "BLOCHSF" } {
       if {$VAR(MODEBSF)==2} {
          $create_inpfile_SPRKKR_wc1.wVARNE.2 configure -from 0 -to 250 -digits 3 -resolution 1
          set VAR(EMIN) $VAR(EMAX)
          set VAR(NE) 1
       }
   }
#-------------------------------------------
destroy $w
}
#------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------
#                     supply column to be filled in
#--------------------------------------------------------------------------------
frame $w.cols
pack configure $w.cols -in $w -anchor w -side top -fill x -expand y

frame $w.cols.col1 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col2 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col3 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col4 -bg $BGP -relief raised -borderwidth 2
frame $w.cols.col5 -bg $BGP -relief raised -borderwidth 2

pack configure $w.cols.col1 -in $w.cols -side left -fill both -expand y
pack configure $w.cols.col2 -in $w.cols -side left -fill both -expand y
pack configure $w.cols.col3 -in $w.cols -side left -fill both -expand y
pack configure $w.cols.col4 -in $w.cols -side left -fill both -expand y
pack configure $w.cols.col5 -in $w.cols -side left -fill both -expand y

set wc1 $w.cols.col1 
set wc2 $w.cols.col2
set wc3 $w.cols.col3 
set wc4 $w.cols.col4
set wc5 $w.cols.col5

frame $w.base -bg $BGTSK -relief raised -borderwidth 2
pack configure $w.base -in $w -anchor w -side top -fill x -expand y

#
#------------------------------- to be used for TASK EKREL and BLOCHSF
#
    set KPLAB(1) "   " ;  set KPLAB(2) "   " ;  set KPLAB(3) "   "
    set KPLAB(4) "   " ;  set KPLAB(5) "   "
	
    switch $BRAVAIS {
        {0}  {# unknown
              set KPATHTOP 0
             }
        {1}  {# triclinic   primitive      -1     C_i
              set KPATHTOP 0
             }
        {2}  {# monoclinic  primitive      2/m    C_2h
              set KPATHTOP 0
	     }											
        {3}  {# monoclinic  base-centered  2/m    C_2h
              set KPATHTOP 0
              }											
        {4}  {# orthorombic primitive      mmm    D_2h 	
              set KPATHTOP 4
              set KPLAB(1) "G-Y-T-Z-G-Y-T-Z + X-S-Y + U-R-T + S-T"
              set KPLAB(2) "G-Y-T-Z-G-Y-T-Z"
              set KPLAB(3) "G-Y-T-Z-G"
              set KPLAB(4) "G-Y-T-Z"
              }											
        {5}  {# orthorombic base-centered  mmm    D_2h
              set KPATHTOP 0
              }											
        {6}  {# orthorombic body-centered  mmm    D_2h
              set KPATHTOP 0
              }											
        {7}  {# orthorombic face-centered  mmm    D_2h	
              set KPATHTOP 0
              }											
        {8}  {# tetragonal  primitive      4/mmm  D_4h
              set KPATHTOP 0
              }											
        {9}  {# tetragonal  body-centered  4/mmm  D_4h
              set KPATHTOP 0
              }											
        {10} {# trigonal (rhombohedral)   primitive      -3m    D_3d
              set KPATHTOP 0
              }
        {11} {# hexagonal   primitive      6/mmm  D_6h
              set KPATHTOP 5
              set KPLAB(1) "G-M-K-G-A-L-H-A + M-L + K-H"
              set KPLAB(2) "G-M-K-G-A-L-H-A"
              set KPLAB(3) "G-M-K-G-A"
              set KPLAB(4) "G-M"
              set KPLAB(5) "K-G"
             }
        {12} {# cubic       primitive      m3m    O_h
              set KPATHTOP 5
              set KPLAB(1) "G-X-M-R-G-M"
              set KPLAB(2) "G-X-M-R-G"
              set KPLAB(3) "G-X-M-R"
              set KPLAB(4) "G-X-M"
              set KPLAB(5) "G-X + G-Y + G-Z"
            }
        {13} {# cubic       face-centered  m3m    O_h
              set KPATHTOP 6
              set KPLAB(1) "X-G-L-W-K-G + L-U-X-W-U"
              set KPLAB(2) "X-G-L-W-K-G" ;
              set KPLAB(3) "X-G-L" ; set KPLAB(4) "G-X" ;  set KPLAB(5) "G-L"
              set KPLAB(6) "G-X + G-Y + G-Z"
              }
        {14} {# cubic       body-centered  m3m    O_h
              set KPATHTOP 6
              set KPLAB(1) "G-H-N-G-P-H + N-P"
              set KPLAB(2) "G-H-N-G-P-H" ; set KPLAB(3) "G-H-N-G-P"
              set KPLAB(4) "G-H-N-G"     ; set KPLAB(5) "G-H"
              set KPLAB(6) "G-X + G-Y + G-Z"
              }
    }
#
#--------------------------------------------------------------------------------
#                             supply additional information for HAMILTONIAN
if {$KEY == "HAMILTONIAN"} {
#
#--------------------------------------- deactivate columns 2,3,4
#
pack forget $w.cols.col2
pack forget $w.cols.col3
pack forget $w.cols.col4

if { $VAR(HAMILTONIAN) == "Brooks-OP" || $VAR(HAMILTONIAN) == "LDA+U" } {
   $Wrbcmd.header configure -text "modified relativistic Hamiltonian   "
} else {
   $Wrbcmd.header configure -text "manipulation of the spin-orbit coupling "
}

#
#------------------------------------------------------------------------ standard REL
if { $VAR(HAMILTONIAN) == "REL" } {
   destros $Wrbcmd
   return
#
#------------------------------------------------------------------------ Brooks-OP
} elseif { $VAR(HAMILTONIAN) == "Brooks-OP" } {

label $wc1.headtxt  -text "add Brooks orbital polarisation (OP) term"  \
                    -anchor n  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x

for {set IT 1} {$IT <= $NT} {incr IT} {
   if {[is_transition_metal $ZT($IT)]} {
      set VAR(Brooks-OP-$IT) "d"
      CreateRADIOBUTTON $wc1 $BGMOD $wl "   $TXTT($IT)" VAR Brooks-OP-$IT a $fe s 0  - d
   } elseif { [is_f_element $ZT($IT)]} {
      set VAR(Brooks-OP-$IT) "f"
      CreateRADIOBUTTON $wc1 $BGMOD $wl "   $TXTT($IT)" VAR Brooks-OP-$IT a $fe s 0  - f
   } else {
      set VAR(Brooks-OP-$IT) "-"
   }
}
#
#------------------------------------------------------------------------ LDA+U
} elseif { $VAR(HAMILTONIAN) == "LDA+U"    } {

label $wc1.headtxt  -text "add LDA+U-term to the Hamiltonian"  \
                     -anchor n -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
frame $wc1.head_dc -bg $BGMOD
label $wc1.head_dc.lab1 -text "double counting mode" -bg $BGMOD
radiobutton $wc1.head_dc.amf -variable VAR(LDA+U-DC) -text "AMF" -value "MF" \
            -bg $BGMOD -highlightthickness 0  
radiobutton $wc1.head_dc.aal -variable VAR(LDA+U-DC) -text "AAL" -value "AL" \
            -bg $BGMOD -highlightthickness 0  
pack configure $wc1.head_dc.lab1 $wc1.head_dc.amf $wc1.head_dc.aal \
     -in $wc1.head_dc -anchor nw -side left -fill x
$wc1.head_dc.aal select

pack configure $wc1.headtxt $wc1.head_dc -in $wc1 -anchor nw -side top -fill x

for {set IT 1} {$IT <= $NT} {incr IT} {
   if {[is_transition_metal $ZT($IT)] ||  [is_f_element $ZT($IT)]} {

      frame $wc1.frame$IT -bg $BGMOD
      set FRIT $wc1.frame$IT
      label $FRIT.lab1 -text " $TXTT($IT)" -width 12 -bg $BGMOD
      radiobutton $FRIT.rad1 -variable VAR(LDA+U-$IT) -text "NO" -value "n" \
                  -bg $BGMOD -highlightthickness 0  \
                  -command "cinp_SPR-KKR_LDA_U $BGMOD $IT \-"

      pack  $FRIT.lab1 $FRIT.rad1 -side left

      if {[is_transition_metal $ZT($IT)] } {

         radiobutton $FRIT.rad2 -variable VAR(LDA+U-$IT) -text "d" -value "d" \
                     -bg $BGMOD -highlightthickness 0  \
                      -command "cinp_SPR-KKR_LDA_U $BGMOD $IT d"

         pack  $FRIT.rad2 -side left

      } else {

         radiobutton $FRIT.rad3 -variable VAR(LDA+U-$IT) -text "f" -value "f" \
                      -bg $BGMOD -highlightthickness 0  \
                      -command "cinp_SPR-KKR_LDA_U $BGMOD $IT f"

         pack  $FRIT.rad3 -side left
      }

      label $FRIT.lab2U -text "         U = " -width 6 -bg $BGMOD
      label $FRIT.lab3U -textvariable VAR(UHub-$IT) -width 4 -bg $BGMOD
      label $FRIT.lab4U -text " eV" -width 2 -bg $BGMOD
      pack  $FRIT.lab2U $FRIT.lab3U $FRIT.lab4U  -side left
      label $FRIT.lab2J -text "         J = " -width 6 -bg $BGMOD
      label $FRIT.lab3J -textvariable VAR(JHub-$IT) -width 4 -bg $BGMOD
      label $FRIT.lab4J -text " eV" -width 2 -bg $BGMOD
      pack  $FRIT.lab2J $FRIT.lab3J $FRIT.lab4J  -side left
#      label $FRIT.lab2E -text "         E = " -width 6 -bg $BGMOD
#      label $FRIT.lab3E -textvariable VAR(EHub-$IT) -width 4 -bg $BGMOD
#      label $FRIT.lab4E -text " Ry" -width 2 -bg $BGMOD
#      pack  $FRIT.lab2E $FRIT.lab3E $FRIT.lab4E  -side left
#      pack  $FRIT.lab2E  -side left
      label $FRIT.space -text "   " -width 4 -bg $BGMOD
      pack  $FRIT.space  -side left
      pack  $wc1.frame$IT -fill x
   }
}

#--------------------------------------------------------------- proc cinp_SPR-KKR_LDA_U
proc cinp_SPR-KKR_LDA_U {BG IT KEY} {
    global VAR TXTT
    global COLOR

    set w .cinp_LDA_U
    if {[winfo exists $w] == 1} {destroy $w}
    toplevel_init $w "supply information for  LDA+U  calculation" 0 0
    wm geometry $w +300+200
    wm positionfrom $w user
    wm sizefrom $w ""
    wm minsize $w

    set buttonbgcolor green1
    set buttonheight  2

    set wl 12
    set we 30
    set ws 160
    set wee 17

    if {$KEY =="-"} {
       set VAR(LDA+U-$IT) "n"
       destros $w
       return
    } elseif {$KEY =="d"} {
       set LOP 2
       set VAR(EHub-$IT) "0.70"
    } else {
       set LOP 3
       set VAR(EHub-$IT) "0.42"
    }

    label $w.header -text "supply information for atom type  $IT  $TXTT($IT)" \
             -bg $BG

    pack configure $w.header -side top -anchor n -fill both

#----------------------------------
label $w.space0U  -text " "  -bg $BG
label $w.hubbU  -text "screened Coulomb parameter "  -bg $BG
pack configure $w.space0U $w.hubbU  -side top -anchor n -fill both

set FRU $w.frameU
frame $FRU -bg $BG
pack  $FRU -fill x
label $FRU.lab1 -text "      U" -width 10 -bg $BG
scale $FRU.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
            -resolution 0.01 -bg $BG -variable VAR(UHub-$IT) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRU.lab2 -text " eV" -width 2 -bg $BG
pack  $FRU.lab1 $FRU.sca $FRU.lab2 -side left

label $w.space0J  -text " "  -bg $BG
label $w.hubbJ  -text "exchange parameter "  -bg $BG
pack configure $w.space0J $w.hubbJ  -side top -anchor n -fill both

set FRJ $w.frameJ
frame $FRJ -bg $BG
pack  $FRJ -fill x
label $FRJ.lab1 -text "      J" -width 10 -bg $BG
scale $FRJ.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
            -resolution 0.01 -bg $BG -variable VAR(JHub-$IT) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRJ.lab2 -text " eV" -width 2 -bg $BG
pack  $FRJ.lab1 $FRJ.sca $FRJ.lab2 -side left

label $w.space0E  -text " "  -bg $BG
#label $w.hubbE  -text "reference energy   "  -bg $BG
#pack configure $w.space0E $w.hubbE  -side top -anchor n -fill both
pack configure $w.space0E   -side top -anchor n -fill both

set FRE $w.frameE
frame $FRE -bg $BG
pack  $FRE -fill x
#label $FRE.lab1 -text "      E" -width 10 -bg $BG
#scale $FRE.sca -length 200 -from 0.0 -to 7.0 -digits 3 \
#            -resolution 0.01 -bg $BG -variable VAR(EHub-$IT) \
#            -orient horizontal -width 10 -showvalue true -highlightthickness 0
#label $FRE.lab2 -text " Ry" -width 2 -bg $BG
#pack  $FRE.lab1 $FRE.sca $FRE.lab2 -side left
#pack  $FRE.lab1 -side left

#---------------------------------------------------------------------------------------
#                                               ml- and spin-resolved occupation numbers

############################################## SUPPRESSED
set SUPPRESS YES

if {$SUPPRESS =="NO"} { ;############################################SUPPRESS

label $w.space1  -text " "  -bg $BG
label $w.occup  -text "ml- and spin-resolved occupation numbers " \
         -bg $BG
pack configure $w.space1 $w.occup  -side top -anchor n -fill both

set FRML $w.frame0
frame $FRML -bg $BG
pack  $FRML -fill x
label $FRML.lab1 -text "      ml" -width 10 -bg $BG
label $FRML.lab2 -text "     spin DOWN" -width 28 -bg $BG
label $FRML.lab3 -text "     spin UP  " -width 28 -bg $BG
label $FRML.lab4 -text "   " -bg $BG
pack  $FRML.lab1 $FRML.lab2 $FRML.lab3 -side left

set ml [expr -$LOP-1]
for {set i 1} {$i <= [expr 2*$LOP+1]} {incr i} {
incr ml
set FRML $w.frame$i
frame $FRML -bg $BG
pack  $FRML -fill x
label $FRML.lab1 -text "      $ml" -width 10 -bg $BG
scale $FRML.sca1 -length 200 -from 0.0 -to 1.0 -digits 3 \
            -resolution 0.01 -bg moccasin -variable VAR(LDA+U-$IT-$ml-DN) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
scale $FRML.sca2 -length 200 -from 0.0 -to 1.0 -digits 3 \
            -resolution 0.01 -bg LightSkyBlue -variable VAR(LDA+U-$IT-$ml-UP) \
            -orient horizontal -width 10 -showvalue true -highlightthickness 0
label $FRML.lab2 -text "  " -width 10 -bg $BG
pack  $FRML.lab1 $FRML.sca1 $FRML.sca2 $FRML.lab2 -side left
}

} ;############################################SUPPRESS
#                                               ml- and spin-resolved occupation numbers
#---------------------------------------------------------------------------------------

label $w.foot -text " " -bg $BG

pack configure $w.foot -side top -anchor n -fill both


frame $w.frameEND -bg $BG
pack  $w.frameEND -fill both
button $w.frameEND.return -text "RETURN"  -width 20 -height $buttonheight -bg $buttonbgcolor \
       -command "destros $w ; return"
pack  $w.frameEND.return

}
#--------------------------------------------------------------- proc cinp_SPR-KKR_LDA_U

#
#------------------------------------------------------------------------ scale-c
} elseif { $VAR(HAMILTONIAN) == "scale-c" } {

label $wc1.headtxt  -text "   by scaling the speed of light " -anchor w  -padx 80 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(scale-c$IT) 1.0
CreateSCALE       $wc1 $BGMOD $wl "   $TXTT($IT)" $ws 0.0  2.0   0 3 0.01  VAR scale-c$IT  $fe
}
CreateTEXT        $wc1 $BGX info \
"�
 manipulate ALL relativsitic effects by scaling 1/c^2

      0:    NON-relativistic limit  (Schroedinger)   
      1:    relativistic case          (Dirac)
     >1:   enhancement of relativistic effects

"
#
#------------------------------------------------------------------------ scale-SOC
} elseif { $VAR(HAMILTONIAN) == "scale-SOC" } {

label $wc1.headtxt  -text "   by scaling the spin-orbit coupling strength" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(scale-SOC$IT) 1.0
CreateSCALE       $wc1 $BGMOD $wl "   $TXTT($IT)" $ws 0.0  2.0   0 3 0.01  VAR scale-SOC$IT  $fe
}
CreateTEXT        $wc1 $BGX info \
"�
 manipulate the strength of the spin-orbit coupling
 all other relativistic effects are unchanged

      0:    scalar-relativistic mode 
      1:    relativistic case       (Dirac)
     >1:   enhancement of spin-orbit coupling

"

#
#------------------------------------------------------------------------ use SOC-xy
} elseif { $VAR(HAMILTONIAN) == "SOC-xy"    } {
label $wc1.headtxt  -text "   by using only the xy-part of the spin-orbit coupling" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(SOC-xy$IT) 1
CreateCHECKBUTTON $wc1 $BGMOD $wl " " VAR SOC-xy$IT    "$TXTT($IT)"
}
#
#------------------------------------------------------------------------ use SOC-zz
} elseif { $VAR(HAMILTONIAN) == "SOC-zz"    } {
label $wc1.headtxt  -text "   by using only the zz-part of the spin-orbit coupling" -anchor w  -padx 30 -bg $BGMOD
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
for {set IT 1} {$IT <= $NT} {incr IT} {
set VAR(SOC-zz$IT) 1
CreateCHECKBUTTON $wc1 $BGMOD $wl " " VAR SOC-zz$IT    "$TXTT($IT)"
}
#--------------------------------------------------------------------------------
} ; # if-else----

} ; # KEY == HAMILTONIAN
#                             supply additional information for HAMILTONIAN
#--------------------------------------------------------------------------------
#


#
#--------------------------------------------------------------------------------
#                             supply additional information for KMROT
if {$KEY == "KMROT"} {

#
#--------------------------------------- deactivate columns 2,3,4
#
pack forget $w.cols.col2
pack forget $w.cols.col3
pack forget $w.cols.col4

$Wrbcmd.header configure -text "orientation of the magnetisation "


#
#------------------------------------------------------------------------ KMROT = 0 z-axis
if { $VAR(KMROT) == "0" } {
   destros $w
   return
#
#------------------------------------------------------------------------ KMROT = 1 global rotation
} elseif { $VAR(KMROT) == "1" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "common orientation vector   MDIR   (Cart. units)" -padx 30 -bg $BGMAG
pack $wc1.headtxt  -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(MDIR-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(MDIR-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(MDIR-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x   -padx 2 -pady 30
#CreateSCALE       $wc1 $BGMAG $wl "        x" $ws -2.0  2.0   0 3 0.01  VAR MDIR-x  $fe
#CreateSCALE       $wc1 $BGMAG $wl "        y" $ws -2.0  2.0   0 3 0.01  VAR MDIR-y  $fe
#CreateSCALE       $wc1 $BGMAG $wl "        z" $ws -2.0  2.0   0 3 0.01  VAR MDIR-z  $fe
#
#------------------------------------------------------------------------ KMROT = 2 non-collinear spin configuration
} elseif { $VAR(KMROT) == "2" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "non-collinear spin configuration   MDIRQ  (Cart. units)" -padx 2 -bg $BGMAG
pack $wc1.headtxt  -fill x -anchor n -pady 2 -side top
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  frame $wc1.line$IQ -bg $BGMAG
  label $wc1.line$IQ.labs -text "site  $IQ    " -anchor w  -padx 2 -bg $BGMAG -width 7
  label $wc1.line$IQ.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.entx -textvariable  VAR(MDIR-x$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
  label $wc1.line$IQ.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.enty -textvariable  VAR(MDIR-y$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
  label $wc1.line$IQ.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
  entry $wc1.line$IQ.entz -textvariable  VAR(MDIR-z$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line$IQ.labs $wc1.line$IQ.labx $wc1.line$IQ.entx \
     $wc1.line$IQ.laby $wc1.line$IQ.enty $wc1.line$IQ.labz $wc1.line$IQ.entz \
     -side left  -fill x
pack $wc1.line$IQ  -fill x -padx 30
set VAR(MDIR-x$IQ) 0
set VAR(MDIR-y$IQ) 0
set VAR(MDIR-z$IQ) 1
}
#
#------------------------------------------------------------------------ KMROT = 3 orthogonal spin-spiral
} elseif { $VAR(KMROT) == "3" } {
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "orthogonal spin-spiral   (Theta=90)"         -padx 2 -bg $BGMAG
label $wc1.headtxt2  -text "spin spiral vector   QMVEC  (Cart. units)"  -padx 2 -bg $BGMAG
pack $wc1.headtxt $wc1.headtxt2 -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(QMVEC-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(QMVEC-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(QMVEC-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x   -padx 30 -pady 5
set VAR(QMVEC-x) 0
set VAR(QMVEC-y) 0
set VAR(QMVEC-z) 1
#
#------------------------------------------------------------------------ KMROT = 4 general spin-spiral
} elseif { $VAR(KMROT) == "4" } {
pack $wc2
$wc2 configure -bg $BGMAG
$wc1 configure -bg $BGMAG
label $wc1.headtxt  -text "spin-spiral with arbitrary tilt angle Theta"         -padx 30 -bg $BGMAG
label $wc1.headtxt2  -text "spin spiral vector   QMVEC  (Cart. units)"  -padx 2 -bg $BGMAG
pack $wc1.headtxt $wc1.headtxt2 -fill x -anchor n -pady 2 -side top
frame $wc1.line -bg $BGMAG
label $wc1.line.labx -text "   x" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entx -textvariable  VAR(QMVEC-x)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.laby -text "   y" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.enty -textvariable  VAR(QMVEC-y)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.line.labz -text "   z" -anchor w  -padx 2 -bg $BGMAG
entry $wc1.line.entz -textvariable  VAR(QMVEC-z)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc1.line.labx $wc1.line.entx $wc1.line.laby $wc1.line.enty $wc1.line.labz $wc1.line.entz \
-side left  -fill x
pack $wc1.line  -fill x  -padx 30
set VAR(QMVEC-x) 0
set VAR(QMVEC-y) 0
set VAR(QMVEC-z) 1

label $wc2.headtet  -text "site-dependent tilt angle Theta (deg.)" -padx 30 -bg $BGMAG
pack $wc2.headtet -fill x -anchor n -pady 2 -side top
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  frame $wc2.line$IQ -bg $BGMAG
  label $wc2.line$IQ.labx -text "         site  $IQ   " -width 15 -anchor w  -padx 2 -bg $BGMAG
  entry $wc2.line$IQ.entx -textvariable  VAR(QMTET$IQ)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack $wc2.line$IQ.labx $wc2.line$IQ.entx -side left  -fill x
pack $wc2.line$IQ  -fill x
set VAR(QMTET$IQ) 90
} ; # IQ



} ; # if-else----

} ; # KEY == KMROT
#                             supply additional information for KMROT
#--------------------------------------------------------------------------------
#



if {$KEY == "TASK" } {


set TASK $VAR(TASK)

regsub "$syssuffix\$" $sysfile "" inpfile
set inpfile "$inpfile\_$VAR(TASK)$inpsuffix"
set VAR(ADSI)    "$TASK"

$Wrbcmd.header configure -text "parameters for TASK   $TASK "

#----------------------------------------------- disable OWRPOT
set VAR(OWRPOT) 0
widget_disable   $create_inpfile_SPRKKR_wc3.wVAROWRPOT.2

#----------------------------------------------- set default for energy scale
$create_inpfile_SPRKKR_wc1.wVARNE.2 configure -from 0 -to 600 -digits 3 -resolution 1

#
#---------------------------------------------------------------- DOS
#
if {$TASK == "DOS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)       50    ;    set VAR(ImE)       0.01
    destroy $Wrbcmd
    widget_enable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
#
#---------------------------------------------------------------- EKREL
#
} elseif {$TASK == "EKREL" } {
#    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
#    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
#    set VAR(NE)     1000    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2


label $wc1.energy    -text "energy range" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.energy   -in $wc1  -anchor nw -side top -fill x
CreateSCALE  $wc1 $BGTSK $wl "  E_min" $ws -4.0 4.0  0 2 0.1  VAR EMINDISP $fe
CreateSCALE  $wc1 $BGTSK $wl "  E_max" $ws -4.0 4.0  0 2 0.1  VAR EMAXDISP $fe
CreateSCALE  $wc1 $BGTSK $wl "  NE   " $ws    1 2000 0 4 20   VAR NEDISP   $fe
label $wc1.kpoints   -text "number of k-points" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.kpoints  -in $wc1  -anchor nw -side top -fill x
CreateSCALE  $wc1 $BGTSK $wl "  NK   " $ws    1 2000 0 4 20   VAR NKDISP   $fe
label $wc1.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.ekrel_bot   -in $wc1  -anchor nw -side top -fill both -expand y

label $wc2.ekrel    -text "path in k-space" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrel   -in $wc2  -anchor nw -side top -fill x

for {set IKPATH 1} {$IKPATH <= $KPATHTOP} {incr IKPATH} {
    CreateRADIOBUTTON $wc2 $BGTSK $wl "KPATH   $IKPATH" \
                VAR KPATH $IKPATH  $fe n $IKPATH "$KPLAB($IKPATH)  "
}


##label $wc2.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
##pack configure $wc2.ekrel_bot   -in $wc2  -anchor nw -side top -fill both -expand y

CreateRADIOBUTTON $wc2 $BGTSK $wl "     "  VAR KPATH  user  $fe n -1  "user defined   (set below)"
label $wc2.ekrela    -text "start vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrela   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  k_x  " $ws -2.0 2.0  0 2 0.1  VAR KA1x $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_y  " $ws -2.0 2.0  0 2 0.1  VAR KA1y $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_z  " $ws -2.0 2.0  0 2 0.1  VAR KA1z $fe
label $wc2.ekrele    -text "end vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrele   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  k_x  " $ws -2.0 2.0  0 2 0.1  VAR KE1x $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_y  " $ws -2.0 2.0  0 2 0.1  VAR KE1y $fe
CreateSCALE  $wc2 $BGTSK $wl "  k_z  " $ws -2.0 2.0  0 2 0.1  VAR KE1z $fe
label $wc2.ekrel_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.ekrel_bot   -in $wc2  -anchor nw -side top -fill both -expand y

#
#---------------------------------------------------------------- BLOCHSF
#
} elseif {$TASK == "BLOCHSF" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      200    ;    set VAR(ImE)       0.0
    if {$NCPA == 0} {set VAR(ImE) 0.001}
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
#----------------------------------------------- set default for energy scale
    $create_inpfile_SPRKKR_wc1.wVARNE.2 configure -from 1 -to 2001 -digits 4 -resolution 20
#
#------------------------------------ column 1
#
label $wc1.mode    -text "E-k-plot for BSF A(E,k)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.mode   -in $wc1  -anchor nw -side top -fill x
CreateRADIOBUTTON $wc1 $BGTSK $wl "     "  VAR MODEBSF  a  $fe n 1  "select"

#set wradbut1 $wc1.wVARMODEBSFa.1
#set wradbut2 $wc1.wVARMODEBSFb.2

#$wradbut1 configure -command  exec_RADIOBUTTON_cmd_MODEBSF
#$wradbut2 configure -command  exec_RADIOBUTTON_cmd_MODEBSF

#global exec_RADIOBUTTON_cmd_wc1
#set exec_RADIOBUTTON_cmd_wc1 $wc1

#proc exec_RADIOBUTTON_cmd_MODEBSF {} {
#global VAR exec_RADIOBUTTON_cmd_wc1
#set wc1 $exec_RADIOBUTTON_cmd_wc1
#set MODE $VAR(MODEBSF)
#if {[winfo exists $wc1.bsf_bot]} {
#   if {$MODE==1} {
#      $wc1.energy configure -text "energy range"
#   } else {
#      $wc1.energy configure -text "fixed energy"
#   }
#}
#}

label $wc1.space    -text " " -anchor w  -padx 2 -bg $BGTSK -height 5
pack configure $wc1.space    -in $wc1  -anchor nw -side top -fill x

label $wc1.energy    -text "energy range" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.energy   -in $wc1  -anchor nw -side top -fill x
CreateENTRY    $wc1 $BGTSK $wl "   EMIN "     $wee VAR EMIN   $fe 3 "Ry"
CreateENTRY    $wc1 $BGTSK $wl "   EMAX "     $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY    $wc1 $BGTSK $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"
CreateSCALE    $wc1 $BGTSK $wl "   NE   " $ws    1 2001 0 4 20   VAR NE   $fe

label $wc1.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.bsf_bot   -in $wc1  -anchor nw -side top -fill both -expand y

#
#------------------------------------ column 2
#
label $wc2.head   -text "k-parameters for E-k-plot" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.head     -in $wc2  -anchor nw -side top -fill x
label $wc2.kpoints   -text "number of k-points" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.kpoints  -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "  NK   " $ws    1 2000 0 4 20   VAR NKBSF   $fe
label $wc2.bsf    -text "path in k-space" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsf   -in $wc2  -anchor nw -side top -fill x

for {set IKPATH 1} {$IKPATH <= $KPATHTOP} {incr IKPATH} {
    CreateRADIOBUTTON $wc2 $BGTSK $wl "KPATH   $IKPATH" \
                VAR KPATH $IKPATH  $fe n $IKPATH "$KPLAB($IKPATH)  "
}

CreateRADIOBUTTON $wc2 $BGTSK $wl "     "  VAR KPATH  user  $fe n -1  "user defined   (below)"
label $wc2.bsfa    -text "start vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsfa   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "   k_x  " $ws -2.0 2.0  0 2 0.1  VAR KA1x $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_y  " $ws -2.0 2.0  0 2 0.1  VAR KA1y $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_z  " $ws -2.0 2.0  0 2 0.1  VAR KA1z $fe
label $wc2.bsfe    -text "end vector (2 pi/a)" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsfe   -in $wc2  -anchor nw -side top -fill x
CreateSCALE  $wc2 $BGTSK $wl "   k_x  " $ws -2.0 2.0  0 2 0.1  VAR KE1x $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_y  " $ws -2.0 2.0  0 2 0.1  VAR KE1y $fe
CreateSCALE  $wc2 $BGTSK $wl "   k_z  " $ws -2.0 2.0  0 2 0.1  VAR KE1z $fe
label $wc2.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.bsf_bot   -in $wc2  -anchor nw -side top -fill both -expand y


#
#------------------------------------ column 3
#
label $wc3.mode-k-k    -text "k-k-plot for BSF A(E,k)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.mode-k-k   -in $wc3  -anchor nw -side top -fill x
CreateRADIOBUTTON $wc3 $BGK $wl "     "  VAR MODEBSF  b  $fe n 2  "select"

label $wc3.bsf_fill    -text " " -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsf_fill   -in $wc3  -anchor nw -side top -fill both -expand y


label $wc3.energy-k-k    -text "fixed energy" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.energy-k-k   -in $wc3  -anchor nw -side top -fill x
CreateENTRY    $wc3 $BGK  $wl "   E_fix "    $wee VAR EMAX   $fe 3 "Ry"
CreateENTRY    $wc3 $BGK  $wl "   Im (E) "   $wee VAR ImE    $fe 3 "Ry"

#
###label $wc3.head   -text "k-parameters for k-k-plot (constant E)" -anchor w  -padx 2 -bg $BGK
###pack configure $wc3.head     -in $wc3  -anchor nw -side top -fill x
#
label $wc3.bsfa    -text "spanning vector k1 (2 pi/a)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsfa   -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "   k1_x  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1x $fe
CreateSCALE  $wc3 $BGK $wl "   k1_y  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1y $fe
CreateSCALE  $wc3 $BGK $wl "   k1_z  " $ws -2.0 2.0  0 2 0.1  VAR KBSF1z $fe
#
label $wc3.lnk1 -text "number of k-points along vector k1" -anchor w -padx 2 -bg $BGK
pack configure $wc3.lnk1  -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "  NK1  " $ws    1 2000 0 4 20   VAR NK1BSF   $fe
#
label $wc3.bsfe    -text "spanning vector k2 (2 pi/a)" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsfe   -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "   k2_x  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2x $fe
CreateSCALE  $wc3 $BGK $wl "   k2_y  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2y $fe
CreateSCALE  $wc3 $BGK $wl "   k2_z  " $ws -2.0 2.0  0 2 0.1  VAR KBSF2z $fe
#
label $wc3.lnk2 -text "number of k-points along vector k2" -anchor w -padx 2 -bg $BGK
pack configure $wc3.lnk2  -in $wc3  -anchor nw -side top -fill x
CreateSCALE  $wc3 $BGK $wl "  NK2  " $ws    1 2000 0 4 20   VAR NK2BSF   $fe
#
label $wc3.bsf_bot    -text "" -anchor w  -padx 2 -bg $BGK
pack configure $wc3.bsf_bot   -in $wc3  -anchor nw -side top -fill both -expand y

#
#---------------------------------------------------------------- WFPLOT
#
} elseif {$TASK == "WFPLOT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.5   
    set VAR(GRID)      3    ;    set VAR(EMAX)      0.5 
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl " core / VB"    VAR CORE_VB    a     $fe s 1  CORE VB       
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateRADIOBUTTON $wc1 $BGMAG $wl "   VB   l "    VAR VB_l       a     $fe s 1  s p d f

CreateTEXT        $wc1 $BGX info \
"�
 plot electronic wave functions for core or band states  

     core states
     - specify core-level 

     band states 
     - select l-character                       
     - set energy via standard E-mask 

     DEFAULT: mj=+1/2
     you can modify mj in the input file
"
#
#
#---------------------------------------------------------------- PLOT1D
#
} elseif {$TASK == "PLOT1D" } {

frame $wc1.toprow  -bg $BGTSK
pack configure $wc1.toprow -in $wc1 -anchor w -side top -fill x -expand y
frame $wc1.toprow.a  -bg $BGTSK
frame $wc1.toprow.b  -bg $BGTSK
pack  $wc1.toprow.a  $wc1.toprow.b -side left -expand y -fill x
CreateCHECKBUTTON $wc1.toprow.a $BGTSK  $wl      "   plot 1D" VAR plot1D-POT "potential"
CreateCHECKBUTTON $wc1.toprow.a $BGTSK  $wl      " "          VAR plot1D-RHO "densities"
CreateCHECKBUTTON $wc1.toprow.a $BGTSK  $wl      " "          VAR plot1D-SFN "shape function"


$wc1 configure -bg $BGTSK

CreateTEXT        $wc1 $BGX info \
"�
 create 1D plots for the potential, densities and shape functions

     ASA:      plot spherical functions (no shape functions)

     FULLPOT:  plot all non-zero (l,m)-contributions

"
#
#
#---------------------------------------------------------------- PLOT2D
#
} elseif {$TASK == "PLOT2D" } {

frame $wc1.toprow
pack configure $wc1.toprow -in $wc1 -anchor w -side top -fill x -expand y
frame $wc1.toprow.a  -bg $BGTSK
frame $wc1.toprow.b  -bg $BGTSK
pack  $wc1.toprow.a  $wc1.toprow.b -side left -expand y -fill x
CreateCHECKBUTTON $wc1.toprow.a $BGTSK  $wl      "   plot 2D" VAR plot2D-POT "potential"
CreateCHECKBUTTON $wc1.toprow.b $BGTSK  $wl      " "          VAR plot2D-RHO "densities"


label $wc1.headtxt  -text "   plot window"   -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x



frame $wc1.lineA -bg $BGTSK
label $wc1.lineA.labx -text "   vec A:   x" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineA.entx -textvariable  VAR(plot2D-AVECx)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineA.laby -text "   y" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineA.enty -textvariable  VAR(plot2D-AVECy)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineA.labz -text "   z" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineA.entz -textvariable  VAR(plot2D-AVECz)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack  $wc1.lineA   -anchor w
pack $wc1.lineA.labx $wc1.lineA.entx $wc1.lineA.laby $wc1.lineA.enty $wc1.lineA.labz $wc1.lineA.entz -side left
CreateSCALE  $wc1.lineA $BGTSK   $wl "   NA  "      $ws 0        250   0 3 1        VAR plot2D-NA  $fe

frame $wc1.lineB -bg $BGTSK
label $wc1.lineB.labx -text "   vec B:   x" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineB.entx -textvariable  VAR(plot2D-BVECx)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineB.laby -text "   y" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineB.enty -textvariable  VAR(plot2D-BVECy)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineB.labz -text "   z" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineB.entz -textvariable  VAR(plot2D-BVECz)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack  $wc1.lineB   -anchor w
pack $wc1.lineB.labx $wc1.lineB.entx $wc1.lineB.laby $wc1.lineB.enty $wc1.lineB.labz $wc1.lineB.entz -side left
CreateSCALE  $wc1.lineB $BGTSK   $wl "   NB  "      $ws 0        250   0 3 1        VAR plot2D-NB  $fe

frame $wc1.lineC -bg $BGTSK -height 6
label $wc1.lineC.labx -text "   vec C:   x" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineC.entx -textvariable  VAR(plot2D-CVECx)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineC.laby -text "   y" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineC.enty -textvariable  VAR(plot2D-CVECy)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label $wc1.lineC.labz -text "   z" -anchor w  -padx 2 -bg $BGTSK
entry $wc1.lineC.entz -textvariable  VAR(plot2D-CVECz)  -width {8} -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
pack  $wc1.lineC   -anchor w
pack $wc1.lineC.labx $wc1.lineC.entx $wc1.lineC.laby $wc1.lineC.enty $wc1.lineC.labz $wc1.lineC.entz -side left 

$wc1 configure -bg $BGTSK

CreateTEXT        $wc1 $BGX info \
"�
 create a 2D-plot for the electronic potential and/or densities

     within the window spanned by the vectors A and B
       
     for a grid specified by NA and NB

     the vector C shifts the plot window away from the origin

     cartesian coordinates in units of lattice parameter  A
"
#
#
#---------------------------------------------------------------- PSHIFT
#
} elseif {$TASK == "PSHIFT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0001
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      100    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#---------------------------------------------------------------- SOCPAR
#
} elseif {$TASK == "SOCPAR" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0001
    set VAR(GRID)      3    ;    set VAR(EMAX)      1.0
    set VAR(NE)      100    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#---------------------------------------------------------------- JXC
#
} elseif {$TASK == "JXC"   } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

CreateSCALE       $wc1 $BGTSK $wl "   CLURAD"  200  0         20   0 3 0.1  VAR    CLURAD_JXC  $fe

CreateTEXT        $wc1 $BGX info \
"�
 calculation of the exchange coupling parameter J_ij     

     - specify the radius CLURAD            
       all atoms j within the sphere of radius CLURAD
       around atom i are considered            
"
#
#---------------------------------------------------------------- FMAG
#
} elseif {$TASK == "FMAG"   } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

    destroy $Wrbcmd ; return
#
#
#---------------------------------------------------------------- MEPLOT
#
} elseif {$TASK == "MEPLOT" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0   
    set VAR(GRID)      3    ;    set VAR(EMAX)      4   
    set VAR(NE)        100  ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "transition"   VAR transition    a     $fe s 1  core-band band-band 
CreateRADIOBUTTON $wc1 $BGTSK $wl "paramagnetic" VAR MEPLOT_NONMAG a     $fe s 1  YES NO
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "   VAR NCXRAY        aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "   VAR LCXRAY        aexec $fe s 1  s p d f
CreateRADIOBUTTON $wc1 $BGMAG $wl "   VB   l "   VAR MEPLOT_l      a     $fe s 1  all s p d f
CreateTEXT        $wc1 $BGE phot  "photon energy range"
CreateSCALE       $wc1 $BGE   $wl "   NE  "      $ws 1        250   0 3 1        VAR NEPHOT  $fe
CreateRADIOBUTTON $wc1 $BGE   $wl "   fixed  "   VAR EPHOT-fixed   aexec $fe s 1  {He I} {He II} {Al Ka} {Mg Ka}
CreateENTRY       $wc1 $BGE   $wl "   EMIN "     $wee VAR EPHOTMIN       $fe 3 "eV"
CreateENTRY       $wc1 $BGE   $wl "   EMAX "     $wee VAR EPHOTMAX       $fe 3 "eV"

CreateTEXT        $wc1 $BGX info \
"�
 plot E-dependence of dipole transition matrix elements  

     core-band
     - specify core-level 
     - specify VB E-range via standard E-mask �

     band-band for fixed photon energy
     - set initial state E-range via standard E-mask
     - specify photon energy EPHOT with NEPHOT = 1�
     - eventually restrict l for initial state�

     band-band for variable photon energy
     - set single initial state E via standard E-mask
     - specify photon energy range
     - eventually restrict l for initial state

     PARAMAGNETIC = YES:
     the exchange splitting will be suppressed
     to avoid states with mixed j-character
"
#
#
#---------------------------------------------------------------- MECHECK
#
} elseif {$TASK == "MECHECK" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      0.0   
    set VAR(GRID)      3    ;    set VAR(EMAX)      4   
    set VAR(NE)        100  ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl " select IT" $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "transition"  VAR transition  a     $fe s 1  core-band band-band 
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core n "  VAR NCXRAY      aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGCPA $wl "   core l "  VAR LCXRAY      aexec $fe s 1  s p d f
CreateTEXT        $wc1 $BGE wERYDI  "initial band state energy "
CreateENTRY       $wc1 $BGE   $wl "   Re E(ini) "     $wee VAR ReERYDI  $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im E(ini) "     $wee VAR ImERYDI  $fe 3 "Ry"
CreateTEXT        $wc1 $BGE wERYDF  "final band state energy "
CreateENTRY       $wc1 $BGE   $wl "   Re E(fin) "     $wee VAR ReERYDF  $fe 3 "Ry"
CreateENTRY       $wc1 $BGE   $wl "   Im E(fin) "     $wee VAR ImERYDF  $fe 3 "Ry"

CreateTEXT        $wc1 $BGX info \
"�
 check the dipole transition matrix elements  

     core-band
     - specify core-level 
     - set final band state energy 

     band-band
     - set initial band state energy
     - set final band state energy 

"
#
#
#---------------------------------------------------------------- VBXPS
#
} elseif {$TASK == "VBXPS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    set VAR(EPHOT)  1253.6
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

CreateENTRY       $wc1 $BGTSK $wl "   EPHOT"     $wee VAR EPHOT     $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   USETAUNN"       VAR USETAUNN  a $fe n 0  NO YES

CreateTEXT        $wc1 $BGX info \
"�
 calculate valence band photo emission spectra 

     - specify photon energy

"

#ME  NAB
#MODE
#USETAUNN off
#
#---------------------------------------------------------------- BIS
#
} elseif {$TASK == "BIS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      [expr $VAR(EF) + 4]
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    set VAR(EPHOT)  1253.6
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

CreateENTRY       $wc1 $BGTSK   $wl "   EPHOT"     $wee VAR EPHOT  $fe 3 "eV"
#
#---------------------------------------------------------------- CLXPS
#
} elseif {$TASK == "CLXPS" } {
     set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
     set VAR(GRID)      3    ;    set VAR(EMAX)      ""
     set VAR(NE)        1    ;    set VAR(ImE)       0.01
     set VAR(EPHOT)  1253.6
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateENTRY       $wc1 $BGTSK $wl "   EPHOT"     $wee VAR EPHOT  $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   USETSS"     VAR USETSS  a $fe n 1  YES NO


#USETSS
#ME
#ANGRES
#ANGQ1
#ANGE1
#ANGK1
#ANGQ[N]
#ANGE[N]
#ANGK[N]
#
#---------------------------------------------------------------- COMPTON
#
} elseif {$TASK == "COMPTON" } {
     set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      "-0.2"
     set VAR(GRID)      5    ;    set VAR(EMAX)      ""
     set VAR(NE)       30    ;    set VAR(ImE)       ""
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

label $wc1.lnk1 -text "   PNVEC  normal vector for Compton profile" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk1  -in $wc1  -anchor nw -side top -fill x
CreateENTRY       $wc1 $BGTSK $wl "   x    "     $wee VAR COMPTON-PNVECx  $fe 3 "  "
CreateENTRY       $wc1 $BGTSK $wl "   y    "     $wee VAR COMPTON-PNVECy  $fe 3 "  "
CreateENTRY       $wc1 $BGTSK $wl "   z    "     $wee VAR COMPTON-PNVECz  $fe 3 "  "
label $wc1.lnk2 -text "   parameters for grid along PNVEC" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk2  -in $wc1  -anchor nw -side top -fill x
CreateSCALE       $wc1 $BGTSK $wl "   NPN    " $ws 1  200  0   3   1  VAR COMPTON-NPN   $fe
CreateSCALE       $wc1 $BGTSK $wl "   PNMAX "  $ws 0   40  0   3 0.1  VAR COMPTON-PNMAX $fe
label $wc1.lnk3 -text "   parameters for grid perpendicular to PNVEC" -anchor w -padx 2 -bg $BGTSK
pack  $wc1.lnk3  -in $wc1  -anchor nw -side top -fill x
CreateSCALE       $wc1 $BGTSK $wl "   NPP    " $ws 1  200  0   3   1  VAR COMPTON-NPP   $fe
CreateSCALE       $wc1 $BGTSK $wl "   PPMAX "  $ws 0   40  0   3 0.1  VAR COMPTON-PPMAX $fe

#
#---------------------------------------------------------------- AES
#
} elseif {$TASK == "AES" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
CreateSCALE       $wc1 $BGTSK $wl "   NEME   " $ws 2 30    0      2 1  VAR AESNEME $fe
#PRINTME
#
#---------------------------------------------------------------- NRAES
#
} elseif {$TASK == "NRAES" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      -0.2
    set VAR(GRID)      3    ;    set VAR(EMAX)      [read_keyed_value $VAR(POTFIL) "EF"]
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
#CALCLS
#OUTGNU
#
#---------------------------------------------------------------- APS
#
} elseif {$TASK == "APS" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      [read_keyed_value $VAR(POTFIL) "EF"]
    set VAR(GRID)      3
    set VAR(NE)      150    ;    set VAR(ImE)       0.01
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
#CALCLS
#OUTGNU
#BRDDOS
#GAMMAV
#
#---------------------------------------------------------------- XAS / XMO / XES / XRS
#
} elseif {$TASK == "XAS" || $TASK == "XMO"|| $TASK == "XES" || $TASK == "XRS"  } {
    set VAR(SEARCHEF)  0    ;    set VAR(ImE)       0.01
    set VAR(NE)      180    ;    set VAR(GRID)  3
    if {$TASK == "XES" } {
       set VAR(EMIN) -0.2
       set VAR(EMAX)  ""
    } else {
       set VAR(EMIN)  ""
       set VAR(EMAX)  4.0
    }
    if {$TASK == "XAS" } {set VAR(GRID)  6}
    if {$TASK == "XMO"|| $TASK == "XRS"} {set VAR(GRID)  7}
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
## CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "      VAR XRAYME     a $fe s 1 ADA NAB GRV
if {$TASK == "XMO"|| $TASK == "XRS"} {
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(-)"   $wee VAR TAUJMIN $fe 3 "eV"
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(+)"   $wee VAR TAUJPLS $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   TSELECT"  VAR TSELECT    a $fe n 0 0 1 2
CreateSCALE       $wc1 $BGTSK $wl "   NE3 "       $ws 1 400  0 3 1    VAR XMONE3    $fe
CreateSCALE       $wc1 $BGTSK $wl "   EMAX3"      $ws 0  50  0 3 0.1  VAR XMOEMAX3  $fe
}

#MECHECK
#OUTPUT
#
#---------------------------------------------------------------- CHI
#
} elseif {$TASK == "CHI" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      "-0.2"
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       50    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

set wcl(1) $wc1 ; set TXTL(1) "s"
set wcl(2) $wc2 ; set TXTL(2) "p"
set wcl(3) $wc3 ; set TXTL(3) "d"
set wcl(4) $wc4 ; set TXTL(4) "f"
####set wcl(5) $wc5 ; set TXTL(5) "g"

###label $wcl(1).dosef    -text "density of states at the Fermi level" -anchor w  -padx 2 -bg $BGTSK
###pack configure $wcl(1).dosef   -in $wcl(1) -anchor nw -side top -fill x
for {set IL 1} {$IL <= $NL} {incr IL} {
   label $wcl($IL).dosef    -text "density of states at the Fermi level" -anchor w  -padx 2 -bg $BGTSK
   pack configure $wcl($IL).dosef   -in $wcl($IL) -anchor nw -side top -fill x
}

for {set IT 1} {$IT <= $NT} {incr IT} {
  for {set IL 1} {$IL <= $NL} {incr IL} {
     set INDNTL "nef$IT{_}$IL"
     set VAR($INDNTL) 0.0
     CreateSCALE  $wcl($IL) $BGTSK $wl "   $TXTT($IT)   $TXTL($IL)" $ws 0.0 40.0 0 4 0.01  VAR $INDNTL $fe
  }
}

CreateSCALE       $wc1 $BGTSK $wl "   PRINT  " $ws 0 5  0  1  1 VAR CHIPRINT  $fe

#
#---------------------------------------------------------------- T1
#
} elseif {$TASK == "T1" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
#
#
#---------------------------------------------------------------- SIGMA or GILBERT
#
} elseif {$TASK == "GILBERT" || $TASK == "SIGMA"  } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      ""
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2

frame $wc1.toprow  -bg $BGTSK
pack configure $wc1.toprow -in $wc1 -anchor w -side top -fill x -expand y
frame $wc1.toprow.a  -bg $BGTSK
frame $wc1.toprow.b  -bg $BGTSK
pack  $wc1.toprow.a  $wc1.toprow.b -side left -expand y -fill x
CreateSCALE       $wc1.toprow.a $BGTSK   $wl "   NTMP   "     $ws 0        250   0 3 1        VAR NTMP  $fe
CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TMPMIN "     $wee VAR TMPMIN       $fe 3 "K"
CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TMPMAX "     $wee VAR TMPMAX       $fe 3 "K"
CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TDEBYE"      $wee VAR TDEBYE     $fe 3 "K"

$wc1 configure -bg $BGTSK

CreateTEXT        $wc1 $BGX info \
"�
 perform calculations for finite temperature for NTMP != 0

 in the energy window  TMIN - TMAX  with  NTMP  steps

 the root mean square displacement is estimated 
 from the Debye temperature  TDEBYE

"

#
#
#---------------------------------------------------------------- KKRSPEC
#                                                                 ARPES
#                                                                 LEED
#
} elseif {$TASK == "ARPES" || $TASK == "LEED"
       || $TASK == "AIPES" || $TASK == "BAND"} {


   create_inpfile_SPR-KKR_KKRSPEC  $TASK $wc1 $wl $ws $wee $fe 


#
#
#---------------------------------------------------------------- RHO
#
} elseif {$TASK == "RHO" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)      $VAR(EF)
    set VAR(GRID)      3    ;    set VAR(EMAX)      ""
    set VAR(NE)        1    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "    VAR RHOME     a $fe s 1 ADA NAB GRV
#
#
#---------------------------------------------------------------- SCF
#
} elseif {$VAR(TASK) == "SCF" } {
    set VAR(SEARCHEF)  0    ;    set VAR(EMIN)     -0.2
    set VAR(GRID)      5    ;    set VAR(EMAX)      ""
    set VAR(NE)       30    ;    set VAR(ImE)       0.0
    widget_disable  $create_inpfile_SPRKKR_wc1.wVARSEARCHEF.2
    widget_enable   $create_inpfile_SPRKKR_wc3.wVAROWRPOT.2

    if { $SPRKKR_EXPERT == 0 } { destroy $Wrbcmd ; return }

set col_width 40

#
#------------------------------------------------- column 1   SCF parameters
#
label $wc1.headtxt  -text "   SCF parameters"   -anchor w  -padx 2 -bg $BGTSK -width $col_width 
pack configure $wc1.headtxt -in $wc1 -anchor nw -side top -fill x
CreateRADIOBUTTON $wc1 $BGTSK $wl "   SCFVXC "    VAR SCFVXC     a $fe s 1  VWN MJW VBH PBE
CreateRADIOBUTTON $wc1 $BGTSK $wl "   SCFALG "    VAR SCFALG     a $fe s 1  TCHEBY BROYDEN2
CreateSCALE       $wc1 $BGTSK $wl "   SCFNITER " $ws 0        300   0 3 10       VAR SCFNITER  $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFTOL "   $ws 0.000001 0.001 0 4 0.000001 VAR SCFTOL    $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFMIX "   $ws 0.001    0.80  0 3 0.001    VAR SCFMIX    $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFMIXOP " $ws 0.001    0.80  0 3 0.001    VAR SCFMIXOP  $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFISTBRY" $ws 1         20   0 2 1        VAR SCFISTBRY $fe
CreateSCALE       $wc1 $BGTSK $wl "   SCFITDEPT" $ws 1         80   0 2 1        VAR SCFITDEPT $fe
set SIM 0
for {set IT 1} {$IT <= $NT} {incr IT} { if {$CONC($IT)<0.999} {set SIM 1 } }
if {$SIM==1} {
CreateSCALE       $wc1 $BGTSK $wl "   SCFSIM"    $ws 0.00     0.80  0 3 0.001    VAR SCFSIM    $fe
}
#

#
#------------------------------------------------- column 2 + 3    SCF START
#
label $wc2.headtxt  -text "   setting up initial charge density"         -anchor w  -padx 2 -bg $BGTSK -width $col_width
label $wc2.qiontxt  -text "   QION   guess for ionicity"                 -anchor w  -padx 2 -bg $BGTSK
label $wc3.headtxt  -text "   setting up initial magnetisation density"  -anchor w  -padx 2 -bg $BGTSK -width $col_width
label $wc3.mspintxt -text "   MSPIN  guess for spin moment"              -anchor w  -padx 2 -bg $BGTSK
pack configure $wc2.headtxt $wc2.qiontxt  -in $wc2 -anchor nw -side top -fill x
pack configure $wc3.headtxt $wc3.mspintxt -in $wc3 -anchor nw -side top -fill x


frame $wc2.d -relief sunken -height 20
text  $wc2.d.tt -width 42  \
	-borderwidth 2 -relief raised -setgrid true \
 	-yscroll "$wc2.d.scroll set" -bg $BGTSK
scrollbar $wc2.d.scroll -command "$wc2.d.tt yview"

pack $wc2.d.scroll -side right  -fill y
pack $wc2.d.tt     -side left   -fill both 

###pack $wc2.d        -side bottom  
###for {set IT 1} {$IT <= 16} {incr IT} {
###CreateSCALE       $wc2.d.tt $BGTSK $wl "   $TXTT($IT)" $ws -2.0    +2.0   0 3 0.02  VAR QION$IT  $fe
###CreateSCALE       $wc3 $BGTSK $wl "   $TXTT($IT)" $ws -7.0    +7.0   0 3 0.05  VAR MSPIN$IT $fe
###}

if { $NT > 10 } {
   set MAX_IT 10
} else {
   set MAX_IT $NT
}


; #
for {set IT 1} {$IT <= $MAX_IT} {incr IT} {
CreateSCALE       $wc2 $BGTSK $wl "   $TXTT($IT)" $ws -2.0    +2.0   0 3 0.02  VAR QION$IT  $fe
CreateSCALE       $wc3 $BGTSK $wl "   $TXTT($IT)" $ws -7.0    +7.0   0 3 0.05  VAR MSPIN$IT $fe
}
set wlX2 [expr $wl*2]
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use QION (as set above)" VAR MATTHEISS a $fe s 1  "QION"
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use defaults           " VAR MATTHEISS c $fe s 0  "DEFAULT"
CreateRADIOBUTTON $wc2 $BGTSK $wlX2 "   use V(Mattheiss)       " VAR MATTHEISS b $fe s 2  "VMATT"

CreateCHECKBUTTON $wc2 $BGTSK $wl " " VAR NOSSITER    "NO rho(Mattheis) iteration"
CreateSCALE       $wc2 $BGTSK $wl "   QIONSCL "   $ws 0.001    0.80  0 2 0.01    VAR QIONSCL    $fe


CreateCHECKBUTTON $wc3 $BGTSK $wl " " VAR USEMSPIN    "use MSPIN  as set above  "
label $wc1.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
label $wc2.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
label $wc3.fill     -text " "   -anchor w  -padx 2 -bg $BGTSK
pack configure $wc1.fill     -in $wc1 -anchor nw -side top -fill both -expand y
pack configure $wc2.fill     -in $wc2 -anchor nw -side top -fill both -expand y
pack configure $wc3.fill     -in $wc3 -anchor nw -side top -fill both -expand y

if { $NT <= 1 } {pack forget $wc2}

#
#------------------------------------------------- column 4 NVALT
#
label $wc4.headtxt  -text "   valence band electrons"   -anchor w  -padx 2 -bg $BGTSK -width $col_width 
label $wc4.nvalttxt -text "   NVALT"              -anchor w  -padx 2 -bg $BGTSK
pack configure $wc4.headtxt $wc4.nvalttxt -in $wc4 -anchor nw -side top -fill x
set wlX2 [expr $wl/2]
set wsX2 [expr $ws/2]
for {set IT 1} {$IT <= $MAX_IT} {incr IT} {
CreateSCALE       $wc4 $BGTSK $wlX2 "   $TXTT($IT)" $wsX2 0 18   0 2 1  VAR NVAL$IT $fe
set VAR(NVAL$IT) $NVALT_DEFAULT($IT)
}
set wlX2 1
CreateRADIOBUTTON $wc4 $BGTSK $wlX2 " " VAR USENVALT a $fe n 0  DEFAULT
CreateRADIOBUTTON $wc4 $BGTSK $wlX2 " " VAR USENVALT b $fe n 1  "as set above"
label $wc4.fill     -text " "   -anchor w  -bg $BGTSK
pack configure $wc4.fill     -in $wc4 -anchor nw -side top -fill both -expand y
set VAR(USENVALT) 0

#
#------------------------------------------------- column 5 scale ALAT
#

CreateCHECKBUTTON $wc5 $BGTSK $col_width  "   scale the lattice paramter ALAT" VAR SCL_ALAT  " "
CreateSCALE       $wc5 $BGTSK $wl "   SCL1     " $ws 0.80     1.20  0 3 0.01     VAR SCL_ALAT-SCL1 $fe
CreateSCALE       $wc5 $BGTSK $wl "   SCL2     " $ws 0.80     1.20  0 3 0.01     VAR SCL_ALAT-SCL2 $fe
CreateSCALE       $wc5 $BGTSK $wl "   NSCL     " $ws 1         30   0 2 1        VAR SCL_ALAT-NSCL $fe

label $wc5.fill     -text " "   -anchor w  -bg $BGTSK
pack configure $wc5.fill     -in $wc5 -anchor nw -side top -fill both -expand y

}
#
}
#============================================================= end KEY
#NL
#Q1
#Q[NQ]
#SREL
#NREL
#OP
#SOC1
#SOC[NT]
#C1
#C[NT]
#BEXT
#NOBLC
#BREITINT T/F
#MALF
#MBET
#MGAM

#ETA
#RMAX
#QMAX
#
}
#                                             END exec_RADIOBUTTON_cmd_TASK  for SPR-KKR
########################################################################################

}
#                                                             END create_inpfile_SPR-KKR
########################################################################################



########################################################################################
#                                                                       write_inpfile
proc write_inpfile_SPR-KKR {Wcinp tuwas} {
 
    global structure_window_calls inpfile inpsuffix potfile potsuffix
    global sysfile syssuffix  PACKAGE      
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint Wcsys
    global system NQ NT ZT NL NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
    global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE

    global CLUSTER_TYPE DIMENSION
    global NQCLU NTCLU IQ_IQCLU N5VEC_IQCLU RQCLU
    global NOCC_IQCLU ZTCLU TXTTCLU CONCCLU ITCLU_OQCLU

    global Wprog inpsuffixlist  VAR VAR0 VARLST NCPA
    global editor edtext edxterm edoptions edsyntaxhelp edback
    global NM IMT IMQ RWSM QMTET QMPHI
    global buttonlist_PROGS
    global NCL NQCL IQECL ICLQ DIMENSION ZRANGE_TYPE NQ_bulk_R NQ_bulk_L
    global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
    global NVALT_DEFAULT TABNCORE
    global bulk_L_eq_bulk_R

    set tcl_precision 17
    set PI 3.141592653589793238462643

    set PRC write_inpfile_SPR-KKR

    set TASK $VAR(TASK)

#=======================================================================================
proc set_default {i value} {

 set PRC "set_default"

global VAR
   if {[info exists VAR($i)]} {
      if {[string trim $VAR($i)]==""} { set VAR($i) $value}
   } else {
      set VAR($i) $value
   }
}
#=======================================================================================

set file_name $inpfile
if {$tuwas == "append" && [file exists $file_name]==1} {
   set inp [open $file_name a]
} else {
   set inp [open $file_name w]
}

set_default EF 0.0

puts  $inp  "###############################################################################"
puts  $inp  "#  SPR-KKR input file    $inpfile "
set TXT     "#  created by xband on [standard_date]"
if { [string length $TXT] > 80 } {
puts  $inp  [string range $TXT 0 79]
} {
puts  $inp  $TXT
}
puts  $inp  "###############################################################################"
puts  $inp  " "
#=======================================================================================
#                                 SECTION CONTROL
#=======================================================================================
puts  $inp  "CONTROL  DATASET     = $VAR(DATASET) "
if { [string trim $VAR(ADSI)]!="" } {
puts  $inp  "         ADSI        = $VAR(ADSI) "
}
puts  $inp  "         POTFIL      = $VAR(POTFIL) "

if {$DIMENSION == "0D" } {
puts  $inp  "         POTFIL_CLU  = $VAR(POTFIL_CLU) "
puts  $inp  "         CLUTYPE     = $CLUSTER_TYPE "
} 

if {$VAR(ITEST)!="0"   } {set TXT  "TEST = $VAR(ITEST)" } else {set TXT  "" }
if {$VAR(TAUIO)!="AUTO"} {append TXT  "   $VAR(TAUIO)"}
puts  $inp  "         PRINT = $VAR(IPRINT)    $TXT"
if {$VAR(NOCORE) !="0"} {set    TXT  "NOCORE" } else {set TXT  "" }
if {$VAR(NOHFF)  !="0"} {append TXT  "   NOHFF"}
if {$VAR(FSOHFF) !="0"} {append TXT  "   FSOHFF"}
if {$VAR(EFG)    !="0"} {append TXT  "   EFG"}
if {$VAR(NONMAG)   !="0"} {append TXT  "   NONMAG"}
if {$TXT !=""} {puts  $inp  "         [string trim $TXT]"}
if {$VAR(NOWRDOS) !="0"} {set    TXT  "NOWRDOS" } else {set TXT  "" }
if {$VAR(WRLMLDOS)!="0"} {append TXT  "   WRLMLDOS"}
if {$VAR(WRKAPDOS)!="0"} {append TXT  "   WRKAPDOS"}
if {$VAR(WRPOLAR) !="0"} {append TXT  "   WRPOLAR"}
if {$VAR(WRCPA)   !="0"} {append TXT  "   WRCPA"}
if {$TXT !=""} {puts  $inp  "         [string trim $TXT]"}


puts  $inp  " "

#=======================================================================================
#                                 SECTION MODE
#=======================================================================================
set WR_MODE 0
#------------------------------------------------------------------- calculation mode
#
if {$VAR(HAMILTONIAN)!="REL"} {
set WR_MODE 1
set TXT "MODE   "
#------------------------------------------------------------------------ scale-c
if {$VAR(HAMILTONIAN) == "scale-c" } {
for {set IT 1} {$IT <= $NT} {incr IT} {
  if {$VAR(scale-c$IT) != "1.0"} {
    if {$VAR(scale-c$IT)<0.001} {set VAR(scale-c$IT) 0.001}
    set TXT1 "  C$IT = \{$VAR(scale-c$IT)\}"
    if { [expr [string length $TXT] + [string length $TXT1] ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $TXT1"
    } else {
	append TXT $TXT1
    }
  }
}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#
#------------------------------------------------------------------------ scale-SOC
} elseif { $VAR(HAMILTONIAN) == "scale-SOC" } {
for {set IT 1} {$IT <= $NT} {incr IT} {
  if {$VAR(scale-SOC$IT) != "1.0"} {
      if {$VAR(scale-SOC$IT)<0.000001} { set VAR(scale-SOC$IT) 0.000001}
    set TXT1 "  SOC$IT = \{$VAR(scale-SOC$IT)\}"
    if { [expr [string length $TXT] + [string length $TXT1] ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $TXT1"
    } else {
	append TXT $TXT1
    }
  }
}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#------------------------------------------------------------------------ use SOC-xy
} elseif { $VAR(HAMILTONIAN) == "SOC-xy"    } {
for {set IT 1} {$IT <= $NT} {incr IT} {
  if {$VAR(SOC-xy$IT) == 1} {
    set TXT1 "  SOC$IT = \{-2\}"
    if { [expr [string length $TXT] + [string length $TXT1] ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $TXT1"
    } else {
	append TXT $TXT1
    }
  }
}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#------------------------------------------------------------------------ use SOC-zz
} elseif { $VAR(HAMILTONIAN) == "SOC-zz"    } {
for {set IT 1} {$IT <= $NT} {incr IT} {
  if {$VAR(SOC-zz$IT) == 1} {
    set TXT1 "  SOC$IT = \{-1\}"
    if { [expr [string length $TXT] + [string length $TXT1] ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $TXT1"
    } else {
	append TXT $TXT1
    }
  }
}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#------------------------------------------------------------------------ Brooks-OP
} elseif { $VAR(HAMILTONIAN) == "Brooks-OP"    } {

puts  $inp  "$TXT  OP = BROOKS   "
set   TXT "         LOPT  = \{ "
for {set IT 1} {$IT <= $NT} {incr IT} {
    if { [expr [string length $TXT] + 2 ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $VAR(Brooks-OP-$IT) "
    } else {
	append TXT "$VAR(Brooks-OP-$IT) "
    }
}
puts  $inp  "$TXT\}"
#------------------------------------------------------------------------ LDA+U
} elseif { $VAR(HAMILTONIAN) == "LDA+U"    } {
puts  $inp  "$TXT  OP = LDA+U-$VAR(LDA+U-DC)   "
set   TXT "         LOPT  = \{ "
for {set IT 1} {$IT <= $NT} {incr IT} {
    if { [expr [string length $TXT] + 2 ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $VAR(LDA+U-$IT) "
    } else {
	append TXT "$VAR(LDA+U-$IT) "
    }
}
puts  $inp  "$TXT\}"

for {set IT 1} {$IT <= $NT} {incr IT} {
    if { $VAR(LDA+U-$IT) != "NO"  } {
       set TXT "         UEFF$IT = [format %4.2f $VAR(UHub-$IT)] eV "
       append TXT     "  JEFF$IT = [format %4.2f $VAR(JHub-$IT)] eV "
#       append TXT     "  EREF$IT = [format %4.2f $VAR(EHub-$IT)] Ry "
       puts  $inp  "$TXT"
    }
}

#############  write out of occupation numbers SUPPRESSED ############
####for {set IT 1} {$IT <= $NT} {incr IT} {
####    if { $VAR(LDA+U-$IT) != "NO"  } {
####       if {$VAR(LDA+U-$IT) =="d"} {
####          set LOP 2
####       } else {
####          set LOP 3
####       }
####       set TXT "         UHUB$IT = [format %4.2f $VAR(UHub-$IT)]  MLSOCC$IT = \{ "
####       for {set ml -$LOP} {$ml <= $LOP} {incr ml} {
####          append TXT  "[format %4.2f $VAR(LDA+U-$IT-$ml-DN)] "
####       }
####       puts  $inp  "$TXT"
####       set TXT "                                   "
####       for {set ml -$LOP} {$ml <= $LOP} {incr ml} {
####          append TXT  "[format %4.2f $VAR(LDA+U-$IT-$ml-UP)] "
####       }
####       puts  $inp  "$TXT\}"
####    }
####}
#############  write out of occupation numbers SUPPRESSED ############

#-----------------------------------------------------------------------------------
} else {
   puts  $inp  "MODE     $VAR(HAMILTONIAN) "
}
} ; # VAR(HAMILTONIAN)!="REL"
#-----------------------------------------------------------------------------------


#------------------------------------------------------------------- Lloyd formula
#
if {$VAR(LLOYD)!="0"} {
if {$WR_MODE == 0   } {
  puts  $inp  "MODE     LLOYD"
} else {
  puts  $inp  "         LLOYD"
}
set WR_MODE 1
}
#-----------------------------------------------------------------------------------


#------------------------------------------------------------------- calculation mode
#
if {$VAR(KMROT)!="0"} {
if {$WR_MODE == 0   } {
   set TXT "MODE   "
} else {
   set TXT "       "
}
set WR_MODE 1
#------------------------------------------------------------------------ KMROT = 1 global rotation
if {$VAR(KMROT) == "1" } {

   puts  $inp  "$TXT  MDIR = \{$VAR(MDIR-x), $VAR(MDIR-y), $VAR(MDIR-z)\}"
#
#------------------------------------------------------------------------ KMROT = 2 non-collinear spin configuration
} elseif { $VAR(KMROT) == "2" } {
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  if {$VAR(MDIR-x$IQ) != "0.0" || $VAR(MDIR-y$IQ) != "0.0" || $VAR(MDIR-z$IQ) != "1.0" } {
    set TXT1 "  MDIR$IQ = \{$VAR(MDIR-x$IQ), $VAR(MDIR-y$IQ),  $VAR(MDIR-z$IQ)\}"
    if { [expr [string length $TXT] + [string length $TXT1] ] > 80 } {
         puts  $inp  $TXT
         set TXT "       $TXT1"
    } else {
	append TXT $TXT1
    }
  }
}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#------------------------------------------------------------------------ KMROT = 3 orthogonal spin-spiral
} elseif { $VAR(KMROT) == "3"    } {

   puts  $inp  "$TXT  QMVEC = \{$VAR(QMVEC-x), $VAR(QMVEC-y), $VAR(QMVEC-z)\}"

#------------------------------------------------------------------------ KMROT = 4 general spin-spiral
} elseif { $VAR(KMROT) == "4"    } {

puts  $inp  "$TXT  QMVEC = \{$VAR(QMVEC-x), $VAR(QMVEC-y), $VAR(QMVEC-z)\}"

set TXT  "         QMTET = \{"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
    set TXT1 " $VAR(QMTET$IQ)"
    if { [expr [string length $TXT] + [string length $TXT1] + 2 ] > 80 } {
         puts  $inp  $TXT
         set TXT "                 "
    }
    if { $IQ < $NQ} {
        append TXT "$TXT1,"
    } else {
	append TXT "$TXT1\}"
    }
}

if {[string trim $TXT] !=""} {puts  $inp  $TXT}
#-----------------------------------------------------------------------------------
} else {
puts  $inp  "MODE     $VAR(KMROT) "
}
} ; # VAR(KMROT)!="0"
#-----------------------------------------------------------------------------------

if {$WR_MODE == 1 } {puts  $inp  " "}
#=======================================================================================



#=======================================================================================
#                                 SECTION SITES
#=======================================================================================
set WR_SITES 0
#
#------------------------------------------------------------------- NVALT
#
if {$VAR(USENVALT) == 1} {
set WR_SITES 1
set TXT "SITES    NVAL = \{"
for {set IT 1} {$IT <= $NT} {incr IT} {
    if { [expr [string length $TXT] + 5 ] > 80 } {
         puts  $inp  $TXT
         set TXT "                 "
    }
    if { $IT < $NT} {
        append TXT " $VAR(NVAL$IT),"
    } else {
	append TXT " $VAR(NVAL$IT)\}"
    }
}
if {[string trim $TXT]!=""} { puts  $inp  $TXT }
}

#
#------------------------------------------------------------------- LEXP
#
if {$VAR(NLUSER) != $NL } {
if {$WR_SITES==0} {
   puts  $inp "SITES    NL = \{ $VAR(NLUSER) \}"
} else {
   puts  $inp "         NL = \{ $VAR(NLUSER) \}"
}
set WR_SITES 1
}

if {$WR_SITES!=0} {puts  $inp " "}
#SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS




#=======================================================================================
set WR_STRCONST 1
set WR_TAU      1
set WR_ENERGY   1

if {$TASK == "SIGMA"    } {                                    set WR_ENERGY 0 }
if {$TASK == "EKREL"    } {                     set WR_TAU 0 ; set WR_ENERGY 0 }
if {$TASK == "WFPLOT"   } { set WR_STRCONST 0 ; set WR_TAU 0  }
if {$TASK == "PSHIFT"   } { set WR_STRCONST 0 ; set WR_TAU 0  }
if {$TASK == "SOCPAR"   } { set WR_STRCONST 0 ; set WR_TAU 0  }
if {$TASK == "MEPLOT"   } { set WR_STRCONST 0 ; set WR_TAU 0  }
if {$TASK == "CLXPS"    } {                                    set WR_ENERGY 0 }
#=======================================================================================


#=======================================================================================
#                                 SECTION STRCONST
#=======================================================================================
if {$WR_STRCONST == 1 && $VAR(STRETA)!=0.0} {
#
#------------------------------------------------------------------- structure constants
#
puts  $inp  "STRCONST ETA=$VAR(STRETA)   RMAX=$VAR(STRGMAX)   GMAX=$VAR(STRGMAX) "
puts  $inp  " "
}
#SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS




#=======================================================================================
#                                 SECTION CPA
#=======================================================================================
if {$NCPA !=0} {
#
#----------------------------------------------------------------------------------- CPA
#
set TXT ""
if {$VAR(CPANITER) == $VAR0(CPANITER) } {
  set TXT ""
} else {
  set TXT  "NITER=$VAR(CPANITER)"
}
if {$VAR(CPATOL)  != $VAR0(CPATOL)  } {append TXT "   TOL=$VAR(CPATOL)"}
if {$VAR(CPAMODE) != $VAR0(CPAMODE) } {
     append TXT "   NLCPA   CPALVL=$VAR(NLCPA-LVL)"
   if {$VAR(NLCPA-WRCFG) != $VAR0(NLCPA-WRCFG) } {append TXT "   WRCFG"}
}

if {$TXT != "" } {
   puts  $inp  "CPA      [string trim $TXT]"
   puts  $inp  " "
}

}
#CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC



#=======================================================================================
#                                 SECTION TAU
#=======================================================================================
#
if {$WR_TAU == 1} {
#
#---------------------------------------------------------------- TAU - calculation mode
#

if { $VAR(KKRMODE) == "CLUSTER" } {     
   set VAR(IBZINT)  "CLUSTER" 
}

set TXT "TAU      BZINT= $VAR(IBZINT)  "
if { $VAR(IBZINT) == "CLUSTER" } {
   set TXT "TAU      CLUSTER"
   if {$VAR(IQCNTR)>0} {
      append TXT "   IQCNTR=$VAR(IQCNTR)"
   } else {
      append TXT "   ITCNTR=$VAR(ITCNTR)"
   }
   if {$VAR(CLU-SPEC)=="NSH" } {
      append TXT "   NSHLCLU=$VAR(NSHLCLU)"
   } else {
      append TXT "   CLURAD=$VAR(CLURAD)"
   }
} elseif { $VAR(IBZINT) == "WEYL" } {
   append TXT "NKMIN= $VAR(NKMIN)   NKMAX= $VAR(NKMAX)"
} elseif { $VAR(IBZINT) == "POINTS" } {
   append TXT "NKTAB= $VAR(NKTAB)"
} elseif { $VAR(IBZINT) == "TET" } {
   append TXT "NF= $VAR($NF)"
} elseif { $VAR(IBZINT) == "ZOOM" } {
   append TXT "NF= $VAR($NF)"
} else {
   set TXT " "
}

if { $VAR(KKRMODE) == "TB" } {          
   puts  $inp  "$TXT "

   set TXT "         KKRMODE=TB"
   if {$VAR(CLU-SPEC)=="NSH" } {
      append TXT "   NSHLCLU=$VAR(NSHLCLU)"
   } else {
      append TXT "   CLURAD=$VAR(CLURAD)"
   }
}

puts  $inp  "$TXT "
puts  $inp  " "
}
#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT


#=======================================================================================
#                                 SECTION ENERGY
#=======================================================================================
#
if {$WR_ENERGY == 1} {
#
#--------------------------------------------------------------------------- energy mesh
#
# use split energy path for COMPTON
if {$TASK == "COMPTON" } {
puts  $inp  "ENERGY   GRID=\{5,3\}  NE=\{$VAR(NE),50\} "
} else {
puts  $inp  "ENERGY   GRID=\{$VAR(GRID)\}  NE=\{$VAR(NE)\} "
}
set TXT     "      "
if {[string trim $VAR(ImE) ] !=""} {append TXT  "   ImE=$VAR(ImE)"}
if {[string trim $TXT] !=""} {append TXT  " Ry"}

if {$TASK == "ARPES" || $TASK == "AIPES"
       || $TASK == "LEED"  || $TASK == "BAND"} {
if {[string trim $VAR(SPR-KKR-ENERG_EMIN_EV) ] == "9999.0"
 && [string trim $VAR(SPR-KKR-ENERG_EMAX_EV) ] == "9999.0"
} {
  if {[string trim $VAR(EMIN)] !=""} {append TXT  "   EMIN=$VAR(EMIN)"}
  if {[string trim $VAR(EMAX)] !=""} {append TXT  "   EMAX=$VAR(EMAX)"}
  if {[string trim $TXT] !=""} {append TXT  " Ry"}
} else {
  if {[string trim $VAR(SPR-KKR-ENERG_EMIN_EV) ] != "9999.0"} {append TXT "\n      EMINEV=$VAR(SPR-KKR-ENERG_EMIN_EV)"}
  if {[string trim $VAR(SPR-KKR-ENERG_EMAX_EV) ] != "9999.0"} {append TXT "\n      EMAXEV=$VAR(SPR-KKR-ENERG_EMAX_EV)"}
}
  if {[string trim $VAR(SPR-KKR-ENERG_EWORK_EV)   ] != "9999.0"} {append TXT    "\n      EWORK_EV=$VAR(SPR-KKR-ENERG_EWORK_EV)"    }
  if {[string trim $VAR(SPR-KKR-ENERG_IMV_INI_EV) ] != "9999.0"} {append TXT "\n     IMV_INI_EV=$VAR(SPR-KKR-ENERG_IMV_INI_EV) IMV_FIN_EV=$VAR(SPR-KKR-ENERG_IMV_FIN_EV)"}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
} else {
  if {[string trim $VAR(EMIN)] !=""} {append TXT  "   EMIN=$VAR(EMIN)"}
  if {[string trim $VAR(EMAX)] !=""} {append TXT  "   EMAX=$VAR(EMAX)"}
  if {[string trim $TXT] !=""} {append TXT  " Ry"}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
}

set TXT     "      "
######## if {$TASK!="SCF" && $TASK!="DOS"} {append TXT  "   EF=$VAR(EF)"}
if {$VAR(SEARCHEF) != 0 && $VAR(GRID)==5} {append TXT  "   SEARCHEF"}
if {[string trim $TXT] !=""} {puts  $inp  $TXT}
puts  $inp  " "
}

if {$TASK == "SIGMA"} {
   puts  $inp  "ENERGY   ImE=$VAR(ImE)"
   puts  $inp  " "
}
#EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE


#=======================================================================================
#                                 SECTION TASK
#=======================================================================================
#
#----------------------------------------------------------------------------------- DOS
#
if {$TASK == "DOS" } {
    puts $inp "TASK     DOS"
#
#--------------------------------------------------------------------------------- EKREL
#
} elseif {$TASK == "EKREL" } {
    puts $inp "TASK     EKREL   EMIN=$VAR(EMINDISP)  EMAX=$VAR(EMAXDISP) Ry  NE=$VAR(NEDISP)"
    if {$VAR(KPATH)=="-1"} {
	puts $inp "         NK = $VAR(NKDISP)    NKDIR = 1"
	set TXTA "KA1 = \{$VAR(KA1x), $VAR(KA1y), $VAR(KA1z) \}"
	set TXTE "KE1 = \{$VAR(KE1x), $VAR(KE1y), $VAR(KE1z) \}"
	puts $inp "         $TXTA   $TXTE"
    } else {
	puts $inp "         NK = $VAR(NKDISP)    KPATH = $VAR(KPATH)"
}
#
#------------------------------------------------------------------------------- BLOCHSF
#
} elseif {$TASK == "BLOCHSF" } {
    if {$VAR(MODEBSF)==1} {
       puts $inp "TASK     BSF"
       if {$VAR(KPATH)=="-1"} {
           puts $inp "         NK = $VAR(NKBSF)    NKDIR = 1"
           set TXTA "KA1 = \{$VAR(KA1x), $VAR(KA1y), $VAR(KA1z) \}"
           set TXTE "KE1 = \{$VAR(KE1x), $VAR(KE1y), $VAR(KE1z) \}"
           puts $inp "         $TXTA   $TXTE"
       } else {
           puts $inp "         NK = $VAR(NKBSF)    KPATH = $VAR(KPATH)"
       }

    } else {
       puts $inp "TASK     BSF"
       puts $inp "         NK1 = $VAR(NK1BSF)    K1 = \{$VAR(KBSF1x), $VAR(KBSF1y), $VAR(KBSF1z) \}"
       puts $inp "         NK2 = $VAR(NK2BSF)    K2 = \{$VAR(KBSF2x), $VAR(KBSF2y), $VAR(KBSF2z) \}"
    }
puts  $inp  " "
#
#-------------------------------------------------------------------------------- WFPLOT
#
} elseif {$TASK == "WFPLOT" } {

   puts $inp "TASK     WFPLOT     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))"

   if {$VAR(CORE_VB)=="VB"} {
      puts $inp "         STATE=BAND  L=$VAR(VB_l)    mj=+1/2    # E taken from  \[ENERGY\]"
      puts $inp "#        STATE=CORE  CL=$VAR(NCXRAY)$VAR(LCXRAY)  mj=+1/2 "
   } else {
      puts $inp "         STATE=CORE  CL=$VAR(NCXRAY)$VAR(LCXRAY)  mj=+1/2 "
      puts $inp "#        STATE=BAND  L=$VAR(VB_l)    mj=+1/2    # E taken from  \[ENERGY\]"
   }
      puts  $inp  " "

#
#-------------------------------------------------------------------------------- PLOT1D
#
} elseif {$TASK == "PLOT1D" } {

   set TXT "TASK     PLOT1D"
   if {$VAR(plot1D-POT) !="0"} {append TXT  "   PLOT1DPOT"}
   if {$VAR(plot1D-RHO) !="0"} {append TXT  "   PLOT1DRHO"}
   if {$VAR(plot1D-SFN) !="0"} {append TXT  "   PLOT1DSFB"}
   puts  $inp  $TXT

   puts  $inp  " "

#
#-------------------------------------------------------------------------------- PLOT2D
#
} elseif {$TASK == "PLOT2D" } {

   set TXT "TASK     PLOT2D"
   if {$VAR(plot2D-POT) !="0"} {append TXT  "   PLOT2DPOT"}
   if {$VAR(plot2D-RHO) !="0"} {append TXT  "   PLOT2DRHO"}
   puts  $inp  $TXT

   set TXT "AVEC = \{ [format %7.3f $VAR(plot2D-AVECx)],[format %7.3f $VAR(plot2D-AVECy)]"
   append TXT    "[format %7.3f $VAR(plot2D-AVECz)] \}   NA =[format %5i $VAR(plot2D-NA)]" 
   puts  $inp   "         $TXT"

   set TXT "BVEC = \{ [format %7.3f $VAR(plot2D-BVECx)],[format %7.3f $VAR(plot2D-BVECy)]"
   append TXT    "[format %7.3f $VAR(plot2D-BVECz)] \}   NB =[format %5i $VAR(plot2D-NB)]"
   puts  $inp   "         $TXT"

   set TXT "CVEC = \{ [format %7.3f $VAR(plot2D-CVECx)],[format %7.3f $VAR(plot2D-CVECy)]"
   append TXT    "[format %7.3f $VAR(plot2D-CVECz)] \}"
   puts  $inp   "         $TXT"

   puts  $inp  " "
#
#-------------------------------------------------------------------------------- PSHIFT
#
} elseif {$TASK == "PSHIFT" } {
    puts $inp "TASK     PSHIFT"
#
#-------------------------------------------------------------------------------- SOCPAR
#
} elseif {$TASK == "SOCPAR" } {
    puts $inp "TASK     SOCPAR"
#
#---------------------------------------------------------------------------------- FMAG
#
} elseif {$TASK == "FMAG" } {
    puts $inp "TASK     FMAG"
#
#
#----------------------------------------------------------------------------------- JXC
#
} elseif {$TASK == "JXC" } {
puts $inp "TASK     JXC   CLURAD=$VAR(CLURAD_JXC)"
#
#-------------------------------------------------------------------------------- MEPLOT
#
} elseif {$TASK == "MEPLOT" } {

   puts $inp "TASK     MEPLOT     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))"

   if {$VAR(transition)=="core-band"} {
       set C_core " " ; set C_band "#"
   } else {
       set C_core "#" ; set C_band " "
   }
   set NEP $VAR(NEPHOT) ; set EPMIN $VAR(EPHOTMIN) ;  set EPMAX $VAR(EPHOTMAX)  
   if {$VAR(MEPLOT_NONMAG) =="YES"} {
       set ME_NONMAG "NONMAG"
   } else {
       set ME_NONMAG "MAGNETIC"
   }
   if {$VAR(MEPLOT_l) == "all" } {set LISEL "" } else {set LISEL "L=$VAR(MEPLOT_l)    "}

   puts $inp "$C_core        INITIALSTATE=CORE  CL=$VAR(NCXRAY)$VAR(LCXRAY)  $ME_NONMAG"
   puts $inp "$C_band        INITIALSTATE=BAND  $LISEL $ME_NONMAG    # E taken from  \[ENERGY\]"
   if {$VAR(NEPHOT) == 1} {
      puts $inp "$C_band                           EPHOT=$EPMIN eV "
      puts $inp "#                           NEPHOT=$NEP EPHOTMIN=$EPMIN  EPHOTMAX=$EPMAX eV "
   } else {
      puts $inp "$C_band                           NEPHOT=$NEP EPHOTMIN=$EPMIN  EPHOTMAX=$EPMAX eV "
      puts $inp "#                           EPHOT=$EPMIN eV "
   }
   puts  $inp  " "
#
#-------------------------------------------------------------------------------- MECHECK
#
} elseif {$TASK == "MECHECK" } {

   puts $inp "TASK     MECHECK     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))"

   if {$VAR(transition)=="core-band"} {
       set C_core " " ; set C_band "#"
   } else {
       set C_core "#" ; set C_band " "
   }

   puts $inp "$C_core        INITIALSTATE=CORE  CL=$VAR(NCXRAY)$VAR(LCXRAY)            ERYDF=\{$VAR(ReERYDF),$VAR(ImERYDF)\} Ry"
   puts $inp "$C_band        INITIALSTATE=BAND  ERYDI=\{$VAR(ReERYDI),$VAR(ImERYDI)\} ERYDF=\{$VAR(ReERYDF),$VAR(ImERYDF)\} Ry"
   puts  $inp  " "
#
#--------------------------------------------------------------------------------- VBXPS
#
} elseif {$TASK == "VBXPS" } {
    set TXT  "TASK     VBPES     EPHOT=$VAR(EPHOT)"
    if {$VAR(USETAUNN)=="1"} {
	puts $inp "$TXT  USETAUNN"
    } else {
	puts $inp  "$TXT"
    }
#ME  NAB
#MODE
#
#----------------------------------------------------------------------------------- BIS
#
} elseif {$TASK == "BIS" } {
puts $inp "TASK     BIS     EPHOT=$VAR(EPHOT)"
#
#--------------------------------------------------------------------------------- CLXPS
#
} elseif {$TASK == "CLXPS" } {
set TXT "TASK     CLXPS     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))  CL=$VAR(NCXRAY)$VAR(LCXRAY)  EPHOT=$VAR(EPHOT)"
if {$VAR(USETSS)=="1"} {
    puts $inp "$TXT  USETSS"
} else {
    puts $inp  "$TXT"
}

#USETSS
#ME
#ANGRES
#ANGQ1
#ANGE1
#ANGK1
#ANGQ[N]
#ANGE[N]
#ANGK[N]
#

#------------------------------------------------------------------------------- COMPTON
#
} elseif {$TASK == "COMPTON" } {
set  TXT "TASK     COMPTON"
puts $inp "$TXT    PNVEC =\{$VAR(COMPTON-PNVECx),$VAR(COMPTON-PNVECy),$VAR(COMPTON-PNVECz)\}"
set  TXT "         PNMAX=$VAR(COMPTON-PNMAX)   NPN=$VAR(COMPTON-NPN)"
puts $inp "$TXT    PPMAX=$VAR(COMPTON-PPMAX)   NPP=$VAR(COMPTON-NPP)"
#
#----------------------------------------------------------------------------------- AES
#
} elseif {$TASK == "AES" } {
set TXT "TASK     AES     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))  CL=$VAR(NCXRAY)$VAR(LCXRAY) "
if {[string trim $VAR(AESNEME)] !=$VAR(AESNEME-DEF)} {append TXT  "   NEME=$VAR(AESNEME)"}
puts $inp $TXT


#PRINTME
#
#--------------------------------------------------------------------------------- NRAES
#
} elseif {$TASK == "NRAES" } {
puts $inp "# NOTE:  run the program first to create the DOS "
puts $inp "#        using this input file as it is i.e. with   TASK  DOS"
puts $inp "#        for the second run supress the line with   TASK  DOS (insert #)"
puts $inp "#        and activate the line with TASK  NRAES (remove #)"
puts $inp " "
puts $inp "TASK     DOS"
puts $inp " "
puts $inp "#TASK     NRAES     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))  CL=$VAR(NCXRAY)$VAR(LCXRAY) "

#CALCLS
#OUTGNU
#
#----------------------------------------------------------------------------------- APS
#
} elseif {$TASK == "APS" } {
puts $inp "# NOTE:  run the program first to create the DOS "
puts $inp "#        using this input file as it is i.e. with   TASK  DOS"
puts $inp "#        for the second run supress the line with   TASK  DOS (insert #)"
puts $inp "#        and activate the line with TASK  APS (remove #)"
puts $inp " "
puts $inp "TASK     DOS"
puts $inp " "
puts $inp "#TASK     APS     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))  CL=$VAR(NCXRAY)$VAR(LCXRAY) "

#CALCLS
#OUTGNU
#BRDDOS
#GAMMAV
#
#----------------------------------------------------------------- XAS / XMO / XES / XRS
#
} elseif {$TASK == "XAS" || $TASK == "XMO"|| $TASK == "XES" || $TASK == "XRS"  } {

puts $inp "TASK     $TASK     IT=$VAR(ITXRAY)  ($TXTT($VAR(ITXRAY)))  CL=$VAR(NCXRAY)$VAR(LCXRAY) "

if {$TASK == "XMO"} {
   set TXT "         "
   if {[string trim $VAR(TAUJMIN)] > 0.0001} {
      append TXT "TAUCORE=\{$VAR(TAUJMIN)"
      if {[string trim $VAR(TAUJPLS)] > 0.0001} {
	  append TXT  ", $VAR(TAUJPLS)\}"
      } else {
	  append TXT  "\}"
      }
   } else {
      if {[string trim $VAR(TAUJPLS)] > 0.0001} {append TXT "TAUCORE=\{$VAR(TAUJPLS),$VAR(TAUJPLS)\}" }
   }

   if {[string trim $TXT] ==""} { set TXT  "      " }

   if {[string trim $VAR(TSELECT)]  !="0"}                {append TXT  "   TSELECT=$VAR(TSELECT)"}
   if {[string trim $VAR(XMONE3)]   !=$VAR(XMONE3-DEF)}   {append TXT  "   NE3=$VAR(XMONE3)"}
   if {[string trim $VAR(XMOEMAX3)] !=$VAR(XMOEMAX3-DEF)} {append TXT  "   EMAX3=$VAR(XMOEMAX3)"}

   if {[string trim $TXT] !=""} { puts $inp $TXT}

} ; #  TASK == "XMO"


if {$VAR(IBZINT) == "CLUSTER" } {
    puts $inp " "
    puts $inp "# NOTE: the TAU-file will be (has been) created for site IQCNTR=$VAR(IQCNTR)"
    puts $inp "#       on which atom type  IT=$VAR(ITXRAY)  is sitting by using the cluster method"
    puts $inp "#       for any other atom type  IT  sitting on another site  IQ "
    puts $inp "#       the TAU-file has to be created anew !"
    puts $inp " "
}

# $TASK == "XAS" || $TASK == "XMO"|| $TASK == "XES" || $TASK == "XRS"


#----------------------------------------------------------------------------------- CHI
#
} elseif {$TASK == "CHI" } {

    set TXT  "TASK     CHI"
    if {$VAR(CHIPRINT) > 0} {append TXT "   PRINT=$VAR(CHIPRINT) "}
    puts $inp $TXT

    for {set IT 1} {$IT <= $NT} {incr IT} {
	set TXT "         NEF$IT = \{"
	for {set IL 1} {$IL <= $NL} {incr IL} {
	    set INDNTL "nef$IT{_}$IL"
	    append TXT "$VAR($INDNTL)"
	    if {$IL < $NL } {
		append TXT ", "
	    } else {
		append TXT "\}"
	    }
	}
	puts $inp $TXT
    }
    puts $inp " "
    
#
#
#------------------------------------------------------------------------------------ T1
#
} elseif {$TASK == "T1" } {
    puts $inp "TASK     T1 "
    puts $inp " "
#
#
#----------------------------------------------------------------------------------- RHO
#
} elseif {$TASK == "RHO" } {
    puts $inp "TASK     RHO"
    puts $inp " "
# RHOME
#
#
#--------------------------------------------------------------------------------- SIGMA or GILBERT
#
} elseif {$TASK == "GILBERT" || $TASK == "SIGMA"  } {

    puts $inp "TASK     $TASK"

    if {$VAR(NTMP) != "0" } {
       puts $inp "         TMPMIN = $VAR(TMPMIN)  TMPMAX = $VAR(TMPMAX)  NTMP = $VAR(NTMP)"
       puts $inp "         TDEBYE = $VAR(TDEBYE) "
    }
    puts $inp " "
#
#
#
#
#---------------------------------------------------------------------------------  KKRSPEC
#                                                                                   ARPES
#                                                                                   LEED
#
} elseif {$TASK == "ARPES" || $TASK == "AIPES"
       || $TASK == "LEED"  || $TASK == "BAND"} {


          write_inpfile_SPR-KKR_KKRSPEC $TASK $inp

#
#
#----------------------------------------------------------------------------------- SCF
#
} elseif {$VAR(TASK) == "SCF" } {
   puts  $inp  "SCF      NITER=$VAR(SCFNITER) MIX=$VAR(SCFMIX) VXC=$VAR(SCFVXC)"
   set TXT "    "
   if {$VAR(SCFALG)    != "BROYDEN2" } {append TXT " ALG=$VAR(SCFALG) "}
   if {$VAR(SCFTOL)    != "0.000001" } {append TXT " TOL=$VAR(SCFTOL) "}
   if {$VAR(SCFMIXOP)  != "0.001"    } {append TXT " MIXOP=$VAR(SCFMIXOP) "}
   if {$VAR(SCFISTBRY) != "0.001"    } {append TXT " ISTBRY=$VAR(SCFISTBRY) "}
   puts  $inp  "    $TXT"

   if {$VAR(FULLPOT)   != "0"        } {
       set TXT "         FULLPOT "
       if {$VAR(SPHERCELL)   != "0" } {append TXT " SPHERCELL " }
       puts  $inp  "$TXT"
   }

   set TXT "         QIONSCL=$VAR(QIONSCL) "
   puts  $inp  "$TXT"


   if {$VAR(MATTHEISS) == "QION"} {
      set TXT "QION=\{$VAR(QION1)"
      for {set IT 2} {$IT <= $NT} {incr IT} {
         set INDEX "QION$IT"
         append TXT  ",$VAR($INDEX)"
      }
      append TXT  "\}"
   puts  $inp  "         $TXT"
   }

   if {$VAR(NOSSITER) == 1 } {
       set TXT "         NOSSITER "
       puts  $inp  "$TXT"
   }

   if {$VAR(SCL_ALAT) == 1 } {
       set TXT "         SCL_ALAT  SCL1 = [format %4.2f $VAR(SCL_ALAT-SCL1)]"
       append TXT               "  SCL2 = [format %4.2f $VAR(SCL_ALAT-SCL2)]"
       append TXT               "  NSCL = [format %4i   $VAR(SCL_ALAT-NSCL)]"
       puts  $inp  "$TXT"
   }

   if {$VAR(USEMSPIN) == 1 } {
      set TXT "MSPIN=\{$VAR(MSPIN1)"
      for {set IT 2} {$IT <= $NT} {incr IT} {
         set INDEX "MSPIN$IT"
         append TXT  ",$VAR($INDEX)"
      }
      append TXT  "\}"
   puts  $inp  "         $TXT"
   }

   if {$VAR(MATTHEISS) == "VMATT"} {puts  $inp  "         USEVMATT"}

#SCFITDEPT" $ws 1         80   0 2 1        VAR SCFITDEPT $fe
#SCFSIM"    $ws 0.00     0.80  0 3 0.001    VAR SCFSIM    $fe
#COREHOLE
#ITHOLE
#NQNHOLE
#LQNHOLE
#
#
#------------------------------------------------------------------------------- TUTORIAL
#
} elseif {$VAR(TASK) == "TUTORIAL" } {

   puts  $inp  "TASK     TUTORIAL  TUT-TASK = $VAR(TUT-TASK) "

   set TUTTASK $VAR(TUT-TASK)

   if { $TUTTASK == "YLM" } {

      puts  $inp  "         L = $VAR(YLM-l)   M = $VAR(YLM-m)"

   } elseif { $TUTTASK == "jlx" } {

      set TXT "         LMAX = $VAR(jlx-lmax)   NX = $VAR(jlx-NX)"
      puts  $inp  "$TXT   XMIN = $VAR(jlx-xmin)   XMAX = $VAR(jlx-xmax)"

   } elseif { $TUTTASK == "jlx-asympt" } {

      set TXT "         LMAX = $VAR(jlx-lmax)   NX = $VAR(jlx-NX)"
      append TXT  "   XMIN = $VAR(jlx-xmin)   XMAX = $VAR(jlx-xmax)"
      puts  $inp  "$TXT   X-RANGE = $VAR(jlx-x-range)"

   } elseif { $TUTTASK == "RWF-E" } {

      set TXT "         L = $VAR(RWF-E-l)"
      puts  $inp  "$TXT   "

   }

}

puts "mm closeinp $inp"
close $inp

#
#------------------------------------------------------ initialize variables for SPR-KKR
#
set I -1
foreach X [list CLUSTER WEYL POINTS TET ZOOM] {
  incr I
  if {$VAR(IBZINT)==$X } {set IBZINT $I}
}


if {$VAR(HAMILTONIAN)=="NREL"} {
   set VAR(IREL) 0
} elseif {$VAR(HAMILTONIAN)=="SREL"} {
   set VAR(IREL) 1
} elseif {$VAR(HAMILTONIAN)=="SP-SREL"} {
   set VAR(IREL) 2
} else {
   set VAR(IREL) 3
}


if {$VAR(BEXT)>0.0} {set VAR(EXTFIELD) T}

set s 0.0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set s [expr $s + pow($RWS($IQ),3)]
}
set s [expr $s*4.0*$PI/3.0]

set VUC [ expr  $RBASX(1)*($RBASY(2)*$RBASZ(3)-$RBASY(3)*$RBASZ(2)) \
              + $RBASX(2)*($RBASY(3)*$RBASZ(1)-$RBASY(1)*$RBASZ(3)) \
              + $RBASX(3)*($RBASY(1)*$RBASZ(2)-$RBASY(2)*$RBASZ(1)) ]
set VUC [ expr  abs($VUC) *  pow($ALAT,3) ]

# ---------------------- for ALAT = 1 use tabulated RWS to fix ALAT

if { [string trim [expr $ALAT*1.0]] == "1.0" } {
   set ALAT [expr pow([expr $s/$VUC],[expr 1.0/3.0]) ]
   set scale_RWS 1.0
   debug $PRC "the lattice parameter ALAT has been set to  $ALAT"
} else {
   set scale_RWS [expr pow([expr $VUC/$s],[expr 1.0/3.0]) ]
   debug $PRC "scaling RWS by $scale_RWS"
}

set s 0.0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set RWS($IQ) [expr $RWS($IQ)*$scale_RWS]
   set s [expr $s + pow([expr $RWS($IQ)*$scale_RWS],3)]
#   set IMQ $IMT($ITOQ(1,$IQ))
}
set s [expr $s*4.0*$PI/3.0]
debug $PRC "volume unit cell    $VUC  $s"

for {set IM 1} {$IM <= $NM} {incr IM} {
   debug $PRC "mesh  $IM   WS        $RWSM($IM)   "
   set  RWSM($IM) [expr $RWSM($IM)*$scale_RWS]
   set  JWS($IM) 721
   set  R1($IM)  0.000001
   set  DX($IM) [expr log($RWSM($IM)/$R1($IM))/($JWS($IM)-1.0)]
   set  RMTM($IM) [expr 0.85 * $RWSM($IM)]
   set  IRMT    [expr 1 + log($RMTM($IM)/$R1($IM))/$DX($IM)]
   set  JMT($IM) [expr int($IRMT) ]
   set  JMT($IM) 0
   debug $PRC "mesh  $IM   previous  [expr $R1($IM)*exp($DX($IM)*($JMT($IM)-1)) ]"
   debug $PRC "mesh  $IM   MT        $RMTM($IM) $JMT($IM) "
   debug $PRC "mesh  $IM   next      [expr $R1($IM)*exp($DX($IM)*($JMT($IM)+1-1)) ]"
   debug $PRC "mesh  $IM   WS        $RWSM($IM) $JWS($IM)    DX $DX($IM)       "
}

#
#****************************************************************************************
#****************************************************************************************
#                                WRITE POTENTIAL FILE
#****************************************************************************************
#****************************************************************************************

if {$DIMENSION != "0D" } {
   set potfile $VAR(POTFIL)
} else {
   set potfile $VAR(POTFIL_CLU)
}

if {[file exists $potfile]=="0" || $VAR(OWRPOT)=="1" } {


debug $PRC "POTFMT $VAR(POTFMT)"

set pot [open $potfile w]

if {$VAR(POTFMT)<=5} {
#**************************************************************************** POTFMT <= 5
set TXT     "SPRKKR    SCF-start data created by xband  [standard_date]"
if { [string length $TXT] > 80 } {
puts  $inp  [string range $TXT 0 79]
} {
puts  $inp  $TXT
}
puts  $pot  "FORMAT    VERSION  3  (13.11.2000)"
puts  $pot  "TITLE     NONE"
puts  $pot  "SYSTEM    $sysfile"
puts  $pot  "NM        [format %10i $NM]"
puts  $pot  "NQ        [format %10i $NQ]"
puts  $pot  "NT        [format %10i $NT]"
puts  $pot  "NS        [format %10i 2]"
puts  $pot  "NE        [format %10i $VAR(NE)]"
puts  $pot  "IREL      [format %10i $VAR(IREL)]"
puts  $pot  "IBZINT    [format %10i $IBZINT]"
puts  $pot  "NKTAB     [format %10i 0]"
puts  $pot  "INFO      NONE"
puts  $pot  "XC-POT    $VAR(SCFVXC)"
puts  $pot  "SCF-ALG   $VAR(SCFALG)"
puts  $pot  "SCF-ITER  [format %10i   0]"
puts  $pot  "SCF-MIX   [format %20.10f $VAR(SCFMIX)]"
puts  $pot  "SCF-TOL   [format %20.10f $VAR(SCFTOL)]"
puts  $pot  "BREITINT    $VAR(BREITINT)"
puts  $pot  "ORBPOL    $VAR(ORBPOL)"
puts  $pot  "EXTFIELD    $VAR(EXTFIELD)"
puts  $pot  "BLCOUPL     $VAR(BLCOUPL)"
puts  $pot  "BEXT      [format %20.10f $VAR(BEXT)]"
puts  $pot  "KMROT     [format %10i    $VAR(KMROT)]"

set TXT "QMVEC     [format %20.10f $VAR(QMVEC-x)][format %20.10f $VAR(QMVEC-y)]"
puts  $pot   [append TXT                        "[format %20.10f $VAR(QMVEC-z)]"]
puts  $pot  "EF        [format %20.10f $VAR(EF)]"
puts  $pot  "BRAVAIS   [format %10i    $BRAVAIS]        $TABBRAVAIS($BRAVAIS)"
puts  $pot  "BASIS     [format %10i    $VAR(IBAS)]"
puts  $pot  "ALAT      [format %20.10f $ALAT]"
puts  $pot  "BOA       [format %20.10f $BOA]"
puts  $pot  "COA       [format %20.10f $COA]"

for {set I 1} {$I <= 3} {incr I} {
   set TXT "R-BAS A([format %1i $I])[format %20.10f $RBASX($I)]"
   append TXT                      "[format %20.10f $RBASY($I)]"
   append TXT                      "[format %20.10f $RBASZ($I)]"
   puts  $pot $TXT
}
puts  $pot  \
"********************************************************************************"
puts  $pot  "MESH INFORMATION"
puts  $pot  "MESH-TYPE EXPONENTIAL "
for {set IM 1} {$IM <= $NM} {incr IM} {
   puts  $pot  "MESH      [format %10i    $IM]"
   puts  $pot  "RWS       [format %20.10f $RWSM($IM)]"
   puts  $pot  "RMT       [format %20.10f $RMTM($IM)]"
   puts  $pot  "JWS       [format %10i    $JWS($IM)]"
   puts  $pot  "JMT       [format %10i    $JMT($IM) ]"
   puts  $pot  "R(1)      [format %20.10f $R1($IM)]"
   puts  $pot  "DX        [format %20.10f $DX($IM)]"
}

puts  $pot  \
"********************************************************************************"
puts  $pot  "SITES     [format %10i    $NQ]"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

   set TXT "[format %4i $IQ]"
   append TXT " Q=([format %14.10f $RQX($IQ)],"
   append TXT "[format %14.10f $RQY($IQ)],"
   append TXT "[format %14.10f $RQZ($IQ)])   NOQ="
   append TXT "[format %3i $NOQ($IQ)]  IT,CONC="
   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ($IO,$IQ)
      append TXT "[format %3i $IT ][format %6.3f $CONC($IT)]"
   }
   puts  $pot $TXT
}

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set TXT "[format %4i $IQ] QMTET = [format %15.10f $QMTET($IQ)]"
   puts  $pot [append TXT  " QMPHI = [format %15.10f $QMPHI($IQ)]"]
}

puts  $pot  \
"********************************************************************************"
puts  $pot  "MADELUNG MATRIX  SMAD"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set NLINES [expr ($NQ-1)/5 + 1]
   for {set I 1} {$I <= $NLINES} {incr I} {puts  $pot  " "}
# write empty lines for  (SMAD(IQ,JQ),JQ=1,NQ)
}

for {set IT 1} {$IT <= $NT} {incr IT} {
   puts  $pot  \
   "********************************************************************************"
   puts  $pot  "TYPE      [format %10i $IT]"
   puts  $pot  [format %-8s $TXTT($IT)]
   puts  $pot  "Z         [format %10i $ZT($IT)]"
   puts  $pot  "NAT       [format %10i $NAT($IT)]"
   puts  $pot  "CONC      [format %20.10f $CONC($IT)]"
   puts  $pot  "MESH      [format %10i $IMT($IT)]"
   set TXT "SITES Q   "
   for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
     if {( [expr $IA-1] % 7) == 0 && $IA!=1} { append TXT "\n          " }  
#    line break at col 70
     set TXT $TXT[format %10i $IQAT($IA,$IT)]
   }
   puts  $pot $TXT
}

#**************************************************************************** POTFMT <= 5
} else {
#**************************************************************************** POTFMT >  5


#----------------------------------------------------------------------------------- HOST
if {$DIMENSION != "0D" } {
#
puts  $pot  \
"*******************************************************************************"
set TXT     "HEADER    'SCF-start data created by xband  [standard_date]'"
if { [string length $TXT] > 80 } {
puts  $inp  [string range $TXT 0 79]
} {
puts  $inp  $TXT
}
puts  $pot  \
"*******************************************************************************"
puts  $pot  "TITLE     'SPR-KKR calculation for $sysfile'"
puts  $pot  "SYSTEM    $sysfile"
puts  $pot  "PACKAGE   SPRKKR"
puts  $pot  "FORMAT    6  (21.05.2007)"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "GLOBAL SYSTEM PARAMETER"
puts  $pot  "NQ        [format %10i $NQ]"
puts  $pot  "NT        [format %10i $NT]"
puts  $pot  "NM        [format %10i $NM]"
puts  $pot  "IREL      [format %10i $VAR(IREL)]"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "SCF-INFO"
puts  $pot  "INFO      NONE"
puts  $pot  "SCFSTATUS START"
puts  $pot  "FULLPOT   F"
puts  $pot  "BREITINT  $VAR(BREITINT)"
if {$VAR(NONMAG) != "0" } {
   puts  $pot  "NONMAG    T"
} else {
   puts  $pot  "NONMAG    F"
}
puts  $pot  "ORBPOL    $VAR(ORBPOL)"
puts  $pot  "EXTFIELD  $VAR(EXTFIELD)"
puts  $pot  "BLCOUPL   $VAR(BLCOUPL)"
puts  $pot  "BEXT      [format %16.10f $VAR(BEXT)]"
if {$VAR(SEMICORE) != "0" } {
   puts  $pot  "SEMICORE  T"
} else {
   puts  $pot  "SEMICORE  F"
}
if {$VAR(LLOYD) != "0" } {
   puts  $pot  "LLOYD     T"
} else {
   puts  $pot  "LLOYD     F"
}
puts  $pot  "NE        [format %10i $VAR(NE)]"
puts  $pot  "IBZINT    [format %10i $IBZINT]"
puts  $pot  "NKTAB     [format %10i 0]"
puts  $pot  "XC-POT    $VAR(SCFVXC)"
puts  $pot  "SCF-ALG   $VAR(SCFALG)"
puts  $pot  "SCF-ITER  [format %10i   0]"
puts  $pot  "SCF-MIX   [format %16.10f $VAR(SCFMIX)]"
puts  $pot  "SCF-TOL   [format %16.10f $VAR(SCFTOL)]"
puts  $pot  "RMSAVV    [format %16.10f 999999]"
puts  $pot  "RMSAVB    [format %16.10f 999999]"
if {$VAR(EF)==""} { set VAR(EF) 999999 }
puts  $pot  "EF        [format %16.10f $VAR(EF)]"
puts  $pot  "VMTZ      [format %16.10f $VAR(VMTZ)]"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "LATTICE"
if {$DIMENSION == "3D" } {

   puts  $pot  "SYSDIM       3D"
   puts  $pot  "SYSTYPE      BULK"

} elseif  {$DIMENSION == "2D" } {

   if {$ZRANGE_TYPE=="extended"} {

#------------ use the sum of the atomic number sum_L and sum_R as a check sum 
      set sum_L 0
      for {set IQ 1} {$IQ <= $NQ_bulk_L} {incr IQ} {
         for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
	    set sum_L [expr $sum_L + $ZT($ITOQ($IA,$IQ))]
puts "LEFT : IQ: $IQ  IA: $IA  IT: $ITOQ($IA,$IQ) Z: $ZT($ITOQ($IA,$IQ)) sum_L: $sum_L"
         }
      }

      set sum_R 0
      for {set IQ [expr $NQ-$NQ_bulk_R+1]} {$IQ <= $NQ} {incr IQ} {
         for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
	    set sum_R [expr $sum_R + $ZT($ITOQ($IA,$IQ))]
puts "RIGHT: IQ: $IQ  IA: $IA  IT: $ITOQ($IA,$IQ) Z: $ZT($ITOQ($IA,$IQ)) sum_R: $sum_R"
         }
      }
      puts "check sums:         sum_L=$sum_L    sum_R=$sum_R"
      puts "bulk_L_eq_bulk_R:   $bulk_L_eq_bulk_R"

      if { $sum_L == $sum_R } {
	  if { $sum_R == "0" } {
                puts  $pot  "SYSDIM    2D"
                puts  $pot  "SYSTYPE   VIV"
            } else {
                puts  $pot  "SYSDIM    2D"
                puts  $pot  "SYSTYPE   LIR"
            } 
      } else {
	  if { $sum_R == "0" } {
	      puts  $pot  "SYSDIM    2D"
	      puts  $pot  "SYSTYPE   LIV"
	  } else {
	      puts  $pot  "SYSDIM    2D"
	      puts  $pot  "SYSTYPE   LIR"
	  }
      }

   } else {
      puts  $pot  "SYSTYPE   ZRANGE_TYPE=${ZRANGE_TYPE} NOT ALLOWED !!!! "
   }
}   
puts  $pot  "BRAVAIS   [format %10i    $BRAVAIS]        $TABBRAVAIS($BRAVAIS)"
puts  $pot  "ALAT      [format %16.10f $ALAT]"
for {set I 1} {$I <= 3} {incr I} {
   puts  $pot "A([format %1i $I])      [format %16.10f%16.10f%16.10f $RBASX($I) $RBASY($I) $RBASZ($I)]"
}

if  {$DIMENSION == "2D" } {
puts  $pot "NQ_L      [format %10i $NQ_bulk_L]"
puts  $pot "A_L(3)    [format %16.10f%16.10f%16.10f $RBASX_L(3) $RBASY_L(3) $RBASZ_L(3)]"
puts  $pot "NQ_R      [format %10i $NQ_bulk_R]"
puts  $pot "A_R(3)    [format %16.10f%16.10f%16.10f $RBASX_R(3) $RBASY_R(3) $RBASZ_R(3)]"
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "SITES"
puts  $pot  "CARTESIAN T"
set SCALE 1.0
puts  $pot "BASSCALE  [format %16.10f%16.10f%16.10f $SCALE $SCALE $SCALE]"
puts  $pot  "        IQ      QX              QY              QZ"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set TXT "[format %10i%16.10f%16.10f%16.10f $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]"
   puts  $pot $TXT
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "OCCUPATION"
puts  $pot  "        IQ     IREFQ       IMQ       NOQ  ITOQ  CONC"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set IREFQ($IQ) $IMQ($IQ)
   set TXT "[format %10i%10i%10i%10i $IQ $IREFQ($IQ) $IMQ($IQ) $NOQ($IQ)]"
   for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
      set IT $ITOQ($IO,$IQ)
      append TXT "[format %6i $IT ][format %6.3f $CONC($IT)]"
   }
   puts  $pot $TXT
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "REFERENCE SYSTEM"
set NREF $NM
puts  $pot  "NREF     [format %10i $NREF ]"
puts  $pot  "      IREF      VREF            RMTREF"
for {set IREF 1} {$IREF <= $NREF} {incr IREF} {
   set VREF($IREF)   4.0
   set RMTREF($IREF) 0.0
   puts  $pot "[format %10i%16.10f%16.10f $IREF $VREF($IREF) $RMTREF($IREF)]"
}

puts  $pot  \
"*******************************************************************************"
puts  $pot "MAGNETISATION DIRECTION"
puts  $pot "KMROT     [format %10i    $VAR(KMROT)]"
puts  $pot "QMVEC     [format %16.10f%16.10f%16.10f $VAR(QMVEC-x) $VAR(QMVEC-y) $VAR(QMVEC-z)]"
puts  $pot  "        IQ      QMTET           QMPHI "
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   puts  $pot "[format %10i%16.10f%16.10f $IQ $QMTET($IQ) $QMPHI($IQ)]"
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "MESH INFORMATION"
puts  $pot  "MESH-TYPE EXPONENTIAL "
puts  $pot  "   IM      R(1)            DX         JRMT      RMT        JRWS      RWS"
for {set IM 1} {$IM <= $NM} {incr IM} {
   puts  $pot  "[format %5i%16.10f%16.10f%5i%16.10f%5i%16.10f  \
                 $IM $R1($IM) $DX($IM) $JMT($IM) $RMTM($IM) $JWS($IM) $RWSM($IM)]"
}

puts  $pot  \
"*******************************************************************************"
puts  $pot "TYPES"
puts  $pot  "   IT     TXTT        ZT     NCORT     NVALT    NSEMCORSHLT"
for {set IT 1} {$IT <= $NT} {incr IT} {
   if {[info exists VAR(NVAL$IT)]==0} {
      set NVALT($IT) [expr $ZT($IT) - [lindex $TABNCORE $ZT($IT)] ]
   } else {
      set NVALT($IT) $VAR(NVAL$IT)
   }
   set NCORT($IT) [expr $ZT($IT) - $NVALT($IT)]
   set NSEMCORSHLT($IT) 0
   set BLANK5     "     "

   puts  $pot  "[format %5i%5s%-8s%10i%10i%10i%15i \
                $IT $BLANK5 $TXTT($IT) $ZT($IT) $NCORT($IT) $NVALT($IT) $NSEMCORSHLT($IT)]"
}


#----------------------------------------------------------------------------------- HOST
} else {
#-------------------------------------------------------------------------------- CLUSTER

#
puts  $pot  \
"*******************************************************************************"
set TXT     "HEADER    'SCF-start data created by xband  [standard_date]'"
if { [string length $TXT] > 80 } {
puts  $inp  [string range $TXT 0 79]
} {
puts  $inp  $TXT
}
puts  $pot  \
"*******************************************************************************"
puts  $pot  "TITLE     'SPR-KKR calculation for $sysfile'"
puts  $pot  "SYSTEM    $sysfile"
;#TAKEN FROM HOST !   puts  $pot  "PACKAGE   SPRKKR"
puts  $pot  "FORMAT    6  (21.05.2007)"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "GLOBAL SYSTEM PARAMETER"
puts  $pot  "NQCLU     [format %10i $NQCLU]"
puts  $pot  "NTCLU     [format %10i $NTCLU]"
set NMCLU 0
puts  $pot  "NMCLU     [format %10i $NMCLU]"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "SCF-INFO"
puts  $pot  "INFO      NONE"
puts  $pot  "SCFSTATUS START"
;#TAKEN FROM HOST !   puts  $pot  "BREITINT  $VAR(BREITINT)"
;#TAKEN FROM HOST !  if {$VAR(NONMAG) != "0" } {
;#TAKEN FROM HOST !     puts  $pot  "NONMAG    T"
;#TAKEN FROM HOST !  } else {
;#TAKEN FROM HOST !     puts  $pot  "NONMAG    F"
;#TAKEN FROM HOST !  }
;#TAKEN FROM HOST !  puts  $pot  "ORBPOL    $VAR(ORBPOL)"
;#TAKEN FROM HOST !  puts  $pot  "EXTFIELD  $VAR(EXTFIELD)"
;#TAKEN FROM HOST !  puts  $pot  "BLCOUPL   $VAR(BLCOUPL)"
;#TAKEN FROM HOST !  puts  $pot  "BEXT      [format %16.10f $VAR(BEXT)]"
;#TAKEN FROM HOST !  if {$VAR(SEMICORE) != "0" } {
;#TAKEN FROM HOST !     puts  $pot  "SEMICORE  T"
;#TAKEN FROM HOST !  } else {
;#TAKEN FROM HOST !     puts  $pot  "SEMICORE  F"
;#TAKEN FROM HOST !  }
;#TAKEN FROM HOST !  if {$VAR(LLOYD) != "0" } {
;#TAKEN FROM HOST !     puts  $pot  "LLOYD     T"
;#TAKEN FROM HOST !  } else {
;#TAKEN FROM HOST !     puts  $pot  "LLOYD     F"
;#TAKEN FROM HOST !  }
puts  $pot  "NE        [format %10i $VAR(NE)]"
puts  $pot  "IBZINT    [format %10i $IBZINT]"
puts  $pot  "NKTAB     [format %10i 0]"
;#TAKEN FROM HOST !  puts  $pot  "XC-POT    $VAR(SCFVXC)"
puts  $pot  "SCF-ALG   $VAR(SCFALG)"
puts  $pot  "SCF-ITER  [format %10i   0]"
puts  $pot  "SCF-MIX   [format %16.10f $VAR(SCFMIX)]"
puts  $pot  "SCF-TOL   [format %16.10f $VAR(SCFTOL)]"
puts  $pot  "RMSAVV    [format %16.10f 999999]"
puts  $pot  "RMSAVB    [format %16.10f 999999]"

puts  $pot  \
"*******************************************************************************"
puts  $pot  "SITES"
puts  $pot  "original lattice site positions"
puts  $pot  "     IQCLU            N5VEC_IQCLU          IQ"
for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {

   set TXT [format "%10i" $IQCLU ]
   append TXT [format "%9i%4i%4i%4i%4i%10i" \
              $N5VEC_IQCLU(1,$IQCLU) $N5VEC_IQCLU(2,$IQCLU) $N5VEC_IQCLU(3,$IQCLU) \
              $N5VEC_IQCLU(4,$IQCLU) $N5VEC_IQCLU(5,$IQCLU)  $IQ_IQCLU($IQCLU) ]

   puts  $pot $TXT
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "SHIFTS"
puts  $pot  "shift from original lattice site positions in case of relaxation"
puts  $pot  "     IQCLU        DQX             DQY             DQZ"
for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
   set DRQCLU(1,$IQCLU) 0.0
   set DRQCLU(2,$IQCLU) 0.0
   set DRQCLU(3,$IQCLU) 0.0
   set TXT "[format %10i%16.10f%16.10f%16.10f $IQCLU $DRQCLU(1,$IQCLU) $DRQCLU(2,$IQCLU) $DRQCLU(3,$IQCLU)]"
   puts  $pot $TXT
}

puts  $pot  \
"*******************************************************************************"
puts  $pot  "OCCUPATION"
puts  $pot  "     IQCLU    IMQCLU    NOQCLU   ITOQCLU      CONC"
for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
set IM_IQCLU($IQCLU) 0
   set TXT "[format %10i%10i%10i $IQCLU $IM_IQCLU($IQCLU) $NOCC_IQCLU($IQCLU)]"
   for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
      set ITCLU $ITCLU_OQCLU($IO,$IQCLU)
      append TXT "[format %10i $ITCLU ][format %10.5f $CONCCLU($ITCLU)]"
   }
   puts  $pot $TXT
}

puts  $pot  \
"*******************************************************************************"
puts  $pot "MAGNETISATION DIRECTION"
puts  $pot "KMROT     [format %10i    $VAR(KMROT)]"
puts  $pot  "     IQCLU      QMTET           QMPHI "
for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
set QMTET_IQCLU($IQCLU) 0.0
set QMPHI_IQCLU($IQCLU) 0.0
   puts  $pot "[format %10i%16.10f%16.10f $IQCLU $QMTET_IQCLU($IQCLU) $QMPHI_IQCLU($IQCLU)]"
}

#puts  $pot  "*******************************************************************************"
#puts  $pot  "MESH INFORMATION"
#puts  $pot  "MESH-TYPE EXPONENTIAL "
#............................

puts  $pot  \
"*******************************************************************************"
puts  $pot "TYPES"
puts  $pot  "ITCLU     TXTT        ZT     NCORT     NVALT    NSEMCORSHLT"
for {set ITCLU 1} {$ITCLU <= $NTCLU} {incr ITCLU} {
   if {[info exists VAR(NVAL$ITCLU)]==0} {
      set NVALT($ITCLU) [expr $ZTCLU($ITCLU) - [lindex $TABNCORE $ZTCLU($ITCLU)] ]
   } else {
      set NVALT($ITCLU) $VAR(NVAL$ITCLU)
   }
   set NCORT($ITCLU) [expr $ZTCLU($ITCLU) - $NVALT($ITCLU)]
   set NSEMCORSHLT($ITCLU) 0
   set BLANK5     "     "

   puts  $pot  "[format %5i%5s%-8s%10i%10i%10i%15i \
                $ITCLU $BLANK5 $TXTTCLU($ITCLU) $ZTCLU($ITCLU) $NCORT($ITCLU) $NVALT($ITCLU) $NSEMCORSHLT($ITCLU)]"
}


}
#-------------------------------------------------------------------------------- CLUSTER



}
#**************************************************************************** POTFMT >  5


close $pot

#exec emacs $potfile &
}



}
#                                                                   END write_inpfile
########################################################################################


########################################################################################
#------------------- return the minimum number of digits to represent an integer
#------------------- used to set parameter value for   SCALE
#
proc PREC_INT {I} {

    set P 7
    if {$I<10} {
	set P 1
    } elseif {$I>=10} {
	set P 2
    } elseif {$I>=100} {
	set P 3
    } elseif {$I>=1000} {
	set P 4
    } elseif {$I>=10000} {
	set P 5
    } elseif {$I>=100000} {
	set P 6
    }
    return $P
}

########################################################################################
#------------------- disable a widget
#
proc widget_disable {ww} {
    debug widget_disable "$ww"
    if {[winfo exists $ww]} {$ww configure -state disabled}
}
########################################################################################
#------------------- enable a widget
#
proc widget_enable {ww} {
    debug widget_enable "$ww"
    if {[winfo exists $ww]} {$ww configure -state normal}
}

########################################################################################
#                                                           create_inpfile_SPR-KKR_check
#
#     perform consistency checks before writing the input file
#
#
proc create_inpfile_SPR-KKR_check {key} {

    set PRC "create_inpfile_SPR-KKR_check"

    global structure_window_calls inpfile inpsuffix potfile
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NL
    global NCL  NQCL ITOQ BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VARLST NCPA COLOR
    global SPRKKR_EXPERT NVALT_DEFAULT
    global create_inpfile_SPRKKR_wc1
    global create_inpfile_SPRKKR_wc2

set TROUBLE 0
#
#--------------------------------------------------------------------------------------
#
if {$VAR(KMROT)>2 && $VAR(HAMILTONIAN)!="SP-SREL" } {
   give_warning "." "WARNING \n\n spin spirals can be treated only \n\n \
                                  within spin-polarized scalar-relativistic mode \n\n "
   return
}

set TASK $VAR(TASK)
#
#-------------------------------------------------------------------------------- PLOT1D
if {$TASK == "PLOT1D" } {
    if {$VAR(plot1D-POT) =="0" && $VAR(plot1D-RHO) =="0" && $VAR(plot1D-SFN)  =="0" } {
      give_warning "." "WARNING \n\n INFO from <create_inpfile_SPR-KKR>: \
                           \n\n TASK = PLOT1D  \
                           \n\n nothing selected for plotting \
                           \n\n select one or more from POT, RHO and SFN  \n "
       set TROUBLE 1
    }
#
#-------------------------------------------------------------------------------- PLOT2D
#
} elseif {$TASK == "PLOT2D" } {
    if {$VAR(plot2D-POT) =="0" && $VAR(plot2D-RHO) =="0" } {
      give_warning "." "WARNING \n\n INFO from <create_inpfile_SPR-KKR>: \
                           \n\n TASK = PLOT2D  \
                           \n\n nothing selected for plotting \
                           \n\n select one or more from POT and RHO  \n "
       set TROUBLE 1
    }
#======================================================================================
#                              $VAR(TASK) <> "SCF"
#======================================================================================
#
} elseif {$VAR(TASK) != "SCF" } {

    if {[file exists ${potfile}]==0} {
       create_inpfile_SPR-KKR_confirm $key potfile-missing
       set TROUBLE 1
    }

    if {[file exists ${potfile}_new]==1} {
       create_inpfile_SPR-KKR_confirm $key new-potfile-exists
       set TROUBLE 1
    }

    if {$TASK == "XAS" || $TASK == "XMO"|| $TASK == "XES" || $TASK == "XRS"  } {
       if {$VAR(IBZINT) == "CLUSTER" } {
           set IO 1
           if {$ITOQ($IO,$VAR(IQCNTR))!=$VAR(ITXRAY)} {
              set IQCNTR0 $VAR(IQCNTR)
              set IQCNTR  0
              set ITXRAY $VAR(ITXRAY)
              for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
                 for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
		    if {$IQCNTR==0} {
                       if {$ITXRAY==$ITOQ($IO,$IQ)} {set IQCNTR $IQ}
		    }
                 }
              }

              if {$IQCNTR==0} {
                 give_warning "." "WARNING \n\n type and site selection inconsistent \n\n \
                                  type  ITXRAY=$VAR(ITXRAY)  \n\n  \
                                  not sitting on  IQCNTR=$IQCNTR0  \n\n\n  \
                                  no proper site found   -->>  check once more \n\n "
                 return
              } else {
                 set VAR(IQCNTR) $IQCNTR
                 give_warning "." "WARNING \n\n type and site selection inconsistent \n\n \
                                  type  ITXRAY=$VAR(ITXRAY)  \n\n  \
                                  not sitting on  IQCNTR=$IQCNTR0  \n\n\n  \
                                  central site set to  IQCNTR=$VAR(IQCNTR) \n\n "
              }
           }
       }
    }

} else {
#======================================================================================
#                              $VAR(TASK) == "SCF"
#======================================================================================
    if {$VAR(IBZINT) == "CLUSTER" } {
        give_warning "." "WARNING \n\n no SCF-calculation possible \n\n in cluster - mode \n\n \
                                     use BZ-integration to get TAU  \n\n "
       return
    }
#--------------------------------------------------------------------------------------
    if {[file exists ${potfile}_new]==1 } {
       create_inpfile_SPR-KKR_confirm $key scf-start-potfile-exists
       set TROUBLE 1
#--------------------------------------------------------------------------------------
    } elseif {[file exists ${potfile}]==1 && $VAR(OWRPOT)==0} {
       create_inpfile_SPR-KKR_confirm $key scf-start-potfile-exists
       set TROUBLE 1
    }

}
#======================================================================================
#
#----------------------------------------------- all is OK --- now quit
#
if {$TROUBLE==0} {create_inpfile_SPR-KKR_quit $key}

} ; # end proc
#                                                      create_inpfile_SPR-KKR_check  END
########################################################################################

########################################################################################
proc create_inpfile_SPR-KKR_confirm {key trouble} {#      create_inpfile_SPR-KKR_confirm

  global file_to_handle jobfile outfile PRINTPS VAR potfile
  global inpsuffixlist
  global Wcinp
  global edxterm editor edoptions edback
  global file_to_handle target
  global unix_prog_suffix_list unix_prog_call sorttype
  global file_suffix

  set PRC create_inpfile_SPR-KKR_confirm

  set cw $Wcinp.confirm

  if {[winfo exists $cw] == 1} {destroy $cw}
  toplevel_init $cw " " 0 0

  wm geometry $cw +300+50

  wm title    $cw "confirm action"
  wm iconname $cw "confirm action"

#------------------------------------------------------------------------
  proc create_inpfile_SPR-KKR_update {pot pot_new pot_sav} {
      set PRC create_inpfile_SPR-KKR_update
      eval set res [catch "exec mv $pot $pot_sav" message]
      eval set res [catch "exec mv $pot_new $pot" message]
      writescr   .d.tt  "\n  replacing $pot by $pot_new  \n $message "
      debug $PRC            "replacing $pot by $pot_new   message: $message "
  }
#------------------------------------------------------------------------



#--------------------------------------------------------------------------------------
#
  if {$trouble=="potfile-missing"} {
     label $cw.pot -text "the potential file   ${potfile}   does not exist"  -pady 10  -padx 10
     button $cw.continue -text "CONTINUE" -width 50 -height 5 -bg green \
            -command "create_inpfile_SPR-KKR_quit $key"
     pack $cw.pot $cw.continue
#--------------------------------------------------------------------------------------
#
  } elseif {$trouble=="new-potfile-exists"} {
     eval set res [catch "exec ls -l ${potfile}  ${potfile}_new" message]

     label $cw.pot -text "a new potential file   ${potfile}_new   exists \n\n$message" \
                -pady 10  -padx 30 -anchor w -justify left
     button $cw.continue -text "CONTINUE\n\nwithout update" -width 50 -height 5 -bg green \
            -command "create_inpfile_SPR-KKR_quit $key"
     button $cw.update -text "replace  ${potfile}  \n\n by   ${potfile}_new \n\n and CONTINUE" \
            -width 50 -height 6 -bg lightblue \
            -command "create_inpfile_SPR-KKR_update ${potfile} ${potfile}_new ${potfile}.sav ; \
                      create_inpfile_SPR-KKR_quit $key"

     pack $cw.pot $cw.continue $cw.update
#
#--------------------------------------------------------------------------------------
#
  } elseif {$trouble=="scf-start-potfile-exists"} {

     if {[file exists ${potfile}]==1} {
         set pot ${potfile}
         set TXT "the potential file  ${potfile}  exists \n\n"
     } else {
         set pot ""
         set TXT ""
     }
     if {[file exists ${potfile}_new]==1} {
         set pot_new "${potfile}_new"
         append TXT "the potential file  ${potfile}_new  exists \n\n"
     } else {
         set pot_new ""
     }

     eval set res [catch "exec ls -l  $pot $pot_new" message]
     label $cw.pot -text "$TXT  \n$message"  -pady 10  -padx 30 -anchor w -justify left
     pack $cw.pot

     if {[file exists ${potfile}]==1} {
     button $cw.continue -text "CONTINUE   \n\n WITHOUT overwrite of ${potfile} " \
            -width 50 -height 5 -bg green \
            -command "set VAR(OWRPOT) 0 ; create_inpfile_SPR-KKR_quit $key"
     button $cw.overwrite -text "CONTINUE   \n\n and overwrite  ${potfile} " \
            -width 50 -height 5 -bg lightblue \
            -command "set VAR(OWRPOT) 1 ; create_inpfile_SPR-KKR_quit $key"
     pack $cw.continue $cw.overwrite
     }

     if {[file exists ${potfile}_new]==1} {
     button $cw.update -text "move  ${potfile}_new \n\n to  ${potfile}  \n\n and CONTINUE" \
            -width 50 -height 6 -bg lightblue \
            -command "create_inpfile_SPR-KKR_update ${potfile} ${potfile}_new ${potfile}.sav ; \
                      create_inpfile_SPR-KKR_quit $key"
     pack $cw.update
     }
#--------------------------------------------------------------------------------------
  }

  button $cw.return -text "RETURN\nto the menu" -width 50 -height 5 -bg tomato1 \
         -command "destros $cw ; return"
  pack  $cw.return

}
#                                                     END create_inpfile_SPR-KKR_confirm
########################################################################################


########################################################################################
proc create_inpfile_SPR-KKR_quit {key} {#              create_inpfile_SPR-KKR_quit
    global Wcinp 
    global TUTORIAL 

    if {$key=="ok" } {
	write_inpfile $Wcinp nix
	if {$TUTORIAL==0} {
          destroy $Wcinp
        } else {
          destroy $Wcinp.confirm
        }
    } elseif {$key=="edit" } {
	write_inpfile $Wcinp edit
	if {$TUTORIAL==0} {
          destroy $Wcinp
        } else {
          destroy $Wcinp.confirm
        }
    } else {
	return
    }
     
}
########################################################################################

