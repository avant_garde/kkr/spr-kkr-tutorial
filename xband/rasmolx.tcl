#
# $Id: rasmolx.tcl,v 1.12 2009-02-05 11:57:18 jm Exp $
#
#######################################################################################
#              description of pdb data format used for   rasmol  and  Jmol
#######################################################################################
#
# taken from: http://acces.ens-lyon.fr/biotic/rastop/help/fileformats.htm
#
# The only record types that are of major interest to the RasMol program 
# are the ATOM and HETATM records which describe the position of each atom. 
# ATOM/HETATM records contain standard atom names and residue abbreviations,
#  along with sequence identifiers, coordinates in �ngstrom units, 
# occupancies and thermal motion factors. The exact details are given below 
# as a FORTRAN format statement. The "fmt" column indicates use of the field 
# in all PDB formats, in the 1992 and earlier formats or in the 1996 
# and later formats.
#
# FORMAT(6A1,I5,1X,A4,A1,A3,1X,A1,I4,A1,3X,3F8.3,2F6.2,1X,I3,2X,A4,2A2)
# Column 	Content 	fmt
# 1-6 	'ATOM' or 'HETATM' 	all
# 7-11 	Atom serial number (may have gaps) 	all
# 13-16 	Atom name, in IUPAC standard format 	all
# 17 	Alternate location indicator indicated by A, B or C 	all
# 18-20 	Residue name, in IUPAC standard format 	all
# 23-26 	Residue sequence number 	all
# 27 	Code for insertions of residues (i.e. 66A & 66B) 	all
# 31-38 	X coordinate 	all
# 39-46 	Y coordinate 	all
# 47-54 	Z coordinate 	all
# 55-60 	Occupancy 	all
# 61-66 	Temperature factor 	all
# 68-70 	Footnote number 	92
# 73-76 	Segment Identifier (left-justified) 	96
# 77-78 	Element Symbol (right-justified) 	96
# 79-80 	Charge on the Atom 	96
# 
# Residues occur in order starting from the N-terminal residue for 
# proteins and 5'-terminus for nucleic acids. If the residue sequence 
# is known, certain atom serial numbers may be omitted to allow for future 
# insertion of any missing atoms. Within each residue, atoms are ordered 
# in a standard manner, starting with the backbone (N-C-C-O for proteins) 
# and proceeding in increasing remoteness from the alpha carbon, along 
# the side chain.
#
# HETATM records are used to define post-translational modifications 
# and cofactors associated with the main molecule. 
# TER records are interpreted as breaks in the main molecule's backbone.
#
# If present, RasMol also inspects HEADER, COMPND, HELIX, SHEET, TURN, 
# CONECT, CRYST1, SCALE, MODEL, ENDMDL, EXPDTA and END records. 
# Information such as the name, database code, revision date and 
# classification of the molecule are extracted from HEADER and COMPND 
# records, initial secondary structure assignments are taken from 
# HELIX, SHEET and TURN records, and the end of the file may be m 
# indicated by an END record.
#
# RasMol also accepts the supplementary COLO record type in the PDB files. 
# This extension is not currently supported by the PDB. 
# The COLO record has the same basic record type as the ATOM and HETATM records.
#
# Colours are assigned to atoms using a matching process. The Mask field is used 
# in the matching process as follows. First RasMol reads in and remembers all the 
# ATOM, HETATM and COLO records in input order. When the user-defined ('User') 
# colour scheme is selected, RasMol goes through each remembered ATOM/HETATM 
# record in turn, and searches for a COLO record that matches in all of columns 7 
# through 30. The first such COLO record to be found determines the colour m 
# and radius of the atom.
# Column 	Content
# 1-6 	'COLOR' or 'COLOUR'
# 7-30 	Mask (described below)
# 31-38 	Red component
# 39-46 	Green component
# 47-54 	Blue component
# 55-60 	Sphere radius in �ngstroms
# 61-70 	Comments
#  
# Note that the Red, Green and Blue components are in the same positions   
# as the X, Y, and Z components of an ATOM or HETA record, and the   
# van der Waals radius goes in the place of the Occupancy.   
# The Red, Green and Blue components must all be in the range 0 to 1.
# 
#######################################################################################
proc sites_graphik {} {

  # build widget .top2
  global RASRAD RASSCL NQ
  global IQCNTR CLURAD NSHLCLU Krasmol
  global IQSURF h_SURF k_SURF l_SURF o_SURF
  global C1 C2 C3 C4 BG1 BG2 BG3 BG4 COLOR
  global L1_SURF L2_SURF L3_SURF
  global LX_BOX LY_BOX LZ_BOX
  global W_sites_graphik
  global TXTT TXTT0 NT ZT
  global XCRYSMODE DIMENSION ZRANGE_TYPE
  global RBASX, RBASY, RBASZ
  global color_scheme_rasmol_Jmol TAB_Jmol_CPK_colors visualizer DIMENSION

  set PRC "sites_graphik" 

  if {![info exists visualizer]}  {set visualizer rasmol}
  if {![info exists Krasmol]}     {set Krasmol 1}
  if {$Krasmol<1 || $Krasmol>4 }  {set Krasmol 1} 
  if {$DIMENSION == "3D" } {      set Krasmol 1 }  
  if {![info exists IQCNTRSURF]}  {set IQCNTRSURF 1}
  if {![info exists IQCNTR]}      {set IQCNTR  1}
  if {![info exists CLURAD]}      {set CLURAD  0}
  if {![info exists NSHLCLU]}     {set NSHLCLU 2}
  if {![info exists LX_BOX]}      {set LX_BOX 1.0}
  if {![info exists LY_BOX]}      {set LY_BOX 1.0}
  if {![info exists LZ_BOX]}      {set LZ_BOX 1.0}
  if {![info exists L1_SURF]}     {set L1_SURF 1.0}
  if {![info exists L2_SURF]}     {set L2_SURF 4.0}
  if {![info exists L3_SURF]}     {set L3_SURF 4.0}
  if {![info exists l_SURF]}      {set l_SURF    1}




#-------------------- color scheme for atoms used by rasmol and Jmol
#                     color_scheme_rasmol_Jmol = "temperature" or user (=Jmol scheme)
  if {![info exists color_scheme]}      {set color_scheme_rasmol_Jmol  user}

  if {![info exists TAB_Jmol_CPK_colors]} {table_Jmol_CPK_colors}

#---------------------------------------------------- 
# presently unused and code always inlined
proc select_Krasmol {k dummy} {

    global C1 C2 C3 C4 BG1 BG2 BG3 BG4 Krasmol

    set PRC "select_Krasmol" 

    set Krasmol $k
    $C1.button configure -bg lightgrey
    $C2.button configure -bg lightgrey
    $C3.button configure -bg lightgrey
    $C4.button configure -bg lightgrey
    if {$Krasmol==1} {$C1.button configure -bg $BG1}
    if {$Krasmol==2} {$C2.button configure -bg $BG2}
    if {$Krasmol==3} {$C3.button configure -bg $BG3}
    if {$Krasmol==4} {$C4.button configure -bg $BG4}
}

#----------------------------------------------------

  writescr0 .d.tt "\nINFO from <run_visualizer>\n\n"

  set win .win_sites_graphik
  set W_sites_graphik $win
  
  toplevel_init $win "run visualizer" 0 0
  wm positionfrom $win user
  wm sizefrom $win user
  wm minsize $win 100 100
  #wm geometry $win  +400+300

  frame $win.cols 

  set C1 $win.cols.col1
  set C2 $win.cols.col2
  set C3 $win.cols.col3
  set C4 $win.cols.col4
  set BG1  LightSalmon1
  set BG2  SteelBlue1  
  set BG3  cornsilk1   
  set BG4  snow
  frame $C1 -relief raised -borderwidth 2 -bg $BG1
  frame $C2 -relief raised -borderwidth 2 -bg $BG2
  frame $C3 -relief raised -borderwidth 2 -bg $BG3
  frame $C4 -relief raised -borderwidth 2 -bg $BG4

  pack $win.cols -side top -fill x -expand y
  if {$DIMENSION == "3D" } {
    pack $C1 $C2 $C3 $C4  -side left -anchor n -fill both -expand y
  } else {
    pack $C1 $C2 $C3  -side left -anchor n -fill both -expand y
  }

# switch Krasmol mode (invoke header button) if scale widget in different section
# is used (has to be in proc because of appended scaleval)
#
# for some odd reason -command is executed on scales with parameter -resolution XXX used  
# if mousepointer moves over the scale without doing a change of the value 
# 
# long known tk bug (based on compiler optimisation problems)
# "scale widget triggering on enter/leave events (LINUX only)"
# https://sourceforge.net/tracker/?func=detail&atid=112997&aid=779559&group_id=12997
#

proc ensure_Krasmol_mode {Krasmol_tobe button {scaleval dummy}} {
    
    global Krasmol
 
    #puts "DBG> $Krasmol - $Krasmol_tobe"
    
    if {$Krasmol != $Krasmol_tobe} {
	$button invoke
    }
    
}

#==============================================================================
#111111111  
  button $C1.button -text {unit cell repeated along primitive axes} -bg $BG1 \
	  -height 2 -borderwidth 4 \
	  -command "set Krasmol 1 ; $C1.button configure -bg $BG1 ; \
	  $C2.button configure -bg lightgrey ; \
	  $C3.button configure -bg lightgrey ; \
	  $C4.button configure -bg lightgrey"

  frame $C1.frame11 -borderwidth 2 -height 30 -width 30 -bg $BG1
  label $C1.frame11.label5 -text {N1} -bg $BG1
  scale $C1.frame11.scale6 -from 1.0 -orient horizontal \
	  -sliderlength 40 -to 25.0 -variable N1 -bg $BG1 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 1 $C1.button]

  frame $C1.frame22 -borderwidth 2 -height 30 -width 30 -bg $BG1
  label $C1.frame22.label5 -text {N2} -bg $BG1
  scale $C1.frame22.scale6 -from 1.0 -orient horizontal \
	  -sliderlength 40 -to 25.0 -variable N2 -bg $BG1 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 1 $C1.button]

  frame $C1.frame23 -borderwidth 2 -height 30 -width 30 -bg $BG1
  label $C1.frame23.label5 -text {N3} -bg $BG1
  scale $C1.frame23.scale6 -from 1.0 -orient horizontal \
	  -sliderlength 40 -to 25.0 -variable N3 -bg $BG1 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 1 $C1.button]

  pack configure $C1.frame11.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C1.frame11.label5 -expand 1 -fill x -side right

  pack configure $C1.frame22.scale6 -anchor w -expand 1  -fill x -side right
  pack configure $C1.frame22.label5 -expand 1 -fill x -side right

  pack configure $C1.frame23.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C1.frame23.label5 -expand 1 -fill x -side right

  pack configure $C1.button  -fill x -anchor n -side top
  pack configure $C1.frame11 -fill x -anchor n -side top
##  || ($DIMENSION == "2D" && $ZRANGE_TYPE=="extended")
  if {$DIMENSION == "3D"} { 
    pack configure $C1.frame22 -fill x -anchor n -side top 
    pack configure $C1.frame23 -fill x -anchor n -side top -expand 1 
  } else {
    pack configure $C1.frame22 -fill x -anchor n -side top -expand 1 
  }

#==============================================================================

  set C2_fr 0.1
  set C2_to 25.0
# 222222222222
  button $C2.button -text {unit cell repeated within rectangular box} \
	  -bg $BG2 -height 2 -borderwidth 4 \
	  -command "select_rectangular_box $C1 $C2 $C3 $C4 $BG2"

#--------------------------------------------------------------------------
  proc select_rectangular_box {C1 C2 C3 C4 BG2} {
      global Krasmol LX_BOX LY_BOX LZ_BOX

      set Krasmol 2
      $C1.button configure -bg lightgrey
      $C2.button configure -bg $BG2
      $C3.button configure -bg lightgrey
      $C4.button configure -bg lightgrey
      
      if {$LX_BOX<=0.11 && $LY_BOX<=0.11 && $LZ_BOX<=0.1 } {
	  set LX_BOX 1.0
	  set LY_BOX 1.0
	  set LZ_BOX 1.0
      }
  }
#--------------------------------------------------------------------------

  # LX_BOX
  frame $C2.frame11 -borderwidth 2 -height 30 -width 30 -bg $BG2
  label $C2.frame11.label5 -text {LX} -bg $BG2
  scale $C2.frame11.scale6 -from $C2_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C2_to -variable LX_BOX -bg $BG2 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 2 $C2.button]
  #if {$LX_BOX!="$C2_fr"} {$C2.frame11.scale6 set $LX_BOX}
  
  # LY_BOX
  frame $C2.frame22 -borderwidth 2 -height 30 -width 30 -bg $BG2
  label $C2.frame22.label5 -text {LY} -bg $BG2
  scale $C2.frame22.scale6 -from $C2_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C2_to -variable LY_BOX -bg $BG2 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 2 $C2.button]
  #if {$LY_BOX!=""} {$C2.frame22.scale6 set $LY_BOX}

  # LZ_BOX
  frame $C2.frame23 -borderwidth 2 -height 30 -width 30 -bg $BG2
  label $C2.frame23.label5 -text {LZ} -bg $BG2
  scale $C2.frame23.scale6 -from $C2_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C2_to -variable LZ_BOX -bg $BG2 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 2 $C2.button]
  #if {$LZ_BOX!=""} {$C2.frame23.scale6 set $LZ_BOX}

  pack configure $C2.frame11.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C2.frame11.label5 -expand 1 -fill x -side right

  pack configure $C2.frame22.scale6 -anchor w -expand 1  -fill x -side right
  pack configure $C2.frame22.label5 -expand 1 -fill x -side right

  pack configure $C2.frame23.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C2.frame23.label5 -expand 1 -fill x -side right

  pack configure $C2.button  -fill x -anchor n
  pack configure $C2.frame11 -fill x -anchor n
##  || ($DIMENSION == "2D" && $ZRANGE_TYPE=="extended")
  if {$DIMENSION == "3D" } { 
    pack configure $C2.frame22 -fill x -anchor n
    pack configure $C2.frame23 -fill x -anchor n -expand 1 
  } else {
    pack configure $C2.frame22 -fill x -anchor n -expand 1
    set LZ_BOX $RBASZ(3)
  }

#==============================================================================
#333333333333
  button $C3.button -text {cluster around selected atomic site} \
	  -bg $BG3 -height 2 -borderwidth 4 \
	  -command "set Krasmol 3 ; \
	  $C1.button configure -bg lightgrey ; \
	  $C2.button configure -bg lightgrey ; \
	  $C3.button configure -bg $BG3 ; \
	  $C4.button configure -bg lightgrey"

  # IQCNTR
  frame $C3.frame11 -borderwidth 2 -height 30 -width 30 -bg $BG3
  label $C3.frame11.label5 -text {IQCNTR} -bg $BG3 -width 5
  scale $C3.frame11.scale6 -from 1 -orient horizontal \
	  -sliderlength 40 -to $NQ -variable IQCNTR -bg $BG3 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 3 $C3.button]

  # NSHLCLU
  frame $C3.frame22 -borderwidth 2 -height 30 -width 30 -bg $BG3
  label $C3.frame22.label5 -text {NSHLCLU} -bg $BG3 -width 5
  scale $C3.frame22.scale6 -from 0 -orient horizontal \
    -sliderlength 40 -to 10 -variable NSHLCLU -bg $BG3 \
    -highlightthickness 0 -command "reset_CLURAD ; ensure_Krasmol_mode 3 $C3.button "
  proc reset_CLURAD {{x dummy}} {global CLURAD ; set CLURAD 0.0 }

  # CLURAD
  frame $C3.frame23 -borderwidth 2 -height 30 -width 30 -bg $BG3
  label $C3.frame23.label5 -text {CLURAD} -bg $BG3 -width 5
  scale $C3.frame23.scale6 -from 0 -orient horizontal -resolution 0.1 \
    -sliderlength 40 -to 25.0 -variable CLURAD -bg $BG3 \
    -highlightthickness 0 -command "reset_NSHLCLU ; ensure_Krasmol_mode 3 $C3.button "

  proc reset_NSHLCLU {{x dummy}} {
      global NSHLCLU CLURAD
      if {$CLURAD==0.0} {
          set NSHLCLU 2
      } else {
          set NSHLCLU 0 
      }
  }
 
  pack configure $C3.frame11.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C3.frame11.label5 -expand 1 -fill x -side right

  pack configure $C3.frame22.scale6 -anchor w -expand 1  -fill x -side right
  pack configure $C3.frame22.label5 -expand 1 -fill x -side right

  pack configure $C3.frame23.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C3.frame23.label5 -expand 1 -fill x -side right

  pack configure $C3.button  -fill x -anchor n
  pack configure $C3.frame11 -fill x -anchor n
  pack configure $C3.frame22 -fill x -anchor n
  pack configure $C3.frame23 -fill x -anchor n -expand 1

  if {$NSHLCLU==0 && $CLURAD==0.0} {set NSHLCLU 2}

#==============================================================================
  
  set C4_fr 0.1
  set C4_to 25.0
#444444444444
  button $C4.button -text "surface including selected atomic site" \
	  -bg $BG4 -height 2 -borderwidth 4 \
          -command "select_surf $C1 $C2 $C3 $C4 $BG4"
  
#--------------------------------------------------------------------------
  proc select_surf {C1 C2 C3 C4 BG4} {
      global Krasmol L1_SURF L2_SURF L3_SURF

      set Krasmol 4 
      $C1.button configure -bg lightgrey
      $C2.button configure -bg lightgrey
      $C3.button configure -bg lightgrey
      $C4.button configure -bg $BG4
      
      if {$L1_SURF<=0.11 && $L2_SURF<=0.11 && $L3_SURF<=0.1 } {
	  set L1_SURF 1.0
	  set L2_SURF 3.0
	  set L3_SURF 4.0
      }
  }
#--------------------------------------------------------------------------

  # IQ_SURF
  frame $C4.frame10 -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frame10.label5 -text {IQSURF} -bg $BG4 -width 5
  scale $C4.frame10.scale6 -from 1 -orient horizontal \
	  -sliderlength 40 -to $NQ -variable IQ_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]

  # h_SURF
  frame $C4.frameH -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frameH.label5 -text "h" -bg $BG4 -width 5
  scale $C4.frameH.scale6 -from 0 -orient horizontal \
	  -sliderlength 40 -to 6 -variable h_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$h_SURF!=""} {$C4.frameH.scale6 set $h_SURF}

  # k_SURF
  frame $C4.frameK -borderwidth 2 -height 30 -width 30 -bg $BG4 
  label $C4.frameK.label5 -text "k" -bg $BG4 -width 5
  scale $C4.frameK.scale6 -from 0 -orient horizontal \
    -sliderlength 40 -to 6 -variable k_SURF -bg $BG4 \
    -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$k_SURF!=""} {$C4.frameK.scale6 set $k_SURF}

  # l_SURF
  frame $C4.frameL -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frameL.label5 -text "l" -bg $BG4 -width 5
  scale $C4.frameL.scale6 -from 0 -orient horizontal \
	  -sliderlength 40 -to 6 -variable l_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$l_SURF!=""} {$C4.frameL.scale6 set $l_SURF}

  # L1_SURF
  frame $C4.frame11 -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frame11.label5 -text "L1" -bg $BG4
  scale $C4.frame11.scale6 -from $C4_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C4_to -variable L1_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$L1_SURF!=""} {$C4.frame11.scale6 set $L1_SURF}

  # L2_SURF
  frame $C4.frame22 -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frame22.label5 -text "L2" -bg $BG4
  scale $C4.frame22.scale6 -from $C4_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C4_to -variable L2_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$L2_SURF!=""} {$C4.frame22.scale6 set $L2_SURF}

  # L3_SURF
  frame $C4.frame23 -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frame23.label5 -text "L3" -bg $BG4
  scale $C4.frame23.scale6 -from $C4_fr -orient horizontal -resolution 0.1 \
	  -sliderlength 40 -to $C4_to -variable L3_SURF -bg $BG4 \
	  -highlightthickness 0 -command [list ensure_Krasmol_mode 4 $C4.button]
  #if {$L3_SURF!=""} {$C4.frame23.scale6 set $L3_SURF}

  # o_SURF
  frame $C4.frameO -borderwidth 2 -height 30 -width 30 -bg $BG4
  label $C4.frameO.label5 -text "orient" -bg $BG4
  radiobutton $C4.frameO.rad1 -variable o_SURF -text "down" \
	  -value down -bg $BG4 -highlightthickness 0 \
	  -command [list ensure_Krasmol_mode 4 $C4.button]
  radiobutton $C4.frameO.rad2 -variable o_SURF -text "up"  \
	  -value up   -bg $BG4 -highlightthickness 0 \
	  -command [list ensure_Krasmol_mode 4 $C4.button]
  $C4.frameO.rad2 select
  
  pack configure $C4.frameH.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frameH.label5 -expand 1 -fill x -side right

  pack configure $C4.frameK.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frameK.label5 -expand 1 -fill x -side right

  pack configure $C4.frameL.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frameL.label5 -expand 1 -fill x -side right

  pack configure $C4.frame10.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frame10.label5 -expand 1 -fill x -side right

  pack configure $C4.frame11.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frame11.label5 -expand 1 -fill x -side right

  pack configure $C4.frame22.scale6 -anchor w -expand 1  -fill x -side right
  pack configure $C4.frame22.label5 -expand 1 -fill x -side right

  pack configure $C4.frame23.scale6 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frame23.label5 -expand 1 -fill x -side right

  pack configure $C4.frameO.rad1 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frameO.rad2 -anchor w -expand 1 -fill x -side right
  pack configure $C4.frameO.label5 -expand 1 -fill x -side right

  pack configure $C4.button -expand 1 -fill x -anchor n
  pack configure $C4.frame10 -fill x
  pack configure $C4.frameH  -fill x
  pack configure $C4.frameK  -fill x
  pack configure $C4.frameL  -fill x
  pack configure $C4.frame11 -fill x
  pack configure $C4.frame22 -fill x
  pack configure $C4.frame23 -fill x
  pack configure $C4.frameO  -fill x

  # color header button according to Krasmol
  $C1.button configure -bg lightgrey
  $C2.button configure -bg lightgrey
  $C3.button configure -bg lightgrey
  $C4.button configure -bg lightgrey
  if {$Krasmol==1} {$C1.button configure -bg $BG1}
  if {$Krasmol==2} {$C2.button configure -bg $BG2}
  if {$Krasmol==3} {$C3.button configure -bg $BG3}
  if {$Krasmol==4} {$C4.button configure -bg $BG4}

  if {$L1_SURF<=0.11 && $L2_SURF<=0.11 && $L3_SURF<=0.1 } {
      set L1_SURF 1.0
      set L2_SURF 4.0
      set L3_SURF 4.0
  }

#==============================================================================

  label $win.patience -text " " -fg black -relief flat -height 3
  pack  $win.patience -expand y -fill x

#==============================================================================

  # seperate proc because they are also used in create system 0D panel
  build_visualizer_parameter_controls $win

# XcrySDen test button
# button $win.xcrysden -bg green1 -height 3 -padx 9 -pady 3 \
#	  -text "run XcrySDen (set up of input may take a while)" \
#	  -command "xcry_start"
 
  # vars are set by controls so they are anyhow in global scope
  button $win.button25 -bg green1 -height 3 -padx 9 -pady 3 \
	  -text "run visualizer (set up of input may take a while)" \
	  -command {run_visualizer $visualizer $Krasmol \
	  $N1 $N2 $N3 \
	  $LX_BOX $LY_BOX $LZ_BOX \
	  $IQCNTR $CLURAD $NSHLCLU \
	  $IQ_SURF $h_SURF $k_SURF $l_SURF $L1_SURF $L2_SURF $L3_SURF $o_SURF \
	  $show_site_labels $show_unit_cell $show_cube $show_axis $show_empty_spheres \
	  $sphere_color} \
	  
  # DON't  destroy .win_sites_graphik
  #-----------------------------------------------------------------
  proc ask_for_patience {win} {
      $win.button25 configure -bg yellow \
	      -text "please have a little patience \n\n
      arithmetics on tcl-level may take its time "
  }
  #-----------------------------------------------------------------
  #


  button $win.button26 -bg $COLOR(CLOSE) -height 3 -padx 9 -pady 3 -text "CLOSE" \
	  -command {destroy .win_sites_graphik}


#  pack configure $win.xcrysden -expand 1 -fill x
  pack configure $win.button25 -expand 1 -fill x 
  pack configure $win.button26 -expand 1 -fill x

  for {set IT 1} {$IT <= $NT} {incr IT} {
      puts "XXX aaaa $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
  }

}  ; # end sites_graphik


#
# 
#
proc build_visualizer_parameter_controls {win {packingstyle wide}} {
    
    global XCRYSMODE sysfile 
    global visualizer RASRAD RASSCL sphere_color
    global show_site_labels show_unit_cell show_cube show_axis show_empty_spheres
    
    # choose type of arranging the widgets
    switch $packingstyle {
	
	compact - 
	narrow {
	    set compactstyle 1
	}
	wide -
	default {
	    set compactstyle 0
	}
	
    }
    
    puts "compactstyle: $compactstyle"

    # more intuitive fuction/shortcut/alternative for [expr {$bool ? a : b}]
    proc iff {bool a b} { 
	if {$bool} { 
	    return $a 
	} else { 
	    return $b 
	}
    }
    
    # enable/disable controls according to visualizer
    set vizcontrols [list \
	    $win.control.show_site_labels \
	    $win.control.show_unit_cell \
	    $win.control.show_cube \
	    $win.control.show_axis \
	    $win.control.show_empty_spheres \
	    $win.frame24.label5 \
	    $win.frame24.scale6 \
	    $win.frame25.label5 \
	    $win.frame25.scale6 \
	    ]
    # enable/disable controls in vizcontrols according to index in on/off
    proc vizcontrols_state {vizcontrols on off} {
    
	foreach i $on {
	    eval [lindex $vizcontrols $i] configure -state normal
	}
	
	foreach i $off { 
	    eval [lindex $vizcontrols $i] configure -state disabled
	}
	
    }

    proc xcry_start {} { global ALAT RBASX RBASY RBASZ NQ RQX RQY RQZ ZT ITOQ LATPAR sysfile Krasmol DIMENSION
                         global show_site_labels show_axis show_unit_cell show_empty_spheres N1 N2 N3 
                         set au2A 0.5291772
	if {$Krasmol == 1 && $DIMENSION == "3D"} {
                         set Datei [open ${sysfile}_xband.xsf w+]
	                 puts $Datei "CRYSTAL"
                         puts $Datei " PRIMVEC"
	                 puts $Datei "   [expr $au2A * $ALAT * $RBASX(1)] [expr $au2A * $ALAT * $RBASY(1)] [expr $au2A * $ALAT * $RBASZ(1)]"        	        
                         puts $Datei "   [expr $au2A * $ALAT * $RBASX(2)] [expr $au2A * $ALAT * $RBASY(2)] [expr $au2A * $ALAT * $RBASZ(2)]"              
 	                 puts $Datei "   [expr $au2A * $ALAT * $RBASX(3)] [expr $au2A * $ALAT * $RBASY(3)] [expr $au2A * $ALAT * $RBASZ(3)]"                       
                         puts $Datei " CONVVEC"
	                 puts $Datei "[expr $au2A * $LATPAR(1)] 0.0 0.0"
	                 puts $Datei "   0.0 [expr $au2A * $LATPAR(2)] 0.0"
	                 puts $Datei "   0.0 0.0 [expr $au2A * $LATPAR(3)]" 
                         puts $Datei " PRIMCOORD"
	                 if {$show_empty_spheres == 0} {
                                                        set i 1
                                                        set a 0
        	                                         while {$i <= $NQ} {
		 			                                    if {$ZT($i) == 0} {set a [expr $a +1]}
                                                                           set i [expr $i +1]               
                                                                           }    
		                                          puts $Datei " [expr $NQ - $a] 1"                                           
			                                                   } else {puts $Datei " $NQ 1"}                        
                           set i 1                                   
	                   while {$i <= $NQ} {                                              
			                      set Z $ITOQ(1,$i)
			                      if { $ZT($Z) != 0} {
			                                          puts $Datei "   $ZT($Z)   [expr $au2A * $ALAT *$RQX($i)]  [expr $au2A * $ALAT *$RQY($i)]  [expr $au2A * $ALAT *$RQZ($i)]" 
		                                                  set i [expr $i +1]
			                                         }			                                                
			                     
                                              if { $ZT($Z) == 0} {
				                 if {$show_empty_spheres == 1} {
			                                                        puts $Datei "   $ZT($Z)   [expr $au2A * $ALAT *$RQX($i)]  [expr $au2A * $ALAT *$RQY($i)]  [expr $au2A * $ALAT *$RQZ($i)]" 
                                                                                set i [expr $i +1]
				                                               } else { break }
			                                          } 
			                      }                                                                          
                         close $Datei
                         set Datei ${sysfile}_xband.xsf                        
#	                 exec "xcrysden" "--xsf $Datei"
                                             
                         set Datei2 [open ${sysfile}_xband.xcrysden w+]
	                 puts $Datei2 "set xsfStructure \{"
                         exec cat < $Datei >@ $Datei2
	                 puts $Datei2 "\}"
                         puts $Datei2 ""

	                 puts $Datei2 {WriteFile $system(SCRDIR)/xc_xsf.$system(PID) $xsfStructure w}
	                 puts $Datei2 {::scripting::open --xsf $system(SCRDIR)/xc_xsf.$system(PID)}
                         puts $Datei2 ""
	                 puts $Datei2 {set wait_window [DisplayUpdateWidget "Reconstructing" "Reconstructing the structure and display parameters. Please wait"]}
                         puts $Datei2 "SetWatchCursor"
                         puts $Datei2 "set xcCursor(dont_update) 1"
                         puts $Datei2 ""
                         if {$show_unit_cell == 1} {
			         puts $Datei2 {array set check {pseudoDens 0 perspective 0 labels 1 depthcuing 0 crds 1 wigner 0 antialias 0 Hbonds 0 forces 0 frames 1 unibond 0 perpective 0}}
			 } else {puts $Datei2 {array set check {pseudoDens 0 perspective 0 labels 1 depthcuing 0 crds 1 wigner 0 antialias 0 Hbonds 0 forces 0 frames 0 unibond 0 perpective 0}}}
                         if {$show_axis == 1} {
                                               puts $Datei2 "CrdSist"
			                      }                         
	                 if {$show_site_labels == 1} {                          
	                                              puts $Datei2 "AtomLabels"
			                             } 

                         if {$show_unit_cell == 1} {
                                               puts $Datei2 "CrysFrames"
			                      }
                         puts $Datei2 ""
                         puts $Datei2 "set xcCursor(dont_update) 0"
                         puts $Datei2 "ResetCursor"
	                 puts $Datei2 {destroy $wait_window}
                         puts $Datei2 ""
                         puts $Datei2 "set nxdir $N1"
                         puts $Datei2 "set nydir $N2"
                         puts $Datei2 "set nzdir $N3"
                         puts $Datei2 ""
                         puts $Datei2 "CellMode 1"
                         close $Datei2
                         set Datei2 ${sysfile}_xband.xcrysden
                         eval exec "xterm -e xcrysden -s $Datei2 &"
#	                 exec "xcrysden" "-s $Datei2 1>&/dev/null"
	                 } else {
			          set pdb [open ${sysfile}_xband.xcrysden w+]
	                          puts $pdb "set xsfStructure \{"
                                  puts $pdb "ATOMS"
			          exec grep ATOM ${sysfile}_xband.pdb | awk {{print $15,$4,$5,$6}} >@ $pdb
	                          puts $pdb "\}"
                                  puts $pdb ""
                                  puts $pdb {WriteFile $system(SCRDIR)/xc_xsf.$system(PID) $xsfStructure w}
	                          puts $pdb {::scripting::open --xsf $system(SCRDIR)/xc_xsf.$system(PID)}
                                  puts $pdb ""
	                          puts $pdb {set wait_window [DisplayUpdateWidget "Reconstructing" "Reconstructing the structure and display parameters. Please wait"]}
                                  puts $pdb "SetWatchCursor"
                                  puts $pdb "set xcCursor(dont_update) 1"
                                  puts $pdb ""
                                  puts $pdb {array set check {pseudoDens 0 perspective 0 labels 1 depthcuing 0 crds 1 wigner 0 antialias 0 Hbonds 0 forces 0 frames 1 unibond 0 perpective 0}}
                                  if {$show_axis == 1} {
                                                        puts $pdb "CrdSist"
			                               }                         
	                          if {$show_site_labels == 1} {                          
	                                                       puts $pdb "AtomLabels"
			                                      } 
                                  puts $pdb ""
                                  puts $pdb "set xcCursor(dont_update) 0"
                                  puts $pdb "ResetCursor"
	                          puts $pdb {destroy $wait_window}
			          close $pdb
                                  set pdb ${sysfile}_xband.xcrysden
			          if {$show_empty_spheres == 0} {
				                                exec grep -v ^0 $pdb  > zzzzzz_dat 
                                                                exec mv  zzzzzz_dat ${sysfile}_xband.xcrysden
			                                        }         
                                  eval exec "xterm -e xcrysden -s $pdb &"                                 
                                 }      
	           
                }     
                       


    # hide choice of visualizers if in XCRYSMODE
    if {$XCRYSMODE} {
	global visualizer   ; # -command of widgets is in global scope
	set visualizer "rasmol"   ; # force rasmol
    } else {
	# choose visualizer
	set frame [frame $win.viz -borderwidth 2 -height 30 -width 30]
	
	label $frame.vizlabel -text "Visualizer:  "

	radiobutton $frame.xcrysden -variable visualizer -text "XCrySDen" \
		-value "xcrysden" -highlightthickness 0 \
	        -command ""
 
	radiobutton $frame.rasmol -variable visualizer -text "rasmol" \
		-value "rasmol" -highlightthickness 0 \
		-command [list vizcontrols_state $vizcontrols [list 0 1 2 3 4 5 6 7 8] [list]]
	
	radiobutton $frame.jmol -variable visualizer -text "Jmol" \
		-value "jmol" -highlightthickness 0 \
		-command [list vizcontrols_state $vizcontrols [list 0 1 2 3 4 5 6 7 8] [list]]
	
	radiobutton $frame.dataexplorer -variable visualizer -text "dataexplorer" \
		-value "dataexplorer"  -highlightthickness 0 \
		-command [list vizcontrols_state $vizcontrols [list 1 2 4 7 8] [list 0 3 5 6]]
	
        if {![info exists visualizer]} {$frame.rasmol select}

	eval pack [winfo children $frame] -side left -anchor w

	pack $frame 
    }     

    frame $win.frame24 -borderwidth 2 -height 30 ; # -width 30

    label $win.frame24.label5 -width 20 -text "scale Wigner-Seitz radius" -anchor e 

    scale $win.frame24.scale6 -from 0.0 -orient horizontal -resolution 0.05 \
	    -digits 3 -to 1.01 -showvalue true -variable RASRAD \
	    -length [iff $compactstyle 100 300] 
    
    frame $win.frame25 -borderwidth 2 -height 30 ; # -width 30
    
    label $win.frame25.label5 -width 20 -text "scale object" -anchor e

    scale $win.frame25.scale6 -from 0 -orient horizontal -resolution 0.25 -digits 3 \
	    -to 10 -variable RASSCL -showvalue true -highlightthickness 0 \
	    -length [iff $compactstyle 100 300] 
    
    frame $win.control -height 4 

    checkbutton $win.control.show_site_labels -anchor nw -variable show_site_labels \
	    -text [iff $compactstyle "site labels" "show site labels"]

    checkbutton $win.control.show_unit_cell -anchor nw -variable show_unit_cell \
	    -text [iff $compactstyle "unit cell" "show unit cell"]
    
    checkbutton $win.control.show_cube -anchor nw -variable show_cube \
	    -text [iff $compactstyle "cube" "show cube"]
    
    checkbutton $win.control.show_axis -anchor nw -variable show_axis \
	    -text [iff $compactstyle "axis" "show axis"]

    checkbutton $win.control.show_empty_spheres -anchor nw -variable show_empty_spheres \
	    -text [iff $compactstyle "empty spheres" "show empty spheres"]

    frame $win.frame_color -borderwidth 2 -height 30 -width 30
    radiobutton $win.frame_color.rad1 -variable sphere_color -text "atom type" \
	    -value IT -highlightthickness 0
    radiobutton $win.frame_color.rad2 -variable sphere_color -text "atomic number"  \
	    -value Z  -highlightthickness 0
    $win.frame_color.rad1 select
    label $win.frame_color.label5 -text "           color of atomic spheres"
    pack configure $win.frame_color.rad1 -anchor w -expand 1 -fill x -side right
    pack configure $win.frame_color.rad2 -anchor w -expand 1 -fill x -side right
    pack configure $win.frame_color.label5 -expand 1 -fill x -side right

    pack configure $win.frame24.label5 -expand 1 -fill x -side left
    pack configure $win.frame24.scale6 -anchor s -expand 1 -fill x -side left -padx 50

    pack configure $win.frame25.label5 -expand 1 -fill x -side left
    pack configure $win.frame25.scale6 -anchor s -expand 1 -fill x -side left -padx 50

    pack configure $win.frame24 -fill x
    pack configure $win.frame25 -fill x
    pack configure $win.control 
    pack configure $win.control.show_site_labels   -side left 
    pack configure $win.control.show_unit_cell     -side left 
    pack configure $win.control.show_cube          -side left 
    pack configure $win.control.show_axis          -side left 
    pack configure $win.control.show_empty_spheres -side left 

    if {$compactstyle} {
	pack $win.frame_color -side top
    } else {
	pack $win.frame_color -side right -in $win.control 
    }
    
    # RASRAD
    if {$RASRAD==0} {$win.frame24.scale6 set 0.4}		 
    # RASSCL
    if {$RASSCL==0} {$win.frame25.scale6 set 1}

    $win.control.show_unit_cell     select
    $win.control.show_cube          select
    $win.control.show_axis          select
    $win.control.show_empty_spheres select
    
}

########################################################################################
# Krasmol modes 1, 2, 3, 4
#
# creates entries with push_systeminfo_atom
#
proc add_Krasmol_systeminfo {Iatom Krasmol N1 N2 N3 \
	LX_BOX LY_BOX LZ_BOX \
	IQCNTR CLURAD NSHLCLU \
	IQ_SURF h_SURF k_SURF l_SURF L1_SURF L2_SURF L3_SURF o_SURF \
	show_empty_spheres sphere_color} {
    
    global LATPAR LATANG BOA SPACEGROUP BRAVAIS
    global NQ RQX RQY RQZ NQCL NCL ITOQ RBASX RBASY RBASZ COA BOA
    global ZT TXTT TXTT0 ALAT NT NAT NOQ IQAT RWS
    
    set PRC [myname] 
    
    for {set IT 1} {$IT <= $NT} {incr IT} {
	puts "XXX AAAA $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
    }
    
    for {set IT 1} {$IT <= $NT} {incr IT} { 
	set NAT($IT) 0 
    }
    for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
	for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
	    set IT $ITOQ($IA,$IQ)
	    incr NAT($IT)
	    set IQAT($NAT($IT),$IT) $IQ
	}
    }
    
    for {set IT 1} {$IT <= $NT} {incr IT} {
	puts "XXX BBBB $IT $ZT($IT) $TXTT($IT)  $TXTT0($IT) "
    }
    
    set TOL 0.0001
    set BOT [expr -$TOL]

    #
    #---------------------------------------- set up quantities for expansion
    #
    for {set B1 1} {$B1 <= 3} {incr B1} {
	for {set B2 1} {$B2 <= 3} {incr B2} {
	    set aa($B1,$B2) [expr $RBASX($B1)*$RBASX($B2)\
		    +$RBASY($B1)*$RBASY($B2)\
		    +$RBASZ($B1)*$RBASZ($B2)]
	}
    }

    set det [expr $aa(1,1)*$aa(2,2)*$aa(3,3) +$aa(1,2)*$aa(2,3)*$aa(3,1) \
	    +$aa(1,3)*$aa(2,1)*$aa(3,2) -$aa(1,1)*$aa(2,3)*$aa(3,2) \
	    -$aa(1,2)*$aa(2,1)*$aa(3,3) -$aa(1,3)*$aa(2,2)*$aa(3,1) ]
    #-------------------------------------------------------------------------

    
#111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
    if {$Krasmol==1} {
	set I1MIN 0
	set I2MIN 0
	set I3MIN 0 
	set I1MAX [expr {$N1-1}] 
	set I2MAX [expr {$N2-1}]
	set I3MAX [expr {$N3-1}]
    }
#222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222
    if {$Krasmol==2} {
  
	set I1MAX 2
	set I2MAX 2					     
	set I3MAX 2					     
	#
	#----------------------------------- find expansion coefficients  V = sum(i) p_i * RBAS_i 
	#
	for {set I1 0} {$I1 <= 1} {incr I1} {
	    for {set I2 0} {$I2 <= 1} {incr I2} {
		for {set I3 0} {$I3 <= 1} {incr I3} {
  
		    set VX [expr {$I1*$LX_BOX}]
		    set VY [expr {$I2*$LY_BOX}]
		    set VZ [expr {$I3*$LZ_BOX}]
  
		    for {set I 1} {$I <= 3} {incr I} {  
			set bb($I) [expr {$VX*$RBASX($I)+$VY*$RBASY($I)+$VZ*$RBASZ($I)}] 
		    }
  
		    set dbb(1) [expr $bb(1)  *$aa(2,2)*$aa(3,3) +$aa(1,2)*$aa(2,3)*$bb(3)   +$aa(1,3)*$bb(2)  *$aa(3,2) \
			    -$bb(1)  *$aa(2,3)*$aa(3,2) -$aa(1,2)*$bb(2)  *$aa(3,3) -$aa(1,3)*$aa(2,2)*$bb(3)  ]
		    
		    set dbb(2) [expr $aa(1,1)*$bb(2)  *$aa(3,3) +$bb(1)  *$aa(2,3)*$aa(3,1) +$aa(1,3)*$aa(2,1)*$bb(3)   \
			    -$aa(1,1)*$aa(2,3)*$bb(3)   -$bb(1)  *$aa(2,1)*$aa(3,3) -$aa(1,3)*$bb(2)  *$aa(3,1) ]
		    
		    set dbb(3) [expr $aa(1,1)*$aa(2,2)*$bb(3)   +$aa(1,2)*$bb(2)  *$aa(3,1) +$bb(1)  *$aa(2,1)*$aa(3,2) \
			    -$aa(1,1)*$bb(2)  *$aa(3,2) -$aa(1,2)*$aa(2,1)*$bb(3)   -$bb(1)  *$aa(2,2)*$aa(3,1) ]
		    
		    for {set I 1} {$I <= 3} {incr I} {  
			set pp($I) [expr int(abs( $dbb($I) / $det )) + 2 ]      
		    }
		    
		    set TXT [format "I %3i%3i%3i  ->  V %10.6f%10.6f%10.6f   p_i:  %3i%3i%3i "\
			    $I1 $I2 $I3 $VX $VY $VZ $pp(1) $pp(2) $pp(3)]
		    debug $PRC $TXT
		    
		    if {$I1MAX<$pp(1)} {set I1MAX $pp(1)}
		    if {$I2MAX<$pp(2)} {set I2MAX $pp(2)}
		    if {$I3MAX<$pp(3)} {set I3MAX $pp(3)}
		    
		}
	    }
	}
  
	set I1MIN [expr -$I1MAX]
	set I2MIN [expr -$I2MAX]
	set I3MIN [expr -$I3MAX]
	
	set TOPX  [expr {$LX_BOX+$TOL}]
	set TOPY  [expr {$LY_BOX+$TOL}]
	set TOPZ  [expr {$LZ_BOX+$TOL}]
    }
#333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
    if {$Krasmol==3} {
	
	if {$NSHLCLU==0 && $CLURAD<0.0001 } {
	    give_warning "." "WARNING \n\n from <$PRC> \n\n  \
		    \n\n NSHLCLU = 0 \n\n AND \n\n  CLURAD = 0  \n\n specify parameters properly "
	    return -1
	}

	set XCNTR $RQX($IQCNTR)
	set YCNTR $RQY($IQCNTR)
	set ZCNTR $RQZ($IQCNTR)

	if {$NSHLCLU !=0} {
	    set I1MIN [expr -$NSHLCLU-1 ]
	    set I2MIN [expr -$NSHLCLU-1 ]
	    set I3MIN [expr -$NSHLCLU-1 ]
	    set I1MAX [expr  $NSHLCLU+1 ] 
	    set I2MAX [expr  $NSHLCLU+1 ]
	    set I3MAX [expr  $NSHLCLU+1 ]
	    
	    set NDRSQR 0 
	    set DRSQRTAB(0) 0.0
	    
	    for {set I1 $I1MIN} {$I1 <= $I1MAX} {incr I1} {
		for {set I2 $I1MIN} {$I2 <= $I2MAX} {incr I2} {
		    for {set I3 $I1MIN} {$I3 <= $I3MAX} {incr I3} {
			#-----------------------------------------------------
			for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
			    
			    set X [expr $RQX($IQ) + $I1*$RBASX(1) + $I2*$RBASX(2) + $I3*$RBASX(3) ]
			    set Y [expr $RQY($IQ) + $I1*$RBASY(1) + $I2*$RBASY(2) + $I3*$RBASY(3) ]
			    set Z [expr $RQZ($IQ) + $I1*$RBASZ(1) + $I2*$RBASZ(2) + $I3*$RBASZ(3) ]
			    
			    set DRSQR [expr pow($X-$XCNTR,2)+ pow($Y-$YCNTR,2)+ pow($Z-$ZCNTR,2) ]
			    
			    set new 1
			    for {set IDR 0} {$IDR <= $NDRSQR} {incr IDR} {
				if {[expr abs( $DRSQR - $DRSQRTAB($IDR) )] < 0.00001 } { set new 0 }
			    }
			    if {$new == 1} {
				set IDR 0
				while {$DRSQR > $DRSQRTAB($IDR) && $IDR < $NDRSQR} {
				    incr IDR   
				}
				
				if {$DRSQR > $DRSQRTAB($NDRSQR)} {
				    incr NDRSQR
				    set DRSQRTAB($NDRSQR) $DRSQR
				} else {
				    set JDR $NDRSQR
				    for {set I $IDR} {$I <= $NDRSQR} {incr I} {
					set DRSQRTAB([expr {$JDR+1}]) $DRSQRTAB($JDR)
					set JDR 	[expr {$JDR-1}]
				    }
				    incr NDRSQR
				    set DRSQRTAB($IDR) $DRSQR
				}              
			    }
			}
			#-----------------------------------------------------
		    }
		}
	    }
	    
	    if { $NSHLCLU > $NDRSQR} {
		set NSHLCLU $NDRSQR
		give_warning "." "WARNING \n\n from <$PRC> \n\n  \
			\n\n NSHLCLU > NDRSQR = $NDRSQR \\  NSHLCLU will be reduced "
	    }
	    
	    set DRMAXSQR $DRSQRTAB($NSHLCLU)
	    
	} else {
	    
	    set RR1 [expr sqrt( pow($RBASX(1),2)+ pow($RBASY(1),2)+ pow($RBASZ(1),2) ) ]
	    set RR2 [expr sqrt( pow($RBASX(2),2)+ pow($RBASY(2),2)+ pow($RBASZ(2),2) ) ]
	    set RR3 [expr sqrt( pow($RBASX(3),2)+ pow($RBASY(3),2)+ pow($RBASZ(3),2) ) ]
	    
	    set I1MAX [expr int(2 + $RR1/$CLURAD)]
	    set I2MAX [expr int(2 + $RR2/$CLURAD)]
	    set I3MAX [expr int(2 + $RR3/$CLURAD)]
	    
	    set I1MIN [expr -$I1MAX]
	    set I2MIN [expr -$I2MAX]
	    set I3MIN [expr -$I3MAX]
	    set DRMAXSQR [expr {$CLURAD * $CLURAD}]
	    
	}
	
    }
    
    #444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444
    if {$Krasmol==4} {
	
	if {$h_SURF==0 && $k_SURF==0 && $l_SURF==0} {       
	    give_warning "." "WARNING \n\n all Miller indices 0  \n\n modify setting "
	    return -1
	}
	
	# use crystallographic primitive vectors in connection with Miller indices (hkl)
	#
	#    set TABBRAVAIS(1)  "triclinic   primitive      -1     C_i " 
	#    set TABBRAVAIS(2)  "monoclinic  primitive      2/m    C_2h"
	#    set TABBRAVAIS(3)  "monoclinic  base centered  2/m    C_2h"
	#    set TABBRAVAIS(4)  "orthorombic primitive      mmm    D_2h"
	#    set TABBRAVAIS(5)  "orthorombic base-centered  mmm    D_2h"
	#    set TABBRAVAIS(6)  "orthorombic body-centered  mmm    D_2h"
	#    set TABBRAVAIS(7)  "orthorombic face-centered  mmm    D_2h"
	#    set TABBRAVAIS(8)  "tetragonal  primitive      4/mmm  D_4h"
	#    set TABBRAVAIS(9)  "tetragonal  body-centered  4/mmm  D_4h"
	#    set TABBRAVAIS(10) "trigonal    primitive      -3m    D_3d"
	#    set TABBRAVAIS(11) "hexagonal   primitive      6/mmm  D_6h"
	#    set TABBRAVAIS(12) "cubic       primitive      m3m    O_h "
	#    set TABBRAVAIS(13) "cubic       face-centered  m3m    O_h "
	#    set TABBRAVAIS(14) "cubic       body-centered  m3m    O_h "

	for {set i 1} {$i <= 3} {incr i} {
	    set CBASX($i) 0
	    set CBASY($i) 0
	    set CBASZ($i) 0
	}
	
	if { $BRAVAIS >= 12 && $BRAVAIS <= 14 } {
	    #------------------------------------------------------ cubic
	    set CBASX(1) 1
	    set CBASY(2) 1
	    set CBASZ(3) 1
	} elseif { $BRAVAIS >= 8 && $BRAVAIS <= 9 } {
	    #------------------------------------------------------ tetragonal
	    set CBASX(1) 1
	    set CBASY(2) 1
	    set CBASZ(3) $COA
	} elseif { $BRAVAIS >= 4 && $BRAVAIS <= 7 } {
	    #------------------------------------------------------ orthorombic
	    set CBASX(1) 1
	    set CBASY(2) $BOA
	    set CBASZ(3) $COA
	    #------------------------------------------------------ otherwise
	} else {
	    for {set i 1} {$i <= 3} {incr i} {
		set CBASX($i) $RBASX($i)
		set CBASY($i) $RBASY($i)
		set CBASZ($i) $RBASZ($i)
	    }
	}
	debug $PRC "-> R1    $CBASX(1) $CBASY(1) $CBASZ(1)"
	debug $PRC "-> R2    $CBASX(2) $CBASY(2) $CBASZ(2)"
	debug $PRC "-> R3    $CBASX(3) $CBASY(3) $CBASZ(3)"
	
	set vx [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "x" ]
	set vy [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "y" ]
	set vz [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "z" ]
	set vol [expr abs( $vx*$CBASX(3) + $vy*$CBASY(3) + $vz*$CBASZ(3) ) ]
	
	set GBASX(1) [expr [cross_prod $CBASX(2) $CBASY(2) $CBASZ(2) $CBASX(3) $CBASY(3) $CBASZ(3) "x" ]/$vol ]
	set GBASY(1) [expr [cross_prod $CBASX(2) $CBASY(2) $CBASZ(2) $CBASX(3) $CBASY(3) $CBASZ(3) "y" ]/$vol ]
	set GBASZ(1) [expr [cross_prod $CBASX(2) $CBASY(2) $CBASZ(2) $CBASX(3) $CBASY(3) $CBASZ(3) "z" ]/$vol ]
	set GBASX(2) [expr [cross_prod $CBASX(3) $CBASY(3) $CBASZ(3) $CBASX(1) $CBASY(1) $CBASZ(1) "x" ]/$vol ]
	set GBASY(2) [expr [cross_prod $CBASX(3) $CBASY(3) $CBASZ(3) $CBASX(1) $CBASY(1) $CBASZ(1) "y" ]/$vol ]
	set GBASZ(2) [expr [cross_prod $CBASX(3) $CBASY(3) $CBASZ(3) $CBASX(1) $CBASY(1) $CBASZ(1) "z" ]/$vol ]
	set GBASX(3) [expr [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "x" ]/$vol ]
	set GBASY(3) [expr [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "y" ]/$vol ]
	set GBASZ(3) [expr [cross_prod $CBASX(1) $CBASY(1) $CBASZ(1) $CBASX(2) $CBASY(2) $CBASZ(2) "z" ]/$vol ]

	debug $PRC "-> G1    $GBASX(1) $GBASY(1) $GBASZ(1)"
	debug $PRC "-> G2    $GBASX(2) $GBASY(2) $GBASZ(2)"
	debug $PRC "-> G3    $GBASX(3) $GBASY(3) $GBASZ(3)"
	debug $PRC "(hkl)    $h_SURF  $k_SURF  $l_SURF "
	
	set NORMX [expr {$h_SURF*$GBASX(1) + $k_SURF*$GBASX(2) + $l_SURF*$GBASX(3)} ]
	set NORMY [expr {$h_SURF*$GBASY(1) + $k_SURF*$GBASY(2) + $l_SURF*$GBASY(3)} ]
	set NORMZ [expr {$h_SURF*$GBASZ(1) + $k_SURF*$GBASZ(2) + $l_SURF*$GBASZ(3)} ]
	debug $PRC "NORMAL VECTOR   $NORMX $NORMY $NORMZ"
	
	set W [expr {sqrt( $NORMX*$NORMX + $NORMY*$NORMY + $NORMZ*$NORMZ )} ]
	set EV1X [expr {$NORMX / $W} ]
	set EV1Y [expr {$NORMY / $W} ]
	set EV1Z [expr {$NORMZ / $W} ]
	debug $PRC "NORMAL VECTOR   $EV1X $EV1Y $EV1Z"
	
	if {$EV1X == 0} {
	    set EV2X 1.0
	    set EV2Y 0.0
	} elseif {$EV1Y == 0} {
	    set EV2X 0.0
	    set EV2Y 1.0
	} else {
	    set EV2X [expr sqrt($EV1Y*$EV1Y/($EV1X*$EV1X+$EV1Y*$EV1Y))]
	    set EV2Y [expr -$EV2X * $EV1X/$EV1Y ]
	} 
	set EV2Z 0.0
	
	set EV3X [cross_prod $EV1X $EV1Y $EV1Z  $EV2X $EV2Y $EV2Z  "x" ]
	set EV3Y [cross_prod $EV1X $EV1Y $EV1Z  $EV2X $EV2Y $EV2Z  "y" ]
	set EV3Z [cross_prod $EV1X $EV1Y $EV1Z  $EV2X $EV2Y $EV2Z  "z" ]
	
	if {$o_SURF=="down"} {
	    set EV1X [expr -$EV1X ]
	    set EV1Y [expr -$EV1Y ]
	    set EV1Z [expr -$EV1Z ]
	}
	
	debug $PRC "-> EV1    $EV1X $EV1Y $EV1Z"
	debug $PRC "-> EV2    $EV2X $EV2Y $EV2Z"
	debug $PRC "-> EV3    $EV3X $EV3Y $EV3Z"
	
	set I1MAX 2
	set I2MAX 2					     
	set I3MAX 2					     
	
	#
	#---------------------------------------- find expansion coefficients  V = sum(i) p_i * RBAS_i 
	#
	for {set I1 0} {$I1 <= 1} {incr I1} {
	    for {set I2 0} {$I2 <= 1} {incr I2} {
		for {set I3 0} {$I3 <= 1} {incr I3} {
		    
		    set VX [expr $I1*$L1_SURF*$EV1X + $I2*$L2_SURF*$EV2X + $I3*$L3_SURF*$EV3X ] 
		    set VY [expr $I1*$L1_SURF*$EV1Y + $I2*$L2_SURF*$EV2Y + $I3*$L3_SURF*$EV3Y ] 
		    set VZ [expr $I1*$L1_SURF*$EV1Z + $I2*$L2_SURF*$EV2Z + $I3*$L3_SURF*$EV3Z ] 
		    
		    for {set I 1} {$I <= 3} {incr I} {  
			set bb($I) [expr $VX*$RBASX($I)+$VY*$RBASY($I)+$VZ*$RBASZ($I)] 
		    }
		    
		    set dbb(1) [expr $bb(1)  *$aa(2,2)*$aa(3,3) +$aa(1,2)*$aa(2,3)*$bb(3)   +$aa(1,3)*$bb(2)  *$aa(3,2) \
			    -$bb(1)  *$aa(2,3)*$aa(3,2) -$aa(1,2)*$bb(2)  *$aa(3,3) -$aa(1,3)*$aa(2,2)*$bb(3)  ]
		    
		    set dbb(2) [expr $aa(1,1)*$bb(2)  *$aa(3,3) +$bb(1)  *$aa(2,3)*$aa(3,1) +$aa(1,3)*$aa(2,1)*$bb(3)   \
			    -$aa(1,1)*$aa(2,3)*$bb(3)   -$bb(1)  *$aa(2,1)*$aa(3,3) -$aa(1,3)*$bb(2)  *$aa(3,1) ]
		    
		    set dbb(3) [expr $aa(1,1)*$aa(2,2)*$bb(3)   +$aa(1,2)*$bb(2)  *$aa(3,1) +$bb(1)  *$aa(2,1)*$aa(3,2) \
			    -$aa(1,1)*$bb(2)  *$aa(3,2) -$aa(1,2)*$aa(2,1)*$bb(3)   -$bb(1)  *$aa(2,2)*$aa(3,1) ]
		    
		    for {set I 1} {$I <= 3} {incr I} {  
			set pp($I) [expr int(abs( $dbb($I) / $det )) + 2 ]      
		    }
		    
		    set TXT [format "I %3i%3i%3i  ->  V %10.6f%10.6f%10.6f   p_i:  %3i%3i%3i "\
			    $I1 $I2 $I3 $VX $VY $VZ $pp(1) $pp(2) $pp(3)]
		    debug $PRC $TXT
		    
		    if {$I1MAX<$pp(1)} {set I1MAX $pp(1)}
		    if {$I2MAX<$pp(2)} {set I2MAX $pp(2)}
		    if {$I3MAX<$pp(3)} {set I3MAX $pp(3)}
		    
		}
	    }
	}
	
	set XCNTR $RQX($IQ_SURF)
	set YCNTR $RQY($IQ_SURF)
	set ZCNTR $RQZ($IQ_SURF)
	
	set I1MIN [expr -$I1MAX]
	set I2MIN [expr -$I2MAX]
	set I3MIN [expr -$I3MAX]
	
	set TOP1  [expr {$L1_SURF+$TOL}]
	set TOP2  [expr {$L2_SURF+$TOL}]
	set TOP3  [expr {$L3_SURF+$TOL}]
    } ;# end Krasmol=4 

    ########################################################################
	
    debug $PRC "  I0=$Iatom"
    debug $PRC "  I1= $I1MIN .. $I1MAX"
    debug $PRC "  I2= $I2MIN .. $I2MAX"
    debug $PRC "  I3= $I3MIN .. $I3MAX"
    
    # for COLOR_CODE calculation 
    set ZTMIN $ZT(1)
    set ZTMAX $ZT(1)
    for {set IT 2} {$IT <= $NT} {incr IT} {
	if {$ZTMIN > $ZT($IT) } {set ZTMIN $ZT($IT)}
	if {$ZTMAX < $ZT($IT) } {set ZTMAX $ZT($IT)}
    }
    if {$ZTMIN==$ZTMAX} {set ZTMIN -1}
    
#---------------------------------------------- specify atomic positions
#
    set I $Iatom
    ##set II 0
    ##set IRAD 0
    for {set I1 $I1MIN} {$I1 <= $I1MAX} {incr I1} {
	for {set I2 $I2MIN} {$I2 <= $I2MAX} {incr I2} {
	    for {set I3 $I3MIN} {$I3 <= $I3MAX} {incr I3} {
		#-----------------------------------------------------
		for {set IT 1} {$IT <= $NT} {incr IT} {
		    
		    if {$sphere_color == "IT" } {
			set COLOR_CODE [expr {$IT*3.0/$NT}]
		    } else {
			set COLOR_CODE [expr {1 + 2*($ZT($IT)-$ZTMIN)/($ZTMAX-$ZTMIN)}]
		    }      
		    
		    puts "################ $IT $ZT($IT)  $TXTT($IT)"
	    
		    for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
			set IQ $IQAT($IA,$IT)
			
			set X [expr {$RQX($IQ) + $I1*$RBASX(1) + $I2*$RBASX(2) + $I3*$RBASX(3)}]
			set Y [expr {$RQY($IQ) + $I1*$RBASY(1) + $I2*$RBASY(2) + $I3*$RBASY(3)}]
			set Z [expr {$RQZ($IQ) + $I1*$RBASZ(1) + $I2*$RBASZ(2) + $I3*$RBASZ(3)}]
			
			set flag 0
			if {$Krasmol==1} { 
			    set flag 1
			} elseif {$Krasmol==2} {
			    if { $X >= $BOT } {
				if { $Y >= $BOT } {
				    if { $Z >= $BOT } {
					if { $X <= $TOPX } {
					    if { $Y <= $TOPY } {
						if { $Z <= $TOPZ } {
						    set flag 1 
						}
					    }
					}
				    }
				}
			    }
			    
			} elseif {$Krasmol==3} {
			    set DRSQR [expr {pow($X-$XCNTR,2)+ pow($Y-$YCNTR,2)+ pow($Z-$ZCNTR,2)}]
			    if { $DRSQR <= $DRMAXSQR } {set flag 1}
			} else {
			    set DX [expr $XCNTR - $X]
			    set DY [expr $YCNTR - $Y]
			    set DZ [expr $ZCNTR - $Z]
			    set DOT1 [expr $DX*$EV1X + $DY*$EV1Y + $DZ*$EV1Z]
			    if { $DOT1 >= $BOT } {
				if { $DOT1 <= $TOP1 } {
				    set DOT2 [expr $DX*$EV2X + $DY*$EV2Y + $DZ*$EV2Z]
				    if { $DOT2 >= $BOT } {
					if { $DOT2 <= $TOP2 } {
					    set DOT3 [expr $DX*$EV3X + $DY*$EV3Y + $DZ*$EV3Z]
					    if { $DOT3 >= $BOT } {
						if { $DOT3 <= $TOP3 } {
						    set flag 1 
						}
					    }
					}
				    }
				}            
			    }
			}
			
			if {$show_empty_spheres==0} {
			    if {$NOQ($IQ)=="1"} {
				if {$ZT($ITOQ(1,$IQ))==0} {set flag 0}
			    }
			}
			
			if {$flag!=0} {
			    incr I
			    push_systeminfo_atom "a" $I $I $X $Y $Z $COLOR_CODE \
				    $TXTT($IT) $IQ $IT $ZT($IT) $RWS($IQ)
			    
			    ##if {$IRAD < [expr $RWS($IQ)*$RS] } {
			    ##	set IRAD [expr $RWS($IQ)*$RS]
			    ##}
			}
			
			##incr II
			##set flagtab($II) $flag
			
		    }
		}
		#-----------------------------------------------------
	    }
	}
    }
    
    return $I
    
}

#
# specific adapter procs with less arguments
#
proc run_visualizer_for_rectangular_box {visualizer LX_BOX LY_BOX LZ_BOX \
	show_site_labels show_unit_cell show_cube show_axis show_empty_spheres sphere_color} {
    
    run_visualizer $visualizer 2 \
	    0 0 0 \
	    $LX_BOX $LY_BOX $LZ_BOX \
	    0 0 0 \
	    0 0 0 0 0 0 0 0 \
	    $show_site_labels $show_unit_cell $show_cube $show_axis $show_empty_spheres \
	    $sphere_color
    
}

proc run_visualizer_for_spherical_cluster {visualizer IQCNTR CLURAD NSHLCLU \
	show_site_labels show_unit_cell show_cube show_axis show_empty_spheres sphere_color} {
    
    run_visualizer $visualizer 3 \
	    0 0 0 \
	    0 0 0 \
	    $IQCNTR $CLURAD $NSHLCLU \
	    0 0 0 0 0 0 0 0 \
	    $show_site_labels $show_unit_cell $show_cube $show_axis $show_empty_spheres \
	    $sphere_color
}

proc run_visualizer_for_arbclu_occtype {visualizer \
	show_site_labels show_unit_cell show_cube show_axis show_empty_spheres sphere_color} {
    
    run_visualizer $visualizer 5 \
	    0 0 0 \
	    0 0 0 \
	    0 0 0 \
	    0 0 0 0 0 0 0 0 \
	    $show_site_labels $show_unit_cell $show_cube $show_axis $show_empty_spheres \
	    $sphere_color
}

#
#
#
proc run_visualizer {visualizer Krasmol N1 N2 N3 \
	LX_BOX LY_BOX LZ_BOX \
	IQCNTR CLURAD NSHLCLU \
	IQ_SURF h_SURF k_SURF l_SURF L1_SURF L2_SURF L3_SURF o_SURF \
	show_site_labels show_unit_cell show_cube show_axis show_empty_spheres sphere_color} {
    
    global LATPAR LATANG BOA SPACEGROUP BRAVAIS
    global NQ RQX RQY RQZ NQCL NCL ITOQ RBASX RBASY RBASZ COA BOA
    global ZT TXTT TXTT0 sysfile ALAT NT NAT NOQ IQAT RWS
    global RASRAD RASSCL 
    global W_sites_graphik
    global RASMOL DATAEXPLORER 
    global color_scheme_rasmol_Jmol
    
    set PRC "run_visualizer" 

    ## RASRAD = 0.4D0     ; # "scale wigner seitz" 0...1
    ## RASSCL = 3         ; # "scale object" 1...10                                                                
    set A0ANG 0.529177
    
    set S  [expr {$RASSCL*$A0ANG*$ALAT}]
    set RS [expr {$RASRAD*$RASSCL*$A0ANG*250}]
    
    if {[info exists W_sites_graphik]} { patience $W_sites_graphik 1 }
    writescr .d.tt "asking for patience \n"
    
    clear_systeminfo
    
#
#------------------------------------------ specify corners of unit cell
#
    if {$show_unit_cell == 0 } {
	set I0 0
    } else {
	set I0 8
	
	set uc_color 2.2
	push_systeminfo_atom "h" 1 1 0.0 0.0 0.0 $uc_color "cell" 0 0 0
	for {set i 1} {$i <= 3} {incr i} {
	    set BRX($i) $RBASX($i)
	    set BRY($i) $RBASY($i)
	    set BRZ($i) $RBASZ($i)
	    push_systeminfo_atom "h" [expr {$i+1}] [expr {$i+1}] \
		    $BRX($i) $BRY($i) $BRZ($i) $uc_color "cell" 0 0 0
	}
	#
	push_systeminfo_atom "h" 5 5 [expr {$BRX(1)+$BRX(2)}] [expr {$BRY(1)+$BRY(2)}] \
		[expr {$BRZ(1)+$BRZ(2)}] $uc_color "cell" 0 0 0 
	push_systeminfo_atom "h" 6 6 [expr {$BRX(1)+$BRX(3)}] [expr {$BRY(1)+$BRY(3)}] \
		[expr {$BRZ(1)+$BRZ(3)}] $uc_color "cell" 0 0 0 
	push_systeminfo_atom "h" 7 7 [expr {$BRX(3)+$BRX(2)}] [expr {$BRY(3)+$BRY(2)}] \
		[expr {$BRZ(3)+$BRZ(2)}] $uc_color "cell" 0 0 0 
	push_systeminfo_atom "h" 8 8 [expr {$BRX(1)+$BRX(2)+$BRX(3)}] \
		[expr {$BRY(1)+$BRY(2)+$BRY(3)}] \
		[expr {$BRZ(1)+$BRZ(2)+$BRZ(3)}] $uc_color "cell" 0 0 0
    }
#
#------------------------- specify corners of cube with edge length ALAT
#
    if {$show_cube != 0 } {

	set I $I0
	set I0 [expr {$I0 + 8}]
	
	set cub_color 3.0
	
	foreach {a b c} {0 0 0   1 0 0   0 1 0   0 0 1   1 1 0   1 0 1   0 1 1   1 1 1} {  
	    incr I
	    push_systeminfo_atom "h" $I $I $a $b $c $cub_color "cube" 0 0 0
	}
	
    }
    
    if {$Krasmol <= 4} {
	set I0 [add_Krasmol_systeminfo $I0 $Krasmol $N1 $N2 $N3 \
		$LX_BOX $LY_BOX $LZ_BOX \
		$IQCNTR $CLURAD $NSHLCLU \
		$IQ_SURF $h_SURF $k_SURF $l_SURF $L1_SURF $L2_SURF $L3_SURF $o_SURF \
		$show_empty_spheres $sphere_color]
	
	# propagate return in case of error (wrong parameter)
	if {$I0 < 0 } {
	    return
	}
	
    } elseif {$Krasmol == 5}  {
	# Show3D from occupation type change menue in arbitrary cluster selection 
	add_arbclu_systeminfo $I0
    }	
    
    # TODO: additional Krasmol modes 6 ... 
    # especially for geometry.f calculated spherical and rectangular clusters
    
    # connections for cell or cube
    if { $show_unit_cell != 0 || $show_cube != 0 } {
	foreach {from to} {1 2 1 3 1 4 2 5 3 5 5 8 2 6 4 6 6 8 3 7 4 7 7 8} {
	    push_systeminfo_bond $from $to
	}
    }
    
    # additional connections for cube 
    if { $show_unit_cell != 0 && $show_cube != 0 } {
	foreach {from to} {9 10 9 11 9 12 10 13 11 13 13 16 10 14 12 14 14 16 11 15 12 15 15 16} {
	    push_systeminfo_bond $from $to
	}
    }

    if {![info exist ::expert_config(VIZ_CALC_BONDS)]} {
	set ::expert_config(VIZ_CALC_BONDS) 1
    }
    set precalc_bonds $::expert_config(VIZ_CALC_BONDS)
    
    # TODO: should be moved to gui
    if {$precalc_bonds} {
	# explicitely calculated bonds
	calculate_systeminfo_bonds $ALAT ; # $RS
    }
    
    if {[info exists W_sites_graphik]} { patience $W_sites_graphik 0 }

    # radius for cell and cube (IQ==0)
    set RWS(0) 0
    
    set fnameroot $sysfile
    append fnameroot _xband
 
    # spawn external visualizer executable
    switch $visualizer {

        xcrysden {
	            write_rasmol_files $fnameroot $S $RS $show_site_labels $show_unit_cell \
		    $show_cube $show_axis $show_empty_spheres $sphere_color
                    xcry_start
                  }
	
	rasmol { 

            set color_scheme_rasmol_Jmol "temperature"
	   
	    write_rasmol_files $fnameroot $S $RS $show_site_labels $show_unit_cell \
		    $show_cube $show_axis $show_empty_spheres $sphere_color
	    writescr .d.tt "set up rasmol input $fnameroot completed \n"
	    
	    eval set res [catch "exec $RASMOL -script $fnameroot.ras &" message]
	    writescr .d.tt "$message \n"
	    
	}
	
	jmol { 
	   
            set color_scheme_rasmol_Jmol "user"

	    write_rasmol_files $fnameroot $S $RS $show_site_labels $show_unit_cell \
		    $show_cube $show_axis $show_empty_spheres $sphere_color
	    writescr .d.tt "set up rasmol input $fnameroot completed \n"
	    
	    eval set res [catch "exec jmol -J $fnameroot.ras &" message]
	    writescr .d.tt "$message \n"
	    
	}
	
	dataexplorer {

	    write_dataexplorer_files $fnameroot $S $RS
	    writescr .d.tt "set up dataexplorer input $fnameroot completed \n"
	    
	    set dx $DATAEXPLORER
	    #lappend dx "-noDXHelp -noImageMenus -noExecuteMenus"
	    #lappend dx "-noPanelAccess -noPanelEdit -noPanelOptions" 
	    lappend dx "-noConfirmedQuit"
	    set res [catch "exec $dx -execute_on_change -image $fnameroot.net &" message]
	    writescr .d.tt "$message \n" 
	    
	} 

	default {
	    writescr .d.tt "unknown visualizer: $visualizer\n";
	} 
	
    }
}

#
#
#
proc write_rasmol_files {fnam scaling radius_scaling \
	show_site_labels show_unit_cell show_cube show_axis show_empty_spheres \
	sphere_color} {
    
    global SYSTEMINFO
    global color_scheme_rasmol_Jmol
    
    set strdat [open "$fnam.pdb" w]
    
    puts $strdat "HEADER    $fnam"
    puts $strdat "SOURCE    X-band    "
    puts $strdat "AUTHOR    H. Ebert  "
    puts $strdat "REMARK    None      "

    if {![info exists color_scheme_rasmol_Jmol]} { 
         set color_scheme_rasmol_Jmol "temperature"
    }

    if {![info exists TAB_Jmol_CPK_colors]} { 
         table_Jmol_CPK_colors
    }
    
    # write atoms
    foreach s $SYSTEMINFO(rasmoltype) i $SYSTEMINFO(NRI) j $SYSTEMINFO(NRJ) \
	    x $SYSTEMINFO(X) y $SYSTEMINFO(Y) z $SYSTEMINFO(Z) t $SYSTEMINFO(COLOR) \
	    txtt $SYSTEMINFO(TXTT) iq $SYSTEMINFO(IQ) it $SYSTEMINFO(IT) \
            zt $SYSTEMINFO(ZT) {
	write_coord_color $strdat $s $i $j [expr {$scaling*$x}] [expr {$scaling*$y}] [expr {$scaling*$z}] \
		$t $txtt $iq $it $zt
    } 
    
    # write connections
    if {[info exists SYSTEMINFO(connection)]} {
	foreach connection $SYSTEMINFO(connection) {
	    puts $strdat [format {CONECT %4s %4s} [lindex $connection 0] [lindex $connection 1]]
	}
    }
    
    puts $strdat "END"
    close $strdat
    
    #
    #--------------------------------------------------- write RASMOL script
    #
    set   strdir  [open "$fnam.ras" w]
    puts  $strdir "load \"$fnam.pdb\" "
    if {$color_scheme_rasmol_Jmol == "user" } {
	puts  $strdir "color user"
    } else {
	puts  $strdir "color temperature"
    }
    puts  $strdir "background white"
    if {$show_site_labels == 1 } {
	puts  $strdir "label true"
    } else {
	puts  $strdir "label false"   
    }
    puts  $strdir "set fontsize 20"
    if { $show_unit_cell != 0 || $show_cube != 0 } {
	puts  $strdir "select 1-8"
	puts  $strdir "label \" \" "
    }
    if { $show_unit_cell != 0 && $show_cube != 0 } {
	puts  $strdir "select 9-16"
	puts  $strdir "label \" \" "
    }
    #
    #------------------------ rasmol expects the radii in multiples of 1/250
    #--------------------------------------- only values <= 500 are accepted
    set IRAD 0.0
    foreach rws $SYSTEMINFO(RWS) {
	if {$rws > $IRAD} {
	    set IRAD $rws
	}
    }
    
    set IRAD [expr {$IRAD*$radius_scaling}]
    set IRADLIM 500.0
    if {$IRAD < $IRADLIM} {
	set SCLIRAD 1
    } else {
	set SCLIRAD [expr {$IRADLIM/$IRAD}]
	writescr .d.tt "\nRASMOL max. atomic radius exceeded \
		atomic radii reduced by $SCLIRAD \n\n"
    }
    
    #
    foreach i $SYSTEMINFO(NRI) iq $SYSTEMINFO(IQ) txtt $SYSTEMINFO(TXTT) \
	    it $SYSTEMINFO(IT) rws $SYSTEMINFO(RWS) {
	
	if {$txtt == "cell" || $txtt == "cube"} {
	    continue
	}
	
	set IRAD [expr {$rws * $radius_scaling * $SCLIRAD}]
	##if {$IRAD > $IRADLIM} {set IRAD $IRADLIM}
	
	puts  $strdir "select $i"
	set txt "[string trim $txtt] :$it:$iq"    
	puts  $strdir "label \"$txt\" "
	puts  $strdir "cpk [expr {int($IRAD)}] "
    }
    
    #-----------------------------------------------------
    
    puts  $strdir "select all "
    if {$show_site_labels == 0 } {
	puts  $strdir "label \" \" "
    }
    if {$show_axis != 0 } {
	puts  $strdir "set axes on "
    }
    puts  $strdir "# use the following command to write a ps-file "
    puts  $strdir "# write vectps  \"$fnam.ps\" "
    #
    close $strdir
    
}


#-------------------------------------------------------------------------------
proc write_coord_color {f s i j x y z t txtt iq it zt} {

# write the coordinates and colors (if requested) in the PDB format
    
##    global ZT 
##    set ZT(0) 0

    global color_scheme_rasmol_Jmol TAB_Jmol_CPK_colors
        
    set PRC "write_coord_color"
    
    if {$s == "a" } {
	set ATOM  "ATOM  "
    } else {
	set ATOM  "HETATM"
    }
    puts $f [format "%6s %4i           %4i    %8.3f%8.3f%8.3f  0.00%8.3f   %8s IQ %4i IT %4i  Z %4i" \
		 $ATOM $i $j $x $y $z $t $txtt $iq $it $zt ]

    if {$color_scheme_rasmol_Jmol == "user" } {

       set cc $TAB_Jmol_CPK_colors($zt)

       set r [getvalue $cc  0 ","]
       set g [getvalue $cc  1 ","]
       set b [getvalue $cc  2 ","]
       
       set r [expr $r / 255.0 ]
       set g [expr $g / 255.0 ]
       set b [expr $b / 255.0 ]

       puts $f [format "%6s %4i           %4i    %8.3f%8.3f%8.3f  0.00%8.3f" \
  		 "COLOR " $i $j $r $g $b $t ]

    }

}
#-------------------------------------------------------------------------------

#
#
#
proc push_systeminfo_atom {s i j x y z t txtt iq it zt {rws 0.0} \
	{mag_x 0.0} {mag_y 0.0} {mag_z 0.0}} {
    
    global SYSTEMINFO 
    
    lappend SYSTEMINFO(rasmoltype) $s   ; # a_tom h_etatom
    lappend SYSTEMINFO(NRI) $i
    lappend SYSTEMINFO(NRJ) $j
    lappend SYSTEMINFO(X) $x            ; # x,y,z coordinates
    lappend SYSTEMINFO(Y) $y
    lappend SYSTEMINFO(Z) $z
    lappend SYSTEMINFO(COLOR) $t        ; # rasmol color temperature float
    lappend SYSTEMINFO(TXTT) $txtt
    lappend SYSTEMINFO(IQ) $iq
    lappend SYSTEMINFO(IT) $it          ; # atomic type
    lappend SYSTEMINFO(ZT) $zt          ; # atomic type

    lappend SYSTEMINFO(RWS) $rws        ; # wigner seitz radius 

    lappend SYSTEMINFO(MAG_X) $mag_x    ; # magnetisation vector
    lappend SYSTEMINFO(MAG_Y) $mag_y
    lappend SYSTEMINFO(MAG_Z) $mag_z
       
  }

#
#
#
proc push_systeminfo_bond {from to} {

    global SYSTEMINFO

    lappend SYSTEMINFO(connection) [list $from $to]

}  

#
#
#
proc clear_systeminfo {} {

    global SYSTEMINFO
    catch {unset SYSTEMINFO}
    
}

#
# atoms are bonded if their distance is smaller than the sum
# of their wigner-seitz-radii
#
# :TODO: use SYSTEMINFO(RWS) instead of global RWS(IQ)
#
#
proc calculate_systeminfo_bonds {ALAT {radius_scaling 1.0}} {

    global SYSTEMINFO RWS

    set dbg 0

    if $dbg { puts "DBG> radius_scaling = $radius_scaling" }

    set natoms [llength $SYSTEMINFO(X)]

    for {set i 0} {$i < $natoms} {incr i} {
	set i_iq [lindex $SYSTEMINFO(IQ) $i]

	if {$i_iq == 0} continue

	for {set j [expr {$i+1}]} {$j < $natoms} {incr j} {
	    set j_iq [lindex $SYSTEMINFO(IQ) $j]

	    if {$j_iq == 0} continue
	    
	    #set distmaxtab(1,1) 2.3
	    # build distmaxtable on the fly
	    if {[info exists distmaxtab($i_iq,$j_iq)]} {
		set distmax $distmaxtab($i_iq,$j_iq)
	    } else {
		#set distmax [expr {$RS*sqrt($RWS($i_iq)+$RWS($j_iq))}]  

		set distmax [expr {$radius_scaling*($RWS($i_iq)+$RWS($j_iq))}]  
		set distmax [expr {$distmax/$ALAT}]

		if $dbg { 
		    puts "DBG> $distmax $i_iq $j_iq $RWS($i_iq) $RWS($j_iq)"
		}
		
		set distmaxtab($i_iq,$j_iq) $distmax
		set distmaxtab($j_iq,$i_iq) $distmax
	    }

	    set dist [expr {sqrt([calc_systeminfo_atom_distance2 $i $j])}]
	    if $dbg { puts "DBG> $dist $i ($i_iq) $j ($j_iq)" }

	    if {$dist < $distmax} {
		push_systeminfo_bond [expr {$i + 1}] [expr {$j + 1}]
	    }

	}
    }

}


#
#
#
proc squared_euclidean_distance {x0 y0 z0 x1 y1 z1} {

    set x [expr {$x1-$x0}]
    set y [expr {$y1-$y0}]
    set z [expr {$z1-$z0}]

    return [expr {$x*$x + $y*$y + $z*$z}]

} 

#
# caveat: index (starting at 0) in SYSTEMINFO not atomnummer
#
proc calc_systeminfo_atom_distance2 {i j} {
    
    global SYSTEMINFO
    
    set i_x  [lindex $SYSTEMINFO(X) $i]
    set i_y  [lindex $SYSTEMINFO(Y) $i]
    set i_z  [lindex $SYSTEMINFO(Z) $i]
    
    set j_x  [lindex $SYSTEMINFO(X) $j]
    set j_y  [lindex $SYSTEMINFO(Y) $j]
    set j_z  [lindex $SYSTEMINFO(Z) $j]
    
    return [squared_euclidean_distance $i_x $i_y $i_z $j_x $j_y $j_z]
    
}


#
# write system information in dataexplorer format 
#
# for a description of the dataexplorer file format see
#   User's Guide Chapter 3: Understanding the Data Model
#
# check correct transfer to dataexplorer via:
# dx -script
#    i = Import("XXX_xband.dx");
#    Print(i,"D");
#
proc write_dataexplorer_files {fnam {scaling 1.0} {radius_scaling 1.0}} {
    
    global SYSTEMINFO
    global xband_path
    
    set strdat [open "$fnam.dx" w]
    puts $strdat "# $fnam written by xband"

    set natoms [llength $SYSTEMINFO(X)]   ; # field X choosen arbitrary
    
    ### radius as data component 
    puts $strdat "# object data"
    puts $strdat "object 1 class array type float rank 0 items $natoms data follows"
    
    foreach rws $SYSTEMINFO(RWS) {
	puts $strdat [format {%12.6f} [expr {$rws * $radius_scaling}]]
    }
    
    puts $strdat {attribute "dep" string "positions"}

    ### additional data components
    ###
    
    ### colors component
    puts $strdat "# object colors"
    puts $strdat "object 2 class array type float rank 1 shape 3 items $natoms data follows"
    
    set rgb_colors [map_rasmol_tempfactor_to_rgb $SYSTEMINFO(COLOR)]
    foreach color $rgb_colors {
	set red   [expr {[lindex $color 0]/255.0}]
	set green [expr {[lindex $color 1]/255.0}]
	set blue  [expr {[lindex $color 2]/255.0}]
	puts $strdat [format {%f %f %f} $red $green $blue]
    }
    puts $strdat {attribute "dep" string "positions"}

    ## :TODO: magnetisation component
    
    ### positions component
    puts $strdat "# object positions"
    puts $strdat "object 3 class array type float rank 1 shape 3 items $natoms data follows"
    foreach x $SYSTEMINFO(X) y $SYSTEMINFO(Y) z $SYSTEMINFO(Z) {
	puts $strdat [format {%12.6f %12.6f %12.6f} \
		[expr {$scaling*$x}] [expr {$scaling*$y}] [expr {$scaling*$z}]]
    }
    puts $strdat {attribute "dep" string "positions"}

    ### connections component
    # dummy entry, no connection detection for dx so far (:TODO:)
    if {![info exists SYSTEMINFO(connection)]} {
	push_systeminfo_bond 1 1
    }

    set nbonds [llength $SYSTEMINFO(connection)]
    puts $strdat "# object connections"
    puts $strdat "object 4 class array type int rank 1 shape 2 items $nbonds data follows"
    foreach connection $SYSTEMINFO(connection) {
	set bond0 [expr {[lindex $connection 0] - 1}] ; # adjust to index start at 0
	set bond1 [expr {[lindex $connection 1] - 1}] 
	puts $strdat [format {%4d %4d} $bond0 $bond1]
    }
    
    puts $strdat {attribute "element type" string "lines"}
    puts $strdat {attribute "ref" string "positions"}

    
    ### write trailer
    puts $strdat {#}
    puts $strdat {object "molecule" class field}
    puts $strdat {component "data" value 1}
    puts $strdat {component "colors" value 2}
    puts $strdat {component "positions" value 3}
    puts $strdat {component "connections" value 4}
    puts $strdat {attribute "name" string "molecule"}
    puts $strdat {#}
    puts $strdat {end}
    
    close $strdat

    # todo: add textlabels  

    if 0 {
	# write net script for startup 
	set strdat [open "$fnam.net" w]
	puts $strdat "# written by xband"
	puts $strdat {include "xbandviz2.net"}
	#puts $strdat "inputfile = $fnam.dx"
	#puts $strdat "main_FileSelector_2_out_2 = \"$fnam.dx\";"
	puts $strdat "main_Import_2_in_1 = \"$fnam.dx\";"
	puts $strdat "main();"
	close $strdat
    } else {    

	# name of dataexplorer network file
	set fname_net "xbandviz.net"
	set fname_net [file join $xband_path $fname_net]
	# name of variable used as filename argument for importer
	# unfortuately the entry in in comment preceedes the varvalue
	# if read in as net
	#set input_var "main_Import_1_in_1"

	# input filename has to be "xband.dx"

	# copy file for visual controls
	file copy -force "[file root $fname_net].cfg" "$fnam.cfg"
	
	# copy dataexplorer net and adjust name of inputfile 
	set finp [open $fname_net r]
	set fout [open "$fnam.net" w]
	
	while {![eof $finp]} {
	    
	    set line [gets $finp] 
	    
	    # replace all "xband.dx" with the actual filename
	    if {[regexp {"xband.dx"} $line]} {
		regsub {"xband.dx"} $line "\"$fnam.dx\"" line
	    }

	    puts $fout $line
	    
	}
	
	close $finp 
	close $fout

    }

    # write minimum required commands in case of missing dx network file
    # Image(); provides interaction, but is a macro
    if 0 {
	set strdat [open "$fnam.net" w]
	puts $strdat "# written by xband"
	puts $strdat "i = Import(\"$fnam.dx\");"
	puts $strdat "g = Glyph(i);"
	puts $strdat "t = Tube(i);"
	puts $strdat "c = Collect(g,t);"
	puts $strdat "s = Shade(c);"
	puts $strdat "a = AutoCamera(s);"
	puts $strdat "Display(s, a);"
	close $strdat
    }


}

#
# convert vector of rasmol temperature-factors to corresponding 
# red green blue tupels
#
# simplified version from rasmol/transfor.c
# (no handling of LastShade and colorpalette overflow) 
#
# void ScaleColourAttrib( int attr )
# int DefineShade( int r, int g, int b )
# void ScaleColourMap( int count )
#
proc map_rasmol_tempfactor_to_rgb {tempfactors} {
    
    if {[llength $tempfactors] == 0} {
	return [list]
    }
    
    # return blue if only one tempfactor
    if {[llength $tempfactors] == 1} {
	return [list [list 0 0 255]]
    }

    if 0 {
	# the original rasmol way ... only partially implemented 
	# not yet: separate nColors/ScaleCount and 
	#          find nearest shade if limit is reached
	# range of values
	set tempfactor_min [lindex $tempfactors 0]
	set tempfactor_max $tempfactor_min
	foreach tempfactor $tempfactors {
	    if {$tempfactor < $tempfactor_min} {
		set tempfactor_min $tempfactor
	    }
	    if {$tempfactor > $tempfactor_max} {
		set tempfactor_max $tempfactor
	    }
	}
	
	# return blue if constant tempfactor
	if {$tempfactor_min == $tempfactor_max} {
	    set rgbs [list]
	    for {set i 0} {$i < [llength $tempfactors]} {incr i} {
		lappend rgbs [list 0 0 255]
	    }
	    return $rgbs
	}
    
	set nColors [expr {round($tempfactor_max - $tempfactor_min + 1.0)}] 
    } else {
	
	# use values as individual "strings" 
	# distance between values is not acounted for
	set idx 0
	foreach tempfactor [lsort -real -increasing $tempfactors] {
	    if {! [info exists tempfactor2idx($tempfactor)]} {
		set tempfactor2idx($tempfactor) $idx
		incr idx
	    }
	}
	
	set nColors [llength [array names tempfactor2idx]]
	
    }

    set ScaleCount $nColors
    
    # precalculate and fill color look up table 
    for {set i 0} {$i<$ScaleCount} {incr i}  { 
	set fract [expr {int((1023*$i)/($ScaleCount-1))}]
	
        if {$fract < 256} {
	    set r 0
	    set g $fract
	    set b 255
        } elseif {$fract < 512 } {
	    set r 0
	    set g 255
	    set b [expr {511-$fract}]
        } elseif {$fract < 768} {
	    set r [expr {$fract-512}]
	    set g 255
	    set b 0
        } else { 
	    # fract < 1024                             
	    set r 255
	    set g [expr {1023-$fract}]
	    set b 0
        }
	
	set color_lut($i) [list $r $g $b]
    }
    
    set rgbs [list]
    foreach tempfactor $tempfactors {
	if 0 {
	    set idx [expr {int(($ScaleCount*$tempfactor)/$nColors)}]
	    if {$idx > $ScaleCount-1} {
		set idx [expr {$ScaleCount-1}]
	    }
	} else {
	    set idx $tempfactor2idx($tempfactor)
	}

	lappend rgbs $color_lut($idx) 
    }
    
    return $rgbs
    
}

#
#
#
proc view_rasmol_colorscale {{ncolors 255}} {

    set dbg 0

    set wroot .rasmolscale
    catch {destroy $wroot}
    toplevel $wroot
    set canv [canvas $wroot.c]
    pack $canv -expand 1 -fill both

    set tempfactors [list]
    for {set i 0} {$i < $ncolors} {incr i} {
	lappend tempfactors $i
    }
    
    set rgbs [map_rasmol_tempfactor_to_rgb $tempfactors]

    set x 10
    set y 10
    set dx 20
    set dy 2

    set x1 [expr {$x+$dx}]
    
    foreach rgb $rgbs {
	set y1 [expr {$y+$dy}]
	if $dbg { puts "DBG> $rgb" }
	set color [rgb_list2string $rgb] 
	$canv create rectangle $x $y $x1 $y1 -fill $color -outline $color
	set y $y1
    }

    return $canv

}

#
#
#
proc rgb_list2string {rgb} {

    return [format "#%02x%02x%02x" [lindex $rgb 0] [lindex $rgb 1] [lindex $rgb 2]]

}


#***************************************************************************************
#                                  table_CHSYM
#***************************************************************************************
#
proc table_Jmol_CPK_colors { } {
    
    global TAB_Jmol_CPK_colors
    
    set TAB_Jmol_CPK_colors(0)  "255,255,255"
    set TAB_Jmol_CPK_colors(1)  "255,255,255"  ;    set TAB_Jmol_CPK_colors(61)  "163,255,199"
    set TAB_Jmol_CPK_colors(2)  "217,255,255"  ;    set TAB_Jmol_CPK_colors(62)  "143,255,199"
    set TAB_Jmol_CPK_colors(3)  "204,128,255"  ;    set TAB_Jmol_CPK_colors(63)  "97,255,199 "
    set TAB_Jmol_CPK_colors(4)  "194,255,0  "  ;    set TAB_Jmol_CPK_colors(64)  "69,255,199 "
    set TAB_Jmol_CPK_colors(5)  "255,181,181"  ;    set TAB_Jmol_CPK_colors(65)  "48,255,199 "
    set TAB_Jmol_CPK_colors(6)  "144,144,144"  ;    set TAB_Jmol_CPK_colors(66)  "31,255,199 "
    set TAB_Jmol_CPK_colors(7)  "48,80,248  "  ;    set TAB_Jmol_CPK_colors(67)  "0,255,156  "
    set TAB_Jmol_CPK_colors(8)  "255,13,13  "  ;    set TAB_Jmol_CPK_colors(68)  "0,230,117  "
    set TAB_Jmol_CPK_colors(9)  "144,224,80 "  ;    set TAB_Jmol_CPK_colors(69)  "0,212,82   "
    set TAB_Jmol_CPK_colors(10) "179,227,245"  ;    set TAB_Jmol_CPK_colors(70)  "0,191,56   "
    set TAB_Jmol_CPK_colors(11) "171,92,242 "  ;    set TAB_Jmol_CPK_colors(71)  "0,171,36   " 
    set TAB_Jmol_CPK_colors(12) "138,255,0  "  ;    set TAB_Jmol_CPK_colors(72)  "77,194,255 " 
    set TAB_Jmol_CPK_colors(13) "191,166,166"  ;    set TAB_Jmol_CPK_colors(73)  "77,166,255 " 
    set TAB_Jmol_CPK_colors(14) "240,200,160"  ;    set TAB_Jmol_CPK_colors(74)  "33,148,214 " 
    set TAB_Jmol_CPK_colors(15) "255,128,0  "  ;    set TAB_Jmol_CPK_colors(75)  "38,125,171 " 
    set TAB_Jmol_CPK_colors(16) "255,255,48 "  ;    set TAB_Jmol_CPK_colors(76)  "38,102,150 " 
    set TAB_Jmol_CPK_colors(17) "31,240,31  "  ;    set TAB_Jmol_CPK_colors(77)  "23,84,135  " 
    set TAB_Jmol_CPK_colors(18) "128,209,227"  ;    set TAB_Jmol_CPK_colors(78)  "208,208,224" 
    set TAB_Jmol_CPK_colors(19) "143,64,212 "  ;    set TAB_Jmol_CPK_colors(79)  "255,209,35 " 
    set TAB_Jmol_CPK_colors(20) "61,255,0   "  ;    set TAB_Jmol_CPK_colors(80)  "184,184,208" 
    set TAB_Jmol_CPK_colors(21) "230,230,230"  ;    set TAB_Jmol_CPK_colors(81)  "166,84,77  " 
    set TAB_Jmol_CPK_colors(22) "191,194,199"  ;    set TAB_Jmol_CPK_colors(82)  "87,89,97   " 
    set TAB_Jmol_CPK_colors(23) "166,166,171"  ;    set TAB_Jmol_CPK_colors(83)  "158,79,181 " 
    set TAB_Jmol_CPK_colors(24) "138,153,199"  ;    set TAB_Jmol_CPK_colors(84)  "171,92,0   " 
    set TAB_Jmol_CPK_colors(25) "156,122,199"  ;    set TAB_Jmol_CPK_colors(85)  "117,79,69  " 
    set TAB_Jmol_CPK_colors(26) "224,102,51 "  ;    set TAB_Jmol_CPK_colors(86)  "66,130,150 " 
    set TAB_Jmol_CPK_colors(27) "240,144,160"  ;    set TAB_Jmol_CPK_colors(87)  "66,0,102   " 
    set TAB_Jmol_CPK_colors(28) "80,208,80  "  ;    set TAB_Jmol_CPK_colors(88)  "0,125,0    "
    set TAB_Jmol_CPK_colors(29) "200,128,51 "  ;    set TAB_Jmol_CPK_colors(89)  "112,171,250"
    set TAB_Jmol_CPK_colors(30) "125,128,176"  ;    set TAB_Jmol_CPK_colors(90)  "0,186,255  "
    set TAB_Jmol_CPK_colors(31) "194,143,143"  ;    set TAB_Jmol_CPK_colors(91)  "0,161,255  " 
    set TAB_Jmol_CPK_colors(32) "102,143,143"  ;    set TAB_Jmol_CPK_colors(92)  "0,143,255  " 
    set TAB_Jmol_CPK_colors(33) "189,128,227"  ;    set TAB_Jmol_CPK_colors(93)  "0,128,255  " 
    set TAB_Jmol_CPK_colors(34) "255,161,0  "  ;    set TAB_Jmol_CPK_colors(94)  "0,107,255  " 
    set TAB_Jmol_CPK_colors(35) "166,41,41  "  ;    set TAB_Jmol_CPK_colors(95)  "84,92,242  " 
    set TAB_Jmol_CPK_colors(36) "92,184,209 "  ;    set TAB_Jmol_CPK_colors(96)  "120,92,227 " 
    set TAB_Jmol_CPK_colors(37) "112,46,176 "  ;    set TAB_Jmol_CPK_colors(97)  "138,79,227 " 
    set TAB_Jmol_CPK_colors(38) "0,255,0    "  ;    set TAB_Jmol_CPK_colors(98)  "161,54,212 "
    set TAB_Jmol_CPK_colors(39) "148,255,255"  ;    set TAB_Jmol_CPK_colors(99)  "179,31,212 "
    set TAB_Jmol_CPK_colors(40) "148,224,224"  ;    set TAB_Jmol_CPK_colors(100) "179,31,186 "
    set TAB_Jmol_CPK_colors(41) "115,194,201"  ;    set TAB_Jmol_CPK_colors(101) "179,13,166 " 
    set TAB_Jmol_CPK_colors(42) "84,181,181 "  ;    set TAB_Jmol_CPK_colors(102) "189,13,135 " 
    set TAB_Jmol_CPK_colors(43) "59,158,158 "  ;    set TAB_Jmol_CPK_colors(103) "199,0,102  " 
    set TAB_Jmol_CPK_colors(44) "36,143,143 "  ;    set TAB_Jmol_CPK_colors(104) "204,0,89   " 
    set TAB_Jmol_CPK_colors(45) "10,125,140 "  ;    set TAB_Jmol_CPK_colors(105) "209,0,79   " 
    set TAB_Jmol_CPK_colors(46) "0,105,133  "  ;    set TAB_Jmol_CPK_colors(106) "217,0,69   " 
    set TAB_Jmol_CPK_colors(47) "192,192,192"  ;    set TAB_Jmol_CPK_colors(107) "224,0,56   " 
    set TAB_Jmol_CPK_colors(48) "255,217,143"  ;    set TAB_Jmol_CPK_colors(108) "230,0,46   "
    set TAB_Jmol_CPK_colors(49) "166,117,115"  ;    set TAB_Jmol_CPK_colors(109) "235,0,38   "
    set TAB_Jmol_CPK_colors(50) "102,128,128"
    set TAB_Jmol_CPK_colors(51) "158,99,181 " 
    set TAB_Jmol_CPK_colors(52) "212,122,0  " 
    set TAB_Jmol_CPK_colors(53) "148,0,148  " 
    set TAB_Jmol_CPK_colors(54) "66,158,176 " 
    set TAB_Jmol_CPK_colors(55) "87,23,143  " 
    set TAB_Jmol_CPK_colors(56) "0,201,0    " 
    set TAB_Jmol_CPK_colors(57) "112,212,255" 
    set TAB_Jmol_CPK_colors(58) "255,255,199"
    set TAB_Jmol_CPK_colors(59) "217,255,199"
    set TAB_Jmol_CPK_colors(60) "199,255,199"
}
