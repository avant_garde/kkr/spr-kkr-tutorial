
########################################################################################
proc write_sysfile { } {

#.................................................................. VERSION 5.0 08/11/21

global structure_window_calls
global sysfile syssuffix sysfile_host sysfile_3DBULK
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
global system NQ NT ZT RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global NM IMT IMQ RWSM NAT QMTET QMPHI 
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION DIMENSION_HOST

global CLUSTER_TYPE
global NQCLU NTCLU IQ_IQCLU N5VEC_IQCLU RQCLU
global NOCC_IQCLU Z_IQCLU TXT_IQCLU CONC_IQCLU ITCLU_OQCLU
global xband_version 
#
#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE  ZRANGE_TYPE  STRUCTURE_SEQUENCE
global struc_L_eq_struc_R bulk_L_eq_bulk_R OCCUPATION_OF_LAYERS
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R 
global N_rep_SU_bulk_L N_rep_SU_bulk_R NSUBSYS SU_SUBSYS N_SU SU_NAME SU_IQ
global sys_data_available 
global BRAVAIS_ORG
if { $BRAVAIS == "0" && [info exists BRAVAIS_ORG]} {
  set BRAVAIS $BRAVAIS_ORG 
}

set PRC write_sysfile

debug $PRC WRHEAD

set tcl_precision 17

set sysdat [open "$sysfile$syssuffix" w]

if {![info exists CLUSTER_TYPE]   } {set CLUSTER_TYPE   "NOT SPECIFIED"}
if {![info exists DIMENSION_HOST] } {set DIMENSION_HOST "NOT SPECIFIED"}

debug $PRC "STRUCTURE_TYPE $STRUCTURE_TYPE"
debug $PRC "DIMENSION_HOST $DIMENSION_HOST"
debug $PRC "DIMENSION      $DIMENSION"
debug $PRC "CLUSTER_TYPE   $CLUSTER_TYPE"

if {$DIMENSION != "3D" } {
   set SPACEGROUP    0
   set SPACEGROUP_AP 0
}

#
#=======================================================================================
#                              write system file
#=======================================================================================
#
puts $sysdat "system data-file created by xband on [standard_date]"
puts $sysdat "$sysfile$syssuffix"
puts $sysdat "xband-version"
puts $sysdat "$xband_version"
puts $sysdat "dimension"
puts $sysdat "$DIMENSION"
if {$DIMENSION == "0D" } { 
   puts $sysdat "dimension of host system"
   puts $sysdat $DIMENSION_HOST
} else { 
   set DIMENSION_HOST $DIMENSION
}
puts $sysdat "Bravais lattice "
puts $sysdat "$BRAVAIS  $TABBRAVAIS($BRAVAIS)"
puts $sysdat "space group number (ITXC and AP)"
puts $sysdat [format "%5i%5i"  $SPACEGROUP  $SPACEGROUP_AP]
puts $sysdat "structure type"
puts $sysdat $STRUCTURE_TYPE
puts $sysdat "lattice parameter A  \[a.u.\] "
puts $sysdat [format "%18.12f" $ALAT]
puts $sysdat "ratio of lattice parameters  b/a  c/a" 
puts $sysdat [format "%18.12f%18.12f" $BOA $COA]
puts $sysdat "lattice parameters  a b c  \[a.u.\] "
puts $sysdat [format "%18.12f%18.12f%18.12f" $LATPAR(1) $LATPAR(2) $LATPAR(3)]
puts $sysdat "lattice angles  alpha beta gamma  \[deg\] "
puts $sysdat [format "%18.12f%18.12f%18.12f" $LATANG(1) $LATANG(2) $LATANG(3)]
puts $sysdat "primitive vectors     (cart. coord.) \[A\]"
for {set I 1} {$I <= 3} {incr I} {
  puts $sysdat [format "%18.12f%18.12f%18.12f" $RBASX($I) $RBASY($I) $RBASZ($I)]
}
#
#------------------------------------------------------------------------------------ 2D
#
if {$DIMENSION == "2D" || $DIMENSION_HOST == "2D" } {
##puts $sysdat "lattice type"
##puts $sysdat $LATTICE_TYPE
puts $sysdat "zrange type      (extended = LIR or slab)"
puts $sysdat $ZRANGE_TYPE
##puts $sysdat "occupation of layers"
##puts $sysdat $OCCUPATION_OF_LAYERS
##puts $sysdat "number of subsystems built from same structure unit"
##puts $sysdat [format %3i $NSUBSYS]
##puts $sysdat "number of structure units"
##puts $sysdat [format %3i $N_SU]
##puts $sysdat "names of structure units"
##for {set I 1} {$I <= $N_SU} {incr I} {
##   puts $sysdat $SU_NAME($I)
##}

if { $ZRANGE_TYPE=="extended" } {
##   puts $sysdat "bulk(L) = bulk(R)"
##   puts $sysdat $bulk_L_eq_bulk_R
##   puts $sysdat "struc(L) = struc(R)"
##   puts $sysdat $struc_L_eq_struc_R
   puts $sysdat "number of sites NQ for bulk(L)"
   puts $sysdat [format %3i $NQ_bulk_L]
##   puts $sysdat "number of repeated structure units for bulk(L)"
##   puts $sysdat [format %3i $N_rep_SU_bulk_L]
   puts $sysdat "primitive vectors for bulk(L)    (cart. coord.) \[A\]"
   for {set I 1} {$I <= 3} {incr I} {
      puts $sysdat [format "%18.12f%18.12f%18.12f" $RBASX_L($I) $RBASY_L($I) $RBASZ_L($I)]
   }
   puts $sysdat "number of sites NQ for bulk(R)"
   puts $sysdat [format %3i $NQ_bulk_R]
##   puts $sysdat "number of repeated structure units for bulk(R)"
##   puts $sysdat [format %3i $N_rep_SU_bulk_R]
   puts $sysdat "primitive vectors for bulk(R)    (cart. coord.) \[A\]"
   for {set I 1} {$I <= 3} {incr I} {
      puts $sysdat [format "%18.12f%18.12f%18.12f" $RBASX_R($I) $RBASY_R($I) $RBASZ_R($I)]
   }
}
}
#------------------------------------------------------------------------------------ 2D



#
#--------------------------------------------------------------------------------- sites
#
puts $sysdat "number of sites NQ"
puts $sysdat [format %3i $NQ]
set TXT " IQ ICL"
if {$DIMENSION == "2D" || $DIMENSION_HOST == "2D" } {
   append TXT " SU WQ"
}
append TXT "     basis vectors     (cart. coord.) \[A\]"
append TXT "                      RWS \[a.u.\]  NLQ  NOQ ITOQ"  
puts $sysdat $TXT

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {

  set TXT [format "%3i%4i" $IQ $ICLQ($IQ) ]
  if {$DIMENSION == "2D" || $DIMENSION_HOST == "2D" } {
     append TXT [format "%3i  %1s" $SU_IQ($IQ) $WYCKOFFQ($IQ) ]
  }

  append TXT [format "%18.12f%18.12f%18.12f  %18.12f%4i%5i " \
              $RQX($IQ) $RQY($IQ) $RQZ($IQ) $RWS($IQ) $NLQ($IQ) $NOQ($IQ)]
  for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
      append TXT [format "%3i" $ITOQ($IA,$IQ)]
  }
  puts $sysdat $TXT
}
#
#-------------------------------------------------------------------------- site classes
#
puts $sysdat "number of sites classes NCL"
puts $sysdat [format %3i $NCL]
puts $sysdat "ICL WYCK NQCL IQECL (equivalent sites)"
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   set TXT [format "%3i   %1s%5i" $ICL $WYCKOFFCL($ICL) $NQCL($ICL)]
   for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
      append TXT [format %3i $IQECL($IE,$ICL)]
   }
   puts $sysdat $TXT
}   

#
#------------------------------------------------------------------------- find NAT IQAT
#
for {set IT 1} {$IT <= $NT} {incr IT} { set NAT($IT) 0 }
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
     set IT $ITOQ($IA,$IQ)
     incr NAT($IT)
     set IQAT($NAT($IT),$IT) $IQ
  }
}
#
#----------------------------------------------------------------------- find mesh table
#
set NM 0
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   set IQ1 $IQECL(1,$ICL)
   incr NM
   set RWSM($NM) $RWS($IQ1)
   set IMQ($IQ1) $NM  
   for {set IE 2} {$IE <= $NQCL($ICL)} {incr IE} {
      set IQ $IQECL($IE,$ICL)
      set IMQ($IQ) $IMQ($IQ1)
   }
}   

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
     set IT $ITOQ($IO,$IQ)
     set IMT($IT) $IMQ($IQ)
  }
}
#
#---------------------------------------------------------------------- init QMTET QMPHI
#
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set QMTET($IQ) 0.0
   set QMPHI($IQ) 0.0
}

#
#--------------------------------------------------------------------------------- types
#
puts $sysdat "number of atom types NT"
puts $sysdat  [format %3i $NT]
puts $sysdat " IT  ZT  TXTT  NAT  CONC  IQAT (sites occupied)"
for {set IT 1} {$IT <= $NT} {incr IT} {
  set TXT [string range "$TXTT($IT)   " 0 7]
  set OUT [format " %2i%4i  %8s%5i%6.3f" $IT $ZT($IT) $TXT $NAT($IT) $CONC($IT)]
  for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
     append OUT [format "%3i" $IQAT($IA,$IT)]
  }
  puts $sysdat $OUT
}

#puts $sysdat "direction of the global magnetisation  (cart. coord.)"
#puts $sysdat [format "%18.12f%18.12f%18.12f" $MAG_DIR(1) $MAG_DIR(2) $MAG_DIR(3)]

#
#------------------------------------------------------------------- 0D embedded cluster
#

#=======================================================================================
#                            writing   OD system information
#=======================================================================================
if {$DIMENSION == "0D" } { 
   puts "writting cluster information"
   puts $sysdat "--------------------------------- cluster information ---------------------------------"
   puts $sysdat "cluster type"
   puts $sysdat  $CLUSTER_TYPE
   puts $sysdat "host system file name"
   puts $sysdat  $sysfile_host

   set ITCLU 0
   for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {
      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
          incr ITCLU
          set ITCLU_OQCLU($IO,$IQCLU) $ITCLU 

          set NATCLU($ITCLU) 1
          set IQCLU_ATCLU(1,$ITCLU) $IQCLU 
          set   ZTCLU($ITCLU)    $Z_IQCLU($IO,$IQCLU)
          set TXTTCLU($ITCLU)  $TXT_IQCLU($IO,$IQCLU)
          set CONCCLU($ITCLU) $CONC_IQCLU($IO,$IQCLU)

      } 
   }
   set NTCLU $ITCLU

   puts $sysdat "number of cluster sites NQCLU"
   puts $sysdat [format %4i $NQCLU]

   set TXT " IQCLU       cluster vectors   (cart. coord.) \[A\]"
   append TXT "                 N5VEC_CLU        IQ  NOCC ITCLU"  

   puts $sysdat $TXT
   for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {

      set TXT [format "%4i" $IQCLU ]
      append TXT [format "%18.12f%18.12f%18.12f " \
                 $RQCLU(1,$IQCLU) $RQCLU(2,$IQCLU) $RQCLU(3,$IQCLU)]
      append TXT [format "%4i%4i%4i%4i%4i  %4i" \
                 $N5VEC_IQCLU(1,$IQCLU) $N5VEC_IQCLU(2,$IQCLU) $N5VEC_IQCLU(3,$IQCLU) \
                 $N5VEC_IQCLU(4,$IQCLU) $N5VEC_IQCLU(5,$IQCLU)  $IQ_IQCLU($IQCLU) ]

      append TXT [format "%6i " $NOCC_IQCLU($IQCLU)]

      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
          append TXT [format "%5i" $ITCLU_OQCLU($IO,$IQCLU)]
      } 
      puts $sysdat $TXT
   }

   puts $sysdat "number of cluster atom types NTCLU"
   puts $sysdat  [format %4i $NTCLU]
   puts $sysdat " ITCLU  ZTCLU  TXTTCLU  NATCLU  CONCCLU  IQCLU_ATCLU (sites occupied)"
   for {set ITCLU 1} {$ITCLU <= $NTCLU} {incr ITCLU} {
     set TXT [string range "$TXTTCLU($ITCLU)   " 0 7]
     set OUT [format " %5i%7i     %8s%8i%9.3f" \
              $ITCLU $ZTCLU($ITCLU) $TXT $NATCLU($ITCLU) $CONCCLU($ITCLU)]
     for {set IA 1} {$IA <= $NATCLU($ITCLU)} {incr IA} {
        append OUT [format "%5i" $IQCLU_ATCLU($IA,$ITCLU)]
     }
     puts $sysdat $OUT
   }

}

#---------------------------------------------------------------------------------------

close $sysdat
 
writescr0  .d.tt "\nINFO from <write_sysfile>: \
sysfile $sysfile$syssuffix written \n\n" 

eval set res [catch "exec cat $sysfile$syssuffix  " message] 
writescr  .d.tt "\n$message \n\n "
#
writescr .d.tt "\nsystem file $sysfile$syssuffix written \n "

set sys_data_available 1

if {$DIMENSION=="3D"} {set sysfile_3DBULK $sysfile}

}
#                                                                      write_sysfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


########################################################################################
#                                                                   reread   system file
proc read_sysfile { } {

global structure_window_calls 
global sysfile syssuffix NCPA sysfile_host sysfile_3DBULK
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint irel Wcsys
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT TXTT0
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE CLUSTER_TYPE
global NM IMT IMQ RWSM NAT QMTET QMPHI
global NCL NQCL IQECL WYCKOFFQ WYCKOFFCL ICLQ DIMENSION
global xband_version 
#

global CLUSTER_TYPE
global NQCLU NTCLU IQ_IQCLU N5VEC_IQCLU RQCLU
global NOCC_IQCLU ZTCLU TXTTCLU CONCCLU ITCLU_OQCLU

#------------------------------------------------------------------------------------ 2D
#
global LATTICE_TYPE  ZRANGE_TYPE  STRUCTURE_SEQUENCE
global struc_L_eq_struc_R bulk_L_eq_bulk_R OCCUPATION_OF_LAYERS
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
global NQ_bulk_L NQ_bulk_R
global N_rep_SU_bulk_L N_rep_SU_bulk_R NSUBSYS SU_SUBSYS N_SU SU_NAME SU_IQ
global sys_data_available
#
set tcl_precision 17

set system_file $sysfile$syssuffix

set PRC read_sysfile

debug $PRC WRHEAD

if {[file exists $system_file]} {
  writescrd .d.tt $PRC "\n\nINFO from <read_sysfile>: \
  reading  sysfile $sysfile$syssuffix  \n \n" 
} else {
  writescrd .d.tt $PRC "\n system file $sysfile$syssuffix does not exist \n "
  return
}
    
    
#=======================================================================================
#                             determine version of system file
#=======================================================================================
set sysdat [open $system_file r] 

gets $sysdat text_line
gets $sysdat text_line
gets $sysdat text_line
if {[lindex $text_line 0] == "xband-version" } { 
   gets $sysdat version
} else {
    set version 0
}
close $sysdat

writescrd .d.tt $PRC "system file format version  $version \n \n"





#=======================================================================================
#                               start reading system file
#=======================================================================================
set sysdat [open $system_file r] 



#OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
#                               OLD system file
#OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
if {$version== 0} {


gets $sysdat text_line
writescrd .d.tt $PRC "$text_line\n"

if {[string trim $text_line] == ""} {
  writescrd .d.tt $PRC "\n system file $sysfile$syssuffix is empty \n "
  return
}

#
#------------ extract information on DIMENSION and eventually CLUSTER (+0D)
#
set DIMENSION_INFO [lindex $text_line 0]
puts "<$PRC>:  DIMENSION_INFO $DIMENSION_INFO"

regsub "\\-system" $DIMENSION_INFO "" DIMENSION_INFO

regsub "\\+0D"     $DIMENSION_INFO "" DIMENSION

if {[string first "+0D" $DIMENSION_INFO]>0} {
   set CLUSTER_TYPE "embedded" 
} else {
   set CLUSTER_TYPE "NONE" 
}

if {[lindex $text_line 0] == "3D-system" } { 
   set DIMENSION "3D"
} elseif {[lindex $text_line 0] == "2D-system" } {
   set DIMENSION "2D"
   set SPACEGROUP    0
   set SPACEGROUP_AP 0
} elseif {[lindex $text_line 0] == "1D-system" } {
   set DIMENSION "1D"
} else {
   set DIMENSION "3D"
}
#
puts "<$PRC>:  DIMENSION_INFO $DIMENSION_INFO"
puts "<$PRC>:  DIMENSION      $DIMENSION"
puts "<$PRC>:  CLUSTER_TYPE   $CLUSTER_TYPE"

#
#--------------------------- supply table
#
table_bravais

#=======================================================================================
#                        read   system file
#=======================================================================================
#

gets $sysdat line                  
writescrd .d.tt $PRC "$line\n"
gets $sysdat text_line  
gets $sysdat line               
set BRAVAIS [lindex $line 0]       
writescrd .d.tt $PRC "\n$text_line    $line\n$BRAVAIS  $TABBRAVAIS($BRAVAIS)\n"

gets $sysdat text_line
gets $sysdat line
set SPACEGROUP    [lindex $line 0]
set SPACEGROUP_AP [lindex $line 1] 
writescrd .d.tt $PRC "\n$text_line\n[format %5i%5i  $SPACEGROUP  $SPACEGROUP_AP]\n"

gets $sysdat text_line
gets $sysdat STRUCTURE_TYPE
writescrd .d.tt $PRC "\n$text_line\n$STRUCTURE_TYPE\n"

gets $sysdat text_line
gets $sysdat ALAT
writescrd .d.tt $PRC "\n$text_line\n[format %18.12f $ALAT]\n"

gets $sysdat text_line
gets $sysdat line
set BOA [lindex $line 0]
set COA [lindex $line 1]
writescrd .d.tt $PRC "\n$text_line\n[format %18.12f%18.12f $BOA $COA]\n"

gets $sysdat text_line
gets $sysdat line
set LATPAR(1) [lindex $line 0]
set LATPAR(2) [lindex $line 1]
set LATPAR(3) [lindex $line 2]
writescrd .d.tt $PRC "\n$text_line\n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATPAR(1) $LATPAR(2) $LATPAR(3)]\n"

gets $sysdat text_line
gets $sysdat line
set LATANG(1) [lindex $line 0]
set LATANG(2) [lindex $line 1]
set LATANG(3) [lindex $line 2]
writescrd .d.tt $PRC "\n$text_line\n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATANG(1) $LATANG(2) $LATANG(3)]\n"

gets $sysdat text_line
writescrd .d.tt $PRC "\n$text_line\n"
for {set I 1} {$I <= 3} {incr I} {
   gets $sysdat line
   set RBASX($I) [lindex $line 0]
   set RBASY($I) [lindex $line 1]
   set RBASZ($I) [lindex $line 2]
   writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $RBASX($I) $RBASY($I) $RBASZ($I)]\n"
}
#
#------------------------------------------------------------------------------------ 2D
#
if {$DIMENSION == "2D" } {
   gets $sysdat text_line
   gets $sysdat LATTICE_TYPE
   writescrd .d.tt $PRC "\n$text_line\n$LATTICE_TYPE\n"

   gets $sysdat text_line
   gets $sysdat ZRANGE_TYPE
   writescrd .d.tt $PRC "\n$text_line\n$ZRANGE_TYPE\n"

   gets $sysdat text_line
   gets $sysdat OCCUPATION_OF_LAYERS
   writescrd .d.tt $PRC "\n$text_line\n$OCCUPATION_OF_LAYERS\n"

   gets $sysdat text_line
   gets $sysdat NSUBSYS
   writescrd .d.tt $PRC "\n$text_line\n[format %3i $NSUBSYS]\n"

   gets $sysdat text_line
   gets $sysdat N_SU
   writescrd .d.tt $PRC "\n$text_line\n[format %3i $N_SU]\n"

   gets $sysdat text_line
   writescrd .d.tt $PRC "\n$text_line\n"
   for {set I 1} {$I <= $N_SU} {incr I} {
     gets $sysdat SU_NAME($I)
     writescrd .d.tt $PRC "$SU_NAME($I)\n"
   }

   if { $ZRANGE_TYPE=="extended" } {
      gets $sysdat text_line
      gets $sysdat bulk_L_eq_bulk_R
      writescrd .d.tt $PRC "\n$text_line\n$bulk_L_eq_bulk_R\n"

      gets $sysdat text_line
      gets $sysdat struc_L_eq_struc_R
      writescrd .d.tt $PRC "\n$text_line\n$struc_L_eq_struc_R\n"

      gets $sysdat text_line
      gets $sysdat NQ_bulk_L
      writescrd .d.tt $PRC "\n$text_line\n[format %3i $NQ_bulk_L]\n"

      gets $sysdat text_line
      gets $sysdat N_rep_SU_bulk_L
      writescrd .d.tt $PRC "\n$text_line\n[format %3i $N_rep_SU_bulk_L]\n"

      gets $sysdat text_line
      writescrd .d.tt $PRC "\n$text_line\n"
      for {set I 1} {$I <= 3} {incr I} {
         gets $sysdat line
         set RBASX_L($I) [lindex $line 0]
         set RBASY_L($I) [lindex $line 1]
         set RBASZ_L($I) [lindex $line 2]
         writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f\
                          $RBASX_L($I) $RBASY_L($I) $RBASZ_L($I)]\n"
      }

      gets $sysdat text_line
      gets $sysdat NQ_bulk_R
      writescrd .d.tt $PRC "\n$text_line\n[format %3i $NQ_bulk_R]\n"

      gets $sysdat text_line
      gets $sysdat N_rep_SU_bulk_R
      writescrd .d.tt $PRC "\n$text_line\n$N_rep_SU_bulk_R\n"

      gets $sysdat text_line
      writescrd .d.tt $PRC "\n$text_line\n"
      for {set I 1} {$I <= 3} {incr I} {
         gets $sysdat line
         set RBASX_R($I) [lindex $line 0]
         set RBASY_R($I) [lindex $line 1]
         set RBASZ_R($I) [lindex $line 2]
         writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f\
                          $RBASX_R($I) $RBASY_R($I) $RBASZ_R($I)]\n"
      }
   }
}
#------------------------------------------------------------------------------------ 2D

} else {

#NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
#                               NEW system file
#NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

gets $sysdat text_line
writescrd .d.tt $PRC "$text_line\n"

if {[string trim $text_line] == ""} {
  writescrd .d.tt $PRC "\n system file $sysfile$syssuffix is empty \n "
  return
}
gets $sysdat sysfile_inp
writescrd .d.tt $PRC "\nsystem file name in input file: $sysfile_inp \n "
#
#--------------------------------------------------------------------------------------
#          get information on version, DIMENSION and eventually CLUSTER (+0D)
#--------------------------------------------------------------------------------------
#
set CLUSTER_TYPE "NOT SPECIFIED"
set version 0

gets $sysdat text_line
#
if {[lindex $text_line 0] != "xband-version" } { 
  writescrd .d.tt $PRC "\n version in system file $sysfile$syssuffix inconsistent \n "
  return
}

gets $sysdat version

gets $sysdat text_line
gets $sysdat DIMENSION
writescrd .d.tt $PRC "\n$text_line\n$DIMENSION\n"

if {$DIMENSION == "0D" } { 

   gets $sysdat text_line
   gets $sysdat DIMENSION_HOST
   writescrd .d.tt $PRC "\n$text_line\n$DIMENSION_HOST\n"

} else {
   set DIMENSION_HOST "NOT SPECIFIED"
}

#--------------------------------------------------------------------------------------

if {$DIMENSION != "3D" } {
   set SPACEGROUP    0
   set SPACEGROUP_AP 0
}
#
puts "<$PRC>:  DIMENSION_HOST $DIMENSION_HOST"
puts "<$PRC>:  DIMENSION      $DIMENSION"
puts "<$PRC>:  CLUSTER_TYPE   $CLUSTER_TYPE"

#
#--------------------------- supply table
#
table_bravais

#=======================================================================================
#
gets $sysdat text_line 
gets $sysdat line               
set BRAVAIS [lindex $line 0]       
writescrd .d.tt $PRC "\n$text_line    $line\n$BRAVAIS  $TABBRAVAIS($BRAVAIS)\n"

gets $sysdat text_line
gets $sysdat line
set SPACEGROUP    [lindex $line 0]
set SPACEGROUP_AP [lindex $line 1] 
writescrd .d.tt $PRC "\n$text_line\n[format %5i%5i  $SPACEGROUP  $SPACEGROUP_AP]\n"

gets $sysdat text_line
gets $sysdat STRUCTURE_TYPE
writescrd .d.tt $PRC "\n$text_line\n$STRUCTURE_TYPE\n"

gets $sysdat text_line
gets $sysdat ALAT
writescrd .d.tt $PRC "\n$text_line\n[format %18.12f $ALAT]\n"

gets $sysdat text_line
gets $sysdat line
set BOA [lindex $line 0]
set COA [lindex $line 1]
writescrd .d.tt $PRC "\n$text_line\n[format %18.12f%18.12f $BOA $COA]\n"

gets $sysdat text_line
gets $sysdat line
set LATPAR(1) [lindex $line 0]
set LATPAR(2) [lindex $line 1]
set LATPAR(3) [lindex $line 2]
writescrd .d.tt $PRC "\n$text_line\n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATPAR(1) $LATPAR(2) $LATPAR(3)]\n"

gets $sysdat text_line
gets $sysdat line
set LATANG(1) [lindex $line 0]
set LATANG(2) [lindex $line 1]
set LATANG(3) [lindex $line 2]
writescrd .d.tt $PRC "\n$text_line\n"
writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $LATANG(1) $LATANG(2) $LATANG(3)]\n"

gets $sysdat text_line
writescrd .d.tt $PRC "\n$text_line\n"
for {set I 1} {$I <= 3} {incr I} {
   gets $sysdat line
   set RBASX($I) [lindex $line 0]
   set RBASY($I) [lindex $line 1]
   set RBASZ($I) [lindex $line 2]
   writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f $RBASX($I) $RBASY($I) $RBASZ($I)]\n"
}
#
#------------------------------------------------------------------------------------ 2D
#
if {$DIMENSION == "2D" || $DIMENSION_HOST == "2D" } {

      set LATTICE_TYPE          "NOT SPECIFIED"
      set OCCUPATION_OF_LAYERS  "NOT SPECIFIED"
      set NSUBSYS               "NOT SPECIFIED"
      set N_SU                  1
      set SU_NAME(1)            "NOT SPECIFIED"
      set bulk_L_eq_bulk_R      "NOT SPECIFIED"
      set struc_L_eq_struc_R    "NOT SPECIFIED"
      set N_rep_SU_bulk_L       1
      set N_rep_SU_bulk_R       1

      gets $sysdat text_line
      gets $sysdat ZRANGE_TYPE
      writescrd .d.tt $PRC "\n$text_line\n$ZRANGE_TYPE\n"

      if { $ZRANGE_TYPE=="extended" } {

         gets $sysdat text_line
         gets $sysdat NQ_bulk_L
         writescrd .d.tt $PRC "\n$text_line\n[format %3i $NQ_bulk_L]\n"

         gets $sysdat text_line
         writescrd .d.tt $PRC "\n$text_line\n"
         for {set I 1} {$I <= 3} {incr I} {
            gets $sysdat line
            set RBASX_L($I) [lindex $line 0]
            set RBASY_L($I) [lindex $line 1]
            set RBASZ_L($I) [lindex $line 2]
            writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f\
                             $RBASX_L($I) $RBASY_L($I) $RBASZ_L($I)]\n"
         }
   
         gets $sysdat text_line
         gets $sysdat NQ_bulk_R
         writescrd .d.tt $PRC "\n$text_line\n[format %3i $NQ_bulk_R]\n"

         gets $sysdat text_line
         writescrd .d.tt $PRC "\n$text_line\n"
         for {set I 1} {$I <= 3} {incr I} {
            gets $sysdat line
            set RBASX_R($I) [lindex $line 0]
            set RBASY_R($I) [lindex $line 1]
            set RBASZ_R($I) [lindex $line 2]
            writescrd .d.tt $PRC "[format %18.12f%18.12f%18.12f\
                          $RBASX_R($I) $RBASY_R($I) $RBASZ_R($I)]\n"
         }
      }

}
#------------------------------------------------------------------------------------ 2D



}
#NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN



#
#--------------------------------------------------------------------------------- sites
#
gets $sysdat text_line
gets $sysdat NQ
writescrd .d.tt $PRC "\n$text_line\n[format %3i $NQ]\n"

gets $sysdat text_line
writescrd .d.tt $PRC "\n$text_line\n"
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   gets $sysdat line
   set i 0
   incr i ; set ICLQ($IQ) [lindex $line $i]
   set TXT [format "%3i%4i" $IQ $ICLQ($IQ) ]

   if {$DIMENSION == "2D" || $DIMENSION_HOST == "2D" } {
      incr i ; set SU_IQ($IQ)   [lindex $line $i]
      incr i ; set WYCKOFFQ($IQ) [lindex $line $i]
      append TXT [format "%3i  %1s" $SU_IQ($IQ) $WYCKOFFQ($IQ) ]
   }
   incr i ; set RQX($IQ)  [lindex $line $i]
   incr i ; set RQY($IQ)  [lindex $line $i]
   incr i ; set RQZ($IQ)  [lindex $line $i]
   incr i ; set RWS($IQ)  [lindex $line $i]
   incr i ; set NLQ($IQ)  [lindex $line $i]
   incr i ; set NOQ($IQ)  [lindex $line $i]
   append TXT [format "%18.12f%18.12f%18.12f  %18.12f%4i%5i " \
              $RQX($IQ) $RQY($IQ) $RQZ($IQ) $RWS($IQ) $NLQ($IQ) $NOQ($IQ)]

   for {set IA 1} {$IA <= $NOQ($IQ)} {incr IA} {
      incr i ; set ITOQ($IA,$IQ)  [lindex $line $i]
      append TXT [format "%3i" $ITOQ($IA,$IQ)]
   }

   writescrd .d.tt $PRC "$TXT\n"
}
#
#-------------------------------------------------------------------------- site classes
#
gets $sysdat text_line
gets $sysdat NCL
writescrd .d.tt $PRC "\n$text_line\n[format %3i $NCL]\n"

gets $sysdat text_line
writescrd .d.tt $PRC "\n$text_line\n"
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   gets $sysdat line
   set WYCKOFFCL($ICL) [lindex $line 1]
   set NQCL($ICL)      [lindex $line 2]
   set TXT [format "%3i   %1s%5i" $ICL $WYCKOFFCL($ICL) $NQCL($ICL)]

   set i 2
   for {set IE 1} {$IE <= $NQCL($ICL)} {incr IE} {
      incr i
      set IQECL($IE,$ICL)  [lindex $line $i]
      append TXT [format %3i $IQECL($IE,$ICL)]
   }
   writescrd .d.tt $PRC "$TXT\n"
}
#
#--------------------------------------------------------------------------------- types
#
gets $sysdat text_line
gets $sysdat NT
writescrd .d.tt $PRC "\n$text_line\n[format %3i $NT]\n"

gets $sysdat text_line
writescrd .d.tt $PRC "\n$text_line\n"
for {set IT 1} {$IT <= $NT} {incr IT} {
   gets $sysdat line
   set   ZT($IT) [lindex $line 1]
   set TXTT($IT) [lindex $line 2]
   set TXTT0($IT) $TXTT($IT)
   set  NAT($IT) [lindex $line 3]
   set CONC($IT) [lindex $line 4]
   set TXT [string range "$TXTT($IT)   " 0 7]
   set OUT [format " %2i%4i  %8s%5i%6.3f" $IT $ZT($IT) $TXT $NAT($IT) $CONC($IT)]

   set i 4
   for {set IA 1} {$IA <= $NAT($IT)} {incr IA} {
      incr i
      set IQAT($IA,$IT) [lindex $line $i]
      append OUT [format "%3i" $IQAT($IA,$IT)]
   }
   writescrd .d.tt $PRC "$OUT\n"
}

set MAG_DIR(1) [lindex $line 0]
set MAG_DIR(2) [lindex $line 1]
set MAG_DIR(3) [lindex $line 2]

set NCPA 0
for {set IQ 1} {$IQ <= $NQ} {incr IQ} { if {$NOQ($IQ) != 1} { set NCPA 1 } }   
#
#----------------------------------------------------------------------- find mesh table
#
set NM 0
for {set ICL 1} {$ICL <= $NCL} {incr ICL} {
   set IQ1 $IQECL(1,$ICL)
   incr NM
   set RWSM($NM) $RWS($IQ1)
   set IMQ($IQ1) $NM  
   for {set IE 2} {$IE <= $NQCL($ICL)} {incr IE} {
      set IQ $IQECL($IE,$ICL)
      set IMQ($IQ) $IMQ($IQ1)
   }
}   

for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
  for {set IO 1} {$IO <= $NOQ($IQ)} {incr IO} {
     set IT $ITOQ($IO,$IQ)
     set IMT($IT) $IMQ($IQ)
  }
}
#
#---------------------------------------------------------------------- init QMTET QMPHI
#
for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
   set QMTET($IQ) 0.0
   set QMPHI($IQ) 0.0
}

#
#
#=======================================================================================
#                            reading   OD system information
#=======================================================================================
if {$DIMENSION == "0D" } {

   puts "reading cluster information"
   gets $sysdat text_line
   gets $sysdat text_line
   gets $sysdat CLUSTER_TYPE
   writescrd .d.tt $PRC "\n$text_line\n$CLUSTER_TYPE\n"

#------------------------------------------------------------------- 0D embedded cluster
   if {$CLUSTER_TYPE != "embedded"} {
      give_warning "." "WARNING \n\n from <read_sysfile>  \n\n
                       cluster type read: $CLUSTER_TYPE  \n\n
                       not consistent with:  embedded \n"
      return
   }

   gets $sysdat text_line
   gets $sysdat  sysfile_host
   writescrd .d.tt $PRC "\n${text_line}: $sysfile_host$syssuffix\n"
   puts "host system file name:   $sysfile_host$syssuffix"

   gets $sysdat text_line
   gets $sysdat NQCLU
   writescrd .d.tt $PRC "\n$text_line\n[format %6i $NQCLU]\n"

   gets $sysdat text_line
   writescrd .d.tt $PRC "\n$text_line\n"
   for {set IQCLU 1} {$IQCLU <= $NQCLU} {incr IQCLU} {

      gets $sysdat line
      set       RQCLU(1,$IQCLU) [lindex $line  1]
      set       RQCLU(2,$IQCLU) [lindex $line  2]
      set       RQCLU(3,$IQCLU) [lindex $line  3]

      set N5VEC_IQCLU(1,$IQCLU) [lindex $line  4]
      set N5VEC_IQCLU(2,$IQCLU) [lindex $line  5]
      set N5VEC_IQCLU(3,$IQCLU) [lindex $line  6]
      set N5VEC_IQCLU(4,$IQCLU) [lindex $line  7]
      set N5VEC_IQCLU(5,$IQCLU) [lindex $line  8]

      set      IQ_IQCLU($IQCLU) [lindex $line  9]

      set    NOCC_IQCLU($IQCLU) [lindex $line 10]

      set i 10
      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
          incr i
          set  ITCLU_OQCLU($IO,$IQCLU) [lindex $line $i]
      } 


      set TXT [format "%4i" $IQCLU ]
      append TXT [format "%18.12f%18.12f%18.12f " \
                 $RQCLU(1,$IQCLU) $RQCLU(2,$IQCLU) $RQCLU(3,$IQCLU)]
      append TXT [format "%4i%4i%4i%4i%4i  %4i" \
                 $N5VEC_IQCLU(1,$IQCLU) $N5VEC_IQCLU(2,$IQCLU) $N5VEC_IQCLU(3,$IQCLU) \
                 $N5VEC_IQCLU(4,$IQCLU) $N5VEC_IQCLU(5,$IQCLU)  $IQ_IQCLU($IQCLU) ]

      append TXT [format "%6i " $NOCC_IQCLU($IQCLU)]

      for {set IO 1} {$IO <= $NOCC_IQCLU($IQCLU)} {incr IO} {
          append TXT [format "%5i" $ITCLU_OQCLU($IO,$IQCLU)]
      } 
      writescrd .d.tt $PRC "$TXT\n"
   }

   gets $sysdat text_line
   gets $sysdat NTCLU
   writescrd .d.tt $PRC "\n$text_line\n[format %6i $NTCLU]\n"

   gets $sysdat text_line
   writescrd .d.tt $PRC "\n$text_line\n"
   for {set ITCLU 1} {$ITCLU <= $NTCLU} {incr ITCLU} {

      gets $sysdat line
      set          ZTCLU($ITCLU) [lindex $line  1]
      set        TXTTCLU($ITCLU) [lindex $line  2]
      set         NATCLU($ITCLU) [lindex $line  3]
      set        CONCCLU($ITCLU) [lindex $line  4]
      set i 4
      for {set IA 1} {$IA <= $NATCLU($ITCLU)} {incr IA} {
        incr i
        set IQCLU_ATCLU($IA,$ITCLU) [lindex $line $i]
      }

      set TXT [string range "$TXTTCLU($ITCLU)   " 0 7]
      set OUT [format " %5i%7i     %8s%8i%9.3f" \
               $ITCLU $ZTCLU($ITCLU) $TXT $NATCLU($ITCLU) $CONCCLU($ITCLU)]
      for {set IA 1} {$IA <= $NATCLU($ITCLU)} {incr IA} {
         append OUT [format "%5i" $IQCLU_ATCLU($IA,$ITCLU)]
      }
      writescrd .d.tt $PRC "$OUT\n"
   }

}

#---------------------------------------------------------------------------------------

close $sysdat

writescrd .d.tt $PRC "  \n\nsystem file $sysfile$syssuffix read in \n "

#
#------------------------------------------------------------------- 0D embedded cluster
#

set sys_data_available 1

if {$DIMENSION=="3D"} {set sysfile_3DBULK $sysfile}

}  
#                                                                       read_sysfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


