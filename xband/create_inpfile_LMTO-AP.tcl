########################################################################################
#                                    LMTO-AP
########################################################################################
#                                                                         create_inpfile
#     
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and 
#     adds the corresponding entries to the input mask created by   create_inpfile
#     
#  
proc create_inpfile_LMTO-AP {} {

global structure_window_calls inpfile inpsuffix 
global sysfile syssuffix  PACKAGE
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
global Wprog inpsuffixlist VAR VARLST NCPA
global COLOR WIDTH HEIGHT FONT
#
   pack forget $Wcinp.a.aa.edit
   pack forget $Wcinp.a.bb.write
   pack forget $Wcinp.a.bb.append

   if {$NCPA>0} {
      give_warning "." "WARNING \n\n \
      NCPA > 0   in  <create_inpfile> \n\n\n \
      program package \n\n $PACKAGE \n\n \
      cannot deal with non-stoichiometric compounds \n\n "
      destroy $Wcinp      
      set sysfile ""
      create_inpfile     
      return
    }
    set inpsuffix ".lmt"   
} 
#                                                                     create_inpfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



########################################################################################
#                                                                       write_inpfile
proc write_inpfile_LMTO-AP {Wcinp tuwas} {

global structure_window_calls inpfile inpsuffix potfile potsuffix
global sysfile syssuffix  PACKAGE
global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys
global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT 
global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
global Wprog inpsuffixlist  VAR VARLST 
global editor edtext edxterm edoptions edsyntaxhelp edback  
global NM IMT IMQ RWSM QMTET QMPHI 
global buttonlist_PROGS
global NCL NQCL IQECL ICLQ
global RQU RQV RQW

set tcl_precision 17
set PI 3.141592653589793238462643              

#
set file_name $inpfile
if {$tuwas == "append" && [file exists $file_name]==1} {
   set inp [open $file_name a]
} else {
   set inp [open $file_name w]
}

if {$SPACEGROUP!=0} {
#=======================================================================================
#                                                                   space group is KNOWN
#
#--------------------- get cryst coordinates RQU RQV RQW
#
   convert_basis_vectors_RQ cart_to_cryst

   puts  $inp  [expr 10000 + $SPACEGROUP_AP]
   puts  $inp  $STRUCTURE_TYPE
   set a [format "%12.7f%12.7f%12.7f%12.7f%12.7f%12.7f  / a, b/a, c/a, alf, bet, gam" \
                  $ALAT $BOA $COA $LATANG(1) $LATANG(2) $LATANG(3)]
   puts  $inp  $a 
   puts  $inp [format "%5i /nsort" $NT]
   puts  $inp "      u           v           w      name"
   for {set IT 1} {$IT <= $NT} {incr IT} {
      set IQ  $IQAT(1,$IT)

      set UVW [format "%12.7f%12.7f%12.7f" $RQU($IQ) $RQV($IQ) $RQW($IQ) ]
#     set UVW [format "%12.7f%12.7f%12.7f" $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
      puts  $inp "$UVW \'$TXTT($IT)\'/ "
   }

} else {
#=======================================================================================
#                                                                 space group is UNKNOWN
   puts  $inp  $SPACEGROUP
   puts  $inp  $STRUCTURE_TYPE
   set a [format "%12.7f 1.0 1.0 0.0 0.0 0.0 /a dummies"  $ALAT]
   puts  $inp  $a 
   
   puts  $inp "STRUCTURE"
   for {set I 1} {$I <= 3} {incr I} {
     puts $inp [format "%18.12f%18.12f%18.12f   / a%1i" $RBASX($I) $RBASY($I) $RBASZ($I) $I]
   }

   puts  $inp [format "%5i /natoms" $NQ]
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IT  $ITOQ(1,$IQ)

      set UVW [format "%12.7f%12.7f%12.7f" $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
      puts  $inp "$UVW \'$TXTT($IT)\'/ "
   }
}
#=======================================================================================

   close $inp

} 
#                                                                      write_inpfile END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

