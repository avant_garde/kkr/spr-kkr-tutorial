#!/bin/bash

PARENTDIR=`pwd`

KKRSCF=~/Documents/bin/kkrscf7.7
KKRGEN=~/Documents/bin/kkrgen7.7

for U in 1 2 3 4 5
do
	WORKDIR=U${U}

	if [ ! -d ${WORKDIR} ] 
	then
		mkdir -p ${WORKDIR}
	fi
	cd ${WORKDIR}

	echo "current directory: `pwd`"

	ln -snf $PARENTDIR/Fe.pot .

	sed -e "s/UEFF1 = 0.00 eV/UEFF1 = ${U} eV/" \
	    -e "s/JEFF1 = 0.00 eV/JEFF1 = 0.90 eV/" $PARENTDIR/Fe_LDA+U.inp \
		> Fe_LDA+U_${U}.inp

	sed -e "s/UEFF1 = 0.00 eV/UEFF1 = ${U} eV/" \
        -e "s/JEFF1 = 0.00 eV/JEFF1 = 0.90 eV/" $PARENTDIR/Fe_LDA+U_DOS.inp \
        > Fe_LDA+U_${U}_DOS.inp

	# run scf first
	$KKRSCF Fe_LDA+U_${U}.inp > Fe_LDA+U_${U}.out
	mv Fe.pot_new Fe.pot
	
	$KKRGEN Fe_LDA+U_${U}_DOS.inp > Fe_LDA+U_${U}_DOS.out

	cd ..
done
	
cd $PARENTDIR




